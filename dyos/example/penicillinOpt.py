import pydyos as pd
import os
# Johannes M. M. Faust, Jan C. Schulze
# Penicillin reactor example
# Tutorial Presentation for using DyOS
# Process Systems Engineering, RWTH Aachen University


# eso input setup

esoInput = pd.EsoInput()
esoInput.type = esoInput.type.FMI
esoInput.relativeFmuTolerance = 1e-6
dirname = os.path.dirname(__file__)
esoInput.model = dirname + "./penicillinFMU"


# stage input setup

stageInput = pd.StageInput()

stageInput.eso = esoInput



# integrator stage input

integratorStageInput = pd.IntegratorStageInput()

# stage final time
stageDuration = pd.ParameterInput()
stageDuration.lowerBound = 150.0
stageDuration.upperBound = 150.0
stageDuration.value = 150.0
stageDuration.sensType = stageDuration.sensType_type.NO
stageDuration.paramType = stageDuration.paramType_type.DURATION



# parameters

gridParameter1 = pd.ParameterGridInput()
gridParameter1.numIntervals = 8
gridParameter1.type = gridParameter1.type.PIECEWISE_LINEAR
gridParameter1.values[:] = [0.1]*(gridParameter1.numIntervals + 1)  # initial guess for control
gridParameter1.timePoints[:] = [i/gridParameter1.numIntervals for i in range(gridParameter1.numIntervals+1)]
gridParameter1.adapt.maxAdaptSteps = 5
gridParameter1.adapt.adaptWave.minRefinementLevel = 1
gridParameter1.adapt.adaptWave.maxRefinementLevel = 20
gridParameter1.adapt.adaptWave.horRefinementDepth = 0
gridParameter1.pcresolution = 2  # finer resolution for path constraints


parameter1 = pd.ParameterInput()
parameter1.name = "F"
parameter1.lowerBound = 0.0
parameter1.upperBound = 10.0
parameter1.paramType = parameter1.paramType_type.PROFILE
parameter1.sensType = parameter1.sensType_type.FULL
parameter1.grids[:] = [gridParameter1]


parameter2 = pd.ParameterInput()
parameter2.name = "X"
parameter2.lowerBound = 0.02
parameter2.upperBound = 2.0
parameter2.value = 1.0
parameter2.paramType = parameter1.paramType_type.INITIAL
parameter2.sensType = parameter1.sensType_type.NO

# add all parameters 
integratorStageInput.parameters[:] = [parameter1, parameter2]

integratorStageInput.duration = stageDuration

integratorStageInput.plotGridResolution = 150  # for finer time resolution of result

stageInput.integrator = integratorStageInput


# optimizer stage input

optimizerStageInput = pd.OptimizerStageInput()


# objective (Mayer type, always a variable of the model)

objective = pd.ConstraintInput()
objective.name = "P"
optimizerStageInput.objective = objective

# constraints (different types: path, endpoint, point (specify time)

constraint1 = pd.ConstraintInput()
constraint1.name = "X"
constraint1.lowerBound = 0
constraint1.upperBound = 40
constraint1.type = constraint1.type_type.PATH


constraint2 = pd.ConstraintInput()
constraint2.name = "S"
constraint2.lowerBound = 0
constraint2.upperBound = 0.5
constraint2.type = constraint1.type_type.PATH


optimizerStageInput.constraints[:] = [constraint1, constraint2]


stageInput.optimizer = optimizerStageInput

stageInput.optimizer.structureDetection.maxStructureSteps = 2


# defining stages

Input = pd.Input()

Input.stages[:] = [stageInput]
Input.stages[0].mapping.fullStateMapping = False
Input.stages[0].treatObjective = True

Input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-8
Input.runningMode = Input.runningMode_type.SINGLE_SHOOTING
Input.integratorInput.type = Input.integratorInput.type_type.NIXE
Input.integratorInput.order = Input.integratorInput.order_type.FIRST_FORWARD
Input.optimizerInput.type = Input.optimizerInput.type_type.IPOPT
Input.optimizerInput.optimizationMode = Input.optimizerInput.optimizationMode_type.MAXIMIZE

Input.optimizerInput.adaptationOptions.adaptStrategy = \
    Input.optimizerInput.adaptationOptions.adaptStrategy_type.ADAPTATION
Input.integratorInput.integratorOptions["absolute tolerance"] = "1e-7"
Input.integratorInput.integratorOptions["relative tolerance"] = "1e-7"

Input.optimizerInput.optimizerOptions["limited_memory_max_history"] = "100"






# creating pyDyos object

PyDyos = pd.PyDyos()

# setting pyDyos input
PyDyos.Input = Input
# save results into json file
PyDyos.nameOutputFile = "penicillinResult"

# solving problem
PyDyos.runPyDyos()

# retrieving Output
Output = PyDyos.Output

# plotting
# for convenience, we gather the data first and then plot
stage = Output.stages[0]
parameters = stage.integrator.parameters
states = stage.integrator.states
plotParameterNames = ["F"]
plotStateNames = ["X", "S", "P"]
UpperBounds = {"X": 40, "S": 0.5}

from matplotlib import pyplot as plt
import numpy as np

# plot the three states and control
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
axs = [ax1, ax2, ax3, ax4]
iAx = -1  # initialize


for i in plotStateNames:
    for j in states:
        if i == j.name:
            iAx += 1
            x = j.grid.timePoints
            y = j.grid.values
            axs[iAx].set_ylabel(i + " [g/L]")
            axs[iAx].set_xlabel('Time [h]')
            axs[iAx].grid()
            axs[iAx].plot(x, y)
            # plot upper bounds
            if i in UpperBounds.keys():
                axs[iAx].plot(x, UpperBounds[i] * np.ones(len(x)), linestyle='dashed')


for i in plotParameterNames:
    for j in parameters:
        if i == j.name:
            iAx += 1
            y = j.grids[0].values
            x = np.array(j.grids[0].timePoints) * j.grids[0].duration

            if str(j.grids[0].type) == "PIECEWISE_LINEAR":
                axs[iAx].plot(x, y)
            else:  # piecewise constant
                y = np.append(y, y[-1])
                axs[iAx].step(x, y, where='post')

            axs[iAx].set_xlabel('Time [h]')
            axs[iAx].set_ylabel(i + " [L/h]")
            axs[iAx].grid()


plt.show()
fig.savefig('penicillin_optimal.png')
