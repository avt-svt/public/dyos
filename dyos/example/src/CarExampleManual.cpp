#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include <utility>
#include <string>

//comprehensive car example
//to compile and run the problem, the correct header and dlls must be available
//it has to be linked against dyos.lib and the bin folder must be in path while
//executing
int main(){
  using namespace UserInput; 
  StageInput stageInput;


  stageInput.eso.type = EsoType::JADE;
  stageInput.eso.model = "parkram";

  stageInput.integrator.duration.lowerBound = 15.0;
  stageInput.integrator.duration.upperBound = 15.0;
  stageInput.integrator.duration.value = 15.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "u";
  stageInput.integrator.parameters[0].lowerBound = 0.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;
  const unsigned numGrids = 1;
  stageInput.integrator.parameters[0].grids.resize(numGrids);

  unsigned numIntervals = 64;
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals, 0.5);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
  stageInput.integrator.parameters[0].grids[0].adapt.maxAdaptSteps = 4;
  stageInput.integrator.parameters[0].grids[0].adapt.adaptType = UserInput::AdaptationInput::SWITCHING_FUNCTION;
  stageInput.integrator.parameters[0].grids[0].adapt.swAdapt.includeTol = 1e-3;
  stageInput.integrator.parameters[0].grids[0].adapt.swAdapt.maxRefinementLevel = 5;


  stageInput.optimizer.objective.name = "obj";
  stageInput.optimizer.constraints.resize(1);
  stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::POINT;
  stageInput.optimizer.constraints[0].name = "g1";
  stageInput.optimizer.constraints[0].lowerBound = -1e30;
  stageInput.optimizer.constraints[0].upperBound = 1e30;
  stageInput.optimizer.constraints[0].timePoint = 0.0;

  Input input;
  input.stages.push_back(stageInput);

  input.integratorInput.daeInit.nonLinSolver.type = UserInput::NonLinearSolverInput::NLEQ1S;

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::LIMEX;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = UserInput::AdaptationOptions::ADAPTATION;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-5;

  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-4";
  input.optimizerInput.optimizerOptions["Function Precision"] = "1.e-5";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["major iterations limit"] = "150";
  input.optimizerInput.optimizerOptions["verify level"] = "-1";
  input.integratorInput.integratorOptions["relative tolerance"] = "1e-5";
  input.integratorInput.integratorOptions["absolute tolerance"] = "1e-5";

  UserOutput::Output output = runDyos(input);
  return 0;
}
