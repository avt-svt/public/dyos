#include "MidoDyos.hpp"
#include <utility>
#include <string>
#include <cassert>

//#define BOOST_TEST_MODULE Mido
//#include "boost/test/unit_test.hpp"
//#include <boost/test/tools/floating_point_comparison.hpp">




//to compile and run the problem, the correct header and dlls must be available
//Changes to "normal" Dyos: Use MidoInput and MidoStageInput and MidoParameterInput and
//for declare binary variables by calling setBinary() on them.
//Use   stage1.integrator.parameters.push_back(ybin3d2).
//Use   input.stages.push_back(stage1).
int main()
{

  UserInput::MidoInput input;
  UserInput::MidoStageInput stage1;

  stage1.eso.type = UserInput::EsoInput::JADE;
  stage1.eso.model = "disjunctive";

  stage1.integrator.duration.lowerBound = 1.0;
  stage1.integrator.duration.upperBound = 1.0;
  stage1.integrator.duration.value = 1.0;
  stage1.integrator.duration.sensType = UserInput::MidoParameterInput::NO;
  stage1.integrator.duration.paramType = UserInput::MidoParameterInput::DURATION;


  UserInput::MidoParameterInput p;
  p.name = "p";
  p.lowerBound = -4.0;
  p.upperBound = 4.0;
  p.value = 4.0;
  p.paramType = UserInput::MidoParameterInput::TIME_INVARIANT;
  p.sensType = UserInput::MidoParameterInput::FULL;
  stage1.integrator.parameters.push_back(p);

  UserInput::MidoParameterInput x10d1 = p;
  x10d1.name = "x10d1";
  x10d1.lowerBound = -30.0;
  x10d1.upperBound = 30.0;
  x10d1.value = 2.0;
  stage1.integrator.parameters.push_back(x10d1);

  UserInput::MidoParameterInput x10d2 = x10d1, x20d1=x10d1, x20d2=x10d1, x30d1=x10d1, x30d2=x10d1;
  x10d2.name = "x10d2";
  x20d1.name = "x20d1";
  x20d2.name = "x20d2";
  x30d1.name = "x30d1";
  x30d2.name = "x30d2";
  stage1.integrator.parameters.push_back(x10d2);
  stage1.integrator.parameters.push_back(x20d1);
  stage1.integrator.parameters.push_back(x20d2);
  stage1.integrator.parameters.push_back(x30d1);
  stage1.integrator.parameters.push_back(x30d2);

  UserInput::MidoParameterInput xobj11 = x10d1, xobj12=x10d1, xobj21=x10d1, xobj22=x10d1, xobj31=x10d1, xobj32=x10d1;
  xobj11.name = "xobj11";
  xobj12.name = "xobj12";
  xobj21.name = "xobj21";
  xobj22.name = "xobj22";
  xobj31.name = "xobj31";
  xobj32.name = "xobj32";
  stage1.integrator.parameters.push_back(xobj11);
  stage1.integrator.parameters.push_back(xobj12);
  stage1.integrator.parameters.push_back(xobj21);
  stage1.integrator.parameters.push_back(xobj22);
  stage1.integrator.parameters.push_back(xobj31);
  stage1.integrator.parameters.push_back(xobj32);



  stage1.optimizer.objective.name = "xobj";

  UserInput::ConstraintInput constr1_lo;
  constr1_lo.name = "constr1_lo";
  constr1_lo.lowerBound = -2000.0;
  constr1_lo.upperBound = 0.0;
  constr1_lo.type = UserInput::ConstraintInput::ENDPOINT;
  stage1.optimizer.constraints.push_back(constr1_lo);

  UserInput::ConstraintInput constr1_up = constr1_lo;
  constr1_up.name = "constr1_up";
  constr1_up.lowerBound = 0.0;
  constr1_up.upperBound = 2000.0;
  stage1.optimizer.constraints.push_back(constr1_up);

  UserInput::ConstraintInput constr2_lo = constr1_lo, constr2_up = constr1_up;
  constr2_lo.name = "constr2_lo";
  constr2_up.name = "constr2_up";
  stage1.optimizer.constraints.push_back(constr2_lo);
  stage1.optimizer.constraints.push_back(constr2_up);

  UserInput::ConstraintInput constr3_lo = constr1_lo, constr3_up = constr1_up;
  constr3_lo.name = "constr3_lo";
  constr3_up.name = "constr3_up";
  stage1.optimizer.constraints.push_back(constr3_lo);
  stage1.optimizer.constraints.push_back(constr3_up);

  UserInput::ConstraintInput constr4_lo = constr1_lo, constr4_up = constr1_up;
  constr4_lo.name = "constr4_lo";
  constr4_up.name = "constr4_up";
  stage1.optimizer.constraints.push_back(constr4_lo);
  stage1.optimizer.constraints.push_back(constr4_up);

  UserInput::ConstraintInput constr5_lo = constr1_lo, constr5_up = constr1_up;
  constr5_lo.name = "constr5_lo";
  constr5_up.name = "constr5_up";
  stage1.optimizer.constraints.push_back(constr5_lo);
  stage1.optimizer.constraints.push_back(constr5_up);

  UserInput::ConstraintInput constr6_lo = constr1_lo, constr6_up = constr1_up;
  constr6_lo.name = "constr6_lo";
  constr6_up.name = "constr6_up";
  stage1.optimizer.constraints.push_back(constr6_lo);
  stage1.optimizer.constraints.push_back(constr6_up);

  UserInput::ConstraintInput constr7_lo = constr1_lo, constr7_up = constr1_up;
  constr7_lo.name = "constr7_lo";
  constr7_up.name = "constr7_up";
  stage1.optimizer.constraints.push_back(constr7_lo);
  stage1.optimizer.constraints.push_back(constr7_up);

  UserInput::ConstraintInput constr8_lo = constr1_lo, constr8_up = constr1_up;
  constr8_lo.name = "constr8_lo";
  constr8_up.name = "constr8_up";
  stage1.optimizer.constraints.push_back(constr8_lo);
  stage1.optimizer.constraints.push_back(constr8_up);

  UserInput::ConstraintInput constr9_lo = constr1_lo, constr9_up = constr1_up;
  constr9_lo.name = "constr9_lo";
  constr9_up.name = "constr9_up";
  stage1.optimizer.constraints.push_back(constr9_lo);
  stage1.optimizer.constraints.push_back(constr9_up);

  UserInput::ConstraintInput constr11_lo = constr1_lo, constr11_up = constr1_up;
  constr11_lo.name = "constr11_lo";
  constr11_up.name = "constr11_up";
  stage1.optimizer.constraints.push_back(constr11_lo);
  stage1.optimizer.constraints.push_back(constr11_up);

  UserInput::ConstraintInput constr13_lo = constr1_lo, constr13_up = constr1_up;
  constr13_lo.name = "constr13_lo";
  constr13_up.name = "constr13_up";
  stage1.optimizer.constraints.push_back(constr13_lo);
  stage1.optimizer.constraints.push_back(constr13_up);

  UserInput::ConstraintInput constr15_lo = constr1_lo, constr15_up = constr1_up;
  constr15_lo.name = "constr15_lo";
  constr15_up.name = "constr15_up";
  stage1.optimizer.constraints.push_back(constr15_lo);
  stage1.optimizer.constraints.push_back(constr15_up);

  UserInput::ConstraintInput obj1_lo = constr1_lo, obj1_up = constr1_up;
  obj1_lo.name = "obj1_lo";
  obj1_up.name = "obj1_up";
  stage1.optimizer.constraints.push_back(obj1_lo);
  stage1.optimizer.constraints.push_back(obj1_up);

  UserInput::ConstraintInput obj2_lo = constr1_lo, obj2_up = constr1_up;
  obj2_lo.name = "obj2_lo";
  obj2_up.name = "obj2_up";
  stage1.optimizer.constraints.push_back(obj2_lo);
  stage1.optimizer.constraints.push_back(obj2_up);

  UserInput::ConstraintInput obj3_lo = constr1_lo, obj3_up = constr1_up;
  obj3_lo.name = "obj3_lo";
  obj3_up.name = "obj3_up";
  stage1.optimizer.constraints.push_back(obj3_lo);
  stage1.optimizer.constraints.push_back(obj3_up);

  UserInput::ConstraintInput obj4_lo = constr1_lo, obj4_up = constr1_up;
  obj4_lo.name = "obj4_lo";
  obj4_up.name = "obj4_up";
  stage1.optimizer.constraints.push_back(obj4_lo);
  stage1.optimizer.constraints.push_back(obj4_up);

  UserInput::ConstraintInput obj5_lo = constr1_lo, obj5_up = constr1_up;
  obj5_lo.name = "obj5_lo";
  obj5_up.name = "obj5_up";
  stage1.optimizer.constraints.push_back(obj5_lo);
  stage1.optimizer.constraints.push_back(obj5_up);

  UserInput::ConstraintInput obj6_lo = constr1_lo, obj6_up = constr1_up;
  obj6_lo.name = "obj6_lo";
  obj6_up.name = "obj6_up";
  stage1.optimizer.constraints.push_back(obj6_lo);
  stage1.optimizer.constraints.push_back(obj6_up);

  UserInput::ConstraintInput obj7_lo = constr1_lo, obj7_up = constr1_up;
  obj7_lo.name = "obj7_lo";
  obj7_up.name = "obj7_up";
  stage1.optimizer.constraints.push_back(obj7_lo);
  stage1.optimizer.constraints.push_back(obj7_up);

  UserInput::ConstraintInput obj8_lo = constr1_lo, obj8_up = constr1_up;
  obj8_lo.name = "obj8_lo";
  obj8_up.name = "obj8_up";
  stage1.optimizer.constraints.push_back(obj8_lo);
  stage1.optimizer.constraints.push_back(obj8_up);

  UserInput::ConstraintInput obj9_lo = constr1_lo, obj9_up = constr1_up;
  obj9_lo.name = "obj9_lo";
  obj9_up.name = "obj9_up";
  stage1.optimizer.constraints.push_back(obj9_lo);
  stage1.optimizer.constraints.push_back(obj9_up);

  UserInput::ConstraintInput obj10_lo = constr1_lo, obj10_up = constr1_up;
  obj10_lo.name = "obj10_lo";
  obj10_up.name = "obj10_up";
  stage1.optimizer.constraints.push_back(obj10_lo);
  stage1.optimizer.constraints.push_back(obj10_up);

  UserInput::ConstraintInput obj11_lo = constr1_lo, obj11_up = constr1_up;
  obj11_lo.name = "obj11_lo";
  obj11_up.name = "obj11_up";
  stage1.optimizer.constraints.push_back(obj11_lo);
  stage1.optimizer.constraints.push_back(obj11_up);

  UserInput::ConstraintInput obj12_lo = constr1_lo, obj12_up = constr1_up;
  obj12_lo.name = "obj12_lo";
  obj12_up.name = "obj12_up";
  stage1.optimizer.constraints.push_back(obj12_lo);
  stage1.optimizer.constraints.push_back(obj12_up);

  stage1.treatObjective = true;
  ///input.stages.push_back(stage1);




  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;



  UserInput::MidoParameterInput ybin1d1, ybin1d2, ybin2d1, ybin2d2, ybin3d1, ybin3d2;
  ybin1d1.name = "ybin1d1";
  ybin1d1.value = 1.0;
  ybin1d1.upperBound=1.0;
  ybin1d1.lowerBound=0.0;
  ybin1d1.setInteger();
  ybin1d2.name = "ybin1d2";
  ybin1d2.value = 0.0;
  ybin1d2.upperBound=1.0;
  ybin1d2.lowerBound=0.0;
  ybin1d2.setInteger();
  ybin2d1.name = "ybin2d1";
  ybin2d1.value = 1.0;
  ybin2d1.upperBound=1.0;
  ybin2d1.lowerBound=0.0;
  ybin2d1.setInteger();
  ybin2d2.name = "ybin2d2";
  ybin2d2.value = 0.0;
  ybin2d2.upperBound=1.0;
  ybin2d2.lowerBound=0.0;
  ybin2d2.setInteger();
  ybin3d1.name = "ybin3d1";
  ybin3d1.value = 1.0;
  ybin3d1.upperBound=1.0;
  ybin3d1.lowerBound=0.0;
  ybin3d1.setInteger();
  ybin3d2.name = "ybin3d2";
  ybin3d2.value = 0.0;
  ybin3d2.upperBound=1.1;
  ybin3d2.lowerBound=0.0;
  ybin3d2.setInteger();


  stage1.integrator.parameters.push_back(ybin1d1);
  stage1.integrator.parameters.push_back(ybin1d2);
  stage1.integrator.parameters.push_back(ybin2d1);
  stage1.integrator.parameters.push_back(ybin2d2);
  stage1.integrator.parameters.push_back(ybin3d1);
  stage1.integrator.parameters.push_back(ybin3d2);

  input.stages.push_back(stage1);

  UserOutput::Output output;
  Logger::Ptr logger(new StandardLogger());
  OutputChannel::Ptr stdOut(new FileChannel("MyFile.txt"));
  //stdOut->setVerbosity(OutputChannel::ERROR_VERBOSITY);
  stdOut->setVerbosity(OutputChannel::DEBUG_VERBOSITY);
  logger->setLoggingChannel(Logger::LoggingChannel::OPTIMIZER,stdOut);
  logger->setLoggingChannel(Logger::LoggingChannel::DYOS_OUT,stdOut);
  logger->setLoggingChannel(Logger::LoggingChannel::ESO,stdOut);



  //test enumeration
  input.startHeuristic=UserInput::MidoInput::StartHeuristic::DIVING;
  output = runDyos(input,logger);
  logger->flushAll();
  assert(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);
  std::cout<<"Solution History Lenght"<<output.solutionHistory.size()<<std::endl;
  assert(output.solutionHistory.size()==127);
  assert(std::abs(output.optimizerOutput.objVal)<= 1E-6);
 

  input.enableAssumptionDyosFindsGlobalSolution();
  output = runDyos(input,logger);
  assert(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);
  std::cout<<"Solution History Lenght"<<output.solutionHistory.size()<<std::endl;
  assert(output.solutionHistory.size()<=39);
  assert(std::abs(output.optimizerOutput.objVal)<= 1E-6);
  //test iteration limit
  input.enableTerminationConditionIterationLimit(10);
  output = runDyos(input,logger);
  std::cout<<"Solution History Lenght"<<output.solutionHistory.size()<<std::endl;
  assert(output.solutionHistory.size()==10);


  //test time limit
  input.enableTerminationConditionTimeLimit(1 /*seconds*/);
  output = runDyos(input,logger);
  std::cout<<"Solution History Lenght"<<output.solutionHistory.size()<<std::endl;
  assert(output.solutionHistory.size()<=5);




}
//test of B&B
//trigger of the following test:
//if DyOS with relaxed variables gives optimizer with exactly one non-binary variable the
//binaries have been fixed in the following branching in old version of optimizeMido()
//-> This may cut off  parts of the tree containing the global optimum
//test case:
//global optimum is [bin1,bin2]=[0,0] whereas relaxed optimization gives [bin1,bin2]=[0.555,1]
//thus bin2 may be fixed wrongly to 1
/*
BOOST_AUTO_TEST_CASE(TestBB_fixBinaries)
{
  UserInput::MidoInput input;
  UserInput::StageInput stage1;

  // integrator input
  stage1.eso.type = UserInput::EsoInput::JADE;
  stage1.eso.model = "testBB_fixBinaries";

  stage1.integrator.duration.lowerBound = 10.0;
  stage1.integrator.duration.upperBound = 10.0;
  stage1.integrator.duration.value = 10.0;
  stage1.integrator.duration.sensType =  UserInput::MidoParameter::NO;
  stage1.integrator.duration.paramType = UserInput::MidoParameter::DURATION;

  UserInput::MidoParameter control;
  control.name = "u";
  control.lowerBound = 0.0;
  control.upperBound = 1.0;
  control.paramType = UserInput::MidoParameter::PROFILE;
  control.sensType = UserInput::MidoParameter::FULL;

  UserInput::ParameterGridInput grid;
  const unsigned numIntervals = 5;
  grid.numIntervals = numIntervals;
  grid.values.resize(numIntervals, 0);
  grid.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  control.grids.push_back(grid);

  stage1.integrator.parameters.push_back(control);

  // optimizer input
  stage1.optimizer.objective.name = "obj";
  stage1.optimizer.objective.lowerBound = -10.0;
  stage1.optimizer.objective.upperBound = 50.0;

  UserInput::ConstraintInput xPathConstr;
  xPathConstr.name = "x";
  xPathConstr.type = UserInput::ConstraintInput::PATH;
  xPathConstr.lowerBound = -20.0;
  xPathConstr.upperBound =  20.0;
  stage1.optimizer.constraints.push_back(xPathConstr);

  // general stage settings
  stage1.mapping.fullStateMapping = false;
  stage1.treatObjective = true;
  input.stages.push_back(stage1);

  // solution methods
  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;

  // Mido settings
  UserInput::MidoParameter bin1, bin2;
  bin1.name = "bin1";
  bin1.value = 0.0;
  bin2.name = "bin2";
  bin2.value = 1.0;

  UserInput::MidoStages midoStage;
  midoStage.binaryParameters.push_back(bin1);
  midoStage.binaryParameters.push_back(bin2);

  input.midoStages.push_back(midoStage);

  // output
  UserOutput::Output output;
  output = runMidoDyos(input,false,LowerBoundInitilizationStrategy::LOCAL_RELAX);

  //check global optimum
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  BOOST_CHECK_EQUAL(output.stages[0].integrator.parameters[1].value, 0); // bin1 == 0
  BOOST_CHECK_EQUAL(output.stages[0].integrator.parameters[2].value, 0); // bin2 == 0
  BOOST_CHECK_CLOSE(output.optimizerOutput.objVal, 0.0, 1E-6);			 // obj  == 0
  //check relaxed optimum (first run)
  BOOST_CHECK_CLOSE(output.solutionHistory[0].stages[0].integrator.parameters[1].value, 0.55555556, 1E-6); // bin1 = 0.555555...
  BOOST_CHECK_CLOSE(output.solutionHistory[0].stages[0].integrator.parameters[2].value, 1.0, 1E-6);        // bin2 = 1
}

BOOST_AUTO_TEST_SUITE_END();
*/
