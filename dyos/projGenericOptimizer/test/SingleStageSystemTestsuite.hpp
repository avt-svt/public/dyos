/** 
* @file SingleStageSystem.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* A complete system test is run here concerning single stage mode. The \n
* system test tests some combinations of integrators, optimizers and   \n
* Eso servers performing a complete optimization.                      \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
* @todo run test for extended car example
*/

#include "SingleStageTestProblemFactory.hpp"

BOOST_AUTO_TEST_SUITE(OptimizerSingleStageSystemTest)

/**
* @test IntegrationTest - test car example using SNOPT as optimizer, 
*                         NIXE as integrator and Jade as Eso server
*/
BOOST_AUTO_TEST_CASE(TestCarExampleSnoptNixeJade)
{
  GenericOptimizer *optimizer3 = createSNOPTOldCar();
  DyosOutput::OptimizerOutput out = optimizer3->solve();
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::FAILED);
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::NOT_OPTIMAL);
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::WRONG_GRADIENTS);
  BOOST_CHECK(out.m_informFlag != DyosOutput::OptimizerOutput::INFEASIBLE);
  
  delete optimizer3;
}

BOOST_AUTO_TEST_SUITE_END()