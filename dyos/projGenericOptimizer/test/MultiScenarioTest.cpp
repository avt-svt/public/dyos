#include "OptimizationProblem.hpp"
#include "MultiScenarioOptProblem.hpp"

class SmallTestProblem : public OptimizationProblem {
private:
  OptimizerProblemDimensions m_opt_prob_dim;
  std::vector<double> m_values;
  std::vector<double> m_lin_lambda;
  std::vector<double> m_nonlin_lambda;
  double m_p;
public:
  SmallTestProblem(double p)
    : m_p(p), m_lin_lambda(1, 0.0), m_nonlin_lambda(1, 0.0)
  {
    m_opt_prob_dim.numOptVars = 2;
    m_opt_prob_dim.numLinConstraints = 1;
    m_opt_prob_dim.numNonLinConstraints = 1;
    m_opt_prob_dim.nonLinObjIndex = -1;
    cs* linJac = cs_spalloc(1, 2, 2, 1, 1);
    linJac->p[0] = 0;
    linJac->i[0] = 0;
    linJac->x[0] = m_p;
    linJac->p[1] = 1;
    linJac->i[1] = 0;
    linJac->x[1] = 1.0;
    linJac->nz = 2;
    m_opt_prob_dim.linJac = linJac;
    cs* nonLinJac = cs_spalloc(1, 2, 2, 1, 1);
    nonLinJac->p[0] = 0;
    nonLinJac->i[0] = 0;
    nonLinJac->p[1] = 1;
    nonLinJac->i[1] = 0;
    nonLinJac->nz = 2;
    m_opt_prob_dim.nonLinJac = nonLinJac;
    // initial values
    m_values.resize(2);
    m_values[0] = 1.0;
    m_values[1] = 2.0;
  }

  virtual ~SmallTestProblem()
  {
    cs_spfree(m_opt_prob_dim.linJac);
    cs_spfree(m_opt_prob_dim.nonLinJac);
  }
  virtual const OptimizerProblemDimensions &getOptProbDim() const{
    return m_opt_prob_dim;
  }
  virtual void getDecVarBounds(std::vector<double> &lowerBounds,
                               std::vector<double> &upperBounds) {
    assert(lowerBounds.size()==2);
    assert(upperBounds.size()==2);
    lowerBounds[0] = 0.0;
    lowerBounds[1] = -1.0;
    upperBounds[0] = 100.0;
    upperBounds[1] = 200.0;
  }
  virtual void getDecVarValues(std::vector<double> &values) {
    assert(values.size()==2);
    values = m_values;
  }
  virtual void setDecVarValues(const std::vector<double> &decVars) {
    assert(decVars.size()==2);
    m_values = decVars;
  }
  virtual void getNonLinConstraintBounds(std::vector<double> &lowerBounds,
                                         std::vector<double> &upperBounds) {
    assert(lowerBounds.size()==1);
    assert(upperBounds.size()==1);
    lowerBounds[0] = 0.0;
    upperBounds[0] = 10.0;
  }
  virtual void getNonLinConstraintValues(std::vector<double> &values) {
    assert(values.size()==1);
    double x,y;
    x = m_values[0];
    y = m_values[1];
    values[0] = m_p*x*y;
  }
  virtual void getNonLinConstraintDerivatives(cs *derivativeMatrix) {
    assert(CS_TRIPLET(derivativeMatrix));
    double x,y;
    x = m_values[0];
    y = m_values[1];
    derivativeMatrix->x[0] = m_p * y;
    derivativeMatrix->x[1] = m_p * x;
  }
  virtual double getNonLinObjValue() {
    return m_values[0] + m_values[1];
  }
  virtual void getNonLinObjDerivative(std::vector<double> &values) {
    assert(values.size()==2);
    values[0] = 1.0;
    values[1] = 1.0;
  }
  virtual void setLagrangeMultipliers(const std::vector<double> &nonLinMultipliers,
                                      const std::vector<double> &linMultipliers) {
    assert(nonLinMultipliers.size()==1);
    assert(linMultipliers.size()==1);
    m_nonlin_lambda[0] = nonLinMultipliers[0];
    m_lin_lambda[0] = linMultipliers[0];
  }
  virtual void getLagrangeMultipliers(std::vector<double> &nonLinMultipliers,
                                      std::vector<double> &linMultipliers) {
    nonLinMultipliers = m_nonlin_lambda;
    linMultipliers = m_lin_lambda;
  }
  virtual void getLinConstraintValues(std::vector<double> &values) {
    assert(values.size()==1);
    values[0] = m_p*m_values[0] + m_values[1];
  }
  virtual void getLinConstraintBounds(std::vector<double> &lowerBounds, std::vector<double> &upperBounds) {
    assert(lowerBounds.size()==1);
    assert(upperBounds.size()==1);
    lowerBounds[0] = 0.0;
    upperBounds[0] = 10.0;
  }
};
