/**
* @file GenericOptimizerTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Test of class GenericOptimizer                                       \n
* =====================================================================\n
* @author Tjalf Hoffmann, Klaus Stockmann, Kathrin Frankl
* @date 11.04.2012
*/


#include "GenericOptimizerTestDummies.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "FirstOrderOptimizationProblem.hpp"
#include "SNOPTWrapper.hpp"
#include "NPSOLLoader.hpp"

BOOST_AUTO_TEST_SUITE(TestGenericOptimizer)

/**
* @test GenericOptimizerTest - test if SNOPTWrapper is working
* @author Klaus Stockmann
*/
BOOST_AUTO_TEST_CASE(TestSNOPTWrapper)
{
  OptimizerSingleStageMetaData optSSMD; // dummy
}

/**
* @brief GenericOptimizerTest - test hardcoded Rosenbrock problem
* @author Tjalf Hoffmann, Kathrin Frankl
*/
BOOST_AUTO_TEST_CASE(RosenbrockTest)
{
  GenericIntegrator::Ptr dummyIntegrator (new EmptyGenericIntegratorDummy());
  OptimizerMetaData::Ptr rosenbrockOmd (new RosenbrockOptimizerMetaData());
  OptimizationProblem::Ptr optProblem(new FirstOrderOptimizationProblem(dummyIntegrator, rosenbrockOmd));
  SNOPTWrapper snopt(optProblem);
  DyosOutput::OptimizerOutput optOut = snopt.solve();
  //precision set in the snopt.config file
  const double functionPrecision = 1e-8;
  BOOST_CHECK_CLOSE(optOut.lagrangeMultiplier[0], 0.0, THRESHOLD);
  BOOST_CHECK_CLOSE(optOut.optDecVarValues[0], 1.0, functionPrecision);
  BOOST_CHECK_CLOSE(optOut.optDecVarValues[1], 1.0, functionPrecision);
}


BOOST_AUTO_TEST_SUITE_END()
