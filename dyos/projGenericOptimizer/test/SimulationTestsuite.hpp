/** 
* @file Simulation.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* A testuite testing or using class SecondOrderIntegration. This class \n
* is no real optimizer class, since it only does a final state         \n
* integration and would by that logic belong to the integrator classes \n
* However for the 2nd order data to be complete some data are needed   \n
* that belong to the optimizer modules (such as Lagrange multiplier and\n
* constraints) so the 2nd order integration is tested fully in this    \n
* testsuite
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 11.04.2012
* @todo run test for extended car example
*/
#include "OptimizerFactory.hpp"
#include "Input.hpp"

BOOST_AUTO_TEST_SUITE(SimulationTests)

BOOST_AUTO_TEST_CASE(TestAdjointsCalculation)
{
  //first construct an optimization problem
  
  FactoryInput::EsoInput eso;
  
  eso.type = EsoType::JADE;
  eso.model = "wobatchinactive";
  
  unsigned numIntervals = 8;
  double durationValue = 1000.0;
  std::vector<double> timePoints(numIntervals +1);
  timePoints.front() = 0.0;
  timePoints.back() = 1.0;
  for(unsigned i=1; i<numIntervals; i++){
    timePoints[i] = i*(1.0/numIntervals);
  }
  std::vector<double> userGrid(2*numIntervals +2);
  userGrid.front() = 0.0;
  userGrid.back() = 1.0;
  for(unsigned i=1; i<2*numIntervals; i++){
    userGrid[i] = i*(0.5/numIntervals);
  }
  
  FactoryInput::ParameterInput fbinCur;
  fbinCur.lowerBound = 0.0;
  fbinCur.upperBound = 5.784;
  fbinCur.sensType = FactoryInput::ParameterInput::FULL;
  fbinCur.type = FactoryInput::ParameterInput::CONTROL;
  fbinCur.esoIndex = 13;
  
  FactoryInput::ParameterizationGridInput fbinCurGrid;
  for(unsigned i=0; i<4; i++){
    fbinCurGrid.values.push_back(5.7839999999999998);
  }
  fbinCurGrid.values.push_back(0.86400019994537391);
  fbinCurGrid.values.push_back(0.0);
  fbinCurGrid.values.push_back(0.0);
  fbinCurGrid.values.push_back(0.0);
  
  fbinCurGrid.timePoints = timePoints;
  fbinCurGrid.duration = durationValue;
  
  assert(fbinCurGrid.timePoints.size() == numIntervals+1);
  fbinCurGrid.approxType = PieceWiseConstant;
  fbinCurGrid.hasFreeDuration = false;
  
  fbinCur.grids.push_back(fbinCurGrid);
  
  FactoryInput::ParameterInput twCur;
  
  twCur.lowerBound = 0.020;
  twCur.upperBound = 0.1;
  twCur.sensType = FactoryInput::ParameterInput::FULL;
  twCur.type = FactoryInput::ParameterInput::CONTROL;
  twCur.esoIndex = 12;
  
  FactoryInput::ParameterizationGridInput twCurGrid;
  twCurGrid.values.push_back(0.082232302959091785);
  twCurGrid.values.push_back(0.064746706219901390);
  twCurGrid.values.push_back(0.035888125780055936);
  twCurGrid.values.push_back(0.10000000000000001);
  twCurGrid.values.push_back(0.053529027313956146);
  twCurGrid.values.push_back(0.020000000000000000);
  twCurGrid.values.push_back(0.029524403098351224);
  twCurGrid.values.push_back(0.10000000000000001);
  assert(twCurGrid.values.size() == numIntervals);
  
  twCurGrid.timePoints = timePoints;
  twCurGrid.duration = durationValue;
  
  twCur.grids.push_back(twCurGrid);
  
  std::vector<FactoryInput::ParameterInput> controls;
  controls.push_back(fbinCur);
  controls.push_back(twCur);
  
  FactoryInput::ParameterInput duration;
  duration.type = FactoryInput::ParameterInput::DURATION;
  duration.lowerBound = durationValue;
  duration.upperBound = durationValue;
  duration.sensType = FactoryInput::ParameterInput::NO;
  duration.value = durationValue;
  
  FactoryInput::IntegratorMetaDataInput integratorMetaDataInput;
  GenericEsoFactory genEsoFac;
  
  integratorMetaDataInput.controls = controls;
  integratorMetaDataInput.duration = duration;
  integratorMetaDataInput.esoPtr = genEsoFac.create(eso);
  integratorMetaDataInput.userGrid = userGrid;
  
  
  FactoryInput::IntegratorInput integratorInput;
  integratorInput.type = FactoryInput::IntegratorInput::NIXE;
  integratorInput.order = FactoryInput::IntegratorInput::SECOND_REVERSE;
  integratorInput.metaDataInput = integratorMetaDataInput;
  
  
  std::vector<FactoryInput::ConstraintInput> constraints;
  FactoryInput::ConstraintInput x7;
  x7.type = FactoryInput::ConstraintInput::POINT;
  x7.esoIndex = 6;
  
  x7.timePoint = 0.0625;
  x7.lagrangeMultiplier = -6.5060155941749657e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.125;
  x7.lagrangeMultiplier = -2.4552534921824986e-008;
  constraints.push_back(x7);
  x7.timePoint = 0.1875;
  x7.lagrangeMultiplier = -2.7786050606296482;
  constraints.push_back(x7);
  x7.timePoint = 0.25;
  x7.lagrangeMultiplier = -1.4386394231553414e-007;
  constraints.push_back(x7);
  x7.timePoint = 0.3125;
  x7.lagrangeMultiplier = -1.9286768248755390;
  constraints.push_back(x7);
  x7.timePoint = 0.375;
  x7.lagrangeMultiplier = -4.4062992173670233e-007;
  constraints.push_back(x7);
  x7.timePoint = 0.4375;
  x7.lagrangeMultiplier = -1.0472776843845250e-008;
  constraints.push_back(x7);
  x7.timePoint = 0.5;
  x7.lagrangeMultiplier = -5.0839946692117579e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.5625;
  x7.lagrangeMultiplier = -2.4006236928790601e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.625;
  x7.lagrangeMultiplier = -1.4832439448170721e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.6875;
  x7.lagrangeMultiplier = -1.0600934778235825e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.75;
  x7.lagrangeMultiplier = -8.0170975443700998e-010;
  constraints.push_back(x7);
  x7.timePoint = 0.8125;
  x7.lagrangeMultiplier = -6.0749242012122018e-010;
  constraints.push_back(x7);
  x7.timePoint = 0.875;
  x7.lagrangeMultiplier = -4.7010833657460413e-010;
  constraints.push_back(x7);
  x7.timePoint = 0.9375;
  x7.lagrangeMultiplier = -2.4095246746257795e-010;
  constraints.push_back(x7);
  x7.timePoint = 1.0;
  x7.lagrangeMultiplier = -4.8399045838140232e-011;
  constraints.push_back(x7);
  
  FactoryInput::ConstraintInput x8;
  x8.type = FactoryInput::ConstraintInput::ENDPOINT;
  x8.lagrangeMultiplier = 1295.7677704702055;
  x8.timePoint = 1.0;
  x8.esoIndex = 7;
  constraints.push_back(x8);

  
  FactoryInput::ConstraintInput objective;
  objective.lagrangeMultiplier = 1.0;
  objective.timePoint = 1.0;
  objective.type = FactoryInput::ConstraintInput::ENDPOINT;
  objective.esoIndex = 8;
  
  FactoryInput::OptimizerMetaDataInput optimizerMetaDataInput;
  optimizerMetaDataInput.constraints = constraints;
  optimizerMetaDataInput.hasTotalEndTime = false;
  optimizerMetaDataInput.objective = objective;
  
  FactoryInput::OptimizerInput input;
  input.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
  input.integratorInput = integratorInput;
  input.metaDataInput = optimizerMetaDataInput;
  


  //then run simulation
  OptimizerFactory optFac;
  GenericOptimizer::Ptr  opt(optFac.createGenericOptimizer(input));
  DyosOutput::Output out;
  out.solutionTime = 0.0;
  out.optimizerOutput = opt->solve();
  out.stageOutput = opt->getOutput();
  
  //compare output
  
  std::ifstream adjointsFileReader("adjointTestResults.txt");
  BOOST_ASSERT(adjointsFileReader.good());

  std::vector<double> comparedAdjoints;
  while(!adjointsFileReader.eof()){
    double d;
    adjointsFileReader >> d;
    if(fabs(d) < THRESHOLD){
      d=0.0;
    }
    comparedAdjoints.push_back(d);
  }
  adjointsFileReader.close();
  
  unsigned num1stOrderAdjoints = out.stageOutput.front().integrator.adjoints.size();
  unsigned num2ndOrderAdjoints = out.stageOutput.front().integrator.adjoints2ndOrder.values.size();
  BOOST_REQUIRE(comparedAdjoints.size()<= num1stOrderAdjoints + num2ndOrderAdjoints);
  double threshold = 0.5;
  //compare 1st order adjoints
  for(unsigned i=0; i<num1stOrderAdjoints; i++){
    if(comparedAdjoints[i] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[i],
                        out.stageOutput.front().integrator.adjoints[i].values.front(),
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput.front().integrator.adjoints[i].values.front(),
                        THRESHOLD);
    }
  }
  //compare 2nd order adjoints
  for(unsigned i=0; i<num2ndOrderAdjoints; i++){
    if(comparedAdjoints[i + num1stOrderAdjoints] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[i + num1stOrderAdjoints],
                        out.stageOutput.front().integrator.adjoints2ndOrder.values[i],
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput.front().integrator.adjoints2ndOrder.values[i],
                        THRESHOLD);
    }
  }

}


BOOST_AUTO_TEST_CASE(TestAdjointsCalculationMultiGrid)
{
  //first construct an optimization problem
  
  FactoryInput::EsoInput eso;
  
  eso.type = EsoType::JADE;
  eso.model = "wobatchinactive";
  
  unsigned numIntervals1 = 4;
  unsigned numIntervals2 = 2*numIntervals1;
  double durationValue1 = 500.0;
  double durationValue2 = 2*durationValue1;
  std::vector<double> timePoints1(numIntervals1 +1);
  timePoints1.front() = 0.0;
  timePoints1.back() = 1.0;
  for(unsigned i=1; i<numIntervals1; i++){
    timePoints1[i] = i*(1.0/numIntervals1);
  }
  std::vector<double> timePoints2(numIntervals2 + 1);
  timePoints2.front() = 0.0;
  timePoints2.back() = 1.0;
  for(unsigned i=1; i<numIntervals2; i++){
    timePoints2[i] = i*(1.0/numIntervals2);
  }
  
  std::vector<double> userGrid(2*numIntervals2 +2);
  userGrid.front() = 0.0;
  userGrid.back() = 1.0;
  for(unsigned i=1; i<2*numIntervals2; i++){
    userGrid[i] = i*(0.5/numIntervals2);
  }
  
  FactoryInput::ParameterInput fbinCur;
  fbinCur.lowerBound = 0.0;
  fbinCur.upperBound = 5.784;
  fbinCur.sensType = FactoryInput::ParameterInput::FULL;
  fbinCur.type = FactoryInput::ParameterInput::CONTROL;
  fbinCur.esoIndex = 13;
  
  FactoryInput::ParameterizationGridInput fbinCurGrid1, fbinCurGrid2;
  fbinCurGrid1.timePoints = timePoints1;
  fbinCurGrid1.duration = durationValue1;
  fbinCurGrid1.approxType = PieceWiseConstant;
  fbinCurGrid1.hasFreeDuration = false;
  
  fbinCurGrid2 = fbinCurGrid1;
  
  for(unsigned i=0; i<4; i++){
    fbinCurGrid1.values.push_back(5.7839999999999998);
  }
  
  fbinCurGrid2.values.push_back(0.86400019994537391);
  fbinCurGrid2.values.push_back(0.0);
  fbinCurGrid2.values.push_back(0.0);
  fbinCurGrid2.values.push_back(0.0);
  
  
  fbinCur.grids.push_back(fbinCurGrid1);
  fbinCur.grids.push_back(fbinCurGrid2);
  
  FactoryInput::ParameterInput twCur;
  
  twCur.lowerBound = 0.020;
  twCur.upperBound = 0.1;
  twCur.sensType = FactoryInput::ParameterInput::FULL;
  twCur.type = FactoryInput::ParameterInput::CONTROL;
  twCur.esoIndex = 12;
  
  FactoryInput::ParameterizationGridInput twCurGrid;
  twCurGrid.values.push_back(0.082232302959091785);
  twCurGrid.values.push_back(0.064746706219901390);
  twCurGrid.values.push_back(0.035888125780055936);
  twCurGrid.values.push_back(0.10000000000000001);
  twCurGrid.values.push_back(0.053529027313956146);
  twCurGrid.values.push_back(0.020000000000000000);
  twCurGrid.values.push_back(0.029524403098351224);
  twCurGrid.values.push_back(0.10000000000000001);
  assert(twCurGrid.values.size() == numIntervals2);
  
  twCurGrid.timePoints = timePoints2;
  twCurGrid.duration = durationValue2;
  
  twCur.grids.push_back(twCurGrid);
  
  std::vector<FactoryInput::ParameterInput> controls;
  controls.push_back(fbinCur);
  controls.push_back(twCur);
  
  FactoryInput::ParameterInput duration;
  duration.type = FactoryInput::ParameterInput::DURATION;
  duration.lowerBound = durationValue2;
  duration.upperBound = durationValue2;
  duration.sensType = FactoryInput::ParameterInput::NO;
  duration.value = durationValue2;
  
  FactoryInput::IntegratorMetaDataInput integratorMetaDataInput;
  GenericEsoFactory genEsoFac;
  
  integratorMetaDataInput.controls = controls;
  integratorMetaDataInput.duration = duration;
  integratorMetaDataInput.esoPtr = genEsoFac.create(eso);
  integratorMetaDataInput.userGrid = userGrid;
  
  
  FactoryInput::IntegratorInput integratorInput;
  integratorInput.type = FactoryInput::IntegratorInput::NIXE;
  integratorInput.order = FactoryInput::IntegratorInput::SECOND_REVERSE;
  integratorInput.metaDataInput = integratorMetaDataInput;
  
  
  std::vector<FactoryInput::ConstraintInput> constraints;
  FactoryInput::ConstraintInput x7;
  x7.type = FactoryInput::ConstraintInput::POINT;
  x7.esoIndex = 6;
  
  x7.timePoint = 0.0625;
  x7.lagrangeMultiplier = -6.5060155941749657e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.125;
  x7.lagrangeMultiplier = -2.4552534921824986e-008;
  constraints.push_back(x7);
  x7.timePoint = 0.1875;
  x7.lagrangeMultiplier = -2.7786050606296482;
  constraints.push_back(x7);
  x7.timePoint = 0.25;
  x7.lagrangeMultiplier = -1.4386394231553414e-007;
  constraints.push_back(x7);
  x7.timePoint = 0.3125;
  x7.lagrangeMultiplier = -1.9286768248755390;
  constraints.push_back(x7);
  x7.timePoint = 0.375;
  x7.lagrangeMultiplier = -4.4062992173670233e-007;
  constraints.push_back(x7);
  x7.timePoint = 0.4375;
  x7.lagrangeMultiplier = -1.0472776843845250e-008;
  constraints.push_back(x7);
  x7.timePoint = 0.5;
  x7.lagrangeMultiplier = -5.0839946692117579e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.5625;
  x7.lagrangeMultiplier = -2.4006236928790601e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.625;
  x7.lagrangeMultiplier = -1.4832439448170721e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.6875;
  x7.lagrangeMultiplier = -1.0600934778235825e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.75;
  x7.lagrangeMultiplier = -8.0170975443700998e-010;
  constraints.push_back(x7);
  x7.timePoint = 0.8125;
  x7.lagrangeMultiplier = -6.0749242012122018e-010;
  constraints.push_back(x7);
  x7.timePoint = 0.875;
  x7.lagrangeMultiplier = -4.7010833657460413e-010;
  constraints.push_back(x7);
  x7.timePoint = 0.9375;
  x7.lagrangeMultiplier = -2.4095246746257795e-010;
  constraints.push_back(x7);
  x7.timePoint = 1.0;
  x7.lagrangeMultiplier = -4.8399045838140232e-011;
  constraints.push_back(x7);
  
  FactoryInput::ConstraintInput x8;
  x8.type = FactoryInput::ConstraintInput::ENDPOINT;
  x8.lagrangeMultiplier = 1295.7677704702055;
  x8.timePoint = 1.0;
  x8.esoIndex = 7;
  constraints.push_back(x8);

  
  FactoryInput::ConstraintInput objective;
  objective.lagrangeMultiplier = 1.0;
  objective.timePoint = 1.0;
  objective.type = FactoryInput::ConstraintInput::ENDPOINT;
  objective.esoIndex = 8;
  
  FactoryInput::OptimizerMetaDataInput optimizerMetaDataInput;
  optimizerMetaDataInput.constraints = constraints;
  optimizerMetaDataInput.hasTotalEndTime = false;
  optimizerMetaDataInput.objective = objective;
  
  FactoryInput::OptimizerInput input;
  input.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
  input.integratorInput = integratorInput;
  input.metaDataInput = optimizerMetaDataInput;

  //then run simulation
  OptimizerFactory optFac;
  GenericOptimizer::Ptr  opt(optFac.createGenericOptimizer(input));
  DyosOutput::Output out;
  out.solutionTime = 0.0;
  out.optimizerOutput = opt->solve();
  out.stageOutput = opt->getOutput();
  
  //compare output
  std::vector<double> comparedAdjoints;
  std::ifstream adjointsFileReader("adjointTestResults.txt");
  while(!adjointsFileReader.eof()){
    double d;
    adjointsFileReader >> d;
    if(fabs(d) < THRESHOLD){
      d=0.0;
    }
    comparedAdjoints.push_back(d);
  }
  adjointsFileReader.close();
  
  unsigned num1stOrderAdjoints = out.stageOutput.front().integrator.adjoints.size();
  unsigned num2ndOrderAdjoints = out.stageOutput.front().integrator.adjoints2ndOrder.values.size();
  BOOST_REQUIRE(comparedAdjoints.size()<= num1stOrderAdjoints + num2ndOrderAdjoints);
  // the reference solution is the multigrid solution on windows. On LINUX the solution differs a bit,
  // so we relax the threshold
  // Also, differences between 32 bit and 64 bit VS2017 were observed. Therefore, threshold is relaxed further.
  double threshold = 9e-10;
  //compare 1st order adjoints
  for(unsigned i=0; i<num1stOrderAdjoints; i++){
    if(comparedAdjoints[i] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[i],
                        out.stageOutput.front().integrator.adjoints[i].values.front(),
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput.front().integrator.adjoints[i].values.front(),
                        THRESHOLD);
    }
  }
  //compare 2nd order adjoints
  for(unsigned i=0; i<num2ndOrderAdjoints; i++){
    if(comparedAdjoints[i + num1stOrderAdjoints] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[i + num1stOrderAdjoints],
                        out.stageOutput.front().integrator.adjoints2ndOrder.values[i],
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput.front().integrator.adjoints2ndOrder.values[i],
                        THRESHOLD);
    }
  }


}

BOOST_AUTO_TEST_CASE(TestAdjointsCalculationMultiStage)
{
  //first construct an optimization problem
  
  FactoryInput::EsoInput eso;
  
  eso.type = EsoType::JADE;
  eso.model = "wobatchinactive";
  
  unsigned numIntervals1 = 4;
  double durationValue1 = 500.0;
  unsigned numStages = 2;
  std::vector<double> timePoints1(numIntervals1 +1);
  timePoints1.front() = 0.0;
  timePoints1.back() = 1.0;
  for(unsigned i=1; i<numIntervals1; i++){
    timePoints1[i] = i*(1.0/numIntervals1);
  }
  
  std::vector<double> userGrid(2*numIntervals1 +2);
  userGrid.front() = 0.0;
  userGrid.back() = 1.0;
  for(unsigned i=1; i<2*numIntervals1; i++){
    userGrid[i] = i*(0.5/numIntervals1);
  }
  
  FactoryInput::ParameterInput fbinCur;
  fbinCur.lowerBound = 0.0;
  fbinCur.upperBound = 5.784;
  fbinCur.sensType = FactoryInput::ParameterInput::FULL;
  fbinCur.type = FactoryInput::ParameterInput::CONTROL;
  fbinCur.esoIndex = 13;
  
  FactoryInput::ParameterizationGridInput fbinCurGrid1;
  fbinCurGrid1.timePoints = timePoints1;
  fbinCurGrid1.duration = durationValue1;
  fbinCurGrid1.approxType = PieceWiseConstant;
  fbinCurGrid1.hasFreeDuration = false;
  
  for(unsigned i=0; i<4; i++){
    fbinCurGrid1.values.push_back(5.7839999999999998);
  }
  fbinCur.grids.push_back(fbinCurGrid1);
  
  FactoryInput::ParameterInput twCur;
  
  twCur.lowerBound = 0.020;
  twCur.upperBound = 0.1;
  twCur.sensType = FactoryInput::ParameterInput::FULL;
  twCur.type = FactoryInput::ParameterInput::CONTROL;
  twCur.esoIndex = 12;
  
  FactoryInput::ParameterizationGridInput twCurGrid;
  twCurGrid.values.push_back(0.082232302959091785);
  twCurGrid.values.push_back(0.064746706219901390);
  twCurGrid.values.push_back(0.035888125780055936);
  twCurGrid.values.push_back(0.10000000000000001);
  assert(twCurGrid.values.size() == numIntervals1);
  
  twCurGrid.timePoints = timePoints1;
  twCurGrid.duration = durationValue1;
  
  twCur.grids.push_back(twCurGrid);
  
  std::vector<FactoryInput::ParameterInput> controls;
  controls.push_back(fbinCur);
  controls.push_back(twCur);
  
  FactoryInput::ParameterInput duration;
  duration.type = FactoryInput::ParameterInput::DURATION;
  duration.lowerBound = durationValue1;
  duration.upperBound = durationValue1;
  duration.sensType = FactoryInput::ParameterInput::NO;
  duration.value = durationValue1;
  
  FactoryInput::IntegratorMetaDataInput integratorMetaDataInput;
  GenericEsoFactory genEsoFac;
  
  integratorMetaDataInput.controls = controls;
  integratorMetaDataInput.duration = duration;
  integratorMetaDataInput.esoPtr = genEsoFac.create(eso);
  integratorMetaDataInput.userGrid = userGrid;
  
  
  FactoryInput::IntegratorInput integratorInput;
  integratorInput.type = FactoryInput::IntegratorInput::NIXE;
  integratorInput.order = FactoryInput::IntegratorInput::SECOND_REVERSE;
  integratorInput.metaDataInput.stages.push_back(integratorMetaDataInput);
  
  fbinCurGrid1.values.clear();
  fbinCurGrid1.values.push_back(0.86400019994537391);
  fbinCurGrid1.values.push_back(0.0);
  fbinCurGrid1.values.push_back(0.0);
  fbinCurGrid1.values.push_back(0.0);
  integratorMetaDataInput.controls[0].grids[0] = fbinCurGrid1;
  twCurGrid.values.clear();
  twCurGrid.values.push_back(0.053529027313956146);
  twCurGrid.values.push_back(0.020000000000000000);
  twCurGrid.values.push_back(0.029524403098351224);
  twCurGrid.values.push_back(0.10000000000000001);
  integratorMetaDataInput.controls[1].grids[0] = twCurGrid;
  
  integratorInput.metaDataInput.stages.push_back(integratorMetaDataInput);
  
  FactoryInput::MappingInput mapping;
  for(unsigned i=0; i<12; i++){
    mapping.eqnIndexMap[i] = i;
  }
  for(unsigned i=0; i<16; i++){
    mapping.varIndexMap[i] = i;
  }

  integratorInput.metaDataInput.mappings.push_back(mapping);
  
  
  std::vector<FactoryInput::ConstraintInput> constraints;
  FactoryInput::ConstraintInput x7;
  x7.type = FactoryInput::ConstraintInput::POINT;
  x7.esoIndex = 6;
  
  x7.timePoint = 0.125;
  x7.lagrangeMultiplier = -6.5060155941749657e-009;
  constraints.push_back(x7);
  x7.timePoint = 0.25;
  x7.lagrangeMultiplier = -2.4552534921824986e-008;
  constraints.push_back(x7);
  x7.timePoint = 0.375;
  x7.lagrangeMultiplier = -2.7786050606296482;
  constraints.push_back(x7);
  x7.timePoint = 0.5;
  x7.lagrangeMultiplier = -1.4386394231553414e-007;
  constraints.push_back(x7);
  x7.timePoint = 0.625;
  x7.lagrangeMultiplier = -1.9286768248755390;
  constraints.push_back(x7);
  x7.timePoint = 0.75;
  x7.lagrangeMultiplier = -4.4062992173670233e-007;
  constraints.push_back(x7);
  x7.timePoint = 0.875;
  x7.lagrangeMultiplier = -1.0472776843845250e-008;
  constraints.push_back(x7);
  x7.timePoint = 1.0;
  x7.lagrangeMultiplier = -5.0839946692117579e-009;
  constraints.push_back(x7);
  

  
  FactoryInput::ConstraintInput objective;
  objective.lagrangeMultiplier = 0.0;
  objective.timePoint = 1.0;
  objective.type = FactoryInput::ConstraintInput::ENDPOINT;
  objective.esoIndex = 8;
  
  FactoryInput::OptimizerMetaDataInput optimizerMetaDataInput;
  optimizerMetaDataInput.constraints = constraints;
  optimizerMetaDataInput.hasTotalEndTime = false;
  optimizerMetaDataInput.objective = objective;
  
  
  
  FactoryInput::OptimizerInput input;
  input.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
  input.integratorInput = integratorInput;
  input.metaDataInput.stages.push_back(optimizerMetaDataInput);
  
  optimizerMetaDataInput.objective.lagrangeMultiplier = 1.0;
  
  optimizerMetaDataInput.constraints[0].lagrangeMultiplier = -2.4006236928790601e-009;
  optimizerMetaDataInput.constraints[1].lagrangeMultiplier = -1.4832439448170721e-009;
  optimizerMetaDataInput.constraints[2].lagrangeMultiplier = -1.0600934778235825e-009;
  optimizerMetaDataInput.constraints[3].lagrangeMultiplier = -8.0170975443700998e-010;
  optimizerMetaDataInput.constraints[4].lagrangeMultiplier = -6.0749242012122018e-010;
  optimizerMetaDataInput.constraints[5].lagrangeMultiplier = -4.7010833657460413e-010;
  optimizerMetaDataInput.constraints[6].lagrangeMultiplier = -2.4095246746257795e-010;
  optimizerMetaDataInput.constraints[7].lagrangeMultiplier = -4.8399045838140232e-011;
  
  FactoryInput::ConstraintInput x8;
  x8.type = FactoryInput::ConstraintInput::ENDPOINT;
  x8.lagrangeMultiplier = 1295.7677704702055;
  x8.timePoint = 1.0;
  x8.esoIndex = 7;
  optimizerMetaDataInput.constraints.push_back(x8);

  input.metaDataInput.stages.push_back(optimizerMetaDataInput);
  input.metaDataInput.treatObjective.resize(numStages, true);
  input.metaDataInput.treatObjective[0] = false;
  input.metaDataInput.hasTotalEndTime = false;
  input.metaDataInput.totalEndTimeLowerBound = numStages*durationValue1;
  input.metaDataInput.totalEndTimeUpperBound = numStages*durationValue1;
  //then run simulation
  OptimizerFactory optFac;
  GenericOptimizer::Ptr  opt(optFac.createGenericOptimizer(input));
  DyosOutput::Output out;
  out.solutionTime = 0.0;
  out.optimizerOutput = opt->solve();
  out.stageOutput = opt->getOutput();
  

  
  //compare output
  //multistage has a different order of the parameters - so the 2nd order adjoints also have a different order
  //in multigrid the order is parameters of control 1, parameters of control 2
  std::vector<int> parameterIndices, comparedAdjointsIndices;
  std::map<int, int> mapParamsCols1, mapParamsCols2;
  DyosOutput::ParameterGridOutput paramGridOut1, paramGridOut2;
  paramGridOut1 = out.stageOutput.back().integrator.parameters[0].grids.front();
  paramGridOut2 = out.stageOutput.back().integrator.parameters[1].grids.front();
  mapParamsCols1 = paramGridOut1.firstSensitivityOutputVector.back().mapParamsCols;
  mapParamsCols2 = paramGridOut2.firstSensitivityOutputVector.back().mapParamsCols;
  for(unsigned i=0; i<mapParamsCols1.size(); i++){
    parameterIndices.push_back(mapParamsCols1[i]);
  }
  for(unsigned i=0; i<mapParamsCols2.size(); i++){
    parameterIndices.push_back(mapParamsCols2[i]);
  }
  
  int numStates = out.stageOutput.front().integrator.states.size();
  for(int i=0; i<numStates; i++){
    comparedAdjointsIndices.push_back(i);
  }
  for(unsigned i=0; i<parameterIndices.size(); i++){
    for(int j=0; j<numStates; j++){
      comparedAdjointsIndices.push_back((parameterIndices[i]+1)*numStates + j);
    }
  }
  
  std::ifstream adjointsFileReader("adjointTestResults.txt");
  std::vector<double> comparedAdjoints;
  while(!adjointsFileReader.eof()){
    double d;
    adjointsFileReader >> d;
    if(fabs(d) < THRESHOLD){
      d=0.0;
    }
    comparedAdjoints.push_back(d);
  }
  adjointsFileReader.close();
  int numAdjoints1stOrder = out.stageOutput.front().integrator.adjoints.size();
  int numAdjoints2ndOrder1stStage = out.stageOutput[0].integrator.adjoints2ndOrder.values.size();
  int numAdjoints2ndOrder2ndStage = out.stageOutput[1].integrator.adjoints2ndOrder.values.size();
  BOOST_REQUIRE_EQUAL(out.stageOutput.size(), 2);
  BOOST_REQUIRE_EQUAL(comparedAdjoints.size(), numAdjoints1stOrder + numAdjoints2ndOrder1stStage 
                                                                   + numAdjoints2ndOrder2ndStage);
  double threshold = 5e-10;
  for(int i=0; i<numAdjoints1stOrder; i++){
    
    if(comparedAdjoints[i] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[i],
                        out.stageOutput[0].integrator.adjoints[i].values.front(),
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput[0].integrator.adjoints[i].values.front(),
                        1e-10);
    }
  }
   for(int i=0; i< numAdjoints2ndOrder1stStage; i++){
    unsigned comparedIndex = numAdjoints1stOrder + i;
    if(comparedAdjoints[comparedAdjointsIndices[comparedIndex]] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[comparedAdjointsIndices[comparedIndex]],
                        out.stageOutput[0].integrator.adjoints2ndOrder.values[i],
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput[0].integrator.adjoints2ndOrder.values[i],
                        1e-10);
    }
  }
  for(int i=0; i< numAdjoints2ndOrder2ndStage; i++){
    unsigned comparedIndex = numAdjoints1stOrder + numAdjoints2ndOrder1stStage + i;
    if(comparedAdjoints[comparedAdjointsIndices[comparedIndex]] != 0.0){
      BOOST_CHECK_CLOSE(comparedAdjoints[comparedAdjointsIndices[comparedIndex]],
                        out.stageOutput[1].integrator.adjoints2ndOrder.values[i],
                        threshold);
    }
    else{
      BOOST_CHECK_SMALL(out.stageOutput[1].integrator.adjoints2ndOrder.values[i],
                        1e-10);
    }
  }

}

BOOST_AUTO_TEST_SUITE_END()
