/**
* @file GenericOptimizerTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizerTest - Part of DyOS                                  \n
* =====================================================================\n
* Test of class GenericOptimizer                                       \n
* =====================================================================\n
* @author Mathias Dunst
* @date 15.11.2012
*/


#include "GenericOptimizerTestDummies.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "FirstOrderOptimizationProblem.hpp"
#include "NPSOLLoader.hpp"

BOOST_AUTO_TEST_SUITE(TestGenericOptimizerWithNPSOL)

BOOST_AUTO_TEST_CASE(TestNpsolLoader)
{
  BOOST_CHECK_NO_THROW(NPSOLLoader npsol);
}

/**
* @brief GenericOptimizerTest - test hardcoded Rosenbrock problem
*/
BOOST_AUTO_TEST_CASE(RosenbrockTest)
{
  GenericIntegrator::Ptr dummyIntegrator (new EmptyGenericIntegratorDummy());
  OptimizerMetaData::Ptr rosenbrockOmd (new RosenbrockOptimizerMetaData());
  OptimizationProblem::Ptr optProblem(new FirstOrderOptimizationProblem(dummyIntegrator, rosenbrockOmd));
  NPSOLWrapper npsol(optProblem);
  DyosOutput::OptimizerOutput optOut = npsol.solve();

  const double functionPrecision = 1e-8;
  BOOST_CHECK_CLOSE(optOut.lagrangeMultiplier[0], 0.0, THRESHOLD);
  BOOST_CHECK_CLOSE(optOut.optDecVarValues[0], 1.0, functionPrecision);
  BOOST_CHECK_CLOSE(optOut.optDecVarValues[1], 1.0, functionPrecision);
}

BOOST_AUTO_TEST_SUITE_END()
