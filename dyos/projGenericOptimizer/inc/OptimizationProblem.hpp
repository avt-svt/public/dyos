
#pragma once

#include "OptimizerMetaData.hpp"
#include "DyosObject.hpp"

#include "cs.h"

/**
 * This interface represents an abstract optimization problem.
 * This should be accepted by a GenericOptimizer and solved.
 *
 * It is very closely related to the OptimizerMetaData.
 * The difference is, that the OptimizationProblem takes care of
 * integrating the system only when necessary (caching).
 */
class OptimizationProblem : public DyosObject
{
public:
  enum ResultFlag{
    FAILED,
    SUCCESS
  };

  enum OptimizationOrder {
    FIRST_ORDER,
    SECOND_ORDER
  };

  typedef std::shared_ptr<OptimizationProblem> Ptr;

  virtual OptimizationOrder getOptimizationOrder() const = 0;
  virtual const OptimizerProblemDimensions &getOptProbDim() const = 0;
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const = 0;
  virtual void getDecVarValues(utils::Array<double> &values) const = 0;
  virtual void setDecVarValues(const utils::Array<const double> &decVars) = 0;
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const = 0;
  virtual ResultFlag getNonLinConstraintValues
                                          (utils::Array<double> &values) = 0;
  virtual ResultFlag getNonLinConstraintDerivativesDecVar
                                          (CsTripletMatrix::Ptr derivativeMatrix) = 0;
  virtual ResultFlag getNonLinConstraintDerivativesConstParam
                                          (CsTripletMatrix::Ptr derivativeMatrix) = 0;
  virtual ResultFlag getNonLinObjValue(double &objValue) = 0;
  virtual ResultFlag getNonLinObjDerivative(utils::Array<double> &values) = 0;
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers) = 0;
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const = 0;
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier) = 0;
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier) = 0;
  virtual ResultFlag getLinConstraintValues(utils::Array<double> &values) = 0;
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const = 0;
  virtual std::vector<DyosOutput::StageOutput> getOutput() = 0;
  virtual ResultFlag getNonLinConstraintHessianDecVar
                   (std::vector<std::vector<double> > &hessianMatrix) = 0;
  virtual ResultFlag getNonLinConstraintHessianDecVarDotD(const utils::Array<double> &d,
                                                          utils::Array<double> &result)
  {
    assert(d.getSize() == result.getSize());
    std::vector<std::vector<double> > hessianMatrix;
    ResultFlag res = getNonLinConstraintHessianDecVar(hessianMatrix);
    if(res == SUCCESS){
      assert(d.getSize() == hessianMatrix.size());
      for(unsigned i=0; i<hessianMatrix.size(); i++){
        assert(hessianMatrix[i].size() == d.getSize());
        result[i]=0.0;
        for(unsigned j=0; j<hessianMatrix[i].size(); j++){
          result[i] += hessianMatrix[i][j]*d[j];
        }
      }
    }
    return res;
  }
  
  virtual ResultFlag getNonLinConstraintHessianConstParam
                  (std::vector<std::vector<double> > &hessianMatrix) = 0;
  virtual ResultFlag getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                       std::vector<std::vector<double> > &adjoints2ndOrder) = 0;
  virtual ResultFlag getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                           std::vector<std::vector<double> > &adjoints2ndOrder) = 0;
  virtual ResultFlag doFinalIntegration() = 0;

  int getNumDecVars() const {return getOptProbDim().numOptVars;}
};
