/** @file NPSOLOptions.hpp
*    @brief header declaring NPSOLOptions class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails NPSOLOptions
*    =====================================================================\n
*   @author Ann-Kathrin Dombrowski
*   @date 12.6.2013
*/

#pragma once
#include "OptimizerOptions.hpp"

class NPSOLOptions : public OptimizerOptions
{
  public:
    NPSOLOptions()
    {
      //key needs to be written in lower case`
      validOptions["warm start"].push_back(EMPTY);
      validOptions["cold start"].push_back(EMPTY);
      validOptions["verify constraint gradients"].push_back(EMPTY);
      validOptions["Start objective check at variable"].push_back(POS_INTEGER);
      validOptions["hessian"].push_back(NO_NUMBER);
      validOptions["iteration limit"].push_back(POS_INTEGER);
      validOptions["central difference interval"].push_back(DBL);
      validOptions["crash tolerance"].push_back(DBL);   //used in conjunction with the optional parameter Cold start
      validOptions["derivative level"].push_back(INTEGER);    //indicates which derivatives are provided by the user in subroutines funobj and funcon.
      validOptions["derivative linesearch"].push_back(INTEGER);
      validOptions["difference interval"].push_back(DBL);
      validOptions["feasibility tolerance"].push_back(DBL);   //defines the maximum acceptable absolute violations in linear and nonlinear constraints at a \feasible" point
      validOptions["linear feasibility tolerance"].push_back(DBL);
      validOptions["nonlinear feasibility tolerance"].push_back(DBL);
      validOptions["function precision"].push_back(DBL);
      validOptions["infinite bound size"].push_back(DBL);
      validOptions["infinite step size"].push_back(DBL);
      validOptions["line search tolerance"].push_back(DBL);   //The value r (0 <= r < 1) controls the accuracy with which the step alfa taken during each iteration approximates a minimum of the merit function along the search direction
      validOptions["major iterations limit"].push_back(POS_INTEGER);   //max(50, 3(n + mL) + 10mN)
      validOptions["minor iterations limit"].push_back(POS_INTEGER);   //max(50, 3(n + mL + mN)
      validOptions["print level"].push_back(POS_INTEGER);
      validOptions["major print level"].push_back(POS_INTEGER);   //controls the amount of printout produced by the major iterations of NPSOL
      validOptions["minor print level"].push_back(POS_INTEGER);   //controls the amount of printout produced by the minor iterations of NPSOL
      validOptions["optimality tolerance"].push_back(DBL);    //specifies the accuracy to which the user wishes the final iterate to approximate a solution of the problem.
      validOptions["print file"].push_back(INTEGER);
      validOptions["step limit"].push_back(DBL);    //If r > 0, r specifies the maximum change in variables at the first step of the line search.
      validOptions["superbasics limit"].push_back(INTEGER);   // min (500, n1 + 1)
      validOptions["summary file"].push_back(INTEGER);    //If i > 0, a brief log will be output to file i.
      validOptions["print frequency"].push_back(INTEGER);
      validOptions["summary frequency"].push_back(INTEGER);
      validOptions["verify level"].push_back(INTEGER);    //cheap check on gradients
      validOptions["start objective check at col 1"].push_back(INTEGER);
    }
};
