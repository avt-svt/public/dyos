/** @file SNOPTLoader.hpp
*    @brief header declaring SNOPTLoader class
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails SNOPTLoader
*    =====================================================================\n
*   @author Tjalf Hoffmann, Klaus Stockmann
*   @date 21.6.2010
*/
#pragma once

#include "Loader.hpp"
#include <vector>
#include "DyosObject.hpp"

/** typedef of function pointer for call of SNOPT - only for better readability of the snopt_func typedef
  * This is a C function.
  */
typedef void (*confun_ptr)(int *, // mode,
                           int *, // nncon,
                           int *, // nnjac,
                           int *, // nejac,
                           double *, // x,
                           double *, // fcon,
                           double *, // gcon,
                           int *, // nstate,
                           char *, // cu,
                           int *, // lencu, // character workspace; not used by DyOS -> into wrapper
                           int *, // iu,
                           int *, // leniu,              // integer workspace; not used by DyOS   -> into wrapper
                           double *, // ru,
                           int *, // lenru         // real workspace; not used by DyOS      -> into wrapper
                           int ); // flencu

/** typedef of function pointer for call of SNOPT - only for better readability of the snopt_func typedef
  * This is a C function.
  */
typedef void (*objfun_ptr)(int *, // mode, // input, output
                           int *, // nnobj,
                           double *, // x,
                           double *, // fobj, // output
                           double *, // gobj, // output
                           int *, // nstate,
                           char *, // cu,
                           int *, // lencu, // character workspace; not used by DyOS -> into wrapper
                           int *, // iu,
                           int *, // leniu,              // integer workspace; not used by DyOS   -> into wrapper
                           double *, // ru,
                           int *,// lenru);          // real workspace; not used by DyOS      -> into wrapper
                           int ); // flencu 

/**
* typedef of FORTRAN routine from SNOPT DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
typedef void ( *snopt_func) (char[], int*, int*, int*, int*, int*,
                                     int*, int*, int*, double*, char[],
                                     confun_ptr , objfun_ptr,
                                     double*, int*, int*,
                                     double*, double*, char[], int*,
                                     double*, double*, double*, int*, int*,
                                     int*, int*, int*, int*, double*, double*, char*,
                                     int*, int*, int*, double*, int*, char*, int*,
                                     int*, int*, double*, int*, size_t, size_t, size_t, size_t, size_t);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void ( *sninit_func) (int *, // iPrint
                                      int *, // iSumm
                                      char *, // cw
                                      int *, // lencw
                                      int *, // iw
                                      int *, // leniw
                                      double *, // rw
                                      int *, //lenrw
									  size_t); // lencwd

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void ( *sngetr_func) (char *,
                                      double *,
                                      int *,
                                      char *,
                                      int *,
                                      int *,
                                      int *,
                                      double *, int *,
                                      int, int);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void ( *snsetr_func) (char *,
                                      double *,
                                      int *,
                                      int *,
                                      int *,
                                      char *,
                                      int *,
                                      int *,
                                      int *,
                                      double *,
                                      int *, int, int);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void ( *snseti_func) (char *,
                                      int *,
                                      int *,
                                      int *,
                                      int *,
                                      char *,
                                      int *,
                                      int *,
                                      int *,
                                      double *,
                                      int*, int, int);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void ( *snset_func) (char *,
                                     int *,
                                     int *,
                                     int *,
                                     char *,
                                     int *,
                                     int *,
                                     int *,
                                     double *,
                                     int *, int ,int);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void ( *snspec_func) (int *,
                                      int *,
                                      char *,
                                      int *,
                                      int *,
                                      int *,
                                      double *,
                                      int*, size_t);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void  ( *sn_openfile_func) (int *,
                                          const  char[],
                                            int);

/** typedef of FORTRAN routine from SNOPT DLL for dynamic loading
  * This is a FORTRAN function called from C.
  */
typedef void  ( *sn_closefile_func) (int *);

/** @brief load SNOPT DLL */
class SNOPTLoader:public utils::Loader, public::DyosObject
{
protected:
  /** @brief length of each string in string array workspace which is passed to SNOPT*/
  size_t m_snoptStringLength;
  /** @brief  handle to SNOPT main routine */
  snopt_func snopt_func_handle;
  /** @brief  handle to SNOPT initialization routine */
  sninit_func sninit_func_handle;
  /** @brief  handle to function which specifies options file */
  snspec_func snspec_func_handle;
  /** @brief  handle to function which gets value of an option of type 'real' */
  sngetr_func sngetr_func_handle;
  /** @brief  handle to function which sets value of an option of type 'real' */
  snsetr_func snsetr_func_handle;
  /** @remarks no wrapper for sngeti implemented; function not required in DyOS*/
  /** @brief  handle to function which sets value of an option of type 'integer' */
  snseti_func snseti_func_handle;
  /** @brief  handle to function which sets single option for SNOPT using a string (corresponds to single line in SNOPT options file */
  snset_func snset_func_handle;

  /** @brief  handle to function which opens file for SNOPT from within C code*/
  sn_openfile_func sn_openfile_func_handle;
  /** @brief  handle to function which closes file for SNOPT from within C code*/
  sn_closefile_func sn_closefile_func_handle;
  void loadFunctions();
  std::string getSNOPTMsg(const int infoFlag);
public:
  SNOPTLoader();
  SNOPTLoader(const SNOPTLoader &snoptLoader);
  ~SNOPTLoader(){unloadDLL();};
  void snopt(int m, int n, int ne, int nnCon,
             char prob[], confun_ptr confun, objfun_ptr objfun,
             double *a, int *ha, int *ka,
             double *bl, double *bu,
             int *hs, double *xs, double *pi, double *rc, int &inform,
             int &nInf, double &sInf, double &obj,
             std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw);
  void snInit(int iPrint, int iSumm, std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw);
  void snSpec(int &optInform, const int iSpecs,
              std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw);

  void snOpenFile(const int fileHandle, const std::string fileName);
  void snCloseFile(const int fileHandle);

};
