/**
* @file OptimizerInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericOptimizer                                                     \n
* =====================================================================\n
* This file contains the declarations of the input structs for creating\n
* GenericOptimizer objects.                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.02.2012
*/

#pragma once

#include "MetaDataInput.hpp"
#include "IntegratorInput.hpp"

namespace FactoryInput
{
  struct AdaptationOptions
  {
    enum AdaptiveStrategy{
      NOADAPTATION,
      ADAPTATION,
      STRUCTURE_DETECTION,
      ADAPT_STRUCTURE
    };
    /// adaptationThreshold threshold that checks the change in the objective as a stopping criterion
    double adaptationThreshold; 
    /// intermConstraintViolationTolerance adaptation stops when the intermediate constraint 
    /// violation is below this tolerance
    double intermConstraintViolationTolerance;
    /// numOfIntermPoints are the number of intermediate points between the path constraint 
    /// discretization which are required to determine the intermeidate constraitn violation
    unsigned numOfIntermPoints;
    AdaptiveStrategy adaptStrategy;
    AdaptationOptions():adaptationThreshold(1e-2),
                        intermConstraintViolationTolerance(0.1),
                        numOfIntermPoints(4),
                        adaptStrategy(NOADAPTATION){}
  };
  /**
  * @struct OptimizerInput
  * @brief information provided by user to create a GenericOptimizer object
  */
  struct OptimizerInput{
    /// @brief type of the optimizer
    enum OptimizerType{
      SNOPT, ///> optimizer type for optimizer SNOPT
      IPOPT, ///> optimizer type for optimizer IPOPT
      NPSOL, ///> optimizer type for optimizer NPSOL
      FILTER_SQP, ///> optimizer type for optimizer filterSQP
      FINAL_INTEGRATION ///> type for simulation with 2nd order derivatives or plot grid
    };

    std::map<std::string, std::string> optimizerOptions;
    OptimizerType type; ///< type of the optimizer
    AdaptationOptions adaptationOptions;
    FactoryInput::IntegratorInput integratorInput; ///< information for creating a GenericIntegrator object
    OptimizerMetaDataInput metaDataInput; ///< information for creating an OptimizerMetaData object
    bool minimize;
    OptimizerInput():type(SNOPT),
                     minimize(true){}
  };
}// FactoryInput
