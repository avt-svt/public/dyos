/**
* @file DyosIpoptNLP.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* IpoptWrapper - Part of DyOS                                          \n
* =====================================================================\n
* Implements the TNLP interface of Ipopt for a dynamic optimization    \n
* problem given by DyOS                                                \n
* =====================================================================\n
* @author Hans Pirnay
* @date 2012-06-27
*/


#include "DyosIpoptNLP.hpp"




using namespace Ipopt;
bool DyosTNLP::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                            Index& nnz_h_lag, IndexStyleEnum& index_style)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  OptimizerProblemDimensions pdim = m_optProb->getOptProbDim();
  
  // get output arguments
  n = pdim.numOptVars;
  m = pdim.numLinConstraints + pdim.numNonLinConstraints;
  nnz_jac_g = pdim.linJac->get_nnz() + pdim.nonLinJacDecVar->get_nnz();

  //nnz_h_lag = 0;
  // hessian is dense, but we need only the subdiagonal and the diagonal entries (Hessian is symmetric)
  // Remark: I hope it is no problem to specify the size, if we do not use the Hessian
  nnz_h_lag = (n*n+n)/2; 
  index_style = C_STYLE;
  std::stringstream sstream;
  sstream << "Problem dimensions:\nn = " << n << "\nm = " << m << "\nnnz_jac_g = " << nnz_jac_g << std::endl;
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, sstream.str());
  // assign to attributes of this instance of DyOSTNLP class
  n_ = n;
  m_nonlin_ = pdim.numNonLinConstraints;
  m_lin_ = pdim.numLinConstraints;
  m_ = m;
  nnz_jac_g_ = nnz_jac_g;

  m_capture.beginCapture();
  return true;
}

bool DyosTNLP::get_var_con_metadata(Index n,
                                    StringMetaDataMapType& var_string_md,
                                    IntegerMetaDataMapType& var_integer_md,
                                    NumericMetaDataMapType& var_numeric_md,
                                    Index m,
                                    StringMetaDataMapType& con_string_md,
                                    IntegerMetaDataMapType& con_integer_md,
                                    NumericMetaDataMapType& con_numeric_md)
{
  return true;
}



/** overload this method to return the information about the bound
 *  on the variables and constraints. The value that indicates
 *  that a bound does not exist is specified in the parameters
 *  nlp_lower_bound_inf and nlp_upper_bound_inf.  By default,
 *  nlp_lower_bound_inf is -1e19 and nlp_upper_bound_inf is
 *  1e19. (see TNLPAdapter) */
bool DyosTNLP::get_bounds_info(Index n, Number* x_l, Number* x_u,
                               Index m, Number* g_l, Number* g_u)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  assert(n==n_);
  assert(m==m_);
  utils::WrappingArray<double> x_lower_bounds(n, x_l);
  utils::WrappingArray<double> x_upper_bounds(n, x_u);
  m_optProb->getDecVarBounds(x_lower_bounds, x_upper_bounds);

  utils::WrappingArray<double> g_lower_bounds_nonlin(m_nonlin_, g_l);
  utils::WrappingArray<double> g_upper_bounds_nonlin(m_nonlin_, g_u);
  m_optProb->getNonLinConstraintBounds(g_lower_bounds_nonlin, g_upper_bounds_nonlin);
  utils::WrappingArray<double> g_lower_bounds_lin(m_lin_, g_l+m_nonlin_);
  utils::WrappingArray<double> g_upper_bounds_lin(m_lin_, g_u+m_nonlin_);
  m_optProb->getLinConstraintBounds(g_lower_bounds_lin, g_upper_bounds_lin);
  m_capture.beginCapture();
  return true;
}

/** overload this method to return scaling parameters. This is
 *  only called if the options are set to retrieve user scaling.
 *  There, use_x_scaling (or use_g_scaling) should get set to true
 *  only if the variables (or constraints) are to be scaled.  This
 *  method should return true only if the scaling parameters could
 *  be provided.
 */
bool DyosTNLP::get_scaling_parameters(Number& obj_scaling,
                                      bool& use_x_scaling, Index n,
                                      Number* x_scaling,
                                      bool& use_g_scaling, Index m,
                                      Number* g_scaling)
{
  return false;
}

/** overload this method to return the variables linearity
 * (TNLP::Linear or TNLP::NonLinear). The var_types
 *  array should be allocated with length at least n. (default implementation
 *  just return false and does not fill the array).*/
bool DyosTNLP::get_variables_linearity(Index n, LinearityType* var_types)
{
  //assert(n==n_);

  return false;
}

/** overload this method to return the constraint linearity.
 * array should be alocated with length at least n. (default implementation
 *  just return false and does not fill the array).*/
bool DyosTNLP::get_constraints_linearity(Index m, LinearityType* const_types)
{
  return false;
}

/** overload this method to return the starting point. The bool
 *  variables indicate whether the algorithm wants you to
 *  initialize x, z_L/z_u, and lambda, respectively.  If, for some
 *  reason, the algorithm wants you to initialize these and you
 *  cannot, return false, which will cause Ipopt to stop.  You
 *  will have to run Ipopt with different options then.
 */
bool DyosTNLP::get_starting_point(Index n, bool init_x, Number* x,
                                  bool init_z, Number* z_L, Number* z_U,
                                  Index m, bool init_lambda,
                                  Number* lambda)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  assert(n==n_);
  if (init_x) {
    utils::WrappingArray<double> xvec(n_, x);
    m_optProb->getDecVarValues(xvec);
  }
  if (init_z || init_lambda)
    assert(false);
  m_capture.beginCapture();
  return true;
}

bool DyosTNLP::get_warm_start_iterate(IteratesVector& warm_start_iterate)
{
  return false;
}

bool DyosTNLP::eval_f(Index n, const Number* x, bool new_x,
                      Number& obj_value)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  assert(n==n_);
  apply_new_x(new_x, x);
  OptimizationProblem::ResultFlag resultflag = m_optProb->getNonLinObjValue(obj_value);
  m_capture.beginCapture();
  return resultflag==OptimizationProblem::SUCCESS;
}

bool DyosTNLP::eval_grad_f(Index n, const Number* x, bool new_x,
                           Number* grad_f)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  assert(n==n_);
  apply_new_x(new_x, x);
  utils::WrappingArray<double> gradvals(n, grad_f);
  OptimizationProblem::ResultFlag resultflag = m_optProb->getNonLinObjDerivative(gradvals);
  m_capture.beginCapture();
  return resultflag==OptimizationProblem::SUCCESS;
}

bool DyosTNLP::eval_g(Index n, const Number* x, bool new_x,
                      Index m, Number* g)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  assert(m == m_nonlin_+ m_lin_);
  apply_new_x(new_x, x);
  utils::WrappingArray<double> nonlin_con_values(m_nonlin_, g);
  utils::WrappingArray<double> lin_con_values(m_lin_, g+m_nonlin_);
  OptimizationProblem::ResultFlag resultflag1 = m_optProb->getNonLinConstraintValues(nonlin_con_values);
  OptimizationProblem::ResultFlag resultflag2 =  m_optProb->getLinConstraintValues(lin_con_values);
  m_capture.beginCapture();
  return (resultflag1==OptimizationProblem::SUCCESS) && (resultflag2==OptimizationProblem::SUCCESS);
}

/** Method to return:
   *   1) The structure of the jacobian (if "values" is NULL)
   *   2) The values of the jacobian (if "values" is not NULL)
   */
bool DyosTNLP::eval_jac_g(Index n, const Number* x, bool new_x,
                          Index m, Index nele_jac, Index* iRow,
                          Index *jCol, Number* values)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  bool retval = true;
  
  if (!values) { // return structure
    OptimizerProblemDimensions pdim = m_optProb->getOptProbDim();
    
    // get nonlinear Jacobian
    cs *nlmat = pdim.nonLinJacDecVar->get_matrix_ptr();
    // once we have methods to access the matrix structure, these asserts should no more be necessary
    assert(nlmat); 
    assert(CS_TRIPLET(nlmat));
    
    // get linear Jacobian
    cs *lmat = pdim.linJac->get_matrix_ptr();
    assert(lmat);
    assert(CS_TRIPLET(lmat));

    assert(nele_jac == (nlmat->nz + lmat->nz));
    
    // nonlinear jacobian entries
    std::copy(nlmat->i, nlmat->i + nlmat->nz, iRow);
    std::copy(nlmat->p, nlmat->p + nlmat->nz, jCol);
    
    // linear jacobian entries
    int offsetRow = nlmat->m;
    int offsetNNZ = nlmat->nz;
    
    for (int i=0; i<lmat->nz; i++) {
      iRow[i+offsetNNZ] = lmat->i[i] + offsetRow;
      jCol[i+offsetNNZ] = lmat->p[i]; 
    }

  }
  else {         // return values
    apply_new_x(new_x, x);
    OptimizerProblemDimensions pdim = m_optProb->getOptProbDim();
    
    OptimizationProblem::ResultFlag resultflag = m_optProb->getNonLinConstraintDerivativesDecVar(pdim.nonLinJacDecVar);
    retval = (resultflag == OptimizationProblem::SUCCESS);
    
    cs *nlmat = pdim.nonLinJacDecVar->get_matrix_ptr();
    // once we have methods to access the matrix structure, these asserts should no more be necessary
    assert(nlmat);
    assert(CS_TRIPLET(nlmat));
    
    cs *lmat = pdim.linJac->get_matrix_ptr();
    assert(lmat);
    assert(CS_TRIPLET(lmat));

    assert(nele_jac == (nlmat->nz + lmat->nz));
   
    // nonlinear jacobian entries
    std::copy(nlmat->x, nlmat->x + nlmat->nz, values);
    
    // linear jacobian entries
    int offsetNNZ = nlmat->nz;
    for (int i=0; i<lmat->nz; i++){
      values[i+offsetNNZ] = lmat->x[i];
    }

  }
  
  m_capture.beginCapture();
  return retval;
}

bool DyosTNLP::eval_h(Index n, const Number* x, bool new_x,
                      Number obj_factor, Index m, const Number* lambda,
                      bool new_lambda, Index nele_hess,
                      Index* iRow, Index* jCol, Number* values)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  // in first order optimization only a dummy implementations returning 'false' is required
  if (m_optProb->getOptimizationOrder() == OptimizationProblem::FIRST_ORDER) {
    m_capture.beginCapture();
    return false;
  }

  OptimizerProblemDimensions pdim = m_optProb->getOptProbDim();
    
  assert(unsigned(n) == pdim.numOptVars);
  assert(unsigned(m) == pdim.numLinConstraints + pdim.numNonLinConstraints);
  assert(nele_hess == (n*n+n)/2);

  if (values == NULL) {
    // return the structure. The hessian is a dense matrix. But, as the 
    // hessian is a symmetric matrix, fill the lower left triangle only.
    Index idx = 0;
    for (Index row = 0; row < n; row++) {
      for (Index col=0; col <= row; col++) {
        iRow[idx] = row;
        jCol[idx] = col;
        idx++;
      }
    }
    assert(idx == nele_hess);
  }
  else {
    apply_new_x(new_x, x);
    m_optProb->setObjectiveLagrangeMultiplier(obj_factor);

    // distribute the values of the Lagrange multiplier to DyOS' internal structures
    // Constraints are sorted as first nonlinear then linear
    // as are variables
    unsigned numLinear = pdim.numLinConstraints;
    unsigned numNonlinear = pdim.numNonLinConstraints;
    

    utils::Array<double> nonlinearLam(numNonlinear);
    utils::Array<double> linearLam(numLinear);

    for (unsigned i=0; i<numNonlinear; i++) nonlinearLam[i] = lambda[i];
    for (unsigned i=0; i<numLinear; i++) linearLam[i] = lambda[i+numNonlinear];
    
    m_optProb->setLagrangeMultipliers(nonlinearLam, linearLam);
    
    // get the Hessian
    std::vector<std::vector<double> > hessianMatrix;
    m_optProb->getNonLinConstraintHessianDecVar(hessianMatrix);
    assert(hessianMatrix.size() == unsigned(n));
    for (int i=0; i<n; i++) {
      assert(hessianMatrix[i].size() == unsigned(n));
    }

    // set the Hessian values to Ipopt 
    Index idx = 0;
    for (Index row = 0; row < n; row++) {
      for (Index col=0; col <= row; col++) {
        values[idx] = hessianMatrix[row][col];
        idx++;
      }
    }
    assert(idx == nele_hess);
  }

  m_capture.beginCapture();
  return true;
}
//@}

/** @name Solution Methods */
//@{
/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
void DyosTNLP::finalize_solution(SolverReturn status,
                                 Index n, const Number* x, const Number* z_L, const Number* z_U,
                                 Index m, const Number* g, const Number* lambda,
                                 Number obj_value,
                                 const IpoptData* ip_data,
                                 IpoptCalculatedQuantities* ip_cq)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY,
                         "*******************\nDone!\n*******************\n");
  utils::WrappingArray<const Number> xArray(n, x);
  m_optProb->setDecVarValues(xArray);
  
  unsigned numNonLinConst = m_optProb->getOptProbDim().numNonLinConstraints;
  unsigned numLinConst = m_optProb->getOptProbDim().numLinConstraints;
  const utils::Array<Number> nonLinearLagrangeMult(numNonLinConst, lambda);
  const utils::Array<Number> linearLagrangeMult(numLinConst, lambda + numNonLinConst);
  
  m_optProb->setObjectiveLagrangeMultiplier(1.0);
  m_optProb->setLagrangeMultipliers(nonLinearLagrangeMult, linearLagrangeMult);
  
  utils::Array<double> controlLagrangeMultipliers(n);
  for(Index i=0; i<n; i++){
    controlLagrangeMultipliers[i] = z_L[i] - z_U[i];
  }
  m_optProb->setDecVarLagrangeMultipliers(controlLagrangeMultipliers);
  
  
  m_capture.beginCapture();
}
/** This method is called just before finalize_solution.  With
 *  this method, the algorithm returns any metadata collected
 *  during its run, including the metadata provided by the user
 *  with the above get_var_con_metada.  Each metadata can be of
 *  type string, integer, and numeric. It can be associated to
 *  either the variables or the constraints.  The metadata that
 *  was associated with the primal variable vector is stored in
 *  var_..._md.  The metadata associated with the constraint
 *  multipliers is stored in con_..._md.  The metadata associated
 *  with the bound multipliers is stored in var_..._md, with the
 *  suffixes "_z_L", and "_z_U", denoting lower and upper
 *  bounds. */
void DyosTNLP::finalize_metadata(Index n,
                                 const StringMetaDataMapType& var_string_md,
                                 const IntegerMetaDataMapType& var_integer_md,
                                 const NumericMetaDataMapType& var_numeric_md,
                                 Index m,
                                 const StringMetaDataMapType& con_string_md,
                                 const IntegerMetaDataMapType& con_integer_md,
                                 const NumericMetaDataMapType& con_numeric_md)
{
  m_capture.endCapture();
  m_logger->sendMessage(Logger::OPTIMIZER, OutputChannel::STANDARD_VERBOSITY, m_capture.getCapture());
}


/** Intermediate Callback method for the user.  Providing dummy
 *  default implementation.  For details see IntermediateCallBack
 *  in IpNLP.hpp. */
bool DyosTNLP::intermediate_callback(AlgorithmMode mode,
                                     Index iter, Number obj_value,
                                     Number inf_pr, Number inf_du,
                                     Number mu, Number d_norm,
                                     Number regularization_size,
                                     Number alpha_du, Number alpha_pr,
                                     Index ls_trials,
                                     const IpoptData* ip_data,
                                     IpoptCalculatedQuantities* ip_cq)
{
  return true;
}
//@}

/** @name Methods for quasi-Newton approximation.  If the second
 *  derivatives are approximated by Ipopt, it is better to do this
 *  only in the space of nonlinear variables.  The following
 *  methods are call by Ipopt if the quasi-Newton approximation is
 *  selected.  If -1 is returned as number of nonlinear variables,
 *  Ipopt assumes that all variables are nonlinear.  Otherwise, it
 *  calls get_list_of_nonlinear_variables with an array into which
 *  the indices of the nonlinear variables should be written - the
 *  array has the lengths num_nonlin_vars, which is identical with
 *  the return value of get_number_of_nonlinear_variables().  It
 *  is assumed that the indices are counted starting with 1 in the
 *  FORTRAN_STYLE, and 0 for the C_STYLE. */
//@{
Index DyosTNLP::get_number_of_nonlinear_variables()
{
  return -1;
}

bool DyosTNLP::get_list_of_nonlinear_variables(Index num_nonlin_vars,
                                               Index* pos_nonlin_vars)
{
  return false;
}

void DyosTNLP::apply_new_x(bool new_x, const Number* x) {
  if (new_x) {
    utils::WrappingArray<const double> xvec(n_, x);
    m_optProb->setDecVarValues(xvec);
  }
}
