#include<MidoUserInput.hpp>
#include<cmath>
using namespace UserInput;

void MidoParameterInput::setInteger()
{
  parameterDiscreteType = INTEGER;
  auto fixBoundsAndValue = [name = name](double& lowerBound, double& upperBound, double& value)
  {

    if (upperBound != std::floor(upperBound))
    {
      std::cerr << "Upper bound of variable" << name
        << "is set to " << std::floor(upperBound) << " from " << upperBound << " because it is declared integer" << std::endl;
    }
    if (lowerBound != std::ceil(lowerBound))
    {
      std::cerr << "Lower bound of variable " << name
        << "is set to " << std::ceil(lowerBound) << "from " << lowerBound << " because it is declared integer" << std::endl;
    }
    upperBound = std::floor(upperBound);
    lowerBound = std::ceil(lowerBound);
    if (value > upperBound || value < lowerBound)
    {
      double newvalue = 0.5 * (upperBound + lowerBound);
      std::cerr << "Initial guess of variable " << name
        << "is set to " << newvalue << " from " << value << " because it is declared integer" << std::endl;
      value = newvalue;
    }
  };
  fixBoundsAndValue(this->lowerBound, this->upperBound, this->value);
  for (UserInput::ParameterGridInput& grid : this->grids)
  {
    if (grid.lowerBounds.size() == grid.upperBounds.size()) // no assert here on purpose, since this is and should be checked in the input Factories
    {
      if (grid.values.empty()) //this is important, because lower level mido structures do not check the special case that values might be empty 
        //because the user wanted to use the value field of the parameter instead of specifing individual bounds for the grid points.
      {
        grid.values.resize(grid.lowerBounds.size(), this->value);
      }
      for (size_t interval = 0; interval < grid.lowerBounds.size(); interval++)
      {
        fixBoundsAndValue(grid.lowerBounds[interval], grid.upperBounds[interval], grid.values[interval]);
      }
    }
  }





}


IntegratorStageInput MidoStageIntegratorInput::convertToIntegratorStageInput() const&
{
  IntegratorStageInput returned = (*this);//copy of base class
  returned.parameters.clear();
  std::transform(parameters.begin(), parameters.end(), std::back_inserter(returned.parameters),
    [](const MidoParameterInput& midoParameter) {return midoParameter.convertToParameterInput(); });
  return returned;

}
IntegratorStageInput&& MidoStageIntegratorInput::convertToIntegratorStageInput()&&//move
{
  IntegratorStageInput::parameters.clear();
  std::transform(std::make_move_iterator(parameters.begin()), std::make_move_iterator(parameters.end()), std::back_inserter(IntegratorStageInput::parameters),
    [](MidoParameterInput&& midoParameter) {return std::move(midoParameter).convertToParameterInput(); });
  return std::move(*this);
}
StageInput  MidoStageInput::convertToStageInput()const&
{
  StageInput returned = (*this);//copy base class
  returned.integrator = integrator.convertToIntegratorStageInput();
  return returned;
}
StageInput&& MidoStageInput::convertToStageInput()&&//move
{
  StageInput::integrator = std::move(integrator).convertToIntegratorStageInput();
  return std::move(*this);
}

Input MidoInput::convertToInput()const&
{
  Input returned = (*this);//copy base class
  assert(returned.stages.empty());
  //Convert  the mido stages to normal stages and add them to returned
  std::transform(stages.begin(), stages.end(), std::back_inserter(returned.stages),
    [](const MidoStageInput& midoStage) {return midoStage.convertToStageInput(); });
  return returned;
}
Input&& MidoInput::convertToInput()&&//Allow moving
{
  std::transform(std::make_move_iterator(stages.begin()), std::make_move_iterator(stages.end()), std::back_inserter(Input::stages),
    [](MidoStageInput&& midoStage) {return std::move(midoStage).convertToStageInput(); });
  return std::move(*this);
}
void MidoInput::enableTerminationConditionTimeLimit(double secondsToRun)
{
  secondsTimeLimit = secondsToRun;
}
void MidoInput::enableTerminationConditionIterationLimit(int maxIterations)
{
  iterationLimit = maxIterations;
}
void MidoInput::enableTerminationConditionFoundFeasibleSolution()
{
  MidoInput::stopAfterFindingFirstFeasibleSolution = true;
}
void MidoInput::enableTerminationConditionObjectiveFunctionLowerThan(double lowerBound)
{
  defaultLowerBound = lowerBound;
}

void MidoInput::enableAssumptionDyosFindsGlobalSolution()
{
  assumeConvex = true;
}

