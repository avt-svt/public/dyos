#include "mex.h"
#include "ConvertInput.hpp"
#include "ConvertOutput.hpp"

#include "MidoDyos.hpp"
#include "UserInput.hpp"
#include "UserOutput.hpp"

class MexOutputChannel : public OutputChannel
{
public:
  virtual void print(const std::string &message)
  {
    mexPrintf(message.c_str());
    mexEvalString("drawnow;");
  }
  virtual void flush()
  {
    mexEvalString("drawnow;");
  };
};

UserInput::MidoStages convertMidoStageInput(const mxArray *matlabInput, const int index)
{
  UserInput::MidoStages midoStage;
  if(matlabInput == NULL){
    mexPrintf("midoStage input struct is empty.\n");
    throw DyosMexException();
  }
  if(!mxIsStruct(matlabInput)){
    mexPrintf("midoStage input must be a struct.\n");
    throw DyosMexException();
  }
  try{
    checkField(matlabInput, "binaryParameters");
    mxArray *binParameters = mxGetField(matlabInput, index, "binaryParameters");
    
    unsigned numBinParameters = mxGetNumberOfElements(binParameters);
    midoStage.binaryParameters.resize(numBinParameters);
    for(unsigned i=0; i<numBinParameters; i++){
      //bin parameter usually needs name and value, so just convert these two
      midoStage.binaryParameters[i].name = convertString(binParameters, "name", i);
      midoStage.binaryParameters[i].value = convertScalar(binParameters, "value", i);
    }
  }
  catch(DyosMexException e){
    mexPrintf("error occurred on stage %d\n", (index+1));
    throw e;
  }
  return midoStage;
}

std::vector<UserInput::MidoStages> convertMidoStages(const mxArray *matlabInput)
{
  std::vector<UserInput::MidoStages> midoStages;
  checkField(matlabInput, "midoStages");
  mxArray *stages = mxGetField(matlabInput, 0, "midoStages");
  int numMidoStages = mxGetNumberOfElements(stages);
  midoStages.resize(numMidoStages);
  for(int i=0; i<numMidoStages; i++){
    midoStages[i] = convertMidoStageInput(stages, i);
  }
  
  return midoStages;
}


UserInput::MidoInput convertMidoInput(const mxArray *matlabInput)
{
  UserInput::MidoInput midoInput;
  UserInput::Input input= convertInput(matlabInput);
  //copy fields into midoInput;
  midoInput.integratorInput = input.integratorInput;
  midoInput.optimizerInput = input.optimizerInput;
  midoInput.runningMode = input.runningMode;
  midoInput.stages = input.stages;
  midoInput.totalEndTimeLowerBound = input.totalEndTimeLowerBound;
  midoInput.totalEndTimeUpperBound = input.totalEndTimeUpperBound;
  if(mxGetFieldNumber(matlabInput, "midoStages")>-1){
    
    midoInput.midoStages = convertMidoStages(matlabInput);
  }
  else{
    mexPrintf("No MidoStages set in input struct\n");
    throw DyosMexException();
  }
  
  return midoInput;
}


void mexFunction( int nlhs, mxArray *plhs[], 
                  int nrhs, const mxArray *prhs[] )
     
{
  if(nlhs != 1){
    mexErrMsgTxt("Wrong number of outputs. Exactly one output expected.");
  }
  if(nrhs != 1){
    mexErrMsgTxt("Wrong number of inputs. Exactly one input expected.");
  }
  plhs[0] = mxDuplicateArray(prhs[0]);
  UserInput::MidoInput dyosIn;
  try{
    dyosIn = convertMidoInput(prhs[0]);
  }
  catch(DyosMexException e){
    mexErrMsgTxt("An error ocurred during input conversion");
  }
  catch(...)
  {
    mexErrMsgTxt("Unknown error occurred during input conversion");
  }
  Logger::Ptr logger(new Logger());
  OutputChannel::Ptr mexOut(new MexOutputChannel());
  mexOut->setVerbosity(OutputChannel::STANDARD_VERBOSITY);
  //for easier debugging always write input struct to file
  OutputChannel::Ptr inputOut(new FileChannel("mexDyosInput.json"));
  inputOut->setVerbosity(OutputChannel::STANDARD_VERBOSITY);
  OutputChannel::Ptr outputOut(new FileChannel("mexDyosOutput.json"));
  outputOut->setVerbosity(OutputChannel::STANDARD_VERBOSITY);
  logger->setLoggingChannel(Logger::DYOS_OUT, mexOut);
  logger->setLoggingChannel(Logger::INTEGRATOR, mexOut);
  logger->setLoggingChannel(Logger::OPTIMIZER, mexOut);
  logger->setLoggingChannel(Logger::ESO, mexOut);
  logger->setLoggingChannel(Logger::INPUT_OUT_JSON, inputOut);
  logger->setLoggingChannel(Logger::FINAL_OUT_JSON, outputOut);
  mexPrintf("finished input conversion\n");
  UserOutput::Output dyosOut;
  if(mxGetFieldNumber(prhs[0], "optimizeForwardWith2ndOrderIntegration")>-1){
    mexErrMsgTxt("optimization with 2nd order output not possible with MIDO");
    
  }
  else{
    dyosOut= runMidoDyosWithLogger(dyosIn, logger);
  }
  mexPrintf("finished run mido dyos\n");
  convertOutput(plhs[0], dyosOut, 0);
  mexPrintf("finished output conversion\n");
}