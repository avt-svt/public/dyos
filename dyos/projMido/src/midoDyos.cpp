/**
* @file MidoDyos.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mido                                                                 \n
* =====================================================================\n
* This file contains the implementation of the mixed integer procedures. \n
* Currently, no lower bounding is implemented.
* =====================================================================\n
* @author  Aron Zingler 
* @date 14.09.2019
*/



#include "MidoDyos.hpp"
#include "ConvertToXmlJson.hpp"
#include "DyosWithLogger.hpp"
#include "InputOutputConversions.hpp"
#include "MidoDyosUtils.hpp"
#include "babBrancher.h"
#include "babUtils.h"
#include "baseHeuristic.hpp"
#include "divingHeuristic.hpp"
#include "feasibilityPump.hpp"
#include "mpecHeuristic.h"
#include <cassert>
#include <chrono>
#include <limits>
#include <map>
#include <set>
#include <stdexcept>
#include <tuple>

using ADdouble = UserInput::ADdouble;


//Short overview: Main entry point is the function runMidoDyosWithLogger. This performs some checking and initialization, with the most notably in initializeOptimizationVariablesAndIndex where an index/mapping between the Dyos format of [stages, parameters, (intervals)] and the babBase format of a single vector of (discrete) optimization variables is constructed. The main algorithm for branch in bound is found in optimizeMido.
// In createMidoStepInput the MidoInput is converted to a normal UserInput, but with the bounds of discrete variables changed to the bounds of the current node. This input is than simply passed to Dyos to compute the continous relaxation.

//Main functions
UserOutput::Output runDyos(const struct UserInput::MidoInput &input);
UserOutput::Output runDyos(struct UserInput::MidoInput input, const Logger::Ptr &logger);
UserOutput::Output optimizeMido(const struct UserInput::MidoInput &input, 
  const std::vector<babBase::OptimizationVariable> &vars, double rootLowerBound, 
  double rootUpperBound, const ParamIndex &paramIndex, 
  std::chrono::steady_clock::time_point startingTime, const Logger::Ptr &logger);

//Functions to do with creating Dyos input to describe continously  relaxed problem at current node.
std::tuple<babBase::BabNode, double, std::vector<double>/*solutionPoint*/, bool /*isIntegerFeasible*/, bool /*isConstraintFeasible*/, struct UserOutput::Output>
processNode(babBase::BabNode nodeToProcess, const std::vector<babBase::OptimizationVariable> &variables, const ParamIndex &paramIndex, const struct UserInput::MidoInput &input, const Logger::Ptr &logger);
UserInput::Input createMidoStepInput(struct UserInput::MidoInput input, const babBase::BabNode &node, const std::vector<babBase::OptimizationVariable> &vars, const ParamIndex &paramIndex);
void branchOnNode(babBase::OutVar<babBase::Brancher> brancher_, babBase::BabNode &node, const std::vector<double> &solPoint, double objValue, const UserInput::MidoInput &input);

//Helper functions
bool isParameterFixed(double lowerBound, double upperBound, const UserInput::MidoInput &input);
bool allIntegersFixed(const babBase::BabNode &node, const std::vector<babBase::OptimizationVariable> &variables, const struct UserInput::MidoInput &input);
unsigned findLeastFractionalInteger(const babBase::BabNode &node, const std::vector<double> &relaxationSolutionPoint, const UserInput::MidoInput &input);
std::vector<double> getSolPointFromDyosOut(const UserOutput::Output &dyosOut, const struct UserInput::MidoInput &input, const ParamIndex &paramIndex, unsigned numberOfIntegerOptimizationVariables);
bool isIntegerInfeasibleVariable(double variable, const UserInput::MidoInput &input);
bool isIntegerFeasiblePoint(const std::vector<double> &solPoint, const std::vector<babBase::OptimizationVariable> &variables, const struct UserInput::MidoInput &input);
bool isConstraintFeasibleSolution(const UserOutput::Output &dyosOut);
bool isMixedIntegerProblem(const UserInput::MidoInput &input);
double getObjValueFromDyosOut(const UserOutput::Output &dyosOut);
void setNewIncumbent(const std::vector<double> &foundSolutionPoint, double foundObjValue, UserOutput::Output currentOutput, babBase::OutVar<babBase::BabNode> nodeWithIncumbent, babBase::OutVar<babBase::Brancher> brancherToInform, babBase::OutVar<bool> incumbentFound, babBase::OutVar<double> bestObjValue, babBase::OutVar<UserOutput::Output> bestSolution);

// Functions to initialize or configure before optimizeMido is called
void configureBrancher(babBase::OutVar<babBase::Brancher> brancher_, double rootLowerBound, double rootUpperBound, const UserInput::MidoInput &input, const std::vector<babBase::OptimizationVariable> &vars, const Logger::Ptr &logger);
void checkAndCorrectInput(babBase::OutVar<UserInput::MidoInput> input_, const Logger::Ptr &logger);
std::pair<ParamIndex, std::vector<babBase::OptimizationVariable>>
initializeOptimizationVariablesAndIndex(const UserInput::MidoInput &input); //adds only discrete variables to the variables treated by the brancher
UserOutput::Output runDyosOnLocalRootRelaxation(const UserInput::MidoInput &input, const Logger::Ptr &logger);

//Functions for runing starting heuristics. More accurately these are rounding heuristics that try to find an constraint and integrality feasible point around a given starting point
struct RunResult;
// A map that is used to save several evaluations of the objective function and if the point was feasible. Since in MIDO Dyos
// an evaluation corresponds to a run of the DYOS Integrator/Optimizer. [Optimizer because currently an evaluation point only contains the (possibly relaxed) discrete variables and the continous controls still must be calculated by the optimizer.]
// Once a lower bound can be calculated, we can also branch on continous variables. To enable this  extractSolutionPont and initializeOptimizationVariablesAndIndex must be changed. Also the number of integer variables must be computed instead of reading the size of branching variables.
// Also a few assertions in the code that test for continous variables can be removed.
using SaveEvalMap = std::unordered_map<std::vector<double>, RunResult>;

RunResult evaluateFixedPointAndSaveEvaluation(const std::vector<double> &x_eval, const babBase::BabNode &node,
                                              const UserInput::MidoInput &input, const std::vector<babBase::OptimizationVariable> &variables,
                                              const ParamIndex &paramIndex, const Logger::Ptr &logger, babBase::OutVar<SaveEvalMap> evalSave_);
std::tuple<bool /*successfull*/, std::vector<double> /*resultPoint*/, double /*objValue*/, UserOutput::Output> runStartingHeuristic(UserInput::MidoInput::StartHeuristic heuristicType, const std::vector<double> &x_start, const UserInput::MidoInput &input, const babBase::BabNode &node, const ParamIndex paramIndex, std::vector<babBase::OptimizationVariable> &variables, const Logger::Ptr &logger);


// Function definitions follow:

/** @brief The function implementing the actual branch and bound algorithm/loop  for a given and configured babBase::Brancher brancher and possible preexisting solutionHistory (existingSolutionWithSolutionHistory).
 *
 * @param input The MidoInput that contains the user input as well as configurations lik stopping criteria.
 * @param vars  All the variables that are to be branched on. Must correspond to the paramIndex.
 * @param rootLowerBound The lower bound given at the root node.
 * @param rootUpperBound The objective value for the best known feasible solution. Nodes that have a lower bound higher than this will be discarded. 
 * @param paramIndex     A struct that maps the entries of vars to the corresponding stage, parameter and Interval in input.
 * @param startingTime   The time to compute the already passed time from.
 * @param brancher_      An already configured brancher with the root node already inserted. Might also contain further nodes of the B&B-Tree if it was previously partially explored. E.g. diving.
 * @param existingSolutionWithSolutionHistory If we already partilly solved the B&B Tree, this gives us the previous incumbent and the previous solutionHistory.
 * @param logger         A logger to pass logging information and messages to.
 * @return               An UserOutput::Output struct. Contains the incumbent if one was found. solutionHistory contains the UserOutputs from the evaluated nodes. If no solution  was found.
 *                       the optimizerOutput.resultFlag is set to FAILED or if we can be sure INFEASIBLE. In that case the output struct will contain the last solved relaxation.
 */
UserOutput::Output optimizeMido(const struct UserInput::MidoInput &input, const std::vector<babBase::OptimizationVariable>& vars, double rootLowerBound, double rootUpperBound, ParamIndex &paramIndex, std::chrono::steady_clock::time_point startingTime, babBase::OutVar<babBase::Brancher> brancher_, UserOutput::Output existingSolutionWithSolutionHistory, const Logger::Ptr &logger)
{
        babBase::Brancher &brancher      = brancher_;
        UserOutput::Output &bestSolution = existingSolutionWithSolutionHistory;

        bool incumbentFound = (existingSolutionWithSolutionHistory.optimizerOutput.resultFlag == UserOutput::OptimizerOutput::OK);
        double bestObjValue = rootUpperBound;

        int iteration                 = 0;
        bool integerFeasibleIsEnought = (input.isStoppingAfterFirstFeasibleEnabled());

        //define when optimality gab is below tolerances
        auto isOptimalityGapAboveTolerances = [&brancher](double relTol, double absTol) {
                return brancher.get_pruning_score_gap() > std::max(absTol, relTol * std::abs(brancher.get_pruning_score_threshold()));
        }; //This will NOT enforce both the relative and absolute tolerance, it will enforce the less strict one, reason: if upper is near 0.0 it is numerically infeasible to ensure any relative tolerance
        auto isTimeLimitExceeded = [&startingTime, &input]() {
                return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - startingTime).count() >= input.getTimeLimit();
        };
        auto isIterationLimitExceeded = [&iteration, &input]() {
                return iteration >= input.getIterationLimit();
        };
        //We at leats want to run once, so we have an output to return.
        auto isNoSolutionHistoryAvailible = [&bestSolution]() { return bestSolution.solutionHistory.empty(); };

        while ((!isTimeLimitExceeded() && !isIterationLimitExceeded() && !(integerFeasibleIsEnought 
          && incumbentFound) && 
          (isOptimalityGapAboveTolerances(input.relTolForOptimalityGap, input.absTolForOptimalityGap) && 
            brancher.get_nodes_in_tree() > 0)) || isNoSolutionHistoryAvailible())
        {
                ++iteration;

                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "BaB Iteration: " + std::to_string(iteration) + "\n");
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::DEBUG_VERBOSITY, "Nodes In Tree: " + std::to_string(brancher.get_nodes_in_tree()) + "\n");
                logger->flushAll();
                
                //pop next node from brancher
                babBase::BabNode currentNode = brancher.get_next_node();
                babBase::BabNode processedNode;
                bool isRelaxationSolvedSuccessfully, isRelaxationSolutionIntegerFeasible;
                double objValue;
                std::vector<double> solPoint;
                UserOutput::Output dyosOut;

                //Process the node, changing e.g. bounds, 
                std::tie(processedNode, objValue, solPoint, isRelaxationSolutionIntegerFeasible, isRelaxationSolvedSuccessfully, dyosOut) = processNode(currentNode, vars, paramIndex, input, logger);
                //This could be integrated into processNode, but is kept here for making it clear how the lower bound is (not) calculated.
                if (input.isConvexAssumed())
                {
                        //The local solution of the continously relaxed problem is a solution of a valid relaxation.
                        processedNode.set_pruning_score(objValue);
                }
                else
                {
                        //currently no lower bounding procedure, we thus can not generate a better lower bound
                        processedNode.set_pruning_score(rootLowerBound);
                }
                
                //informs the brancher about process made by processing the node so it can collect statistics
                brancher.register_node_change(currentNode.get_ID(), processedNode);

                //check if integer-feasible Solution has been found and if its an incument
                if (isRelaxationSolvedSuccessfully && isRelaxationSolutionIntegerFeasible)
                {
                        if (objValue < bestObjValue)
                        {
                                //inform brancher on new incumbent
                                //out_par means that the variable is changed by the function call
                                using babBase::out_par;
                                setNewIncumbent(solPoint, objValue, out_par(dyosOut), out_par(processedNode), out_par(brancher), out_par(incumbentFound), out_par(bestObjValue), out_par(bestSolution));
                                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Found new incumbent" + std::to_string(bestObjValue) + "\n");
                        }

                        //wasnt in original, but even when a solution was integer, since it is a local solution, it still needs to be branched on
                        if (!input.isConvexAssumed())
                        {
                                if (!allIntegersFixed(processedNode, vars, input))
                                {

                                        branchOnNode(babBase::out_par(brancher), processedNode, solPoint, objValue, input);
                                }
                        }
                }
                //discards full solutions where all integers are fixed but solver can not find a solution
                else if (allIntegersFixed(processedNode, vars, input))
                {
                        //drop solutions
                }
                else //branch
                {
                        //inserts two child nodes into the B&B tree. The brancher decides which variable is branched on
                        branchOnNode(babBase::out_par(brancher), processedNode, solPoint, objValue, input);
                }

                bestSolution.solutionHistory.push_back(dyosOut);
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::DEBUG_VERBOSITY, "Time gone (sec): " + std::to_string(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - startingTime).count()) + "\n");
       
       }//end while
        if (incumbentFound)
        {
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "ObjValue Found: " + std::to_string(bestObjValue) + "\n");
                return bestSolution;
        }
        else
        {
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Termination Without Finding Solution\n");
                UserOutput::Output out;
                //at least one solution should be calculated
                assert(!bestSolution.solutionHistory.empty());
                //normaly the UserOutput contains the incumbent, but we have not found one, so we return the last solved relaxation for eventual inspection
                out                 = bestSolution.solutionHistory.back();
                out.solutionHistory = bestSolution.solutionHistory;
                out.optimizerOutput.objVal     = std::numeric_limits<double>::infinity();
                //no incumbent found -> FAILED
                out.optimizerOutput.resultFlag = UserOutput::OptimizerOutput::FAILED;
                if (input.isConvexAssumed())
                {
                        out.optimizerOutput.resultFlag = UserOutput::OptimizerOutput::INFEASIBLE;
                }
                return out;
        }
        logger->flushAll();
}

/// @brief Function to process a node from the branch and bound tree and return the results of the computation.

/// Will run the continous relaxed problem described by the current node and return results such as if the solver found a constraint feasible point and the objective function value found.
///
///@return The Tuple(processed node, the obtained objectiveValue, the solution point of the relaxation,
//         if the solutionPoint is integer feasible, if it is constraint feasible, and the obtained DyosOutput to be saved to the solution history).
std::tuple<babBase::BabNode, double /*objValue*/, std::vector<double> /*solPoint*/, bool /*isIntegerFeasible*/, bool /*isConstraintFeasible*/, struct UserOutput::Output> processNode(babBase::BabNode nodeToProcess, const std::vector<babBase::OptimizationVariable> &variables, const ParamIndex &paramIndex, const struct UserInput::MidoInput &input, const Logger::Ptr &logger)
{
        //Convert node to Dyos input.
        UserInput::Input inputForCurrentNode = createMidoStepInput(input, nodeToProcess, variables, paramIndex);
        //Run Dyos on generated input
        UserOutput::Output currentDyosOut = runDyos(inputForCurrentNode, logger);
        assert(!currentDyosOut.stages.empty());
        double objValue              = getObjValueFromDyosOut(currentDyosOut);
        std::vector<double> solPoint = getSolPointFromDyosOut(currentDyosOut, input, paramIndex, variables.size());
        bool isIntegerFeasible       = isIntegerFeasiblePoint(solPoint, variables, input);
        bool isConstraintFeasible    = isConstraintFeasibleSolution(currentDyosOut);
        //Currently no bound tightening.
        babBase::BabNode processedNode(-std::numeric_limits<double>::infinity() /*to be set outside*/, nodeToProcess.get_lower_bounds(), nodeToProcess.get_upper_bounds(), nodeToProcess.get_ID(), nodeToProcess.holds_incumbent());
        return std::make_tuple(processedNode, objValue, solPoint, isIntegerFeasible, isConstraintFeasible, currentDyosOut);
}

///@brief Branches on a node
void branchOnNode(babBase::OutVar<babBase::Brancher> brancher_, babBase::BabNode &node, const std::vector<double> &solPoint, double objValue, const UserInput::MidoInput &input)
{
        babBase::Brancher &brancher = brancher_;

        bool isDivingActive = (input.startHeuristic == UserInput::MidoInput::StartHeuristic::DIVING);

        if (!isDivingActive)
        {
                brancher.branch_on_node(node, solPoint, objValue);
        }
        else //diving is active, so the least fractional integer must be fixed
        {
                unsigned varNumToFix            = findLeastFractionalInteger(node, solPoint, input);
                std::vector<double> upperBounds = node.get_upper_bounds();
                std::vector<double> lowerBounds = node.get_lower_bounds();

                int integerToFixTo = std::round(solPoint[varNumToFix]);
                // split node in 3 pieces fixed, above fixed, below fixed
                // for example [0 4] that is to be fixed at 2 is seperated into [0 1],[3 4],[2]

                //up branche eg. [3 4]
                if(integerToFixTo+1<upperBounds[varNumToFix])
             {
                 std::vector<double>lowerBounds_up=node.get_lower_bounds();
                 lowerBounds_up[varNumToFix]=integerToFixTo+1;
                 brancher.branch_on_node(babBase::BabNode(node.get_pruning_score(),lowerBounds_up,node.get_upper_bounds(),0,false),solPoint,objValue);
             }
             //down branch eg. [0 1]
             if(integerToFixTo-1>lowerBounds[varNumToFix])
             {
                 std::vector<double>upperBounds_down=node.get_upper_bounds();
                 upperBounds_down[varNumToFix]=integerToFixTo-1;
                 brancher.branch_on_node(babBase::BabNode(node.get_pruning_score(),node.get_lower_bounds(),upperBounds_down,0,false),solPoint,objValue);
             }
                upperBounds[varNumToFix] = integerToFixTo;
                lowerBounds[varNumToFix] = integerToFixTo;
                //fixed branch (e.g. [2]) last, since we are searching depth first and the fixed branch is to be the next to be explored
                brancher.branch_on_node(babBase::BabNode(node.get_pruning_score(), lowerBounds, upperBounds, 0, false), solPoint, objValue);
        }
}

/// @brief //Find the index of the variable that is closest to a being integer, but still not integer feasible or fixed.
unsigned findLeastFractionalInteger(const babBase::BabNode &node,
                                    const std::vector<double> &relaxationSolutionPoint, const UserInput::MidoInput &input)
{
        unsigned indexToSelect                = 0;
        double minFractionality               = 1.1;
        const std::vector<double> lowerBounds = node.get_lower_bounds();
        const std::vector<double> upperBounds = node.get_upper_bounds();

        for (unsigned i = 0; i < relaxationSolutionPoint.size(); i++)
        {
                double x = relaxationSolutionPoint[i];
                if (std::abs(x - std::round(x)) < minFractionality && isIntegerInfeasibleVariable(x, input) && !isParameterFixed(lowerBounds[i], upperBounds[i], input))
                {
                        indexToSelect    = i;
                        minFractionality = std::abs(x - std::round(x));
                }
        }
        if (minFractionality > 1.0) //no fractional variable left
        {
                assert(false);
        }
        return indexToSelect;
}

/**
 * @brief Sets a new incumbent with the solution point foundSolutionPoint and the objective value foundObjValue.
 *
 * The brancher object is informed about new incumbent, as is the node the incument was found in. The best found objective value (passed in bestObjValueIn) is changed as is the current output. The variable incumbentFoundIn is set to true.
 *
 * @param foundSolutionPoint : Solution point (in terms of integer variables) the new incumbent was found on.
 * @param foundObjValue : The objective value found for the new incumbent
 * @param incumbentOutputIn : The output from Dyos from the run that found the new incumbent
 * @param nodeWithIncumbentIn : The node at which the new incumbent was found
 * @param brancherToInformIn : The brancher that needs to be informed
 * @param incumbentFoundIn : The variable to set to true if a incumbent was found
 * @param bestObjValueIn : The variable to be updated to the best objective function
 * @param outputWithBestSolutionIn The Dyos output to be updated. Contains also the full solution point (including continous variables).Holds also the current solution history.
 */
void setNewIncumbent(const std::vector<double> &foundSolutionPoint, double foundObjValue, UserOutput::Output incumbentOutputIn, babBase::OutVar<babBase::BabNode> nodeWithIncumbentIn, babBase::OutVar<babBase::Brancher> brancherToInformIn, babBase::OutVar<bool> incumbentFoundIn, babBase::OutVar<double> bestObjValueIn, babBase::OutVar<UserOutput::Output> outputWithBestSolutionIn)
{
        //Defining references for easier access to  the OutVar variables
        babBase::Brancher &brancherToInform        = brancherToInformIn;
        babBase::BabNode &nodeWithIncumbent        = nodeWithIncumbentIn;
        bool &incumbentFound                       = incumbentFoundIn;
        double &bestObjValue                       = bestObjValueIn;
        UserOutput::Output &outputWithBestSolution = outputWithBestSolutionIn;
        UserOutput::Output &incumbentOutput        = incumbentOutputIn;

        brancherToInform.set_new_incumbent_point(foundSolutionPoint);
        nodeWithIncumbent.set_holds_incumbent(true);
        brancherToInform.decrease_pruning_score_threshold_to(foundObjValue);
        incumbentFound = true;
        bestObjValue   = foundObjValue;
        //Transfer the solution history to incumbent output
        incumbentOutput.solutionHistory = outputWithBestSolution.solutionHistory;
        outputWithBestSolution          = incumbentOutput;
}

/// @brief Extracts the solution point (only in terms of integer variables) from the Dyosout.
std::vector<double> getSolPointFromDyosOut(const UserOutput::Output &dyosOut, const struct UserInput::MidoInput &input, const ParamIndex &paramIndex, unsigned numberOfIntegerOptimizationVariables)
{
        std::vector<double> solutionPoint(numberOfIntegerOptimizationVariables, 0.0);

        //     const UserOutput::ParameterOutput& paramInOutput=dyosOut.stages[stage].integrator.parameters[parameterNum];
        for (unsigned i = 0; i < numberOfIntegerOptimizationVariables; i++)
        {
                unsigned stage;
                unsigned paramNum;
                bool isTimeInvariant;
                unsigned intervalNum;
                double currentEntryToSolutionPoint                      = 0.0;
                std::tie(stage, paramNum, isTimeInvariant, intervalNum) = paramIndex.getFromOptimizationVariablePosition(i);
                assert(dyosOut.stages.size() > stage);
                assert(dyosOut.stages[stage].integrator.parameters.size() > paramNum);
                if (isTimeInvariant)
                {
                        currentEntryToSolutionPoint = dyosOut.stages[stage].integrator.parameters[paramNum].value;
                }
                else
                {
                        assert(dyosOut.stages[stage].integrator.parameters[paramNum].paramType == UserOutput::ParameterOutput::PROFILE);
                        currentEntryToSolutionPoint = dyosOut.stages[stage].integrator.parameters[paramNum].grids.at(0).values[intervalNum];
                }
                solutionPoint[i] = currentEntryToSolutionPoint;
        }

        return solutionPoint;
}

///

/**
 *  @brief Constructs an input for 'normal' Dyos from a BabNode that is used in the branch and bound algorithm.
 *
 * The problem solved here is that we branch only on the discrete variables. This branching is handled with the babBase 'library'.
 * There, we do not operate on stages and parameters, but only on OptimizationVariables. We need to convert the MidoInput input to a
 * normal  input we can plug into 'normal' Dyos in order to solve the continously relaxed optimization problem. MidoInput already is
 * convertible to Input, but we need to add the constraints imposed by the branching and saved in the BabNode node to the  parameters.
 *
 * @param input : Input to MidoDyos that is to be changed with the bounds from the node and to be converted to a input for 'normal' Dyos.
 * @param node :  Contains the bounds applying to the current node / subproblem to solve.
 * @param vars :  Vector of used optimization variables handeled by the B&B tree, which are currently only the discrete variables.
 * @param paramIndex : Mapping of (Stage,Parameter) in Dyos Notation to the position in vars.
 * @return UserInput::Input
 */
UserInput::Input createMidoStepInput(struct UserInput::MidoInput input, const babBase::BabNode &node, const std::vector<babBase::OptimizationVariable> &vars, const ParamIndex &paramIndex)
{
        //input is taken by copy on purpose, since a copy is needed anyway

        assert(!input.stages.empty());
        //Convert node to a binStatus Matrix
        const std::vector<double> upperBounds = node.get_upper_bounds();
        const std::vector<double> lowerBounds = node.get_lower_bounds();

        //go through all variabels in the OptimizationVariables Vector e.g. the integer variables  set  corresponding variables in MidoInput input as fixed if they are fixed to an integer in the OptimizationVariables Vector.
        for (unsigned positionInOptimizationVariables = 0; positionInOptimizationVariables < vars.size(); positionInOptimizationVariables++)
        {
                unsigned stage, parameterNum, intervalNum;
                bool isParameterTimeInvariant;

                //get the discrete parameter in (stage,paramNum)-notation using reverse lookup of the index. If parameter is time-variant, intervalNumber represents the index of the interval the parameter corresponds to.
                std::tie(stage, parameterNum, isParameterTimeInvariant, intervalNum) = paramIndex.getFromOptimizationVariablePosition(positionInOptimizationVariables);

                //take note of the reference, if parameterToChange is changed, this modifies the copy of input!
                UserInput::MidoParameterInput &parameterToChange = input.stages[stage].integrator.parameters[parameterNum];

                //Only discrete types should be output by the index
                assert(parameterToChange.getDiscreteType() != UserInput::MidoParameterInput::CONTINUOUS);

                bool fixed          = isParameterFixed(lowerBounds[positionInOptimizationVariables], upperBounds[positionInOptimizationVariables], input);
                bool closeToInteger = !isIntegerInfeasibleVariable(lowerBounds[positionInOptimizationVariables], input);
                if (isParameterTimeInvariant)
                {
                        if (fixed)
                        {
                                const double &u = upperBounds[positionInOptimizationVariables];
                                const double &l = lowerBounds[positionInOptimizationVariables];

                                double toFixTo = 0.5 * (u + l);
                                if (closeToInteger)
                                {
                                        toFixTo = std::round(toFixTo);
                                }
                                parameterToChange.sensType   = UserInput::MidoParameterInput::NO;
                                parameterToChange.upperBound = toFixTo;
                                parameterToChange.lowerBound = toFixTo;
                                parameterToChange.value      = toFixTo;
                        }
                        else
                        {
                                parameterToChange.lowerBound = lowerBounds[positionInOptimizationVariables];
                                parameterToChange.upperBound = upperBounds[positionInOptimizationVariables];
                        }
                }
                else
                {
                        if (fixed)
                        {
                                const double &u = upperBounds[positionInOptimizationVariables];
                                const double &l = lowerBounds[positionInOptimizationVariables];
                                double toFixTo  = 0.5 * (u + l);
                                if (closeToInteger)
                                {
                                        toFixTo = std::round(toFixTo);
                                }
                                //Choose rounded value of bound wi
                                parameterToChange.grids.at(0).lowerBounds[intervalNum] = toFixTo;
                                parameterToChange.grids.at(0).upperBounds[intervalNum] = toFixTo;

                                assert(parameterToChange.grids.at(0).values.size() == parameterToChange.grids.at(0).lowerBounds.size());
                                parameterToChange.grids.at(0).values[intervalNum] = toFixTo;
                        }
                        else
                        {
                                parameterToChange.grids.at(0).lowerBounds[intervalNum] = lowerBounds[positionInOptimizationVariables];
                                parameterToChange.grids.at(0).upperBounds[intervalNum] = upperBounds[positionInOptimizationVariables];
                        }
                }
        }

        //convert the MidoInput to normal UserInput
        return std::move(input).convertToInput();
}

/// @brief Adds a quadratic penalty term to the objective function. The form is a*oldObjective+b*sum_i(u_i-u_i_soll)^2. This is used for heuristics like the feasability pump.
double a;

/**
 * @param input  The input whose objective function is to be changed by adding the penalty.
 * @param vars   All the optimization variables threated by branch and bound in babBase format.
 * @param goals  Representing u_i_soll in the formula above. Each entry corresponds to the entries in vars.
 * @param oldObjectiveFactor Corresponds to a in the formula above.
 * @param penaltyFactor Corresponds to b in the formula above.
 * @param paramIndex Mapping from vars to UserInput format, i.e. u_i.
 * @return A modiefied version of input containing a objective in form of a*oldObjective+b*sum_i(u_i-u_i_soll)^2 
 */
UserInput::MidoInput addQuadraticPenaltyToStepInput(struct UserInput::MidoInput input, const std::vector<babBase::OptimizationVariable> &vars, const std::vector<double> &goals, double oldObjectiveFactor, double penaltyFactor, const ParamIndex &paramIndex)
{
        //input is taken by copy on purpose, since a copy is needed anyway

        //Controls have a name and possibly multiple grids. Each grid has multiple intervals (for time variant controls).
        //For piece-wise constant controls there is a parameterization variable for each interval corresponding to the value of the control over that interval
        //For piece-wise linear controls there are interval+1 parameterization variables, where each corresponds to the value on the beginning of the interval and the last one is the value at the end of the last interval.
        //Discrete variables are always piece-wise-constant so intervals correspond to an parameterization parameter.

        //Most of this function is just to create the right input for stageiCustomPenalty.

        std::vector<std::map<std::pair<std::string /*name*/, unsigned /*gridNum*/>, std::vector<std::pair<unsigned /*intervalNum*/, double /*goal*/>>>> stageGoals;
        stageGoals.resize(input.stages.size());
        std::vector<std::set<std::tuple<std::string /*name*/, unsigned /*gridNum*/, unsigned /*numberIntervals*/>>> stageRequestLists;
        stageRequestLists.resize(input.stages.size());
        //go through all variabels in the OptimizationVariables Vector e.g. the integer variables  set  corresponding variables, set goal and request their evaluation in for the penalty function
        assert(vars.size() == goals.size());
        for (unsigned positionInOptimizationVariables = 0; positionInOptimizationVariables < vars.size(); positionInOptimizationVariables++)
        {

                unsigned stage, parameterNum, intervalNum;
                bool isParameterTimeInvariant;

                //get the discrete parameter in (stage,paramNum)-notation using reverse lookup of the index. If parameter is time-variant, intervalNumber represents the index of the interval the parameter corresponds to.
                std::tie(stage, parameterNum, isParameterTimeInvariant, intervalNum) = paramIndex.getFromOptimizationVariablePosition(positionInOptimizationVariables);

                //take note of the reference, if parameterToChange is changed, this modifies the copy of input!
                const UserInput::ParameterInput &parameterToChange = input.stages[stage].integrator.parameters[parameterNum].convertToParameterInput();
                unsigned numIntervals;
                //Time invariant only has one grid and one interval so intervalNum=0 (given for timeInvariant by paramIndex)  is not  correct.
                if (parameterToChange.paramType == UserInput::ParameterInput::TIME_INVARIANT)
                {
                        numIntervals = 1; //in Dyos so defined
                        intervalNum  = 0;
                }
                else
                {
                        assert(!parameterToChange.grids.empty());
                        numIntervals = parameterToChange.grids[0].numIntervals;
                }

                // save parameterization grids that stageCustioObjFunction will later use
                //requests the whole grid (may insert multiple times for each parameterization variables -> use  a set
                stageRequestLists[stage].insert(std::make_tuple(parameterToChange.name, /*gridNum=*/0, numIntervals));

                double goal = goals[positionInOptimizationVariables];

                stageGoals[stage][std::make_pair(parameterToChange.name, 0)].push_back(std::make_pair(intervalNum, goal));
        }

        //modify for each stage the objective function
        for (int stage = 0; stage < input.stages.size(); stage++)
        {

                const std::map<std::pair<std::string /*name*/, unsigned /*gridNum*/>, std::vector<std::pair<unsigned /*intervalNum*/, double /*goal*/>>> currentStageGoals = stageGoals[stage];
                input.stages[stage].optimizer.customObjectiveInformation.requestedListOfParameterizationGrids.assign(stageRequestLists[stage].begin(), stageRequestLists[stage].end());
                auto stageiCustomPenalty = [currentStageGoals](std::map<std::pair<std::string /*NameOfParameter*/, unsigned /*GridIndex*/>, std::vector<ADdouble> /*values of the decision Variables*/> requestedDecVarValues) {
                        ADdouble penalty = 0.0;

                        for (const std::pair<const std::pair<std::string /*name*/, unsigned /*GridIndex*/>, std::vector<ADdouble>> &p : requestedDecVarValues)
                        {
                                auto iterator = currentStageGoals.find(p.first);
                                if (iterator == currentStageGoals.end())
                                {
                                        std::cout << "Could not find a goal" << std::endl;
                                        std::string name = p.first.first;
                                        std::cout << name << "with" << p.first.second << "intervals" << std::endl;
                                        std::cout << "Size currentStageGoals" << currentStageGoals.size() << std::endl;
                                }
                                else
                                {
                                        std::vector<std::pair<unsigned /*intervalNum*/, double /*goal*/>> goalsCurrentParameterCurrentGrid = iterator->second;
                                        const std::vector<ADdouble> &actualValuesCurrentParameterCurrentGrid                               = p.second;
                                        for (const std::pair<unsigned, double> &c : goalsCurrentParameterCurrentGrid)
                                        {
                                                penalty += pow(actualValuesCurrentParameterCurrentGrid[c.first] - c.second, 2);
                                        }
                                }
                        }

                        return penalty;
                };

                if (input.stages[stage].treatObjective)
                {
                        input.stages[stage].optimizer.customObjectiveInformation.originalObjectiveFactor = oldObjectiveFactor;
                        input.stages[stage].optimizer.customObjectiveInformation.penaltyFactor           = penaltyFactor;
                        input.stages[stage].optimizer.customObjectiveInformation.penaltyFunction         = stageiCustomPenalty;
                }
                else
                {
                        // if the objective was set to be ignored before, we have to enable it and set the oldObj factor to zero
                        input.stages[stage].treatObjective                                               = true;
                        input.stages[stage].optimizer.customObjectiveInformation.originalObjectiveFactor = 0.0;
                        input.stages[stage].optimizer.customObjectiveInformation.penaltyFactor           = penaltyFactor;
                        input.stages[stage].optimizer.customObjectiveInformation.penaltyFunction         = stageiCustomPenalty;
                }
        }
        return std::move(input);
}

/// @brief Check if a (integer) parameter is fixed, i.e. if th bounds are close enought.
bool isParameterFixed(double lowerBound, double upperBound, const UserInput::MidoInput &input)
{
        //Relative Tolerance is problematic for values near zero.
        if (std::abs(lowerBound) + std::abs(upperBound) < input.absTolForIntegralityConstraint)
        {
                return true;
        }
        return babBase::larger_or_equal_within_rel_and_abs_tolerance(lowerBound, upperBound, input.relTolForIntegralityConstraint, input.absTolForIntegralityConstraint);
}

/// @brief Checks if a variable is integer feasible within the tolerances for integrality  in input.
bool isIntegerInfeasibleVariable(double variable, const UserInput::MidoInput &input)
{
        //Relative Tolerance is problematic for values near zero.
        if (std::abs(variable) < input.absTolForIntegralityConstraint)
        {
                 return false;
        }

        //Huge integers like 10^6 might also make relative tolerance cause unexpected behavior.
        return !(babBase::larger_or_equal_within_rel_and_abs_tolerance(std::floor(variable), variable, input.relTolForIntegralityConstraint, input.absTolForIntegralityConstraint)) && !(babBase::larger_or_equal_within_rel_and_abs_tolerance(-std::ceil(variable), -variable, input.relTolForIntegralityConstraint, input.absTolForIntegralityConstraint));
        //negative sign could dropped when changing the first two arguments, but toleranc should be relative to continous variable as stated in the documentation
}

///@brief Checks if a the solution point solPoint has integer variables that are still fractional, i.e. not integer at the solution point.
bool isIntegerFeasiblePoint(const std::vector<double> &solPoint, const std::vector<babBase::OptimizationVariable> &variables, const struct UserInput::MidoInput &input)
{

        assert(solPoint.size() == variables.size());

        for (unsigned int i = 0; i < solPoint.size(); i++)
        {
                if ((variables[i].get_variable_type() != babBase::enums::VT_CONTINUOUS) && isIntegerInfeasibleVariable(solPoint[i], input))
                {

                        return false;
                }
        }

        // all variables passed the test
        return true;
}

/// @brief Checks if the node has all integer variables with fixed value.
bool allIntegersFixed(const babBase::BabNode &node, const std::vector<babBase::OptimizationVariable> &variables, const struct UserInput::MidoInput &input)
{
        assert(node.get_lower_bounds().size() == node.get_upper_bounds().size());

        for (unsigned int i = 0; i < node.get_lower_bounds().size(); i++)
        {

                if ((variables[i].get_variable_type() != babBase::enums::VT_CONTINUOUS) && !(isParameterFixed(node.get_lower_bounds()[i], node.get_upper_bounds()[i], input)))
                {
                        return false;
                }
        }

        // All variables are fixed

        return true;
}

/// @brief Extracts the objective value from the given Dyos output dyosOut
double getObjValueFromDyosOut(const UserOutput::Output &dyosOut)
{
        return dyosOut.optimizerOutput.objVal;
}

/// @brief Varifies if Dyos returned a solution that is feasible to the continous constraints. The solution need not be optimal to return true.
bool isConstraintFeasibleSolution(const UserOutput::Output &dyosOut)
{
        bool feasibleFound = (dyosOut.optimizerOutput.resultFlag == UserOutput::OptimizerOutput::OK || dyosOut.optimizerOutput.resultFlag == UserOutput::OptimizerOutput::NOT_OPTIMAL);
        if (!feasibleFound && dyosOut.optimizerOutput.resultFlag == UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS)
        {
                for (const UserOutput::StageOutput &stageOutput : dyosOut.stages)
                {
                        for (const UserOutput::ConstraintOutput &constraint : stageOutput.optimizer.nonlinearConstraints)
                        {

                                for (const UserOutput::ConstraintGridOutput &grid : constraint.grids)
                                {
                                        for (const double value : grid.values)
                                        {
                                                feasibleFound = feasibleFound && value >= constraint.lowerBound && value <= constraint.upperBound;
                                                if (!feasibleFound)
                                                        break;
                                        }
                                }
                                if (!feasibleFound)
                                        break;
                        }
                        for (const UserOutput::ConstraintOutput &constraint : stageOutput.optimizer.linearConstraints)
                        {
                                for (const UserOutput::ConstraintGridOutput &grid : constraint.grids)
                                {

                                        for (const double value : grid.values)
                                        {
                                                feasibleFound = feasibleFound && value >= constraint.lowerBound && value <= constraint.upperBound;
                                                if (!feasibleFound)
                                                        break;
                                        }
                                }
                                if (!feasibleFound)
                                        break;
                        }
                }
        }
        //did the optimizer return a feasible point?
        return feasibleFound;
}

///@brief Tests if the input is a Mixed Integer Problem, by verifying that there are discrete parameters in input.
bool isMixedIntegerProblem(const UserInput::MidoInput &input)
{
        for (const UserInput::MidoStageInput &testedStage : input.stages)
                for (const UserInput::MidoParameterInput &testedParameter : testedStage.integrator.parameters)
                {
                        if (testedParameter.getDiscreteType() != UserInput::MidoParameterInput::CONTINUOUS)
                        {
                                return true;
                        }
                }

        //contains no discrete parameters
        return false;
}

/**
 * @brief Configures the brancher according to the settings in input. If the brancher has no nodes, a root node with the given rootLowerBound will be added. The upper bound saved in brancher will be set tho rootUpperBound if this constitutes a decrease.
 *
 * @param[in,out] brancher_ : The brancher to be configured
 * @param rootLowerBound : The lowerBound to be set
 * @param rootUpperBound : The upperBound to be set
 * @param input : MidoInput with NodeSelectionStrategy and BranchingStrategy settings
 * @param logger : logger to log Warnings to
 */
void configureBrancher(babBase::OutVar<babBase::Brancher> brancher_, double rootLowerBound, double rootUpperBound, const UserInput::MidoInput &input, const std::vector<babBase::OptimizationVariable> &vars, const Logger::Ptr &logger)
{

        babBase::Brancher &brancher = brancher_;
        //strategy is best score not(!) bestBound
        //how the score is calculated is determined by  brancher.set_node_selection_score_function
        brancher.set_node_selection_strategy(babBase::enums::NS::NS_BESTBOUND);
        switch (input.nodeSelectionStrategy)
        {
                case UserInput::MidoInput::NodeSelectionStrategy::BEST_FIRST_SEARCH:
                        brancher.set_node_selection_score_function(babBase::low_pruning_score_first);
                        break;

                case UserInput::MidoInput::NodeSelectionStrategy::BREADTH_FIRST_SEARCH:
                        brancher.set_node_selection_score_function(babBase::low_id_first);
                        break;

                case UserInput::MidoInput::NodeSelectionStrategy::DEPTH_FIRST_SEARCH:
                        brancher.set_node_selection_score_function(babBase::high_id_first);

                case UserInput::MidoInput::NodeSelectionStrategy::BEST_EXPECTED:
                        brancher.set_node_selection_score_function(babBase::best_expected_first);
                        break;

                default:
                        logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY, " Selected NodeSelectionStrategy  is not yet implemented, best-first is used.");
                        brancher.set_node_selection_score_function(babBase::low_pruning_score_first);
                        break;
        }

        switch (input.variableBranchingStrategy)
        {
                case UserInput::MidoInput::BranchingStrategy::FRACTIONAL:
                        brancher.set_branching_dimension_selection_strategy(babBase::enums::BV_ABSDIAM);
                        break;
                case UserInput::MidoInput::BranchingStrategy::RELIABILITY:
                        brancher.set_branching_dimension_selection_strategy(babBase::enums::BV_PSCOSTS);

                default:
                        logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY, " Selected VariableBranchingStrategy is not yet implemented, fractional is used.");
                        brancher.set_branching_dimension_selection_strategy(babBase::enums::BV_ABSDIAM);
        }

        if (brancher.get_nodes_in_tree() == 0)
        {
                brancher.insert_root_node(babBase::BabNode(rootLowerBound, vars, 0, false));
        }

        brancher.decrease_pruning_score_threshold_to(rootUpperBound);
}

/**
* @brief main routine of midos branch and bound. A default logger is created.
*
* @param[in] input input containing complete input
* @return result of the branch and bound algorithm
*/
LINKDLL UserOutput::Output runDyos(const struct UserInput::MidoInput &input)
{
        Logger::Ptr logger(new StandardLogger());

        return runDyos(input, logger);
}

/**
* @brief main routine of midos branch and bound
*
* @param[in] input input containing complete input
* @param[in] logger the logger to hand message and logging information to.
* @return result of the branch and bound algorithm. If no integer feasible solution was found. The return is the last calculated relaxation.
* In this case optimizerOutput.resultFlag is set to FAILED, or if we can be sure the problem is infeasible to INFEASIBE. The objVal is also set to INF regardless of
* the objective value of the last relaxation. The evaluated points (outside of evaluations from heuristics) are saved in the solutionHistory field.
*/
LINKDLL UserOutput::Output runDyos(struct UserInput::MidoInput input,
                                   const Logger::Ptr &logger)
{
        if (!isMixedIntegerProblem(input))
        {
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY, "The supplied MidoInput did not have any discrete variables declared, but MidoDyos is meant to be used to handle discrete inputs.  Dyos will be called without considering discrete variables, but notice that Input should be used instead. \n");
                //Run normal Dyos
                return runDyos(input.convertToInput(), logger);
        }
        else
        {
                checkAndCorrectInput(babBase::out_par(input), logger); //  throws on errors

                //Take starting time measurement
                std::chrono::steady_clock::time_point startingTime = std::chrono::steady_clock::now();
                double lowerBound                                  = input.getDefaultLowerBound();
                double upperBound                                  = DYOS_DBL_MAX;
                UserOutput::Output dyosOut;

                if (input.isConvexAssumed() == true)
                {
                        //Use local relaxation to find a lowerBound
                        dyosOut = runDyosOnLocalRootRelaxation(input, logger);
                        // currently initialize for minimization otherwise swap upperBound and lowerBound
                        lowerBound = std::max(lowerBound, dyosOut.solutionHistory.back().optimizerOutput.objVal);
                }

                //the brancher/babBase saves all variables to branch on in a vector of optimizization variables
                //this vector is filled here and a mapping from the (stage,parameterNum) notation of Dyos to the position
                //in this vector is constructed here and saved in paramIndex
                ParamIndex paramIndex;
                std::vector<babBase::OptimizationVariable> integerVariables;
                std::tie(paramIndex, integerVariables) = initializeOptimizationVariablesAndIndex(input);

                babBase::Brancher brancher(integerVariables);
                configureBrancher(babBase::out_par(brancher), lowerBound, upperBound, input, integerVariables, logger);
                UserInput::MidoInput input_copy = input; //copy input so it can be modified to delete flags for already used heuristics

                //Dive by starting the B&B-Search with Depth-First-Search and fixing least fractional integers.
                if (input.startHeuristic == UserInput::MidoInput::StartHeuristic::DIVING)
                {

                        brancher.set_node_selection_strategy(babBase::enums::NS::NS_DEPTHFIRST);
                        input_copy.enableTerminationConditionFoundFeasibleSolution();
                        input_copy.enableTerminationConditionIterationLimit(std::min(2 * (int)integerVariables.size(), input.getIterationLimit()));
                        //run branch and bound with other settings as 'heuristic' for limited iterations,  save branchings in branch and bound tree of brancher since the BaB-Tree is partially explored already
                        dyosOut = optimizeMido(input_copy, integerVariables, lowerBound, upperBound, paramIndex, startingTime, babBase::out_par(brancher), dyosOut, logger);
                        //Afterwards brancher is changed and has information from previous run
                        upperBound = std::min(upperBound, dyosOut.optimizerOutput.objVal);

                        //Reset settings to original input settings and set upperBound, incumbent if found is saved in dyosOut
                        configureBrancher(babBase::out_par(brancher), lowerBound, upperBound, input, integerVariables, logger);
                        //Set Limit to Remaining Iterations
                        input_copy = input;
                        input_copy.enableTerminationConditionIterationLimit(input.getIterationLimit() - dyosOut.solutionHistory.size());
                        //Delete the diving setting
                        input_copy.startHeuristic = UserInput::MidoInput::StartHeuristic::NONE;
                }
                else if (input.startHeuristic != UserInput::MidoInput::StartHeuristic::NONE) //other heuristics
                {
                        if (dyosOut.solutionHistory.empty())
                        {
                                dyosOut = runDyosOnLocalRootRelaxation(input, logger);
                        }
                        std::vector<double> x_start = getSolPointFromDyosOut(dyosOut.solutionHistory.back(), input, paramIndex, integerVariables.size());
                        //extract root node from bracher
                        babBase::BabNode rootNode = brancher.get_next_node();

                        bool foundIncumbent;
                        std::vector<double> incumbentPoint;
                        double foundObjValue;
                        UserOutput::Output incumbentOutput;
                        //run heuristic from the start point
                        std::tie(foundIncumbent, incumbentPoint, foundObjValue, incumbentOutput) = runStartingHeuristic(input.startHeuristic, x_start, input, rootNode, paramIndex, integerVariables, logger);

                        if (foundIncumbent)
                        {
                                //save new incumbent in brancher, and modify dyosOut to include the new incumbent
                                setNewIncumbent(incumbentPoint, foundObjValue, incumbentOutput, babBase::out_par(rootNode), babBase::out_par(brancher), babBase::out_par(foundIncumbent), babBase::out_par(foundObjValue), babBase::out_par(dyosOut));
                                upperBound = std::min(upperBound, foundObjValue);
                        }

                        //also reinsert root node that was extracted earlier
                        brancher.insert_root_node(rootNode);
                        configureBrancher(babBase::out_par(brancher), lowerBound, upperBound, input, integerVariables, logger);
                        input_copy.startHeuristic = UserInput::MidoInput::StartHeuristic::NONE;
                }

                //start branch and bound algorithm
                return optimizeMido(input_copy, integerVariables, lowerBound, upperBound, paramIndex, startingTime, babBase::out_par(brancher), dyosOut, logger);
        } //end if(!isMixedIntegerProblem)
}

/// @brief checks for inconsistencies in input and corrects input if possible, otherwise logs all errors and throws
void checkAndCorrectInput(babBase::OutVar<UserInput::MidoInput> input_, const Logger::Ptr &logger)
{
        bool inputFaulty            = false;
        UserInput::MidoInput &input = input_;
        if (input.optimizerInput.optimizationMode != UserInput::OptimizerInput::MINIMIZE)
        {
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, " Currently only minimization is supported!");
                inputFaulty = true;
        }
        int stageNum = -1;
        for (UserInput::MidoStageInput &stage : input.stages)
        {
                stageNum++;

                for (UserInput::MidoParameterInput &parameter : stage.integrator.parameters)
                {
                        //Special rules must be checked for noncontinous variables
                        if (parameter.getDiscreteType() != UserInput::MidoParameterInput::ParameterDiscreteType::CONTINUOUS)
                        {
                                if (parameter.paramType == UserInput::MidoParameterInput::PROFILE)
                                {
                                        if (input.optimizerInput.adaptationOptions.adaptStrategy == UserInput::AdaptationOptions::ADAPT_STRUCTURE || input.optimizerInput.adaptationOptions.adaptStrategy == UserInput::AdaptationOptions::STRUCTURE_DETECTION)
                                        {
                                                // Since MIDO will treat each interval in the grid of a discrete and time-dependant (PROFILE) variable as
                                                // a binary variable in the branch and bound tree, we need to ensure that this interval is not subdivided from
                                                // adaptation/structureDetection on another PROFILE variable.
                                                if (stage.optimizer.structureDetection.maxStructureSteps != 0)
                                                {
                                                        stage.optimizer.structureDetection.maxStructureSteps = 0;
                                                        logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY, "For Mido need to ensure structureDetection.maxStructureSteps = 0 for all stages with discrete PROFILE controls. This is violated in stage: " + std::to_string(stageNum) + " and has been corrected.");
                                                }
                                        }
                                        //grids must be set and we currently only support a single grid on each discrete variable
                                        if (parameter.grids.size() != 1)
                                        {
                                                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, "Non-continous variables that are from type PROFILE need to have exactly one(!) grid. This is violated in variable: " + parameter.name);
                                                inputFaulty = true;
                                        }

                                        else
                                        {
                                                //Only PIECEWISE_CONSTANT possible
                                                if (parameter.grids[0].type != UserInput::ParameterGridInput::PIECEWISE_CONSTANT)
                                                {
                                                        logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, "Non-continous variables that are from type PROFILE can only have a grid type of PIECEWISE_CONSTANT. This is violated in variable: " + parameter.name);
                                                        inputFaulty = true;
                                                }
                                                //Dyos promises to use value if values is empty
                                                if (parameter.grids[0].values.empty())
                                                {
                                                        parameter.grids[0].values.resize(parameter.grids[0].lowerBounds.size(), parameter.value);
                                                }
                                                if (parameter.grids[0].adapt.maxAdaptSteps != 0)
                                                {
                                                        logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY, "Non-continous variables that are from type PROFILE can only have a grid with disabled adaptation (maxAdaptSteps=0). This is violated in variable: " + parameter.name + ". Adaptation was disabled for this variable");
                                                        parameter.grids[0].adapt.maxAdaptSteps = 0;
                                                }
                                        }
                                }
                        }
                }
        }
        if (inputFaulty)
        {
                throw std::runtime_error("Input to Mido had faults that could not be corrected. See the DYOS_OUT error log for more details.");
        }
}

//@brief  Runs Dyos on the specified output as if no discrete variables where present.
UserOutput::Output runDyosOnLocalRootRelaxation(const UserInput::MidoInput &input, const Logger::Ptr &logger)
{
        //calculate first node of branch and bound, where all integers are still unfixed and relax all of them
        UserInput::Input startNodeInput = input.convertToInput();

        UserOutput::Output dyosOut = runDyos(startNodeInput, logger);
        UserOutput::Output dyosOutOpt;
        //solutionHistory is where solves that are not incumbents are saved
        //the dyosOut itself is for an incumbent
        dyosOutOpt.solutionHistory.push_back(dyosOut);
        return dyosOutOpt;
}

/// @brief Initializes the mapping between babBase format and Dyos format

/// @return a ParamIndex mapping and a vetor of optimization variables
///
/// Extracts all control parameters ( more precisely  the control parameterization for each interval) for all discrete controls.
/// Initializes the ParamIndex, which makes it possible to convert from the position in the vector of babBase::OptimizationVariables to the corresponding
//  Stage, the corresponding position in stageInput.parameters and the corresponding interval for PROFILE type controls.
//  Also fills the vector of optimization variables with informations abount the variables (e.g. bounds)
std::pair<ParamIndex, std::vector<babBase::OptimizationVariable>> initializeOptimizationVariablesAndIndex(const UserInput::MidoInput &input)
{
        ParamIndex paramIndex;
        std::vector<babBase::OptimizationVariable> integerVariables;
        unsigned parameterCouter = 0;

        for (unsigned stage = 0; stage < input.stages.size(); stage++)
                for (unsigned variableNum = 0; variableNum < input.stages[stage].integrator.parameters.size(); variableNum++)
                {
                        const UserInput::MidoParameterInput &parameter = input.stages[stage].integrator.parameters[variableNum];
                        //only save discrete values in the vector, since only they are currently to be branched on
                        //also ignore variables with sensType=NO, those are also not branched on.
                        if (parameter.getDiscreteType() != UserInput::MidoParameterInput::CONTINUOUS && parameter.sensType != UserInput::MidoParameterInput::NO)
                        {

                                if (parameter.paramType == UserInput::MidoParameterInput::TIME_INVARIANT)
                                {
                                        integerVariables.push_back(babBase::OptimizationVariable(babBase::Bounds(parameter.lowerBound, parameter.upperBound), babBase::enums::VT::VT_INTEGER, true, parameter.name));
                                        paramIndex.insert(parameterCouter++, stage, variableNum, true, 0);
                                }
                                else
                                {
                                        assert(parameter.paramType == UserInput::MidoParameterInput::PROFILE);

                                        //Control profiles having a grid size >0 is also assumed in Dyos
                                        //Have to check when multiple grids are necessary and what they mean                    assert(parameter.grids.size()==1);
                                        assert(parameter.grids[0].type == UserInput::ParameterGridInput::PIECEWISE_CONSTANT);
                                        for (unsigned gridInterval = 0; gridInterval < parameter.grids[0].numIntervals; gridInterval++)
                                        {
                                                double lowerBound = parameter.lowerBound;
                                                double upperBound = parameter.upperBound;
                                                assert(parameter.grids[0].lowerBounds.size() == parameter.grids[0].upperBounds.size());
                                                if (parameter.grids[0].lowerBounds.size() > 0) //if individual lowerBounds are given
                                                {
                                                        lowerBound = parameter.grids[0].lowerBounds[gridInterval];
                                                        upperBound = parameter.grids[0].upperBounds[gridInterval];
                                                }
                                                integerVariables.push_back(babBase::OptimizationVariable(babBase::Bounds(lowerBound, upperBound), babBase::enums::VT::VT_INTEGER, true, parameter.name));
                                                paramIndex.insert(parameterCouter++, stage, variableNum, false, gridInterval);
                                        }
                                }
                        }
                }

        return std::make_pair(paramIndex, integerVariables);
}

/// A small struct used to define the result of an evaluation.
struct RunResult
{
        double objVal;
        bool feasibleConstraint;
        bool feasibleInteger;
        UserOutput::Output userOutput;
};

///@brief Small helper function used in the heuristics, since they often want to evaluate a fixed point. And the evaluation is expensive so we want to cache it.
///       An evaluation is only prescribing the values for variabes treated by branch and bound (currently only discrete variables).
///       Other variables need to be determined by the continous solver (Dyos).
RunResult evaluateFixedPointAndSaveEvaluation(const std::vector<double> &x_eval, const babBase::BabNode &node,
                                              const UserInput::MidoInput &input, const std::vector<babBase::OptimizationVariable> &variables,
                                              const ParamIndex &paramIndex, const Logger::Ptr &logger, babBase::OutVar<SaveEvalMap> evalSave_)
{
        SaveEvalMap &evalSave          = evalSave_;
        SaveEvalMap::const_iterator it = evalSave.find(x_eval);
        if (it != evalSave.end())
        {
                return it->second;
        }
        else
        {
                babBase::BabNode node_copy = node;

                node_copy.set_lower_bound(x_eval);
                node_copy.set_upper_bound(x_eval);

                bool isIntegerFeasible;
                bool isConstraintFeasible;

                double objValue;
                UserOutput::Output userOutput;

                //not possible to just do integrate only, because we only operate only on the non continous part of the variables. The continous are 'hidden' from the babBase interface
                //-> solver must still  optimize those. If branching is also done on continous variables, this should be changed!
                std::tie(std::ignore, objValue, std::ignore, isIntegerFeasible, isConstraintFeasible, userOutput) = processNode(node_copy, variables, paramIndex, input, logger);

                RunResult runResult{ objValue, isConstraintFeasible,  isIntegerFeasible,  userOutput};
                evalSave.insert(std::make_pair(x_eval, runResult));
                return runResult;
        }
}

///@brief Runs the specified starting heuristic from the given start point.
///
///@return Returns the tuple (successfull,resultPoint,objValue,UserOutput). Successfull is true if we found an integer feasible and constraint feasible solution.
///        If we return true, the result point is filled with the found point and objValue with the obtained objective value and the user Output is the UserOutput of the evaluation of that point
///        If we return false,the result point is some point (but not empty) and objValue is INFINITY, while the UserOutput is default intialized. I.e. no useful information is returned other than the
///        failure of the heuristic.
std::tuple<bool /*successfull*/, std::vector<double> /*resultPoint*/, double /*objValue*/, UserOutput::Output> runStartingHeuristic(UserInput::MidoInput::StartHeuristic heuristicType, const std::vector<double> &x_start, const UserInput::MidoInput &input, const babBase::BabNode &node, const ParamIndex paramIndex, std::vector<babBase::OptimizationVariable> &variables, const Logger::Ptr &logger)
{
        babBase::HeuristicProblemInterface interface;
        std::vector<bool> isInteger(variables.size());

        SaveEvalMap evalSave;
        for (unsigned i = 0; i < variables.size(); i++)
        {
                isInteger[i] = (variables[i].get_variable_type() == babBase::enums::VT::VT_BINARY || variables[i].get_variable_type() == babBase::enums::VT_INTEGER);
        }
        interface.get_is_integer             = [isInteger]() { return isInteger; };
        interface.solve_continous_relaxation = [&input, &variables, &paramIndex, &logger, &evalSave](const std::vector<double> x_start, const babBase::BabNode &node) {
                //std::tuple<babBase::BabNode, double/*objValue*/, std::vector<double>/*solPoint*/, bool /*isIntegerFeasible*/, bool /*isConstraintFeasible*/, struct UserOutput::Output>
                bool isIntegerFeasible;
                bool isConstraintFeasible;
                std::vector<double> solutionPoint;
                double objValue;

                std::tie(std::ignore, objValue, solutionPoint, isIntegerFeasible, isConstraintFeasible, std::ignore) = processNode(node, variables, paramIndex, input, logger);
                evalSave.insert(std::make_pair(solutionPoint, RunResult{objValue, isConstraintFeasible, 
                  isIntegerFeasible}));
                return std::make_pair(solutionPoint, objValue);
        };
        interface.get_obj_value = [&input, &variables, &paramIndex, &logger, &evalSave](const std::vector<double> &x_eval, const babBase::BabNode &node) {
                RunResult result = evaluateFixedPointAndSaveEvaluation(x_eval, node, input, variables, paramIndex, logger, babBase::out_par(evalSave));
                return result.objVal;
        };

        interface.is_feasible = [&input, &variables, &paramIndex, &logger, &evalSave](const std::vector<double> &x_eval, const babBase::BabNode &node, bool ignoreIntegralityConstraints) {
                RunResult result = evaluateFixedPointAndSaveEvaluation(x_eval, node, input, variables, paramIndex, logger, babBase::out_par(evalSave));

                return result.feasibleConstraint && (ignoreIntegralityConstraints || result.feasibleInteger);
        };
        std::shared_ptr<babBase::RoundingHeuristic> heuristic;
        switch (heuristicType)
        {

                case UserInput::MidoInput::StartHeuristic::NONE:
                {
                        // return std::make_pair(/*successfull*/ false, startPoint);
                        break;
                }
                case UserInput::MidoInput::StartHeuristic::DIVING:
                {
                        //handeled in runDyos;
                }
                case UserInput::MidoInput::StartHeuristic::MPEC_DIVING:
                {
                        std::function<std::vector<double>(std::vector<double>, const std::vector<double> &, double, double, const babBase::BabNode &)> solveMPECPenaltyProblem =
                            [&input, &variables, &paramIndex, &logger](std::vector<double> start, const std::vector<double> &antigoalsforInteger, double oldObjectiveFactor, double penaltyFactor, const babBase::BabNode &node) {
                                    UserInput::MidoInput inputWithPenalty = addQuadraticPenaltyToStepInput(input, variables, antigoalsforInteger, oldObjectiveFactor, -1.0 * penaltyFactor, paramIndex);
                                    std::vector<double> solutionPoint;
                                    std::tie(std::ignore, std::ignore, solutionPoint, std::ignore, std::ignore, std::ignore) = processNode(node, variables, paramIndex, inputWithPenalty, logger);
                                    return solutionPoint;
                            };
                        heuristic = std::make_shared<babBase::FractionalDivingRoundingHeuristic>(solveMPECPenaltyProblem);
                        break;
                }
                case UserInput::MidoInput::StartHeuristic::FEASIBILITY_PUMP:
                {
                        std::function<std::vector<double>(std::vector<double>, const std::vector<double> &, const babBase::BabNode &, double)> solveFeasibilityPumpProblem =
                            [&input, &variables, &paramIndex, &logger](std::vector<double> start, const std::vector<double> &goalsForInteger, const babBase::BabNode &node, double alpha) {
                                    UserInput::MidoInput inputWithPenalty = addQuadraticPenaltyToStepInput(input, variables, goalsForInteger, alpha, (1 - alpha), paramIndex);
                                    std::vector<double> solutionPoint;
                                    std::tie(std::ignore, std::ignore, solutionPoint, std::ignore, std::ignore, std::ignore) = processNode(node, variables, paramIndex, inputWithPenalty, logger);
                                    return solutionPoint;
                            };
                        heuristic = std::make_shared<babBase::FeasibilityPumpRoundingHeuristic>(solveFeasibilityPumpProblem);
                        break;
                }
                case UserInput::MidoInput::StartHeuristic::MPEC:
                {
                        std::function<std::vector<double>(std::vector<double>, const std::vector<double> &, double, double, const babBase::BabNode &)> solveMPECPenaltyProblem =
                            [&input, &variables, &paramIndex, &logger](std::vector<double> start, const std::vector<double> &antigoalsforInteger, double oldObjectiveFactor, double penaltyFactor, const babBase::BabNode &node) {
                                    UserInput::MidoInput inputWithPenalty = addQuadraticPenaltyToStepInput(input, variables, antigoalsforInteger, oldObjectiveFactor, -1.0 * penaltyFactor, paramIndex);
                                    std::vector<double> solutionPoint;
                                    std::tie(std::ignore, std::ignore, solutionPoint, std::ignore, std::ignore, std::ignore) = processNode(node, variables, paramIndex, inputWithPenalty, logger);
                                    return solutionPoint;
                            };
                        heuristic = std::make_shared<babBase::MPECRoundingHeuristic>(solveMPECPenaltyProblem);
                        break;
                }
        }
        if (heuristic)
        {
                bool succesfull;
                std::vector<double> solutionPoint;
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Starting  heuristic search for integer feasible points.");
                logger->flushAll();
                std::tie(succesfull, solutionPoint) = heuristic->run(x_start, node, interface, std::min(10, input.getIterationLimit()), input.getTimeLimit());
                logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Heuristic search for integer feasible points was successfull:" + std::to_string(succesfull));
                RunResult res = evaluateFixedPointAndSaveEvaluation(solutionPoint, node, input, variables, paramIndex, logger, babBase::out_par(evalSave));
                return std::make_tuple(res.feasibleConstraint && res.feasibleInteger, solutionPoint, res.objVal, res.userOutput);
        }
        else
        {
                return std::make_tuple(/*successful*/ false, x_start, std::numeric_limits<double>::infinity(), UserOutput::Output());
        }
}
