/**
* @file MidoUserInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mido                                                                 \n
* =====================================================================\n
* This file contains the enhanced input struct for Mido users. The     \n
* parameter struct gets the additional type ParameterDiscrete Type     \n
* and Mido options are added to the toplevel input struct.             \n
* =====================================================================\n
* @author Aron Zingler
* @date 12.06.2019
* @sa UserInput.hpp
*/

#pragma once

#include "UserInput.hpp"
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <limits>

#ifdef WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

#undef LINKDLL
#if WIN32
#ifdef MAKE_MIDO_DLL
#define LINKDLL __declspec(dllexport)
#else
#define LINKDLL __declspec(dllimport)
#endif
#else
#define LINKDLL
#endif

//namespace documented in UserInput.hpp
namespace UserInput
{
  /// Class that expands the ParameterInput class by the functions setBinary and setFixedTo
  class MidoParameterInput:protected ParameterInput //To disable implicit conversion
  {
    public:
    //Make parts of ParameterInput public
    using ParameterInput::name;
    using ParameterInput::lowerBound;
    using ParameterInput::upperBound;
    using ParameterInput::value;
    using ParameterInput::grids;

    using ParameterInput::DURATION;
    using ParameterInput::FRACTIONAL;
    using ParameterInput::FULL;
    using ParameterInput::INITIAL;
    using ParameterInput::NO;
    using ParameterInput::PROFILE;
    using ParameterInput::TIME_INVARIANT;
    //Ideally these two should be private and only be able to set in constructor
    //This would make sure they are used consistent
    //But would change the way users have to initialize
    using ParameterInput::paramType;
    using ParameterInput::sensType;

    enum ParameterDiscreteType
    {
        CONTINUOUS,    // the variable is an ordinary continuous variable
        INTEGER
    };

    MidoParameterInput(){parameterDiscreteType=CONTINUOUS;};

    /// Maybe should contain some checks on bounds and value
    const ParameterInput& convertToParameterInput()const & {return  *this;}; //copy
    const ParameterInput&& convertToParameterInput()&&{return std::move(*this);}; //move

    LINKDLL void setInteger();


    ParameterDiscreteType getDiscreteType()const{return parameterDiscreteType;};

    private:
    ParameterDiscreteType parameterDiscreteType;



  };
  struct MidoStageIntegratorInput: protected  IntegratorStageInput
  {
    using IntegratorStageInput::duration;
    using IntegratorStageInput::explicitPlotGrid;
    using IntegratorStageInput::plotGridResolution;
    std::vector<UserInput::MidoParameterInput> parameters;
    IntegratorStageInput convertToIntegratorStageInput() const &;
     IntegratorStageInput&& convertToIntegratorStageInput()&&;//move

  };
  struct MidoStageInput: protected StageInput //To disable implicit conversion
  {
    //make parts of protected StageInput public
    using StageInput::treatObjective;
    using StageInput::mapping;
    using StageInput::eso;
    using StageInput::esoPtr;
    using StageInput::optimizer;
    //using StageInput::integrator;
    MidoStageIntegratorInput integrator;

    StageInput  convertToStageInput()const &;
    StageInput&& convertToStageInput()&&;//move

  };

  /**
  * @struct MidoInput
  * @brief information provided by user to create a GenericEso object
  */
  struct MidoInput : private Input//To disable implicit conversion
  {




	std::vector<MidoStageInput> stages;

    enum class NodeSelectionStrategy
	{ DEPTH_FIRST_SEARCH,
	  BREADTH_FIRST_SEARCH,
	  BEST_FIRST_SEARCH,
	  BEST_EXPECTED
	};
	enum class BranchingStrategy
	{
		FRACTIONAL,
		RELIABILITY
	};
    enum class StartHeuristic
	{
		NONE,
		DIVING,
		FEASIBILITY_PUMP,
		MPEC,
		MPEC_DIVING
	};




    Input convertToInput()const &;
    Input&& convertToInput()&&;//Allow moving

    //tolerances with their default values (taken from MAiNGO)
    double relTolForIntegralityConstraint{1e-4};
    double absTolForIntegralityConstraint{1e-6};
    double relTolForOptimalityGap{1e-4};
    double absTolForOptimalityGap{1e-6};
	LINKDLL void enableTerminationConditionTimeLimit(double secondsToRun);
    void enableTerminationConditionIterationLimit( int maxIterations);
	void enableTerminationConditionFoundFeasibleSolution();
	void enableTerminationConditionObjectiveFunctionLowerThan( double lowerBound);
	void enableAssumptionDyosFindsGlobalSolution();
    double getTimeLimit() const{return secondsTimeLimit;};
    int    getIterationLimit() const{return iterationLimit;};
    bool   isConvexAssumed() const {return assumeConvex;};
    bool   isStoppingAfterFirstFeasibleEnabled() const {return stopAfterFindingFirstFeasibleSolution;};
    double getDefaultLowerBound() const {return defaultLowerBound;};

	//Allow access to these parts of input
    using Input::finalIntegration;
    using Input::integratorInput;
    using Input::optimizerInput;
    using Input::runningMode;
    using Input::totalEndTimeLowerBound;
    using Input::totalEndTimeUpperBound;
	NodeSelectionStrategy nodeSelectionStrategy{NodeSelectionStrategy::BEST_FIRST_SEARCH};
	BranchingStrategy variableBranchingStrategy{BranchingStrategy::FRACTIONAL};
	StartHeuristic startHeuristic{StartHeuristic::NONE};
    //Further Options with their defaults.
private:
	//Terminations
    int iterationLimit{std::numeric_limits<int>::max()};
	double secondsTimeLimit{std::numeric_limits<int>::max()};
	bool stopAfterFindingFirstFeasibleSolution{false};
    double defaultLowerBound{-std::numeric_limits<double>::infinity()};
	//Assumptions
	bool assumeConvex{false};
	bool assumeStickingInteger{false};

	//Start Heuristics
	bool useFeabilityPump{false};
	bool useFractionalDiving{false};
	bool useMpec{false};

  };


}




