/**
* @file MidoDyos.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mido                                                                 \n
* =====================================================================\n
* This file contains the Dyos interface for Mido users. The interface  \n
* contains an enhanced user input struct.                              \n
* =====================================================================\n
* @author Tjalf Hoffmann, Aron Zingler
* @date 14.9.2019
*/

/**
 * To make it easier to understand the implementation of Mido, an overview
over the most important components and their interaction is given in the
following.


We will group the used code into the MidoInput part, which extends the
UserInput with additional options for the user to specify to control the
behaviour of the branch and bound algorithm, the babBase dependency
which manages the branch and bound tree and the actual Mido code
framework that solves the dynamic optimization problem with discrete
variables.

First, we have the MidoUserInput part (MidoUserInput.hpp). Here we
extend the classes from UserInput with additional options. We do this by
deriving from the original UserInput classes with the access modifier
protected, but forwarding most of the members of the base classes. This
way, we can use the functionality of the base classes without code
duplication. We also ensure that conversion from MidoUserInput to
UserInput is not done implicitly, but by calling a designated conversion
function, which lets us customize the behavior of the conversion, e.g.
by introducing checks.

Second, we have the babBase code base , a library that handles the
details of the branch and bound algorithm, e.g. selecting which node to
process next, how and where to branch on nodes, keeping track of all
open nodes and the current incumbent. The main interface is through the
babBase::Brancher class. It is important to note that in babBase the
optimization variables are organized in a flat array. This is in
contrast with the hierarchical structure in Dyos, where a stage may have
multiple integer parameters which each can have multiple intervals if
they are time-variant.

It is therefore necessary to convert between those two notations. For
this purpose we will use the ParamIndex struct (MidoDyosUtils.hpp).
After initialization, this index will assign each discrete degree of
freedom (one for the whole parameter for time-invariant parameters or
one for each interval for a time-variant parameter) to one position in
an array of optimization variables.

The main functionality is located in midoDyos.cpp. It includes some
helper functions, but the most important logic is located in the
functions optimizeMido, createMidoStepInput and runDyos.

runDyos is an overload of the normal runDyos function, which is called
with MidoUserInput. It checks the input given by the user and tries to
change non critical flaws in the input, like disabeling grid adaptation
on integer parameters. It also performs initialization (e.g. Brancher
and ParamIndex) and triggers the execution of the start heuristics. It
than calls optimizeMido for the actual branch and bound iterations.

optimizeMido implements the branch and bound logic with heavy
utilization of the babBase library. Until termination is triggered, it
will receive a node from the Brancher and process it. Afterwards, it may
discard an infeasible node or direct the Brancher to branch on this node.

The main part in processing a node is to solve the continuous
relaxation. To this end the function createMidoStepInput will create a
UserInput for the 'normal' Dyos by using the current node and a copy of
the MidoUserInput that was supplied by the user. The bounds of all
variables in MidoUserInput that are saved in paramIndex (discrete
degrees of freedom) are set to the bounds from the node and the
MidoUserInput is converted to a UserInput by using the explicit
conversion function.
*/
#pragma once
#include "MidoUserInput.hpp"
#include "UserOutput.hpp"
#include "Logger.hpp"

/**
* @def LINKDLL
* @brief defines whether a function is to be imported or exported
*/
#undef LINKDLL
#if WIN32
#ifdef MAKE_MIDO_DLL
#define MIDO_LINKDLL __declspec(dllexport)
#else
#define MIDO_LINKDLL __declspec(dllimport)
#endif
#else
#define MIDO_LINKDLL
#endif



/**
* @brief main routine of midos branch and bound
*
* @param[in] input input containing complete input
* @param[in] logger the logger to hand message and logging information to.
* @return result of the branch and bound algorithm. If no integer feasible solution was found. The return is the last calculated relaxation.
* In this case optimizerOutput.resultFlag is set to FAILED, or if we can be sure the problem is infeasible to INFEASIBE. The objVal is also set to INF regardless of
* the objective value of the last relaxation. The evaluated points (outside of evaluations from heuristics) are saved in the solutionHistory field.
*/
MIDO_LINKDLL UserOutput::Output runDyos(const struct UserInput::MidoInput &input);
MIDO_LINKDLL UserOutput::Output runDyos( struct UserInput::MidoInput input,
                                                 const Logger::Ptr &logger);
