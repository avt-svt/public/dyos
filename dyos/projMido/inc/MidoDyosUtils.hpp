/**
* @file MidoDyosUtils.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mido                                                                 \n
* =====================================================================\n
* This file contains a struct that is used to map the pair of (stage,parameterNumber) of the UserInput\n
* to the position in the OptimizationVariable Vector used in babBase  \n
* =====================================================================\n
* @author Aron Zingler
* @date 12.06.2019
* @sa UserInput.hpp
*/


#include <unordered_map>
#include <utility>
#include <vector>
/*
namespace std
{
template<>
struct hash<pair<unsigned, unsigned>>
{
    inline size_t operator()(const pair<unsigned, unsigned> & v) const
    {
        //Szudzik's function
        if(v.first>v.second)
        {
            return v.first*v.first+v.first+v.second;
        }
        else
        {
            return v.first+v.second*v.second;
        }
    }
};
}
*/


inline void hash_combine(std::size_t& seed, double const& v)
{
    seed ^= std::hash<double>()(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}
namespace std
{
   template<>
    struct hash<std::vector<double>>
    {
        typedef vector<double> argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& in) const
        {
            size_t size = in.size();
            size_t seed = 0;
            for (size_t i = 0; i < size; i++)
                //Combine the hash of the current vector with the hashes of the previous ones
                hash_combine(seed, in[i]);
            return seed;
        }
    };
}


struct ParamIndex
{
    std::unordered_map<unsigned,std::tuple<unsigned,unsigned,bool,unsigned>> mapping;
    /// @brief Saves the fact that the positonInOptimizationVariableVector-th variable in the OptimizationVariable-vector used by babBase refers to the parameterNum-th variable in the stage-th stage.
    ///If the parameter is not time-invariant, the also saves the fact that it refers to a certain interval of that parameter
    void insert(unsigned positonInOptimizationVariableVector,unsigned stage, unsigned parameterNum, bool parameterTimeInvariant, unsigned interval)
    {
        if(parameterTimeInvariant) assert(interval==0);
        mapping[positonInOptimizationVariableVector]=std::make_tuple(stage,parameterNum,parameterTimeInvariant,interval);
    }
    std::tuple<unsigned/*stage*/,unsigned /*parameterNum*/, bool /*isTimeInvariant*/, unsigned /*interval*/ >
    getFromOptimizationVariablePosition(unsigned positonInOptimizationVariableVector) const
    {
        return mapping.at(positonInOptimizationVariableVector);
    }

};

