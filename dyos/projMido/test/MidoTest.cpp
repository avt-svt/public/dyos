#include "MidoDyos.hpp"
#include <utility>
#include <string>
#include <cassert>

#define BOOST_TEST_MODULE TestMido
#include "boost/test/unit_test.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>




//to compile and run the problem, the correct header and dlls must be available
//Changes to "normal" Dyos: Use MidoInput and MidoStageInput and MidoParameterInput and
//for declare binary variables by calling setBinary() on them.
//Use   stage1.integrator.parameters.push_back(ybin3d2).
//Use   input.stages.push_back(stage1).

//This has a constructor that prepares the basic settings for the testcases using the disjunctive model.
struct FixtureDisjunctive
{
 UserInput::MidoInput input;
 UserInput::MidoStageInput stage1;
 UserInput::MidoParameterInput ybin1d1, ybin1d2, ybin2d1, ybin2d2, ybin3d1, ybin3d2;
 OutputChannel::Ptr stdOut;
 Logger::Ptr logger;
 UserOutput::Output output;
 FixtureDisjunctive():stdOut(new FileChannel("MyFile.txt")),logger(new StandardLogger())
 {
    input.optimizerInput.optimizerOptions["print_level"]="0";

    stage1.eso.type = EsoType::JADE;
    stage1.eso.model = "disjunctive";

    stage1.integrator.duration.lowerBound = 1.0;
    stage1.integrator.duration.upperBound = 1.0;
    stage1.integrator.duration.value = 1.0;
    stage1.integrator.duration.sensType = UserInput::MidoParameterInput::NO;
    stage1.integrator.duration.paramType = UserInput::MidoParameterInput::DURATION;


    UserInput::MidoParameterInput p;
    p.name = "p";
    p.lowerBound = -4.0;
    p.upperBound = 4.0;
    p.value = 4.0;
    p.paramType = UserInput::MidoParameterInput::TIME_INVARIANT;
    p.sensType = UserInput::MidoParameterInput::FULL;
    stage1.integrator.parameters.push_back(p);


    UserInput::MidoParameterInput x10d1 = p;




    x10d1.name = "x10d1";
    x10d1.lowerBound = -30.0;
    x10d1.upperBound = 30.0;
    x10d1.value = 2.0;

    stage1.integrator.parameters.push_back(x10d1);

    UserInput::MidoParameterInput x10d2 = x10d1, x20d1=x10d1, x20d2=x10d1, x30d1=x10d1, x30d2=x10d1;
    x10d2.name = "x10d2";
    x20d1.name = "x20d1";
    x20d2.name = "x20d2";
    x30d1.name = "x30d1";
    x30d2.name = "x30d2";
    stage1.integrator.parameters.push_back(x10d2);
    stage1.integrator.parameters.push_back(x20d1);
    stage1.integrator.parameters.push_back(x20d2);
    stage1.integrator.parameters.push_back(x30d1);
    stage1.integrator.parameters.push_back(x30d2);

    UserInput::MidoParameterInput xobj11 = x10d1, xobj12=x10d1, xobj21=x10d1, xobj22=x10d1, xobj31=x10d1, xobj32=x10d1;
    xobj11.name = "xobj11";
    xobj12.name = "xobj12";
    xobj21.name = "xobj21";
    xobj22.name = "xobj22";
    xobj31.name = "xobj31";
    xobj32.name = "xobj32";
    stage1.integrator.parameters.push_back(xobj11);
    stage1.integrator.parameters.push_back(xobj12);
    stage1.integrator.parameters.push_back(xobj21);
    stage1.integrator.parameters.push_back(xobj22);
    stage1.integrator.parameters.push_back(xobj31);
    stage1.integrator.parameters.push_back(xobj32);



    stage1.optimizer.objective.name = "xobj";

    UserInput::ConstraintInput constr1_lo;
    constr1_lo.name = "constr1_lo";
    constr1_lo.lowerBound = -2000.0;
    constr1_lo.upperBound = 0.0;
    constr1_lo.type = UserInput::ConstraintInput::ENDPOINT;
    stage1.optimizer.constraints.push_back(constr1_lo);

    UserInput::ConstraintInput constr1_up = constr1_lo;
    constr1_up.name = "constr1_up";
    constr1_up.lowerBound = 0.0;
    constr1_up.upperBound = 2000.0;
    stage1.optimizer.constraints.push_back(constr1_up);

    UserInput::ConstraintInput constr2_lo = constr1_lo, constr2_up = constr1_up;
    constr2_lo.name = "constr2_lo";
    constr2_up.name = "constr2_up";
    stage1.optimizer.constraints.push_back(constr2_lo);
    stage1.optimizer.constraints.push_back(constr2_up);

    UserInput::ConstraintInput constr3_lo = constr1_lo, constr3_up = constr1_up;
    constr3_lo.name = "constr3_lo";
    constr3_up.name = "constr3_up";
    stage1.optimizer.constraints.push_back(constr3_lo);
    stage1.optimizer.constraints.push_back(constr3_up);

    UserInput::ConstraintInput constr4_lo = constr1_lo, constr4_up = constr1_up;
    constr4_lo.name = "constr4_lo";
    constr4_up.name = "constr4_up";
    stage1.optimizer.constraints.push_back(constr4_lo);
    stage1.optimizer.constraints.push_back(constr4_up);

    UserInput::ConstraintInput constr5_lo = constr1_lo, constr5_up = constr1_up;
    constr5_lo.name = "constr5_lo";
    constr5_up.name = "constr5_up";
    stage1.optimizer.constraints.push_back(constr5_lo);
    stage1.optimizer.constraints.push_back(constr5_up);

    UserInput::ConstraintInput constr6_lo = constr1_lo, constr6_up = constr1_up;
    constr6_lo.name = "constr6_lo";
    constr6_up.name = "constr6_up";
    stage1.optimizer.constraints.push_back(constr6_lo);
    stage1.optimizer.constraints.push_back(constr6_up);

    UserInput::ConstraintInput constr7_lo = constr1_lo, constr7_up = constr1_up;
    constr7_lo.name = "constr7_lo";
    constr7_up.name = "constr7_up";
    stage1.optimizer.constraints.push_back(constr7_lo);
    stage1.optimizer.constraints.push_back(constr7_up);

    UserInput::ConstraintInput constr8_lo = constr1_lo, constr8_up = constr1_up;
    constr8_lo.name = "constr8_lo";
    constr8_up.name = "constr8_up";
    stage1.optimizer.constraints.push_back(constr8_lo);
    stage1.optimizer.constraints.push_back(constr8_up);

    UserInput::ConstraintInput constr9_lo = constr1_lo, constr9_up = constr1_up;
    constr9_lo.name = "constr9_lo";
    constr9_up.name = "constr9_up";
    stage1.optimizer.constraints.push_back(constr9_lo);
    stage1.optimizer.constraints.push_back(constr9_up);

    UserInput::ConstraintInput constr11_lo = constr1_lo, constr11_up = constr1_up;
    constr11_lo.name = "constr11_lo";
    constr11_up.name = "constr11_up";
    stage1.optimizer.constraints.push_back(constr11_lo);
    stage1.optimizer.constraints.push_back(constr11_up);

    UserInput::ConstraintInput constr13_lo = constr1_lo, constr13_up = constr1_up;
    constr13_lo.name = "constr13_lo";
    constr13_up.name = "constr13_up";
    stage1.optimizer.constraints.push_back(constr13_lo);
    stage1.optimizer.constraints.push_back(constr13_up);

    UserInput::ConstraintInput constr15_lo = constr1_lo, constr15_up = constr1_up;
    constr15_lo.name = "constr15_lo";
    constr15_up.name = "constr15_up";
    stage1.optimizer.constraints.push_back(constr15_lo);
    stage1.optimizer.constraints.push_back(constr15_up);

    UserInput::ConstraintInput obj1_lo = constr1_lo, obj1_up = constr1_up;
    obj1_lo.name = "obj1_lo";
    obj1_up.name = "obj1_up";
    stage1.optimizer.constraints.push_back(obj1_lo);
    stage1.optimizer.constraints.push_back(obj1_up);

    UserInput::ConstraintInput obj2_lo = constr1_lo, obj2_up = constr1_up;
    obj2_lo.name = "obj2_lo";
    obj2_up.name = "obj2_up";
    stage1.optimizer.constraints.push_back(obj2_lo);
    stage1.optimizer.constraints.push_back(obj2_up);

    UserInput::ConstraintInput obj3_lo = constr1_lo, obj3_up = constr1_up;
    obj3_lo.name = "obj3_lo";
    obj3_up.name = "obj3_up";
    stage1.optimizer.constraints.push_back(obj3_lo);
    stage1.optimizer.constraints.push_back(obj3_up);

    UserInput::ConstraintInput obj4_lo = constr1_lo, obj4_up = constr1_up;
    obj4_lo.name = "obj4_lo";
    obj4_up.name = "obj4_up";
    stage1.optimizer.constraints.push_back(obj4_lo);
    stage1.optimizer.constraints.push_back(obj4_up);

    UserInput::ConstraintInput obj5_lo = constr1_lo, obj5_up = constr1_up;
    obj5_lo.name = "obj5_lo";
    obj5_up.name = "obj5_up";
    stage1.optimizer.constraints.push_back(obj5_lo);
    stage1.optimizer.constraints.push_back(obj5_up);

    UserInput::ConstraintInput obj6_lo = constr1_lo, obj6_up = constr1_up;
    obj6_lo.name = "obj6_lo";
    obj6_up.name = "obj6_up";
    stage1.optimizer.constraints.push_back(obj6_lo);
    stage1.optimizer.constraints.push_back(obj6_up);

    UserInput::ConstraintInput obj7_lo = constr1_lo, obj7_up = constr1_up;
    obj7_lo.name = "obj7_lo";
    obj7_up.name = "obj7_up";
    stage1.optimizer.constraints.push_back(obj7_lo);
    stage1.optimizer.constraints.push_back(obj7_up);

    UserInput::ConstraintInput obj8_lo = constr1_lo, obj8_up = constr1_up;
    obj8_lo.name = "obj8_lo";
    obj8_up.name = "obj8_up";
    stage1.optimizer.constraints.push_back(obj8_lo);
    stage1.optimizer.constraints.push_back(obj8_up);

    UserInput::ConstraintInput obj9_lo = constr1_lo, obj9_up = constr1_up;
    obj9_lo.name = "obj9_lo";
    obj9_up.name = "obj9_up";
    stage1.optimizer.constraints.push_back(obj9_lo);
    stage1.optimizer.constraints.push_back(obj9_up);

    UserInput::ConstraintInput obj10_lo = constr1_lo, obj10_up = constr1_up;
    obj10_lo.name = "obj10_lo";
    obj10_up.name = "obj10_up";
    stage1.optimizer.constraints.push_back(obj10_lo);
    stage1.optimizer.constraints.push_back(obj10_up);

    UserInput::ConstraintInput obj11_lo = constr1_lo, obj11_up = constr1_up;
    obj11_lo.name = "obj11_lo";
    obj11_up.name = "obj11_up";
    stage1.optimizer.constraints.push_back(obj11_lo);
    stage1.optimizer.constraints.push_back(obj11_up);

    UserInput::ConstraintInput obj12_lo = constr1_lo, obj12_up = constr1_up;
    obj12_lo.name = "obj12_lo";
    obj12_up.name = "obj12_up";
    stage1.optimizer.constraints.push_back(obj12_lo);
    stage1.optimizer.constraints.push_back(obj12_up);

    stage1.treatObjective = true;
    ///input.stages.push_back(stage1);




    input.runningMode = UserInput::Input::SINGLE_SHOOTING;
    input.integratorInput.type = UserInput::IntegratorInput::NIXE;
    input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
    input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;



 
    ybin1d1.name = "ybin1d1";
    ybin1d1.value = 1.0;
    ybin1d1.upperBound=1.0;
    ybin1d1.lowerBound=0.0;
    ybin1d1.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    ybin1d1.setInteger();
    ybin1d2.name = "ybin1d2";
    ybin1d2.value = 0.0;
    ybin1d2.upperBound=1.0;
    ybin1d2.lowerBound=0.0;
    ybin1d2.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    ybin1d2.setInteger();
    ybin2d1.name = "ybin2d1";
    ybin2d1.value = 1.0;
    ybin2d1.upperBound=1.0;
    ybin2d1.lowerBound=0.0;
    ybin2d1.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    ybin2d1.setInteger();
    ybin2d2.name = "ybin2d2";
    ybin2d2.value = 0.0;
    ybin2d2.upperBound=1.0;
    ybin2d2.lowerBound=0.0;
    ybin2d2.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    ybin2d2.setInteger();
    ybin3d1.name = "ybin3d1";
    ybin3d1.value = 1.0;
    ybin3d1.upperBound=1.0;
    ybin3d1.lowerBound=0.0;
    ybin3d1.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    ybin3d1.setInteger();
    ybin3d2.name = "ybin3d2";
    ybin3d2.value = 0.0;
    ybin3d2.upperBound=1.1;
    ybin3d2.lowerBound=0.0;
    ybin3d2.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    ybin3d2.setInteger();
    
    
    
    
  OutputChannel::Ptr consoleOut(new StandardOut());
  consoleOut->setVerbosity(OutputChannel::ERROR_VERBOSITY);
  stdOut->setVerbosity(OutputChannel::STANDARD_VERBOSITY);
  MultiChannel::Ptr multiChannel(new MultiChannel());
  multiChannel->addChannel(consoleOut);
  multiChannel->addChannel(stdOut);
  //stdOut->setVerbosity(OutputChannel::ERROR_VERBOSITY);
  logger->setLoggingChannel(Logger::LoggingChannel::OPTIMIZER,stdOut);
  logger->setLoggingChannel(Logger::LoggingChannel::DYOS_OUT,multiChannel);
  logger->setLoggingChannel(Logger::LoggingChannel::ESO,stdOut);

 }
};
BOOST_AUTO_TEST_SUITE(TestsuiteMido)
BOOST_FIXTURE_TEST_SUITE( TestsuiteMidoDisjunctive, FixtureDisjunctive )

#ifdef BUILD_EXPENSIVE_TESTS
//#Expensive
BOOST_AUTO_TEST_CASE( TestDisjunctiveStandard )
{


  
  stage1.integrator.parameters.push_back(ybin1d1);
  stage1.integrator.parameters.push_back(ybin1d2);
  stage1.integrator.parameters.push_back(ybin2d1);
  stage1.integrator.parameters.push_back(ybin2d2);
  stage1.integrator.parameters.push_back(ybin3d1);
  stage1.integrator.parameters.push_back(ybin3d2);

  input.stages.push_back(stage1);

  
  
  //test enumeration
  output = runDyos(input,logger);
  logger->flushAll();
  
  //make sure full tree with 2^(n+1)-1 nodes is searched, since no node can be discarded without a lower bounding LowerBoundInitilizationStrategy
  BOOST_TEST(output.solutionHistory.size()==127);

  BOOST_TEST(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);
  //solution should be 0
  BOOST_TEST(std::abs(output.optimizerOutput.objVal)<= 1E-6);
}

//#Expensive
BOOST_AUTO_TEST_CASE( TestDisjunctiveOptions)
{
  stage1.integrator.parameters.push_back(ybin1d1);
  stage1.integrator.parameters.push_back(ybin1d2);
  stage1.integrator.parameters.push_back(ybin2d1);
  stage1.integrator.parameters.push_back(ybin2d2);
  stage1.integrator.parameters.push_back(ybin3d1);
  stage1.integrator.parameters.push_back(ybin3d2);

  input.stages.push_back(stage1);
  
 
  
  UserInput::MidoInput input_copy=input;
  //test with enabled convexity assumption
  input.enableAssumptionDyosFindsGlobalSolution();
  output = runDyos(input,logger);
  BOOST_TEST(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);
  
  BOOST_TEST(output.solutionHistory.size()<=40);//40 is only a current value from experience!
  BOOST_TEST(std::abs(output.optimizerOutput.objVal)<= 1E-6);
  
  //test iteration limit with convexity assumption
  input.enableTerminationConditionIterationLimit(10);
  output = runDyos(input,logger);
  //one more, since iterations are babIterations and 1 of the saved solutions is the 
  //solution from the lower bound initialization that is activated by assumeConvex
  BOOST_TEST(output.solutionHistory.size()==11);

  //test iteration limit without convexity assumption
  input=input_copy;
  input.enableTerminationConditionIterationLimit(10);
  output = runDyos(input,logger);
  BOOST_TEST(output.solutionHistory.size()==10);

  //test time limit
  input.enableTerminationConditionTimeLimit(0 /*seconds*/);
  output = runDyos(input,logger);
  //in theory more than 2 iterations could be done, if the running PC is very fast
  BOOST_TEST(output.solutionHistory.size()<=1);
  //check if output was created even when there was no time to solve problems
  std::string name;
  BOOST_CHECK_NO_THROW(name=output.stages[0].integrator.parameters[18].name);
  
}
#endif
//#Inexpensive
BOOST_AUTO_TEST_CASE( TestDisjunctiveAlwaysOutput)
{
stage1.integrator.parameters.push_back(ybin1d1);
stage1.integrator.parameters.push_back(ybin1d2);
stage1.integrator.parameters.push_back(ybin2d1);
stage1.integrator.parameters.push_back(ybin2d2);
stage1.integrator.parameters.push_back(ybin3d1);
stage1.integrator.parameters.push_back(ybin3d2);

input.stages.push_back(stage1);
input.enableTerminationConditionTimeLimit(0 /*seconds*/);

output = runDyos(input,logger);
//in theory more than 2 iterations could be done, if the running PC is very fast
BOOST_TEST(output.solutionHistory.size()==1);
//check if output was created even when there was no time to solve problems
std::string name;
BOOST_CHECK_NO_THROW(name=output.stages[0].integrator.parameters[18].name);

//even when enabling heuristic
input.startHeuristic=UserInput::MidoInput::StartHeuristic::FEASIBILITY_PUMP;
output = runDyos(input,logger);
//in theory more than 2 iterations could be done, if the running PC is very fast
BOOST_TEST(output.solutionHistory.size()==1);
//check if output was created even when there was no time to solve problems
BOOST_CHECK_NO_THROW(name=output.stages[0].integrator.parameters[18].name);
}

#ifdef BUILD_EXPENSIVE_TESTS
//#Expensive
//Test the diving heuristic
BOOST_AUTO_TEST_CASE(TestDisjunctiveDiving)
{
  stage1.integrator.parameters.push_back(ybin1d1);
  stage1.integrator.parameters.push_back(ybin1d2);
  stage1.integrator.parameters.push_back(ybin2d1);
  stage1.integrator.parameters.push_back(ybin2d2);
  stage1.integrator.parameters.push_back(ybin3d1);
  stage1.integrator.parameters.push_back(ybin3d2);

  input.stages.push_back(stage1);
  input.startHeuristic=UserInput::MidoInput::StartHeuristic::DIVING;
  input.enableAssumptionDyosFindsGlobalSolution();
  
  output=runDyos(input,logger);
  BOOST_TEST(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);
  BOOST_TEST(std::abs(output.optimizerOutput.objVal)<= 1E-6);
  
  
  
  input.enableTerminationConditionIterationLimit(10);
  input.enableAssumptionDyosFindsGlobalSolution();
  output = runDyos(input,logger);
  
  BOOST_TEST(output.solutionHistory.size()==10);// as long as no lower bounding procedure is implemented, the algorithm should always take exactly 10 iterations. Because of the relaxation solved in the beginning, caused by assumeConvex, exactly one more solution should be in the history. (Iterations are BaB iterations).
  
}
//#Expensive
//test time varyng integers
BOOST_AUTO_TEST_CASE(TestDisjunctiveTimeVarying)
{
    
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE;
    UserInput::ParameterGridInput gridx;
    gridx.type=UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    gridx.numIntervals=2;
    gridx.lowerBounds= {0,0};
    gridx.upperBounds= {1,2};
    ybin1d1.setInteger(); //Here setInteger is called before ybin1d1 gets its grid.
    //As a consequence the checks in setIntegers are not carried out.
    //This caused an error in a previous version, because values is not initialized and the initialization of 
    //values to value if values is empty was done in setInteger. Now Mido also checks this.
    ybin1d1.grids.push_back(gridx);
    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    output = runDyos(input,logger);
    BOOST_TEST(output.solutionHistory.size()==383);// Assume ybin1d1(2)=[0,2] is split first to [0] and [1,2].
    //Then we would have to root + the [1,2] node and three open nodes ybin1d1=0,1,2 which each expands to the a tree of size 127 (2^(6+1)-1) -> 2+3*(127)=383
    
}
//#Expensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveHeuristic)
{
    UserInput::MidoParameterInput  y1d2, y2d1, y2d2, y3d1;
    y1d2.name = "ybin1d2";
    y1d2.value = 0.0;
    y1d2.upperBound=1.0;
    y1d2.lowerBound=0.0;
    y1d2.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
    y2d2=y3d1=y2d1=y1d2; // disable integer for all but two, to make problem faster
    y2d2.name = "ybin2d2";
    y3d1.name = "ybin3d1";
    y2d1.name = "ybin2d1";
    
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE; 
    UserInput::ParameterGridInput gridx;
    gridx.type=UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    gridx.numIntervals=2;
    gridx.lowerBounds= {0,0};
    gridx.upperBounds= {1,2};
    ybin1d1.grids.push_back(gridx);
    int indexOfVariabeWithGrid=stage1.integrator.parameters.size();
    //two grids not supported!
    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(y1d2);
    stage1.integrator.parameters.push_back(y2d1);
    stage1.integrator.parameters.push_back(y2d2);
    stage1.integrator.parameters.push_back(y3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    input.enableTerminationConditionFoundFeasibleSolution();
    input.enableTerminationConditionIterationLimit(4);//limit iterations so all heuristics can be checked without excessive time investment
    for(UserInput::MidoInput::StartHeuristic a :{UserInput::MidoInput::StartHeuristic::MPEC,UserInput::MidoInput::StartHeuristic::MPEC_DIVING,UserInput::MidoInput::StartHeuristic::FEASIBILITY_PUMP})
    {
      input.startHeuristic=a;
      BOOST_CHECK_NO_THROW(output=runDyos(input,logger));
      BOOST_TEST(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);
    }
    input.enableTerminationConditionIterationLimit(15);
    output=runDyos( input,logger);
    BOOST_TEST(output.optimizerOutput.resultFlag==UserOutput::OptimizerOutput::OK);

    BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters[indexOfVariabeWithGrid].grids[0].values[0],0.0,0.1); //the last known solution to get found at that iteration limit
    BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters[indexOfVariabeWithGrid].grids[0].values[1],0.0,0.1); //other interger solutions are also viable
    BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters[indexOfVariabeWithGrid+5].value,0.0,0.1);
    BOOST_TEST(output.solutionHistory.size()<=7);//just from previos runs, not fix.
    BOOST_TEST(std::abs(output.optimizerOutput.objVal-(-60.0))<= 0.1);

    
}
#endif
//#Inexpensive
//test that ensures that sensType=No leads to a variable to be excluded from branching
BOOST_AUTO_TEST_CASE(TestDisjunctiveSensTypeNoMeansNoBranching)
{
    ybin1d1.sensType=UserInput::MidoParameterInput::NO;
    stage1.integrator.parameters.push_back(ybin1d1);
    ybin1d2.sensType=ybin1d1.sensType;
    stage1.integrator.parameters.push_back(ybin1d2);
    ybin2d1.sensType=ybin1d1.sensType;
    stage1.integrator.parameters.push_back(ybin2d1);
    ybin2d2.sensType=ybin1d1.sensType;
    stage1.integrator.parameters.push_back(ybin2d2);
    ybin3d1.sensType=ybin1d1.sensType;
    stage1.integrator.parameters.push_back(ybin3d1);
    ybin3d2.sensType=ybin1d1.sensType;
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    
    output=runDyos(input,logger);
    BOOST_TEST(output.solutionHistory.size()==1);
}
//test throwing at wrong inputs
//#Inexpensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveThrowMaximize)
{
    
    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);  
    input.optimizerInput.optimizationMode = UserInput::OptimizerInput::MAXIMIZE;
    input.enableTerminationConditionTimeLimit(1 /*seconds*/);
    BOOST_CHECK_THROW(runDyos(input,logger),std::runtime_error);
    input.optimizerInput.optimizationMode = UserInput::OptimizerInput::MINIMIZE;
    
}
//test throwing at wrong inputs
//#Inexpensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveThrowNoGrid)
{
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE; 
    //NOTE: has no grid!
    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    input.enableTerminationConditionTimeLimit(1 /*seconds*/);
    BOOST_CHECK_THROW(runDyos(input,logger),std::runtime_error);

    
}
//test throwing at wrong inputs
//#Inexpensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveThrowTwoGrids)
{
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE; 
    UserInput::ParameterGridInput gridx;
    gridx.type=UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    gridx.numIntervals=2;
    gridx.lowerBounds= {0,0};
    gridx.upperBounds= {1,2};
    ybin1d1.grids.push_back(gridx);
    ybin1d1.grids.push_back(gridx);
    //two grids not supported!
    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    input.enableTerminationConditionTimeLimit(1 /*seconds*/);
    BOOST_CHECK_THROW(runDyos(input,logger),std::runtime_error);

    
}
//test throwing at wrong inputs
//#Inexpensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveThrowPiecewiseLinear)
{
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE; 
    UserInput::ParameterGridInput gridx;
    gridx.type=UserInput::ParameterGridInput::PIECEWISE_LINEAR;
    gridx.numIntervals=2;
    gridx.lowerBounds= {0,0};
    gridx.upperBounds= {1,2};
    ybin1d1.grids.push_back(gridx);

    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    input.enableTerminationConditionTimeLimit(1 /*seconds*/);
    BOOST_CHECK_THROW(runDyos(input,logger),std::runtime_error);

    
}
//test throwing at wrong inputs
//#Inexpensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveThrowOnlyWarningStructureDetectionInStageWithDiscrete)
{
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE; 
    UserInput::ParameterGridInput gridx;
    gridx.type=UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    gridx.numIntervals=2;
    gridx.lowerBounds= {0,0};
    gridx.upperBounds= {1,2};
    ybin1d1.grids.push_back(gridx);

    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    stage1.optimizer.structureDetection.maxStructureSteps=3;
    input.stages.push_back(stage1);
    stage1.optimizer.structureDetection.maxStructureSteps=3;
    input.stages.push_back(stage1);
    input.optimizerInput.adaptationOptions.adaptStrategy=UserInput::AdaptationOptions::ADAPT_STRUCTURE;
    input.enableTerminationConditionTimeLimit(15 /*seconds*/);
    //gets adjusted
    BOOST_CHECK_NO_THROW(runDyos(input,logger));
    
}

#ifdef BUILD_EXPENSIVE_TESTS
//test throwing at wrong inputs
//#Expensive
BOOST_AUTO_TEST_CASE(TestDisjunctiveDontThrowStructureDetectionInStageWithoutDiscrete)
{
    ybin1d1.paramType=UserInput::MidoParameterInput::PROFILE; 
    UserInput::ParameterGridInput gridx;
    gridx.type=UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
    gridx.numIntervals=2;
    gridx.lowerBounds= {0,0};
    gridx.upperBounds= {1,2};
    ybin1d1.grids.push_back(gridx);
    UserInput::MidoStageInput stage2=stage1;
    stage1.integrator.parameters.push_back(ybin1d1);
    stage1.integrator.parameters.push_back(ybin1d2);
    stage1.integrator.parameters.push_back(ybin2d1);
    stage1.integrator.parameters.push_back(ybin2d2);
    stage1.integrator.parameters.push_back(ybin3d1);
    stage1.integrator.parameters.push_back(ybin3d2);

    input.stages.push_back(stage1);
    stage1.optimizer.structureDetection.maxStructureSteps=0;
    UserInput::MidoParameterInput  y1d1,y1d2, y2d1, y2d2, y3d1,y3d2;
       y1d2.name = "ybin1d2";
       y1d2.value = 0.0;
       y1d2.upperBound=1.0;
       y1d2.lowerBound=0.0;
       y1d2.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
       y1d1=y3d2=y2d2=y3d1=y2d1=y1d2; // disable integer so we can have structure detection
       y2d2.name = "ybin2d2";
       y3d1.name = "ybin3d1";
       y2d1.name = "ybin2d1";
       y1d1.name = "ybin1d1";
       y3d2.name = "ybin3d2";
       stage2.integrator.parameters.push_back(y1d1);
       stage2.integrator.parameters.push_back(y1d2);
       stage2.integrator.parameters.push_back(y2d1);
       stage2.integrator.parameters.push_back(y2d2);
       stage2.integrator.parameters.push_back(y3d1);
       stage2.integrator.parameters.push_back(y3d2);
       stage2.optimizer.structureDetection.maxStructureSteps=5;
    input.stages.push_back(stage2);
    input.optimizerInput.adaptationOptions.adaptStrategy=UserInput::AdaptationOptions::ADAPT_STRUCTURE;
    input.enableTerminationConditionTimeLimit(30);
    input.enableTerminationConditionIterationLimit(2);
    //No warning expected
    BOOST_CHECK_NO_THROW(runDyos(input,logger));
    
}
#endif
BOOST_AUTO_TEST_SUITE_END();
//test of B&B
//trigger of the following test:
//if DyOS with relaxed variables gives optimizer with exactly one non-binary variable the
//binaries have been fixed in the following branching in old version of optimizeMido()
//-> This may cut off  parts of the tree containing the global optimum
//test case:
//global optimum is [bin1,bin2]=[0,0] whereas relaxed optimization gives [bin1,bin2]=[0.555,1]
//thus bin2 may be fixed wrongly to 1

#ifdef BUILD_EXPENSIVE_TESTS
////#Expensive
BOOST_AUTO_TEST_CASE(TestBB_fixBinaries)
{
  UserInput::MidoInput input;
  UserInput::MidoStageInput stage1;

  // integrator input
  stage1.eso.type = UserInput::EsoInput::JADE;
  stage1.eso.model = "testBB_fixBinaries";

  stage1.integrator.duration.lowerBound = 10.0;
  stage1.integrator.duration.upperBound = 10.0;
  stage1.integrator.duration.value = 10.0;
  stage1.integrator.duration.sensType =  UserInput::MidoParameterInput::NO;
  stage1.integrator.duration.paramType = UserInput::MidoParameterInput::DURATION;

  UserInput::MidoParameterInput control;
  control.name = "u";
  control.lowerBound = 0.0;
  control.upperBound = 1.0;
  control.paramType = UserInput::MidoParameterInput::PROFILE;
  control.sensType = UserInput::MidoParameterInput::FULL;

  UserInput::ParameterGridInput grid;
  const unsigned numIntervals = 5;
  grid.numIntervals = numIntervals;
  grid.values.resize(numIntervals, 0);
  grid.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  control.grids.push_back(grid);

  stage1.integrator.parameters.push_back(control);

  // optimizer input
  stage1.optimizer.objective.name = "obj";
  stage1.optimizer.objective.lowerBound = -10.0;
  stage1.optimizer.objective.upperBound = 50.0;

  UserInput::ConstraintInput xPathConstr;
  xPathConstr.name = "x";
  xPathConstr.type = UserInput::ConstraintInput::PATH;
  xPathConstr.lowerBound = -20.0;
  xPathConstr.upperBound =  20.0;
  stage1.optimizer.constraints.push_back(xPathConstr);

  // general stage settings
  stage1.mapping.fullStateMapping = false;
  stage1.treatObjective = true;

  // solution methods
  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;

  // Mido settings
  UserInput::MidoParameterInput bin1, bin2;
  bin1.name = "bin1";
  bin1.value = 0.0;
  bin1.upperBound=1.0;
  bin1.lowerBound=0.0;
  bin1.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
  bin1.setInteger();
  bin2.name = "bin2";
  bin2.value = 1.0;
  bin2.upperBound=1.0;
  bin2.lowerBound=0.0;
  bin2.paramType=UserInput::MidoParameterInput::TIME_INVARIANT;
  bin2.setInteger();

  
  
  stage1.integrator.parameters.push_back(bin1);
  stage1.integrator.parameters.push_back(bin2);

  input.stages.push_back(stage1);

  // output
  UserOutput::Output output;
  input.enableAssumptionDyosFindsGlobalSolution();
  input.absTolForIntegralityConstraint=0;
  input.relTolForIntegralityConstraint=1e-8;
  output = runDyos(input);

  //check global optimum
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  BOOST_CHECK_EQUAL(output.stages[0].integrator.parameters[1].value, 0); // bin1 == 0
  BOOST_CHECK_EQUAL(output.stages[0].integrator.parameters[2].value, 0); // bin2 == 0
  BOOST_CHECK_CLOSE(output.optimizerOutput.objVal, 0.0, 1E-6);			 // obj  == 0
  //check relaxed optimum (first run)
  BOOST_CHECK_CLOSE(output.solutionHistory[0].stages[0].integrator.parameters[1].value, 0.55555556, 1E-6); // bin1 = 0.555555...
  BOOST_CHECK_CLOSE(output.solutionHistory[0].stages[0].integrator.parameters[2].value, 1.0, 1E-6);        // bin2 = 1
}
#endif
BOOST_AUTO_TEST_SUITE_END();

