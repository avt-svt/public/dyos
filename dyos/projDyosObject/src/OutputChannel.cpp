/**
* @file OutputChannel.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DyosObject                                                           \n
* =====================================================================\n
* This file contains the definition of the OutputChannel class         \n
* The OutputChannel redirects an ostream to a certain target           \n
* used by the Logger class                                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 28.11.2012
*/

#include "OutputChannel.hpp"

/**
* @brief standard constructor setting verbosity to STANDARD
*/
OutputChannel::OutputChannel()
{
  m_verbosity = STANDARD_VERBOSITY;
}


void OutputChannel::setVerbosity(VerbosityLevel verbosity)
{
  m_verbosity = verbosity;
}
/**
* @brief print message to output channel if verbosityLevel is high enough
* @param message message to be printed
* @param verbosityLevel verbosity of the message. If the level is equal or higher
*                       than the objects verbosity, then the message is printed.
*                       The highest verbosity level is error, the lowest is debug
*/
void OutputChannel::print(const std::string &message,const VerbosityLevel verbosityLevel)
{
  if(verbosityLevel >= m_verbosity){
    print(message);
  }
}

/**
* @brief print message to cout
* @param message message to be printed
*/
void StandardOut::print(const std::string &message)
{
  std::cout<<message;
}

void StandardOut::flush()
{
  std::cout.flush();
}

StandardErr::StandardErr()
{
  m_verbosity = OutputChannel::ERROR_VERBOSITY;
}

/**
* @brief print message to cerr
* @param message message to be printed
*/
void StandardErr::print(const std::string &message)
{
  std::cerr<<message;
}

void StandardErr::flush()
{
  std::cerr.flush();
}

/**
* @brief print message to clog
* @param message message to be printed
*/
void StandardLog::print(const std::string &message)
{
  std::clog<<message;
}

void StandardLog::flush()
{
  std::clog.flush();
}


/**
* @brief constructor of FileChannel class opening given file
* @param filename string containing the file to be used by this FileChannel object
*/
FileChannel::FileChannel(const std::string &filename) 
{
  m_file.open(filename.c_str(), std::ios_base::out);
}

/**
* @brief destructor closing the file associated with this FileChannel object
*/
FileChannel::~FileChannel()
{
  m_file.close();
}

/**
* @brief print message to associated file
* @param message message to be printed
*/
void FileChannel::print(const std::string &message)
{
  m_file<<message;
}

void FileChannel::flush()
{
  m_file.flush();
}


/**
* @brief add an OutputChannel object to the list of OutputChannels
* @param newChannel pointer to OutputChannel object that is to be added
*/
void MultiChannel::addChannel(const OutputChannel::Ptr &newChannel)
{
  m_channel.push_back(newChannel);
}

/**
* @brief this function is unused by MultiChannel
*/
void MultiChannel::print(const std::string &message)
{}

/**
* @brief print message all output channels in the list
* @param message message to be printed
*/
void MultiChannel::print(const std::string &message,
                         const OutputChannel::VerbosityLevel verbosityLevel)
{
  if(verbosityLevel >= m_verbosity){
    for(unsigned i=0; i<m_channel.size(); i++){
      m_channel[i]->print(message, verbosityLevel);
    }
  }
}

void MultiChannel::flush()
{
  for(unsigned i=0; i<m_channel.size(); i++){
    m_channel[i]->flush();
  }
}
