model wobatchinactive
 constant Integer n_x = 9 annotation(Evaluate=true);
constant Real Tin = 35 annotation(Evaluate=true);
constant Real b2 = 6.6667 annotation(Evaluate=true);
constant Real FbinMin = 0 annotation(Evaluate=true);
constant Real FbinMax = 5.784 annotation(Evaluate=true);
constant Real TwMin = 0.02 annotation(Evaluate=true);
constant Real TwMax = 0.1 annotation(Evaluate=true);
input Real TwCur( start=0.1);
input Real FbinCur( start=5.784);
parameter Real TMin = 60 annotation(Evaluate=true);
parameter Real FinalTime = 1 annotation(Evaluate=true);

Real X1(start = 1);
Real X2(start = 0);
Real X3(start = 0);
Real X4(start = 0);
Real X5(start = 0);
Real X6(start = 0);
Real X7(start = 65.0);
Real X8(start = 2);
Real X9(start = 0);

Real Fbin;
output Real Tw;
Real derX7;
initial equation
 X1 = 1;
 X2 = 0;
 X3 = 0;
 X4 = 0;
 X5 = 0;
 X6 = 0;
 X7 = 65.0;
 X8 = 2;
 X9 = 0;

equation
der(X1) =  -FinalTime*(X1*Fbin/(1000.0*X8)+1659900.0*exp( -1000.0*b2/(
  273.15+X7))*X1*X2);
der(X2) = FinalTime*(Fbin*(1-X2)/(1000.0*X8)-(1659900.0*exp( -1000.0*b2/(
  273.15+X7))*X1*X2+721170000.0*exp( -8333.3/(273.15+X7))*X2*X3));
der(X3) = FinalTime*(3319800.0*exp( -1000.0*b2/(273.15+X7))*X1*X2-
  1442340000.0*exp( -8333.3/(273.15+X7))*X2*X3-2674500000000.0*exp( -
  11111.0/(273.15+X7))*X3*X4-X3*Fbin/(1000.0*X8));
der(X4) = FinalTime*(721170000.0*exp( -8333.3/(273.15+X7))*X2*X3-
  1337250000000.0*exp( -11111.0/(273.15+X7))*X3*X4-X4*Fbin/(1000.0*X8));
der(X5) = FinalTime*(1442340000.0*exp( -8333.3/(273.15+X7))*X2*X3-X5*
  Fbin/(1000.0*X8));
der(X6) = FinalTime*(4011750000000.0*exp( -11111.0/(273.15+X7))*X4*X3-X6
  *Fbin/(1000.0*X8));
der(X8) = 0.001*FinalTime*Fbin;
der(X9) = FinalTime*((-5554.1)*(721170000.0*exp( -8333.3/(273.15+X7))*X2*X3
  -1337250000000.0*exp( -11111.0/(273.15+X7))*X3*X4)*X8-
  181605029400.0*exp( -8333.3/(273.15+X7))*X2*X3*X8);
derX7 = FinalTime*(Fbin*Tin/(1000*X8)+0.000239005736137667*(437881620000.0*exp(
    -1000.0*b2/(273.15+X7))*X1*X2+114161211000000.0*exp( -8333.3/(273.15+X7))
  *X2*X3+6.0523935E+017*exp( -11111.0/(273.15+X7))*X3*X4)-
  0.000243454685774894*(X7-1000.0*Tw)-Fbin*X7/(1000*X8));
Fbin = FbinCur;
Tw = TwCur;
der(X7) = derX7;

annotation(
    __OpenModelica_simulationFlags(jacobian = "symbolical", lv = "LOG_STATS", s = "dassl"),
    experiment(StartTime = 0, StopTime = 1, Tolerance = 1e-6, Interval = 0.002));end wobatchinactive;
