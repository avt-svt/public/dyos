#include "wobatch2_t2a1_eval.hpp"

template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<0>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<1>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<2>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<3>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<4>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<5>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<6>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<7>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<8>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<9>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<10>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void wobatch2Model<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<11>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;

