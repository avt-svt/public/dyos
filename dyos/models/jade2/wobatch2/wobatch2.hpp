#ifndef wobatch2_MODEL_HPP
#define wobatch2_MODEL_HPP
#include "Eso/StandardGroups.hpp"
#include "Eigen/SparseCore"
#include <vector>
#include <string>
#include <cmath>
#include <memory>

#ifdef _MSC_BUILD
#pragma warning(push) 
#pragma warning(disable : 4060)
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


template<typename RealType>
class wobatch2Model {
public:
  template<int Exponent>
  static RealType integerPower(const RealType& x){
    RealType result = static_cast<RealType>(1.);
    for(int i=0; i< std::abs(Exponent);++i) {
      result *= x;
    }
    if(Exponent >= 0)
      return result;
    else {
      return static_cast<RealType>(1.0)/result;
    }
  }


  template<int Exponent>
  static RealType integerPowerDerivative(const RealType& x){
    if(Exponent == 0) return 0;
    RealType result = static_cast<RealType>(1.);
    for(int i=0; i< std::abs(Exponent-1);++i) {
      result *= x;
    }
    if(Exponent >= 0)
      return static_cast<RealType>(Exponent)*result;
    else {
      return static_cast<RealType>(Exponent)/result;
    }
  }


  template<int Exponent>
  static RealType integerPowerHessian(const RealType& x){
    if(Exponent == 0) return 0;
    if(Exponent == 1) return 0;
    RealType result = static_cast<RealType>(1.);
    for(int i=0; i< std::abs(Exponent-2);++i) {
      result *= x;
    }
    if(Exponent >= 0)
      return static_cast<RealType>(Exponent * (Exponent-1))*result;
    else {
      return static_cast<RealType>(Exponent * (Exponent-1))/result;
    }
}

  static constexpr int numEquations(const DiffEqns) { return 9;}
  static constexpr int numEquations(const AlgEqns) { return 3;}
  static constexpr int numEquations(const AllEqns) { return 12;}
  static constexpr int numVariables(DiffVars) { return 9;}
  static constexpr int numVariables(AlgVars) { return 3;}
  static constexpr int numVariables(TimeVar) { return 0;}
  static constexpr int numVariables(ParamVars) { return 4;}
  static constexpr int numVariables(AllVars) {return 25;}
  static constexpr int numVariables(StateVars) { return 12; }
  static constexpr int numVariables(DerDiffVars) {return 9; }
  static constexpr int numVariables(StateTimeVars) { return numVariables(StateVars{}) +  numVariables(TimeVar{});}
  static constexpr int numVariables(StateTimeParamVars) { return numVariables(StateTimeVars{}) +  numVariables(ParamVars{});}

  RealType eval(AllEqns, EqIndex<0>) const;
  RealType eval(AllEqns, EqIndex<1>) const;
  RealType eval(AllEqns, EqIndex<2>) const;
  RealType eval(AllEqns, EqIndex<3>) const;
  RealType eval(AllEqns, EqIndex<4>) const;
  RealType eval(AllEqns, EqIndex<5>) const;
  RealType eval(AllEqns, EqIndex<6>) const;
  RealType eval(AllEqns, EqIndex<7>) const;
  RealType eval(AllEqns, EqIndex<8>) const;
  RealType eval(AllEqns, EqIndex<9>) const;
  RealType eval(AllEqns, EqIndex<10>) const;
  RealType eval(AllEqns, EqIndex<11>) const;
  

  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<0>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<1>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<2>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<3>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<4>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<5>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<6>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<7>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<8>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<9>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<10>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<11>, const Tangents& tangents) const;
  

  template<class Tangents, class TangentResiduals>
  void evalAllTangents(const Tangents& tangents, TangentResiduals& tangentResiduals)
  {
    int i = 0;
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<0>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<1>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<2>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<3>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<4>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<5>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<6>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<7>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<8>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<9>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<10>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<11>{}, tangents);

  }

  
  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<0>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    RealType v1 = x(0);
    RealType v2 = y(0);
    RealType v3 = v1*v2;
    const RealType v4 = static_cast<RealType>(1000.0);
    RealType v5 = x(7);
    RealType v6 = v4*v5;
    RealType v7 = v3/v6;
    const RealType v8 = static_cast<RealType>(1659900.0);
    const RealType v9 = static_cast<RealType>(1000.0);
    RealType v10 = c(2);
    RealType v11 = v9*v10;
    const RealType v12 = static_cast<RealType>(273.15);
    RealType v13 = x(6);
    RealType v14 = v12+v13;
    RealType v15 = v11/v14;
    RealType v16 = -v15;
    RealType v17 = exp(v16);
    RealType v18 = v8*v17;
    RealType v19 = v18*v1;
    RealType v20 = x(1);
    RealType v21 = v19*v20;
    RealType v22 = v7+v21;
    RealType v23 = v0*v22;
    RealType v24 = -v23;
    RealType v25 = der_x(0);
    RealType v26 = v24-v25;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = static_cast<RealType>(0.);
    RealType a1_v25 = static_cast<RealType>(0.);
    RealType a1_v26 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},0);
    a1_v25 -= a1_v26;
    a1_v24 += a1_v26;
    a1_v23 -= a1_v24;
    a1_v0 += a1_v23 * v22;
    a1_v22 += a1_v23 * v0;
    a1_v7 += a1_v22;
    a1_v21 += a1_v22;
    a1_v19 += a1_v21 * v20;
    a1_v20 += a1_v21 * v19;
    a1_v18 += a1_v19 * v1;
    a1_v1 += a1_v19 * v18;
    a1_v8 += a1_v18 * v17;
    a1_v17 += a1_v18 * v8;
    a1_v16 += a1_v17 * exp(v16);
    a1_v15 -= a1_v16;
    RealType tmp_a1_v15 = a1_v15 / v14;
    a1_v11 += tmp_a1_v15;
    a1_v14 -= v11 * tmp_a1_v15 / v14;
    a1_v12 += a1_v14;
    a1_v13 += a1_v14;
    a1_v12 = static_cast<RealType>(0.);
    a1_v9 += a1_v11 * v10;
    a1_v10 += a1_v11 * v9;
    a1_v9 = static_cast<RealType>(0.);
    a1_v8 = static_cast<RealType>(0.);
    RealType tmp_a1_v7 = a1_v7 / v6;
    a1_v3 += tmp_a1_v7;
    a1_v6 -= v3 * tmp_a1_v7 / v6;
    a1_v4 += a1_v6 * v5;
    a1_v5 += a1_v6 * v4;
    a1_v4 = static_cast<RealType>(0.);
    a1_v1 += a1_v3 * v2;
    a1_v2 += a1_v3 * v1;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v1;
        gradient[startIndex++] = a1_v20;
        gradient[startIndex++] = a1_v13;
        gradient[startIndex++] = a1_v5;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v2;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v25;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[0-offs] += a1_v1;
        gradient[1-offs] += a1_v20;
        gradient[6-offs] += a1_v13;
        gradient[7-offs] += a1_v5;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v2;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[16-offs] += a1_v25;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<1>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    RealType v1 = y(0);
    const RealType v2 = static_cast<RealType>(1);
    RealType v3 = x(1);
    RealType v4 = v2-v3;
    RealType v5 = v1*v4;
    const RealType v6 = static_cast<RealType>(1000.0);
    RealType v7 = x(7);
    RealType v8 = v6*v7;
    RealType v9 = v5/v8;
    const RealType v10 = static_cast<RealType>(1659900.0);
    const RealType v11 = static_cast<RealType>(1000.0);
    RealType v12 = c(2);
    RealType v13 = v11*v12;
    const RealType v14 = static_cast<RealType>(273.15);
    RealType v15 = x(6);
    RealType v16 = v14+v15;
    RealType v17 = v13/v16;
    RealType v18 = -v17;
    RealType v19 = exp(v18);
    RealType v20 = v10*v19;
    RealType v21 = x(0);
    RealType v22 = v20*v21;
    RealType v23 = v22*v3;
    const RealType v24 = static_cast<RealType>(721170000.0);
    const RealType v25 = static_cast<RealType>(8333.3);
    const RealType v26 = static_cast<RealType>(273.15);
    RealType v27 = v26+v15;
    RealType v28 = v25/v27;
    RealType v29 = -v28;
    RealType v30 = exp(v29);
    RealType v31 = v24*v30;
    RealType v32 = v31*v3;
    RealType v33 = x(2);
    RealType v34 = v32*v33;
    RealType v35 = v23+v34;
    RealType v36 = v9-v35;
    RealType v37 = v0*v36;
    RealType v38 = der_x(1);
    RealType v39 = v37-v38;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = static_cast<RealType>(0.);
    RealType a1_v25 = static_cast<RealType>(0.);
    RealType a1_v26 = static_cast<RealType>(0.);
    RealType a1_v27 = static_cast<RealType>(0.);
    RealType a1_v28 = static_cast<RealType>(0.);
    RealType a1_v29 = static_cast<RealType>(0.);
    RealType a1_v30 = static_cast<RealType>(0.);
    RealType a1_v31 = static_cast<RealType>(0.);
    RealType a1_v32 = static_cast<RealType>(0.);
    RealType a1_v33 = static_cast<RealType>(0.);
    RealType a1_v34 = static_cast<RealType>(0.);
    RealType a1_v35 = static_cast<RealType>(0.);
    RealType a1_v36 = static_cast<RealType>(0.);
    RealType a1_v37 = static_cast<RealType>(0.);
    RealType a1_v38 = static_cast<RealType>(0.);
    RealType a1_v39 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},1);
    a1_v38 -= a1_v39;
    a1_v37 += a1_v39;
    a1_v0 += a1_v37 * v36;
    a1_v36 += a1_v37 * v0;
    a1_v9 += a1_v36;
    a1_v35 -= a1_v36;
    a1_v23 += a1_v35;
    a1_v34 += a1_v35;
    a1_v32 += a1_v34 * v33;
    a1_v33 += a1_v34 * v32;
    a1_v31 += a1_v32 * v3;
    a1_v3 += a1_v32 * v31;
    a1_v24 += a1_v31 * v30;
    a1_v30 += a1_v31 * v24;
    a1_v29 += a1_v30 * exp(v29);
    a1_v28 -= a1_v29;
    RealType tmp_a1_v28 = a1_v28 / v27;
    a1_v25 += tmp_a1_v28;
    a1_v27 -= v25 * tmp_a1_v28 / v27;
    a1_v26 += a1_v27;
    a1_v15 += a1_v27;
    a1_v26 = static_cast<RealType>(0.);
    a1_v25 = static_cast<RealType>(0.);
    a1_v24 = static_cast<RealType>(0.);
    a1_v22 += a1_v23 * v3;
    a1_v3 += a1_v23 * v22;
    a1_v20 += a1_v22 * v21;
    a1_v21 += a1_v22 * v20;
    a1_v10 += a1_v20 * v19;
    a1_v19 += a1_v20 * v10;
    a1_v18 += a1_v19 * exp(v18);
    a1_v17 -= a1_v18;
    RealType tmp_a1_v17 = a1_v17 / v16;
    a1_v13 += tmp_a1_v17;
    a1_v16 -= v13 * tmp_a1_v17 / v16;
    a1_v14 += a1_v16;
    a1_v15 += a1_v16;
    a1_v14 = static_cast<RealType>(0.);
    a1_v11 += a1_v13 * v12;
    a1_v12 += a1_v13 * v11;
    a1_v11 = static_cast<RealType>(0.);
    a1_v10 = static_cast<RealType>(0.);
    RealType tmp_a1_v9 = a1_v9 / v8;
    a1_v5 += tmp_a1_v9;
    a1_v8 -= v5 * tmp_a1_v9 / v8;
    a1_v6 += a1_v8 * v7;
    a1_v7 += a1_v8 * v6;
    a1_v6 = static_cast<RealType>(0.);
    a1_v1 += a1_v5 * v4;
    a1_v4 += a1_v5 * v1;
    a1_v2 += a1_v4;
    a1_v3 -= a1_v4;
    a1_v2 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v21;
        gradient[startIndex++] = a1_v3;
        gradient[startIndex++] = a1_v33;
        gradient[startIndex++] = a1_v15;
        gradient[startIndex++] = a1_v7;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v1;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v38;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[0-offs] += a1_v21;
        gradient[1-offs] += a1_v3;
        gradient[2-offs] += a1_v33;
        gradient[6-offs] += a1_v15;
        gradient[7-offs] += a1_v7;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v1;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[17-offs] += a1_v38;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<2>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(3319800.0);
    const RealType v2 = static_cast<RealType>(1000.0);
    RealType v3 = c(2);
    RealType v4 = v2*v3;
    const RealType v5 = static_cast<RealType>(273.15);
    RealType v6 = x(6);
    RealType v7 = v5+v6;
    RealType v8 = v4/v7;
    RealType v9 = -v8;
    RealType v10 = exp(v9);
    RealType v11 = v1*v10;
    RealType v12 = x(0);
    RealType v13 = v11*v12;
    RealType v14 = x(1);
    RealType v15 = v13*v14;
    const RealType v16 = static_cast<RealType>(1442340000.0);
    const RealType v17 = static_cast<RealType>(8333.3);
    const RealType v18 = static_cast<RealType>(273.15);
    RealType v19 = v18+v6;
    RealType v20 = v17/v19;
    RealType v21 = -v20;
    RealType v22 = exp(v21);
    RealType v23 = v16*v22;
    RealType v24 = v23*v14;
    RealType v25 = x(2);
    RealType v26 = v24*v25;
    RealType v27 = v15-v26;
    const RealType v28 = static_cast<RealType>(2674500000000.0);
    const RealType v29 = static_cast<RealType>(11111.0);
    const RealType v30 = static_cast<RealType>(273.15);
    RealType v31 = v30+v6;
    RealType v32 = v29/v31;
    RealType v33 = -v32;
    RealType v34 = exp(v33);
    RealType v35 = v28*v34;
    RealType v36 = v35*v25;
    RealType v37 = x(3);
    RealType v38 = v36*v37;
    RealType v39 = v27-v38;
    RealType v40 = y(0);
    RealType v41 = v25*v40;
    const RealType v42 = static_cast<RealType>(1000.0);
    RealType v43 = x(7);
    RealType v44 = v42*v43;
    RealType v45 = v41/v44;
    RealType v46 = v39-v45;
    RealType v47 = v0*v46;
    RealType v48 = der_x(2);
    RealType v49 = v47-v48;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = static_cast<RealType>(0.);
    RealType a1_v25 = static_cast<RealType>(0.);
    RealType a1_v26 = static_cast<RealType>(0.);
    RealType a1_v27 = static_cast<RealType>(0.);
    RealType a1_v28 = static_cast<RealType>(0.);
    RealType a1_v29 = static_cast<RealType>(0.);
    RealType a1_v30 = static_cast<RealType>(0.);
    RealType a1_v31 = static_cast<RealType>(0.);
    RealType a1_v32 = static_cast<RealType>(0.);
    RealType a1_v33 = static_cast<RealType>(0.);
    RealType a1_v34 = static_cast<RealType>(0.);
    RealType a1_v35 = static_cast<RealType>(0.);
    RealType a1_v36 = static_cast<RealType>(0.);
    RealType a1_v37 = static_cast<RealType>(0.);
    RealType a1_v38 = static_cast<RealType>(0.);
    RealType a1_v39 = static_cast<RealType>(0.);
    RealType a1_v40 = static_cast<RealType>(0.);
    RealType a1_v41 = static_cast<RealType>(0.);
    RealType a1_v42 = static_cast<RealType>(0.);
    RealType a1_v43 = static_cast<RealType>(0.);
    RealType a1_v44 = static_cast<RealType>(0.);
    RealType a1_v45 = static_cast<RealType>(0.);
    RealType a1_v46 = static_cast<RealType>(0.);
    RealType a1_v47 = static_cast<RealType>(0.);
    RealType a1_v48 = static_cast<RealType>(0.);
    RealType a1_v49 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},2);
    a1_v48 -= a1_v49;
    a1_v47 += a1_v49;
    a1_v0 += a1_v47 * v46;
    a1_v46 += a1_v47 * v0;
    a1_v39 += a1_v46;
    a1_v45 -= a1_v46;
    RealType tmp_a1_v45 = a1_v45 / v44;
    a1_v41 += tmp_a1_v45;
    a1_v44 -= v41 * tmp_a1_v45 / v44;
    a1_v42 += a1_v44 * v43;
    a1_v43 += a1_v44 * v42;
    a1_v42 = static_cast<RealType>(0.);
    a1_v25 += a1_v41 * v40;
    a1_v40 += a1_v41 * v25;
    a1_v27 += a1_v39;
    a1_v38 -= a1_v39;
    a1_v36 += a1_v38 * v37;
    a1_v37 += a1_v38 * v36;
    a1_v35 += a1_v36 * v25;
    a1_v25 += a1_v36 * v35;
    a1_v28 += a1_v35 * v34;
    a1_v34 += a1_v35 * v28;
    a1_v33 += a1_v34 * exp(v33);
    a1_v32 -= a1_v33;
    RealType tmp_a1_v32 = a1_v32 / v31;
    a1_v29 += tmp_a1_v32;
    a1_v31 -= v29 * tmp_a1_v32 / v31;
    a1_v30 += a1_v31;
    a1_v6 += a1_v31;
    a1_v30 = static_cast<RealType>(0.);
    a1_v29 = static_cast<RealType>(0.);
    a1_v28 = static_cast<RealType>(0.);
    a1_v15 += a1_v27;
    a1_v26 -= a1_v27;
    a1_v24 += a1_v26 * v25;
    a1_v25 += a1_v26 * v24;
    a1_v23 += a1_v24 * v14;
    a1_v14 += a1_v24 * v23;
    a1_v16 += a1_v23 * v22;
    a1_v22 += a1_v23 * v16;
    a1_v21 += a1_v22 * exp(v21);
    a1_v20 -= a1_v21;
    RealType tmp_a1_v20 = a1_v20 / v19;
    a1_v17 += tmp_a1_v20;
    a1_v19 -= v17 * tmp_a1_v20 / v19;
    a1_v18 += a1_v19;
    a1_v6 += a1_v19;
    a1_v18 = static_cast<RealType>(0.);
    a1_v17 = static_cast<RealType>(0.);
    a1_v16 = static_cast<RealType>(0.);
    a1_v13 += a1_v15 * v14;
    a1_v14 += a1_v15 * v13;
    a1_v11 += a1_v13 * v12;
    a1_v12 += a1_v13 * v11;
    a1_v1 += a1_v11 * v10;
    a1_v10 += a1_v11 * v1;
    a1_v9 += a1_v10 * exp(v9);
    a1_v8 -= a1_v9;
    RealType tmp_a1_v8 = a1_v8 / v7;
    a1_v4 += tmp_a1_v8;
    a1_v7 -= v4 * tmp_a1_v8 / v7;
    a1_v5 += a1_v7;
    a1_v6 += a1_v7;
    a1_v5 = static_cast<RealType>(0.);
    a1_v2 += a1_v4 * v3;
    a1_v3 += a1_v4 * v2;
    a1_v2 = static_cast<RealType>(0.);
    a1_v1 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v12;
        gradient[startIndex++] = a1_v14;
        gradient[startIndex++] = a1_v25;
        gradient[startIndex++] = a1_v37;
        gradient[startIndex++] = a1_v6;
        gradient[startIndex++] = a1_v43;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v40;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v48;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[0-offs] += a1_v12;
        gradient[1-offs] += a1_v14;
        gradient[2-offs] += a1_v25;
        gradient[3-offs] += a1_v37;
        gradient[6-offs] += a1_v6;
        gradient[7-offs] += a1_v43;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v40;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[18-offs] += a1_v48;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<3>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(721170000.0);
    const RealType v2 = static_cast<RealType>(8333.3);
    const RealType v3 = static_cast<RealType>(273.15);
    RealType v4 = x(6);
    RealType v5 = v3+v4;
    RealType v6 = v2/v5;
    RealType v7 = -v6;
    RealType v8 = exp(v7);
    RealType v9 = v1*v8;
    RealType v10 = x(1);
    RealType v11 = v9*v10;
    RealType v12 = x(2);
    RealType v13 = v11*v12;
    const RealType v14 = static_cast<RealType>(1337250000000.0);
    const RealType v15 = static_cast<RealType>(11111.0);
    const RealType v16 = static_cast<RealType>(273.15);
    RealType v17 = v16+v4;
    RealType v18 = v15/v17;
    RealType v19 = -v18;
    RealType v20 = exp(v19);
    RealType v21 = v14*v20;
    RealType v22 = v21*v12;
    RealType v23 = x(3);
    RealType v24 = v22*v23;
    RealType v25 = v13-v24;
    RealType v26 = y(0);
    RealType v27 = v23*v26;
    const RealType v28 = static_cast<RealType>(1000.0);
    RealType v29 = x(7);
    RealType v30 = v28*v29;
    RealType v31 = v27/v30;
    RealType v32 = v25-v31;
    RealType v33 = v0*v32;
    RealType v34 = der_x(3);
    RealType v35 = v33-v34;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = static_cast<RealType>(0.);
    RealType a1_v25 = static_cast<RealType>(0.);
    RealType a1_v26 = static_cast<RealType>(0.);
    RealType a1_v27 = static_cast<RealType>(0.);
    RealType a1_v28 = static_cast<RealType>(0.);
    RealType a1_v29 = static_cast<RealType>(0.);
    RealType a1_v30 = static_cast<RealType>(0.);
    RealType a1_v31 = static_cast<RealType>(0.);
    RealType a1_v32 = static_cast<RealType>(0.);
    RealType a1_v33 = static_cast<RealType>(0.);
    RealType a1_v34 = static_cast<RealType>(0.);
    RealType a1_v35 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},3);
    a1_v34 -= a1_v35;
    a1_v33 += a1_v35;
    a1_v0 += a1_v33 * v32;
    a1_v32 += a1_v33 * v0;
    a1_v25 += a1_v32;
    a1_v31 -= a1_v32;
    RealType tmp_a1_v31 = a1_v31 / v30;
    a1_v27 += tmp_a1_v31;
    a1_v30 -= v27 * tmp_a1_v31 / v30;
    a1_v28 += a1_v30 * v29;
    a1_v29 += a1_v30 * v28;
    a1_v28 = static_cast<RealType>(0.);
    a1_v23 += a1_v27 * v26;
    a1_v26 += a1_v27 * v23;
    a1_v13 += a1_v25;
    a1_v24 -= a1_v25;
    a1_v22 += a1_v24 * v23;
    a1_v23 += a1_v24 * v22;
    a1_v21 += a1_v22 * v12;
    a1_v12 += a1_v22 * v21;
    a1_v14 += a1_v21 * v20;
    a1_v20 += a1_v21 * v14;
    a1_v19 += a1_v20 * exp(v19);
    a1_v18 -= a1_v19;
    RealType tmp_a1_v18 = a1_v18 / v17;
    a1_v15 += tmp_a1_v18;
    a1_v17 -= v15 * tmp_a1_v18 / v17;
    a1_v16 += a1_v17;
    a1_v4 += a1_v17;
    a1_v16 = static_cast<RealType>(0.);
    a1_v15 = static_cast<RealType>(0.);
    a1_v14 = static_cast<RealType>(0.);
    a1_v11 += a1_v13 * v12;
    a1_v12 += a1_v13 * v11;
    a1_v9 += a1_v11 * v10;
    a1_v10 += a1_v11 * v9;
    a1_v1 += a1_v9 * v8;
    a1_v8 += a1_v9 * v1;
    a1_v7 += a1_v8 * exp(v7);
    a1_v6 -= a1_v7;
    RealType tmp_a1_v6 = a1_v6 / v5;
    a1_v2 += tmp_a1_v6;
    a1_v5 -= v2 * tmp_a1_v6 / v5;
    a1_v3 += a1_v5;
    a1_v4 += a1_v5;
    a1_v3 = static_cast<RealType>(0.);
    a1_v2 = static_cast<RealType>(0.);
    a1_v1 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v10;
        gradient[startIndex++] = a1_v12;
        gradient[startIndex++] = a1_v23;
        gradient[startIndex++] = a1_v4;
        gradient[startIndex++] = a1_v29;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v26;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v34;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[1-offs] += a1_v10;
        gradient[2-offs] += a1_v12;
        gradient[3-offs] += a1_v23;
        gradient[6-offs] += a1_v4;
        gradient[7-offs] += a1_v29;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v26;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[19-offs] += a1_v34;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<4>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(1442340000.0);
    const RealType v2 = static_cast<RealType>(8333.3);
    const RealType v3 = static_cast<RealType>(273.15);
    RealType v4 = x(6);
    RealType v5 = v3+v4;
    RealType v6 = v2/v5;
    RealType v7 = -v6;
    RealType v8 = exp(v7);
    RealType v9 = v1*v8;
    RealType v10 = x(1);
    RealType v11 = v9*v10;
    RealType v12 = x(2);
    RealType v13 = v11*v12;
    RealType v14 = x(4);
    RealType v15 = y(0);
    RealType v16 = v14*v15;
    const RealType v17 = static_cast<RealType>(1000.0);
    RealType v18 = x(7);
    RealType v19 = v17*v18;
    RealType v20 = v16/v19;
    RealType v21 = v13-v20;
    RealType v22 = v0*v21;
    RealType v23 = der_x(4);
    RealType v24 = v22-v23;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},4);
    a1_v23 -= a1_v24;
    a1_v22 += a1_v24;
    a1_v0 += a1_v22 * v21;
    a1_v21 += a1_v22 * v0;
    a1_v13 += a1_v21;
    a1_v20 -= a1_v21;
    RealType tmp_a1_v20 = a1_v20 / v19;
    a1_v16 += tmp_a1_v20;
    a1_v19 -= v16 * tmp_a1_v20 / v19;
    a1_v17 += a1_v19 * v18;
    a1_v18 += a1_v19 * v17;
    a1_v17 = static_cast<RealType>(0.);
    a1_v14 += a1_v16 * v15;
    a1_v15 += a1_v16 * v14;
    a1_v11 += a1_v13 * v12;
    a1_v12 += a1_v13 * v11;
    a1_v9 += a1_v11 * v10;
    a1_v10 += a1_v11 * v9;
    a1_v1 += a1_v9 * v8;
    a1_v8 += a1_v9 * v1;
    a1_v7 += a1_v8 * exp(v7);
    a1_v6 -= a1_v7;
    RealType tmp_a1_v6 = a1_v6 / v5;
    a1_v2 += tmp_a1_v6;
    a1_v5 -= v2 * tmp_a1_v6 / v5;
    a1_v3 += a1_v5;
    a1_v4 += a1_v5;
    a1_v3 = static_cast<RealType>(0.);
    a1_v2 = static_cast<RealType>(0.);
    a1_v1 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v10;
        gradient[startIndex++] = a1_v12;
        gradient[startIndex++] = a1_v14;
        gradient[startIndex++] = a1_v4;
        gradient[startIndex++] = a1_v18;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v15;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v23;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[1-offs] += a1_v10;
        gradient[2-offs] += a1_v12;
        gradient[4-offs] += a1_v14;
        gradient[6-offs] += a1_v4;
        gradient[7-offs] += a1_v18;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v15;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[20-offs] += a1_v23;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<5>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(4011750000000.0);
    const RealType v2 = static_cast<RealType>(11111.0);
    const RealType v3 = static_cast<RealType>(273.15);
    RealType v4 = x(6);
    RealType v5 = v3+v4;
    RealType v6 = v2/v5;
    RealType v7 = -v6;
    RealType v8 = exp(v7);
    RealType v9 = v1*v8;
    RealType v10 = x(3);
    RealType v11 = v9*v10;
    RealType v12 = x(2);
    RealType v13 = v11*v12;
    RealType v14 = x(5);
    RealType v15 = y(0);
    RealType v16 = v14*v15;
    const RealType v17 = static_cast<RealType>(1000.0);
    RealType v18 = x(7);
    RealType v19 = v17*v18;
    RealType v20 = v16/v19;
    RealType v21 = v13-v20;
    RealType v22 = v0*v21;
    RealType v23 = der_x(5);
    RealType v24 = v22-v23;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},5);
    a1_v23 -= a1_v24;
    a1_v22 += a1_v24;
    a1_v0 += a1_v22 * v21;
    a1_v21 += a1_v22 * v0;
    a1_v13 += a1_v21;
    a1_v20 -= a1_v21;
    RealType tmp_a1_v20 = a1_v20 / v19;
    a1_v16 += tmp_a1_v20;
    a1_v19 -= v16 * tmp_a1_v20 / v19;
    a1_v17 += a1_v19 * v18;
    a1_v18 += a1_v19 * v17;
    a1_v17 = static_cast<RealType>(0.);
    a1_v14 += a1_v16 * v15;
    a1_v15 += a1_v16 * v14;
    a1_v11 += a1_v13 * v12;
    a1_v12 += a1_v13 * v11;
    a1_v9 += a1_v11 * v10;
    a1_v10 += a1_v11 * v9;
    a1_v1 += a1_v9 * v8;
    a1_v8 += a1_v9 * v1;
    a1_v7 += a1_v8 * exp(v7);
    a1_v6 -= a1_v7;
    RealType tmp_a1_v6 = a1_v6 / v5;
    a1_v2 += tmp_a1_v6;
    a1_v5 -= v2 * tmp_a1_v6 / v5;
    a1_v3 += a1_v5;
    a1_v4 += a1_v5;
    a1_v3 = static_cast<RealType>(0.);
    a1_v2 = static_cast<RealType>(0.);
    a1_v1 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v12;
        gradient[startIndex++] = a1_v10;
        gradient[startIndex++] = a1_v14;
        gradient[startIndex++] = a1_v4;
        gradient[startIndex++] = a1_v18;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v15;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v23;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[2-offs] += a1_v12;
        gradient[3-offs] += a1_v10;
        gradient[5-offs] += a1_v14;
        gradient[6-offs] += a1_v4;
        gradient[7-offs] += a1_v18;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v15;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[21-offs] += a1_v23;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<6>, VarGroup<K>, RealType* gradient) {
    const RealType v0 = static_cast<RealType>(0.001);
    RealType v1 = p(3);
    RealType v2 = v0*v1;
    RealType v3 = y(0);
    RealType v4 = v2*v3;
    RealType v5 = der_x(7);
    RealType v6 = v4-v5;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},6);
    a1_v5 -= a1_v6;
    a1_v4 += a1_v6;
    a1_v2 += a1_v4 * v3;
    a1_v3 += a1_v4 * v2;
    a1_v0 += a1_v2 * v1;
    a1_v1 += a1_v2 * v0;
    a1_v0 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v3;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v1;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v5;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v3;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v1;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[23-offs] += a1_v5;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<7>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(5554.1);
    RealType v2 = -v1;
    const RealType v3 = static_cast<RealType>(721170000.0);
    const RealType v4 = static_cast<RealType>(8333.3);
    const RealType v5 = static_cast<RealType>(273.15);
    RealType v6 = x(6);
    RealType v7 = v5+v6;
    RealType v8 = v4/v7;
    RealType v9 = -v8;
    RealType v10 = exp(v9);
    RealType v11 = v3*v10;
    RealType v12 = x(1);
    RealType v13 = v11*v12;
    RealType v14 = x(2);
    RealType v15 = v13*v14;
    const RealType v16 = static_cast<RealType>(1337250000000.0);
    const RealType v17 = static_cast<RealType>(11111.0);
    const RealType v18 = static_cast<RealType>(273.15);
    RealType v19 = v18+v6;
    RealType v20 = v17/v19;
    RealType v21 = -v20;
    RealType v22 = exp(v21);
    RealType v23 = v16*v22;
    RealType v24 = v23*v14;
    RealType v25 = x(3);
    RealType v26 = v24*v25;
    RealType v27 = v15-v26;
    RealType v28 = v2*v27;
    RealType v29 = x(7);
    RealType v30 = v28*v29;
    const RealType v31 = static_cast<RealType>(181605029400.0);
    const RealType v32 = static_cast<RealType>(8333.3);
    const RealType v33 = static_cast<RealType>(273.15);
    RealType v34 = v33+v6;
    RealType v35 = v32/v34;
    RealType v36 = -v35;
    RealType v37 = exp(v36);
    RealType v38 = v31*v37;
    RealType v39 = v38*v12;
    RealType v40 = v39*v14;
    RealType v41 = v40*v29;
    RealType v42 = v30-v41;
    RealType v43 = v0*v42;
    RealType v44 = der_x(8);
    RealType v45 = v43-v44;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = static_cast<RealType>(0.);
    RealType a1_v25 = static_cast<RealType>(0.);
    RealType a1_v26 = static_cast<RealType>(0.);
    RealType a1_v27 = static_cast<RealType>(0.);
    RealType a1_v28 = static_cast<RealType>(0.);
    RealType a1_v29 = static_cast<RealType>(0.);
    RealType a1_v30 = static_cast<RealType>(0.);
    RealType a1_v31 = static_cast<RealType>(0.);
    RealType a1_v32 = static_cast<RealType>(0.);
    RealType a1_v33 = static_cast<RealType>(0.);
    RealType a1_v34 = static_cast<RealType>(0.);
    RealType a1_v35 = static_cast<RealType>(0.);
    RealType a1_v36 = static_cast<RealType>(0.);
    RealType a1_v37 = static_cast<RealType>(0.);
    RealType a1_v38 = static_cast<RealType>(0.);
    RealType a1_v39 = static_cast<RealType>(0.);
    RealType a1_v40 = static_cast<RealType>(0.);
    RealType a1_v41 = static_cast<RealType>(0.);
    RealType a1_v42 = static_cast<RealType>(0.);
    RealType a1_v43 = static_cast<RealType>(0.);
    RealType a1_v44 = static_cast<RealType>(0.);
    RealType a1_v45 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},7);
    a1_v44 -= a1_v45;
    a1_v43 += a1_v45;
    a1_v0 += a1_v43 * v42;
    a1_v42 += a1_v43 * v0;
    a1_v30 += a1_v42;
    a1_v41 -= a1_v42;
    a1_v40 += a1_v41 * v29;
    a1_v29 += a1_v41 * v40;
    a1_v39 += a1_v40 * v14;
    a1_v14 += a1_v40 * v39;
    a1_v38 += a1_v39 * v12;
    a1_v12 += a1_v39 * v38;
    a1_v31 += a1_v38 * v37;
    a1_v37 += a1_v38 * v31;
    a1_v36 += a1_v37 * exp(v36);
    a1_v35 -= a1_v36;
    RealType tmp_a1_v35 = a1_v35 / v34;
    a1_v32 += tmp_a1_v35;
    a1_v34 -= v32 * tmp_a1_v35 / v34;
    a1_v33 += a1_v34;
    a1_v6 += a1_v34;
    a1_v33 = static_cast<RealType>(0.);
    a1_v32 = static_cast<RealType>(0.);
    a1_v31 = static_cast<RealType>(0.);
    a1_v28 += a1_v30 * v29;
    a1_v29 += a1_v30 * v28;
    a1_v2 += a1_v28 * v27;
    a1_v27 += a1_v28 * v2;
    a1_v15 += a1_v27;
    a1_v26 -= a1_v27;
    a1_v24 += a1_v26 * v25;
    a1_v25 += a1_v26 * v24;
    a1_v23 += a1_v24 * v14;
    a1_v14 += a1_v24 * v23;
    a1_v16 += a1_v23 * v22;
    a1_v22 += a1_v23 * v16;
    a1_v21 += a1_v22 * exp(v21);
    a1_v20 -= a1_v21;
    RealType tmp_a1_v20 = a1_v20 / v19;
    a1_v17 += tmp_a1_v20;
    a1_v19 -= v17 * tmp_a1_v20 / v19;
    a1_v18 += a1_v19;
    a1_v6 += a1_v19;
    a1_v18 = static_cast<RealType>(0.);
    a1_v17 = static_cast<RealType>(0.);
    a1_v16 = static_cast<RealType>(0.);
    a1_v13 += a1_v15 * v14;
    a1_v14 += a1_v15 * v13;
    a1_v11 += a1_v13 * v12;
    a1_v12 += a1_v13 * v11;
    a1_v3 += a1_v11 * v10;
    a1_v10 += a1_v11 * v3;
    a1_v9 += a1_v10 * exp(v9);
    a1_v8 -= a1_v9;
    RealType tmp_a1_v8 = a1_v8 / v7;
    a1_v4 += tmp_a1_v8;
    a1_v7 -= v4 * tmp_a1_v8 / v7;
    a1_v5 += a1_v7;
    a1_v6 += a1_v7;
    a1_v5 = static_cast<RealType>(0.);
    a1_v4 = static_cast<RealType>(0.);
    a1_v3 = static_cast<RealType>(0.);
    a1_v1 -= a1_v2;
    a1_v1 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v12;
        gradient[startIndex++] = a1_v14;
        gradient[startIndex++] = a1_v25;
        gradient[startIndex++] = a1_v6;
        gradient[startIndex++] = a1_v29;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v44;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[1-offs] += a1_v12;
        gradient[2-offs] += a1_v14;
        gradient[3-offs] += a1_v25;
        gradient[6-offs] += a1_v6;
        gradient[7-offs] += a1_v29;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[24-offs] += a1_v44;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<8>, VarGroup<K>, RealType* gradient) {
    RealType v0 = y(2);
    RealType v1 = der_x(6);
    RealType v2 = v0-v1;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},8);
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v1;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[11-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[22-offs] += a1_v1;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<9>, VarGroup<K>, RealType* gradient) {
    using std::exp;
    RealType v0 = p(3);
    RealType v1 = y(0);
    RealType v2 = c(1);
    RealType v3 = v1*v2;
    const RealType v4 = static_cast<RealType>(1000);
    RealType v5 = x(7);
    RealType v6 = v4*v5;
    RealType v7 = v3/v6;
    const RealType v8 = static_cast<RealType>(0.000239005736137667);
    const RealType v9 = static_cast<RealType>(437881620000.0);
    const RealType v10 = static_cast<RealType>(1000.0);
    RealType v11 = c(2);
    RealType v12 = v10*v11;
    const RealType v13 = static_cast<RealType>(273.15);
    RealType v14 = x(6);
    RealType v15 = v13+v14;
    RealType v16 = v12/v15;
    RealType v17 = -v16;
    RealType v18 = exp(v17);
    RealType v19 = v9*v18;
    RealType v20 = x(0);
    RealType v21 = v19*v20;
    RealType v22 = x(1);
    RealType v23 = v21*v22;
    const RealType v24 = static_cast<RealType>(114161211000000.0);
    const RealType v25 = static_cast<RealType>(8333.3);
    const RealType v26 = static_cast<RealType>(273.15);
    RealType v27 = v26+v14;
    RealType v28 = v25/v27;
    RealType v29 = -v28;
    RealType v30 = exp(v29);
    RealType v31 = v24*v30;
    RealType v32 = v31*v22;
    RealType v33 = x(2);
    RealType v34 = v32*v33;
    RealType v35 = v23+v34;
    const RealType v36 = static_cast<RealType>(6.0523935E+017);
    const RealType v37 = static_cast<RealType>(11111.0);
    const RealType v38 = static_cast<RealType>(273.15);
    RealType v39 = v38+v14;
    RealType v40 = v37/v39;
    RealType v41 = -v40;
    RealType v42 = exp(v41);
    RealType v43 = v36*v42;
    RealType v44 = v43*v33;
    RealType v45 = x(3);
    RealType v46 = v44*v45;
    RealType v47 = v35+v46;
    RealType v48 = v8*v47;
    RealType v49 = v7+v48;
    const RealType v50 = static_cast<RealType>(0.000243454685774894);
    const RealType v51 = static_cast<RealType>(1000.0);
    RealType v52 = y(1);
    RealType v53 = v51*v52;
    RealType v54 = v14-v53;
    RealType v55 = v50*v54;
    RealType v56 = v49-v55;
    RealType v57 = v1*v14;
    const RealType v58 = static_cast<RealType>(1000);
    RealType v59 = v58*v5;
    RealType v60 = v57/v59;
    RealType v61 = v56-v60;
    RealType v62 = v0*v61;
    RealType v63 = y(2);
    RealType v64 = v62-v63;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = static_cast<RealType>(0.);
    RealType a1_v15 = static_cast<RealType>(0.);
    RealType a1_v16 = static_cast<RealType>(0.);
    RealType a1_v17 = static_cast<RealType>(0.);
    RealType a1_v18 = static_cast<RealType>(0.);
    RealType a1_v19 = static_cast<RealType>(0.);
    RealType a1_v20 = static_cast<RealType>(0.);
    RealType a1_v21 = static_cast<RealType>(0.);
    RealType a1_v22 = static_cast<RealType>(0.);
    RealType a1_v23 = static_cast<RealType>(0.);
    RealType a1_v24 = static_cast<RealType>(0.);
    RealType a1_v25 = static_cast<RealType>(0.);
    RealType a1_v26 = static_cast<RealType>(0.);
    RealType a1_v27 = static_cast<RealType>(0.);
    RealType a1_v28 = static_cast<RealType>(0.);
    RealType a1_v29 = static_cast<RealType>(0.);
    RealType a1_v30 = static_cast<RealType>(0.);
    RealType a1_v31 = static_cast<RealType>(0.);
    RealType a1_v32 = static_cast<RealType>(0.);
    RealType a1_v33 = static_cast<RealType>(0.);
    RealType a1_v34 = static_cast<RealType>(0.);
    RealType a1_v35 = static_cast<RealType>(0.);
    RealType a1_v36 = static_cast<RealType>(0.);
    RealType a1_v37 = static_cast<RealType>(0.);
    RealType a1_v38 = static_cast<RealType>(0.);
    RealType a1_v39 = static_cast<RealType>(0.);
    RealType a1_v40 = static_cast<RealType>(0.);
    RealType a1_v41 = static_cast<RealType>(0.);
    RealType a1_v42 = static_cast<RealType>(0.);
    RealType a1_v43 = static_cast<RealType>(0.);
    RealType a1_v44 = static_cast<RealType>(0.);
    RealType a1_v45 = static_cast<RealType>(0.);
    RealType a1_v46 = static_cast<RealType>(0.);
    RealType a1_v47 = static_cast<RealType>(0.);
    RealType a1_v48 = static_cast<RealType>(0.);
    RealType a1_v49 = static_cast<RealType>(0.);
    RealType a1_v50 = static_cast<RealType>(0.);
    RealType a1_v51 = static_cast<RealType>(0.);
    RealType a1_v52 = static_cast<RealType>(0.);
    RealType a1_v53 = static_cast<RealType>(0.);
    RealType a1_v54 = static_cast<RealType>(0.);
    RealType a1_v55 = static_cast<RealType>(0.);
    RealType a1_v56 = static_cast<RealType>(0.);
    RealType a1_v57 = static_cast<RealType>(0.);
    RealType a1_v58 = static_cast<RealType>(0.);
    RealType a1_v59 = static_cast<RealType>(0.);
    RealType a1_v60 = static_cast<RealType>(0.);
    RealType a1_v61 = static_cast<RealType>(0.);
    RealType a1_v62 = static_cast<RealType>(0.);
    RealType a1_v63 = static_cast<RealType>(0.);
    RealType a1_v64 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},9);
    a1_v63 -= a1_v64;
    a1_v62 += a1_v64;
    a1_v0 += a1_v62 * v61;
    a1_v61 += a1_v62 * v0;
    a1_v56 += a1_v61;
    a1_v60 -= a1_v61;
    RealType tmp_a1_v60 = a1_v60 / v59;
    a1_v57 += tmp_a1_v60;
    a1_v59 -= v57 * tmp_a1_v60 / v59;
    a1_v58 += a1_v59 * v5;
    a1_v5 += a1_v59 * v58;
    a1_v58 = static_cast<RealType>(0.);
    a1_v1 += a1_v57 * v14;
    a1_v14 += a1_v57 * v1;
    a1_v49 += a1_v56;
    a1_v55 -= a1_v56;
    a1_v50 += a1_v55 * v54;
    a1_v54 += a1_v55 * v50;
    a1_v14 += a1_v54;
    a1_v53 -= a1_v54;
    a1_v51 += a1_v53 * v52;
    a1_v52 += a1_v53 * v51;
    a1_v51 = static_cast<RealType>(0.);
    a1_v50 = static_cast<RealType>(0.);
    a1_v7 += a1_v49;
    a1_v48 += a1_v49;
    a1_v8 += a1_v48 * v47;
    a1_v47 += a1_v48 * v8;
    a1_v35 += a1_v47;
    a1_v46 += a1_v47;
    a1_v44 += a1_v46 * v45;
    a1_v45 += a1_v46 * v44;
    a1_v43 += a1_v44 * v33;
    a1_v33 += a1_v44 * v43;
    a1_v36 += a1_v43 * v42;
    a1_v42 += a1_v43 * v36;
    a1_v41 += a1_v42 * exp(v41);
    a1_v40 -= a1_v41;
    RealType tmp_a1_v40 = a1_v40 / v39;
    a1_v37 += tmp_a1_v40;
    a1_v39 -= v37 * tmp_a1_v40 / v39;
    a1_v38 += a1_v39;
    a1_v14 += a1_v39;
    a1_v38 = static_cast<RealType>(0.);
    a1_v37 = static_cast<RealType>(0.);
    a1_v36 = static_cast<RealType>(0.);
    a1_v23 += a1_v35;
    a1_v34 += a1_v35;
    a1_v32 += a1_v34 * v33;
    a1_v33 += a1_v34 * v32;
    a1_v31 += a1_v32 * v22;
    a1_v22 += a1_v32 * v31;
    a1_v24 += a1_v31 * v30;
    a1_v30 += a1_v31 * v24;
    a1_v29 += a1_v30 * exp(v29);
    a1_v28 -= a1_v29;
    RealType tmp_a1_v28 = a1_v28 / v27;
    a1_v25 += tmp_a1_v28;
    a1_v27 -= v25 * tmp_a1_v28 / v27;
    a1_v26 += a1_v27;
    a1_v14 += a1_v27;
    a1_v26 = static_cast<RealType>(0.);
    a1_v25 = static_cast<RealType>(0.);
    a1_v24 = static_cast<RealType>(0.);
    a1_v21 += a1_v23 * v22;
    a1_v22 += a1_v23 * v21;
    a1_v19 += a1_v21 * v20;
    a1_v20 += a1_v21 * v19;
    a1_v9 += a1_v19 * v18;
    a1_v18 += a1_v19 * v9;
    a1_v17 += a1_v18 * exp(v17);
    a1_v16 -= a1_v17;
    RealType tmp_a1_v16 = a1_v16 / v15;
    a1_v12 += tmp_a1_v16;
    a1_v15 -= v12 * tmp_a1_v16 / v15;
    a1_v13 += a1_v15;
    a1_v14 += a1_v15;
    a1_v13 = static_cast<RealType>(0.);
    a1_v10 += a1_v12 * v11;
    a1_v11 += a1_v12 * v10;
    a1_v10 = static_cast<RealType>(0.);
    a1_v9 = static_cast<RealType>(0.);
    a1_v8 = static_cast<RealType>(0.);
    RealType tmp_a1_v7 = a1_v7 / v6;
    a1_v3 += tmp_a1_v7;
    a1_v6 -= v3 * tmp_a1_v7 / v6;
    a1_v4 += a1_v6 * v5;
    a1_v5 += a1_v6 * v4;
    a1_v4 = static_cast<RealType>(0.);
    a1_v1 += a1_v3 * v2;
    a1_v2 += a1_v3 * v1;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v20;
        gradient[startIndex++] = a1_v22;
        gradient[startIndex++] = a1_v33;
        gradient[startIndex++] = a1_v45;
        gradient[startIndex++] = a1_v14;
        gradient[startIndex++] = a1_v5;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v1;
        gradient[startIndex++] = a1_v52;
        gradient[startIndex++] = a1_v63;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[0-offs] += a1_v20;
        gradient[1-offs] += a1_v22;
        gradient[2-offs] += a1_v33;
        gradient[3-offs] += a1_v45;
        gradient[6-offs] += a1_v14;
        gradient[7-offs] += a1_v5;
      }
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v1;
        gradient[10-offs] += a1_v52;
        gradient[11-offs] += a1_v63;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[15-offs] += a1_v0;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<10>, VarGroup<K>, RealType* gradient) {
    RealType v0 = p(1);
    RealType v1 = y(0);
    RealType v2 = v0-v1;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},10);
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v1;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[9-offs] += a1_v1;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[13-offs] += a1_v0;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<11>, VarGroup<K>, RealType* gradient) {
    RealType v0 = p(0);
    RealType v1 = y(1);
    RealType v2 = v0-v1;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},11);
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v1;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[10-offs] += a1_v1;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[12-offs] += a1_v0;
      }
    }
}



  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<0>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<1>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<2>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<3>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<4>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<5>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<6>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<7>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<8>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<9>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<10>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<11>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;




// Boiler plate code starts here
private:
  static constexpr int pseudoLastEquationIndex = numEquations(AllEqns{}); //hack to help Visual Studio
public:
  RealType eval(AllEqns, EqIndex<pseudoLastEquationIndex>) const {
    return static_cast<RealType>(0.);
  }
  template <int J>
  RealType eval(DiffEqns,EqIndex<J>) const {
    return eval(AllEqns{}, EqIndex<J>{});
  }

  template <int J>
  RealType eval(AlgEqns,EqIndex<J>) const {
    return eval(AllEqns{}, EqIndex<J+numEquations(DiffEqns{})>{});
  }

  template <int J, class Tangents>
  RealType evalTangent(DiffEqns,EqIndex<J>, const Tangents& tangents) const {
    return evalTangent(AllEqns{}, EqIndex<J>{}, tangents);
  }

  template <int J, class Tangents>
  RealType evalTangent(AlgEqns,EqIndex<J>, const Tangents& tangents) const {
    return evalTangent(AllEqns{}, EqIndex<J+numEquations(DiffEqns{})>{}, tangents);
  }

  template <int K, bool doEvalGradient, int J>
  void evalGradient(DiffEqns, EqIndex<J>, VarGroup<K>, RealType *gradient)  {
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<J>{}, VarGroup<K>{},gradient);
  }

  template <int K, bool doEvalGradient, int J>
  void evalGradient(AlgEqns, EqIndex<J>, VarGroup<K>, RealType *gradient)  {
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<J+numEquations(DiffEqns{})>{}, VarGroup<K>{}, gradient);
  }

  template<int K>
  void setVariable(VarGroup<K> varGroup, const int index, const RealType& value){
    variables[index+variableIndexOffset(varGroup)] = value;
  }

  template<int K>
  RealType getVariable(VarGroup<K> varGroup, const int index) const {
    return variables[index+variableIndexOffset(varGroup)];
  }

  template<int I>
  RealType getAdjoint(EqGroup<I> eqGroup, const int index) const {
    return adjoints[index+equationIndexOffset(eqGroup)];
  }

  template<int I>
  void setAdjoint(EqGroup<I> eqGroup, const int index, const RealType& value){
    adjoints[index+equationIndexOffset(eqGroup)] = value;
  }

  template<int K>
  RealType getInitialValue(VarGroup<K> varGroup, const int index) const {
    return initialValues[index+variableIndexOffset(varGroup)];
  }

  template<int K, int A, int B, int C, int D>
  void setVariables(VarGroup<K>,const Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<Eigen::Matrix<RealType,A,1,B,C,D> > varView(
          variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    varView = values;
  }

  template<int K>
  void setVariables(VarGroup<K>,const RealType *values){
    Eigen::Map<Eigen::Matrix<RealType,Eigen::Dynamic,1> > varView(
        variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    Eigen::Map<const Eigen::Matrix<RealType,Eigen::Dynamic,1> > valuesView(
        values,numVariables(VarGroup<K>{}),1);
    varView = valuesView;
  }

  template<int I, int A, int B, int C, int D>
  void setAdjoints(EqGroup<I>,const Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<Eigen::Matrix<RealType,A,1,B,C,D> > varView(
        adjoints.data()+equationIndexOffset(EqGroup<I>{}),numEquations(EqGroup<I>{}),1);
    varView = values;
  }

  template<int I>
  void setOnesAdjoints(EqGroup<I>) {
    Eigen::Map<Eigen::Matrix<RealType,numEquations(EqGroup<I>{}),1> > varView(
      adjoints.data()+equationIndexOffset(EqGroup<I>{}),numEquations(EqGroup<I>{}),1);
    varView = Eigen::Matrix<RealType,numEquations(EqGroup<I>{}),1>::Ones();
  }

  template<int K, int A, int B, int C, int D>
  void getVariables(VarGroup<K>, Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<Eigen::Matrix<RealType,A,1,B,C,D> > varView(
          variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    values = varView;
  }

  template<int K>
  void getVariables(VarGroup<K>, RealType *values){
    Eigen::Map<Eigen::Matrix<RealType,Eigen::Dynamic,1> > varView(
        variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    Eigen::Map<Eigen::Matrix<RealType,Eigen::Dynamic,1> > valuesView(
        values,numVariables(VarGroup<K>{}),1);
    valuesView = varView;
  }

  template<int K, int A, int B, int C, int D>
  void getInitialValues(VarGroup<K>, Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<const Eigen::Matrix<RealType,A,1,B,C,D> > varView(
          initialValues.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    values = varView;
  }

  template<int K, class Indices, class Values>
  void setVariables(VarGroup<K> varGroup, const Indices& indices, const Values& values)
  {
    int index = 0;
    for(auto p = indices.cbegin(); p != indices.cend(); ++p,++index)
    {
      this->setVariable(varGroup,*p,values[index]);
    }
  }


  template<int K, class Tangents, class TangentAdjoints, class RHS>
  void  evalAllSecondOrderAdjoints(AllEqns, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RHS& rhs)
  {
    rhs.setZero();
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<0>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<1>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<2>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<3>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<4>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<5>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<6>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<7>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<8>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<9>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<10>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<11>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
  }

  template<int K, class RHS>
  void  evalAllAdjoints(AllEqns, VarGroup<K>, RHS& rhs)
  {
    rhs.setZero();
    evalGradient<K,false>(AllEqns{}, EqIndex<0>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<1>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<2>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<3>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<4>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<5>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<6>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<7>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<8>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<9>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<10>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<11>{}, VarGroup<K>{}, rhs.data());
  }

  template<typename  V>
  void  evalAll(AllEqns, V& residuals)
  {
    residuals[0] = eval(AllEqns{},EqIndex<0>{});
    residuals[1] = eval(AllEqns{},EqIndex<1>{});
    residuals[2] = eval(AllEqns{},EqIndex<2>{});
    residuals[3] = eval(AllEqns{},EqIndex<3>{});
    residuals[4] = eval(AllEqns{},EqIndex<4>{});
    residuals[5] = eval(AllEqns{},EqIndex<5>{});
    residuals[6] = eval(AllEqns{},EqIndex<6>{});
    residuals[7] = eval(AllEqns{},EqIndex<7>{});
    residuals[8] = eval(AllEqns{},EqIndex<8>{});
    residuals[9] = eval(AllEqns{},EqIndex<9>{});
    residuals[10] = eval(AllEqns{},EqIndex<10>{});
    residuals[11] = eval(AllEqns{},EqIndex<11>{});
  }

  template<typename  V>
  void  evalAll(DiffEqns, V& residuals)
  {
    residuals[0] = eval(DiffEqns{},EqIndex<0>{});
    residuals[1] = eval(DiffEqns{},EqIndex<1>{});
    residuals[2] = eval(DiffEqns{},EqIndex<2>{});
    residuals[3] = eval(DiffEqns{},EqIndex<3>{});
    residuals[4] = eval(DiffEqns{},EqIndex<4>{});
    residuals[5] = eval(DiffEqns{},EqIndex<5>{});
    residuals[6] = eval(DiffEqns{},EqIndex<6>{});
    residuals[7] = eval(DiffEqns{},EqIndex<7>{});
    residuals[8] = eval(DiffEqns{},EqIndex<8>{});
  }

  template<typename  V>
  void  evalAll(AlgEqns, V& residuals)
  {
    residuals[0] = eval(AlgEqns{},EqIndex<0>{});
    residuals[1] = eval(AlgEqns{},EqIndex<1>{});
    residuals[2] = eval(AlgEqns{},EqIndex<2>{});
  }

  RealType eval(AllEqns, int equationIndex) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = eval(AllEqns{},EqIndex<0>{}); break;
      case 1: result = eval(AllEqns{},EqIndex<1>{}); break;
      case 2: result = eval(AllEqns{},EqIndex<2>{}); break;
      case 3: result = eval(AllEqns{},EqIndex<3>{}); break;
      case 4: result = eval(AllEqns{},EqIndex<4>{}); break;
      case 5: result = eval(AllEqns{},EqIndex<5>{}); break;
      case 6: result = eval(AllEqns{},EqIndex<6>{}); break;
      case 7: result = eval(AllEqns{},EqIndex<7>{}); break;
      case 8: result = eval(AllEqns{},EqIndex<8>{}); break;
      case 9: result = eval(AllEqns{},EqIndex<9>{}); break;
      case 10: result = eval(AllEqns{},EqIndex<10>{}); break;
      case 11: result = eval(AllEqns{},EqIndex<11>{}); break;
    }
    return result;
  }

  RealType eval(DiffEqns, int equationIndex) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = eval(DiffEqns{},EqIndex<0>{}); break;
      case 1: result = eval(DiffEqns{},EqIndex<1>{}); break;
      case 2: result = eval(DiffEqns{},EqIndex<2>{}); break;
      case 3: result = eval(DiffEqns{},EqIndex<3>{}); break;
      case 4: result = eval(DiffEqns{},EqIndex<4>{}); break;
      case 5: result = eval(DiffEqns{},EqIndex<5>{}); break;
      case 6: result = eval(DiffEqns{},EqIndex<6>{}); break;
      case 7: result = eval(DiffEqns{},EqIndex<7>{}); break;
      case 8: result = eval(DiffEqns{},EqIndex<8>{}); break;
    }
    return result;
  }

  RealType eval(AlgEqns, int equationIndex) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = eval(AlgEqns{},EqIndex<0>{}); break;
      case 1: result = eval(AlgEqns{},EqIndex<1>{}); break;
      case 2: result = eval(AlgEqns{},EqIndex<2>{}); break;
    }
    return result;
  }

template<int I, int K, typename _Scalar, int _Rows, int _Cols, int _Options,
         int _MaxRows, int _MaxCols>
  void evalJacobianPattern(EqGroup<I>, VarGroup<K> ,
      Eigen::Matrix< _Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols >& jacobian)
  {
    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables
    jacobian.resize(numEqns,numVars);
  }

  template<int I, int K, typename _Scalar, int _Rows, int _Cols, int _Options,
      int _MaxRows, int _MaxCols>
  void evalJacobianValues(EqGroup<I> eqGroup, VarGroup<K> varGroup,
      Eigen::Matrix< _Scalar, _Rows, _Cols, _Options,
      _MaxRows, _MaxCols >& jacobian)
  {
    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables
    for(int varIndex=0; varIndex < numVars; ++varIndex){
      for(int eqIndex=0; eqIndex < numEqns;++eqIndex) {
        jacobian(eqIndex,varIndex) =  evalDerivative(eqGroup,eqIndex,varGroup,varIndex);
      }
    }
  }

  template<int I, int K>
  RealType evalDerivative(EqGroup<I> eqGroup, int equationIndex, VarGroup<K>, int variableIndex){
    const int index = variableIndexOffset(VarGroup<K>{}) + variableIndex;
    tangents[index] = static_cast<RealType>(1.);
    RealType result = evalTangent(eqGroup,equationIndex,tangents);
    tangents[index] = static_cast<RealType>(0.);
    return result;
  }

  template<class Tangents>
  RealType evalTangent(AllEqns, int equationIndex, const Tangents& tangents) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = evalTangent(AllEqns{},EqIndex<0>{},tangents); break;
      case 1: result = evalTangent(AllEqns{},EqIndex<1>{},tangents); break;
      case 2: result = evalTangent(AllEqns{},EqIndex<2>{},tangents); break;
      case 3: result = evalTangent(AllEqns{},EqIndex<3>{},tangents); break;
      case 4: result = evalTangent(AllEqns{},EqIndex<4>{},tangents); break;
      case 5: result = evalTangent(AllEqns{},EqIndex<5>{},tangents); break;
      case 6: result = evalTangent(AllEqns{},EqIndex<6>{},tangents); break;
      case 7: result = evalTangent(AllEqns{},EqIndex<7>{},tangents); break;
      case 8: result = evalTangent(AllEqns{},EqIndex<8>{},tangents); break;
      case 9: result = evalTangent(AllEqns{},EqIndex<9>{},tangents); break;
      case 10: result = evalTangent(AllEqns{},EqIndex<10>{},tangents); break;
      case 11: result = evalTangent(AllEqns{},EqIndex<11>{},tangents); break;
    }
    return result;
  }

  template<class Tangents>
  RealType evalTangent(DiffEqns, int equationIndex, const Tangents& tangents) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = evalTangent(DiffEqns{},EqIndex<0>{},tangents); break;
      case 1: result = evalTangent(DiffEqns{},EqIndex<1>{},tangents); break;
      case 2: result = evalTangent(DiffEqns{},EqIndex<2>{},tangents); break;
      case 3: result = evalTangent(DiffEqns{},EqIndex<3>{},tangents); break;
      case 4: result = evalTangent(DiffEqns{},EqIndex<4>{},tangents); break;
      case 5: result = evalTangent(DiffEqns{},EqIndex<5>{},tangents); break;
      case 6: result = evalTangent(DiffEqns{},EqIndex<6>{},tangents); break;
      case 7: result = evalTangent(DiffEqns{},EqIndex<7>{},tangents); break;
      case 8: result = evalTangent(DiffEqns{},EqIndex<8>{},tangents); break;
    }
    return result;
  }

  template<class Tangents>
  RealType evalTangent(AlgEqns, int equationIndex, const Tangents& tangents) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = evalTangent(AlgEqns{},EqIndex<0>{},tangents); break;
      case 1: result = evalTangent(AlgEqns{},EqIndex<1>{},tangents); break;
      case 2: result = evalTangent(AlgEqns{},EqIndex<2>{},tangents); break;
    }
    return result;
  }



  template<int K, bool doEvalGradient = true>
  void evalAllGradients(AllEqns, VarGroup<K>, RealType *gradients)
  {
    startIndex = 0;
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<0>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<1>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<2>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<3>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<4>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<5>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<6>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<7>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<8>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<9>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<10>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<11>{}, VarGroup<K>{}, gradients);
  }

  template<int K, bool doEvalGradient = true>
  void evalAllGradients(DiffEqns, VarGroup<K>, RealType *gradients)
  {
    startIndex = 0;
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<0>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<1>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<2>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<3>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<4>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<5>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<6>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<7>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<8>{}, VarGroup<K>{}, gradients);
  }

  template<int K, bool doEvalGradient = true>
  void evalAllGradients(AlgEqns, VarGroup<K>, RealType *gradients) const
  {
    startIndex = 0;
    evalGradient<K,doEvalGradient>(AlgEqns{},EqIndex<0>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AlgEqns{},EqIndex<1>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AlgEqns{},EqIndex<2>{}, VarGroup<K>{}, gradients);
  }

  template<int I, class EquationIndices, class Residuals>
  void evalBlock(EqGroup<I> eqGroup,const EquationIndices& indices, Residuals& residuals)
  {
    int index=0;
    for(auto i : indices){
        residuals[index++] = this->eval(eqGroup,i);
    }
  }

  template<int I, int K, class EquationIndices, class VariableIndices, typename Jacobian>
  void evalBlockJacobian(EqGroup<I> eqGroup, VarGroup<K> varGroup,
      const EquationIndices& equationIndices, const VariableIndices& stateIndices,
      Jacobian& jacobian)
  {
    int jcol =0;
    for(auto j = stateIndices.cbegin(); j != stateIndices.cend(); ++j,++jcol) {
      int irow = 0;
      for(auto i = equationIndices.cbegin(); i != equationIndices.cend();++i,++irow)
        jacobian(irow,jcol) = evalDerivative(eqGroup,*i,varGroup,*j);
    }
  }

  template<int I, int K, typename _Scalar>
  void evalJacobianPattern(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<_Scalar>& jacobian)
  {
    using TripletType = Eigen::Triplet<_Scalar>;
    std::vector<TripletType> triplets;
    triplets.reserve(columnIndices.size());
    using IndexTripletType = Eigen::Triplet<int>;
    std::vector<IndexTripletType> indexTriplets;
    indexTriplets.reserve(columnIndices.size());
    const int numEqns = numEquations(EqGroup<I>{});
    const int startRow = equationIndexOffset(EqGroup<I>{});
    const int stopRow = startRow + numEqns;
    const int numVars = numVariables(VarGroup<K>{});
    const int startCol = variableIndexOffset(VarGroup<K>{});
    const int stopCol = startCol + numVars;
    int index = 0;
    for(int i = startRow; i < stopRow; ++i) {
      for(int p = rowPointers[i]; p < rowPointers[i+1];++p) {
        int j = columnIndices[p];
        if( j >= startCol && j < stopCol){
          triplets.push_back(TripletType{i-startRow,j-startCol,_Scalar{0.}});
          indexTriplets.push_back(IndexTripletType(i-startRow,j-startCol,index++));
        }
      }
    }
    jacobian.resize(numEqns,numVars);
    jacobian.setFromTriplets(triplets.begin(),triplets.end());
    jacobian.makeCompressed();
    Eigen::SparseMatrix<int> indexJacobian;
    indexJacobian.resize(numEqns,numVars);
    indexJacobian.setFromTriplets(indexTriplets.begin(),indexTriplets.end());
    indexJacobian.makeCompressed();
    auto *store = jacobianManager.getJacobianStore(EqGroup<I>{}, VarGroup<K>{});
    const size_t nnz = static_cast<size_t>(indexJacobian.nonZeros());
    store->map.resize(nnz);
    store->store.resize(nnz);
    for(size_t i =0; i < nnz;++i)
      store->map[i] = indexJacobian.valuePtr()[i];
  }

  template<int I, int K>
  void evalJacobianValues(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<RealType>& jacobian)
  {
    auto *store = jacobianManager.getJacobianStore(EqGroup<I>{}, VarGroup<K>{});
    evalAllGradients(EqGroup<I>{},VarGroup<K>{},store->store.data());
    RealType *jacobianValues =  jacobian.valuePtr();
    const int nnz = jacobian.nonZeros();
    for(int i=0; i < nnz; ++i){
      jacobianValues[i] = store->store[store->map[i]];
    }

  }

  template<int I, int K>
  void evalTransposedJacobianValues(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<RealType>& transposedJacobian)
  {
    evalAllGradients(EqGroup<I>{},VarGroup<K>{},transposedJacobian.valuePtr());
  }

  template<int I, int K, typename _Scalar>
  void evalTransposedJacobianPattern(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<_Scalar>& transposedJacobian)
  {
    using TripletType = Eigen::Triplet<_Scalar>;
    std::vector<TripletType> triplets;
    triplets.reserve(columnIndices.size());
    const int numEqns = numEquations(EqGroup<I>{});
    const int startRow = equationIndexOffset(EqGroup<I>{});
    const int stopRow = startRow + numEqns;
    const int numVars = numVariables(VarGroup<K>{});
    const int startCol = variableIndexOffset(VarGroup<K>{});
    const int stopCol = startCol + numVars;
    for(int i = startRow; i < stopRow; ++i) {
      for(int p = rowPointers[i]; p < rowPointers[i+1];++p) {
        int j = columnIndices[p];
        if( j >= startCol && j < stopCol)
          triplets.push_back(TripletType{j-startCol,i-startRow,_Scalar{0.}});
      }
    }
    transposedJacobian.resize(numVars,numEqns);
    transposedJacobian.setFromTriplets(triplets.begin(),triplets.end());
    transposedJacobian.makeCompressed();
  }

  static constexpr int equationIndexOffset(AllEqns){ return 0;}
  static constexpr int equationIndexOffset(DiffEqns){ return equationIndexOffset(AllEqns{});}
  static constexpr int equationIndexOffset(AlgEqns){ return numEquations(DiffEqns{}) + equationIndexOffset(DiffEqns{});}
  static constexpr int variableIndexOffset(AllVars){ return 0;}
  static constexpr int variableIndexOffset(DiffVars){ return 0;}
  static constexpr int variableIndexOffset(AlgVars){ return numVariables(DiffVars{}) + variableIndexOffset(DiffVars{});}
  static constexpr int variableIndexOffset(TimeVar){ return numVariables(AlgVars{}) + variableIndexOffset(AlgVars{});}
  static constexpr int variableIndexOffset(ParamVars){ return numVariables(TimeVar{}) + variableIndexOffset(TimeVar{});}
  static constexpr int variableIndexOffset(DerDiffVars){ return numVariables(ParamVars{}) + variableIndexOffset(ParamVars{}); }
  static constexpr int variableIndexOffset(StateVars){ return 0;}
  static constexpr int variableIndexOffset(StateTimeVars){ return 0;}
  static constexpr int variableIndexOffset(StateTimeParamVars){ return 0;}
  static std::vector<std::string> getVariableNames();
private:
  RealType x(const int i) const { return getVariable(DiffVars{},i); }
  RealType y(const int i) const { return getVariable(AlgVars{},i); }
  RealType der_x(const int i) const { return getVariable(DerDiffVars{},i); }
  RealType p(const int i) const { return getVariable(ParamVars{},i); }
  const RealType c(const int i) const {return  constVariables[i];}
  RealType time() const { return getVariable(TimeVar{},0); }

  template<class Tangents>
  RealType t1_x(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(DiffVars{})]; }
  template<class Tangents>
  RealType t1_y(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(AlgVars{})]; }
  template<class Tangents>
  RealType der_t1_x(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(DerDiffVars{})]; }
  template<class Tangents>
  RealType t1_p(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(ParamVars{})]; }
  const RealType t1_c(const int) const {return static_cast<RealType>(0.);}
  template<class Tangents>
  RealType t1_time(const Tangents& tangents) const { return tangents[variableIndexOffset(TimeVar{})]; }

  int startIndex = 0;
  Eigen::Matrix<RealType,Eigen::Dynamic,1> variables = getInitialValues();
  const std::vector<RealType> constVariables = getConstVariables();
  Eigen::Matrix<RealType,Eigen::Dynamic,1> tangents =
    Eigen::Matrix<RealType,Eigen::Dynamic,1>::Zero(numVariables(AllVars{}),1);
  Eigen::Matrix<RealType,Eigen::Dynamic,1> adjoints =
      Eigen::Matrix<RealType,Eigen::Dynamic,1>::Ones(numEquations(AllEqns{}),1);
  const Eigen::Matrix<RealType,Eigen::Dynamic,1> initialValues = getInitialValues();
  const std::vector<int> rowPointers = getRowPointers();
  const std::vector<int> columnIndices = getColumnIndices();

  static const Eigen::Matrix<RealType,Eigen::Dynamic,1> getInitialValues();
  static const std::vector<RealType> getConstVariables();
  static const std::vector<int> getRowPointers();
  static const std::vector<int> getColumnIndices();
  struct JacobianStore {
    void resize(size_t size) {
      store.resize(size);
      map.resize(size);
    }
    std::vector<RealType> store;
    std::vector<int> map;
  };

  class JacobianManager {
  public:
    template<int I, int J>
    JacobianStore *getJacobianStore(EqGroup<I>,VarGroup<J>)
    {
      static_assert (J < 1024,"Index J of VarGroup<J> is out of range" );
      int key = 1024*I + J;
      if(table.find(key) != table.end()){
        return  table.at(key);
      }
      else {
        stores.emplace_back(std::make_unique<JacobianStore>());
        JacobianStore* entry = stores.back().get();
        table[key] = entry;
        return entry;
      }
    }
  private:
    std::map<int,JacobianStore*> table;
    std::vector<std::unique_ptr<JacobianStore> > stores;
  };
  JacobianManager jacobianManager;
};
// Boiler plate code ends here


template<typename RealType>
const Eigen::Matrix<RealType,Eigen::Dynamic,1> wobatch2Model<RealType>::getInitialValues(){
  Eigen::Matrix<RealType,Eigen::Dynamic,1> initialValues(numVariables(AllVars{}));
  initialValues <<
    static_cast<RealType>(1), // X1
    static_cast<RealType>(0), // X2
    static_cast<RealType>(0), // X3
    static_cast<RealType>(0), // X4
    static_cast<RealType>(0), // X5
    static_cast<RealType>(0), // X6
    static_cast<RealType>(65), // X7
    static_cast<RealType>(2), // X8
    static_cast<RealType>(0), // X9
    static_cast<RealType>(0.), // Fbin
    static_cast<RealType>(0.), // Tw
    static_cast<RealType>(0.), // derX7
    static_cast<RealType>(0.02), // TwCur
    static_cast<RealType>(5.784), // FbinCur
    static_cast<RealType>(60), // TMin
    static_cast<RealType>(1), // FinalTime
    static_cast<RealType>(0.), // der(X1)
    static_cast<RealType>(0.), // der(X2)
    static_cast<RealType>(0.), // der(X3)
    static_cast<RealType>(0.), // der(X4)
    static_cast<RealType>(0.), // der(X5)
    static_cast<RealType>(0.), // der(X6)
    static_cast<RealType>(0.), // der(X7)
    static_cast<RealType>(0.), // der(X8)
    static_cast<RealType>(0.) // der(X9)
  ;
  return initialValues;
}

template<typename RealType>
const std::vector<RealType> wobatch2Model<RealType>::getConstVariables(){
  return
  {  static_cast<RealType>(9), // n_x
    static_cast<RealType>(35), // Tin
    static_cast<RealType>(6.6667), // b2
    static_cast<RealType>(0), // FbinMin
    static_cast<RealType>(5.784), // FbinMax
    static_cast<RealType>(0.02), // TwMin
    static_cast<RealType>(0.1) // TwMax
  };
}


template<typename RealType>
std::vector<std::string>  wobatch2Model<RealType>::getVariableNames(){
  std::vector<std::string> names;
  names.reserve(static_cast<size_t>(numVariables(AllVars{})));

  names.push_back("X1");
  names.push_back("X2");
  names.push_back("X3");
  names.push_back("X4");
  names.push_back("X5");
  names.push_back("X6");
  names.push_back("X7");
  names.push_back("X8");
  names.push_back("X9");
  names.push_back("Fbin");
  names.push_back("Tw");
  names.push_back("derX7");
  names.push_back("TwCur");
  names.push_back("FbinCur");
  names.push_back("TMin");
  names.push_back("FinalTime");
  names.push_back("der(X1)");
  names.push_back("der(X2)");
  names.push_back("der(X3)");
  names.push_back("der(X4)");
  names.push_back("der(X5)");
  names.push_back("der(X6)");
  names.push_back("der(X7)");
  names.push_back("der(X8)");
  names.push_back("der(X9)");
  
  return names;
}

template<typename RealType>
const std::vector<int> wobatch2Model<RealType>::getRowPointers(){
  return {
    0,
    7,
    15,
    24,
    32,
    40,
    48,
    51,
    58,
    60,
    70,
    72,
    74 };
}

template<typename RealType>
const std::vector<int> wobatch2Model<RealType>::getColumnIndices(){
  return {
    0,
    1,
    6,
    7,
    9,
    15,
    16,
    0,
    1,
    2,
    6,
    7,
    9,
    15,
    17,
    0,
    1,
    2,
    3,
    6,
    7,
    9,
    15,
    18,
    1,
    2,
    3,
    6,
    7,
    9,
    15,
    19,
    1,
    2,
    4,
    6,
    7,
    9,
    15,
    20,
    2,
    3,
    5,
    6,
    7,
    9,
    15,
    21,
    9,
    15,
    23,
    1,
    2,
    3,
    6,
    7,
    15,
    24,
    11,
    22,
    0,
    1,
    2,
    3,
    6,
    7,
    9,
    10,
    11,
    15,
    9,
    13,
    10,
    12 };
}

#ifdef _MSC_BUILD
#pragma warning(pop)
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif


#endif // wobatch2_MODEL_HPP
