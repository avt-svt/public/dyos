#include "wobatch2_t1_eval.hpp"

template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<0>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<1>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<2>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<3>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<4>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<5>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<6>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<7>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<8>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<9>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<10>, double const * const &tangents) const;
template double wobatch2Model<double>::evalTangent<double const*>(AllEqns,
    EqIndex<11>, double const * const &tangents) const;

