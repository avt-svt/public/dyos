#ifndef wobatch2_MODEL_EVAL_HPP
#define wobatch2_MODEL_EVAL_HPP

#include "wobatch2.hpp"

template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<0>) const {
    using std::exp;
    RealType v0 = p(3);
    RealType v1 = x(0);
    RealType v2 = y(0);
    RealType v3 = v1*v2;
    const RealType v4 = static_cast<RealType>(1000.0);
    RealType v5 = x(7);
    RealType v6 = v4*v5;
    RealType v7 = v3/v6;
    const RealType v8 = static_cast<RealType>(1659900.0);
    const RealType v9 = static_cast<RealType>(1000.0);
    RealType v10 = c(2);
    RealType v11 = v9*v10;
    const RealType v12 = static_cast<RealType>(273.15);
    RealType v13 = x(6);
    RealType v14 = v12+v13;
    RealType v15 = v11/v14;
    RealType v16 = -v15;
    RealType v17 = exp(v16);
    RealType v18 = v8*v17;
    RealType v19 = v18*v1;
    RealType v20 = x(1);
    RealType v21 = v19*v20;
    RealType v22 = v7+v21;
    RealType v23 = v0*v22;
    RealType v24 = -v23;
    RealType v25 = der_x(0);
    RealType v26 = v24-v25;
    return v26;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<1>) const {
    using std::exp;
    RealType v0 = p(3);
    RealType v1 = y(0);
    const RealType v2 = static_cast<RealType>(1);
    RealType v3 = x(1);
    RealType v4 = v2-v3;
    RealType v5 = v1*v4;
    const RealType v6 = static_cast<RealType>(1000.0);
    RealType v7 = x(7);
    RealType v8 = v6*v7;
    RealType v9 = v5/v8;
    const RealType v10 = static_cast<RealType>(1659900.0);
    const RealType v11 = static_cast<RealType>(1000.0);
    RealType v12 = c(2);
    RealType v13 = v11*v12;
    const RealType v14 = static_cast<RealType>(273.15);
    RealType v15 = x(6);
    RealType v16 = v14+v15;
    RealType v17 = v13/v16;
    RealType v18 = -v17;
    RealType v19 = exp(v18);
    RealType v20 = v10*v19;
    RealType v21 = x(0);
    RealType v22 = v20*v21;
    RealType v23 = v22*v3;
    const RealType v24 = static_cast<RealType>(721170000.0);
    const RealType v25 = static_cast<RealType>(8333.3);
    const RealType v26 = static_cast<RealType>(273.15);
    RealType v27 = v26+v15;
    RealType v28 = v25/v27;
    RealType v29 = -v28;
    RealType v30 = exp(v29);
    RealType v31 = v24*v30;
    RealType v32 = v31*v3;
    RealType v33 = x(2);
    RealType v34 = v32*v33;
    RealType v35 = v23+v34;
    RealType v36 = v9-v35;
    RealType v37 = v0*v36;
    RealType v38 = der_x(1);
    RealType v39 = v37-v38;
    return v39;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<2>) const {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(3319800.0);
    const RealType v2 = static_cast<RealType>(1000.0);
    RealType v3 = c(2);
    RealType v4 = v2*v3;
    const RealType v5 = static_cast<RealType>(273.15);
    RealType v6 = x(6);
    RealType v7 = v5+v6;
    RealType v8 = v4/v7;
    RealType v9 = -v8;
    RealType v10 = exp(v9);
    RealType v11 = v1*v10;
    RealType v12 = x(0);
    RealType v13 = v11*v12;
    RealType v14 = x(1);
    RealType v15 = v13*v14;
    const RealType v16 = static_cast<RealType>(1442340000.0);
    const RealType v17 = static_cast<RealType>(8333.3);
    const RealType v18 = static_cast<RealType>(273.15);
    RealType v19 = v18+v6;
    RealType v20 = v17/v19;
    RealType v21 = -v20;
    RealType v22 = exp(v21);
    RealType v23 = v16*v22;
    RealType v24 = v23*v14;
    RealType v25 = x(2);
    RealType v26 = v24*v25;
    RealType v27 = v15-v26;
    const RealType v28 = static_cast<RealType>(2674500000000.0);
    const RealType v29 = static_cast<RealType>(11111.0);
    const RealType v30 = static_cast<RealType>(273.15);
    RealType v31 = v30+v6;
    RealType v32 = v29/v31;
    RealType v33 = -v32;
    RealType v34 = exp(v33);
    RealType v35 = v28*v34;
    RealType v36 = v35*v25;
    RealType v37 = x(3);
    RealType v38 = v36*v37;
    RealType v39 = v27-v38;
    RealType v40 = y(0);
    RealType v41 = v25*v40;
    const RealType v42 = static_cast<RealType>(1000.0);
    RealType v43 = x(7);
    RealType v44 = v42*v43;
    RealType v45 = v41/v44;
    RealType v46 = v39-v45;
    RealType v47 = v0*v46;
    RealType v48 = der_x(2);
    RealType v49 = v47-v48;
    return v49;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<3>) const {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(721170000.0);
    const RealType v2 = static_cast<RealType>(8333.3);
    const RealType v3 = static_cast<RealType>(273.15);
    RealType v4 = x(6);
    RealType v5 = v3+v4;
    RealType v6 = v2/v5;
    RealType v7 = -v6;
    RealType v8 = exp(v7);
    RealType v9 = v1*v8;
    RealType v10 = x(1);
    RealType v11 = v9*v10;
    RealType v12 = x(2);
    RealType v13 = v11*v12;
    const RealType v14 = static_cast<RealType>(1337250000000.0);
    const RealType v15 = static_cast<RealType>(11111.0);
    const RealType v16 = static_cast<RealType>(273.15);
    RealType v17 = v16+v4;
    RealType v18 = v15/v17;
    RealType v19 = -v18;
    RealType v20 = exp(v19);
    RealType v21 = v14*v20;
    RealType v22 = v21*v12;
    RealType v23 = x(3);
    RealType v24 = v22*v23;
    RealType v25 = v13-v24;
    RealType v26 = y(0);
    RealType v27 = v23*v26;
    const RealType v28 = static_cast<RealType>(1000.0);
    RealType v29 = x(7);
    RealType v30 = v28*v29;
    RealType v31 = v27/v30;
    RealType v32 = v25-v31;
    RealType v33 = v0*v32;
    RealType v34 = der_x(3);
    RealType v35 = v33-v34;
    return v35;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<4>) const {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(1442340000.0);
    const RealType v2 = static_cast<RealType>(8333.3);
    const RealType v3 = static_cast<RealType>(273.15);
    RealType v4 = x(6);
    RealType v5 = v3+v4;
    RealType v6 = v2/v5;
    RealType v7 = -v6;
    RealType v8 = exp(v7);
    RealType v9 = v1*v8;
    RealType v10 = x(1);
    RealType v11 = v9*v10;
    RealType v12 = x(2);
    RealType v13 = v11*v12;
    RealType v14 = x(4);
    RealType v15 = y(0);
    RealType v16 = v14*v15;
    const RealType v17 = static_cast<RealType>(1000.0);
    RealType v18 = x(7);
    RealType v19 = v17*v18;
    RealType v20 = v16/v19;
    RealType v21 = v13-v20;
    RealType v22 = v0*v21;
    RealType v23 = der_x(4);
    RealType v24 = v22-v23;
    return v24;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<5>) const {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(4011750000000.0);
    const RealType v2 = static_cast<RealType>(11111.0);
    const RealType v3 = static_cast<RealType>(273.15);
    RealType v4 = x(6);
    RealType v5 = v3+v4;
    RealType v6 = v2/v5;
    RealType v7 = -v6;
    RealType v8 = exp(v7);
    RealType v9 = v1*v8;
    RealType v10 = x(3);
    RealType v11 = v9*v10;
    RealType v12 = x(2);
    RealType v13 = v11*v12;
    RealType v14 = x(5);
    RealType v15 = y(0);
    RealType v16 = v14*v15;
    const RealType v17 = static_cast<RealType>(1000.0);
    RealType v18 = x(7);
    RealType v19 = v17*v18;
    RealType v20 = v16/v19;
    RealType v21 = v13-v20;
    RealType v22 = v0*v21;
    RealType v23 = der_x(5);
    RealType v24 = v22-v23;
    return v24;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<6>) const {
    const RealType v0 = static_cast<RealType>(0.001);
    RealType v1 = p(3);
    RealType v2 = v0*v1;
    RealType v3 = y(0);
    RealType v4 = v2*v3;
    RealType v5 = der_x(7);
    RealType v6 = v4-v5;
    return v6;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<7>) const {
    using std::exp;
    RealType v0 = p(3);
    const RealType v1 = static_cast<RealType>(5554.1);
    RealType v2 = -v1;
    const RealType v3 = static_cast<RealType>(721170000.0);
    const RealType v4 = static_cast<RealType>(8333.3);
    const RealType v5 = static_cast<RealType>(273.15);
    RealType v6 = x(6);
    RealType v7 = v5+v6;
    RealType v8 = v4/v7;
    RealType v9 = -v8;
    RealType v10 = exp(v9);
    RealType v11 = v3*v10;
    RealType v12 = x(1);
    RealType v13 = v11*v12;
    RealType v14 = x(2);
    RealType v15 = v13*v14;
    const RealType v16 = static_cast<RealType>(1337250000000.0);
    const RealType v17 = static_cast<RealType>(11111.0);
    const RealType v18 = static_cast<RealType>(273.15);
    RealType v19 = v18+v6;
    RealType v20 = v17/v19;
    RealType v21 = -v20;
    RealType v22 = exp(v21);
    RealType v23 = v16*v22;
    RealType v24 = v23*v14;
    RealType v25 = x(3);
    RealType v26 = v24*v25;
    RealType v27 = v15-v26;
    RealType v28 = v2*v27;
    RealType v29 = x(7);
    RealType v30 = v28*v29;
    const RealType v31 = static_cast<RealType>(181605029400.0);
    const RealType v32 = static_cast<RealType>(8333.3);
    const RealType v33 = static_cast<RealType>(273.15);
    RealType v34 = v33+v6;
    RealType v35 = v32/v34;
    RealType v36 = -v35;
    RealType v37 = exp(v36);
    RealType v38 = v31*v37;
    RealType v39 = v38*v12;
    RealType v40 = v39*v14;
    RealType v41 = v40*v29;
    RealType v42 = v30-v41;
    RealType v43 = v0*v42;
    RealType v44 = der_x(8);
    RealType v45 = v43-v44;
    return v45;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<8>) const {
    RealType v0 = y(2);
    RealType v1 = der_x(6);
    RealType v2 = v0-v1;
    return v2;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<9>) const {
    using std::exp;
    RealType v0 = p(3);
    RealType v1 = y(0);
    RealType v2 = c(1);
    RealType v3 = v1*v2;
    const RealType v4 = static_cast<RealType>(1000);
    RealType v5 = x(7);
    RealType v6 = v4*v5;
    RealType v7 = v3/v6;
    const RealType v8 = static_cast<RealType>(0.000239005736137667);
    const RealType v9 = static_cast<RealType>(437881620000.0);
    const RealType v10 = static_cast<RealType>(1000.0);
    RealType v11 = c(2);
    RealType v12 = v10*v11;
    const RealType v13 = static_cast<RealType>(273.15);
    RealType v14 = x(6);
    RealType v15 = v13+v14;
    RealType v16 = v12/v15;
    RealType v17 = -v16;
    RealType v18 = exp(v17);
    RealType v19 = v9*v18;
    RealType v20 = x(0);
    RealType v21 = v19*v20;
    RealType v22 = x(1);
    RealType v23 = v21*v22;
    const RealType v24 = static_cast<RealType>(114161211000000.0);
    const RealType v25 = static_cast<RealType>(8333.3);
    const RealType v26 = static_cast<RealType>(273.15);
    RealType v27 = v26+v14;
    RealType v28 = v25/v27;
    RealType v29 = -v28;
    RealType v30 = exp(v29);
    RealType v31 = v24*v30;
    RealType v32 = v31*v22;
    RealType v33 = x(2);
    RealType v34 = v32*v33;
    RealType v35 = v23+v34;
    const RealType v36 = static_cast<RealType>(6.0523935E+017);
    const RealType v37 = static_cast<RealType>(11111.0);
    const RealType v38 = static_cast<RealType>(273.15);
    RealType v39 = v38+v14;
    RealType v40 = v37/v39;
    RealType v41 = -v40;
    RealType v42 = exp(v41);
    RealType v43 = v36*v42;
    RealType v44 = v43*v33;
    RealType v45 = x(3);
    RealType v46 = v44*v45;
    RealType v47 = v35+v46;
    RealType v48 = v8*v47;
    RealType v49 = v7+v48;
    const RealType v50 = static_cast<RealType>(0.000243454685774894);
    const RealType v51 = static_cast<RealType>(1000.0);
    RealType v52 = y(1);
    RealType v53 = v51*v52;
    RealType v54 = v14-v53;
    RealType v55 = v50*v54;
    RealType v56 = v49-v55;
    RealType v57 = v1*v14;
    const RealType v58 = static_cast<RealType>(1000);
    RealType v59 = v58*v5;
    RealType v60 = v57/v59;
    RealType v61 = v56-v60;
    RealType v62 = v0*v61;
    RealType v63 = y(2);
    RealType v64 = v62-v63;
    return v64;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<10>) const {
    RealType v0 = p(1);
    RealType v1 = y(0);
    RealType v2 = v0-v1;
    return v2;
}
template<typename RealType> RealType
wobatch2Model<RealType>::eval(AllEqns, EqIndex<11>) const {
    RealType v0 = p(0);
    RealType v1 = y(1);
    RealType v2 = v0-v1;
    return v2;
}


#endif // wobatch2_MODEL_EVAL_HPP
