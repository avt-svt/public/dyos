#ifndef wobatch2_MODEL_T1_EVAL_HPP
#define wobatch2_MODEL_T1_EVAL_HPP

#include "wobatch2.hpp"

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<0>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    RealType v1 = x(0);
    RealType t1_v1 = t1_x(0, tangents);
    RealType v2 = y(0);
    RealType t1_v2 = t1_y(0, tangents);
    RealType v3 = v1*v2;
    RealType t1_v3 = t1_v1*v2 + v1*t1_v2;
    const RealType v4 = static_cast<RealType>(1000.0);
    const RealType t1_v4 = static_cast<RealType>(0.);
    RealType v5 = x(7);
    RealType t1_v5 = t1_x(7, tangents);
    RealType v6 = v4*v5;
    RealType t1_v6 = t1_v4*v5 + v4*t1_v5;
    RealType v7 = v3/v6;
    RealType t1_v7 =  ( v6 * t1_v3 - v3 * t1_v6 ) / ( v6 * v6 );
    const RealType v8 = static_cast<RealType>(1659900.0);
    const RealType t1_v8 = static_cast<RealType>(0.);
    const RealType v9 = static_cast<RealType>(1000.0);
    const RealType t1_v9 = static_cast<RealType>(0.);
    RealType v10 = c(2);
    RealType t1_v10 = static_cast<RealType>(0.);
    RealType v11 = v9*v10;
    RealType t1_v11 = t1_v9*v10 + v9*t1_v10;
    const RealType v12 = static_cast<RealType>(273.15);
    const RealType t1_v12 = static_cast<RealType>(0.);
    RealType v13 = x(6);
    RealType t1_v13 = t1_x(6, tangents);
    RealType v14 = v12+v13;
    RealType t1_v14 = t1_v12+t1_v13;
    RealType v15 = v11/v14;
    RealType t1_v15 =  ( v14 * t1_v11 - v11 * t1_v14 ) / ( v14 * v14 );
    RealType v16 = -v15;
    RealType t1_v16 = -t1_v15;
    RealType v17 = exp(v16);
    RealType t1_v17 = t1_v16 * exp(v16);
    RealType v18 = v8*v17;
    RealType t1_v18 = t1_v8*v17 + v8*t1_v17;
    RealType v19 = v18*v1;
    RealType t1_v19 = t1_v18*v1 + v18*t1_v1;
    RealType v20 = x(1);
    RealType t1_v20 = t1_x(1, tangents);
    RealType v21 = v19*v20;
    RealType t1_v21 = t1_v19*v20 + v19*t1_v20;
    RealType v22 = v7+v21;
    RealType t1_v22 = t1_v7+t1_v21;
    RealType v23 = v0*v22;
    RealType t1_v23 = t1_v0*v22 + v0*t1_v22;
    RealType v24 = -v23;
    RealType t1_v24 = -t1_v23;
    RealType v25 = der_x(0);
    RealType t1_v25 = der_t1_x(0, tangents);
    RealType v26 = v24-v25;
    RealType t1_v26 = t1_v24 - t1_v25;
    return t1_v26;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<1>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    RealType v1 = y(0);
    RealType t1_v1 = t1_y(0, tangents);
    const RealType v2 = static_cast<RealType>(1);
    const RealType t1_v2 = static_cast<RealType>(0.);
    RealType v3 = x(1);
    RealType t1_v3 = t1_x(1, tangents);
    RealType v4 = v2-v3;
    RealType t1_v4 = t1_v2-t1_v3;
    RealType v5 = v1*v4;
    RealType t1_v5 = t1_v1*v4 + v1*t1_v4;
    const RealType v6 = static_cast<RealType>(1000.0);
    const RealType t1_v6 = static_cast<RealType>(0.);
    RealType v7 = x(7);
    RealType t1_v7 = t1_x(7, tangents);
    RealType v8 = v6*v7;
    RealType t1_v8 = t1_v6*v7 + v6*t1_v7;
    RealType v9 = v5/v8;
    RealType t1_v9 =  ( v8 * t1_v5 - v5 * t1_v8 ) / ( v8 * v8 );
    const RealType v10 = static_cast<RealType>(1659900.0);
    const RealType t1_v10 = static_cast<RealType>(0.);
    const RealType v11 = static_cast<RealType>(1000.0);
    const RealType t1_v11 = static_cast<RealType>(0.);
    RealType v12 = c(2);
    RealType t1_v12 = static_cast<RealType>(0.);
    RealType v13 = v11*v12;
    RealType t1_v13 = t1_v11*v12 + v11*t1_v12;
    const RealType v14 = static_cast<RealType>(273.15);
    const RealType t1_v14 = static_cast<RealType>(0.);
    RealType v15 = x(6);
    RealType t1_v15 = t1_x(6, tangents);
    RealType v16 = v14+v15;
    RealType t1_v16 = t1_v14+t1_v15;
    RealType v17 = v13/v16;
    RealType t1_v17 =  ( v16 * t1_v13 - v13 * t1_v16 ) / ( v16 * v16 );
    RealType v18 = -v17;
    RealType t1_v18 = -t1_v17;
    RealType v19 = exp(v18);
    RealType t1_v19 = t1_v18 * exp(v18);
    RealType v20 = v10*v19;
    RealType t1_v20 = t1_v10*v19 + v10*t1_v19;
    RealType v21 = x(0);
    RealType t1_v21 = t1_x(0, tangents);
    RealType v22 = v20*v21;
    RealType t1_v22 = t1_v20*v21 + v20*t1_v21;
    RealType v23 = v22*v3;
    RealType t1_v23 = t1_v22*v3 + v22*t1_v3;
    const RealType v24 = static_cast<RealType>(721170000.0);
    const RealType t1_v24 = static_cast<RealType>(0.);
    const RealType v25 = static_cast<RealType>(8333.3);
    const RealType t1_v25 = static_cast<RealType>(0.);
    const RealType v26 = static_cast<RealType>(273.15);
    const RealType t1_v26 = static_cast<RealType>(0.);
    RealType v27 = v26+v15;
    RealType t1_v27 = t1_v26+t1_v15;
    RealType v28 = v25/v27;
    RealType t1_v28 =  ( v27 * t1_v25 - v25 * t1_v27 ) / ( v27 * v27 );
    RealType v29 = -v28;
    RealType t1_v29 = -t1_v28;
    RealType v30 = exp(v29);
    RealType t1_v30 = t1_v29 * exp(v29);
    RealType v31 = v24*v30;
    RealType t1_v31 = t1_v24*v30 + v24*t1_v30;
    RealType v32 = v31*v3;
    RealType t1_v32 = t1_v31*v3 + v31*t1_v3;
    RealType v33 = x(2);
    RealType t1_v33 = t1_x(2, tangents);
    RealType v34 = v32*v33;
    RealType t1_v34 = t1_v32*v33 + v32*t1_v33;
    RealType v35 = v23+v34;
    RealType t1_v35 = t1_v23+t1_v34;
    RealType v36 = v9-v35;
    RealType t1_v36 = t1_v9-t1_v35;
    RealType v37 = v0*v36;
    RealType t1_v37 = t1_v0*v36 + v0*t1_v36;
    RealType v38 = der_x(1);
    RealType t1_v38 = der_t1_x(1, tangents);
    RealType v39 = v37-v38;
    RealType t1_v39 = t1_v37 - t1_v38;
    return t1_v39;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<2>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    const RealType v1 = static_cast<RealType>(3319800.0);
    const RealType t1_v1 = static_cast<RealType>(0.);
    const RealType v2 = static_cast<RealType>(1000.0);
    const RealType t1_v2 = static_cast<RealType>(0.);
    RealType v3 = c(2);
    RealType t1_v3 = static_cast<RealType>(0.);
    RealType v4 = v2*v3;
    RealType t1_v4 = t1_v2*v3 + v2*t1_v3;
    const RealType v5 = static_cast<RealType>(273.15);
    const RealType t1_v5 = static_cast<RealType>(0.);
    RealType v6 = x(6);
    RealType t1_v6 = t1_x(6, tangents);
    RealType v7 = v5+v6;
    RealType t1_v7 = t1_v5+t1_v6;
    RealType v8 = v4/v7;
    RealType t1_v8 =  ( v7 * t1_v4 - v4 * t1_v7 ) / ( v7 * v7 );
    RealType v9 = -v8;
    RealType t1_v9 = -t1_v8;
    RealType v10 = exp(v9);
    RealType t1_v10 = t1_v9 * exp(v9);
    RealType v11 = v1*v10;
    RealType t1_v11 = t1_v1*v10 + v1*t1_v10;
    RealType v12 = x(0);
    RealType t1_v12 = t1_x(0, tangents);
    RealType v13 = v11*v12;
    RealType t1_v13 = t1_v11*v12 + v11*t1_v12;
    RealType v14 = x(1);
    RealType t1_v14 = t1_x(1, tangents);
    RealType v15 = v13*v14;
    RealType t1_v15 = t1_v13*v14 + v13*t1_v14;
    const RealType v16 = static_cast<RealType>(1442340000.0);
    const RealType t1_v16 = static_cast<RealType>(0.);
    const RealType v17 = static_cast<RealType>(8333.3);
    const RealType t1_v17 = static_cast<RealType>(0.);
    const RealType v18 = static_cast<RealType>(273.15);
    const RealType t1_v18 = static_cast<RealType>(0.);
    RealType v19 = v18+v6;
    RealType t1_v19 = t1_v18+t1_v6;
    RealType v20 = v17/v19;
    RealType t1_v20 =  ( v19 * t1_v17 - v17 * t1_v19 ) / ( v19 * v19 );
    RealType v21 = -v20;
    RealType t1_v21 = -t1_v20;
    RealType v22 = exp(v21);
    RealType t1_v22 = t1_v21 * exp(v21);
    RealType v23 = v16*v22;
    RealType t1_v23 = t1_v16*v22 + v16*t1_v22;
    RealType v24 = v23*v14;
    RealType t1_v24 = t1_v23*v14 + v23*t1_v14;
    RealType v25 = x(2);
    RealType t1_v25 = t1_x(2, tangents);
    RealType v26 = v24*v25;
    RealType t1_v26 = t1_v24*v25 + v24*t1_v25;
    RealType v27 = v15-v26;
    RealType t1_v27 = t1_v15-t1_v26;
    const RealType v28 = static_cast<RealType>(2674500000000.0);
    const RealType t1_v28 = static_cast<RealType>(0.);
    const RealType v29 = static_cast<RealType>(11111.0);
    const RealType t1_v29 = static_cast<RealType>(0.);
    const RealType v30 = static_cast<RealType>(273.15);
    const RealType t1_v30 = static_cast<RealType>(0.);
    RealType v31 = v30+v6;
    RealType t1_v31 = t1_v30+t1_v6;
    RealType v32 = v29/v31;
    RealType t1_v32 =  ( v31 * t1_v29 - v29 * t1_v31 ) / ( v31 * v31 );
    RealType v33 = -v32;
    RealType t1_v33 = -t1_v32;
    RealType v34 = exp(v33);
    RealType t1_v34 = t1_v33 * exp(v33);
    RealType v35 = v28*v34;
    RealType t1_v35 = t1_v28*v34 + v28*t1_v34;
    RealType v36 = v35*v25;
    RealType t1_v36 = t1_v35*v25 + v35*t1_v25;
    RealType v37 = x(3);
    RealType t1_v37 = t1_x(3, tangents);
    RealType v38 = v36*v37;
    RealType t1_v38 = t1_v36*v37 + v36*t1_v37;
    RealType v39 = v27-v38;
    RealType t1_v39 = t1_v27-t1_v38;
    RealType v40 = y(0);
    RealType t1_v40 = t1_y(0, tangents);
    RealType v41 = v25*v40;
    RealType t1_v41 = t1_v25*v40 + v25*t1_v40;
    const RealType v42 = static_cast<RealType>(1000.0);
    const RealType t1_v42 = static_cast<RealType>(0.);
    RealType v43 = x(7);
    RealType t1_v43 = t1_x(7, tangents);
    RealType v44 = v42*v43;
    RealType t1_v44 = t1_v42*v43 + v42*t1_v43;
    RealType v45 = v41/v44;
    RealType t1_v45 =  ( v44 * t1_v41 - v41 * t1_v44 ) / ( v44 * v44 );
    RealType v46 = v39-v45;
    RealType t1_v46 = t1_v39-t1_v45;
    RealType v47 = v0*v46;
    RealType t1_v47 = t1_v0*v46 + v0*t1_v46;
    RealType v48 = der_x(2);
    RealType t1_v48 = der_t1_x(2, tangents);
    RealType v49 = v47-v48;
    RealType t1_v49 = t1_v47 - t1_v48;
    return t1_v49;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<3>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    const RealType v1 = static_cast<RealType>(721170000.0);
    const RealType t1_v1 = static_cast<RealType>(0.);
    const RealType v2 = static_cast<RealType>(8333.3);
    const RealType t1_v2 = static_cast<RealType>(0.);
    const RealType v3 = static_cast<RealType>(273.15);
    const RealType t1_v3 = static_cast<RealType>(0.);
    RealType v4 = x(6);
    RealType t1_v4 = t1_x(6, tangents);
    RealType v5 = v3+v4;
    RealType t1_v5 = t1_v3+t1_v4;
    RealType v6 = v2/v5;
    RealType t1_v6 =  ( v5 * t1_v2 - v2 * t1_v5 ) / ( v5 * v5 );
    RealType v7 = -v6;
    RealType t1_v7 = -t1_v6;
    RealType v8 = exp(v7);
    RealType t1_v8 = t1_v7 * exp(v7);
    RealType v9 = v1*v8;
    RealType t1_v9 = t1_v1*v8 + v1*t1_v8;
    RealType v10 = x(1);
    RealType t1_v10 = t1_x(1, tangents);
    RealType v11 = v9*v10;
    RealType t1_v11 = t1_v9*v10 + v9*t1_v10;
    RealType v12 = x(2);
    RealType t1_v12 = t1_x(2, tangents);
    RealType v13 = v11*v12;
    RealType t1_v13 = t1_v11*v12 + v11*t1_v12;
    const RealType v14 = static_cast<RealType>(1337250000000.0);
    const RealType t1_v14 = static_cast<RealType>(0.);
    const RealType v15 = static_cast<RealType>(11111.0);
    const RealType t1_v15 = static_cast<RealType>(0.);
    const RealType v16 = static_cast<RealType>(273.15);
    const RealType t1_v16 = static_cast<RealType>(0.);
    RealType v17 = v16+v4;
    RealType t1_v17 = t1_v16+t1_v4;
    RealType v18 = v15/v17;
    RealType t1_v18 =  ( v17 * t1_v15 - v15 * t1_v17 ) / ( v17 * v17 );
    RealType v19 = -v18;
    RealType t1_v19 = -t1_v18;
    RealType v20 = exp(v19);
    RealType t1_v20 = t1_v19 * exp(v19);
    RealType v21 = v14*v20;
    RealType t1_v21 = t1_v14*v20 + v14*t1_v20;
    RealType v22 = v21*v12;
    RealType t1_v22 = t1_v21*v12 + v21*t1_v12;
    RealType v23 = x(3);
    RealType t1_v23 = t1_x(3, tangents);
    RealType v24 = v22*v23;
    RealType t1_v24 = t1_v22*v23 + v22*t1_v23;
    RealType v25 = v13-v24;
    RealType t1_v25 = t1_v13-t1_v24;
    RealType v26 = y(0);
    RealType t1_v26 = t1_y(0, tangents);
    RealType v27 = v23*v26;
    RealType t1_v27 = t1_v23*v26 + v23*t1_v26;
    const RealType v28 = static_cast<RealType>(1000.0);
    const RealType t1_v28 = static_cast<RealType>(0.);
    RealType v29 = x(7);
    RealType t1_v29 = t1_x(7, tangents);
    RealType v30 = v28*v29;
    RealType t1_v30 = t1_v28*v29 + v28*t1_v29;
    RealType v31 = v27/v30;
    RealType t1_v31 =  ( v30 * t1_v27 - v27 * t1_v30 ) / ( v30 * v30 );
    RealType v32 = v25-v31;
    RealType t1_v32 = t1_v25-t1_v31;
    RealType v33 = v0*v32;
    RealType t1_v33 = t1_v0*v32 + v0*t1_v32;
    RealType v34 = der_x(3);
    RealType t1_v34 = der_t1_x(3, tangents);
    RealType v35 = v33-v34;
    RealType t1_v35 = t1_v33 - t1_v34;
    return t1_v35;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<4>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    const RealType v1 = static_cast<RealType>(1442340000.0);
    const RealType t1_v1 = static_cast<RealType>(0.);
    const RealType v2 = static_cast<RealType>(8333.3);
    const RealType t1_v2 = static_cast<RealType>(0.);
    const RealType v3 = static_cast<RealType>(273.15);
    const RealType t1_v3 = static_cast<RealType>(0.);
    RealType v4 = x(6);
    RealType t1_v4 = t1_x(6, tangents);
    RealType v5 = v3+v4;
    RealType t1_v5 = t1_v3+t1_v4;
    RealType v6 = v2/v5;
    RealType t1_v6 =  ( v5 * t1_v2 - v2 * t1_v5 ) / ( v5 * v5 );
    RealType v7 = -v6;
    RealType t1_v7 = -t1_v6;
    RealType v8 = exp(v7);
    RealType t1_v8 = t1_v7 * exp(v7);
    RealType v9 = v1*v8;
    RealType t1_v9 = t1_v1*v8 + v1*t1_v8;
    RealType v10 = x(1);
    RealType t1_v10 = t1_x(1, tangents);
    RealType v11 = v9*v10;
    RealType t1_v11 = t1_v9*v10 + v9*t1_v10;
    RealType v12 = x(2);
    RealType t1_v12 = t1_x(2, tangents);
    RealType v13 = v11*v12;
    RealType t1_v13 = t1_v11*v12 + v11*t1_v12;
    RealType v14 = x(4);
    RealType t1_v14 = t1_x(4, tangents);
    RealType v15 = y(0);
    RealType t1_v15 = t1_y(0, tangents);
    RealType v16 = v14*v15;
    RealType t1_v16 = t1_v14*v15 + v14*t1_v15;
    const RealType v17 = static_cast<RealType>(1000.0);
    const RealType t1_v17 = static_cast<RealType>(0.);
    RealType v18 = x(7);
    RealType t1_v18 = t1_x(7, tangents);
    RealType v19 = v17*v18;
    RealType t1_v19 = t1_v17*v18 + v17*t1_v18;
    RealType v20 = v16/v19;
    RealType t1_v20 =  ( v19 * t1_v16 - v16 * t1_v19 ) / ( v19 * v19 );
    RealType v21 = v13-v20;
    RealType t1_v21 = t1_v13-t1_v20;
    RealType v22 = v0*v21;
    RealType t1_v22 = t1_v0*v21 + v0*t1_v21;
    RealType v23 = der_x(4);
    RealType t1_v23 = der_t1_x(4, tangents);
    RealType v24 = v22-v23;
    RealType t1_v24 = t1_v22 - t1_v23;
    return t1_v24;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<5>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    const RealType v1 = static_cast<RealType>(4011750000000.0);
    const RealType t1_v1 = static_cast<RealType>(0.);
    const RealType v2 = static_cast<RealType>(11111.0);
    const RealType t1_v2 = static_cast<RealType>(0.);
    const RealType v3 = static_cast<RealType>(273.15);
    const RealType t1_v3 = static_cast<RealType>(0.);
    RealType v4 = x(6);
    RealType t1_v4 = t1_x(6, tangents);
    RealType v5 = v3+v4;
    RealType t1_v5 = t1_v3+t1_v4;
    RealType v6 = v2/v5;
    RealType t1_v6 =  ( v5 * t1_v2 - v2 * t1_v5 ) / ( v5 * v5 );
    RealType v7 = -v6;
    RealType t1_v7 = -t1_v6;
    RealType v8 = exp(v7);
    RealType t1_v8 = t1_v7 * exp(v7);
    RealType v9 = v1*v8;
    RealType t1_v9 = t1_v1*v8 + v1*t1_v8;
    RealType v10 = x(3);
    RealType t1_v10 = t1_x(3, tangents);
    RealType v11 = v9*v10;
    RealType t1_v11 = t1_v9*v10 + v9*t1_v10;
    RealType v12 = x(2);
    RealType t1_v12 = t1_x(2, tangents);
    RealType v13 = v11*v12;
    RealType t1_v13 = t1_v11*v12 + v11*t1_v12;
    RealType v14 = x(5);
    RealType t1_v14 = t1_x(5, tangents);
    RealType v15 = y(0);
    RealType t1_v15 = t1_y(0, tangents);
    RealType v16 = v14*v15;
    RealType t1_v16 = t1_v14*v15 + v14*t1_v15;
    const RealType v17 = static_cast<RealType>(1000.0);
    const RealType t1_v17 = static_cast<RealType>(0.);
    RealType v18 = x(7);
    RealType t1_v18 = t1_x(7, tangents);
    RealType v19 = v17*v18;
    RealType t1_v19 = t1_v17*v18 + v17*t1_v18;
    RealType v20 = v16/v19;
    RealType t1_v20 =  ( v19 * t1_v16 - v16 * t1_v19 ) / ( v19 * v19 );
    RealType v21 = v13-v20;
    RealType t1_v21 = t1_v13-t1_v20;
    RealType v22 = v0*v21;
    RealType t1_v22 = t1_v0*v21 + v0*t1_v21;
    RealType v23 = der_x(5);
    RealType t1_v23 = der_t1_x(5, tangents);
    RealType v24 = v22-v23;
    RealType t1_v24 = t1_v22 - t1_v23;
    return t1_v24;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<6>, const Tangents& tangents) const {
    const RealType v0 = static_cast<RealType>(0.001);
    const RealType t1_v0 = static_cast<RealType>(0.);
    RealType v1 = p(3);
    RealType t1_v1 = t1_p(3, tangents);
    RealType v2 = v0*v1;
    RealType t1_v2 = t1_v0*v1 + v0*t1_v1;
    RealType v3 = y(0);
    RealType t1_v3 = t1_y(0, tangents);
    RealType v4 = v2*v3;
    RealType t1_v4 = t1_v2*v3 + v2*t1_v3;
    RealType v5 = der_x(7);
    RealType t1_v5 = der_t1_x(7, tangents);
    RealType v6 = v4-v5;
    RealType t1_v6 = t1_v4 - t1_v5;
    return t1_v6;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<7>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    const RealType v1 = static_cast<RealType>(5554.1);
    const RealType t1_v1 = static_cast<RealType>(0.);
    RealType v2 = -v1;
    RealType t1_v2 = -t1_v1;
    const RealType v3 = static_cast<RealType>(721170000.0);
    const RealType t1_v3 = static_cast<RealType>(0.);
    const RealType v4 = static_cast<RealType>(8333.3);
    const RealType t1_v4 = static_cast<RealType>(0.);
    const RealType v5 = static_cast<RealType>(273.15);
    const RealType t1_v5 = static_cast<RealType>(0.);
    RealType v6 = x(6);
    RealType t1_v6 = t1_x(6, tangents);
    RealType v7 = v5+v6;
    RealType t1_v7 = t1_v5+t1_v6;
    RealType v8 = v4/v7;
    RealType t1_v8 =  ( v7 * t1_v4 - v4 * t1_v7 ) / ( v7 * v7 );
    RealType v9 = -v8;
    RealType t1_v9 = -t1_v8;
    RealType v10 = exp(v9);
    RealType t1_v10 = t1_v9 * exp(v9);
    RealType v11 = v3*v10;
    RealType t1_v11 = t1_v3*v10 + v3*t1_v10;
    RealType v12 = x(1);
    RealType t1_v12 = t1_x(1, tangents);
    RealType v13 = v11*v12;
    RealType t1_v13 = t1_v11*v12 + v11*t1_v12;
    RealType v14 = x(2);
    RealType t1_v14 = t1_x(2, tangents);
    RealType v15 = v13*v14;
    RealType t1_v15 = t1_v13*v14 + v13*t1_v14;
    const RealType v16 = static_cast<RealType>(1337250000000.0);
    const RealType t1_v16 = static_cast<RealType>(0.);
    const RealType v17 = static_cast<RealType>(11111.0);
    const RealType t1_v17 = static_cast<RealType>(0.);
    const RealType v18 = static_cast<RealType>(273.15);
    const RealType t1_v18 = static_cast<RealType>(0.);
    RealType v19 = v18+v6;
    RealType t1_v19 = t1_v18+t1_v6;
    RealType v20 = v17/v19;
    RealType t1_v20 =  ( v19 * t1_v17 - v17 * t1_v19 ) / ( v19 * v19 );
    RealType v21 = -v20;
    RealType t1_v21 = -t1_v20;
    RealType v22 = exp(v21);
    RealType t1_v22 = t1_v21 * exp(v21);
    RealType v23 = v16*v22;
    RealType t1_v23 = t1_v16*v22 + v16*t1_v22;
    RealType v24 = v23*v14;
    RealType t1_v24 = t1_v23*v14 + v23*t1_v14;
    RealType v25 = x(3);
    RealType t1_v25 = t1_x(3, tangents);
    RealType v26 = v24*v25;
    RealType t1_v26 = t1_v24*v25 + v24*t1_v25;
    RealType v27 = v15-v26;
    RealType t1_v27 = t1_v15-t1_v26;
    RealType v28 = v2*v27;
    RealType t1_v28 = t1_v2*v27 + v2*t1_v27;
    RealType v29 = x(7);
    RealType t1_v29 = t1_x(7, tangents);
    RealType v30 = v28*v29;
    RealType t1_v30 = t1_v28*v29 + v28*t1_v29;
    const RealType v31 = static_cast<RealType>(181605029400.0);
    const RealType t1_v31 = static_cast<RealType>(0.);
    const RealType v32 = static_cast<RealType>(8333.3);
    const RealType t1_v32 = static_cast<RealType>(0.);
    const RealType v33 = static_cast<RealType>(273.15);
    const RealType t1_v33 = static_cast<RealType>(0.);
    RealType v34 = v33+v6;
    RealType t1_v34 = t1_v33+t1_v6;
    RealType v35 = v32/v34;
    RealType t1_v35 =  ( v34 * t1_v32 - v32 * t1_v34 ) / ( v34 * v34 );
    RealType v36 = -v35;
    RealType t1_v36 = -t1_v35;
    RealType v37 = exp(v36);
    RealType t1_v37 = t1_v36 * exp(v36);
    RealType v38 = v31*v37;
    RealType t1_v38 = t1_v31*v37 + v31*t1_v37;
    RealType v39 = v38*v12;
    RealType t1_v39 = t1_v38*v12 + v38*t1_v12;
    RealType v40 = v39*v14;
    RealType t1_v40 = t1_v39*v14 + v39*t1_v14;
    RealType v41 = v40*v29;
    RealType t1_v41 = t1_v40*v29 + v40*t1_v29;
    RealType v42 = v30-v41;
    RealType t1_v42 = t1_v30-t1_v41;
    RealType v43 = v0*v42;
    RealType t1_v43 = t1_v0*v42 + v0*t1_v42;
    RealType v44 = der_x(8);
    RealType t1_v44 = der_t1_x(8, tangents);
    RealType v45 = v43-v44;
    RealType t1_v45 = t1_v43 - t1_v44;
    return t1_v45;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<8>, const Tangents& tangents) const {
    RealType v0 = y(2);
    RealType t1_v0 = t1_y(2, tangents);
    RealType v1 = der_x(6);
    RealType t1_v1 = der_t1_x(6, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    return t1_v2;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<9>, const Tangents& tangents) const {
    using std::exp;
    RealType v0 = p(3);
    RealType t1_v0 = t1_p(3, tangents);
    RealType v1 = y(0);
    RealType t1_v1 = t1_y(0, tangents);
    RealType v2 = c(1);
    RealType t1_v2 = static_cast<RealType>(0.);
    RealType v3 = v1*v2;
    RealType t1_v3 = t1_v1*v2 + v1*t1_v2;
    const RealType v4 = static_cast<RealType>(1000);
    const RealType t1_v4 = static_cast<RealType>(0.);
    RealType v5 = x(7);
    RealType t1_v5 = t1_x(7, tangents);
    RealType v6 = v4*v5;
    RealType t1_v6 = t1_v4*v5 + v4*t1_v5;
    RealType v7 = v3/v6;
    RealType t1_v7 =  ( v6 * t1_v3 - v3 * t1_v6 ) / ( v6 * v6 );
    const RealType v8 = static_cast<RealType>(0.000239005736137667);
    const RealType t1_v8 = static_cast<RealType>(0.);
    const RealType v9 = static_cast<RealType>(437881620000.0);
    const RealType t1_v9 = static_cast<RealType>(0.);
    const RealType v10 = static_cast<RealType>(1000.0);
    const RealType t1_v10 = static_cast<RealType>(0.);
    RealType v11 = c(2);
    RealType t1_v11 = static_cast<RealType>(0.);
    RealType v12 = v10*v11;
    RealType t1_v12 = t1_v10*v11 + v10*t1_v11;
    const RealType v13 = static_cast<RealType>(273.15);
    const RealType t1_v13 = static_cast<RealType>(0.);
    RealType v14 = x(6);
    RealType t1_v14 = t1_x(6, tangents);
    RealType v15 = v13+v14;
    RealType t1_v15 = t1_v13+t1_v14;
    RealType v16 = v12/v15;
    RealType t1_v16 =  ( v15 * t1_v12 - v12 * t1_v15 ) / ( v15 * v15 );
    RealType v17 = -v16;
    RealType t1_v17 = -t1_v16;
    RealType v18 = exp(v17);
    RealType t1_v18 = t1_v17 * exp(v17);
    RealType v19 = v9*v18;
    RealType t1_v19 = t1_v9*v18 + v9*t1_v18;
    RealType v20 = x(0);
    RealType t1_v20 = t1_x(0, tangents);
    RealType v21 = v19*v20;
    RealType t1_v21 = t1_v19*v20 + v19*t1_v20;
    RealType v22 = x(1);
    RealType t1_v22 = t1_x(1, tangents);
    RealType v23 = v21*v22;
    RealType t1_v23 = t1_v21*v22 + v21*t1_v22;
    const RealType v24 = static_cast<RealType>(114161211000000.0);
    const RealType t1_v24 = static_cast<RealType>(0.);
    const RealType v25 = static_cast<RealType>(8333.3);
    const RealType t1_v25 = static_cast<RealType>(0.);
    const RealType v26 = static_cast<RealType>(273.15);
    const RealType t1_v26 = static_cast<RealType>(0.);
    RealType v27 = v26+v14;
    RealType t1_v27 = t1_v26+t1_v14;
    RealType v28 = v25/v27;
    RealType t1_v28 =  ( v27 * t1_v25 - v25 * t1_v27 ) / ( v27 * v27 );
    RealType v29 = -v28;
    RealType t1_v29 = -t1_v28;
    RealType v30 = exp(v29);
    RealType t1_v30 = t1_v29 * exp(v29);
    RealType v31 = v24*v30;
    RealType t1_v31 = t1_v24*v30 + v24*t1_v30;
    RealType v32 = v31*v22;
    RealType t1_v32 = t1_v31*v22 + v31*t1_v22;
    RealType v33 = x(2);
    RealType t1_v33 = t1_x(2, tangents);
    RealType v34 = v32*v33;
    RealType t1_v34 = t1_v32*v33 + v32*t1_v33;
    RealType v35 = v23+v34;
    RealType t1_v35 = t1_v23+t1_v34;
    const RealType v36 = static_cast<RealType>(6.0523935E+017);
    const RealType t1_v36 = static_cast<RealType>(0.);
    const RealType v37 = static_cast<RealType>(11111.0);
    const RealType t1_v37 = static_cast<RealType>(0.);
    const RealType v38 = static_cast<RealType>(273.15);
    const RealType t1_v38 = static_cast<RealType>(0.);
    RealType v39 = v38+v14;
    RealType t1_v39 = t1_v38+t1_v14;
    RealType v40 = v37/v39;
    RealType t1_v40 =  ( v39 * t1_v37 - v37 * t1_v39 ) / ( v39 * v39 );
    RealType v41 = -v40;
    RealType t1_v41 = -t1_v40;
    RealType v42 = exp(v41);
    RealType t1_v42 = t1_v41 * exp(v41);
    RealType v43 = v36*v42;
    RealType t1_v43 = t1_v36*v42 + v36*t1_v42;
    RealType v44 = v43*v33;
    RealType t1_v44 = t1_v43*v33 + v43*t1_v33;
    RealType v45 = x(3);
    RealType t1_v45 = t1_x(3, tangents);
    RealType v46 = v44*v45;
    RealType t1_v46 = t1_v44*v45 + v44*t1_v45;
    RealType v47 = v35+v46;
    RealType t1_v47 = t1_v35+t1_v46;
    RealType v48 = v8*v47;
    RealType t1_v48 = t1_v8*v47 + v8*t1_v47;
    RealType v49 = v7+v48;
    RealType t1_v49 = t1_v7+t1_v48;
    const RealType v50 = static_cast<RealType>(0.000243454685774894);
    const RealType t1_v50 = static_cast<RealType>(0.);
    const RealType v51 = static_cast<RealType>(1000.0);
    const RealType t1_v51 = static_cast<RealType>(0.);
    RealType v52 = y(1);
    RealType t1_v52 = t1_y(1, tangents);
    RealType v53 = v51*v52;
    RealType t1_v53 = t1_v51*v52 + v51*t1_v52;
    RealType v54 = v14-v53;
    RealType t1_v54 = t1_v14-t1_v53;
    RealType v55 = v50*v54;
    RealType t1_v55 = t1_v50*v54 + v50*t1_v54;
    RealType v56 = v49-v55;
    RealType t1_v56 = t1_v49-t1_v55;
    RealType v57 = v1*v14;
    RealType t1_v57 = t1_v1*v14 + v1*t1_v14;
    const RealType v58 = static_cast<RealType>(1000);
    const RealType t1_v58 = static_cast<RealType>(0.);
    RealType v59 = v58*v5;
    RealType t1_v59 = t1_v58*v5 + v58*t1_v5;
    RealType v60 = v57/v59;
    RealType t1_v60 =  ( v59 * t1_v57 - v57 * t1_v59 ) / ( v59 * v59 );
    RealType v61 = v56-v60;
    RealType t1_v61 = t1_v56-t1_v60;
    RealType v62 = v0*v61;
    RealType t1_v62 = t1_v0*v61 + v0*t1_v61;
    RealType v63 = y(2);
    RealType t1_v63 = t1_y(2, tangents);
    RealType v64 = v62-v63;
    RealType t1_v64 = t1_v62 - t1_v63;
    return t1_v64;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<10>, const Tangents& tangents) const {
    RealType v0 = p(1);
    RealType t1_v0 = t1_p(1, tangents);
    RealType v1 = y(0);
    RealType t1_v1 = t1_y(0, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    return t1_v2;
}

template<typename RealType>
template<class Tangents>
RealType wobatch2Model<RealType>::evalTangent(AllEqns,
  EqIndex<11>, const Tangents& tangents) const {
    RealType v0 = p(0);
    RealType t1_v0 = t1_p(0, tangents);
    RealType v1 = y(1);
    RealType t1_v1 = t1_y(1, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    return t1_v2;
}




#endif // wobatch2_MODEL_T1_EVAL_HPP
