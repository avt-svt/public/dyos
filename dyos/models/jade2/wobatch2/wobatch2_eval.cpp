#include "wobatch2_eval.hpp"

template double wobatch2Model<double>::eval(AllEqns, EqIndex<0>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<1>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<2>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<3>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<4>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<5>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<6>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<7>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<8>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<9>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<10>) const;
template double wobatch2Model<double>::eval(AllEqns, EqIndex<11>) const;

