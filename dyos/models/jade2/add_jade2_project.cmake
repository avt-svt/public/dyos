function(add_jade2_project projectName)

find_package(Eso REQUIRED)
set(JADE2_EXE $<TARGET_FILE:ade>)

set(SRC_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}_eval.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}_eval.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}_t1_eval.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}_t1_eval.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}_t2a1_eval.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}_t2a1_eval.cpp
)

#add_custom_command(OUTPUT ${SRC_FILES}
#                   COMMAND ${JADE2_EXE} ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}.mof  ${projectName}
#                   DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${projectName}.mof ade
#                   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

add_library(${projectName} SHARED ${SRC_FILES})

target_link_libraries(${projectName} Eso::Eso)
set_target_properties(${projectName} PROPERTIES CXX_VISIBILITY_PRESET hidden)


if(MSVC)
  set_target_properties(${projectName} PROPERTIES FOLDER Models)
endif()

endfunction()
