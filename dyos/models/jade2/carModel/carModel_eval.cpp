#include "carModel_eval.hpp"

template double carModel<double>::eval(AllEqns, EqIndex<0>) const;
template double carModel<double>::eval(AllEqns, EqIndex<1>) const;
template double carModel<double>::eval(AllEqns, EqIndex<2>) const;
template double carModel<double>::eval(AllEqns, EqIndex<3>) const;
template double carModel<double>::eval(AllEqns, EqIndex<4>) const;

