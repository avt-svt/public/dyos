#ifndef carModel_MODEL_EVAL_HPP
#define carModel_MODEL_EVAL_HPP

#include "carModel.hpp"

template<typename RealType> RealType
carModel<RealType>::eval(AllEqns, EqIndex<0>) const {
    RealType v0 = x(2);
    RealType v1 = der_x(0);
    RealType v2 = v0-v1;
    return v2;
}
template<typename RealType> RealType
carModel<RealType>::eval(AllEqns, EqIndex<1>) const {
    RealType v0 = p(0);
    RealType v1 = c(0);
    RealType v2 = x(2);
    RealType v3 = v1*v2;
    RealType v4 = v3*v2;
    RealType v5 = c(3);
    RealType v6 = v4/v5;
    RealType v7 = v0-v6;
    RealType v8 = der_x(2);
    RealType v9 = v7-v8;
    return v9;
}
template<typename RealType> RealType
carModel<RealType>::eval(AllEqns, EqIndex<2>) const {
    const RealType v0 = static_cast<RealType>(1);
    RealType v1 = der_x(1);
    RealType v2 = v0-v1;
    return v2;
}
template<typename RealType> RealType
carModel<RealType>::eval(AllEqns, EqIndex<3>) const {
    RealType v0 = y(0);
    RealType v1 = der_x(3);
    RealType v2 = v0-v1;
    return v2;
}
template<typename RealType> RealType
carModel<RealType>::eval(AllEqns, EqIndex<4>) const {
    using std::tanh;
    RealType v0 = c(2);
    const RealType v1 = static_cast<RealType>(0.5);
    RealType v2 = c(1);
    RealType v3 = v1*v2;
    RealType v4 = p(0);
    RealType v5 = v3*v4;
    const RealType v6 = static_cast<RealType>(1);
    RealType v7 = c(3);
    RealType v8 = v7*v4;
    RealType v9 = tanh(v8);
    RealType v10 = v6+v9;
    RealType v11 = v5*v10;
    RealType v12 = v0+v11;
    RealType v13 = y(0);
    RealType v14 = v12-v13;
    return v14;
}


#endif // carModel_MODEL_EVAL_HPP
