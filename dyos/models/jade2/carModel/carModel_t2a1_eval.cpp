#include "carModel_t2a1_eval.hpp"

template void carModel<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<0>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void carModel<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<1>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void carModel<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<2>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void carModel<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<3>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;
template void carModel<double>::evalSecondOrderAdjoints(AllEqns, EqIndex<4>,
    AllVars, double* const &tangents,
    double* const & tangentAdjoints, double *secOrdAdjoints) const;

