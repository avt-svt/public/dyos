#include "carModel_t1_eval.hpp"

template double carModel<double>::evalTangent<double const*>(AllEqns,
    EqIndex<0>, double const * const &tangents) const;
template double carModel<double>::evalTangent<double const*>(AllEqns,
    EqIndex<1>, double const * const &tangents) const;
template double carModel<double>::evalTangent<double const*>(AllEqns,
    EqIndex<2>, double const * const &tangents) const;
template double carModel<double>::evalTangent<double const*>(AllEqns,
    EqIndex<3>, double const * const &tangents) const;
template double carModel<double>::evalTangent<double const*>(AllEqns,
    EqIndex<4>, double const * const &tangents) const;

