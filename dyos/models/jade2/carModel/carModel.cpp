#include "carModel.hpp"
#include <iostream>

#if defined _WIN32 || defined WIN32
#define DECLSPEC __declspec(dllexport)
#else
#if defined __GNUC__ || defined __clang__
#define DECLSPEC __attribute__ ((visibility ("default")))
#else
#define DECLSPEC
#endif
#endif

extern "C" DECLSPEC void* allocModel() {
    void *modelPointer = new carModel<double>;
    return modelPointer;
}

extern "C" DECLSPEC void freeModel(void* modelPointer) {
    delete static_cast<carModel<double>*>(modelPointer);
}

extern "C" DECLSPEC void getNumVariables(int varGroup, int& numVariables)
{
  switch (varGroup) {
    case AllVars::value:
      numVariables = carModel<double>::numVariables(AllVars{});
      break;
    case StateVars::value:
      numVariables = carModel<double>::numVariables(StateVars{});
      break;
    case DiffVars::value:
      numVariables = carModel<double>::numVariables(DiffVars{});
      break;
    case AlgVars::value:
      numVariables = carModel<double>::numVariables(AlgVars{});
      break;
    case ParamVars::value:
      numVariables = carModel<double>::numVariables(ParamVars{});
      break;
    case StateTimeParamVars::value:
      numVariables = carModel<double>::numVariables(StateTimeParamVars{});
      break;
    case StateTimeVars::value:
      numVariables = carModel<double>::numVariables(StateTimeParamVars{});
      break;
    case TimeVar::value:
      numVariables = carModel<double>::numVariables(TimeVar{});
      break;
    case DerDiffVars::value:
      numVariables = carModel<double>::numVariables(DerDiffVars{});
      break;
        default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::getNumVariables:\n the variableGroup="
                         << varGroup << " has to be between 0 and 8.";
            std::cerr << errorMessage.str() << std::endl;
            std::exit(1);
  }

}

extern "C" DECLSPEC void getNumEquations(int eqGroup, int& numEquations)
{
  switch (eqGroup) {
    case AllEqns::value:
      numEquations = carModel<double>::numEquations(AllEqns{});
      break;
    case DiffEqns::value:
      numEquations = carModel<double>::numEquations(DiffEqns{});
      break;
    case AlgEqns::value:
            numEquations = carModel<double>::numEquations(AlgEqns{});
            break;
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::getNumEquations:\n the equationGroup="
                         << eqGroup << " has to be between 0 and 2.";
            std::cerr << errorMessage.str() << std::endl;
            std::exit(1);
  }

}

extern "C" DECLSPEC void evalJacobianPattern(void* modelPointer, int eqGroup, int varGroup, Eigen::SparseMatrix<double> &jac) {

    if (eqGroup != AllEqns::value){
        std::ostringstream errorMessage;
        errorMessage << "carModel::evalJacobianPattern:\n eqGroup has to be equal "
                     << AllEqns::value << ".";
        std::cerr << errorMessage.str() << std::endl;
        std::exit(1);
    }

  carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

  switch (varGroup) {
  case StateTimeParamVars::value:
    return model->evalJacobianPattern(AllEqns{}, StateTimeParamVars{}, jac);
  case StateVars::value:
    return model->evalJacobianPattern(AllEqns{}, StateVars{}, jac);
  case ParamVars::value:
    return model->evalJacobianPattern(AllEqns{}, ParamVars{}, jac);
  case DerDiffVars::value:
    return model->evalJacobianPattern(AllEqns{}, DerDiffVars{}, jac);
  case AllVars::value:
    return model->evalJacobianPattern(AllEqns{}, AllVars{}, jac);
  case StateTimeVars::value:
    return model->evalJacobianPattern(AllEqns{}, StateTimeVars{}, jac);
  case TimeVar::value:
    return model->evalJacobianPattern(AllEqns{}, TimeVar{}, jac);
  case DiffVars::value:
    return model->evalJacobianPattern(AllEqns{}, DiffVars{}, jac);
  case AlgVars::value:
    return model->evalJacobianPattern(AllEqns{}, AlgVars{}, jac);
  default:
        std::ostringstream errorMessage;
        errorMessage << "carModel::evalJacobianPattern:\n the variableGroup="
                     << varGroup << " has to be between 0 and 8.";
        std::cerr << errorMessage.str() << std::endl;
        std::exit(1);
  }
}

extern "C" DECLSPEC void evalJacobianValues(void* modelPointer, int eqGroup, int varGroup, Eigen::SparseMatrix<double> &jac) {

    if (eqGroup != AllEqns::value){
        std::ostringstream errorMessage;
        errorMessage << "carModel::evalJacobianValues:\n eqGroup has to be equal "
                     << AllEqns::value << ".";
        std::cerr << errorMessage.str() << std::endl;
        std::exit(1);
    }

  carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

  switch (varGroup) {
  case StateTimeParamVars::value:
    return model->evalJacobianValues(AllEqns{}, StateTimeParamVars{}, jac);
  case StateVars::value:
    return model->evalJacobianValues(AllEqns{}, StateVars{}, jac);
  case ParamVars::value:
    return model->evalJacobianValues(AllEqns{}, ParamVars{}, jac);
  case DerDiffVars::value:
    return model->evalJacobianValues(AllEqns{}, DerDiffVars{}, jac);
  case AllVars::value:
    return model->evalJacobianValues(AllEqns{}, AllVars{}, jac);
  case StateTimeVars::value:
    return model->evalJacobianValues(AllEqns{}, StateTimeVars{}, jac);
  case TimeVar::value:
    return model->evalJacobianValues(AllEqns{}, TimeVar{}, jac);
  case DiffVars::value:
    return model->evalJacobianValues(AllEqns{}, DiffVars{}, jac);
  case AlgVars::value:
    return model->evalJacobianValues(AllEqns{}, AlgVars{}, jac);
  default:
        std::ostringstream errorMessage;
        errorMessage << "carModel::evalJacobianValues:\n the variableGroup="
                     << varGroup << " has to be between 0 and 8.";
        std::cerr << errorMessage.str() << std::endl;
        std::exit(1);
  }
}

extern "C" DECLSPEC void getInitialValues(void* modelPointer, int varGroup, Eigen::Matrix<double, Eigen::Dynamic, 1> &initial_values) {

  carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

  switch (varGroup) {
  case StateTimeParamVars::value:
    return model->getInitialValues(StateTimeParamVars{}, initial_values);
  case StateVars::value:
    return model->getInitialValues(StateVars{}, initial_values);
  case ParamVars::value:
    return model->getInitialValues(ParamVars{}, initial_values);
  case DerDiffVars::value:
    return model->getInitialValues(DerDiffVars{}, initial_values);
  case AllVars::value:
    return model->getInitialValues(AllVars{}, initial_values);
  case StateTimeVars::value:
    return model->getInitialValues(StateTimeVars{}, initial_values);
  case TimeVar::value:
    return model->getInitialValues(TimeVar{}, initial_values);
  case DiffVars::value:
    return model->getInitialValues(DiffVars{}, initial_values);
  case AlgVars::value:
    return model->getInitialValues(AlgVars{}, initial_values);
  default:
        std::ostringstream errorMessage;
        errorMessage << "carModel::getInitialValues:\n the variableGroup="
                     << varGroup << " has to be between 0 and 8.";
        std::cerr << errorMessage.str() << std::endl;
        std::exit(1);
  }
}

  extern "C" DECLSPEC void getInitialValue(void* modelPointer, int varGroup, int i, double &val) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (varGroup) {
    case StateTimeParamVars::value:
      val = model->getInitialValue(StateTimeParamVars{}, i);
      break;
    case StateVars::value:
      val = model->getInitialValue(StateVars{}, i);
      break;
    case ParamVars::value:
      val = model->getInitialValue(ParamVars{}, i);
      break;
    case DerDiffVars::value:
      val = model->getInitialValue(DerDiffVars{}, i);
      break;
    case AllVars::value:
      val = model->getInitialValue(AllVars{}, i);
      break;
    case StateTimeVars::value:
      val = model->getInitialValue(StateTimeVars{}, i);
      break;
    case TimeVar::value:
      val = model->getInitialValue(TimeVar{}, i);
      break;
    case DiffVars::value:
      val = model->getInitialValue(DiffVars{}, i);
      break;
    case AlgVars::value:
      val = model->getInitialValue(AlgVars{}, i);
      break;
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::getInitialValue:\n the variableGroup="
                         << varGroup << " has to be between 0 and 8.";
            std::cerr << errorMessage.str() << std::endl;
            std::exit(1);
    }
  }

  extern "C" DECLSPEC void getVariable(void *modelPointer, int varGroup, int i, double &val) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (varGroup) {
    case StateTimeParamVars::value:
      val = model->getVariable(StateTimeParamVars{}, i);
      break;
    case StateVars::value:
      val = model->getVariable(StateVars{}, i);
      break;
    case ParamVars::value:
      val = model->getVariable(ParamVars{}, i);
      break;
    case DerDiffVars::value:
      val = model->getVariable(DerDiffVars{}, i);
      break;
    case AllVars::value:
      val = model->getVariable(AllVars{}, i);
      break;
    case StateTimeVars::value:
      val = model->getVariable(StateTimeVars{}, i);
      break;
    case TimeVar::value:
      val = model->getVariable(TimeVar{}, i);
      break;
    case DiffVars::value:
      val = model->getVariable(DiffVars{}, i);
      break;
    case AlgVars::value:
      val = model->getVariable(AlgVars{}, i);
      break;
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::getVariable:\n the variableGroup="
                         << varGroup << " has to be between 0 and 8.";
            std::cerr << errorMessage.str() << std::endl;
            std::exit(1);
    }
  }

  extern "C" DECLSPEC void setVariable(void *modelPointer, int varGroup, int i, const double val) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (varGroup) {
    case StateTimeParamVars::value:
      return model->setVariable(StateTimeParamVars{}, i, val);
    case StateVars::value:
      return model->setVariable(StateVars{}, i, val);
    case ParamVars::value:
      return model->setVariable(ParamVars{}, i, val);
    case DerDiffVars::value:
      return model->setVariable(DerDiffVars{}, i, val);
    case AllVars::value:
      return model->setVariable(AllVars{}, i, val);
    case StateTimeVars::value:
      return model->setVariable(StateTimeVars{}, i, val);
    case TimeVar::value:
      if(carModel<double>::numVariables(TimeVar{}) == 1)
         return model->setVariable(TimeVar{}, i, val);
      else
         return;
    case DiffVars::value:
      return model->setVariable(DiffVars{}, i, val);
    case AlgVars::value:
      return model->setVariable(AlgVars{}, i, val);
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::setVariable:\n the variableGroup="
                         << varGroup << " has to be between 0 and 8.";
            std::cerr << errorMessage.str() << std::endl;
            std::exit(1);
    }
  }

  extern "C" DECLSPEC void setVariables(void *modelPointer, int varGroup, const double* variables) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (varGroup) {
    case StateTimeParamVars::value:
      return model->setVariables(StateTimeParamVars{}, variables);
    case StateVars::value:
      return model->setVariables(StateVars{}, variables);
    case ParamVars::value:
      return model->setVariables(ParamVars{}, variables);
    case DerDiffVars::value:
      return model->setVariables(DerDiffVars{}, variables);
    case AllVars::value:
      return model->setVariables(AllVars{}, variables);
    case StateTimeVars::value:
      return model->setVariables(StateTimeVars{}, variables);
    case TimeVar::value:
    if(carModel<double>::numVariables(TimeVar{}) == 1)
       return model->setVariables(TimeVar{}, variables);
    else
       return;
    case DiffVars::value:
      return model->setVariables(DiffVars{}, variables);
    case AlgVars::value:
      return model->setVariables(AlgVars{}, variables);
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::setVariables:\n the variableGroup="
                         << varGroup << " has to be between 0 and 8.";
                         std::cerr << errorMessage.str() << std::endl;
                         std::exit(1);
    }
  }

  extern "C" DECLSPEC void getVariables(void *modelPointer, int varGroup, double* variables) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (varGroup) {
    case StateTimeParamVars::value:
      return model->getVariables(StateTimeParamVars{}, variables);
    case StateVars::value:
      return model->getVariables(StateVars{}, variables);
    case ParamVars::value:
      return model->getVariables(ParamVars{}, variables);
    case DerDiffVars::value:
      return model->getVariables(DerDiffVars{}, variables);
    case AllVars::value:
      return model->getVariables(AllVars{}, variables);
    case StateTimeVars::value:
      return model->getVariables(StateTimeVars{}, variables);
    case TimeVar::value:
      return model->getVariables(TimeVar{}, variables);
    case DiffVars::value:
      return model->getVariables(DiffVars{}, variables);
    case AlgVars::value:
      return model->getVariables(AlgVars{}, variables);
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::getVariables:\n the variableGroup="
                         << varGroup << " has to be between 0 and 8.";
                         std::cerr << errorMessage.str() << std::endl;
                         std::exit(1);
    }
  }

  extern "C" DECLSPEC void evalAll(void *modelPointer, int eqGroup, double *residuals) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (eqGroup) {
      case AllEqns::value:
        return model->evalAll(AllEqns{}, residuals);
      case DiffEqns::value:
        return model->evalAll(DiffEqns{}, residuals);
      case AlgEqns::value:
        return model->evalAll(AlgEqns{}, residuals);
      default:
                std::ostringstream errorMessage;
                errorMessage << "carModel::evalAll:\n the equationGroup="
                             << eqGroup << " has to be between 0 and 2.";
                             std::cerr << errorMessage.str() << std::endl;
                             std::exit(1);
    }
  }

  extern "C" DECLSPEC void eval(void *modelPointer, int eqGroup, int i, double &residual) {

    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (eqGroup) {
    case AllEqns::value:
      residual = model->eval(AllEqns{}, i);
      break;
    case DiffEqns::value:
      residual = model->eval(DiffEqns{}, i);
      break;
    case AlgEqns::value:
      residual = model->eval(AlgEqns{}, i);
      break;
    default:
            std::ostringstream errorMessage;
            errorMessage << "carModel::eval:\n the equationGroup="
                         << eqGroup << " has to be between 0 and 2.";
                         std::cerr << errorMessage.str() << std::endl;
                         std::exit(1);
    }
  }

    extern "C" DECLSPEC void getVariableNames(void *modelPointer, std::vector<std::string> &names){
        carModel<double>* model = static_cast<carModel<double>*>(modelPointer);
        names = model->getVariableNames();
    }

    extern "C" DECLSPEC void variableIndexOffset(void* modelPointer, int varGroup, int &offset){
        carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

        switch (varGroup) {
            case AllVars::value:
                offset = model->variableIndexOffset(AllVars{});
                break;
            case StateVars::value:
                offset = model->variableIndexOffset(StateVars{});
                break;
            case DiffVars::value:
                offset = model->variableIndexOffset(DiffVars{});
                break;
            case AlgVars::value:
                offset = model->variableIndexOffset(AlgVars{});
                break;
            case ParamVars::value:
                offset = model->variableIndexOffset(ParamVars{});
                break;
            case StateTimeParamVars::value:
                offset = model->variableIndexOffset(StateTimeParamVars{});
                break;
            case StateTimeVars::value:
                offset = model->variableIndexOffset(StateTimeParamVars{});
                break;
            case TimeVar::value:
                offset = model->variableIndexOffset(TimeVar{});
                break;
            case DerDiffVars::value:
                offset = model->variableIndexOffset(DerDiffVars{});
                break;
            default:
                std::ostringstream errorMessage;
                errorMessage << "carModel::variableIndexOffset:\n the variableGroup="
                             << varGroup << " has to be between 0 and 8.";
                             std::cerr << errorMessage.str() << std::endl;
                             std::exit(1);
        }

        //offset = model->variableIndexOffset()
    }

extern "C" DECLSPEC void equationIndexOffset(void* modelPointer, int eqGroup, int &offset) {
    carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

    switch (eqGroup) {
        case AllEqns::value:
            offset = model->equationIndexOffset(AllEqns{});
            break;
        case DiffEqns::value:
            offset = model->equationIndexOffset(DiffEqns{});
            break;
        case AlgEqns::value:
            offset = model->equationIndexOffset(AlgEqns{});
            break;
    }
}

extern "C" DECLSPEC void setAdjoints(void* modelPointer, const int eqGroup, Eigen::VectorXd &a_yy) {
  carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

  return model->setAdjoints(AllEqns{}, a_yy);
}

extern "C" DECLSPEC void evalAllSecondOrderAdjoints
(void* modelPointer, const int varGroup, double *tangents, double *tangentAdjoints, Eigen::VectorXd &rhs) {
  carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

  return model->evalAllSecondOrderAdjoints(AllEqns{}, AllVars{}, tangents, tangentAdjoints, rhs);
}

extern "C" DECLSPEC void evalAllTangents
(void* modelPointer, const double* tangents, double* tangentResiduals)
{
  carModel<double>* model = static_cast<carModel<double>*>(modelPointer);

  return model->evalAllTangents( tangents, tangentResiduals);

}
