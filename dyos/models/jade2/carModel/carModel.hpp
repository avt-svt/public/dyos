#ifndef carModel_MODEL_HPP
#define carModel_MODEL_HPP
#include "Eso/StandardGroups.hpp"
#include "Eigen/SparseCore"
#include <vector>
#include <string>
#include <cmath>
#include <memory>

#ifdef _MSC_BUILD
#pragma warning(push) 
#pragma warning(disable : 4060)
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


template<typename RealType>
class carModel {
public:
  template<int Exponent>
  static RealType integerPower(const RealType& x){
    RealType result = static_cast<RealType>(1.);
    for(int i=0; i< std::abs(Exponent);++i) {
      result *= x;
    }
    if(Exponent >= 0)
      return result;
    else {
      return static_cast<RealType>(1.0)/result;
    }
  }


  template<int Exponent>
  static RealType integerPowerDerivative(const RealType& x){
    if(Exponent == 0) return 0;
    RealType result = static_cast<RealType>(1.);
    for(int i=0; i< std::abs(Exponent-1);++i) {
      result *= x;
    }
    if(Exponent >= 0)
      return static_cast<RealType>(Exponent)*result;
    else {
      return static_cast<RealType>(Exponent)/result;
    }
  }


  template<int Exponent>
  static RealType integerPowerHessian(const RealType& x){
    if(Exponent == 0) return 0;
    if(Exponent == 1) return 0;
    RealType result = static_cast<RealType>(1.);
    for(int i=0; i< std::abs(Exponent-2);++i) {
      result *= x;
    }
    if(Exponent >= 0)
      return static_cast<RealType>(Exponent * (Exponent-1))*result;
    else {
      return static_cast<RealType>(Exponent * (Exponent-1))/result;
    }
}

  static constexpr int numEquations(const DiffEqns) { return 4;}
  static constexpr int numEquations(const AlgEqns) { return 1;}
  static constexpr int numEquations(const AllEqns) { return 5;}
  static constexpr int numVariables(DiffVars) { return 4;}
  static constexpr int numVariables(AlgVars) { return 1;}
  static constexpr int numVariables(TimeVar) { return 0;}
  static constexpr int numVariables(ParamVars) { return 1;}
  static constexpr int numVariables(AllVars) {return 10;}
  static constexpr int numVariables(StateVars) { return 5; }
  static constexpr int numVariables(DerDiffVars) {return 4; }
  static constexpr int numVariables(StateTimeVars) { return numVariables(StateVars{}) +  numVariables(TimeVar{});}
  static constexpr int numVariables(StateTimeParamVars) { return numVariables(StateTimeVars{}) +  numVariables(ParamVars{});}

  RealType eval(AllEqns, EqIndex<0>) const;
  RealType eval(AllEqns, EqIndex<1>) const;
  RealType eval(AllEqns, EqIndex<2>) const;
  RealType eval(AllEqns, EqIndex<3>) const;
  RealType eval(AllEqns, EqIndex<4>) const;
  

  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<0>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<1>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<2>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<3>, const Tangents& tangents) const;
  template<class Tangents>
  RealType evalTangent(AllEqns, EqIndex<4>, const Tangents& tangents) const;
  

  template<class Tangents, class TangentResiduals>
  void evalAllTangents(const Tangents& tangents, TangentResiduals& tangentResiduals)
  {
    int i = 0;
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<0>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<1>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<2>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<3>{}, tangents);
    tangentResiduals[i++] = evalTangent(AllEqns{}, EqIndex<4>{}, tangents);

  }

  
  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<0>, VarGroup<K>, RealType* gradient) {
    RealType v0 = x(2);
    RealType v1 = der_x(0);
    RealType v2 = v0-v1;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},0);
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v1;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[2-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[6-offs] += a1_v1;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<1>, VarGroup<K>, RealType* gradient) {
    RealType v0 = p(0);
    RealType v1 = c(0);
    RealType v2 = x(2);
    RealType v3 = v1*v2;
    RealType v4 = v3*v2;
    RealType v5 = c(3);
    RealType v6 = v4/v5;
    RealType v7 = v0-v6;
    RealType v8 = der_x(2);
    RealType v9 = v7-v8;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},1);
    a1_v8 -= a1_v9;
    a1_v7 += a1_v9;
    a1_v0 += a1_v7;
    a1_v6 -= a1_v7;
    RealType tmp_a1_v6 = a1_v6 / v5;
    a1_v4 += tmp_a1_v6;
    a1_v5 -= v4 * tmp_a1_v6 / v5;
    a1_v3 += a1_v4 * v2;
    a1_v2 += a1_v4 * v3;
    a1_v1 += a1_v3 * v2;
    a1_v2 += a1_v3 * v1;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 4){
        gradient[startIndex++] = a1_v2;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v8;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 4){
        gradient[2-offs] += a1_v2;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[5-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[8-offs] += a1_v8;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<2>, VarGroup<K>, RealType* gradient) {
    const RealType v0 = static_cast<RealType>(1);
    RealType v1 = der_x(1);
    RealType v2 = v0-v1;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},2);
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    a1_v0 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v1;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[7-offs] += a1_v1;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<3>, VarGroup<K>, RealType* gradient) {
    RealType v0 = y(0);
    RealType v1 = der_x(3);
    RealType v2 = v0-v1;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},3);
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[startIndex++] = a1_v1;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[4-offs] += a1_v0;
      }
      if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
        gradient[9-offs] += a1_v1;
      }
    }
}


  template<int K, bool doEvalGradient = true>
  void evalGradient(AllEqns, EqIndex<4>, VarGroup<K>, RealType* gradient) {
    using std::tanh;
    RealType v0 = c(2);
    const RealType v1 = static_cast<RealType>(0.5);
    RealType v2 = c(1);
    RealType v3 = v1*v2;
    RealType v4 = p(0);
    RealType v5 = v3*v4;
    const RealType v6 = static_cast<RealType>(1);
    RealType v7 = c(3);
    RealType v8 = v7*v4;
    RealType v9 = tanh(v8);
    RealType v10 = v6+v9;
    RealType v11 = v5*v10;
    RealType v12 = v0+v11;
    RealType v13 = y(0);
    RealType v14 = v12-v13;
    RealType a1_v0 = static_cast<RealType>(0.);
    RealType a1_v1 = static_cast<RealType>(0.);
    RealType a1_v2 = static_cast<RealType>(0.);
    RealType a1_v3 = static_cast<RealType>(0.);
    RealType a1_v4 = static_cast<RealType>(0.);
    RealType a1_v5 = static_cast<RealType>(0.);
    RealType a1_v6 = static_cast<RealType>(0.);
    RealType a1_v7 = static_cast<RealType>(0.);
    RealType a1_v8 = static_cast<RealType>(0.);
    RealType a1_v9 = static_cast<RealType>(0.);
    RealType a1_v10 = static_cast<RealType>(0.);
    RealType a1_v11 = static_cast<RealType>(0.);
    RealType a1_v12 = static_cast<RealType>(0.);
    RealType a1_v13 = static_cast<RealType>(0.);
    RealType a1_v14 = doEvalGradient?static_cast<RealType>(1.):getAdjoint(AllEqns{},4);
    a1_v13 -= a1_v14;
    a1_v12 += a1_v14;
    a1_v0 += a1_v12;
    a1_v11 += a1_v12;
    a1_v5 += a1_v11 * v10;
    a1_v10 += a1_v11 * v5;
    a1_v6 += a1_v10;
    a1_v9 += a1_v10;
    a1_v8 += a1_v9 * (-tanh(v8)*tanh(v8) + static_cast<RealType>(1.));
    a1_v7 += a1_v8 * v4;
    a1_v4 += a1_v8 * v7;
    a1_v6 = static_cast<RealType>(0.);
    a1_v3 += a1_v5 * v4;
    a1_v4 += a1_v5 * v3;
    a1_v1 += a1_v3 * v2;
    a1_v2 += a1_v3 * v1;
    a1_v1 = static_cast<RealType>(0.);
    if(doEvalGradient) {
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[startIndex++] = a1_v13;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[startIndex++] = a1_v4;
      }
    }
    else {
      constexpr int offs = variableIndexOffset(VarGroup<K>{});
      if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
        gradient[4-offs] += a1_v13;
      }
      if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
        gradient[5-offs] += a1_v4;
      }
    }
}



  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<0>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<1>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<2>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<3>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;

  template<int K, class Tangents, class TangentAdjoints>
  void evalSecondOrderAdjoints(AllEqns, EqIndex<4>, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const;




// Boiler plate code starts here
private:
  static constexpr int pseudoLastEquationIndex = numEquations(AllEqns{}); //hack to help Visual Studio
public:
  RealType eval(AllEqns, EqIndex<pseudoLastEquationIndex>) const {
    return static_cast<RealType>(0.);
  }
  template <int J>
  RealType eval(DiffEqns,EqIndex<J>) const {
    return eval(AllEqns{}, EqIndex<J>{});
  }

  template <int J>
  RealType eval(AlgEqns,EqIndex<J>) const {
    return eval(AllEqns{}, EqIndex<J+numEquations(DiffEqns{})>{});
  }

  template <int J, class Tangents>
  RealType evalTangent(DiffEqns,EqIndex<J>, const Tangents& tangents) const {
    return evalTangent(AllEqns{}, EqIndex<J>{}, tangents);
  }

  template <int J, class Tangents>
  RealType evalTangent(AlgEqns,EqIndex<J>, const Tangents& tangents) const {
    return evalTangent(AllEqns{}, EqIndex<J+numEquations(DiffEqns{})>{}, tangents);
  }

  template <int K, bool doEvalGradient, int J>
  void evalGradient(DiffEqns, EqIndex<J>, VarGroup<K>, RealType *gradient)  {
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<J>{}, VarGroup<K>{},gradient);
  }

  template <int K, bool doEvalGradient, int J>
  void evalGradient(AlgEqns, EqIndex<J>, VarGroup<K>, RealType *gradient)  {
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<J+numEquations(DiffEqns{})>{}, VarGroup<K>{}, gradient);
  }

  template<int K>
  void setVariable(VarGroup<K> varGroup, const int index, const RealType& value){
    variables[index+variableIndexOffset(varGroup)] = value;
  }

  template<int K>
  RealType getVariable(VarGroup<K> varGroup, const int index) const {
    return variables[index+variableIndexOffset(varGroup)];
  }

  template<int I>
  RealType getAdjoint(EqGroup<I> eqGroup, const int index) const {
    return adjoints[index+equationIndexOffset(eqGroup)];
  }

  template<int I>
  void setAdjoint(EqGroup<I> eqGroup, const int index, const RealType& value){
    adjoints[index+equationIndexOffset(eqGroup)] = value;
  }

  template<int K>
  RealType getInitialValue(VarGroup<K> varGroup, const int index) const {
    return initialValues[index+variableIndexOffset(varGroup)];
  }

  template<int K, int A, int B, int C, int D>
  void setVariables(VarGroup<K>,const Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<Eigen::Matrix<RealType,A,1,B,C,D> > varView(
          variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    varView = values;
  }

  template<int K>
  void setVariables(VarGroup<K>,const RealType *values){
    Eigen::Map<Eigen::Matrix<RealType,Eigen::Dynamic,1> > varView(
        variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    Eigen::Map<const Eigen::Matrix<RealType,Eigen::Dynamic,1> > valuesView(
        values,numVariables(VarGroup<K>{}),1);
    varView = valuesView;
  }

  template<int I, int A, int B, int C, int D>
  void setAdjoints(EqGroup<I>,const Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<Eigen::Matrix<RealType,A,1,B,C,D> > varView(
        adjoints.data()+equationIndexOffset(EqGroup<I>{}),numEquations(EqGroup<I>{}),1);
    varView = values;
  }

  template<int I>
  void setOnesAdjoints(EqGroup<I>) {
    Eigen::Map<Eigen::Matrix<RealType,numEquations(EqGroup<I>{}),1> > varView(
      adjoints.data()+equationIndexOffset(EqGroup<I>{}),numEquations(EqGroup<I>{}),1);
    varView = Eigen::Matrix<RealType,numEquations(EqGroup<I>{}),1>::Ones();
  }

  template<int K, int A, int B, int C, int D>
  void getVariables(VarGroup<K>, Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<Eigen::Matrix<RealType,A,1,B,C,D> > varView(
          variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    values = varView;
  }

  template<int K>
  void getVariables(VarGroup<K>, RealType *values){
    Eigen::Map<Eigen::Matrix<RealType,Eigen::Dynamic,1> > varView(
        variables.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    Eigen::Map<Eigen::Matrix<RealType,Eigen::Dynamic,1> > valuesView(
        values,numVariables(VarGroup<K>{}),1);
    valuesView = varView;
  }

  template<int K, int A, int B, int C, int D>
  void getInitialValues(VarGroup<K>, Eigen::Matrix<RealType,A,1,B,C,D>& values){
    Eigen::Map<const Eigen::Matrix<RealType,A,1,B,C,D> > varView(
          initialValues.data()+variableIndexOffset(VarGroup<K>{}),numVariables(VarGroup<K>{}),1);
    values = varView;
  }

  template<int K, class Indices, class Values>
  void setVariables(VarGroup<K> varGroup, const Indices& indices, const Values& values)
  {
    int index = 0;
    for(auto p = indices.cbegin(); p != indices.cend(); ++p,++index)
    {
      this->setVariable(varGroup,*p,values[index]);
    }
  }


  template<int K, class Tangents, class TangentAdjoints, class RHS>
  void  evalAllSecondOrderAdjoints(AllEqns, VarGroup<K>, const Tangents& tangents,
    const TangentAdjoints& tangentAdjoints, RHS& rhs)
  {
    rhs.setZero();
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<0>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<1>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<2>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<3>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
    evalSecondOrderAdjoints(AllEqns{}, EqIndex<4>{}, VarGroup<K>{},tangents, tangentAdjoints, rhs.data());
  }

  template<int K, class RHS>
  void  evalAllAdjoints(AllEqns, VarGroup<K>, RHS& rhs)
  {
    rhs.setZero();
    evalGradient<K,false>(AllEqns{}, EqIndex<0>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<1>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<2>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<3>{}, VarGroup<K>{}, rhs.data());
    evalGradient<K,false>(AllEqns{}, EqIndex<4>{}, VarGroup<K>{}, rhs.data());
  }

  template<typename  V>
  void  evalAll(AllEqns, V& residuals)
  {
    residuals[0] = eval(AllEqns{},EqIndex<0>{});
    residuals[1] = eval(AllEqns{},EqIndex<1>{});
    residuals[2] = eval(AllEqns{},EqIndex<2>{});
    residuals[3] = eval(AllEqns{},EqIndex<3>{});
    residuals[4] = eval(AllEqns{},EqIndex<4>{});
  }

  template<typename  V>
  void  evalAll(DiffEqns, V& residuals)
  {
    residuals[0] = eval(DiffEqns{},EqIndex<0>{});
    residuals[1] = eval(DiffEqns{},EqIndex<1>{});
    residuals[2] = eval(DiffEqns{},EqIndex<2>{});
    residuals[3] = eval(DiffEqns{},EqIndex<3>{});
  }

  template<typename  V>
  void  evalAll(AlgEqns, V& residuals)
  {
    residuals[0] = eval(AlgEqns{},EqIndex<0>{});
  }

  RealType eval(AllEqns, int equationIndex) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = eval(AllEqns{},EqIndex<0>{}); break;
      case 1: result = eval(AllEqns{},EqIndex<1>{}); break;
      case 2: result = eval(AllEqns{},EqIndex<2>{}); break;
      case 3: result = eval(AllEqns{},EqIndex<3>{}); break;
      case 4: result = eval(AllEqns{},EqIndex<4>{}); break;
    }
    return result;
  }

  RealType eval(DiffEqns, int equationIndex) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = eval(DiffEqns{},EqIndex<0>{}); break;
      case 1: result = eval(DiffEqns{},EqIndex<1>{}); break;
      case 2: result = eval(DiffEqns{},EqIndex<2>{}); break;
      case 3: result = eval(DiffEqns{},EqIndex<3>{}); break;
    }
    return result;
  }

  RealType eval(AlgEqns, int equationIndex) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = eval(AlgEqns{},EqIndex<0>{}); break;
    }
    return result;
  }

template<int I, int K, typename _Scalar, int _Rows, int _Cols, int _Options,
         int _MaxRows, int _MaxCols>
  void evalJacobianPattern(EqGroup<I>, VarGroup<K> ,
      Eigen::Matrix< _Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols >& jacobian)
  {
    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables
    jacobian.resize(numEqns,numVars);
  }

  template<int I, int K, typename _Scalar, int _Rows, int _Cols, int _Options,
      int _MaxRows, int _MaxCols>
  void evalJacobianValues(EqGroup<I> eqGroup, VarGroup<K> varGroup,
      Eigen::Matrix< _Scalar, _Rows, _Cols, _Options,
      _MaxRows, _MaxCols >& jacobian)
  {
    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables
    for(int varIndex=0; varIndex < numVars; ++varIndex){
      for(int eqIndex=0; eqIndex < numEqns;++eqIndex) {
        jacobian(eqIndex,varIndex) =  evalDerivative(eqGroup,eqIndex,varGroup,varIndex);
      }
    }
  }

  template<int I, int K>
  RealType evalDerivative(EqGroup<I> eqGroup, int equationIndex, VarGroup<K>, int variableIndex){
    const int index = variableIndexOffset(VarGroup<K>{}) + variableIndex;
    tangents[index] = static_cast<RealType>(1.);
    RealType result = evalTangent(eqGroup,equationIndex,tangents);
    tangents[index] = static_cast<RealType>(0.);
    return result;
  }

  template<class Tangents>
  RealType evalTangent(AllEqns, int equationIndex, const Tangents& tangents) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = evalTangent(AllEqns{},EqIndex<0>{},tangents); break;
      case 1: result = evalTangent(AllEqns{},EqIndex<1>{},tangents); break;
      case 2: result = evalTangent(AllEqns{},EqIndex<2>{},tangents); break;
      case 3: result = evalTangent(AllEqns{},EqIndex<3>{},tangents); break;
      case 4: result = evalTangent(AllEqns{},EqIndex<4>{},tangents); break;
    }
    return result;
  }

  template<class Tangents>
  RealType evalTangent(DiffEqns, int equationIndex, const Tangents& tangents) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = evalTangent(DiffEqns{},EqIndex<0>{},tangents); break;
      case 1: result = evalTangent(DiffEqns{},EqIndex<1>{},tangents); break;
      case 2: result = evalTangent(DiffEqns{},EqIndex<2>{},tangents); break;
      case 3: result = evalTangent(DiffEqns{},EqIndex<3>{},tangents); break;
    }
    return result;
  }

  template<class Tangents>
  RealType evalTangent(AlgEqns, int equationIndex, const Tangents& tangents) const
  {
    RealType result = static_cast<RealType>(0.);
    switch(equationIndex) {
      case 0: result = evalTangent(AlgEqns{},EqIndex<0>{},tangents); break;
    }
    return result;
  }



  template<int K, bool doEvalGradient = true>
  void evalAllGradients(AllEqns, VarGroup<K>, RealType *gradients)
  {
    startIndex = 0;
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<0>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<1>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<2>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<3>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(AllEqns{},EqIndex<4>{}, VarGroup<K>{}, gradients);
  }

  template<int K, bool doEvalGradient = true>
  void evalAllGradients(DiffEqns, VarGroup<K>, RealType *gradients)
  {
    startIndex = 0;
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<0>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<1>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<2>{}, VarGroup<K>{}, gradients);
    evalGradient<K,doEvalGradient>(DiffEqns{},EqIndex<3>{}, VarGroup<K>{}, gradients);
  }

  template<int K, bool doEvalGradient = true>
  void evalAllGradients(AlgEqns, VarGroup<K>, RealType *gradients) const
  {
    startIndex = 0;
    evalGradient<K,doEvalGradient>(AlgEqns{},EqIndex<0>{}, VarGroup<K>{}, gradients);
  }

  template<int I, class EquationIndices, class Residuals>
  void evalBlock(EqGroup<I> eqGroup,const EquationIndices& indices, Residuals& residuals)
  {
    int index=0;
    for(auto i : indices){
        residuals[index++] = this->eval(eqGroup,i);
    }
  }

  template<int I, int K, class EquationIndices, class VariableIndices, typename Jacobian>
  void evalBlockJacobian(EqGroup<I> eqGroup, VarGroup<K> varGroup,
      const EquationIndices& equationIndices, const VariableIndices& stateIndices,
      Jacobian& jacobian)
  {
    int jcol =0;
    for(auto j = stateIndices.cbegin(); j != stateIndices.cend(); ++j,++jcol) {
      int irow = 0;
      for(auto i = equationIndices.cbegin(); i != equationIndices.cend();++i,++irow)
        jacobian(irow,jcol) = evalDerivative(eqGroup,*i,varGroup,*j);
    }
  }

  template<int I, int K, typename _Scalar>
  void evalJacobianPattern(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<_Scalar>& jacobian)
  {
    using TripletType = Eigen::Triplet<_Scalar>;
    std::vector<TripletType> triplets;
    triplets.reserve(columnIndices.size());
    using IndexTripletType = Eigen::Triplet<int>;
    std::vector<IndexTripletType> indexTriplets;
    indexTriplets.reserve(columnIndices.size());
    const int numEqns = numEquations(EqGroup<I>{});
    const int startRow = equationIndexOffset(EqGroup<I>{});
    const int stopRow = startRow + numEqns;
    const int numVars = numVariables(VarGroup<K>{});
    const int startCol = variableIndexOffset(VarGroup<K>{});
    const int stopCol = startCol + numVars;
    int index = 0;
    for(int i = startRow; i < stopRow; ++i) {
      for(int p = rowPointers[i]; p < rowPointers[i+1];++p) {
        int j = columnIndices[p];
        if( j >= startCol && j < stopCol){
          triplets.push_back(TripletType{i-startRow,j-startCol,_Scalar{0.}});
          indexTriplets.push_back(IndexTripletType(i-startRow,j-startCol,index++));
        }
      }
    }
    jacobian.resize(numEqns,numVars);
    jacobian.setFromTriplets(triplets.begin(),triplets.end());
    jacobian.makeCompressed();
    Eigen::SparseMatrix<int> indexJacobian;
    indexJacobian.resize(numEqns,numVars);
    indexJacobian.setFromTriplets(indexTriplets.begin(),indexTriplets.end());
    indexJacobian.makeCompressed();
    auto *store = jacobianManager.getJacobianStore(EqGroup<I>{}, VarGroup<K>{});
    const size_t nnz = static_cast<size_t>(indexJacobian.nonZeros());
    store->map.resize(nnz);
    store->store.resize(nnz);
    for(size_t i =0; i < nnz;++i)
      store->map[i] = indexJacobian.valuePtr()[i];
  }

  template<int I, int K>
  void evalJacobianValues(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<RealType>& jacobian)
  {
    auto *store = jacobianManager.getJacobianStore(EqGroup<I>{}, VarGroup<K>{});
    evalAllGradients(EqGroup<I>{},VarGroup<K>{},store->store.data());
    RealType *jacobianValues =  jacobian.valuePtr();
    const int nnz = jacobian.nonZeros();
    for(int i=0; i < nnz; ++i){
      jacobianValues[i] = store->store[store->map[i]];
    }

  }

  template<int I, int K>
  void evalTransposedJacobianValues(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<RealType>& transposedJacobian)
  {
    evalAllGradients(EqGroup<I>{},VarGroup<K>{},transposedJacobian.valuePtr());
  }

  template<int I, int K, typename _Scalar>
  void evalTransposedJacobianPattern(EqGroup<I>, VarGroup<K>,
      Eigen::SparseMatrix<_Scalar>& transposedJacobian)
  {
    using TripletType = Eigen::Triplet<_Scalar>;
    std::vector<TripletType> triplets;
    triplets.reserve(columnIndices.size());
    const int numEqns = numEquations(EqGroup<I>{});
    const int startRow = equationIndexOffset(EqGroup<I>{});
    const int stopRow = startRow + numEqns;
    const int numVars = numVariables(VarGroup<K>{});
    const int startCol = variableIndexOffset(VarGroup<K>{});
    const int stopCol = startCol + numVars;
    for(int i = startRow; i < stopRow; ++i) {
      for(int p = rowPointers[i]; p < rowPointers[i+1];++p) {
        int j = columnIndices[p];
        if( j >= startCol && j < stopCol)
          triplets.push_back(TripletType{j-startCol,i-startRow,_Scalar{0.}});
      }
    }
    transposedJacobian.resize(numVars,numEqns);
    transposedJacobian.setFromTriplets(triplets.begin(),triplets.end());
    transposedJacobian.makeCompressed();
  }

  static constexpr int equationIndexOffset(AllEqns){ return 0;}
  static constexpr int equationIndexOffset(DiffEqns){ return equationIndexOffset(AllEqns{});}
  static constexpr int equationIndexOffset(AlgEqns){ return numEquations(DiffEqns{}) + equationIndexOffset(DiffEqns{});}
  static constexpr int variableIndexOffset(AllVars){ return 0;}
  static constexpr int variableIndexOffset(DiffVars){ return 0;}
  static constexpr int variableIndexOffset(AlgVars){ return numVariables(DiffVars{}) + variableIndexOffset(DiffVars{});}
  static constexpr int variableIndexOffset(TimeVar){ return numVariables(AlgVars{}) + variableIndexOffset(AlgVars{});}
  static constexpr int variableIndexOffset(ParamVars){ return numVariables(TimeVar{}) + variableIndexOffset(TimeVar{});}
  static constexpr int variableIndexOffset(DerDiffVars){ return numVariables(ParamVars{}) + variableIndexOffset(ParamVars{}); }
  static constexpr int variableIndexOffset(StateVars){ return 0;}
  static constexpr int variableIndexOffset(StateTimeVars){ return 0;}
  static constexpr int variableIndexOffset(StateTimeParamVars){ return 0;}
  static std::vector<std::string> getVariableNames();
private:
  RealType x(const int i) const { return getVariable(DiffVars{},i); }
  RealType y(const int i) const { return getVariable(AlgVars{},i); }
  RealType der_x(const int i) const { return getVariable(DerDiffVars{},i); }
  RealType p(const int i) const { return getVariable(ParamVars{},i); }
  const RealType c(const int i) const {return  constVariables[i];}
  RealType time() const { return getVariable(TimeVar{},0); }

  template<class Tangents>
  RealType t1_x(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(DiffVars{})]; }
  template<class Tangents>
  RealType t1_y(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(AlgVars{})]; }
  template<class Tangents>
  RealType der_t1_x(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(DerDiffVars{})]; }
  template<class Tangents>
  RealType t1_p(const int i, const Tangents& tangents) const { return tangents[i+variableIndexOffset(ParamVars{})]; }
  const RealType t1_c(const int) const {return static_cast<RealType>(0.);}
  template<class Tangents>
  RealType t1_time(const Tangents& tangents) const { return tangents[variableIndexOffset(TimeVar{})]; }

  int startIndex = 0;
  Eigen::Matrix<RealType,Eigen::Dynamic,1> variables = getInitialValues();
  const std::vector<RealType> constVariables = getConstVariables();
  Eigen::Matrix<RealType,Eigen::Dynamic,1> tangents =
    Eigen::Matrix<RealType,Eigen::Dynamic,1>::Zero(numVariables(AllVars{}),1);
  Eigen::Matrix<RealType,Eigen::Dynamic,1> adjoints =
      Eigen::Matrix<RealType,Eigen::Dynamic,1>::Ones(numEquations(AllEqns{}),1);
  const Eigen::Matrix<RealType,Eigen::Dynamic,1> initialValues = getInitialValues();
  const std::vector<int> rowPointers = getRowPointers();
  const std::vector<int> columnIndices = getColumnIndices();

  static const Eigen::Matrix<RealType,Eigen::Dynamic,1> getInitialValues();
  static const std::vector<RealType> getConstVariables();
  static const std::vector<int> getRowPointers();
  static const std::vector<int> getColumnIndices();
  struct JacobianStore {
    void resize(size_t size) {
      store.resize(size);
      map.resize(size);
    }
    std::vector<RealType> store;
    std::vector<int> map;
  };

  class JacobianManager {
  public:
    template<int I, int J>
    JacobianStore *getJacobianStore(EqGroup<I>,VarGroup<J>)
    {
      static_assert (J < 1024,"Index J of VarGroup<J> is out of range" );
      int key = 1024*I + J;
      if(table.find(key) != table.end()){
        return  table.at(key);
      }
      else {
        stores.emplace_back(std::make_unique<JacobianStore>());
        JacobianStore* entry = stores.back().get();
        table[key] = entry;
        return entry;
      }
    }
  private:
    std::map<int,JacobianStore*> table;
    std::vector<std::unique_ptr<JacobianStore> > stores;
  };
  JacobianManager jacobianManager;
};
// Boiler plate code ends here


template<typename RealType>
const Eigen::Matrix<RealType,Eigen::Dynamic,1> carModel<RealType>::getInitialValues(){
  Eigen::Matrix<RealType,Eigen::Dynamic,1> initialValues(numVariables(AllVars{}));
  initialValues <<
    static_cast<RealType>(2), // s
    static_cast<RealType>(4), // t
    static_cast<RealType>(3), // v
    static_cast<RealType>(5), // obj
    static_cast<RealType>(0.5), // deriv
    static_cast<RealType>(1), // a
    static_cast<RealType>(0.), // der(s)
    static_cast<RealType>(0.), // der(t)
    static_cast<RealType>(0.), // der(v)
    static_cast<RealType>(0.) // der(obj)
  ;
  return initialValues;
}

template<typename RealType>
const std::vector<RealType> carModel<RealType>::getConstVariables(){
  return
  {  static_cast<RealType>(0.36), // alpha
    static_cast<RealType>(15), // k1
    static_cast<RealType>(1), // k2
    static_cast<RealType>(1000) // m
  };
}


template<typename RealType>
std::vector<std::string>  carModel<RealType>::getVariableNames(){
  std::vector<std::string> names;
  names.reserve(static_cast<size_t>(numVariables(AllVars{})));

  names.push_back("s");
  names.push_back("t");
  names.push_back("v");
  names.push_back("obj");
  names.push_back("deriv");
  names.push_back("a");
  names.push_back("der(s)");
  names.push_back("der(t)");
  names.push_back("der(v)");
  names.push_back("der(obj)");
  
  return names;
}

template<typename RealType>
const std::vector<int> carModel<RealType>::getRowPointers(){
  return {
    0,
    2,
    5,
    6,
    8,
    10 };
}

template<typename RealType>
const std::vector<int> carModel<RealType>::getColumnIndices(){
  return {
    2,
    6,
    2,
    5,
    8,
    7,
    4,
    9,
    4,
    5 };
}

#ifdef _MSC_BUILD
#pragma warning(pop)
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif


#endif // car_MODEL_HPP
