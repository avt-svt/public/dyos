#ifndef carModel_MODEL_T2A1_EVAL_HPP
#define carModel_MODEL_T2A1_EVAL_HPP

#include "carModel.hpp"


template<typename RealType>
template<int K, class Tangents, class TangentAdjoints>
void carModel<RealType>::evalSecondOrderAdjoints(AllEqns, EqIndex<0>, VarGroup<K>, const Tangents& tangents,
  const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const {
    RealType v0 = x(2);
    RealType t1_v0 = t1_x(2, tangents);
    RealType v1 = der_x(0);
    RealType t1_v1 = der_t1_x(0, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    RealType t1_a1_v0 = static_cast<RealType>(0.);
    RealType    a1_v0 = static_cast<RealType>(0.);
    RealType t1_a1_v1 = static_cast<RealType>(0.);
    RealType    a1_v1 = static_cast<RealType>(0.);
    RealType t1_a1_v2 = tangentAdjoints[0];
    RealType a1_v2 = getAdjoint(AllEqns{},0);
    t1_a1_v1 -= t1_a1_v2;
    t1_a1_v0 += t1_a1_v2;
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    constexpr int offs = variableIndexOffset(VarGroup<K>{});
    if(VarGroup<K>::value <= 4){
      secOrdAdjoints[2-offs] += t1_a1_v0;
    }
    if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
      secOrdAdjoints[6-offs] += t1_a1_v1;
    }
}

template<typename RealType>
template<int K, class Tangents, class TangentAdjoints>
void carModel<RealType>::evalSecondOrderAdjoints(AllEqns, EqIndex<1>, VarGroup<K>, const Tangents& tangents,
  const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const {
    RealType v0 = p(0);
    RealType t1_v0 = t1_p(0, tangents);
    RealType v1 = c(0);
    RealType t1_v1 = static_cast<RealType>(0.);
    RealType v2 = x(2);
    RealType t1_v2 = t1_x(2, tangents);
    RealType v3 = v1*v2;
    RealType t1_v3 = t1_v1*v2 + v1*t1_v2;
    RealType v4 = v3*v2;
    RealType t1_v4 = t1_v3*v2 + v3*t1_v2;
    RealType v5 = c(3);
    RealType t1_v5 = static_cast<RealType>(0.);
    RealType v6 = v4/v5;
    RealType t1_v6 =  ( v5 * t1_v4 - v4 * t1_v5 ) / ( v5 * v5 );
    RealType v7 = v0-v6;
    RealType t1_v7 = t1_v0-t1_v6;
    RealType v8 = der_x(2);
    RealType t1_v8 = der_t1_x(2, tangents);
    RealType v9 = v7-v8;
    RealType t1_v9 = t1_v7 - t1_v8;
    RealType t1_a1_v0 = static_cast<RealType>(0.);
    RealType    a1_v0 = static_cast<RealType>(0.);
    RealType t1_a1_v1 = static_cast<RealType>(0.);
    RealType    a1_v1 = static_cast<RealType>(0.);
    RealType t1_a1_v2 = static_cast<RealType>(0.);
    RealType    a1_v2 = static_cast<RealType>(0.);
    RealType t1_a1_v3 = static_cast<RealType>(0.);
    RealType    a1_v3 = static_cast<RealType>(0.);
    RealType t1_a1_v4 = static_cast<RealType>(0.);
    RealType    a1_v4 = static_cast<RealType>(0.);
    RealType t1_a1_v5 = static_cast<RealType>(0.);
    RealType    a1_v5 = static_cast<RealType>(0.);
    RealType t1_a1_v6 = static_cast<RealType>(0.);
    RealType    a1_v6 = static_cast<RealType>(0.);
    RealType t1_a1_v7 = static_cast<RealType>(0.);
    RealType    a1_v7 = static_cast<RealType>(0.);
    RealType t1_a1_v8 = static_cast<RealType>(0.);
    RealType    a1_v8 = static_cast<RealType>(0.);
    RealType t1_a1_v9 = tangentAdjoints[1];
    RealType a1_v9 = getAdjoint(AllEqns{},1);
    t1_a1_v8 -= t1_a1_v9;
    t1_a1_v7 += t1_a1_v9;
    a1_v8 -= a1_v9;
    a1_v7 += a1_v9;
    t1_a1_v0 += t1_a1_v7;
    a1_v0 += a1_v7;
    t1_a1_v6 -= t1_a1_v7;
    a1_v6 -= a1_v7;
    RealType t1_tmp_a1_v6 = (t1_a1_v6 * v5 - a1_v6 * t1_v5) / (v5 * v5);
    RealType tmp_a1_v6 = a1_v6 / v5;
    t1_a1_v4 += t1_tmp_a1_v6;
    a1_v4 += tmp_a1_v6;
    t1_a1_v5 -= ((t1_v4 * tmp_a1_v6 + v4 * t1_tmp_a1_v6) * v5 - v4 * tmp_a1_v6 * t1_v5)/(v5 * v5);
    a1_v5 -= v4 * tmp_a1_v6 / v5;
    t1_a1_v3 += t1_a1_v4 * v2 + a1_v4 * t1_v2;
    a1_v3 += a1_v4 * v2;
    t1_a1_v2 += t1_a1_v4 * v3 + a1_v4 * t1_v3;
    a1_v2 += a1_v4 * v3;
    t1_a1_v1 += t1_a1_v3 * v2 + a1_v3 * t1_v2;
    a1_v1 += a1_v3 * v2;
    t1_a1_v2 += t1_a1_v3 * v1 + a1_v3 * t1_v1;
    a1_v2 += a1_v3 * v1;
    constexpr int offs = variableIndexOffset(VarGroup<K>{});
    if(VarGroup<K>::value <= 4){
      secOrdAdjoints[2-offs] += t1_a1_v2;
    }
    if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
      secOrdAdjoints[5-offs] += t1_a1_v0;
    }
    if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
      secOrdAdjoints[8-offs] += t1_a1_v8;
    }
}

template<typename RealType>
template<int K, class Tangents, class TangentAdjoints>
void carModel<RealType>::evalSecondOrderAdjoints(AllEqns, EqIndex<2>, VarGroup<K>, const Tangents& tangents,
  const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const {
    const RealType v0 = static_cast<RealType>(1);
    const RealType t1_v0 = static_cast<RealType>(0.);
    RealType v1 = der_x(1);
    RealType t1_v1 = der_t1_x(1, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    RealType t1_a1_v0 = static_cast<RealType>(0.);
    RealType    a1_v0 = static_cast<RealType>(0.);
    RealType t1_a1_v1 = static_cast<RealType>(0.);
    RealType    a1_v1 = static_cast<RealType>(0.);
    RealType t1_a1_v2 = tangentAdjoints[2];
    RealType a1_v2 = getAdjoint(AllEqns{},2);
    t1_a1_v1 -= t1_a1_v2;
    t1_a1_v0 += t1_a1_v2;
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    t1_a1_v0 = static_cast<RealType>(0.);
    a1_v0 = static_cast<RealType>(0.);
    constexpr int offs = variableIndexOffset(VarGroup<K>{});
    if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
      secOrdAdjoints[7-offs] += t1_a1_v1;
    }
}

template<typename RealType>
template<int K, class Tangents, class TangentAdjoints>
void carModel<RealType>::evalSecondOrderAdjoints(AllEqns, EqIndex<3>, VarGroup<K>, const Tangents& tangents,
  const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const {
    RealType v0 = y(0);
    RealType t1_v0 = t1_y(0, tangents);
    RealType v1 = der_x(3);
    RealType t1_v1 = der_t1_x(3, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    RealType t1_a1_v0 = static_cast<RealType>(0.);
    RealType    a1_v0 = static_cast<RealType>(0.);
    RealType t1_a1_v1 = static_cast<RealType>(0.);
    RealType    a1_v1 = static_cast<RealType>(0.);
    RealType t1_a1_v2 = tangentAdjoints[3];
    RealType a1_v2 = getAdjoint(AllEqns{},3);
    t1_a1_v1 -= t1_a1_v2;
    t1_a1_v0 += t1_a1_v2;
    a1_v1 -= a1_v2;
    a1_v0 += a1_v2;
    constexpr int offs = variableIndexOffset(VarGroup<K>{});
    if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
      secOrdAdjoints[4-offs] += t1_a1_v0;
    }
    if(VarGroup<K>::value == 0 || VarGroup<K>::value == 8){
      secOrdAdjoints[9-offs] += t1_a1_v1;
    }
}

template<typename RealType>
template<int K, class Tangents, class TangentAdjoints>
void carModel<RealType>::evalSecondOrderAdjoints(AllEqns, EqIndex<4>, VarGroup<K>, const Tangents& tangents,
  const TangentAdjoints& tangentAdjoints, RealType *secOrdAdjoints) const {
    using std::tanh;
    RealType v0 = c(2);
    RealType t1_v0 = static_cast<RealType>(0.);
    const RealType v1 = static_cast<RealType>(0.5);
    const RealType t1_v1 = static_cast<RealType>(0.);
    RealType v2 = c(1);
    RealType t1_v2 = static_cast<RealType>(0.);
    RealType v3 = v1*v2;
    RealType t1_v3 = t1_v1*v2 + v1*t1_v2;
    RealType v4 = p(0);
    RealType t1_v4 = t1_p(0, tangents);
    RealType v5 = v3*v4;
    RealType t1_v5 = t1_v3*v4 + v3*t1_v4;
    const RealType v6 = static_cast<RealType>(1);
    const RealType t1_v6 = static_cast<RealType>(0.);
    RealType v7 = c(3);
    RealType t1_v7 = static_cast<RealType>(0.);
    RealType v8 = v7*v4;
    RealType t1_v8 = t1_v7*v4 + v7*t1_v4;
    RealType v9 = tanh(v8);
    RealType t1_v9 = t1_v8 * (-tanh(v8)*tanh(v8) + static_cast<RealType>(1.));
    RealType v10 = v6+v9;
    RealType t1_v10 = t1_v6+t1_v9;
    RealType v11 = v5*v10;
    RealType t1_v11 = t1_v5*v10 + v5*t1_v10;
    RealType v12 = v0+v11;
    RealType t1_v12 = t1_v0+t1_v11;
    RealType v13 = y(0);
    RealType t1_v13 = t1_y(0, tangents);
    RealType v14 = v12-v13;
    RealType t1_v14 = t1_v12 - t1_v13;
    RealType t1_a1_v0 = static_cast<RealType>(0.);
    RealType    a1_v0 = static_cast<RealType>(0.);
    RealType t1_a1_v1 = static_cast<RealType>(0.);
    RealType    a1_v1 = static_cast<RealType>(0.);
    RealType t1_a1_v2 = static_cast<RealType>(0.);
    RealType    a1_v2 = static_cast<RealType>(0.);
    RealType t1_a1_v3 = static_cast<RealType>(0.);
    RealType    a1_v3 = static_cast<RealType>(0.);
    RealType t1_a1_v4 = static_cast<RealType>(0.);
    RealType    a1_v4 = static_cast<RealType>(0.);
    RealType t1_a1_v5 = static_cast<RealType>(0.);
    RealType    a1_v5 = static_cast<RealType>(0.);
    RealType t1_a1_v6 = static_cast<RealType>(0.);
    RealType    a1_v6 = static_cast<RealType>(0.);
    RealType t1_a1_v7 = static_cast<RealType>(0.);
    RealType    a1_v7 = static_cast<RealType>(0.);
    RealType t1_a1_v8 = static_cast<RealType>(0.);
    RealType    a1_v8 = static_cast<RealType>(0.);
    RealType t1_a1_v9 = static_cast<RealType>(0.);
    RealType    a1_v9 = static_cast<RealType>(0.);
    RealType t1_a1_v10 = static_cast<RealType>(0.);
    RealType    a1_v10 = static_cast<RealType>(0.);
    RealType t1_a1_v11 = static_cast<RealType>(0.);
    RealType    a1_v11 = static_cast<RealType>(0.);
    RealType t1_a1_v12 = static_cast<RealType>(0.);
    RealType    a1_v12 = static_cast<RealType>(0.);
    RealType t1_a1_v13 = static_cast<RealType>(0.);
    RealType    a1_v13 = static_cast<RealType>(0.);
    RealType t1_a1_v14 = tangentAdjoints[4];
    RealType a1_v14 = getAdjoint(AllEqns{},4);
    t1_a1_v13 -= t1_a1_v14;
    t1_a1_v12 += t1_a1_v14;
    a1_v13 -= a1_v14;
    a1_v12 += a1_v14;
    t1_a1_v0 += t1_a1_v12;
    a1_v0 += a1_v12;
    t1_a1_v11 += t1_a1_v12;
    a1_v11 += a1_v12;
    t1_a1_v5 += t1_a1_v11 * v10 + a1_v11 * t1_v10;
    a1_v5 += a1_v11 * v10;
    t1_a1_v10 += t1_a1_v11 * v5 + a1_v11 * t1_v5;
    a1_v10 += a1_v11 * v5;
    t1_a1_v6 += t1_a1_v10;
    a1_v6 += a1_v10;
    t1_a1_v9 += t1_a1_v10;
    a1_v9 += a1_v10;
    t1_a1_v8+=t1_a1_v9 * (-tanh(v8)*tanh(v8) + static_cast<RealType>(1.)) + a1_v9 * static_cast<RealType>(2.)*(tanh(v8)*tanh(v8) - static_cast<RealType>(1.)) * tanh(v8) * t1_v8;
    a1_v8 += a1_v9 * (-tanh(v8)*tanh(v8) + static_cast<RealType>(1.));
    t1_a1_v7 += t1_a1_v8 * v4 + a1_v8 * t1_v4;
    a1_v7 += a1_v8 * v4;
    t1_a1_v4 += t1_a1_v8 * v7 + a1_v8 * t1_v7;
    a1_v4 += a1_v8 * v7;
    t1_a1_v6 = static_cast<RealType>(0.);
    a1_v6 = static_cast<RealType>(0.);
    t1_a1_v3 += t1_a1_v5 * v4 + a1_v5 * t1_v4;
    a1_v3 += a1_v5 * v4;
    t1_a1_v4 += t1_a1_v5 * v3 + a1_v5 * t1_v3;
    a1_v4 += a1_v5 * v3;
    t1_a1_v1 += t1_a1_v3 * v2 + a1_v3 * t1_v2;
    a1_v1 += a1_v3 * v2;
    t1_a1_v2 += t1_a1_v3 * v1 + a1_v3 * t1_v1;
    a1_v2 += a1_v3 * v1;
    t1_a1_v1 = static_cast<RealType>(0.);
    a1_v1 = static_cast<RealType>(0.);
    constexpr int offs = variableIndexOffset(VarGroup<K>{});
    if(VarGroup<K>::value <= 3 || VarGroup<K>::value == 5){
      secOrdAdjoints[4-offs] += t1_a1_v13;
    }
    if(VarGroup<K>::value <= 1 || VarGroup<K>::value == 7){
      secOrdAdjoints[5-offs] += t1_a1_v4;
    }
}



#endif // carModel_MODEL_T2A1_EVAL_HPP
