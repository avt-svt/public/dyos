#ifndef carModel_MODEL_T1_EVAL_HPP
#define carModel_MODEL_T1_EVAL_HPP

#include "carModel.hpp"

template<typename RealType>
template<class Tangents>
RealType carModel<RealType>::evalTangent(AllEqns,
  EqIndex<0>, const Tangents& tangents) const {
    RealType v0 = x(2);
    RealType t1_v0 = t1_x(2, tangents);
    RealType v1 = der_x(0);
    RealType t1_v1 = der_t1_x(0, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    return t1_v2;
}

template<typename RealType>
template<class Tangents>
RealType carModel<RealType>::evalTangent(AllEqns,
  EqIndex<1>, const Tangents& tangents) const {
    RealType v0 = p(0);
    RealType t1_v0 = t1_p(0, tangents);
    RealType v1 = c(0);
    RealType t1_v1 = static_cast<RealType>(0.);
    RealType v2 = x(2);
    RealType t1_v2 = t1_x(2, tangents);
    RealType v3 = v1*v2;
    RealType t1_v3 = t1_v1*v2 + v1*t1_v2;
    RealType v4 = v3*v2;
    RealType t1_v4 = t1_v3*v2 + v3*t1_v2;
    RealType v5 = c(3);
    RealType t1_v5 = static_cast<RealType>(0.);
    RealType v6 = v4/v5;
    RealType t1_v6 =  ( v5 * t1_v4 - v4 * t1_v5 ) / ( v5 * v5 );
    RealType v7 = v0-v6;
    RealType t1_v7 = t1_v0-t1_v6;
    RealType v8 = der_x(2);
    RealType t1_v8 = der_t1_x(2, tangents);
    RealType v9 = v7-v8;
    RealType t1_v9 = t1_v7 - t1_v8;
    return t1_v9;
}

template<typename RealType>
template<class Tangents>
RealType carModel<RealType>::evalTangent(AllEqns,
  EqIndex<2>, const Tangents& tangents) const {
    const RealType v0 = static_cast<RealType>(1);
    const RealType t1_v0 = static_cast<RealType>(0.);
    RealType v1 = der_x(1);
    RealType t1_v1 = der_t1_x(1, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    return t1_v2;
}

template<typename RealType>
template<class Tangents>
RealType carModel<RealType>::evalTangent(AllEqns,
  EqIndex<3>, const Tangents& tangents) const {
    RealType v0 = y(0);
    RealType t1_v0 = t1_y(0, tangents);
    RealType v1 = der_x(3);
    RealType t1_v1 = der_t1_x(3, tangents);
    RealType v2 = v0-v1;
    RealType t1_v2 = t1_v0 - t1_v1;
    return t1_v2;
}

template<typename RealType>
template<class Tangents>
RealType carModel<RealType>::evalTangent(AllEqns,
  EqIndex<4>, const Tangents& tangents) const {
    using std::tanh;
    RealType v0 = c(2);
    RealType t1_v0 = static_cast<RealType>(0.);
    const RealType v1 = static_cast<RealType>(0.5);
    const RealType t1_v1 = static_cast<RealType>(0.);
    RealType v2 = c(1);
    RealType t1_v2 = static_cast<RealType>(0.);
    RealType v3 = v1*v2;
    RealType t1_v3 = t1_v1*v2 + v1*t1_v2;
    RealType v4 = p(0);
    RealType t1_v4 = t1_p(0, tangents);
    RealType v5 = v3*v4;
    RealType t1_v5 = t1_v3*v4 + v3*t1_v4;
    const RealType v6 = static_cast<RealType>(1);
    const RealType t1_v6 = static_cast<RealType>(0.);
    RealType v7 = c(3);
    RealType t1_v7 = static_cast<RealType>(0.);
    RealType v8 = v7*v4;
    RealType t1_v8 = t1_v7*v4 + v7*t1_v4;
    RealType v9 = tanh(v8);
    RealType t1_v9 = t1_v8 * (-tanh(v8)*tanh(v8) + static_cast<RealType>(1.));
    RealType v10 = v6+v9;
    RealType t1_v10 = t1_v6+t1_v9;
    RealType v11 = v5*v10;
    RealType t1_v11 = t1_v5*v10 + v5*t1_v10;
    RealType v12 = v0+v11;
    RealType t1_v12 = t1_v0+t1_v11;
    RealType v13 = y(0);
    RealType t1_v13 = t1_y(0, tangents);
    RealType v14 = v12-v13;
    RealType t1_v14 = t1_v12 - t1_v13;
    return t1_v14;
}




#endif // carModel_MODEL_T1_EVAL_HPP
