within ;
model penicillinFMU

// constants
constant Real K_L =     0.006;
constant Real mu =      0.11;
constant Real Y_xs =    0.47;
constant Real theta_m = 0.004;
constant Real Y_p =     1.2;
constant Real K_i =     0.1;
constant Real M_x =     0.029;
constant Real S_0 =     400;
constant Real K_xp =    0.01;
constant Real K_p =     0.0001;

// control
input Real F(start=0.1);

// states
Real X;
Real S;
Real P;
Real V;

// algebraic state as output to be accessible
output Real objective;

initial equation
 X = 1;
 S = 0.5;
 P = 0;
 V = 250;

equation

  der(X) = mu*X*S/(K_L*X + S) -F *X/V;
  der(S) = -mu*X*S/(K_L*X + S)/Y_xs - theta_m/Y_p*(X*S)/(S + K_p + S^2/K_i) -
            M_x*X +F *(S_0 - S)/V;
  der(P) = theta_m*X*S/(S + K_p + S^2/K_i) - K_xp*P -F *P/V;
  der(V) = F;

  objective = -P;

end penicillinFMU;
