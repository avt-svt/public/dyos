#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 0.5; // F
	p[1] = 35; // T
	x[0] = 10; // cA
	x[1] = 1.1685; // cB
	x[2] = 0; // cC
	x[3] = 1; // V
	x[4] = 0; // k1
	x[5] = 0; // k2
	x[6] = 0; // Q
	x[7] = 0; // Obj
	n_x = 8; n_p = 2; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("cA");
	names->push_back("cB");
	names->push_back("cC");
	names->push_back("V");
	names->push_back("k1");
	names->push_back("k2");
	names->push_back("Q");
	names->push_back("Obj");
}
void get_num_vars(int & nv) {
  nv = 8;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("F");
	names->push_back("T");
}
void get_num_pars(int & np) {
  np = 2;
}


void get_num_eqns(int& ne){
 ne = 8;
}

void get_num_cond(int& nc){
 nc = 0;
}
