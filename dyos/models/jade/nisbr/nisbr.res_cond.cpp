#include "EsoAcsammm_Header.hpp"

void res_cond(double * cond, 
double * der_x, double * x, 
double * p, int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep cond
{

	int i_E=0;
	int i_C=0;
	int cond___=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_k10 = 0;//  constant var k10
	double var_k20 = 0;//  constant var k20
	double var_E1 = 0;//  constant var E1
	double var_E2 = 0;//  constant var E2
	double var_DH1 = 0;//  constant var DH1
	double var_DH2 = 0;//  constant var DH2
	double var_R = 0;//  constant var R
	double var_cBin = 0;//  constant var cBin
	double var_CtoK = 0;//  constant var CtoK
	double par_F=0;
	double par_T=0;
	double var_cA=0;
	double der_var_cA=0;
	double var_cB=0;
	double der_var_cB=0;
	double var_cC=0;
	double der_var_cC=0;
	double var_V=0;
	double der_var_V=0;
	double var_k1=0;
	double der_var_k1=0;
	double var_k2=0;
	double der_var_k2=0;
	double var_Q=0;
	double der_var_Q=0;
	double var_Obj=0;
	double der_var_Obj=0;
	double var_div0=0; // procedure var
	double var_div1=0; // procedure var
	double var_div2=0; // procedure var
	double var_i_expr=0; // assign var
	double var_div3=0; // procedure var
	double var_exp4=0; // procedure var
	double var_i_expr5=0; // assign var
	double var_i_expr7=0; // assign var
	double var_div6=0; // procedure var
	double var_exp8=0; // procedure var
	double var_i_expr9=0; // assign var
	double var_i_expr11=0; // assign var
	double var_i_expr12=0; // assign var
	double var_div10=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_k10 = 4;
	var_k20 = 800;
	var_E1 = 6000;
	var_E2 = 20000;
	var_DH1 = -30000;
	var_DH2 = -10000;
	var_R = 8.31;
	var_cBin = 20;
	var_CtoK = 273;
	par_F = p[0];
	par_T = p[1];
	var_cA = x[0];
	der_var_cA = der_x[0];
	var_cB = x[1];
	der_var_cB = der_x[1];
	var_cC = x[2];
	der_var_cC = der_x[2];
	var_V = x[3];
	der_var_V = der_x[3];
	var_k1 = x[4];
	der_var_k1 = der_x[4];
	var_k2 = x[5];
	der_var_k2 = der_x[5];
	var_Q = x[6];
	der_var_Q = der_x[6];
	var_Obj = x[7];
	der_var_Obj = der_x[7];
// procedure assignment div0 not here
// procedure assignment div1 not here
// procedure assignment div2 not here
// temporary assignment i_expr not here
// procedure assignment div3 not here
// procedure assignment exp4 not here
// temporary assignment i_expr5 not here
// temporary assignment i_expr7 not here
// procedure assignment div6 not here
// procedure assignment exp8 not here
// temporary assignment i_expr9 not here
// temporary assignment i_expr11 not here
// temporary assignment i_expr12 not here
// procedure assignment div10 not here

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

  }
