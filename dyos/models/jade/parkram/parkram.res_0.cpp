#include "EsoAcsammm_Header.hpp"

void res(double * yy, 
double* der_x, double * x, 
double* p,
int* condit, int* lock, int* prev,
int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double par_u=0;
	double var_z1=0;
	double der_var_z1=0;
	double var_z2=0;
	double der_var_z2=0;
	double var_z3=0;
	double der_var_z3=0;
	double var_z4=0;
	double der_var_z4=0;
	double var_z5=0;
	double der_var_z5=0;
	double var_g1=0;
	double der_var_g1=0;
	double var_g2=0;
	double der_var_g2=0;
	double var_g3=0;
	double der_var_g3=0;
	double var_g4=0;
	double der_var_g4=0;
	double var_obj=0;
	double der_var_obj=0;
	double var_div0=0; // procedure var
	double var_div1=0; // procedure var
	double var_div2=0; // procedure var
	double var_div3=0; // procedure var
	double var_i_expr=0; // assign var
	double var_div4=0; // procedure var
	double var_i_expr6=0; // assign var
	double var_div5=0; // procedure var
	double var_exp7=0; // procedure var
	double var_i_expr8=0; // assign var
	double var_i_expr10=0; // assign var
	double var_div9=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	par_u = p[0];
	var_z1 = x[0];
	der_var_z1 = der_x[0];
	var_z2 = x[1];
	der_var_z2 = der_x[1];
	var_z3 = x[2];
	der_var_z3 = der_x[2];
	var_z4 = x[3];
	der_var_z4 = der_x[3];
	var_z5 = x[4];
	der_var_z5 = der_x[4];
	var_g1 = x[5];
	der_var_g1 = der_x[5];
	var_g2 = x[6];
	der_var_g2 = der_x[6];
	var_g3 = x[7];
	der_var_g3 = der_x[7];
	var_g4 = x[8];
	der_var_g4 = der_x[8];
	var_obj = x[9];
	der_var_obj = der_x[9];
// procedure assignment div0 not here
// procedure assignment div1 not here
// procedure assignment div2 not here
// procedure assignment div3 not here
// temporary assignment i_expr not here
// procedure assignment div4 not here
// temporary assignment i_expr6 not here
// procedure assignment div5 not here
// procedure assignment exp7 not here
// temporary assignment i_expr8 not here
// temporary assignment i_expr10 not here
// procedure assignment div9 not here

// #pragma dcc equation begin
// scalar equation 0
acs_div(par_u, var_z5, var_div0);
yy[i_E] = der_var_z1 - (var_g1 *  ( var_z2-var_z1 ) -var_div0 * var_z1);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 1
acs_div(par_u, var_z5, var_div1);
yy[i_E] = der_var_z2 - (var_g2 * var_z3-var_div1 * var_z2);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 2
acs_div(par_u, var_z5, var_div2);
yy[i_E] = der_var_z3 - (var_g3 * var_z3-var_div2 * var_z3);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 3
acs_div(par_u, var_z5, var_div3);
yy[i_E] = der_var_z4 - (-7.3 * var_g3 * var_z3+var_div3 *  ( 20-var_z4 ) );
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 4
yy[i_E] = der_var_z5 - (par_u);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 5
var_i_expr= ( 0.12+var_g3 ) ;
acs_div(var_g3, var_i_expr, var_div4);
yy[i_E] = var_g1 - (4.75 * var_div4);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 6
var_i_expr6= ( 0.1+var_z4 ) ;
acs_div(var_z4, var_i_expr6, var_div5);
var_i_expr8=-5 * var_z4;
acs_exp(var_i_expr8, var_exp7);
yy[i_E] = var_g2 - (var_div5 * var_exp7);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 7
var_i_expr10= (  ( var_z4+0.4 )  *  ( var_z4+62.5 )  ) ;
acs_div(var_z4, var_i_expr10, var_div9);
yy[i_E] = var_g3 - (21.87 * var_div9);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 8
yy[i_E] = var_g4 - (58.75 * var_g3 * var_g3+1.71);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 9
yy[i_E] = var_obj - (-var_z1 * var_z5);
i_E = i_E+1;
// #pragma dcc equation end

n_c=i_C;
}
