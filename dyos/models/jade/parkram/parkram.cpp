#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 0.5; // u
	x[0] = 0; // z1
	x[1] = 0; // z2
	x[2] = 1; // z3
	x[3] = 5; // z4
	x[4] = 1; // z5
	x[5] = 0; // g1
	x[6] = 0; // g2
	x[7] = 0; // g3
	x[8] = 0; // g4
	x[9] = 0; // obj
	n_x = 10; n_p = 1; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("z1");
	names->push_back("z2");
	names->push_back("z3");
	names->push_back("z4");
	names->push_back("z5");
	names->push_back("g1");
	names->push_back("g2");
	names->push_back("g3");
	names->push_back("g4");
	names->push_back("obj");
}
void get_num_vars(int & nv) {
  nv = 10;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("u");
}
void get_num_pars(int & np) {
  np = 1;
}


void get_num_eqns(int& ne){
 ne = 10;
}

void get_num_cond(int& nc){
 nc = 0;
}
