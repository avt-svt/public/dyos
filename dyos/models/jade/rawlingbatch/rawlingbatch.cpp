#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 1; // eta
	p[1] = 0.5; // cA0
	p[2] = 0.05; // cB0
	p[3] = 0; // cC0
	p[4] = 0.5; // K1
	p[5] = 0.05; // K2
	p[6] = 0.2; // K3
	p[7] = 0.01; // K4
	p[8] = 0.0625; // R
	x[0] = 0.5; // cA
	x[1] = 0.05; // cB
	x[2] = 0; // cC
	x[3] = 0; // r1
	x[4] = 0; // r2
	x[5] = 0; // y
	x[6] = 0; // summand
	x[7] = 0; // summandstart
	n_x = 8; n_p = 9; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("cA");
	names->push_back("cB");
	names->push_back("cC");
	names->push_back("r1");
	names->push_back("r2");
	names->push_back("y");
	names->push_back("summand");
	names->push_back("summandstart");
}
void get_num_vars(int & nv) {
  nv = 8;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("eta");
	names->push_back("cA0");
	names->push_back("cB0");
	names->push_back("cC0");
	names->push_back("K1");
	names->push_back("K2");
	names->push_back("K3");
	names->push_back("K4");
	names->push_back("R");
}
void get_num_pars(int & np) {
  np = 9;
}


void get_num_eqns(int& ne){
 ne = 8;
}

void get_num_cond(int& nc){
 nc = 0;
}
