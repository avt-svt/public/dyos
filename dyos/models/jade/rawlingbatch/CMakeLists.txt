cmake_minimum_required(VERSION 3.11)


project(rawlingbatch CXX)

add_library(rawlingbatch SHARED
 	rawlingbatch.cpp
	rawlingbatch.block_0.cpp
	rawlingbatch.res_0.cpp
	a1_rawlingbatch.res_0.cpp
	jsp_rawlingbatch.res_0.cpp
	t1_rawlingbatch.block_0.cpp
	t1_rawlingbatch.res_0.cpp
	t2_a1_rawlingbatch.res_0.cpp
	rawlingbatch.eval_cond.cpp
	rawlingbatch.res_cond.cpp
)


set_property(TARGET rawlingbatch PROPERTY COMPILE_DEFINITIONS MAKE_MOFC_DLL)

target_link_libraries(rawlingbatch debug EsoCommon)
target_link_libraries(rawlingbatch optimized EsoCommon)

if(MSVC)
  set_target_properties(rawlingbatch PROPERTIES COMPILE_FLAGS -wd4068)
  set_target_properties(rawlingbatch PROPERTIES FOLDER Models)
endif()
