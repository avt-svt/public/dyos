#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 2; // accel
	p[1] = 0.0025; // alpha
	p[2] = 1; // tf
	x[0] = 0; // ttime
	x[1] = 0; // velo
	x[2] = 0; // dist
	n_x = 3; n_p = 3; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("ttime");
	names->push_back("velo");
	names->push_back("dist");
}
void get_num_vars(int & nv) {
  nv = 3;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("accel");
	names->push_back("alpha");
	names->push_back("tf");
}
void get_num_pars(int & np) {
  np = 3;
}


void get_num_eqns(int& ne){
 ne = 3;
}

void get_num_cond(int& nc){
 nc = 0;
}
