#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	x[0] = 0; // ttime
	x[1] = 0; // dist
	x[2] = 0; // velo
	x[3] = 0; // accel
	x[4] = 0; // obj
	p[0] = 0.015; // alpha
	p[1] = 2; // k
	n_x = 5; n_p = 2; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("ttime");
	names->push_back("dist");
	names->push_back("velo");
	names->push_back("accel");
	names->push_back("obj");
}
void get_num_vars(int & nv) {
  nv = 5;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("alpha");
	names->push_back("k");
}
void get_num_pars(int & np) {
  np = 2;
}


void get_num_eqns(int& ne){
 ne = 5;
}

void get_num_cond(int& nc){
 nc = 0;
}
