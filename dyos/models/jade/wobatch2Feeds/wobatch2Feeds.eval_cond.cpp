#include "EsoAcsammm_Header.hpp"

void eval_cond(int * cond, 
double * der_x, double * x, 
double * p, int &n_x, int &n_p, int &n_c)
{

	int i_E=0;
	int i_C=0;
	int cond___=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_n_x = 0;//  constant var n_x
	double var_Tin = 0;//  constant var Tin
	double var_FbinMin = 0;//  constant var FbinMin
	double var_FbinMax = 0;//  constant var FbinMax
	double var_TwMin = 0;//  constant var TwMin
	double var_TwMax = 0;//  constant var TwMax
	double var_TK0 = 0;//  constant var TK0
	double var_b2 = 0;//  constant var b2
	double var_Fscal = 0;//  constant var Fscal
	double var_MA = 0;//  constant var MA
	double var_MB = 0;//  constant var MB
	double var_MC = 0;//  constant var MC
	double var_ME = 0;//  constant var ME
	double var_MG = 0;//  constant var MG
	double var_MP = 0;//  constant var MP
	double var_k1 = 0;//  constant var k1
	double var_k2 = 0;//  constant var k2
	double var_k3 = 0;//  constant var k3
	double var_E1 = 0;//  constant var E1
	double var_E2 = 0;//  constant var E2
	double var_E3 = 0;//  constant var E3
	double var_DH1 = 0;//  constant var DH1
	double var_DH2 = 0;//  constant var DH2
	double var_DH3 = 0;//  constant var DH3
	double var_CP = 0;//  constant var CP
	double var_cRevP = 0;//  constant var cRevP
	double var_cRevE = 0;//  constant var cRevE
	double var_Aref = 0;//  constant var Aref
	double var_mref = 0;//  constant var mref
	double var_U = 0;//  constant var U
	double par_TwCur=0;
	double par_FbinCur=0;
	double par_FainCur=0;
	double par_FinalTime=0;
	double var_rhoA=0;
	double der_var_rhoA=0;
	double var_rhoB=0;
	double der_var_rhoB=0;
	double var_rhoC=0;
	double der_var_rhoC=0;
	double var_rhoE=0;
	double der_var_rhoE=0;
	double var_rhoG=0;
	double der_var_rhoG=0;
	double var_rhoP=0;
	double der_var_rhoP=0;
	double var_TR=0;
	double der_var_TR=0;
	double var_V=0;
	double der_var_V=0;
	double var_phi=0;
	double der_var_phi=0;
	double var_r1=0;
	double der_var_r1=0;
	double var_r2=0;
	double der_var_r2=0;
	double var_r3=0;
	double der_var_r3=0;
	double var_i_expr=0; // assign var
	double var_div0=0; // procedure var
	double var_i_expr2=0; // assign var
	double var_i_expr3=0; // assign var
	double var_div1=0; // procedure var
	double var_i_expr5=0; // assign var
	double var_div4=0; // procedure var
	double var_i_expr7=0; // assign var
	double var_i_expr8=0; // assign var
	double var_div6=0; // procedure var
	double var_i_expr10=0; // assign var
	double var_i_expr11=0; // assign var
	double var_div9=0; // procedure var
	double var_div12=0; // procedure var
	double var_div13=0; // procedure var
	double var_i_expr15=0; // assign var
	double var_i_expr16=0; // assign var
	double var_div14=0; // procedure var
	double var_div17=0; // procedure var
	double var_i_expr19=0; // assign var
	double var_i_expr20=0; // assign var
	double var_div18=0; // procedure var
	double var_div21=0; // procedure var
	double var_i_expr23=0; // assign var
	double var_i_expr24=0; // assign var
	double var_div22=0; // procedure var
	double var_div25=0; // procedure var
	double var_i_expr27=0; // assign var
	double var_i_expr28=0; // assign var
	double var_div26=0; // procedure var
	double var_div29=0; // procedure var
	double var_div30=0; // procedure var
	double var_div31=0; // procedure var
	double var_i_expr33=0; // assign var
	double var_i_expr34=0; // assign var
	double var_div32=0; // procedure var
	double var_i_expr36=0; // assign var
	double var_div35=0; // procedure var
	double var_i_expr38=0; // assign var
	double var_div37=0; // procedure var
	double var_exp39=0; // procedure var
	double var_i_expr41=0; // assign var
	double var_div40=0; // procedure var
	double var_exp42=0; // procedure var
	double var_i_expr44=0; // assign var
	double var_div43=0; // procedure var
	double var_exp45=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_n_x = 9;
	var_Tin = 35;
	var_FbinMin = 0;
	var_FbinMax = 5.784;
	var_TwMin = 20;
	var_TwMax = 100;
	var_TK0 = 273.15;
	var_b2 = 6.6667;
	var_Fscal = 1000;
	var_MA = 100;
	var_MB = 100;
	var_MC = 200;
	var_ME = 200;
	var_MG = 300;
	var_MP = 100;
	var_k1 = 1.6599e+06;
	var_k2 = 7.2117e+08;
	var_k3 = 2.6745e+12;
	var_E1 = -6666.7;
	var_E2 = -8333.3;
	var_E3 = -11111;
	var_DH1 = -263.8;
	var_DH2 = -158.3;
	var_DH3 = -226.3;
	var_CP = 4.184;
	var_cRevP = 5554.1;
	var_cRevE = 125.91;
	var_Aref = 9.2903;
	var_mref = 2105.2;
	var_U = 0.23082;
	par_TwCur = p[0];
	par_FbinCur = p[1];
	par_FainCur = p[2];
	par_FinalTime = p[3];
	var_rhoA = x[0];
	der_var_rhoA = der_x[0];
	var_rhoB = x[1];
	der_var_rhoB = der_x[1];
	var_rhoC = x[2];
	der_var_rhoC = der_x[2];
	var_rhoE = x[3];
	der_var_rhoE = der_x[3];
	var_rhoG = x[4];
	der_var_rhoG = der_x[4];
	var_rhoP = x[5];
	der_var_rhoP = der_x[5];
	var_TR = x[6];
	der_var_TR = der_x[6];
	var_V = x[7];
	der_var_V = der_x[7];
	var_phi = x[8];
	der_var_phi = der_x[8];
	var_r1 = x[9];
	der_var_r1 = der_x[9];
	var_r2 = x[10];
	der_var_r2 = der_x[10];
	var_r3 = x[11];
	der_var_r3 = der_x[11];
// temporary assignment i_expr not here
// procedure assignment div0 not here
// temporary assignment i_expr2 not here
// temporary assignment i_expr3 not here
// procedure assignment div1 not here
// temporary assignment i_expr5 not here
// procedure assignment div4 not here
// temporary assignment i_expr7 not here
// temporary assignment i_expr8 not here
// procedure assignment div6 not here
// temporary assignment i_expr10 not here
// temporary assignment i_expr11 not here
// procedure assignment div9 not here
// procedure assignment div12 not here
// procedure assignment div13 not here
// temporary assignment i_expr15 not here
// temporary assignment i_expr16 not here
// procedure assignment div14 not here
// procedure assignment div17 not here
// temporary assignment i_expr19 not here
// temporary assignment i_expr20 not here
// procedure assignment div18 not here
// procedure assignment div21 not here
// temporary assignment i_expr23 not here
// temporary assignment i_expr24 not here
// procedure assignment div22 not here
// procedure assignment div25 not here
// temporary assignment i_expr27 not here
// temporary assignment i_expr28 not here
// procedure assignment div26 not here
// procedure assignment div29 not here
// procedure assignment div30 not here
// procedure assignment div31 not here
// temporary assignment i_expr33 not here
// temporary assignment i_expr34 not here
// procedure assignment div32 not here
// temporary assignment i_expr36 not here
// procedure assignment div35 not here
// temporary assignment i_expr38 not here
// procedure assignment div37 not here
// procedure assignment exp39 not here
// temporary assignment i_expr41 not here
// procedure assignment div40 not here
// procedure assignment exp42 not here
// temporary assignment i_expr44 not here
// procedure assignment div43 not here
// procedure assignment exp45 not here

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

//condition of equation begin
 //end

  }
