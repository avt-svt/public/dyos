#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 20; // TwCur
	p[1] = 0; // FbinCur
	p[2] = 0; // FainCur
	p[3] = 1; // FinalTime
	x[0] = 0; // rhoA
	x[1] = 1; // rhoB
	x[2] = 0; // rhoC
	x[3] = 0; // rhoE
	x[4] = 0; // rhoG
	x[5] = 0; // rhoP
	x[6] = 65; // TR
	x[7] = 2; // V
	x[8] = 0; // phi
	x[9] = 0; // r1
	x[10] = 0; // r2
	x[11] = 0; // r3
	n_x = 12; n_p = 4; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("rhoA");
	names->push_back("rhoB");
	names->push_back("rhoC");
	names->push_back("rhoE");
	names->push_back("rhoG");
	names->push_back("rhoP");
	names->push_back("TR");
	names->push_back("V");
	names->push_back("phi");
	names->push_back("r1");
	names->push_back("r2");
	names->push_back("r3");
}
void get_num_vars(int & nv) {
  nv = 12;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("TwCur");
	names->push_back("FbinCur");
	names->push_back("FainCur");
	names->push_back("FinalTime");
}
void get_num_pars(int & np) {
  np = 4;
}


void get_num_eqns(int& ne){
 ne = 12;
}

void get_num_cond(int& nc){
 nc = 0;
}
