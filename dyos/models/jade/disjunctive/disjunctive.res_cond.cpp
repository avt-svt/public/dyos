#include "EsoAcsammm_Header.hpp"

void res_cond(double * cond, 
double * der_x, double * x, 
double * p, int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep cond
{

	int i_E=0;
	int i_C=0;
	int cond___=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_bigM = 0;//  constant var bigM
	double par_p=0;
	double par_ybin1d1=0;
	double par_ybin1d2=0;
	double par_ybin2d1=0;
	double par_ybin2d2=0;
	double par_ybin3d1=0;
	double par_ybin3d2=0;
	double par_x10d1=0;
	double par_x10d2=0;
	double par_x20d1=0;
	double par_x20d2=0;
	double par_x30d1=0;
	double par_x30d2=0;
	double par_xobj11=0;
	double par_xobj12=0;
	double par_xobj21=0;
	double par_xobj22=0;
	double par_xobj31=0;
	double par_xobj32=0;
	double var_x1d1=0;
	double der_var_x1d1=0;
	double var_x1d2=0;
	double der_var_x1d2=0;
	double var_x2d1=0;
	double der_var_x2d1=0;
	double var_x2d2=0;
	double der_var_x2d2=0;
	double var_x3d1=0;
	double der_var_x3d1=0;
	double var_x3d2=0;
	double der_var_x3d2=0;
	double var_xobj1d1=0;
	double der_var_xobj1d1=0;
	double var_xobj1d2=0;
	double der_var_xobj1d2=0;
	double var_xobj2d1=0;
	double der_var_xobj2d1=0;
	double var_xobj2d2=0;
	double der_var_xobj2d2=0;
	double var_xobj3d1=0;
	double der_var_xobj3d1=0;
	double var_xobj3d2=0;
	double der_var_xobj3d2=0;
	double var_x1Strd1=0;
	double der_var_x1Strd1=0;
	double var_x1Strd2=0;
	double der_var_x1Strd2=0;
	double var_x2Strd1=0;
	double der_var_x2Strd1=0;
	double var_x2Strd2=0;
	double der_var_x2Strd2=0;
	double var_x3Strd1=0;
	double der_var_x3Strd1=0;
	double var_x3Strd2=0;
	double der_var_x3Strd2=0;
	double var_xobj=0;
	double der_var_xobj=0;
	double var_constr1_lo=0;
	double der_var_constr1_lo=0;
	double var_constr1_up=0;
	double der_var_constr1_up=0;
	double var_constr2_lo=0;
	double der_var_constr2_lo=0;
	double var_constr2_up=0;
	double der_var_constr2_up=0;
	double var_constr3_lo=0;
	double der_var_constr3_lo=0;
	double var_constr3_up=0;
	double der_var_constr3_up=0;
	double var_constr4_lo=0;
	double der_var_constr4_lo=0;
	double var_constr4_up=0;
	double der_var_constr4_up=0;
	double var_constr5_lo=0;
	double der_var_constr5_lo=0;
	double var_constr5_up=0;
	double der_var_constr5_up=0;
	double var_constr6_lo=0;
	double der_var_constr6_lo=0;
	double var_constr6_up=0;
	double der_var_constr6_up=0;
	double var_constr7_lo=0;
	double der_var_constr7_lo=0;
	double var_constr7_up=0;
	double der_var_constr7_up=0;
	double var_constr8_lo=0;
	double der_var_constr8_lo=0;
	double var_constr8_up=0;
	double der_var_constr8_up=0;
	double var_constr9_lo=0;
	double der_var_constr9_lo=0;
	double var_constr9_up=0;
	double der_var_constr9_up=0;
	double var_constr11_lo=0;
	double der_var_constr11_lo=0;
	double var_constr11_up=0;
	double der_var_constr11_up=0;
	double var_constr13_lo=0;
	double der_var_constr13_lo=0;
	double var_constr13_up=0;
	double der_var_constr13_up=0;
	double var_constr15_lo=0;
	double der_var_constr15_lo=0;
	double var_constr15_up=0;
	double der_var_constr15_up=0;
	double var_obj1_lo=0;
	double der_var_obj1_lo=0;
	double var_obj1_up=0;
	double der_var_obj1_up=0;
	double var_obj2_lo=0;
	double der_var_obj2_lo=0;
	double var_obj2_up=0;
	double der_var_obj2_up=0;
	double var_obj3_lo=0;
	double der_var_obj3_lo=0;
	double var_obj3_up=0;
	double der_var_obj3_up=0;
	double var_obj4_lo=0;
	double der_var_obj4_lo=0;
	double var_obj4_up=0;
	double der_var_obj4_up=0;
	double var_obj5_lo=0;
	double der_var_obj5_lo=0;
	double var_obj5_up=0;
	double der_var_obj5_up=0;
	double var_obj6_lo=0;
	double der_var_obj6_lo=0;
	double var_obj6_up=0;
	double der_var_obj6_up=0;
	double var_obj7_lo=0;
	double der_var_obj7_lo=0;
	double var_obj7_up=0;
	double der_var_obj7_up=0;
	double var_obj8_lo=0;
	double der_var_obj8_lo=0;
	double var_obj8_up=0;
	double der_var_obj8_up=0;
	double var_obj9_lo=0;
	double der_var_obj9_lo=0;
	double var_obj9_up=0;
	double der_var_obj9_up=0;
	double var_obj10_lo=0;
	double der_var_obj10_lo=0;
	double var_obj10_up=0;
	double der_var_obj10_up=0;
	double var_obj11_lo=0;
	double der_var_obj11_lo=0;
	double var_obj11_up=0;
	double der_var_obj11_up=0;
	double var_obj12_lo=0;
	double der_var_obj12_lo=0;
	double var_obj12_up=0;
	double der_var_obj12_up=0;
	double var_tim=0;
	double der_var_tim=0;
	double var_i_expr=0; // assign var
	double var_i_expr1=0; // assign var
	double var_div0=0; // procedure var
	double var_i_expr3=0; // assign var
	double var_i_expr4=0; // assign var
	double var_div2=0; // procedure var
	double var_i_expr6=0; // assign var
	double var_i_expr7=0; // assign var
	double var_div5=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_bigM = 30;
	par_p = p[0];
	par_ybin1d1 = p[1];
	par_ybin1d2 = p[2];
	par_ybin2d1 = p[3];
	par_ybin2d2 = p[4];
	par_ybin3d1 = p[5];
	par_ybin3d2 = p[6];
	par_x10d1 = p[7];
	par_x10d2 = p[8];
	par_x20d1 = p[9];
	par_x20d2 = p[10];
	par_x30d1 = p[11];
	par_x30d2 = p[12];
	par_xobj11 = p[13];
	par_xobj12 = p[14];
	par_xobj21 = p[15];
	par_xobj22 = p[16];
	par_xobj31 = p[17];
	par_xobj32 = p[18];
	var_x1d1 = x[0];
	der_var_x1d1 = der_x[0];
	var_x1d2 = x[1];
	der_var_x1d2 = der_x[1];
	var_x2d1 = x[2];
	der_var_x2d1 = der_x[2];
	var_x2d2 = x[3];
	der_var_x2d2 = der_x[3];
	var_x3d1 = x[4];
	der_var_x3d1 = der_x[4];
	var_x3d2 = x[5];
	der_var_x3d2 = der_x[5];
	var_xobj1d1 = x[6];
	der_var_xobj1d1 = der_x[6];
	var_xobj1d2 = x[7];
	der_var_xobj1d2 = der_x[7];
	var_xobj2d1 = x[8];
	der_var_xobj2d1 = der_x[8];
	var_xobj2d2 = x[9];
	der_var_xobj2d2 = der_x[9];
	var_xobj3d1 = x[10];
	der_var_xobj3d1 = der_x[10];
	var_xobj3d2 = x[11];
	der_var_xobj3d2 = der_x[11];
	var_x1Strd1 = x[12];
	der_var_x1Strd1 = der_x[12];
	var_x1Strd2 = x[13];
	der_var_x1Strd2 = der_x[13];
	var_x2Strd1 = x[14];
	der_var_x2Strd1 = der_x[14];
	var_x2Strd2 = x[15];
	der_var_x2Strd2 = der_x[15];
	var_x3Strd1 = x[16];
	der_var_x3Strd1 = der_x[16];
	var_x3Strd2 = x[17];
	der_var_x3Strd2 = der_x[17];
	var_xobj = x[18];
	der_var_xobj = der_x[18];
	var_constr1_lo = x[19];
	der_var_constr1_lo = der_x[19];
	var_constr1_up = x[20];
	der_var_constr1_up = der_x[20];
	var_constr2_lo = x[21];
	der_var_constr2_lo = der_x[21];
	var_constr2_up = x[22];
	der_var_constr2_up = der_x[22];
	var_constr3_lo = x[23];
	der_var_constr3_lo = der_x[23];
	var_constr3_up = x[24];
	der_var_constr3_up = der_x[24];
	var_constr4_lo = x[25];
	der_var_constr4_lo = der_x[25];
	var_constr4_up = x[26];
	der_var_constr4_up = der_x[26];
	var_constr5_lo = x[27];
	der_var_constr5_lo = der_x[27];
	var_constr5_up = x[28];
	der_var_constr5_up = der_x[28];
	var_constr6_lo = x[29];
	der_var_constr6_lo = der_x[29];
	var_constr6_up = x[30];
	der_var_constr6_up = der_x[30];
	var_constr7_lo = x[31];
	der_var_constr7_lo = der_x[31];
	var_constr7_up = x[32];
	der_var_constr7_up = der_x[32];
	var_constr8_lo = x[33];
	der_var_constr8_lo = der_x[33];
	var_constr8_up = x[34];
	der_var_constr8_up = der_x[34];
	var_constr9_lo = x[35];
	der_var_constr9_lo = der_x[35];
	var_constr9_up = x[36];
	der_var_constr9_up = der_x[36];
	var_constr11_lo = x[37];
	der_var_constr11_lo = der_x[37];
	var_constr11_up = x[38];
	der_var_constr11_up = der_x[38];
	var_constr13_lo = x[39];
	der_var_constr13_lo = der_x[39];
	var_constr13_up = x[40];
	der_var_constr13_up = der_x[40];
	var_constr15_lo = x[41];
	der_var_constr15_lo = der_x[41];
	var_constr15_up = x[42];
	der_var_constr15_up = der_x[42];
	var_obj1_lo = x[43];
	der_var_obj1_lo = der_x[43];
	var_obj1_up = x[44];
	der_var_obj1_up = der_x[44];
	var_obj2_lo = x[45];
	der_var_obj2_lo = der_x[45];
	var_obj2_up = x[46];
	der_var_obj2_up = der_x[46];
	var_obj3_lo = x[47];
	der_var_obj3_lo = der_x[47];
	var_obj3_up = x[48];
	der_var_obj3_up = der_x[48];
	var_obj4_lo = x[49];
	der_var_obj4_lo = der_x[49];
	var_obj4_up = x[50];
	der_var_obj4_up = der_x[50];
	var_obj5_lo = x[51];
	der_var_obj5_lo = der_x[51];
	var_obj5_up = x[52];
	der_var_obj5_up = der_x[52];
	var_obj6_lo = x[53];
	der_var_obj6_lo = der_x[53];
	var_obj6_up = x[54];
	der_var_obj6_up = der_x[54];
	var_obj7_lo = x[55];
	der_var_obj7_lo = der_x[55];
	var_obj7_up = x[56];
	der_var_obj7_up = der_x[56];
	var_obj8_lo = x[57];
	der_var_obj8_lo = der_x[57];
	var_obj8_up = x[58];
	der_var_obj8_up = der_x[58];
	var_obj9_lo = x[59];
	der_var_obj9_lo = der_x[59];
	var_obj9_up = x[60];
	der_var_obj9_up = der_x[60];
	var_obj10_lo = x[61];
	der_var_obj10_lo = der_x[61];
	var_obj10_up = x[62];
	der_var_obj10_up = der_x[62];
	var_obj11_lo = x[63];
	der_var_obj11_lo = der_x[63];
	var_obj11_up = x[64];
	der_var_obj11_up = der_x[64];
	var_obj12_lo = x[65];
	der_var_obj12_lo = der_x[65];
	var_obj12_up = x[66];
	der_var_obj12_up = der_x[66];
	var_tim = x[67];
	der_var_tim = der_x[67];
// temporary assignment i_expr not here
// temporary assignment i_expr1 not here
// procedure assignment div0 not here
// temporary assignment i_expr3 not here
// temporary assignment i_expr4 not here
// procedure assignment div2 not here
// temporary assignment i_expr6 not here
// temporary assignment i_expr7 not here
// procedure assignment div5 not here

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

  }
