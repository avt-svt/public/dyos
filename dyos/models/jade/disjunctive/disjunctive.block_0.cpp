#include "EsoAcsammm_Header.hpp"

void res_block_0(double * yy, 
double * der_x, double * x, 
double * p, int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind, int& i_E_start, int& i_E_end)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_bigM = 0;//  constant var bigM
	double par_p=0;
	double par_ybin1d1=0;
	double par_ybin1d2=0;
	double par_ybin2d1=0;
	double par_ybin2d2=0;
	double par_ybin3d1=0;
	double par_ybin3d2=0;
	double par_x10d1=0;
	double par_x10d2=0;
	double par_x20d1=0;
	double par_x20d2=0;
	double par_x30d1=0;
	double par_x30d2=0;
	double par_xobj11=0;
	double par_xobj12=0;
	double par_xobj21=0;
	double par_xobj22=0;
	double par_xobj31=0;
	double par_xobj32=0;
	double var_x1d1=0;
	double der_var_x1d1=0;
	double var_x1d2=0;
	double der_var_x1d2=0;
	double var_x2d1=0;
	double der_var_x2d1=0;
	double var_x2d2=0;
	double der_var_x2d2=0;
	double var_x3d1=0;
	double der_var_x3d1=0;
	double var_x3d2=0;
	double der_var_x3d2=0;
	double var_xobj1d1=0;
	double der_var_xobj1d1=0;
	double var_xobj1d2=0;
	double der_var_xobj1d2=0;
	double var_xobj2d1=0;
	double der_var_xobj2d1=0;
	double var_xobj2d2=0;
	double der_var_xobj2d2=0;
	double var_xobj3d1=0;
	double der_var_xobj3d1=0;
	double var_xobj3d2=0;
	double der_var_xobj3d2=0;
	double var_x1Strd1=0;
	double der_var_x1Strd1=0;
	double var_x1Strd2=0;
	double der_var_x1Strd2=0;
	double var_x2Strd1=0;
	double der_var_x2Strd1=0;
	double var_x2Strd2=0;
	double der_var_x2Strd2=0;
	double var_x3Strd1=0;
	double der_var_x3Strd1=0;
	double var_x3Strd2=0;
	double der_var_x3Strd2=0;
	double var_xobj=0;
	double der_var_xobj=0;
	double var_constr1_lo=0;
	double der_var_constr1_lo=0;
	double var_constr1_up=0;
	double der_var_constr1_up=0;
	double var_constr2_lo=0;
	double der_var_constr2_lo=0;
	double var_constr2_up=0;
	double der_var_constr2_up=0;
	double var_constr3_lo=0;
	double der_var_constr3_lo=0;
	double var_constr3_up=0;
	double der_var_constr3_up=0;
	double var_constr4_lo=0;
	double der_var_constr4_lo=0;
	double var_constr4_up=0;
	double der_var_constr4_up=0;
	double var_constr5_lo=0;
	double der_var_constr5_lo=0;
	double var_constr5_up=0;
	double der_var_constr5_up=0;
	double var_constr6_lo=0;
	double der_var_constr6_lo=0;
	double var_constr6_up=0;
	double der_var_constr6_up=0;
	double var_constr7_lo=0;
	double der_var_constr7_lo=0;
	double var_constr7_up=0;
	double der_var_constr7_up=0;
	double var_constr8_lo=0;
	double der_var_constr8_lo=0;
	double var_constr8_up=0;
	double der_var_constr8_up=0;
	double var_constr9_lo=0;
	double der_var_constr9_lo=0;
	double var_constr9_up=0;
	double der_var_constr9_up=0;
	double var_constr11_lo=0;
	double der_var_constr11_lo=0;
	double var_constr11_up=0;
	double der_var_constr11_up=0;
	double var_constr13_lo=0;
	double der_var_constr13_lo=0;
	double var_constr13_up=0;
	double der_var_constr13_up=0;
	double var_constr15_lo=0;
	double der_var_constr15_lo=0;
	double var_constr15_up=0;
	double der_var_constr15_up=0;
	double var_obj1_lo=0;
	double der_var_obj1_lo=0;
	double var_obj1_up=0;
	double der_var_obj1_up=0;
	double var_obj2_lo=0;
	double der_var_obj2_lo=0;
	double var_obj2_up=0;
	double der_var_obj2_up=0;
	double var_obj3_lo=0;
	double der_var_obj3_lo=0;
	double var_obj3_up=0;
	double der_var_obj3_up=0;
	double var_obj4_lo=0;
	double der_var_obj4_lo=0;
	double var_obj4_up=0;
	double der_var_obj4_up=0;
	double var_obj5_lo=0;
	double der_var_obj5_lo=0;
	double var_obj5_up=0;
	double der_var_obj5_up=0;
	double var_obj6_lo=0;
	double der_var_obj6_lo=0;
	double var_obj6_up=0;
	double der_var_obj6_up=0;
	double var_obj7_lo=0;
	double der_var_obj7_lo=0;
	double var_obj7_up=0;
	double der_var_obj7_up=0;
	double var_obj8_lo=0;
	double der_var_obj8_lo=0;
	double var_obj8_up=0;
	double der_var_obj8_up=0;
	double var_obj9_lo=0;
	double der_var_obj9_lo=0;
	double var_obj9_up=0;
	double der_var_obj9_up=0;
	double var_obj10_lo=0;
	double der_var_obj10_lo=0;
	double var_obj10_up=0;
	double der_var_obj10_up=0;
	double var_obj11_lo=0;
	double der_var_obj11_lo=0;
	double var_obj11_up=0;
	double der_var_obj11_up=0;
	double var_obj12_lo=0;
	double der_var_obj12_lo=0;
	double var_obj12_up=0;
	double der_var_obj12_up=0;
	double var_tim=0;
	double der_var_tim=0;
	double var_i_expr=0; // assign var
	double var_i_expr1=0; // assign var
	double var_div0=0; // procedure var
	double var_i_expr3=0; // assign var
	double var_i_expr4=0; // assign var
	double var_div2=0; // procedure var
	double var_i_expr6=0; // assign var
	double var_i_expr7=0; // assign var
	double var_div5=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_bigM = 30;
	par_p = p[0];
	par_ybin1d1 = p[1];
	par_ybin1d2 = p[2];
	par_ybin2d1 = p[3];
	par_ybin2d2 = p[4];
	par_ybin3d1 = p[5];
	par_ybin3d2 = p[6];
	par_x10d1 = p[7];
	par_x10d2 = p[8];
	par_x20d1 = p[9];
	par_x20d2 = p[10];
	par_x30d1 = p[11];
	par_x30d2 = p[12];
	par_xobj11 = p[13];
	par_xobj12 = p[14];
	par_xobj21 = p[15];
	par_xobj22 = p[16];
	par_xobj31 = p[17];
	par_xobj32 = p[18];
	var_x1d1 = x[0];
	der_var_x1d1 = der_x[0];
	var_x1d2 = x[1];
	der_var_x1d2 = der_x[1];
	var_x2d1 = x[2];
	der_var_x2d1 = der_x[2];
	var_x2d2 = x[3];
	der_var_x2d2 = der_x[3];
	var_x3d1 = x[4];
	der_var_x3d1 = der_x[4];
	var_x3d2 = x[5];
	der_var_x3d2 = der_x[5];
	var_xobj1d1 = x[6];
	der_var_xobj1d1 = der_x[6];
	var_xobj1d2 = x[7];
	der_var_xobj1d2 = der_x[7];
	var_xobj2d1 = x[8];
	der_var_xobj2d1 = der_x[8];
	var_xobj2d2 = x[9];
	der_var_xobj2d2 = der_x[9];
	var_xobj3d1 = x[10];
	der_var_xobj3d1 = der_x[10];
	var_xobj3d2 = x[11];
	der_var_xobj3d2 = der_x[11];
	var_x1Strd1 = x[12];
	der_var_x1Strd1 = der_x[12];
	var_x1Strd2 = x[13];
	der_var_x1Strd2 = der_x[13];
	var_x2Strd1 = x[14];
	der_var_x2Strd1 = der_x[14];
	var_x2Strd2 = x[15];
	der_var_x2Strd2 = der_x[15];
	var_x3Strd1 = x[16];
	der_var_x3Strd1 = der_x[16];
	var_x3Strd2 = x[17];
	der_var_x3Strd2 = der_x[17];
	var_xobj = x[18];
	der_var_xobj = der_x[18];
	var_constr1_lo = x[19];
	der_var_constr1_lo = der_x[19];
	var_constr1_up = x[20];
	der_var_constr1_up = der_x[20];
	var_constr2_lo = x[21];
	der_var_constr2_lo = der_x[21];
	var_constr2_up = x[22];
	der_var_constr2_up = der_x[22];
	var_constr3_lo = x[23];
	der_var_constr3_lo = der_x[23];
	var_constr3_up = x[24];
	der_var_constr3_up = der_x[24];
	var_constr4_lo = x[25];
	der_var_constr4_lo = der_x[25];
	var_constr4_up = x[26];
	der_var_constr4_up = der_x[26];
	var_constr5_lo = x[27];
	der_var_constr5_lo = der_x[27];
	var_constr5_up = x[28];
	der_var_constr5_up = der_x[28];
	var_constr6_lo = x[29];
	der_var_constr6_lo = der_x[29];
	var_constr6_up = x[30];
	der_var_constr6_up = der_x[30];
	var_constr7_lo = x[31];
	der_var_constr7_lo = der_x[31];
	var_constr7_up = x[32];
	der_var_constr7_up = der_x[32];
	var_constr8_lo = x[33];
	der_var_constr8_lo = der_x[33];
	var_constr8_up = x[34];
	der_var_constr8_up = der_x[34];
	var_constr9_lo = x[35];
	der_var_constr9_lo = der_x[35];
	var_constr9_up = x[36];
	der_var_constr9_up = der_x[36];
	var_constr11_lo = x[37];
	der_var_constr11_lo = der_x[37];
	var_constr11_up = x[38];
	der_var_constr11_up = der_x[38];
	var_constr13_lo = x[39];
	der_var_constr13_lo = der_x[39];
	var_constr13_up = x[40];
	der_var_constr13_up = der_x[40];
	var_constr15_lo = x[41];
	der_var_constr15_lo = der_x[41];
	var_constr15_up = x[42];
	der_var_constr15_up = der_x[42];
	var_obj1_lo = x[43];
	der_var_obj1_lo = der_x[43];
	var_obj1_up = x[44];
	der_var_obj1_up = der_x[44];
	var_obj2_lo = x[45];
	der_var_obj2_lo = der_x[45];
	var_obj2_up = x[46];
	der_var_obj2_up = der_x[46];
	var_obj3_lo = x[47];
	der_var_obj3_lo = der_x[47];
	var_obj3_up = x[48];
	der_var_obj3_up = der_x[48];
	var_obj4_lo = x[49];
	der_var_obj4_lo = der_x[49];
	var_obj4_up = x[50];
	der_var_obj4_up = der_x[50];
	var_obj5_lo = x[51];
	der_var_obj5_lo = der_x[51];
	var_obj5_up = x[52];
	der_var_obj5_up = der_x[52];
	var_obj6_lo = x[53];
	der_var_obj6_lo = der_x[53];
	var_obj6_up = x[54];
	der_var_obj6_up = der_x[54];
	var_obj7_lo = x[55];
	der_var_obj7_lo = der_x[55];
	var_obj7_up = x[56];
	der_var_obj7_up = der_x[56];
	var_obj8_lo = x[57];
	der_var_obj8_lo = der_x[57];
	var_obj8_up = x[58];
	der_var_obj8_up = der_x[58];
	var_obj9_lo = x[59];
	der_var_obj9_lo = der_x[59];
	var_obj9_up = x[60];
	der_var_obj9_up = der_x[60];
	var_obj10_lo = x[61];
	der_var_obj10_lo = der_x[61];
	var_obj10_up = x[62];
	der_var_obj10_up = der_x[62];
	var_obj11_lo = x[63];
	der_var_obj11_lo = der_x[63];
	var_obj11_up = x[64];
	der_var_obj11_up = der_x[64];
	var_obj12_lo = x[65];
	der_var_obj12_lo = der_x[65];
	var_obj12_up = x[66];
	der_var_obj12_up = der_x[66];
	var_tim = x[67];
	der_var_tim = der_x[67];
// temporary assignment i_expr not here
// temporary assignment i_expr1 not here
// procedure assignment div0 not here
// temporary assignment i_expr3 not here
// temporary assignment i_expr4 not here
// procedure assignment div2 not here
// temporary assignment i_expr6 not here
// temporary assignment i_expr7 not here
// procedure assignment div5 not here

i_E = i_E_start;
while (i_E < i_E_end) {
  i__switch = yy_ind[i_E];
  switch (i__switch) {

  case 0: // scalar equation 0
  yy[i_E] = der_var_x1Strd1 - (-2 * var_tim * var_x1d1+par_p);
  i_E = i_E+1;  break;
  case 1: // scalar equation 1
  yy[i_E] = var_x1d1 - (par_x10d1+var_x1Strd1);
  i_E = i_E+1;  break;
  case 2: // scalar equation 2
  yy[i_E] = der_var_xobj1d1 - (var_x1d1 * var_x1d1);
  i_E = i_E+1;  break;
  case 3: // scalar equation 3
  yy[i_E] = var_obj1_lo - (par_xobj11-var_xobj1d1-var_bigM *  ( 1-par_ybin1d1 ) );
  i_E = i_E+1;  break;
  case 4: // scalar equation 4
  yy[i_E] = var_obj1_up - (par_xobj11-var_xobj1d1+var_bigM *  ( 1-par_ybin1d1 ) );
  i_E = i_E+1;  break;
  case 5: // scalar equation 5
  yy[i_E] = var_constr1_lo - (par_x20d1-var_x1d1-var_bigM *  ( 1-par_ybin1d1 ) );
  i_E = i_E+1;  break;
  case 6: // scalar equation 6
  yy[i_E] = var_constr1_up - (par_x20d1-var_x1d1+var_bigM *  ( 1-par_ybin1d1 ) );
  i_E = i_E+1;  break;
  case 7: // scalar equation 7
  yy[i_E] = var_constr2_lo - (par_x10d1-1-var_bigM *  ( 1-par_ybin1d1 ) );
  i_E = i_E+1;  break;
  case 8: // scalar equation 8
  yy[i_E] = var_constr2_up - (par_x10d1-1+var_bigM *  ( 1-par_ybin1d1 ) );
  i_E = i_E+1;  break;
  case 9: // scalar equation 9
  yy[i_E] = var_constr3_lo - (par_x20d1-var_bigM * par_ybin1d1);
  i_E = i_E+1;  break;
  case 10: // scalar equation 10
  yy[i_E] = var_constr3_up - (par_x20d1+var_bigM * par_ybin1d1);
  i_E = i_E+1;  break;
  case 11: // scalar equation 11
  yy[i_E] = var_constr4_lo - (par_x10d1-var_bigM * par_ybin1d1);
  i_E = i_E+1;  break;
  case 12: // scalar equation 12
  yy[i_E] = var_constr4_up - (par_x10d1+var_bigM * par_ybin1d1);
  i_E = i_E+1;  break;
  case 13: // scalar equation 13
  yy[i_E] = var_obj2_lo - (par_xobj11-var_bigM * par_ybin1d1);
  i_E = i_E+1;  break;
  case 14: // scalar equation 14
  yy[i_E] = var_obj2_up - (par_xobj11+var_bigM * par_ybin1d1);
  i_E = i_E+1;  break;
  case 15: // scalar equation 15
var_i_expr= ( var_x1d2+par_p ) ;
var_i_expr1= ( var_tim+10 ) ;
acs_div(var_i_expr, var_i_expr1, var_div0);
  yy[i_E] = der_var_x1Strd2 - (var_div0);
  i_E = i_E+1;  break;
  case 16: // scalar equation 16
  yy[i_E] = var_x1d2 - (par_x10d2+var_x1Strd2);
  i_E = i_E+1;  break;
  case 17: // scalar equation 17
  yy[i_E] = der_var_xobj1d2 - (var_x1d2 * var_x1d2);
  i_E = i_E+1;  break;
  case 18: // scalar equation 18
  yy[i_E] = var_obj3_lo - (par_xobj12-var_xobj1d2-var_bigM *  ( 1-par_ybin1d2 ) );
  i_E = i_E+1;  break;
  case 19: // scalar equation 19
  yy[i_E] = var_obj3_up - (par_xobj12-var_xobj1d2+var_bigM *  ( 1-par_ybin1d2 ) );
  i_E = i_E+1;  break;
  case 20: // scalar equation 20
  yy[i_E] = var_constr5_lo - (par_x20d2-var_x1d2-var_bigM *  ( 1-par_ybin1d2 ) );
  i_E = i_E+1;  break;
  case 21: // scalar equation 21
  yy[i_E] = var_constr5_up - (par_x20d2-var_x1d2+var_bigM *  ( 1-par_ybin1d2 ) );
  i_E = i_E+1;  break;
  case 22: // scalar equation 22
  yy[i_E] = var_constr6_lo - (par_x10d2-1-var_bigM *  ( 1-par_ybin1d2 ) );
  i_E = i_E+1;  break;
  case 23: // scalar equation 23
  yy[i_E] = var_constr6_up - (par_x10d2-1+var_bigM *  ( 1-par_ybin1d2 ) );
  i_E = i_E+1;  break;
  case 24: // scalar equation 24
  yy[i_E] = var_constr7_lo - (par_x20d2-var_bigM * par_ybin1d2);
  i_E = i_E+1;  break;
  case 25: // scalar equation 25
  yy[i_E] = var_constr7_up - (par_x20d2+var_bigM * par_ybin1d2);
  i_E = i_E+1;  break;
  case 26: // scalar equation 26
  yy[i_E] = var_constr8_lo - (par_x10d2-var_bigM * par_ybin1d2);
  i_E = i_E+1;  break;
  case 27: // scalar equation 27
  yy[i_E] = var_constr8_up - (par_x10d2+var_bigM * par_ybin1d2);
  i_E = i_E+1;  break;
  case 28: // scalar equation 28
  yy[i_E] = var_obj4_lo - (par_xobj12-var_bigM * par_ybin1d2);
  i_E = i_E+1;  break;
  case 29: // scalar equation 29
  yy[i_E] = var_obj4_up - (par_xobj12+var_bigM * par_ybin1d2);
  i_E = i_E+1;  break;
  case 30: // scalar equation 30
  yy[i_E] = der_var_x2Strd1 - (-2 *  ( var_tim+1 )  * var_x2d1+par_p);
  i_E = i_E+1;  break;
  case 31: // scalar equation 31
  yy[i_E] = var_x2d1 - (par_x20d1+par_x20d2+var_x2Strd1);
  i_E = i_E+1;  break;
  case 32: // scalar equation 32
  yy[i_E] = der_var_xobj2d1 - (var_x2d1 * var_x2d1);
  i_E = i_E+1;  break;
  case 33: // scalar equation 33
  yy[i_E] = var_obj5_lo - (par_xobj21-var_xobj2d1-var_bigM *  ( 1-par_ybin2d1 ) );
  i_E = i_E+1;  break;
  case 34: // scalar equation 34
  yy[i_E] = var_obj5_up - (par_xobj21-var_xobj2d1+var_bigM *  ( 1-par_ybin2d1 ) );
  i_E = i_E+1;  break;
  case 35: // scalar equation 35
  yy[i_E] = var_constr9_lo - (par_x30d1-var_x2d1-var_bigM *  ( 1-par_ybin2d1 ) );
  i_E = i_E+1;  break;
  case 36: // scalar equation 36
  yy[i_E] = var_constr9_up - (par_x30d1-var_x2d1+var_bigM *  ( 1-par_ybin2d1 ) );
  i_E = i_E+1;  break;
  case 37: // scalar equation 37
  yy[i_E] = var_constr11_lo - (par_x30d1-var_bigM * par_ybin2d1);
  i_E = i_E+1;  break;
  case 38: // scalar equation 38
  yy[i_E] = var_constr11_up - (par_x30d1+var_bigM * par_ybin2d1);
  i_E = i_E+1;  break;
  case 39: // scalar equation 39
  yy[i_E] = var_obj6_lo - (par_xobj21-var_bigM * par_ybin2d1);
  i_E = i_E+1;  break;
  case 40: // scalar equation 40
  yy[i_E] = var_obj6_up - (par_xobj21+var_bigM * par_ybin2d1);
  i_E = i_E+1;  break;
  case 41: // scalar equation 41
var_i_expr3= ( var_x2d2+par_p ) ;
var_i_expr4= ( var_tim+1+10 ) ;
acs_div(var_i_expr3, var_i_expr4, var_div2);
  yy[i_E] = der_var_x2Strd2 - (var_div2);
  i_E = i_E+1;  break;
  case 42: // scalar equation 42
  yy[i_E] = var_x2d2 - (par_x20d1+par_x20d2+var_x2Strd2);
  i_E = i_E+1;  break;
  case 43: // scalar equation 43
  yy[i_E] = der_var_xobj2d2 - (var_x2d2 * var_x2d2);
  i_E = i_E+1;  break;
  case 44: // scalar equation 44
  yy[i_E] = var_obj7_lo - (par_xobj22-var_xobj2d2-var_bigM *  ( 1-par_ybin2d2 ) );
  i_E = i_E+1;  break;
  case 45: // scalar equation 45
  yy[i_E] = var_obj7_up - (par_xobj22-var_xobj2d2+var_bigM *  ( 1-par_ybin2d2 ) );
  i_E = i_E+1;  break;
  case 46: // scalar equation 46
  yy[i_E] = var_constr13_lo - (par_x30d2-var_x2d2-var_bigM *  ( 1-par_ybin2d2 ) );
  i_E = i_E+1;  break;
  case 47: // scalar equation 47
  yy[i_E] = var_constr13_up - (par_x30d2-var_x2d2-var_bigM *  ( 1-par_ybin2d2 ) );
  i_E = i_E+1;  break;
  case 48: // scalar equation 48
  yy[i_E] = var_constr15_lo - (par_x30d2-var_bigM * par_ybin2d2);
  i_E = i_E+1;  break;
  case 49: // scalar equation 49
  yy[i_E] = var_constr15_up - (par_x30d2+var_bigM * par_ybin2d2);
  i_E = i_E+1;  break;
  case 50: // scalar equation 50
  yy[i_E] = var_obj8_lo - (par_xobj22-var_bigM * par_ybin2d2);
  i_E = i_E+1;  break;
  case 51: // scalar equation 51
  yy[i_E] = var_obj8_up - (par_xobj22+var_bigM * par_ybin2d2);
  i_E = i_E+1;  break;
  case 52: // scalar equation 52
  yy[i_E] = der_var_x3Strd1 - (-2 *  ( var_tim+2 )  * var_x3d1+par_p);
  i_E = i_E+1;  break;
  case 53: // scalar equation 53
  yy[i_E] = var_x3d1 - (par_x30d1+par_x30d2+var_x3Strd1);
  i_E = i_E+1;  break;
  case 54: // scalar equation 54
  yy[i_E] = der_var_xobj3d1 - (var_x3d1 * var_x3d1);
  i_E = i_E+1;  break;
  case 55: // scalar equation 55
  yy[i_E] = var_obj9_lo - (par_xobj31-var_xobj3d1-var_bigM *  ( 1-par_ybin3d1 ) );
  i_E = i_E+1;  break;
  case 56: // scalar equation 56
  yy[i_E] = var_obj9_up - (par_xobj31-var_xobj3d1+var_bigM *  ( 1-par_ybin3d1 ) );
  i_E = i_E+1;  break;
  case 57: // scalar equation 57
  yy[i_E] = var_obj10_lo - (par_xobj31-var_bigM * par_ybin3d1);
  i_E = i_E+1;  break;
  case 58: // scalar equation 58
  yy[i_E] = var_obj10_up - (par_xobj31+var_bigM * par_ybin3d1);
  i_E = i_E+1;  break;
  case 59: // scalar equation 59
var_i_expr6= ( var_x3d2+par_p ) ;
var_i_expr7= ( var_tim+2+10 ) ;
acs_div(var_i_expr6, var_i_expr7, var_div5);
  yy[i_E] = der_var_x3Strd2 - (var_div5);
  i_E = i_E+1;  break;
  case 60: // scalar equation 60
  yy[i_E] = var_x3d2 - (par_x30d1+par_x30d2+var_x3Strd2);
  i_E = i_E+1;  break;
  case 61: // scalar equation 61
  yy[i_E] = der_var_xobj3d2 - (var_x3d2 * var_x3d2);
  i_E = i_E+1;  break;
  case 62: // scalar equation 62
  yy[i_E] = var_obj11_lo - (par_xobj32-var_xobj3d2-var_bigM *  ( 1-par_ybin3d2 ) );
  i_E = i_E+1;  break;
  case 63: // scalar equation 63
  yy[i_E] = var_obj11_up - (par_xobj32-var_xobj3d2+var_bigM *  ( 1-par_ybin3d2 ) );
  i_E = i_E+1;  break;
  case 64: // scalar equation 64
  yy[i_E] = var_obj12_lo - (par_xobj32-var_bigM * par_ybin3d2);
  i_E = i_E+1;  break;
  case 65: // scalar equation 65
  yy[i_E] = var_obj12_up - (par_xobj32-var_bigM * par_ybin3d2);
  i_E = i_E+1;  break;
  case 66: // scalar equation 66
  yy[i_E] = var_xobj - (par_xobj11+par_xobj12+par_xobj21+par_xobj22+par_xobj31+par_xobj32);
  i_E = i_E+1;  break;
  case 67: // scalar equation 67
  yy[i_E] = der_var_tim - (1);
  i_E = i_E+1;  break;
// //default: i_E=i_E+1; break; 
  } // switch
} // end while
} // end of block_* function
void res_block(double * yy, 
double * der_x, double * x, double * p,
int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind)
  #pragma ad indep x p
  #pragma ad dep yy
{
 int i_E = 0;
 int i_E_end = 68;
 int i__start = 0;
 int i__end = 0;
 int i__switch = 0;
while (i_E < n_yy_ind) {
  i__switch = yy_ind[i_E];
i__start = 0;
i__end = 68;
if ((i__switch >= i__start) && (i__switch < i__end)) {
  i__end = i_E+1;
  res_block_0(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c, n_yy_ind, yy_ind, i_E, i__end);
}
i_E = i_E+1; } // while
}  // res_block
