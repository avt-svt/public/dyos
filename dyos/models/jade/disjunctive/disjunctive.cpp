#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 4; // p
	p[1] = 1; // ybin1d1
	p[2] = 0; // ybin1d2
	p[3] = 0; // ybin2d1
	p[4] = 1; // ybin2d2
	p[5] = 0; // ybin3d1
	p[6] = 1; // ybin3d2
	p[7] = 1; // x10d1
	p[8] = 1; // x10d2
	p[9] = 1; // x20d1
	p[10] = 1; // x20d2
	p[11] = 1; // x30d1
	p[12] = 1; // x30d2
	p[13] = 0; // xobj11
	p[14] = 0; // xobj12
	p[15] = 0; // xobj21
	p[16] = 0; // xobj22
	p[17] = 0; // xobj31
	p[18] = 0; // xobj32
	x[0] = 0; // x1d1
	x[1] = 0; // x1d2
	x[2] = 0; // x2d1
	x[3] = 0; // x2d2
	x[4] = 0; // x3d1
	x[5] = 0; // x3d2
	x[6] = 0; // xobj1d1
	x[7] = 0; // xobj1d2
	x[8] = 0; // xobj2d1
	x[9] = 0; // xobj2d2
	x[10] = 0; // xobj3d1
	x[11] = 0; // xobj3d2
	x[12] = 0; // x1Strd1
	x[13] = 0; // x1Strd2
	x[14] = 0; // x2Strd1
	x[15] = 0; // x2Strd2
	x[16] = 0; // x3Strd1
	x[17] = 0; // x3Strd2
	x[18] = 0; // xobj
	x[19] = 0; // constr1_lo
	x[20] = 0; // constr1_up
	x[21] = 0; // constr2_lo
	x[22] = 0; // constr2_up
	x[23] = 0; // constr3_lo
	x[24] = 0; // constr3_up
	x[25] = 0; // constr4_lo
	x[26] = 0; // constr4_up
	x[27] = 0; // constr5_lo
	x[28] = 0; // constr5_up
	x[29] = 0; // constr6_lo
	x[30] = 0; // constr6_up
	x[31] = 0; // constr7_lo
	x[32] = 0; // constr7_up
	x[33] = 0; // constr8_lo
	x[34] = 0; // constr8_up
	x[35] = 0; // constr9_lo
	x[36] = 0; // constr9_up
	x[37] = 0; // constr11_lo
	x[38] = 0; // constr11_up
	x[39] = 0; // constr13_lo
	x[40] = 0; // constr13_up
	x[41] = 0; // constr15_lo
	x[42] = 0; // constr15_up
	x[43] = 0; // obj1_lo
	x[44] = 0; // obj1_up
	x[45] = 0; // obj2_lo
	x[46] = 0; // obj2_up
	x[47] = 0; // obj3_lo
	x[48] = 0; // obj3_up
	x[49] = 0; // obj4_lo
	x[50] = 0; // obj4_up
	x[51] = 0; // obj5_lo
	x[52] = 0; // obj5_up
	x[53] = 0; // obj6_lo
	x[54] = 0; // obj6_up
	x[55] = 0; // obj7_lo
	x[56] = 0; // obj7_up
	x[57] = 0; // obj8_lo
	x[58] = 0; // obj8_up
	x[59] = 0; // obj9_lo
	x[60] = 0; // obj9_up
	x[61] = 0; // obj10_lo
	x[62] = 0; // obj10_up
	x[63] = 0; // obj11_lo
	x[64] = 0; // obj11_up
	x[65] = 0; // obj12_lo
	x[66] = 0; // obj12_up
	x[67] = 0; // tim
	n_x = 68; n_p = 19; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("x1d1");
	names->push_back("x1d2");
	names->push_back("x2d1");
	names->push_back("x2d2");
	names->push_back("x3d1");
	names->push_back("x3d2");
	names->push_back("xobj1d1");
	names->push_back("xobj1d2");
	names->push_back("xobj2d1");
	names->push_back("xobj2d2");
	names->push_back("xobj3d1");
	names->push_back("xobj3d2");
	names->push_back("x1Strd1");
	names->push_back("x1Strd2");
	names->push_back("x2Strd1");
	names->push_back("x2Strd2");
	names->push_back("x3Strd1");
	names->push_back("x3Strd2");
	names->push_back("xobj");
	names->push_back("constr1_lo");
	names->push_back("constr1_up");
	names->push_back("constr2_lo");
	names->push_back("constr2_up");
	names->push_back("constr3_lo");
	names->push_back("constr3_up");
	names->push_back("constr4_lo");
	names->push_back("constr4_up");
	names->push_back("constr5_lo");
	names->push_back("constr5_up");
	names->push_back("constr6_lo");
	names->push_back("constr6_up");
	names->push_back("constr7_lo");
	names->push_back("constr7_up");
	names->push_back("constr8_lo");
	names->push_back("constr8_up");
	names->push_back("constr9_lo");
	names->push_back("constr9_up");
	names->push_back("constr11_lo");
	names->push_back("constr11_up");
	names->push_back("constr13_lo");
	names->push_back("constr13_up");
	names->push_back("constr15_lo");
	names->push_back("constr15_up");
	names->push_back("obj1_lo");
	names->push_back("obj1_up");
	names->push_back("obj2_lo");
	names->push_back("obj2_up");
	names->push_back("obj3_lo");
	names->push_back("obj3_up");
	names->push_back("obj4_lo");
	names->push_back("obj4_up");
	names->push_back("obj5_lo");
	names->push_back("obj5_up");
	names->push_back("obj6_lo");
	names->push_back("obj6_up");
	names->push_back("obj7_lo");
	names->push_back("obj7_up");
	names->push_back("obj8_lo");
	names->push_back("obj8_up");
	names->push_back("obj9_lo");
	names->push_back("obj9_up");
	names->push_back("obj10_lo");
	names->push_back("obj10_up");
	names->push_back("obj11_lo");
	names->push_back("obj11_up");
	names->push_back("obj12_lo");
	names->push_back("obj12_up");
	names->push_back("tim");
}
void get_num_vars(int & nv) {
  nv = 68;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("p");
	names->push_back("ybin1d1");
	names->push_back("ybin1d2");
	names->push_back("ybin2d1");
	names->push_back("ybin2d2");
	names->push_back("ybin3d1");
	names->push_back("ybin3d2");
	names->push_back("x10d1");
	names->push_back("x10d2");
	names->push_back("x20d1");
	names->push_back("x20d2");
	names->push_back("x30d1");
	names->push_back("x30d2");
	names->push_back("xobj11");
	names->push_back("xobj12");
	names->push_back("xobj21");
	names->push_back("xobj22");
	names->push_back("xobj31");
	names->push_back("xobj32");
}
void get_num_pars(int & np) {
  np = 19;
}


void get_num_eqns(int& ne){
 ne = 68;
}

void get_num_cond(int& nc){
 nc = 0;
}
