#include "EsoAcsammm_Header.hpp"

void res(double * yy, 
double* der_x, double * x, 
double* p,
int* condit, int* lock, int* prev,
int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_mu_m = 0;//  constant var mu_m
	double var_K_m = 0;//  constant var K_m
	double var_K_I = 0;//  constant var K_I
	double var_Y_x = 0;//  constant var Y_x
	double var_Y_p = 0;//  constant var Y_p
	double var_v = 0;//  constant var v
	double var_S_in = 0;//  constant var S_in
	double par_u=0;
	double var_X=0;
	double der_var_X=0;
	double var_S=0;
	double der_var_S=0;
	double var_P=0;
	double der_var_P=0;
	double var_V=0;
	double der_var_V=0;
	double var_muS=0;
	double der_var_muS=0;
	double var_div0=0; // procedure var
	double var_div1=0; // procedure var
	double var_div2=0; // procedure var
	double var_div3=0; // procedure var
	double var_div4=0; // procedure var
	double var_div5=0; // procedure var
	double var_i_expr=0; // assign var
	double var_div6=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_mu_m = 0.02;
	var_K_m = 0.05;
	var_K_I = 5;
	var_Y_x = 0.5;
	var_Y_p = 1.2;
	var_v = 0.004;
	var_S_in = 200;
	par_u = p[0];
	var_X = x[0];
	der_var_X = der_x[0];
	var_S = x[1];
	der_var_S = der_x[1];
	var_P = x[2];
	der_var_P = der_x[2];
	var_V = x[3];
	der_var_V = der_x[3];
	var_muS = x[4];
	der_var_muS = der_x[4];
// procedure assignment div0 not here
// procedure assignment div1 not here
// procedure assignment div2 not here
// procedure assignment div3 not here
// procedure assignment div4 not here
// procedure assignment div5 not here
// temporary assignment i_expr not here
// procedure assignment div6 not here

// #pragma dcc equation begin
// scalar equation 0
acs_div(par_u, var_V, var_div0);
yy[i_E] = der_var_X - (var_muS * var_X-var_div0 * var_X);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 1
acs_div(var_X, var_Y_x, var_div1);
acs_div(var_X, var_Y_p, var_div2);
acs_div(par_u, var_V, var_div3);
yy[i_E] = der_var_S - (var_muS * var_div1-var_v * var_div2+var_div3 *  ( var_S_in-var_S ) );
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 2
acs_div(par_u, var_V, var_div4);
yy[i_E] = der_var_P - (var_v * var_X-var_div4 * var_P);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 3
yy[i_E] = der_var_V - (par_u);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 4
acs_div(var_S, var_K_I, var_div5);
var_i_expr= ( var_K_m+var_S+ ( var_S * var_div5 )  ) ;
acs_div(var_S, var_i_expr, var_div6);
yy[i_E] = var_muS - (var_mu_m * var_div6);
i_E = i_E+1;
// #pragma dcc equation end

n_c=i_C;
}
