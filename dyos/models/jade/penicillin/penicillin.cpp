#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 0; // u
	x[0] = 1; // X
	x[1] = 0.5; // S
	x[2] = 0; // P
	x[3] = 150; // V
	x[4] = 0; // muS
	n_x = 5; n_p = 1; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("X");
	names->push_back("S");
	names->push_back("P");
	names->push_back("V");
	names->push_back("muS");
}
void get_num_vars(int & nv) {
  nv = 5;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("u");
}
void get_num_pars(int & np) {
  np = 1;
}


void get_num_eqns(int& ne){
 ne = 5;
}

void get_num_cond(int& nc){
 nc = 0;
}
