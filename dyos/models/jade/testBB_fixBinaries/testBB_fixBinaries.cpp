#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 0; // u
	p[1] = 1; // bin1
	p[2] = 1; // bin2
	x[0] = 0; // x
	x[1] = 0; // obj
	n_x = 2; n_p = 3; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("x");
	names->push_back("obj");
}
void get_num_vars(int & nv) {
  nv = 2;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("u");
	names->push_back("bin1");
	names->push_back("bin2");
}
void get_num_pars(int & np) {
  np = 3;
}


void get_num_eqns(int& ne){
 ne = 2;
}

void get_num_cond(int& nc){
 nc = 0;
}
