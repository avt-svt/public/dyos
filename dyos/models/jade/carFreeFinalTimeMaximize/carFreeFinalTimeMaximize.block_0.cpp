#include "EsoAcsammm_Header.hpp"

void res_block_0(double * yy, 
double * der_x, double * x, 
double * p, int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind, int& i_E_start, int& i_E_end)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_pi = 0;//  constant var pi
	double par_accel=0;
	double par_alpha=0;
	double par_tf=0;
	double var_ttime=0;
	double der_var_ttime=0;
	double var_velo=0;
	double der_var_velo=0;
	double var_dist=0;
	double der_var_dist=0;
	double var_obj=0;
	double der_var_obj=0;
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_pi = 3.14159;
	par_accel = p[0];
	par_alpha = p[1];
	par_tf = p[2];
	var_ttime = x[0];
	der_var_ttime = der_x[0];
	var_velo = x[1];
	der_var_velo = der_x[1];
	var_dist = x[2];
	der_var_dist = der_x[2];
	var_obj = x[3];
	der_var_obj = der_x[3];

i_E = i_E_start;
while (i_E < i_E_end) {
  i__switch = yy_ind[i_E];
  switch (i__switch) {

  case 0: // scalar equation 0
  yy[i_E] = der_var_dist - (par_tf * var_velo);
  i_E = i_E+1;  break;
  case 1: // scalar equation 1
  yy[i_E] = der_var_ttime - (par_tf * 1);
  i_E = i_E+1;  break;
  case 2: // scalar equation 2
  yy[i_E] = der_var_velo - (par_tf *  ( par_accel-par_alpha * var_velo * var_velo ) );
  i_E = i_E+1;  break;
  case 3: // scalar equation 3
  yy[i_E] = var_obj - (-var_ttime);
  i_E = i_E+1;  break;
// //default: i_E=i_E+1; break; 
  } // switch
} // end while
} // end of block_* function
void res_block(double * yy, 
double * der_x, double * x, double * p,
int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind)
  #pragma ad indep x p
  #pragma ad dep yy
{
 int i_E = 0;
 int i_E_end = 4;
 int i__start = 0;
 int i__end = 0;
 int i__switch = 0;
while (i_E < n_yy_ind) {
  i__switch = yy_ind[i_E];
i__start = 0;
i__end = 4;
if ((i__switch >= i__start) && (i__switch < i__end)) {
  i__end = i_E+1;
  res_block_0(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c, n_yy_ind, yy_ind, i_E, i__end);
}
i_E = i_E+1; } // while
}  // res_block
