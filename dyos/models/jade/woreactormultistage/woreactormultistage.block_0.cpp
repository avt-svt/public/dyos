#include "EsoAcsammm_Header.hpp"

void res_block_0(double * yy, 
double * der_x, double * x, 
double * p, int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind, int& i_E_start, int& i_E_end)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double par_tw=0;
	double par_fbin=0;
	double par_deltatime=0;
	double par_tain=0;
	double par_tbin=0;
	double par_b1=0;
	double par_b2=0;
	double par_b3=0;
	double par_b4=0;
	double par_b5=0;
	double par_b6=0;
	double par_yconti=0;
	double par_u=0;
	double par_kout=0;
	double par_mwa=0;
	double par_mwb=0;
	double par_mwp=0;
	double par_mwc=0;
	double par_mwe=0;
	double par_mwg=0;
	double par_cp=0;
	double par_rho=0;
	double par_dh1=0;
	double par_dh2=0;
	double par_dh3=0;
	double par_a0=0;
	double par_v0=0;
	double par_fain=0;
	double var_k1=0;
	double der_var_k1=0;
	double var_k2=0;
	double der_var_k2=0;
	double var_k3=0;
	double der_var_k3=0;
	double var_r1=0;
	double der_var_r1=0;
	double var_r2=0;
	double der_var_r2=0;
	double var_r3=0;
	double der_var_r3=0;
	double var_wa=0;
	double der_var_wa=0;
	double var_wb=0;
	double der_var_wb=0;
	double var_wc=0;
	double der_var_wc=0;
	double var_wp=0;
	double der_var_wp=0;
	double var_wg=0;
	double der_var_wg=0;
	double var_we=0;
	double der_var_we=0;
	double var_mg=0;
	double der_var_mg=0;
	double var_fin=0;
	double der_var_fin=0;
	double var_fout=0;
	double der_var_fout=0;
	double var_v=0;
	double der_var_v=0;
	double var_dtdt=0;
	double der_var_dtdt=0;
	double var_dma=0;
	double der_var_dma=0;
	double var_dmb=0;
	double der_var_dmb=0;
	double var_dmc=0;
	double der_var_dmc=0;
	double var_dme=0;
	double der_var_dme=0;
	double var_dmp=0;
	double der_var_dmp=0;
	double var_h_masschange=0;
	double der_var_h_masschange=0;
	double var_h_inout=0;
	double der_var_h_inout=0;
	double var_h_transfer=0;
	double der_var_h_transfer=0;
	double var_h_react=0;
	double der_var_h_react=0;
	double var_m=0;
	double der_var_m=0;
	double var_ma=0;
	double der_var_ma=0;
	double var_mb=0;
	double der_var_mb=0;
	double var_mc=0;
	double der_var_mc=0;
	double var_mp=0;
	double der_var_mp=0;
	double var_me=0;
	double der_var_me=0;
	double var_tr=0;
	double der_var_tr=0;
	double var_obj=0;
	double der_var_obj=0;
	double var_ttime=0;
	double der_var_ttime=0;
	double var_i_expr=0; // assign var
	double var_div0=0; // procedure var
	double var_exp1=0; // procedure var
	double var_i_expr2=0; // assign var
	double var_i_expr4=0; // assign var
	double var_div3=0; // procedure var
	double var_exp5=0; // procedure var
	double var_i_expr6=0; // assign var
	double var_i_expr8=0; // assign var
	double var_div7=0; // procedure var
	double var_exp9=0; // procedure var
	double var_i_expr10=0; // assign var
	double var_div11=0; // procedure var
	double var_div12=0; // procedure var
	double var_div13=0; // procedure var
	double var_div14=0; // procedure var
	double var_div15=0; // procedure var
	double var_div16=0; // procedure var
	double var_div17=0; // procedure var
	double var_div18=0; // procedure var
	double var_div19=0; // procedure var
	double var_div20=0; // procedure var
	double var_div21=0; // procedure var
	double var_div22=0; // procedure var
	double var_i_expr24=0; // assign var
	double var_i_expr25=0; // assign var
	double var_div23=0; // procedure var
	double var_i_expr27=0; // assign var
	double var_div26=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	par_tw = p[0];
	par_fbin = p[1];
	par_deltatime = p[2];
	par_tain = p[3];
	par_tbin = p[4];
	par_b1 = p[5];
	par_b2 = p[6];
	par_b3 = p[7];
	par_b4 = p[8];
	par_b5 = p[9];
	par_b6 = p[10];
	par_yconti = p[11];
	par_u = p[12];
	par_kout = p[13];
	par_mwa = p[14];
	par_mwb = p[15];
	par_mwp = p[16];
	par_mwc = p[17];
	par_mwe = p[18];
	par_mwg = p[19];
	par_cp = p[20];
	par_rho = p[21];
	par_dh1 = p[22];
	par_dh2 = p[23];
	par_dh3 = p[24];
	par_a0 = p[25];
	par_v0 = p[26];
	par_fain = p[27];
	var_k1 = x[0];
	der_var_k1 = der_x[0];
	var_k2 = x[1];
	der_var_k2 = der_x[1];
	var_k3 = x[2];
	der_var_k3 = der_x[2];
	var_r1 = x[3];
	der_var_r1 = der_x[3];
	var_r2 = x[4];
	der_var_r2 = der_x[4];
	var_r3 = x[5];
	der_var_r3 = der_x[5];
	var_wa = x[6];
	der_var_wa = der_x[6];
	var_wb = x[7];
	der_var_wb = der_x[7];
	var_wc = x[8];
	der_var_wc = der_x[8];
	var_wp = x[9];
	der_var_wp = der_x[9];
	var_wg = x[10];
	der_var_wg = der_x[10];
	var_we = x[11];
	der_var_we = der_x[11];
	var_mg = x[12];
	der_var_mg = der_x[12];
	var_fin = x[13];
	der_var_fin = der_x[13];
	var_fout = x[14];
	der_var_fout = der_x[14];
	var_v = x[15];
	der_var_v = der_x[15];
	var_dtdt = x[16];
	der_var_dtdt = der_x[16];
	var_dma = x[17];
	der_var_dma = der_x[17];
	var_dmb = x[18];
	der_var_dmb = der_x[18];
	var_dmc = x[19];
	der_var_dmc = der_x[19];
	var_dme = x[20];
	der_var_dme = der_x[20];
	var_dmp = x[21];
	der_var_dmp = der_x[21];
	var_h_masschange = x[22];
	der_var_h_masschange = der_x[22];
	var_h_inout = x[23];
	der_var_h_inout = der_x[23];
	var_h_transfer = x[24];
	der_var_h_transfer = der_x[24];
	var_h_react = x[25];
	der_var_h_react = der_x[25];
	var_m = x[26];
	der_var_m = der_x[26];
	var_ma = x[27];
	der_var_ma = der_x[27];
	var_mb = x[28];
	der_var_mb = der_x[28];
	var_mc = x[29];
	der_var_mc = der_x[29];
	var_mp = x[30];
	der_var_mp = der_x[30];
	var_me = x[31];
	der_var_me = der_x[31];
	var_tr = x[32];
	der_var_tr = der_x[32];
	var_obj = x[33];
	der_var_obj = der_x[33];
	var_ttime = x[34];
	der_var_ttime = der_x[34];
// temporary assignment i_expr not here
// procedure assignment div0 not here
// procedure assignment exp1 not here
// temporary assignment i_expr2 not here
// temporary assignment i_expr4 not here
// procedure assignment div3 not here
// procedure assignment exp5 not here
// temporary assignment i_expr6 not here
// temporary assignment i_expr8 not here
// procedure assignment div7 not here
// procedure assignment exp9 not here
// temporary assignment i_expr10 not here
// procedure assignment div11 not here
// procedure assignment div12 not here
// procedure assignment div13 not here
// procedure assignment div14 not here
// procedure assignment div15 not here
// procedure assignment div16 not here
// procedure assignment div17 not here
// procedure assignment div18 not here
// procedure assignment div19 not here
// procedure assignment div20 not here
// procedure assignment div21 not here
// procedure assignment div22 not here
// temporary assignment i_expr24 not here
// temporary assignment i_expr25 not here
// procedure assignment div23 not here
// temporary assignment i_expr27 not here
// procedure assignment div26 not here

i_E = i_E_start;
while (i_E < i_E_end) {
  i__switch = yy_ind[i_E];
  switch (i__switch) {

  case 0: // scalar equation 0
var_i_expr= ( 273.15+var_tr ) ;
acs_div(par_b2, var_i_expr, var_div0);
var_i_expr2=-1000 * var_div0;
acs_exp(var_i_expr2, var_exp1);
  yy[i_E] = var_k1 - (par_b1 * var_exp1);
  i_E = i_E+1;  break;
  case 1: // scalar equation 1
var_i_expr4= ( 273.15+var_tr ) ;
acs_div(par_b4, var_i_expr4, var_div3);
var_i_expr6=-1000 * var_div3;
acs_exp(var_i_expr6, var_exp5);
  yy[i_E] = var_k2 - (par_b3 * var_exp5);
  i_E = i_E+1;  break;
  case 2: // scalar equation 2
var_i_expr8= ( 273.15+var_tr ) ;
acs_div(par_b6, var_i_expr8, var_div7);
var_i_expr10=-1000 * var_div7;
acs_exp(var_i_expr10, var_exp9);
  yy[i_E] = var_k3 - (par_b5 * var_exp9);
  i_E = i_E+1;  break;
  case 3: // scalar equation 3
  yy[i_E] = var_r1 - (1000 * var_k1 * var_wa * var_wb);
  i_E = i_E+1;  break;
  case 4: // scalar equation 4
  yy[i_E] = var_r2 - (1000 * var_k2 * var_wb * var_wc);
  i_E = i_E+1;  break;
  case 5: // scalar equation 5
  yy[i_E] = var_r3 - (1000 * var_k3 * var_wc * var_wp);
  i_E = i_E+1;  break;
  case 6: // scalar equation 6
acs_div(var_ma, var_m, var_div11);
  yy[i_E] = var_wa - (var_div11);
  i_E = i_E+1;  break;
  case 7: // scalar equation 7
acs_div(var_mb, var_m, var_div12);
  yy[i_E] = var_wb - (var_div12);
  i_E = i_E+1;  break;
  case 8: // scalar equation 8
acs_div(var_mc, var_m, var_div13);
  yy[i_E] = var_wc - (var_div13);
  i_E = i_E+1;  break;
  case 9: // scalar equation 9
acs_div(var_mp, var_m, var_div14);
  yy[i_E] = var_wp - (var_div14);
  i_E = i_E+1;  break;
  case 10: // scalar equation 10
acs_div(var_me, var_m, var_div15);
  yy[i_E] = var_we - (var_div15);
  i_E = i_E+1;  break;
  case 11: // scalar equation 11
acs_div(var_mg, var_m, var_div16);
  yy[i_E] = var_wg - (var_div16);
  i_E = i_E+1;  break;
  case 12: // scalar equation 12
  yy[i_E] = der_var_m - (par_deltatime *  ( var_fin-var_fout ) );
  i_E = i_E+1;  break;
  case 13: // scalar equation 13
  yy[i_E] = var_fin - (par_yconti * par_fain+par_fbin);
  i_E = i_E+1;  break;
  case 14: // scalar equation 14
  yy[i_E] = var_fout - (par_yconti * var_fin);
  i_E = i_E+1;  break;
  case 15: // scalar equation 15
  yy[i_E] = der_var_ma - (par_deltatime * var_dma);
  i_E = i_E+1;  break;
  case 16: // scalar equation 16
  yy[i_E] = var_dma - (par_yconti * par_fain-var_wa * var_fout-0.001 * var_r1 * var_m);
  i_E = i_E+1;  break;
  case 17: // scalar equation 17
  yy[i_E] = der_var_mb - (par_deltatime * var_dmb);
  i_E = i_E+1;  break;
  case 18: // scalar equation 18
acs_div(var_r1, par_mwa, var_div17);
  yy[i_E] = var_dmb - (par_fbin-var_wb * var_fout- ( 0.001 * par_mwb * var_div17+0.001 * var_r2 )  * var_m);
  i_E = i_E+1;  break;
  case 19: // scalar equation 19
  yy[i_E] = der_var_mc - (par_deltatime * var_dmc);
  i_E = i_E+1;  break;
  case 20: // scalar equation 20
acs_div(var_r1, par_mwa, var_div18);
acs_div(var_r2, par_mwb, var_div19);
  yy[i_E] = var_dmc - ( ( 0.001 * par_mwc * var_div18-0.001 * par_mwc * var_div19-0.001 * var_r3 )  * var_m-var_wc * var_fout);
  i_E = i_E+1;  break;
  case 21: // scalar equation 21
  yy[i_E] = der_var_mp - (par_deltatime * var_dmp);
  i_E = i_E+1;  break;
  case 22: // scalar equation 22
acs_div(var_r2, par_mwb, var_div20);
acs_div(var_r3, par_mwc, var_div21);
  yy[i_E] = var_dmp - ( ( 0.001 * par_mwp * var_div20-0.001 * par_mwp * var_div21 )  * var_m-var_wp * var_fout);
  i_E = i_E+1;  break;
  case 23: // scalar equation 23
  yy[i_E] = der_var_me - (par_deltatime * var_dme);
  i_E = i_E+1;  break;
  case 24: // scalar equation 24
acs_div(var_m, par_mwb, var_div22);
  yy[i_E] = var_dme - (0.001 * par_mwe * var_r2 * var_div22-var_we * var_fout);
  i_E = i_E+1;  break;
  case 25: // scalar equation 25
  yy[i_E] = var_m - (var_ma+var_mb+var_mc+var_mp+var_me+var_mg);
  i_E = i_E+1;  break;
  case 26: // scalar equation 26
  yy[i_E] = var_m - (par_rho * var_v);
  i_E = i_E+1;  break;
  case 27: // scalar equation 27
  yy[i_E] = der_var_tr - (par_deltatime * var_dtdt);
  i_E = i_E+1;  break;
  case 28: // scalar equation 28
var_i_expr24= ( var_h_masschange+var_h_inout+var_h_transfer+var_h_react ) ;
var_i_expr25= ( var_m * par_cp ) ;
acs_div(var_i_expr24, var_i_expr25, var_div23);
  yy[i_E] = var_dtdt - (var_div23);
  i_E = i_E+1;  break;
  case 29: // scalar equation 29
  yy[i_E] = var_h_react - (-0.001 *  ( par_dh1 * var_r1 * var_m+par_dh2 * var_r2 * var_m+par_dh3 * var_r3 * var_m ) );
  i_E = i_E+1;  break;
  case 30: // scalar equation 30
var_i_expr27= ( var_tr-par_tw ) ;
acs_div(var_i_expr27, par_v0, var_div26);
  yy[i_E] = var_h_transfer - (-var_v * par_a0 * par_u * var_div26);
  i_E = i_E+1;  break;
  case 31: // scalar equation 31
  yy[i_E] = var_h_inout - (par_fbin * par_cp * par_tbin+par_yconti * par_fain * par_cp * par_tain-par_yconti * var_fout * par_cp * var_tr);
  i_E = i_E+1;  break;
  case 32: // scalar equation 32
  yy[i_E] = var_h_masschange - (- ( 1-par_yconti )  * par_fbin * par_cp * var_tr);
  i_E = i_E+1;  break;
  case 33: // scalar equation 33
  yy[i_E] = der_var_obj - (par_deltatime *  ( -1143.38 * var_wp * var_fout-25.92 * var_we * var_fout+114.34 * par_fbin+76.23 * par_fain * par_yconti ) );
  i_E = i_E+1;  break;
  case 34: // scalar equation 34
  yy[i_E] = der_var_ttime - (par_deltatime);
  i_E = i_E+1;  break;
// //default: i_E=i_E+1; break; 
  } // switch
} // end while
} // end of block_* function
void res_block(double * yy, 
double * der_x, double * x, double * p,
int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
int& n_yy_ind, int* yy_ind)
  #pragma ad indep x p
  #pragma ad dep yy
{
 int i_E = 0;
 int i_E_end = 35;
 int i__start = 0;
 int i__end = 0;
 int i__switch = 0;
while (i_E < n_yy_ind) {
  i__switch = yy_ind[i_E];
i__start = 0;
i__end = 35;
if ((i__switch >= i__start) && (i__switch < i__end)) {
  i__end = i_E+1;
  res_block_0(yy, der_x, x, p, condit, lock, prev, n_x, n_p, n_c, n_yy_ind, yy_ind, i_E, i__end);
}
i_E = i_E+1; } // while
}  // res_block
