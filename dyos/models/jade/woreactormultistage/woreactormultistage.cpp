#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	p[0] = 100; // tw
	p[1] = 5.784; // fbin
	p[2] = 1; // deltatime
	p[3] = 25; // tain
	p[4] = 25; // tbin
	p[5] = 1659900; // b1
	p[6] = 6.6667; // b2
	p[7] = 721170000; // b3
	p[8] = 8.333299999999999; // b4
	p[9] = 2674500000000; // b5
	p[10] = 11.111; // b6
	p[11] = 0; // yconti
	p[12] = 20; // u
	p[13] = 0.0031413; // kout
	p[14] = 100; // mwa
	p[15] = 100; // mwb
	p[16] = 100; // mwp
	p[17] = 200; // mwc
	p[18] = 200; // mwe
	p[19] = 300; // mwg
	p[20] = 4.184; // cp
	p[21] = 1000; // rho
	p[22] = -263.8; // dh1
	p[23] = -158.3; // dh2
	p[24] = -226.3; // dh3
	p[25] = 9.2903; // a0
	p[26] = 2.1052; // v0
	p[27] = 2; // fain
	x[0] = 0; // k1
	x[1] = 0; // k2
	x[2] = 0; // k3
	x[3] = 0; // r1
	x[4] = 0; // r2
	x[5] = 0; // r3
	x[6] = 0; // wa
	x[7] = 0; // wb
	x[8] = 0; // wc
	x[9] = 0; // wp
	x[10] = 0; // wg
	x[11] = 0; // we
	x[12] = 0; // mg
	x[13] = 0; // fin
	x[14] = 0; // fout
	x[15] = 0; // v
	x[16] = 0; // dtdt
	x[17] = 0; // dma
	x[18] = 0; // dmb
	x[19] = 0; // dmc
	x[20] = 0; // dme
	x[21] = 0; // dmp
	x[22] = 0; // h_masschange
	x[23] = 0; // h_inout
	x[24] = 0; // h_transfer
	x[25] = 0; // h_react
	x[26] = 1000; // m
	x[27] = 1000; // ma
	x[28] = 0; // mb
	x[29] = 0; // mc
	x[30] = 0; // mp
	x[31] = 0; // me
	x[32] = 65; // tr
	x[33] = 0; // obj
	x[34] = 0; // ttime
	n_x = 35; n_p = 28; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("k1");
	names->push_back("k2");
	names->push_back("k3");
	names->push_back("r1");
	names->push_back("r2");
	names->push_back("r3");
	names->push_back("wa");
	names->push_back("wb");
	names->push_back("wc");
	names->push_back("wp");
	names->push_back("wg");
	names->push_back("we");
	names->push_back("mg");
	names->push_back("fin");
	names->push_back("fout");
	names->push_back("v");
	names->push_back("dtdt");
	names->push_back("dma");
	names->push_back("dmb");
	names->push_back("dmc");
	names->push_back("dme");
	names->push_back("dmp");
	names->push_back("h_masschange");
	names->push_back("h_inout");
	names->push_back("h_transfer");
	names->push_back("h_react");
	names->push_back("m");
	names->push_back("ma");
	names->push_back("mb");
	names->push_back("mc");
	names->push_back("mp");
	names->push_back("me");
	names->push_back("tr");
	names->push_back("obj");
	names->push_back("ttime");
}
void get_num_vars(int & nv) {
  nv = 35;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("tw");
	names->push_back("fbin");
	names->push_back("deltatime");
	names->push_back("tain");
	names->push_back("tbin");
	names->push_back("b1");
	names->push_back("b2");
	names->push_back("b3");
	names->push_back("b4");
	names->push_back("b5");
	names->push_back("b6");
	names->push_back("yconti");
	names->push_back("u");
	names->push_back("kout");
	names->push_back("mwa");
	names->push_back("mwb");
	names->push_back("mwp");
	names->push_back("mwc");
	names->push_back("mwe");
	names->push_back("mwg");
	names->push_back("cp");
	names->push_back("rho");
	names->push_back("dh1");
	names->push_back("dh2");
	names->push_back("dh3");
	names->push_back("a0");
	names->push_back("v0");
	names->push_back("fain");
}
void get_num_pars(int & np) {
  np = 28;
}


void get_num_eqns(int& ne){
 ne = 35;
}

void get_num_cond(int& nc){
 nc = 0;
}
