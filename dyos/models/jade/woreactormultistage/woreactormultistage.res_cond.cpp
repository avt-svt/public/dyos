#include "EsoAcsammm_Header.hpp"

void res_cond(double * cond, 
double * der_x, double * x, 
double * p, int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep cond
{

	int i_E=0;
	int i_C=0;
	int cond___=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double par_tw=0;
	double par_fbin=0;
	double par_deltatime=0;
	double par_tain=0;
	double par_tbin=0;
	double par_b1=0;
	double par_b2=0;
	double par_b3=0;
	double par_b4=0;
	double par_b5=0;
	double par_b6=0;
	double par_yconti=0;
	double par_u=0;
	double par_kout=0;
	double par_mwa=0;
	double par_mwb=0;
	double par_mwp=0;
	double par_mwc=0;
	double par_mwe=0;
	double par_mwg=0;
	double par_cp=0;
	double par_rho=0;
	double par_dh1=0;
	double par_dh2=0;
	double par_dh3=0;
	double par_a0=0;
	double par_v0=0;
	double par_fain=0;
	double var_k1=0;
	double der_var_k1=0;
	double var_k2=0;
	double der_var_k2=0;
	double var_k3=0;
	double der_var_k3=0;
	double var_r1=0;
	double der_var_r1=0;
	double var_r2=0;
	double der_var_r2=0;
	double var_r3=0;
	double der_var_r3=0;
	double var_wa=0;
	double der_var_wa=0;
	double var_wb=0;
	double der_var_wb=0;
	double var_wc=0;
	double der_var_wc=0;
	double var_wp=0;
	double der_var_wp=0;
	double var_wg=0;
	double der_var_wg=0;
	double var_we=0;
	double der_var_we=0;
	double var_mg=0;
	double der_var_mg=0;
	double var_fin=0;
	double der_var_fin=0;
	double var_fout=0;
	double der_var_fout=0;
	double var_v=0;
	double der_var_v=0;
	double var_dtdt=0;
	double der_var_dtdt=0;
	double var_dma=0;
	double der_var_dma=0;
	double var_dmb=0;
	double der_var_dmb=0;
	double var_dmc=0;
	double der_var_dmc=0;
	double var_dme=0;
	double der_var_dme=0;
	double var_dmp=0;
	double der_var_dmp=0;
	double var_h_masschange=0;
	double der_var_h_masschange=0;
	double var_h_inout=0;
	double der_var_h_inout=0;
	double var_h_transfer=0;
	double der_var_h_transfer=0;
	double var_h_react=0;
	double der_var_h_react=0;
	double var_m=0;
	double der_var_m=0;
	double var_ma=0;
	double der_var_ma=0;
	double var_mb=0;
	double der_var_mb=0;
	double var_mc=0;
	double der_var_mc=0;
	double var_mp=0;
	double der_var_mp=0;
	double var_me=0;
	double der_var_me=0;
	double var_tr=0;
	double der_var_tr=0;
	double var_obj=0;
	double der_var_obj=0;
	double var_ttime=0;
	double der_var_ttime=0;
	double var_i_expr=0; // assign var
	double var_div0=0; // procedure var
	double var_exp1=0; // procedure var
	double var_i_expr2=0; // assign var
	double var_i_expr4=0; // assign var
	double var_div3=0; // procedure var
	double var_exp5=0; // procedure var
	double var_i_expr6=0; // assign var
	double var_i_expr8=0; // assign var
	double var_div7=0; // procedure var
	double var_exp9=0; // procedure var
	double var_i_expr10=0; // assign var
	double var_div11=0; // procedure var
	double var_div12=0; // procedure var
	double var_div13=0; // procedure var
	double var_div14=0; // procedure var
	double var_div15=0; // procedure var
	double var_div16=0; // procedure var
	double var_div17=0; // procedure var
	double var_div18=0; // procedure var
	double var_div19=0; // procedure var
	double var_div20=0; // procedure var
	double var_div21=0; // procedure var
	double var_div22=0; // procedure var
	double var_i_expr24=0; // assign var
	double var_i_expr25=0; // assign var
	double var_div23=0; // procedure var
	double var_i_expr27=0; // assign var
	double var_div26=0; // procedure var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	par_tw = p[0];
	par_fbin = p[1];
	par_deltatime = p[2];
	par_tain = p[3];
	par_tbin = p[4];
	par_b1 = p[5];
	par_b2 = p[6];
	par_b3 = p[7];
	par_b4 = p[8];
	par_b5 = p[9];
	par_b6 = p[10];
	par_yconti = p[11];
	par_u = p[12];
	par_kout = p[13];
	par_mwa = p[14];
	par_mwb = p[15];
	par_mwp = p[16];
	par_mwc = p[17];
	par_mwe = p[18];
	par_mwg = p[19];
	par_cp = p[20];
	par_rho = p[21];
	par_dh1 = p[22];
	par_dh2 = p[23];
	par_dh3 = p[24];
	par_a0 = p[25];
	par_v0 = p[26];
	par_fain = p[27];
	var_k1 = x[0];
	der_var_k1 = der_x[0];
	var_k2 = x[1];
	der_var_k2 = der_x[1];
	var_k3 = x[2];
	der_var_k3 = der_x[2];
	var_r1 = x[3];
	der_var_r1 = der_x[3];
	var_r2 = x[4];
	der_var_r2 = der_x[4];
	var_r3 = x[5];
	der_var_r3 = der_x[5];
	var_wa = x[6];
	der_var_wa = der_x[6];
	var_wb = x[7];
	der_var_wb = der_x[7];
	var_wc = x[8];
	der_var_wc = der_x[8];
	var_wp = x[9];
	der_var_wp = der_x[9];
	var_wg = x[10];
	der_var_wg = der_x[10];
	var_we = x[11];
	der_var_we = der_x[11];
	var_mg = x[12];
	der_var_mg = der_x[12];
	var_fin = x[13];
	der_var_fin = der_x[13];
	var_fout = x[14];
	der_var_fout = der_x[14];
	var_v = x[15];
	der_var_v = der_x[15];
	var_dtdt = x[16];
	der_var_dtdt = der_x[16];
	var_dma = x[17];
	der_var_dma = der_x[17];
	var_dmb = x[18];
	der_var_dmb = der_x[18];
	var_dmc = x[19];
	der_var_dmc = der_x[19];
	var_dme = x[20];
	der_var_dme = der_x[20];
	var_dmp = x[21];
	der_var_dmp = der_x[21];
	var_h_masschange = x[22];
	der_var_h_masschange = der_x[22];
	var_h_inout = x[23];
	der_var_h_inout = der_x[23];
	var_h_transfer = x[24];
	der_var_h_transfer = der_x[24];
	var_h_react = x[25];
	der_var_h_react = der_x[25];
	var_m = x[26];
	der_var_m = der_x[26];
	var_ma = x[27];
	der_var_ma = der_x[27];
	var_mb = x[28];
	der_var_mb = der_x[28];
	var_mc = x[29];
	der_var_mc = der_x[29];
	var_mp = x[30];
	der_var_mp = der_x[30];
	var_me = x[31];
	der_var_me = der_x[31];
	var_tr = x[32];
	der_var_tr = der_x[32];
	var_obj = x[33];
	der_var_obj = der_x[33];
	var_ttime = x[34];
	der_var_ttime = der_x[34];
// temporary assignment i_expr not here
// procedure assignment div0 not here
// procedure assignment exp1 not here
// temporary assignment i_expr2 not here
// temporary assignment i_expr4 not here
// procedure assignment div3 not here
// procedure assignment exp5 not here
// temporary assignment i_expr6 not here
// temporary assignment i_expr8 not here
// procedure assignment div7 not here
// procedure assignment exp9 not here
// temporary assignment i_expr10 not here
// procedure assignment div11 not here
// procedure assignment div12 not here
// procedure assignment div13 not here
// procedure assignment div14 not here
// procedure assignment div15 not here
// procedure assignment div16 not here
// procedure assignment div17 not here
// procedure assignment div18 not here
// procedure assignment div19 not here
// procedure assignment div20 not here
// procedure assignment div21 not here
// procedure assignment div22 not here
// temporary assignment i_expr24 not here
// temporary assignment i_expr25 not here
// procedure assignment div23 not here
// temporary assignment i_expr27 not here
// procedure assignment div26 not here

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

//condition of equation begin
// simple equation //end

  }
