
#pragma once

#include "Loader.hpp"

  
namespace utils {

  class DummyLoader:public utils::Loader {
  public:
      DummyLoader();
      DummyLoader(std::string);
      DummyLoader(const DummyLoader& l);
      ~DummyLoader(); 
      int dllCallOf(std::string, int);
   };

}//namespace utils
