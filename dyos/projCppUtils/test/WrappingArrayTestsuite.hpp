/** @file WrappingArray.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* Testfile for the Array class                                         \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 28.6.2011
*/


#include "Array.hpp"

using namespace utils;

/**
* @test TestWrappingArray - test for the constructor WrappingArray(size, pointer)
*
* case: it is tested whether a WrappingArray can be instantiated with a preallocated C-Array
*/
BOOST_AUTO_TEST_CASE(ConstructorData) 
{ 
  unsigned arraySize=10;
  int value=20;
  std::vector<int> intField(arraySize);
  intField[0]=20;
  WrappingArray<int> testArray(arraySize, intField.data());
  BOOST_CHECK_EQUAL(intField[0], testArray[0]);
}

/**
* @test TestWrappingArray - test for function setData
* 
* case: it is tested whether the data pointer can be reset after instantiation of a WrapperArray object
*/
BOOST_AUTO_TEST_CASE(TestSetData)
{
  unsigned initialArraySize=10;
  double initialValue=0.5;
  unsigned arraySize=4;
  double testValue=1.5;
  std::vector<double> values(arraySize);
  values[0]=testValue;
  WrappingArray<double> testArray(initialArraySize, &testValue);
  testArray.setData(arraySize, values.data());
  BOOST_CHECK_EQUAL(arraySize, testArray.getSize());
  BOOST_CHECK_EQUAL(testValue, testArray[0]);
}

/**
* @test TestWrappingArray - test for destructor
*
* case: it is tested whether a C-Array pointer is still usable after destruction of the WrapperArray
*       object that was containing it.
*/
BOOST_AUTO_TEST_CASE(Destructor)
{
  unsigned arraySize=4;
  std::vector<double> values(arraySize);
  double firstValue=0.5;
  {
    WrappingArray<double> testArray(arraySize, values.data());
    testArray[0]=firstValue;
  }
  BOOST_CHECK_EQUAL(firstValue, values[0]);
}