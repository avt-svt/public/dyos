#include "CscMatrix.hpp"
#include "TripletMatrix.hpp"
#include "MatrixExceptions.hpp"




BOOST_AUTO_TEST_SUITE(TestCscMatrix)


/**
* @test TestCscMatrix - test createCscMatrix
*
* this test checks if the Matrix constructor work correctly
* implicitly also checks get_matrix_ptr (which is a simple getter function)
*/
BOOST_AUTO_TEST_CASE(TestCreateTripletMatrix)
{
  //test construction with cs pointer
  const int m=3, n=4, nnz=5;
  int rowIndices[nnz] = {0,0,1,2,2};
  int colIndices[nnz] = {2,3,0,1,3};
  double values[nnz] = {0.5, 1.5, 2.5, 3.5, 4.5};
  cs* mat = cs_spalloc(m, n, nnz, true, true);
  mat->nz = nnz;
  for(int i=0; i<nnz; i++){
    mat->i[i] = rowIndices[i];
    mat->p[i] = colIndices[i];
    mat->x[i] = values[i];
  }
  
  bool owner=true;
  CsCscMatrix test(mat,owner);
  //check if matrix has been compressed
  cs* compMat = test.get_matrix_ptr();
  BOOST_CHECK(mat != compMat);
  BOOST_CHECK_EQUAL(compMat->nz, -1);
  BOOST_CHECK_EQUAL(compMat->m, m);
  BOOST_CHECK_EQUAL(compMat->n, n);
  BOOST_CHECK_EQUAL(compMat->nzmax, nnz);
  
  BOOST_CHECK_EQUAL(compMat->i[0], 1);
  BOOST_CHECK_EQUAL(compMat->x[0], 2.5);
  
  BOOST_CHECK_EQUAL(compMat->i[1], 2);
  BOOST_CHECK_EQUAL(compMat->x[1], 3.5);
  
  BOOST_CHECK_EQUAL(compMat->i[2], 0);
  BOOST_CHECK_EQUAL(compMat->x[2], 0.5);
  
  BOOST_CHECK_EQUAL(compMat->i[3], 0);
  BOOST_CHECK_EQUAL(compMat->x[3], 1.5);
  
  BOOST_CHECK_EQUAL(compMat->i[4], 2);
  BOOST_CHECK_EQUAL(compMat->x[4], 4.5);
  
  BOOST_CHECK_EQUAL(compMat->p[0], 0);
  BOOST_CHECK_EQUAL(compMat->p[1], 1);
  BOOST_CHECK_EQUAL(compMat->p[2], 2);
  BOOST_CHECK_EQUAL(compMat->p[3], 3);
  BOOST_CHECK_EQUAL(compMat->p[4], 5);
}

/**
* @test TestTripletMatrix - TestGetNumFunctions
*
* test the functions get_n_rows, get_n_cols and get_nnz
*/
BOOST_AUTO_TEST_CASE(TestGetNumFunctions)
{
  const int m=6, n=12, nnz=7;
  cs *mat = cs_spalloc(m, n, nnz, true, false);
  
  for(int i=0; i<nnz; i++){
    mat->p[i] = i;
    mat->i[i] = i;
    mat->x[i] = 0.5 + i;
  }
  for(int i=nnz; i<=n; i++){
    mat->p[i] = nnz;
  }
  CsCscMatrix test(mat, true);
  BOOST_CHECK_EQUAL(test.get_n_rows(), m);
  BOOST_CHECK_EQUAL(test.get_n_cols(), n);
  BOOST_CHECK_EQUAL(test.get_nnz(), nnz);
}

BOOST_AUTO_TEST_CASE(TestGetValues)
{
  const int m=8, n=17, nnz=5;
  
  int rows[nnz] = {2,1,7,4,2};
  int cols[nnz] = {5,8,1,6,4};
  double values[nnz] = {6.2345674567, 74354.239423, 93456.05453, 324242.5, 0.123355};
  CsTripletMatrix triplet(m, n, nnz, rows, cols, values);
  CsCscMatrix::Ptr test = triplet.compress();
  
  double checkValues[nnz];
  BOOST_CHECK_NO_THROW(test->get_values(nnz, checkValues));
  //values are now sorted columnwise
  BOOST_CHECK_EQUAL(checkValues[0], values[2]);
  BOOST_CHECK_EQUAL(checkValues[1], values[4]);
  BOOST_CHECK_EQUAL(checkValues[2], values[0]);
  BOOST_CHECK_EQUAL(checkValues[3], values[3]);
  BOOST_CHECK_EQUAL(checkValues[4], values[1]);

}

/**
* @test TestTripletMatrix - test getValue
*/
BOOST_AUTO_TEST_CASE(TestGetValue)
{
  const int m=10, n=12, nnz=3;
  
  int rows[nnz] = {2,1,7};
  int cols[nnz] = {5,8,1};
  double values[nnz] = {6.2345674567, 74354.239423, 93456.05453};
  CsTripletMatrix triplet(m, n, nnz, rows, cols, values);
  CsCscMatrix::Ptr test = triplet.compress();
  
  double value = 0.12345;
  BOOST_CHECK_NO_THROW(test->getValue(rows[1], cols[1], value));
  BOOST_CHECK_EQUAL(value, values[1]);  
  BOOST_CHECK_NO_THROW(test->getValue(4, 3, value));
  BOOST_CHECK_EQUAL(value, 0.0);
}

/**
* @test TestCscMatrix - test function horzcat_in_place
*/
BOOST_AUTO_TEST_CASE(TestHorzcatInPlace)
{
  const int m=10, n1=5, n2=12, nnz1=6, nnz2=4;
  
  int rows1[nnz1] = {5,3,7,6,9,7};
  int cols1[nnz1] = {0,3,4,2,2,1};
  double values1[nnz1] = {1.1, 1.2, 1.3, 1.4, 1.5, 1.6};
  CsTripletMatrix triplet1(m, n1, nnz1, rows1, cols1, values1);
  CsCscMatrix::Ptr matrix1 = triplet1.compress();
  
  int rows2[nnz2] = {2,0,4,3};
  int cols2[nnz2] = {0,0,3,11};
  double values2[nnz2] = {2.1, 2.2, 2.3, 2.4};
  CsTripletMatrix triplet2(m, n2, nnz2, rows2, cols2, values2);
  CsCscMatrix::Ptr matrix2 = triplet2.compress();
  
  matrix1->horzcat_in_place(matrix2);
  
  cs* mat = matrix1->get_matrix_ptr();
  
  BOOST_CHECK_EQUAL(mat->m, m);
  BOOST_CHECK_EQUAL(mat->n, n1+n2);
  BOOST_CHECK_EQUAL(mat->nzmax, nnz1+nnz2);
  
  BOOST_CHECK_EQUAL(mat->i[0], 5);
  BOOST_CHECK_EQUAL(mat->x[0], 1.1);
  
  BOOST_CHECK_EQUAL(mat->i[1], 7);
  BOOST_CHECK_EQUAL(mat->x[1], 1.6);
  
  BOOST_CHECK_EQUAL(mat->i[2], 6);
  BOOST_CHECK_EQUAL(mat->x[2], 1.4);
  
  BOOST_CHECK_EQUAL(mat->i[3], 9);
  BOOST_CHECK_EQUAL(mat->x[3], 1.5);
  
  BOOST_CHECK_EQUAL(mat->i[4], 3);
  BOOST_CHECK_EQUAL(mat->x[4], 1.2);
  
  BOOST_CHECK_EQUAL(mat->i[5], 7);
  BOOST_CHECK_EQUAL(mat->x[5], 1.3);
  
  BOOST_CHECK_EQUAL(mat->i[6], 2);
  BOOST_CHECK_EQUAL(mat->x[6], 2.1);
  
  BOOST_CHECK_EQUAL(mat->i[7], 0);
  BOOST_CHECK_EQUAL(mat->x[7], 2.2);
  
  BOOST_CHECK_EQUAL(mat->i[8], 4);
  BOOST_CHECK_EQUAL(mat->x[8], 2.3);
  
  BOOST_CHECK_EQUAL(mat->i[9], 3);
  BOOST_CHECK_EQUAL(mat->x[9], 2.4);
  
  BOOST_CHECK_EQUAL(mat->p[0], 0);
  BOOST_CHECK_EQUAL(mat->p[1], 1);
  BOOST_CHECK_EQUAL(mat->p[2], 2);
  BOOST_CHECK_EQUAL(mat->p[3], 4);
  BOOST_CHECK_EQUAL(mat->p[4], 5);
  BOOST_CHECK_EQUAL(mat->p[5], 6);
  BOOST_CHECK_EQUAL(mat->p[6], 8);
  BOOST_CHECK_EQUAL(mat->p[7], 8);
  BOOST_CHECK_EQUAL(mat->p[8], 8);
  BOOST_CHECK_EQUAL(mat->p[9], 9);
  BOOST_CHECK_EQUAL(mat->p[10], 9);
  BOOST_CHECK_EQUAL(mat->p[11], 9);
  BOOST_CHECK_EQUAL(mat->p[12], 9);
  BOOST_CHECK_EQUAL(mat->p[13], 9);
  BOOST_CHECK_EQUAL(mat->p[14], 9);
  BOOST_CHECK_EQUAL(mat->p[15], 9);
  BOOST_CHECK_EQUAL(mat->p[16], 9);
  BOOST_CHECK_EQUAL(mat->p[17], 10);
}

/**
* @test TestCscMatrix - test function extractColumn
*/
BOOST_AUTO_TEST_CASE(TestExtractColumn)
{
  const int m=6, n = 8, nnz = 10;
  
  int rows[nnz] = {5,2,4,1,1,0,4,5,3,3};
  int cols[nnz] = {4,3,0,0,2,3,1,7,7,3};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
  CsTripletMatrix triplet(m, n, nnz, rows, cols, values);
  CsCscMatrix::Ptr matrix = triplet.compress();
  
  double column[m];
  matrix->extractColumn(3, column, m);
  
  BOOST_CHECK_EQUAL(column[0], 6.0);
  BOOST_CHECK_EQUAL(column[1], 0.0);
  BOOST_CHECK_EQUAL(column[2], 2.0);
  BOOST_CHECK_EQUAL(column[3], 10.0);
  BOOST_CHECK_EQUAL(column[4], 0.0);
  BOOST_CHECK_EQUAL(column[5], 0.0);
  
}

/**
* @test TestCscMatrix - tests function getMatrixRank
*/
BOOST_AUTO_TEST_CASE(TestGetMatrixRank)
{
  //construct a matrix with zero colums and rows
  const int m=5, n=7, nnz=6;
  int rows[nnz] = {0,0,2,3,4,4};
  int cols[nnz] = {1,3,4,2,5,5};
  double values[nnz]= {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  
  CsTripletMatrix triplet(m, n, nnz, rows, cols, values);
  CsCscMatrix::Ptr matrix = triplet.compress();
  
  BOOST_CHECK_EQUAL(matrix->getMatrixRank(), 4);
}

BOOST_AUTO_TEST_SUITE_END()
