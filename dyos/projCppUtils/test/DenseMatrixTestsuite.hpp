#include "DenseMatrix.hpp"

BOOST_AUTO_TEST_SUITE(TestDenseMatrix)

/**
* @test TestDenseMatrix - test matrix conversion
*/
BOOST_AUTO_TEST_CASE(TestSparseMatrixToFull)
{
  const int m=3, n=4, nnz=5;
  int rows[nnz] = {0,1,0,2,2};
  int cols[nnz] = {0,2,3,2,1};
  double values[nnz] = {1.0, 2.0, 3.0, 4.0, 5.0};
  
  CsTripletMatrix::Ptr triplet(new CsTripletMatrix(m, n, nnz, rows, cols, values));
  CsCscMatrix::Ptr csc = triplet->compress();
  std::vector<std::vector<double> > matrixFromTriplet, matrixFromCsc;
  
  convertSparseMatrixToFull(triplet, matrixFromTriplet);
  convertSparseMatrixToFull(csc, matrixFromCsc);
  
  BOOST_REQUIRE_EQUAL(matrixFromTriplet.size(), m);
  BOOST_REQUIRE_EQUAL(matrixFromCsc.size(), m);
  for(int i=0; i<m; i++){
    BOOST_REQUIRE_EQUAL(matrixFromTriplet[i].size(), n);
    BOOST_REQUIRE_EQUAL(matrixFromCsc[i].size(), n);
  }
  
  BOOST_CHECK_EQUAL(matrixFromTriplet[0][0], 1.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[0][1], 0.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[0][2], 0.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[0][3], 3.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[1][0], 0.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[1][1], 0.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[1][2], 2.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[1][3], 0.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[2][0], 0.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[2][1], 5.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[2][2], 4.0);
  BOOST_CHECK_EQUAL(matrixFromTriplet[2][3], 0.0);

  BOOST_CHECK_EQUAL(matrixFromCsc[0][0], 1.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[0][1], 0.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[0][2], 0.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[0][3], 3.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[1][0], 0.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[1][1], 0.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[1][2], 2.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[1][3], 0.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[2][0], 0.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[2][1], 5.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[2][2], 4.0);
  BOOST_CHECK_EQUAL(matrixFromCsc[2][3], 0.0);
}

BOOST_AUTO_TEST_CASE(TestMultiply)
{
  std::vector<std::vector<double> > matrix1, matrix2, matrix3;
  
  const unsigned m=3, n=4, r=2;
  matrix1.resize(m);
  matrix2.resize(n);
  for(unsigned i=0; i<matrix1.size(); i++){
    matrix1[i].resize(matrix2.size(), 1.0);
    for(unsigned j=0; j<matrix2.size(); j++){
      matrix2[j].resize(r, 1.0);
    }
  }
  
  matrix1[0][1] = 2.0;
  matrix1[0][2] = 3.0;
  matrix1[1][1] = 0.0;
  matrix1[1][3] = -1.0;
  matrix1[2][0] = 2.0;
  matrix1[2][2] = 2.0;
  matrix1[2][3] = 2.0;
  
  matrix2[0][1] = -1.0;
  matrix2[1][0] = 2.0;
  matrix2[1][1] = 2.0;
  matrix2[2][1] = 3.0;
  matrix2[3][0] = 2.0;
  
  matrix3 = multiply(matrix1, matrix2);
  
  BOOST_REQUIRE_EQUAL(matrix3.size(), m);
  for(unsigned i=0; i<m; i++){
    BOOST_REQUIRE_EQUAL(matrix3[i].size(), r);
  }
  
  const double threshold = 1e-16;
  BOOST_CHECK_CLOSE(matrix3[0][0], 10.0, threshold);
  BOOST_CHECK_CLOSE(matrix3[0][1], 13.0, threshold);
  BOOST_CHECK_CLOSE(matrix3[1][0], 0.0, threshold);
  BOOST_CHECK_CLOSE(matrix3[1][1], 1.0, threshold);
  BOOST_CHECK_CLOSE(matrix3[2][0], 10.0, threshold);
  BOOST_CHECK_CLOSE(matrix3[2][1], 8.0, threshold);
}

/**
* @test TestDenseMatrix - test function invertMatrix
*/
BOOST_AUTO_TEST_CASE(TestInvertMatrix)
{
  const unsigned dim = 3;
  std::vector<std::vector<double> > matrix(dim), singularMatrix(dim), result;
  
  for(unsigned i=0; i<dim; i++){
    matrix[i].resize(dim, 1.0);
    singularMatrix[i].resize(dim, i*1.0);
  }
  
  matrix[0][1] = 2.0;
  matrix[0][2] = 3.0;
  matrix[1][2] = 2.0;
  
  BOOST_REQUIRE_NO_THROW(result = invertMatrix(matrix));
  
  BOOST_REQUIRE_EQUAL(result.size(), dim);
  for(unsigned i=0; i<dim; i++){
    BOOST_REQUIRE_EQUAL(result[i].size(), dim);
  }
  const double threshold = 1e-16;
  BOOST_CHECK_CLOSE(result[0][0], -1.0, threshold);
  BOOST_CHECK_CLOSE(result[0][1], 1.0, threshold);
  BOOST_CHECK_CLOSE(result[0][2], 1.0, threshold);
  BOOST_CHECK_CLOSE(result[1][0], 1.0, threshold);
  BOOST_CHECK_CLOSE(result[1][1], -2.0, threshold);
  BOOST_CHECK_CLOSE(result[1][2], 1.0, threshold);
  BOOST_CHECK_CLOSE(result[2][0], 0.0, threshold);
  BOOST_CHECK_CLOSE(result[2][1], 1.0, threshold);
  BOOST_CHECK_CLOSE(result[2][2], -1.0, threshold);
  
  // somehow this function only throws an exception if there is a 0 column (so no pivot element may be picked)
  // if a singular matrix without zero entries is given, no exception is thrown, but the invert matrix is wrong
  // (contains some high values)
  BOOST_CHECK_THROW(invertMatrix(singularMatrix), SingularMatrixException);
  
}

BOOST_AUTO_TEST_SUITE_END()