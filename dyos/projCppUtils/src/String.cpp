/** 
* @file String.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* string utilities not contained in std::string                        \n
* =====================================================================\n 
* @author Tjalf Hoffmann       
* @date 18.7.2011
*/

#include <string>
#include "String.hpp"

using namespace std;
using namespace utils;

/**
 * @brief converting all characters of a string to uppercase letters
 * @param s string to be converted
 */
void utils::toUpperCase(string &s)
{
  for(unsigned int i=0; i<s.length(); ++i){
    s[i] = char(toupper(s[i]));
  }
}

/**
 * @brief converting all characters of a string to lowercase letters
 * @param s string to be converted
 */
void utils::toLowerCase(string &s)
{
  for(unsigned int i=0; i<s.length(); ++i){
    s[i] = char(tolower(s[i]));
  }
}

/**
* @brief compares to strings
*
* This function is meant to compare strings case insensitive, but case sensitive comparison is also possible
* @param first string to be compared compare
* @param second second string to be compared
* @param caseSensitive flag the define whether strings are to be compared case sensitive - default: false
* @return true, if strings are equal, false otherwise
*/
bool utils::compareStrings(std::string first, std::string second, const bool caseSensitive)
{
  if(!caseSensitive){
    toUpperCase(first);
    toUpperCase(second);
  }
  return first.compare(second)==0;
}

