/** @file Spline.cpp
*    @brief implementation of Spline class
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
*    =====================================================================\n
*
*   @author Klaus Stockmann
*   @date 08.03.2012                                       
*/

#include <cassert>
#include <cstdio>
#include <vector>

#include "Spline.hpp"


/** @brief constructor
  *
  * constructor for Spline class. Spline is constructed with simple knots only (no knot duplicity, save for
  * the first and the last knot, depending on the spline order).
  * The timepoints must be scaled onto the closed interval [0.0, 1.0], i.e. the first timepoint must be t[0]=0.0 
  * and the last timepoint t[end] = 1.0. The number of timepoints must be equal to the number of values. In the
  * current implementation, the order is restricted to 1 (piecewise constant) and 2 (piecewise linear).
  *
  * @param[in] t vector with timepoints t = [0, .., 1.0]
  * @param[in] x vector with values
  * @param[in] order spline order (currently only supported piecewise constant: 1, piecewise linear: 2)  
  */ 
Spline::Spline(const std::vector<double> &t, std::vector<double> &x, const unsigned order)
{
  initializeSpline(t, x, order);
}

/** @brief copy constructor
  *
  * @param[in] splineIn object of class Spline to be copied
  */  
Spline::Spline(const Spline &splineIn)
{ 
  m_order = splineIn.m_order;
  m_t = splineIn.m_t;
  m_x = splineIn.m_x;
  m_knots = splineIn.m_knots;
}  
 
//! @brief destructor
Spline::~Spline()
{
  
}







/**
* @brief evaluates a spline s = sum(c_i * phi_i) at timepoint t. Works with arbitrary spline order. Constraints timepoint: 0 <= t 1.0 <= 
*
* @param[in] tp timepoint at which spline is evaluated
* @return spline value
*/
double Spline::eval(const double tp)
{
  assert((tp >= 0.0) && (tp<=1.0)); // scaled onto closed interval [0.0, 1.0]
  
  double s = 0.0;

  unsigned splineDim = m_d.size(); // dimension of spline space ^= number of basis functions 

  
  /* evaluation */
  /* apply deBoor algorithm (see Deuflhard/Hohmann, Numerische Mathematik I, p. 253) */
  for(unsigned i=1; i<=splineDim; i++)
    s += m_d[i-1] * basisSplineW(tp, i, m_order, m_knots);

  return s;
}

/**
* @brief evaluates derivative of a spline w.r.t a spline coefficient s = sum(c_i * phi_i) at timepoint t. Works with arbitrary spline order. Constraints timepoint: 0 <= t <= 1.0 
*
* evaluates derivative of a spline w.r.t a spline coefficient c_i: ds/dc_i (t) = d(sum( c_i * phi_i(t) ))/dc_i = phi_i(t) 
*
* @param[in] tp timepoint at which spline is evaluated
* @param[in] splineIndex index of basis spline
* @return derivative value
*/
double Spline::dSpline(const double tp, const unsigned splineIndex)
{
  assert((tp >= 0.0) && (tp<=1.0)); // scaled onto closed interval [0.0, 1.0]
  
  // assert that splineIndex is in correct range
  unsigned minSplineIndex = 1; // spline indices start with '1'
  unsigned maxSplineIndex = m_d.size(); // dimension of spline space, i.e. number of basis functions 
  assert( (splineIndex >= minSplineIndex) && (splineIndex <= maxSplineIndex) );


  //basis_splinew(Time, param_index, order, &knots_ext[0], nknots_ext);
  double dSpline = basisSplineW(tp, splineIndex, m_order, m_knots);

  return dSpline;
}


// private methods
/**
* @brief compute spline coefficients. Currently only for order = {1, 2}, i.e. piecewise constant and linear implemented
*
* for the currently implemented spline orders 1 and 2, computing the spline coefficients is trivial, because d[i] = x[i].
* Mind, however, that for order == 1 the number of spline coefficients is x.size()-1, while for order == 1 it is equal to x.size()
* In the general case order > 2, we would have to solve a banded linear equation system
*
* @param[in] t timepoints
* @param[in] x values at timepoints
* @param[in] order spline order; Currently only 1 and 2, i.e. piecewise constant and linear implemented
* @return spline coefficients
*/
std::vector<double>& Spline::getSplineCoefficients(const std::vector<double> &t, const std::vector<double> &x, const unsigned order)
{
  assertSplineInput(t, x, order);
  
  unsigned splineDim; // dimension of spline space, in general: splineDim = #knots - order (with #knots the number of extended knot sequence)

  if (order == 1) splineDim = x.size()-1;
  else if (order == 2) splineDim = x.size();
  else assert(1==0);

  static std::vector<double> d;
  d.resize(splineDim);

  for (unsigned i=0; i<splineDim; i++) d[i] = x[i];

  return d;
}


/**
* @brief wrapper for method basisSpline which evaluates a basis spline phi_i at timepoint t. Works with arbitrary spline order.
*
* @param[in] t timepoint at which basis spline is evaluated
* @param[in] i index of basis spline
* @param[in] k order of basis spline
* @param[in] knots vector with knots
* @return spline value
*/
double Spline::basisSplineW(const double t, const unsigned i, const unsigned k, const std::vector<double> &knots)
{
  unsigned nknots = knots.size();

  double N_ikw = basisSpline(t, i, k, knots);

  /* falls intervallende und letzte Basisfunktion, dann 1.0 setzen */
  if (i == (nknots - k)) {
    if (t == knots[nknots-1]) N_ikw=1.0;
  }

  return N_ikw;
}

/**
* @brief evaluate a basis spline phi_i (recursively) at timepoint t.  Works with arbitrary spline order.
*
* @param[in] t timepoint at which basis spline is evaluated
* @param[in] i number of spline
* @param[in] k spline order (piecewise constant: 1, piecewise linear: 2, etc.)
* @param[in] knots vector with knots
* @return spline value
*/

double Spline::basisSpline(const double t, const unsigned i, const unsigned k, const std::vector<double> &knots)
{
  double N_ik;
  double coeff1, coeff2;
  double spline1, spline2;


  if (k==1) {
    if ((knots[i-1] <= t) && (t < knots[i])) N_ik = 1.0;
    else                                     N_ik = 0.0;
  }
  else {
    if ((knots[i+k-2]-knots[i-1]) == 0)  coeff1 = 0.0;
    else                                 coeff1 = (t-knots[i-1])/(knots[i+k-2]-knots[i-1]);
    
    if ((knots[i+k-1]-knots[i]) == 0) coeff2= 0.0;
    else                              coeff2= (knots[i+k-1] - t)/(knots[i+k-1]-knots[i]);
    
    spline1 =  basisSpline(t,i,(k-1),knots);
    spline2 =  basisSpline(t,(i+1),(k-1),knots);
    N_ik = coeff1* spline1+ coeff2 * spline2;
  }
  return N_ik;
}


/**
* @brief assert consistency of input from which spline is constructed (timepoints, values, order)
*
* @param[in] t timepoints 
* @param[in] x values at t
* @param[in] order spline order (piecewise constant: 1, piecewise linear: 2, etc.)
* @note currently only spline orders 1 and 2, i.e. piecewise constant and linear splines are implemented
*/
void Spline::assertSplineInput(const std::vector<double> &t, const std::vector<double> &x, const unsigned order)
{
  assert(t.size() == x.size());
  assert(t.size() >= 2); // valid for all values of order?
  assert((t[0] == 0.0) && (t[t.size()-1] == 1.0)); // time must be scaled onto [0.0, 1.0], better use approximate comparison and if no exact match put t[0]=0.0, t[end] = 1.0
  assert( (order == 1) || (order == 2)); // currently only piecewise constant and linear splines implemented
}

void Spline::initializeSpline(const std::vector<double> &t, const std::vector<double> &x, const unsigned order)
{
  assertSplineInput(t, x, order);

  m_t = t;
  m_x = x;
  m_order = order;

  // compute extended knot sequence. Depending on the spline order, first and last knot have multiplicity. 
  // This is the difference between timepoints and knot sequence
  unsigned nt = t.size(); // number of timepoints
  unsigned nknots = (nt - 2) + 2*order; // number of knots
  m_knots.resize(nknots);
  //unsigned n = nknots - order; // dimension of spline space

  for (unsigned i=0; i<order; i++)                 m_knots[i] = t[0];
  for (unsigned i=(nt + order - 2); i<nknots; i++) m_knots[i] = t[nt-1];
  for (unsigned i=order; i<(nt + order - 2); i++)  m_knots[i] = t[i-order+1];

  m_d = getSplineCoefficients(t, x, order);
}