/**  @file SignalAnalysis.cpp
*    @brief source for SignalAnalysis class. 
*    This class is required for e.g. the adaptation of the control grid in DyOS
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 01.06.2012
*/

#include <math.h>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cassert>
#include <iostream>
#include <fstream>

#include "SignalAnalysis.hpp"
#include "WaveletTransform.h"
#include "UtilityFunctions.hpp"

using namespace std;

bool comparator ( const std::pair<int,double>& l, const std::pair<int,double>& r)
{
  return l.second < r.second;
}


/** @brief constructor that fills the class with grid and values
*   @param[in] type either piecewise constant or linear
*   @param[in] upperBound is the upper bound of the signal in the time domain
*   @param[in] lowerBound is the lower bound of the signal in the time domain
*   @param[in] thresholdedIndices are the indices which should not be included during refinement
*   @param[in] maxRefinementLevel maximum level of refinement. Usually 10
*   @param[in] minRefinementLevel minimum level of refinement. Usually 3
*   @param[in] horRefinementDepth horizontal refinement depth. Depth of wavelets considered for refinement in horizontal direction.
*   @param[in] verRefinementDepth vertical refinement depth. Depth of wavelets considered for refinement in vertical direction.
*   @param[in] etres constant that marks wavelets for elimination.
*   @param[in] epsilon constant that marks wavelets for insertion.
*/
SignalAnalysis::SignalAnalysis(const int type,
                               const double upperBound,
                               const double lowerBound,
                               const std::vector<int> thresholdedIndices,
                               const int maxRefinementLevel,
                               const int minRefinementLevel,
                               const int horRefinementDepth,
                               const int verRefinementDepth,
                               const double etres,
                               const double epsilon)
{
  m_etres = etres;
  m_epsilon = epsilon;
  m_type = type;
  m_maxRefinementLevel = maxRefinementLevel;
  m_minRefinementLevel = minRefinementLevel;
  m_upperBound = upperBound;
  m_lowerBound = lowerBound;
  m_thresholdedIndices = thresholdedIndices;
  m_horizontalDepth = horRefinementDepth;
  m_verticalDepth = verRefinementDepth;
  setDimensions();
}
/**
* @brief performs wavelet analysis and obtains new mesh
* @param[in] timeGrid initial grid in time domain for which will be analyzed
* @param[in] timeValues values of the grid points in the time domain
*/
void SignalAnalysis::obtainNewMesh(const vector<double> timeGrid,
                                   const vector<double> timeValues)
{
  /* step 1 Get wavelet compatible grid and indexset associated to this grid------------*/
  vector<double> roundedGrid = roundOntoWaveletGrid(timeGrid);
  int order = m_type; // value taken from old DyOS
  // if the value m_maxRefinementLevel is large, then this function is not doing much. But we keep it anyway! faas
  vector<double> roundedValues = utils::interp(&timeGrid[0], &timeValues[0],
      static_cast<int>(timeGrid.size()), &roundedGrid[0],
      static_cast<int>(roundedGrid.size()), order);

  const vector<int> inputWaveletIndices = gridPointsToIndices(roundedGrid);
  vector<double> multiScaleVals = transformTimeToWaveletScale(roundedGrid, roundedValues);

  vector<double> inputCoefficients(roundedGrid.size());
  for(unsigned i=0; i<inputCoefficients.size(); i++){
    inputCoefficients[i] = multiScaleVals[static_cast<size_t>(inputWaveletIndices[i] - 1)];
  }
  m_inputCoef = inputCoefficients;
  m_inputInd = inputWaveletIndices;
  // this function changes m_thresholdedIndices. multiScaleVals is also changed
  vector<int> waveletSetAfterElimination = eliminateWavelets(multiScaleVals, inputWaveletIndices);
  /* ---------------------------------------------------------- */
  /*  STEP 5 IN THE ALGORITHM                                   */
  /* ---------------------------------------------------------- */
  vector<int> boundaryWavelets = getBoundaryIndices(inputWaveletIndices,
                                                    m_thresholdedIndices);

  // get the number of nonzeros of the Randwerte of the thresholded coefficient vector  //
  int numNonZeros = getNumNonZeroEntriesInBoundary(multiScaleVals, boundaryWavelets);

  vector<double> boundaryWaveletCoefficients(boundaryWavelets.size());
  for (unsigned i=0; i<boundaryWavelets.size(); i++) {
    boundaryWaveletCoefficients[i] = multiScaleVals[static_cast<size_t>(boundaryWavelets[i]-1)];
  }

  vector<double> coefficientsKeep;
  vector<int> waveletIndicesKeep;
  approximateSignal(boundaryWaveletCoefficients,
                    boundaryWavelets,
                    numNonZeros,
                    coefficientsKeep,
                    waveletIndicesKeep);

  // this variable is not used. It clarifies what happened.
  // int numberOfDroppedCoefficients = boundaryWaveletCoefficients.size() - coefficientsKeep.size();     /* number of dropped coefficients */

  vector<int> significantWavelets = getSignificantWavelets(waveletIndicesKeep,
                                                           inputWaveletIndices,
                                                           waveletSetAfterElimination);
  vector<double> wCoefficients(significantWavelets.size());
  for (unsigned i=0; i<significantWavelets.size(); i++){
    wCoefficients[i] = multiScaleVals[static_cast<size_t>(significantWavelets[i]-1)];
  }

  setRefinedGridandValues(significantWavelets, roundedGrid, roundedValues);
}
/**
* @brief round grid points onto wavelet grid
*  round an arbitrary input grid onto a dyadic (wavelet) grid with equidistant points
*  (with appr. 2^levelmax points)
*  @param[in] timeGrid can be any time grid which should be converted into a dyadic one
* @return a dyadic time grid which corresponds to the non dyadic input timeGrid.
*/
vector<double> SignalAnalysis::roundOntoWaveletGrid(const vector<double>& timeGrid) const
{
  size_t gridSize = timeGrid.size();
  assert(timeGrid.back() == 1.0);
  if(m_type == 1){
    gridSize--;
  }

  vector<double> roundedGrid(gridSize);
  for (size_t i=0; i<gridSize; i++){
    /* STEP 1 */
    /* determine position (range: 1...nmax)*/
    double div = timeGrid[i] / m_hmin;
    int position = 0;
    if (div - static_cast<int>(div) < 0.5){
      position = static_cast<int>(div) + 1;
    }
    else{
      position = static_cast<int>(div)  +2;
    }
    int gridratnum = position - 1;
    roundedGrid[i] = static_cast<double>(gridratnum) /  static_cast<double>(m_gridratdenom);
  }
  // ueberpruefen, ob doppelte Eintraege vorhanden in timeGrid 
  sort( roundedGrid.begin(), roundedGrid.end() );
  unique( roundedGrid.begin(), roundedGrid.end() );
  return roundedGrid;
}
/** @brief determines the significant wavelets from the 3 subsets given by the user. If a minimum grid is not fulfilled, it fills up to the
  *        minimum grid
  * @param[in] waveletIndicesKeep this are the indices that we want to keep after we applied the elimination
  * @param[in] inputWaveletIndices this is the index set of the original signal. So no manipulation took place
  * @param[in] waveletSetAfterElimination contains the indices of those wavelets after the elimination procedure
  */
vector<int> SignalAnalysis::getSignificantWavelets(const vector<int>& waveletIndicesKeep,
                                                   const vector<int>& inputWaveletIndices,
                                                   const vector<int>& waveletSetAfterElimination)const
{
  vector<int> allNeighbors = getNeighborsForSeveralLayers(waveletIndicesKeep);
  vector<int> unitedNeighbors = uniteNeighbors(inputWaveletIndices, allNeighbors);

  size_t sizeOfSignificantWavelets = waveletSetAfterElimination.size()
                                + ( unitedNeighbors.size() - inputWaveletIndices.size() );

  vector<int> significantWavelets(sizeOfSignificantWavelets);
  for (unsigned i=0; i<waveletSetAfterElimination.size(); i++)
    significantWavelets[i] = waveletSetAfterElimination[i]; /* waveletSetAfterElimination contains significant wavelets of input signal */

  // comment taken from old DyOS (Voice from the past)
  // significantWavelets contains original wavelet coeffs plus  neighbors of biggest randwerte 
  // This means, that an index which was eliminated through a thresholded can not be included later on.
  // significantWavelets enthaelt DIE Nachbarn der groessten Randwerte,
  // German comment kept for clarification.
  // die nicht im (ungethresholdetem) Inputvector enthalten waren 
  // das BEDEUTET: ein Index, der per threshold rausgeflogen ist, kann
  // nicht per Nachbareinfuegen wieder mit hinzugenommen werden
    for (unsigned i=0;i < (unitedNeighbors.size() - inputWaveletIndices.size()); i++)
    significantWavelets[waveletSetAfterElimination.size() + i] =
                        unitedNeighbors[inputWaveletIndices.size() + i];

  sort(significantWavelets.begin(), significantWavelets.end());
  unique( significantWavelets.begin(), significantWavelets.end());

  /*  Code to ensure that the coarsest returned grid corresponds */
  /*   to wavelets with indices 1 through m_nmin */
  int minNumber = 0;
  for (unsigned i=0;i<significantWavelets.size(); i++) {
    if (significantWavelets[i] <= m_nmin){
      minNumber = minNumber + 1;
    }else{
      break;
    }
  }
  if (minNumber != m_nmin) {
    significantWavelets =  fillToMinimumGrid(significantWavelets); //F2MG
  }
  /* end of code ensuring a minimum grid  */
  return significantWavelets;
}

/** @brief creates from the significant wavelets the refined grid and overwrites the members m_refinedGrid and m_refinedValues
  * @param[in] significantWavelets contain those wavelets with significant coefficients. So we includ grid points there
  * @param[in] roundedGrid is the grid which was rounded on a dyadic grid. This is the grid prior to starting the refinement
  * @param[in] roundedValues are the values correspondingg to the roundedGrid.
  */
void SignalAnalysis::setRefinedGridandValues(const vector<int>& significantWavelets,
                                             const vector<double>& roundedGrid,
                                             const vector<double>& roundedValues)
{
  m_refinedGrid.clear();
  m_refinedValues.clear();

  m_refinedGrid = indicesToGridPoints(significantWavelets);
  int order = m_type;

  m_refinedValues = utils::interp(&roundedGrid[0], &roundedValues[0],
      static_cast<int>(roundedGrid.size()),
      &m_refinedGrid[0], static_cast<int>(m_refinedGrid.size()), order);

  if(m_type == 1){
    m_refinedGrid.push_back(1.0);
    m_refinedValues.push_back(m_refinedValues.back());
  }

  /* make sure that interpolated control variables are not violating the bounds */
  for(unsigned i=0; i<m_refinedValues.size(); i++) {
    if (m_refinedValues[i] < m_lowerBound){
      m_refinedValues[i] = m_lowerBound;
    }else if (m_refinedValues[i] > m_upperBound){
      m_refinedValues[i] = m_upperBound;
    }
  }
}
/**
* @brief convert grid points to corresponding wavelet indices
*
* convert grid points to corresponding indices
* @param[in] timeGrid is dyadic time grid which should be converted into wavelet indices
* @return index set with the wavelet indices
*/
vector<int> SignalAnalysis::gridPointsToIndices(const vector<double>& timeGrid)const
{
  size_t ngrid = timeGrid.size();
  vector<int> indexSet(ngrid);
  for (unsigned j=0; j<ngrid; j++) {
    // this should always be an int.
    //! @todo check if the devision leads to an int
    int gridratnum = int(timeGrid[j] / m_hmin);

    switch (m_type) {
     case 1:  /*(Haar functions)*/
       indexSet[j] = gridPointsToIndicesHaar(gridratnum);
       break;
     case 2:  /* (hat functions)*/
       indexSet[j] = gridPointsToIndicesHut(gridratnum);
       break;
    } /* end of switch statement */
  } /* end of main for-loop*/
  /* indexset sortieren */
  sort(indexSet.begin(), indexSet.end());
  return indexSet;
}

/**
* @brief convert grid points to corresponding wavelet indices for Haar
*
* @param[in] gridratnum; I don't know what this is
* @return index of corresponding time point
*/
int SignalAnalysis::gridPointsToIndicesHaar(const int gridratnum)const
{
    /*STEP 3
    now let's get the wavelet indices i (translation-) and j (skale index)*/

  int waveIndScale, waveIndTranslat;
  if (gridratnum == 0) {    /*nur wenn der punkt (0/0) der ersten Skalenfunktion zugeordnet ist*/
    /* wie z. B. bei den Haarfunktionen*/
    waveIndScale=1;
    waveIndTranslat=1;
  }
  else {
    int gcd = 1 << utils::numBinaryZeros(gridratnum); /* ==2^numBinaryZeros*/
    int dummynum = gridratnum / gcd;
    int dummydenom = m_gridratdenom / gcd;
    waveIndTranslat = (dummynum + 1) / 2;
    waveIndScale = utils::numBinaryZeros(dummydenom) + 1; /* ==log2(dummydenom)+1*/
  }

  int offset = 0;
  /* STEP 4
  determine indexset*/
  if (waveIndScale == 1) offset = 0;
  else                   offset = (1<<(waveIndScale-2));

  return offset + waveIndTranslat;
}

/**
* @brief convert grid points to corresponding wavelet indices for Hut
*
* @param[in] gridratnum; I don't know what this is. faas
* @return index of corresponding time point
*/
int SignalAnalysis::gridPointsToIndicesHut(const int gridratnum)const
{
    /*STEP 3
    now let's get the wavelet indices i (translation-) and j (skale index)*/
  int waveIndScale, waveIndTranslat;
  /* erstes if und die zwei else ifs gelten nur fuer hutfunktionen*/
  if (gridratnum == 0) {      /* nur wenn der punkt (0/0) der ersten Skalenfunktion zugeordnet ist*/
    /* wie z. B. bei den Hutfunktionen*/
    waveIndScale = 1;
    waveIndTranslat = 1;
  }
  else if (2* gridratnum == m_gridratdenom) { /* 1/2*/
    /* wie z. B. bei den Hutfunktionen*/
    waveIndScale=1;
    waveIndTranslat = 2;
  }
  else if (gridratnum == m_gridratdenom) { /* 1/1*/
    /* wie z. B. bei den Hutfunktionen*/
    waveIndScale = 1;
    waveIndTranslat = 3;
  }
  else {
    int gcd = 1 << utils::numBinaryZeros(gridratnum); /* ==2^numBinaryZeros*/
    int dummynum = gridratnum / gcd;
    int dummydenom = m_gridratdenom / gcd;
    waveIndTranslat = (dummynum+1)/2;
    waveIndScale = utils::numBinaryZeros(dummydenom); /* ==log2(dummydenom)*/
  }

  assert(waveIndScale>0);
  /* STEP 4
  determine indexset*/
  int offset = 0;
  if (waveIndScale != 1) 
    offset = (1<<(waveIndScale-1))+1;

  return offset+ waveIndTranslat;
}

/**
* @brief converts wavelet indices to grid points 
* @param[in] Vector which contains the wavelet coefficients for the new proposed mesh.
* @return Vector which contains the points where the intervals for the new mesh start and end.
*/
vector<double> SignalAnalysis::indicesToGridPoints(const vector<int>& waveletIndices)const
{
  switch(m_type){
    case 1:
      return indicesToGridPointsHaar(waveletIndices);
    case 2:
      return indicesToGridPointsHut(waveletIndices);
    default:
      assert(false);
      vector<double> empty;
      return empty;
  }
}
/** @copydoc SignalAnalysis::indicesToGridPoints 
*   for haar wavelets
*/
vector<double> SignalAnalysis::indicesToGridPointsHaar(const vector<int>& waveletIndices)const
{
  size_t numWavelets = waveletIndices.size();
  vector<double> gridPoints(numWavelets);
  for(unsigned i=0; i<numWavelets; i++){
    if(waveletIndices[i] == 1){
      gridPoints[i] = 0.0;
    }else{
      double ddummy = (log(waveletIndices[i] - 1.0)/log(2.0));
      ddummy = utils::mround(ddummy); 
      // get wavelet-scale and -translation index from sequential index
      int JJ = static_cast<int>(ddummy) + 1;
      int KK = waveletIndices[i] - (1 << (JJ-1));   
      // get gridpoints associated to wavelet index
      gridPoints[i] = (2 * KK - 1.0) / (1 << JJ);
    }
  }
  for(unsigned i=0;i<numWavelets;i++)
    assert(gridPoints[i]>=0.0 && gridPoints[i]<=1.0);
  sort(gridPoints.begin(), gridPoints.end());
  return gridPoints;
}
/** @copydoc SignalAnalysis::indicesToGridPoints 
*   for hut wavelets
*/
vector<double> SignalAnalysis::indicesToGridPointsHut(const vector<int>& waveletIndices)const
{
  size_t numWavelets = waveletIndices.size();
  vector<double> gridPoints(numWavelets);
  for(unsigned i=0; i<numWavelets; i++){
    if (waveletIndices[i] == 1){
      gridPoints[i] = 0.0;
    }else if (waveletIndices[i] == 2){
      gridPoints[i] = 0.5;
    }else if (waveletIndices[i] == 3){
       gridPoints[i] = 1.0;
    }else {
      int index = waveletIndices[i];
      // get wavelet-scale and -translation index from sequential index 
      double ddummy = utils::mround(log(index-1.0 -1.0)/log(2.0)); 
      int JJ = static_cast<int>(ddummy);
      int KK = (index-1) - (1 << (JJ - 1 + 1) ) + 1 - 1;
      // get gridpoints associated to wavelet index 
      gridPoints[i] = (2 * KK - 1.0) / (1 << (JJ + 1));
    }
  }
  for(unsigned i=0; i<numWavelets; i++) 
    assert(gridPoints[i]>=0.0 && gridPoints[i]<=1.0);
  sort(gridPoints.begin(), gridPoints.end());
  return gridPoints;
}

/**
* @brief  returns several layers of neighbors of a given set of wavelet indices
*
* returns several layers of neighbors of a given set 
* of wavelet indices (up to a max. refinement scale ) 
* horizontal and vertical refinement depths may differ
* @param[in] currentLayer vector with current indices
* @return out vector with indices of new wavelet layer 
*/
vector<int> SignalAnalysis::getNeighborsForSeveralLayers(const vector<int>& currentLayer)const
{
  vector<int> allLayers, nextLayer, consideredLayer;
  int minhv = 0, height= 0, equal = 0;

  if (m_verticalDepth < m_horizontalDepth){
    minhv = m_verticalDepth;
  }else if (m_verticalDepth > m_horizontalDepth){ 
    minhv = m_horizontalDepth;
    height= 1;
  }else{
    minhv = m_horizontalDepth;
    equal = 1;
  }

  consideredLayer = currentLayer;

  // horizontal and vertical
  for (int j=1; j <= minhv; j++){
    nextLayer = getNextLayerIndices(consideredLayer, AllLayers);
    allLayers.insert(allLayers.end(), nextLayer.begin(), nextLayer.end() );
    consideredLayer = nextLayer;
  }
  if (equal == 0){
    // vertical
    if (height== 1){
      for (int j=(minhv+1); j<=m_verticalDepth; j++){
        nextLayer = getNextLayerIndices(consideredLayer, VerticalLayer);
        allLayers.insert(allLayers.end(), nextLayer.begin(), nextLayer.end() );
        consideredLayer = nextLayer;
      }
    }
    else{
      // horizontal
      for (int j=(minhv+1);j <= m_horizontalDepth; j++) {
        nextLayer = getNextLayerIndices(consideredLayer, HorizontalLayer);
        allLayers.insert(allLayers.end(), nextLayer.begin(), nextLayer.end() );
        consideredLayer = nextLayer;
      }
    } /* end if height==1 */
  } /* end if equal==0 */

  // select only indices smaller than NMAX
  using std::placeholders::_1;
  std::remove_if(allLayers.begin(), allLayers.end(), std::bind(greater<int>(), _1, m_nmax));

  return allLayers;
}
/**
* @brief get indices of next wavelet layer
* @param[in] currentLayer vector with indices of current wavelet layer
* @param[in] direction specifies direction (horizontal and vertical, only horizontal, or only vertical)
* @return indices of next wavelet layer
*/
vector<int> SignalAnalysis::getNextLayerIndices(const vector<int>& currentLayer,
                                                const LayerDirection direction)const
{
  vector<int> nextLayer, allLayers;
  for (unsigned i=0; i<currentLayer.size(); i++){
    if (direction == AllLayers) {
      nextLayer = getDirectNeighbors(currentLayer[i]);
    }
    else if (direction == HorizontalLayer) {
      nextLayer = getHorizontalNeighbors(currentLayer[i]);
    }
    else if (direction == VerticalLayer){
      nextLayer = getVerticalNeighbors(currentLayer[i]);
    }
    else {
      assert(false);
    }
    allLayers.insert(allLayers.end(), nextLayer.begin(), nextLayer.end());
  }
  sort(allLayers.begin(), allLayers.end());
  allLayers.erase( std::unique( allLayers.begin(), allLayers.end() ), allLayers.end() );

  return allLayers;
}

/**
* @brief returns the direct neighbors (as a set of indices) of a given wavelet (specified by its index); 
* @param[in] index index of wavelet whose neighbors are computed
* @return index set containing the neighbors of input wavelet ( 0<= dimension <= 4)
*/
vector<int> SignalAnalysis::getDirectNeighbors(const int index)const
{
   // all neighbors, only horizontal, only vertical
  vector<int> hNeighbors = getHorizontalNeighbors(index);
  vector<int> vNeighbors = getVerticalNeighbors(index);
  vector<int> neighbors(hNeighbors.size() + vNeighbors.size());
  merge(hNeighbors.begin(), hNeighbors.end(), vNeighbors.begin(), vNeighbors.end(), neighbors.begin());

  std::unique(neighbors.begin(), neighbors.end());
  sort(neighbors.begin(), neighbors.end());

  unsigned maxSize = 4;
  assert(neighbors.size() <= maxSize);
  return neighbors;
}
/**
* @brief returns the direct vertical neighbors (as a set of indices) of a given wavelet (specified by its index);
* @param[in] index index of wavelet whose neighbors are computed
* @return index set containing the vertical neighbors of input wavelet ( 0<= dimension <= 2)
*/
vector<int> SignalAnalysis::getVerticalNeighbors(const int index)const
{
  switch(m_type){
    case 1:
      return getVerticalNeighborsHaar(index);
    case 2:
      return getVerticalNeighborsHut(index);
    default:
      assert(false);
      vector<int> empty;
      return empty;
  }
}
/**
* @brief returns the direct horizontal neighbors (as a set of indices) of a given wavelet (specified by its index);
* @param[in] index index of wavelet whose neighbors are computed
* @return index set containing the horizontal neighbors of input wavelet ( 0<= dimension <= 2)
*/
vector<int> SignalAnalysis::getHorizontalNeighbors(const int index)const
{
  switch(m_type){
    case 1:
      return getHorizontalNeighborsHaar(index);
    case 2:
      return getHorizontalNeighborsHut(index);
    default:
      assert(false);
      vector<int> empty;
      return empty;
  }
}

/** @copydoc SignalAnalysis::getVerticalNeighbors 
*   Haar version
*/
vector<int> SignalAnalysis::getVerticalNeighborsHaar(const int index)const
{
  vector<int> neighbors;
  unsigned maxSize = 2;
  neighbors.reserve(maxSize);
  int scala;         /* scale on which input index lies */

  if (index == 1)
    scala=0;
  else {
    scala= static_cast<int>(log(index-1.0)/log(2.0)) + 1;   /*int(x)+1 ^= ceil(x), es sei denn x = int(x), */
                                                 /* deswegen (index-1.0) */
  }
  if (scala == 0) {
    neighbors.push_back(2);
  }
  if (scala == 1) {
    neighbors.push_back(3);
    neighbors.push_back(4);
  }
  if ((scala>1) && (scala< m_maxRefinementLevel)) {
    neighbors.push_back(2 * index - 1);
    neighbors.push_back(2 * index);
  }

  assert(neighbors.size() <= maxSize);
  return neighbors;
}

/** @copydoc SignalAnalysis::getVerticalNeighbors
*   Hut version
*/
vector<int> SignalAnalysis::getVerticalNeighborsHut(const int index)const
{
  vector<int> neighbors;
  unsigned maxSize = 2;
  neighbors.reserve(maxSize);
  int scala;        /* scale on which input index lies */
  int lira, rera;   /* left and right boundary of scala */

  if ((index >= 1) && (index<=3))
    scala=0;
  else {
    double dummy= ((index-1)-1)/2.0;
    scala=static_cast<int>(log(dummy)/log(2.0)) + 1;  /* int(x)+1 ^= ceil(x), es sei denn x=int(x) */
                                           /* deswegen (index-1.0) */
  }

  if (scala==0) lira = 1;
  else          lira = ((1<<(scala)) + 1) +1;
  rera = (1<<(scala+1)) + 1;

  if (scala==0) {
    if (index == 1) {
      neighbors.push_back(4);
    }

    if (index == 2) {
      neighbors.push_back(4);
      neighbors.push_back(5);
    }
    if (index == 3) {
      neighbors.push_back(5);
    }
  }
  if (scala > 0) {
    int modifiedIndex = index - 1;
    lira = lira - 1;
    rera = rera - 1;

    if ((scala>0) && (scala<m_maxRefinementLevel)) {
      neighbors.push_back(2 * modifiedIndex - 1);
      neighbors.push_back(2 * modifiedIndex );
    }

    for (unsigned i=0; i<neighbors.size(); i++){
      neighbors[i] += 1;
    }
  }
  return neighbors;
}

/** @copydoc SignalAnalysis::getHorizontalNeighbors 
*   Haar version
*/
vector<int> SignalAnalysis::getHorizontalNeighborsHaar(const int index)const
{
  vector<int> neighbors;
  unsigned maxSize = 2;
  neighbors.reserve(maxSize);
  // returns only horizontal neighbors 
  int scala;   // scale on which input index lies
  int lira=-1, rera=-1;  // left and right boundary of scala 

  if (index == 1){
    scala=0;
  }else {
    scala=static_cast<int>(log(index-1.0)/log(2.0)) + 1;  // int(x)+1 ^= ceil(x),  es sei denn x=int(x)
                                                // deswegen (index-1.0)
    lira=(1<<(scala-1))+1;
    rera=1<<scala;
  }

  if ((scala > 1) && (scala <m_maxRefinementLevel)) {
    if(index == rera) {        // wavelet am rechten Rand
      neighbors.push_back(index - 1);
    }
    else if (index == lira) {  // wavelet am linken Rand
      neighbors.push_back(index + 1);
    }
    else {                      // wavelet in der Mitte
      neighbors.push_back(index - 1);
      neighbors.push_back(index + 1);
    }
  }
  if (scala == m_maxRefinementLevel) {
    if(index == rera) {        //  wavelet am rechten Rand
      neighbors.push_back(index - 1);
    }
    else if (index == lira) {  // wavelet am linken Rand
      neighbors.push_back(index + 1);
    }
    else {                    // wavelet in der Mitte
      neighbors.push_back(index - 1);
      neighbors.push_back(index - 1);
    }
  }

  assert(neighbors.size() <= maxSize);
  return neighbors;
}
/** @copydoc SignalAnalysis::getHorizontalNeighbors 
*   Hut version
*/
vector<int> SignalAnalysis::getHorizontalNeighborsHut(const int index)const
{
  vector<int> neighbors;
  unsigned maxSize = 2;
  neighbors.reserve(maxSize);
  int scala;        /* scale on which input index lies */
  int lira, rera;   /* left and right boundary of scala */

  if ((index >= 1) && (index<=3)){
    scala=0;
  }else{
    double dummy= ((index-1)-1)/2.0;
    scala=static_cast<int>(log(dummy)/log(2.0)) + 1;  /* int(x)+1 ^= ceil(x), */
                                           /* es sei denn x=int(x) */
                                           /* deswegen (index-1.0) */
  }

  if (scala==0)
    lira=1;
  else
    lira= ((1<<(scala)) + 1) +1;
  rera=(1<<(scala+1)) + 1;

  if (scala==0) {
    if (index == 1) {
      neighbors.push_back(2);
    }

    if (index == 2) {
      neighbors.push_back(1);
      neighbors.push_back(3);
    }
    if (index == 3) {
      neighbors.push_back(2);
    }
  }
  if (scala > 0) {
    int modifiedIndex = index - 1;
    lira = lira-1;
    rera = rera-1;

    if ((scala>0) && (scala< m_maxRefinementLevel)) {
      if(modifiedIndex == rera) {        /* wavelet am rechten Rand */
        neighbors.push_back(modifiedIndex-1);
      }
      else if (modifiedIndex == lira) {  /* wavelet am linken Rand */
        neighbors.push_back(modifiedIndex+1);
      }
      else {                     /* wavelet in der Mitte */
        neighbors.push_back(modifiedIndex-1);
        neighbors.push_back(modifiedIndex+1);
      }
    }

    if (scala == m_maxRefinementLevel) {
      if(modifiedIndex == rera) {        /* wavelet am rechten Rand */
        neighbors.push_back(modifiedIndex-1);
      }
      else if (modifiedIndex == lira) {  /* wavelet am linken Rand */
        neighbors.push_back(modifiedIndex+1);
      }
      else {                     /* wavelet in der Mitte */
        neighbors.push_back(modifiedIndex-1);
        neighbors.push_back(modifiedIndex+1);
      }
    }
    for (unsigned i=0;i<neighbors.size();i++)
      neighbors[i] += 1;
  }

  assert(neighbors.size() <= maxSize);
  return neighbors;
}

/**
* @brief fill to minimum grid
*
* this routine ensures that the indexset resulting from sumesh contains at least all indices from 1 up to MIMG 
* @param[in] waveletIndices index set
* @return filled wavelet indices
*/
vector<int> SignalAnalysis::fillToMinimumGrid(const vector<int>& waveletIndices)const
{
  vector<int> minWaveletGrid;

  size_t minNumber = 0;
  for (unsigned i=0;i<waveletIndices.size();i++) {
    if (waveletIndices[i] <= m_nmin)
      minNumber ++;
    else
      break;
  }
  if(minNumber != static_cast<size_t>(m_nmin)){
    for (int index=1; index<=m_nmin; index++){
      minWaveletGrid.push_back(index);
    }
    for (size_t i = minNumber; i<waveletIndices.size(); i++){
      minWaveletGrid.push_back(waveletIndices[i]);
    }
  }
  return minWaveletGrid;
}

/**
* @brief special purpose set union function
*
* performs the union between the vertical neighbors (neighbors of the bigger elements) 
* and the original mesh (waveletIndices)
* @param[in] waveletIndices indices of current mesh
* @param[in] neighborIndices neighbour indices of the bigger elements
* @return contains the union of both indices
*/
vector<int> SignalAnalysis::uniteNeighbors(const vector<int>& waveletIndices,
                                           const vector<int>& neighborIndices)const
{
  vector<int> waveletsUnited;//(waveletIndices.size() + neighborIndices.size());
  waveletsUnited.reserve(waveletIndices.size() + neighborIndices.size());
  waveletsUnited.insert(waveletsUnited.begin(), waveletIndices.begin(), waveletIndices.end());
  // we only want to add those neighbors at the end of the vector of those elements that are not duplicates
  for(unsigned i=0; i<neighborIndices.size(); i++){
    bool duplicate = false;
    for(unsigned j=0; j<waveletIndices.size(); j++){
      if(waveletIndices[j] == neighborIndices[i]){
        duplicate = true;
        break;
      }
    }
    if(!duplicate){
      waveletsUnited.push_back(neighborIndices[i]);
    }
  }
  // check post-condition
//  int indmax = (1<<m_maxRefinementLevel) + 1;
//  for (unsigned i=0; i< waveletsUnited.size(); i++) {
//    assert(waveletsUnited[i] <= indmax);
//  }
  return waveletsUnited;
}

/* -------------------------------------------------------------------- */
/*  SUBROUTINE RANDWERTE, STEP 5 IN THE ALGORITHM */
/* -------------------------------------------------------------------- */

/**
* @brief returns 'RANDWERTE' without the thresholded wavelets that were eliminated in previous steps
* @param[in] originalWavelets vector with indices containing 'original wavelets'
* @param[in] thresholdWavelets vector with indices of thresholded wavelets
* @return vector with indices of 'randwerte' (dimension on input must be '0')
*/
vector<int> SignalAnalysis::getBoundaryIndices(const vector<int>& originalWavelets,  // RANDWERTE
                                               const vector<int>& thresholdWavelets)const
{
  vector<int> boundaryIndices;
  boundaryIndices.reserve(originalWavelets.size());

  /* Vereinigungsmenge von Vorgabemenge und Position der gethresholdeten */
  /* Funktionen bilden                                                   */
  vector<int> unionOfAllWavelets(originalWavelets.size() + thresholdWavelets.size());
  std::vector<int> tmpOriginalWavelets(originalWavelets);
  std::vector<int> tmpThresholdWavelets(thresholdWavelets);
  std::sort(tmpOriginalWavelets.begin(), tmpOriginalWavelets.end());
  std::sort(tmpThresholdWavelets.begin(), tmpThresholdWavelets.end());
  vector<int>::iterator it = set_union(tmpOriginalWavelets.begin(), tmpOriginalWavelets.end(),
                                       tmpThresholdWavelets.begin(), tmpThresholdWavelets.end(),
                                       unionOfAllWavelets.begin());
  unionOfAllWavelets.erase(it, unionOfAllWavelets.end());

  for (unsigned i=0; i<originalWavelets.size(); i++) {
    bool hasFreeNeighbor = true;
    int counter = 0;
    vector<int> neighbors = getDirectNeighbors(originalWavelets[i]);
    for(unsigned j=0; j<neighbors.size(); j++) {
      for(unsigned k=0; k<unionOfAllWavelets.size(); k++) {
        if(neighbors[j] == unionOfAllWavelets[k]){
          counter += 1;
        }
      }
    }
    if (counter == int(neighbors.size())){
      hasFreeNeighbor = false;
    }

    if(hasFreeNeighbor) {
      boundaryIndices.push_back(originalWavelets[i]);
    }
  }
  return boundaryIndices;
}

/**
* @brief perform the approximation of a given signal
*
* perform the approximation of a given signal specified by a set of wavelet indices 
* and their associated wavelet coefficients. Input variable 'epsilon' specifies how approximation.
* works: \n
* if 0.0<epsilon<1.0: error level of the new signal; number of discarded wavelets depends on input signal\n
* if epsilon>=1.0: a fixed number of 'epsilon' wavelets is discarded\n
* Remark: though this function is generic, the usage of 'approximateSignal' in this module is such that the input 
* wavelet base consists of the boundary layer wavelets
* @param[in] inputCoefficients wavelet coefficients of the input signal
* @param[in] inputIndices wavelet indices of the input signal
* @param[in] nnzra if m_epsilon > 1, then nnzra is an upper limit for konst. 
* @param[out] coefficients wavelet coefficients of the approximated signal
* @param[out] indices wavelet indices of the approximated signal
* nnzra is used to assure that only boundary layer wavelets are taken into account
*/
void SignalAnalysis::approximateSignal(const vector<double> &inputCoefficients,
                                       const vector<int> &inputIndices,
                                       const int nnzra,
                                       vector<double> &coefficients,
                                       vector<int> &indices)const
{
  assert(coefficients.size() == 0);
  assert(indices.size() == 0);

  double checksum = 0.0;
  // by definition, if konst==0.0 then no grid refinement shall be performed
  // approximateSignal is also left, if all wavelet coefficient are identically zero
  for(unsigned i=0; i<inputCoefficients.size(); i++){
    checksum += fabs(inputCoefficients[i]);
  }
  if (m_epsilon == 0.0 || checksum == 0.0){
    return;
  }

  vector<double> absoluteCoefficients(inputCoefficients.size()); // absolute values of input vector, sorted in ascending order
  /* sort absolute values of input vector de in ascending order*/
  for(unsigned i=0; i<inputCoefficients.size(); i++){
    absoluteCoefficients[i]= fabs(inputCoefficients[i]);
  }
  double sumAbsCoefficients = accumulate(absoluteCoefficients.begin(), absoluteCoefficients.end(), 0.0);
  double threshold = m_epsilon * sumAbsCoefficients;
  // we want to sort the vector and get the corresponding index
  // thus we have to use pairs.
  vector<std::pair<int,double> > sortCoefficients(absoluteCoefficients.size());
  vector<std::pair<int,double> >::iterator it;
  size_t j = 0;
  // first we assign the values
  for(it = sortCoefficients.begin(), j = 0; it!=sortCoefficients.end() &&
      j<sortCoefficients.size(); it++, j++){
    it->first = static_cast<int>(j)+1;
    it->second = absoluteCoefficients[j];
  }
  // sort absolute values in ascending order
  sort(sortCoefficients.begin(), sortCoefficients.end(),comparator);
  vector<int> reverseIndices(absoluteCoefficients.size()); // index vector , entries in range 1..sde (not 0.. sde-1)
  for(it = sortCoefficients.begin(), j=0; it!=sortCoefficients.end() && j<sortCoefficients.size(); it++, j++){
    reverseIndices[j] = it->first;
    absoluteCoefficients[j] = it->second;
  }

  // reverse order
  reverse(absoluteCoefficients.begin(), absoluteCoefficients.end());
  reverse(reverseIndices.begin(), reverseIndices.end());

  /* select n largest values, where n is the smallest number for which it holds that
     the partial sum of the n largest values is greater than or equal the threshold (sum_de_abs * konst) */
  double partialSumCoeffienctsAbs = 0;
  if (m_epsilon < 1.0){
    while (partialSumCoeffienctsAbs <= threshold) {
      partialSumCoeffienctsAbs += absoluteCoefficients[indices.size()];
      coefficients.push_back(inputCoefficients[static_cast<size_t>(reverseIndices[indices.size()]-1)]);    // select coefficients to keep
      indices.push_back(inputIndices[static_cast<size_t>(reverseIndices[indices.size()]-1)]);  // select corresponding wavelet base
    }
  }

  /* select n=konst largest values*/
  if (m_epsilon >= 1.0){
    // if konst>sde, reset konst=sde
    int iEpsilon = static_cast<int>(m_epsilon);
    if (iEpsilon > int(inputCoefficients.size())){
      iEpsilon = int(coefficients.size());
    }
    /* take only nonzeros of boundary layer into account */
    if (iEpsilon > nnzra){
      iEpsilon = nnzra;
    }
    for(int i=0; i<iEpsilon; i++){
      coefficients.push_back(inputCoefficients[static_cast<size_t>(reverseIndices[indices.size()]-1)]);    // select coefficients to keep
      indices.push_back(inputIndices[static_cast<size_t>(reverseIndices[indices.size()]-1)]);  // select corresponding wavelet base
    }
  }
}

/**
* @brief  eliminate those wavelets from index set whose coefficients fall below a threshold
*
* eleminates the functions whose wavelet coefficients are under the threshold constant 'ETRES'
* @param[in, out] waveCoefficients vector with wavelet coefficients of the interpolated signal. On output, 
* coefficients below the threshold will be replaced by '0.0'.
* @param[in] waveIndices vector of wavelet indices. On input contains wavelet indices of current 
* solution. 
* @return On output contains indices ot the significant wavelets only. All other indices are set to '0' 
*/
vector<int> SignalAnalysis::eliminateWavelets(vector<double>& waveCoefficients,
                                              const vector<int>& waveIndices)
{
  vector<int> significantWaves = waveIndices;
  // removedIndices vector with indices of functions which are removed
  vector<int> removedIndices;
  removedIndices.reserve(waveCoefficients.size());
  size_t numWavelets = waveIndices.size();
  for (size_t i=0; i<numWavelets; i++){
    double value = fabs(waveCoefficients[static_cast<size_t>(waveIndices[i] - 1)]);
    if (value < m_etres){
      removedIndices.push_back(significantWaves[i]);
      waveCoefficients[static_cast<size_t>(significantWaves[i]-1)] = 0;
      significantWaves[i] = 0 ;
    }
  }
  size_t counter = 0;
  for (size_t i=0; i< numWavelets; i++){
    if (significantWaves[i] != 0){
      significantWaves[counter] = significantWaves[i];
      counter++;
    }
  }
  significantWaves.resize(counter);

  // we merge the wavelets that should be eliminated in this iterations by the ones from any previous one
  m_thresholdedIndices.insert(m_thresholdedIndices.end(),removedIndices.begin(), removedIndices.end() );
  // erase all non unique entries
  std::unique(m_thresholdedIndices.begin(), m_thresholdedIndices.end());

  return significantWaves;
}


/** @brief sets necessary dimension for the calculation of the wavelet grids.
*   This is only called in the constructor
*/
void SignalAnalysis::setDimensions()
{
  switch (m_type) {
case 1:  /* haar wavelets */
  m_nmax = 1 << (m_maxRefinementLevel); 
  m_hmin = 1.0 / m_nmax;
  m_gridratdenom = m_nmax;
  m_nmin = 1 << m_minRefinementLevel;
  break;

case 2:    /* hut wavelets */
  m_nmax = (1 << m_maxRefinementLevel) + 1; /*==2^levelmax + 1*/
  m_hmin = 1.0 / (m_nmax - 1);
  m_gridratdenom = m_nmax-1;
  m_nmin = (1 << m_minRefinementLevel) + 1;
  break;
  //case 4:    /*employ hut wavelets for decomposition of piecewise cubic signal*/
  //  nmax=(1<<m_maxRefinementLevel) +1; /*==2^levelmax + 1*/
  //  hmin=1./(nmax-1);
  //  gridratdenom= nmax-1;
  //  break;
default:
  // we should never get here
  assert(false);
  }
}
/** @brief create equidistant mesh of dimension m_nmax 
*   @return equidistant fine mesh
*/
vector<double> SignalAnalysis::createFinestMesh()const
{
  vector<double> fineGrid(static_cast<size_t>(m_nmax));
  if(m_type == 1){
    for(int i=0; i<m_nmax; i++){
      fineGrid[static_cast<size_t>(i)] = i * (1.0 / m_nmax);
    }
  }else{
     for(int i=0; i<m_nmax; i++){
      fineGrid[static_cast<size_t>(i)] = i * (1.0/(m_nmax-1));
    }
  }
  return fineGrid;
}


/** @brief getter function for attribute m_refinedGrid
*   @returns a copy of the vector
*/
vector<double> SignalAnalysis::getRefinedGrid()const
{
  return m_refinedGrid;
}
/** @brief getter function for attribute m_refinedValues
*   @returns a copy of the vector
*/
vector<double> SignalAnalysis::getRefinedValues()const
{
  return m_refinedValues;
}

/** @brief transforms the time domain into the wavelet domain
*   @param[in] roundedGrid the dyadic grid proints
*   @param[in] roundedValues the corresponding values of to the roundedGrid
*   @return multi scale values
*/
vector<double> SignalAnalysis::transformTimeToWaveletScale(const vector<double> &roundedGrid,
                                                           const vector<double> &roundedValues)const
{
  vector<double> finestMesh = createFinestMesh();
  int order = m_type;
  // interpolate given signal on the finest mesh. The grid always needs time point 1.0. In case it does not exist, we will add it.
  vector<double> roundedGridCopy = roundedGrid;
  if(roundedGridCopy.back() != 1.0){
    roundedGridCopy.push_back(1.0);
  }
  vector<double> multiScaleVals = utils::interp(&roundedGridCopy[0], &roundedValues[0],
      static_cast<int>(roundedValues.size()), &finestMesh[0],
      static_cast<int>(finestMesh.size()), order);
  /* transformation Zeit->Einzelsk. bereich */
  /* !!! was passiert wenn JNUA ungerade ist ??? */
  for(size_t i=0; i < static_cast<size_t>(m_nmax); i++){
    multiScaleVals[i] = multiScaleVals[i] / (1<<(m_maxRefinementLevel/2));
  }
  /* transformation Einzelsk. bereich -> Multiskalenbereich */
  if (m_type == 1){
    haar_fwt(&multiScaleVals[0], static_cast<int>(multiScaleVals.size()), 1);
  }else{
    hat_fwt(&multiScaleVals[0] - 1, static_cast<int>(multiScaleVals.size()), 1); /* -1 wg. fortran gemaesser Indizierung in hat_fwt */
  }
  return multiScaleVals;
}
/** @brief counts the number of nonzero entires of the boundary wavelets
  * @param[in] multiScaleVals the multi scale values from the fwt. They should be already altered by the eliminate function
  * @param[in] boundaryWavelets vector with indices of 'randwerte'
  * @return number of nonzeros in the multiscaleVals according to the boundary wavelets
  */
int SignalAnalysis::getNumNonZeroEntriesInBoundary(const vector<double>& multiScaleVals,
                                                   const vector<int>& boundaryWavelets)const
{
  // get the number of nonzeros of the Randwerte of the thresholded coefficient vector  //
  int numNonZeros = 0;
  for (unsigned i=0;i<boundaryWavelets.size();i++) {
    if (multiScaleVals[static_cast<size_t>(boundaryWavelets[i]-1)] != 0.)
      numNonZeros++;
  }
  return numNonZeros;
}


void SignalAnalysis::printWavelet(std::string fileName)const
{
  std::ofstream waveFile;
  waveFile.open(fileName.c_str());
  waveFile << m_inputInd.size() << "\t" << m_inputInd.size() << std::endl;
  for(unsigned i=0; i<m_inputInd.size(); i++){
    waveFile << m_inputInd[i] << "\t" << m_inputCoef[i] << std::endl;
  }
  waveFile.close();
}
