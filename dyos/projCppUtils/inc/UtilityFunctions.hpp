/**  @file UtilityFunctions.hpp
*    @brief Utility functions. For example interploation or other manipulation
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 18.06.2012
*/

#pragma once

#include <vector>
#include <map>

namespace utils
{
  //functions for interpolating
  std::vector<double> interp(const double *x0,const double *y0, const int n0,
                             const double *xn, const int nn, const int type);
  double nevilleInterpolation(const double *x, const double *y, const unsigned n, const double t);

  double mround(const double value);
  int numBinaryZeros(const int value);
  //function to copy vector elements
  void copy(const int iCopySize, const double *src, double *dest);
  void copy(const int iCopySize, const int *src, int *dest);
  bool compDbl(const double val1, const double val2, const double tol = 1e-9);
  double findMapKey(const std::map<double, double> &mapArray, const double val);
  int findEntry(const double val, const std::vector<double> &vec);
  std::map<double, double>::const_iterator findMapIter(const std::map<double, double> &mapArray,
                                                       const double val);
}
