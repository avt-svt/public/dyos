
#pragma once

#include "cs.h"
#include <boost/shared_ptr.hpp>
#include "CscMatrix.hpp"
#include <vector>

class CsTripletMatrix
{
public:
  typedef boost::shared_ptr<CsTripletMatrix> Ptr;
protected:
  cs* m_mat;
  bool owner;
  void getVertCatMatrixData(CsTripletMatrix::Ptr matrix,
                            int *rowIndices,
                            int *colIndices,
                            double *values);
                            
  void getMatrixSlicesData(int n_rows, const int* row_idx,
                           int n_cols, const int* col_idx,
                           cs **sliced_triplet)const;
  void getMatrixSlicesKeepSizeData(int numRows, const int* rowIndex,
                                   int numCols, const int* colIndex,
                                   int &numNonZeroes,
                                   std::vector<int> &reducedRow,
                                   std::vector<int> &reducedCol,
                                   std::vector<double> &reducedVal)const;
public:
  CsTripletMatrix();
  // if values == NULL, values are not allocated.
  CsTripletMatrix(int n_rows, int n_cols, int nnz, const int* row_idx, const int* col_idx, const double* values);
  CsTripletMatrix(cs* mat, bool owner = true);
  virtual ~CsTripletMatrix();

  int get_n_rows() const {return m_mat->m;}
  int get_n_cols() const {return m_mat->n;}
  int get_nnz() const {return m_mat->nz;}
  cs* get_matrix_ptr() {return m_mat;}
  CsCscMatrix::Ptr compress() const;
  
  virtual CsTripletMatrix::Ptr vertcat(CsTripletMatrix::Ptr matrix);
  virtual CsTripletMatrix::Ptr get_matrix_slices(int n_rows, const int* row_idx,
                                                 int n_cols, const int* col_idx) const;
  virtual CsTripletMatrix::Ptr get_matrix_slices_keep_size(int numRows, const int* rowIndex,
                                                           int numCols, const int* colIndex) const;
  
  void transpose_in_place();
  virtual void setValue(const int rowIndex, const int colIndex, const double value);
  virtual double getValue(const int rowIndex, const int colIndex);
  virtual void setSubMatrix(CsTripletMatrix::Ptr subMatrix,
                            unsigned upperLeftRowIndex,
                            unsigned upperLeftColIndex);
};

/**
* @class SortedCsTripletMatrix
* @brief row and col indices are sorted making setValue and setSubMatrix
*        function calls more efficient. Input indices are internally sorted.
*/
class SortedCsTripletMatrix : public CsTripletMatrix
{
protected:
  void sort();
public:
  SortedCsTripletMatrix();
  // if values == NULL, values are not allocated.
  SortedCsTripletMatrix(int n_rows, int n_cols, int nnz,
                        const int* row_idx, const int* col_idx, const double* values);
  SortedCsTripletMatrix(cs* mat, bool owner = true);
  virtual ~SortedCsTripletMatrix();
  virtual void setValue(const int rowIndex, const int colIndex, const double value);
  virtual double getValue(const int rowIndex, const int colIndex);
  virtual void setSubMatrix(CsTripletMatrix::Ptr subMatrix,
                            unsigned upperLeftRowIndex,
                            unsigned upperLeftColIndex);
  virtual CsTripletMatrix::Ptr vertcat(CsTripletMatrix::Ptr matrix);
  virtual CsTripletMatrix::Ptr get_matrix_slices(int n_rows, const int* row_idx,
                                                 int n_cols, const int* col_idx) const;
  virtual CsTripletMatrix::Ptr get_matrix_slices_keep_size(int numRows, const int* rowIndex,
                                                           int numCols, const int* colIndex) const;
};
