

extern "C" {
  /** computes x = c*x */
  void dscal_(const int* n, const double* c, double* x, const int* incx);
  /** computes y = a*x+y */
  void daxpy_(const int* N, const double* a, const double* x, const int* incx, double* y, const int* incy);
}
