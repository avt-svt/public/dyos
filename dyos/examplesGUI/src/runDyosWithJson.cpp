/**
* @file runDyosWithJson.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Example for GUI (CES Softwarepraktikum)                              \n
* =====================================================================\n
* This file run the json input file "genericDyosInput.json             \n
* It can be made more generic with an input argument for the main      \n
* function.
* =====================================================================\n
* @author Johannes M. M. Faust
* @date 01.07.2019
*/

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"
#include "FMILargeSystemTestConfig.hpp"
#include "ConvertFromXmlJson.hpp"
#include <iostream>


int main(){
  // Output files are written to current directory, i.e., it may be at a different location than expected!
 
  UserInput::Input input; 
  convertUserInputFromXmlJson("genericDyosInput.json", input, JSON);
  UserOutput::Output output;

  //define logger for output (otherwise standard logger is used)
  Logger::Ptr logger(new StandardLogger());
  //create logger writing genInt
  OutputChannel::Ptr genInt(new FileChannel("genInt.json"));
  logger->setLoggingChannel(Logger::INPUT_OUT_JSON, genInt);

  //create logger writing into finalstates.dat
  OutputChannel::Ptr genOut(new FileChannel("genOut.json"));
  logger->setLoggingChannel(Logger::FINAL_OUT_JSON, genOut);

  output = runDyos(input, logger);


  // generic outputName
  std::string filename = "genericDyosOutput.json";
  convertUserOutputToXmlJson(filename, output, JSON);


  return 0;
 }
