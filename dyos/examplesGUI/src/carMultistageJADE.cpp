/**
* @file carMultistageJADE.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Example for GUI (CES Softwarepraktikum)                              \n
* Based on MultiStageSystemTestsTestsuite.hpp                          \n
* =====================================================================\n
* This file contains a multistage car model                            \n
* =====================================================================\n
* @author Susanne Sass
* @date 31.05.2019
*/

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"
#include "ConvertFromXmlJson.hpp"
#include <iostream>
#include <utility>
#include <string>

using namespace UserInput;

/**
* divide the example in three stages with full state mapping between all stages - first using
* the fullStateMapping flag and then map all variables explicitly
*/
int main(){
	// Stage 1
	StageInput stageInput;
	stageInput.eso.type = EsoType::JADE;
	stageInput.eso.model = "carFreeFinalTimeTwoObjectives";

	stageInput.integrator.duration.lowerBound = 20.0;
	stageInput.integrator.duration.upperBound = 20.0;
	stageInput.integrator.duration.value = 20.0;
	stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
	stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

	stageInput.integrator.parameters.resize(1);
	stageInput.integrator.parameters[0].name = "accel";
	stageInput.integrator.parameters[0].lowerBound = -2.0;
	stageInput.integrator.parameters[0].upperBound = 2.0;
	stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
	stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

	const unsigned int numIntervals = 5;
	stageInput.integrator.parameters[0].grids.resize(1);
	stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
	stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals + 1, 2.0);
	stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;

	stageInput.optimizer.objective.name = "fuel";
	stageInput.optimizer.objective.lowerBound = 0.0;
	stageInput.optimizer.objective.upperBound = 500.0;

	stageInput.optimizer.constraints.resize(1);
	stageInput.optimizer.constraints[0].name = "velo";
	stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
	stageInput.optimizer.constraints[0].lowerBound = 0.0;
	stageInput.optimizer.constraints[0].upperBound = 10.0;

	stageInput.treatObjective = false; 


	Input input;

	input.stages.push_back(stageInput);
	input.stages.back().mapping.fullStateMapping = true;

	// Stage 2 based on Stage 1

	stageInput.integrator.duration.lowerBound = 5.0;
	stageInput.integrator.duration.upperBound = 5.0;
	stageInput.integrator.duration.value = 5.0;
	stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;

	UserInput::ParameterInput initialValueParameter;
	initialValueParameter.name = "velo";
	initialValueParameter.sensType = UserInput::ParameterInput::FRACTIONAL;
	initialValueParameter.paramType = UserInput::ParameterInput::INITIAL;
	initialValueParameter.value = 5.0;

	stageInput.integrator.parameters.push_back(initialValueParameter);

	input.stages.push_back(stageInput);
	input.stages.back().mapping.fullStateMapping = true;

	// Stage 3 based on Stage 2

	stageInput.integrator.duration.lowerBound = 0.1;
	stageInput.integrator.duration.upperBound = 100.0;
	stageInput.integrator.duration.value = 15.0;
	stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;

	stageInput.integrator.parameters.back().name = "ttime";
	stageInput.integrator.parameters.back().value = 0.0;

	UserInput::ConstraintInput constraint;
	constraint.name = "velo";
	constraint.type = UserInput::ConstraintInput::ENDPOINT;
	constraint.lowerBound = 0.0;
	constraint.upperBound = 0.0;

	stageInput.optimizer.constraints.push_back(constraint);
	input.stages.back().mapping.fullStateMapping = false;


	constraint.name = "dist";
	constraint.lowerBound = 300.0;
	constraint.upperBound = 300.0;

	stageInput.optimizer.constraints.push_back(constraint);

	stageInput.optimizer.objective.name = "ttime";
	stageInput.optimizer.objective.lowerBound = 0.0;
	stageInput.optimizer.objective.upperBound = 100.0;
	stageInput.treatObjective = true; 

	input.stages.push_back(stageInput);

	input.runningMode = Input::SINGLE_SHOOTING;
	
	input.integratorInput.type = IntegratorInput::LIMEX;
	input.integratorInput.order = IntegratorInput::FIRST_FORWARD;

	input.optimizerInput.type = OptimizerInput::SNOPT;
	std::map<std::string, std::string> optimizerOptions;
	optimizerOptions.insert(std::make_pair<std::string, std::string>("Major Iterations Limit", "200"));
	input.optimizerInput.optimizerOptions = optimizerOptions;

	// write JSON input file
	std::string filenameIn = "carMultistageJADEin.json"; 

	convertUserInputToXmlJson(filenameIn, input, JSON); 
	// generic inputName
	filenameIn = "genericDyosInput.json";
	convertUserInputToXmlJson(filenameIn, input, JSON);


	UserOutput::Output output; 
	output = runDyos(input);

	std::string filename = "carMultistageJADEout.json";
	convertUserOutputToXmlJson(filename, output, JSON);

	// generic outputName
	filename = "genericDyosOutput.json"; 
	convertUserOutputToXmlJson(filename, output, JSON);
	
	return 0;
}