/**
* @file wobatchFMU.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Example for GUI (CES Softwarepraktikum)                              \n
* Based on LargeSystemTestFMI.cpp                                      \n
* =====================================================================\n
* This file couples williams otto model via FMU                        \n
* =====================================================================\n
* @author Susanne Sass, Adrian Caspari
* @date 31.05.2019
*/

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"
#include "FMILargeSystemTestConfig.hpp"

#include <iostream>

using namespace UserInput;

int main(){
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoType::FMI;
    esoInput.model = PATH_FMI_LARGE_SYSTEM_TEST_FMI;
	//esoInput.relativeFmuTolerance = 1e-6;
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0;
      stageDuration.upperBound = 1000.0;
      stageDuration.value = 1000.0;
      stageDuration.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
		std::cout << "Piecewise constant controls/parameters have as many values as intervals in the grid!" << std::endl; 
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.resize(numIntervals , 5.784);
        grids[0].adapt.maxAdaptSteps = 4;
        grids[0].adapt.adaptWave.minRefinementLevel = 1;
        grids[0].adapt.adaptWave.horRefinementDepth = 0;
        grids[0].pcresolution = 2;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals, 100e-3);
        grids[0].adapt.maxAdaptSteps = 4;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "X9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "X7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;
	  
      constraints[1].name = "X8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }

  input.stages.push_back(stageInput);
  input.stages[0].treatObjective = true;
  
  input.runningMode = Input::SINGLE_SHOOTING; 
  
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-4";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-4";
  //input.integratorInput.integratorOptions["forward sensitivity method"] = "staggered";
  //input.integratorInput.daeInit.linSolver.type = LinearSolverInput::KLU;
  
  input.optimizerInput.type = OptimizerInput::IPOPT;
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-6";
  //input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  //input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  //input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  //input.optimizerInput.optimizerOptions["function precision"] = "1.e-6";
  //input.optimizerInput.optimizerOptions["elastic weight"] = "1.e+6";
  //input.optimizerInput.optimizerOptions["warm start"];

  //define logger for output (otherwise standard logger is used)
  Logger::Ptr logger(new StandardLogger());
  //create logger writing into statenames.dat
  OutputChannel::Ptr statenamesDat(new FileChannel("statenames.dat"));
  logger->setLoggingChannel(Logger::STATENAMES_OUT, statenamesDat);
  //create logger writing into finalstates.dat
  OutputChannel::Ptr finalstateDat(new FileChannel("finalstates.dat"));
  logger->setLoggingChannel(Logger::FINALSTATES_OUT, finalstateDat);

  // write JSON input file
  std::string filenameIn = "wobatchFMUpwcin.json";

  convertUserInputToXmlJson(filenameIn, input, JSON);
  // generic inputName
  filenameIn = "genericDyosInput.json";
  convertUserInputToXmlJson(filenameIn, input, JSON);
  
 
  UserOutput::Output output;
  output = runDyos(input, logger);

  std::string filename = "wobatchFMUpwcout.json";
  convertUserOutputToXmlJson(filename, output, JSON);

  // generic outputName
  filename = "genericDyosOutput.json";
  convertUserOutputToXmlJson(filename, output, JSON);


  return 0;


 }
