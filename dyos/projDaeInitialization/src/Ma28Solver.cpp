/**
* @file Ma28Solver.cpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Implementation of the linear solver class Ma28Solver                 \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 04.10.2012
*/

#include "Ma28Solver.hpp"

/**
* @brief fortran function LINSOL solving the linear system Ax=b
*
* @param[in] matrixOrder order of matrix (>=1)
* @param[in] numNonZeroes number of nonzeros in matrix, (>=1)
* @param[in] matrixValues array of length lenColIndices, first numNonZeroes entries
*                          hold the nonzero entries of A, rest of length 3*numNonZeroes
*                          is working space, which need not be set on input
* @param[in] matrixRowIndices integer array of length lenRowIndices,
*                             first NZ entries hold the row indices of the nonzero values
*                             stored in A, rest of length numNonZeroes is working space,
*                             which need not be set on input
* @param[in] matrixColIndices integer array of length lenColIndices,
*                             first NZ entries hold the column indices of the nonzero values
*                             stored in A, rest of length 3*numNonZeroes is working space,
*                             which need not be set on input
* @param[in, out] b double precision array of length matrixOrder which contains the right hand side
*                   on exit this vector contains the solution of the system
* @param[in] lenRowIndices length of array matrixRowIndices
* @param[in] lenColIndices length of arrays matrixValues and matrixColIndices
* @param[in] iKeep integer array of length 5*matrixOrder
* @param[in] intWorkspace integer workspace array of length 8*matrixOrder
* @param[in] doubleWorkspace double precision array of length matrixOrder
* @param[out] errorFlag error flag, value of zero on output indicates success
* @param[in] mode if mode==0, do decomposition, else solve without decomposition
* @param[in] tidisp integer array of length 2
*/
#ifdef WIN32
#define LINSOL linsol
#else
#define LINSOL linsol_
#endif
extern "C" void  LINSOL(const int &matrixOrder,
                        const int &numNonZeroes,
                        const double *matrixValues,
                        const int* matrixRowIndices,
                        const int *matrixColInidces,
                              double *b,
                        const int &lenRowIndices,
                        const int &lenColIndices,
                        const int *iKeep,
                        const int *intWorkspace,
                        const double *doubleWorkspace,
                              int &errorFlag,
                        const int &mode,
                        const int *tidisp);

/// @brief standard constructor
Ma28Solver::Ma28Solver(){}

/// @brief destructor
Ma28Solver::~Ma28Solver(){}

/// @copydoc LinearSolver::solve
LinearSolver::ResultFlag Ma28Solver::solve(const CsTripletMatrix::Ptr &matrix, utils::Array<double> &xb) const
{
  const int matrixOrder = matrix->get_n_rows();
  const int numNonZeroes = matrix->get_nnz();
  assert(matrixOrder == matrix->get_n_cols()); // square matrix expected
  assert(matrixOrder >= 1);
  assert(numNonZeroes >=1);

  assert(unsigned(matrixOrder) == xb.getSize());


  const int lenRowIndices = 2*numNonZeroes;
  const int lenColIndices = 4*numNonZeroes;
  utils::Array<int> matrixRowIndices(lenRowIndices, 0);
  utils::Array<int> matrixColIndices(lenColIndices, 0);
  utils::Array<double> matrixValues(lenColIndices, 0.0);

  const cs* matrixPtr = matrix->get_matrix_ptr();
  // copy matrix data and add +1 offset to index arrays
  for(int i=0; i<numNonZeroes; i++){
    matrixRowIndices[i] = matrixPtr->i[i] + 1;
    matrixColIndices[i] = matrixPtr->p[i] + 1;
    matrixValues[i] = matrixPtr->x[i];
  }

  utils::Array<int> iKeep(5*matrixOrder), intWorkspace(8*matrixOrder);
  utils::Array<double> doubleWorkspace(matrixOrder);

  //set
  int doDecomposition = 0;
  int tidisp[2];
  int errorFlag;

  LINSOL(matrixOrder,
         numNonZeroes,
         matrixValues.getData(),
         matrixRowIndices.getData(),
         matrixColIndices.getData(),
         xb.getData(),
         lenRowIndices,
         lenColIndices,
         iKeep.getData(),
         intWorkspace.getData(),
         doubleWorkspace.getData(),
         errorFlag,
         doDecomposition,
         tidisp);


  //create output
  LinearSolver::ResultFlag resultFlag;
  switch(errorFlag){
    case 0:
      resultFlag = LinearSolver::OK;
      break;
    default:
      resultFlag = LinearSolver::FAILED;
  }
  return resultFlag;
}
