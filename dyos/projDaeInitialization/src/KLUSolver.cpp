/**
* @file Ma28Solver.cpp
*
* =====================================================================\n
* &copy; Aachener Verfahrenstechnik-Prozesstechnik, RWTH Aachen        \n
* =====================================================================\n
* Implementation of the linear solver class KLUSolver                  \n
* =====================================================================\n
* @author Sascha Bastian
* @date 31.03.2014
*/

#include "KLUSolver.hpp"


/// @brief standard constructor
KLUSolver::KLUSolver(){}

/// @brief destructor
KLUSolver::~KLUSolver(){}

/// @copydoc LinearSolver::solve
LinearSolver::ResultFlag KLUSolver::solve(const CsTripletMatrix::Ptr &matrix, utils::Array<double> &xb) const
{
	CsCscMatrix::Ptr mc = matrix->compress();
	cs* mp = mc->get_matrix_ptr();

	const int size = mp->m;

	double *b;
	b=xb.getData();

	int errorFlag;

	klu_symbolic *Symbolic ;
    klu_numeric *Numeric ;
    klu_common Common ;
    klu_defaults (&Common) ;

	//set
	Symbolic = klu_analyze (size, mp->p, mp->i, &Common) ;
    Numeric = klu_factor (mp->p, mp->i, mp->x, Symbolic, &Common) ;

    errorFlag=klu_solve (Symbolic, Numeric, size, 1, b, &Common) ;

    klu_free_symbolic (&Symbolic, &Common) ;
    klu_free_numeric (&Numeric, &Common) ;

  
  
  //create output
  LinearSolver::ResultFlag resultFlag;
  switch(errorFlag){
    case 1:
      resultFlag = LinearSolver::OK;
      break;
    default:
      resultFlag = LinearSolver::FAILED;
  }
  return resultFlag;
}
