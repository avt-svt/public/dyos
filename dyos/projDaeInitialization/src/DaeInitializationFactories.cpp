/**
* @file DaeInitializationFactories.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DaeInitialization                                                    \n
* =====================================================================\n
* This file contains the definitions of member functions of the        \n
* factories for the classes DaeInitialization, LinearSolver and        \n
* NonLinearSolver                                                      \n
* =====================================================================\n
* @author Tjalf Hoffmann, Adrian Caspari
* @date 17.10.2018
*/

#include "DaeInitializationFactories.hpp"

#ifdef BUILD_LINSOL_MA28
Ma28Solver *LinearSolverFactory::createMa28Solver() const
{
  return new Ma28Solver();
}
#endif

KLUSolver *LinearSolverFactory::createKLUSolver() const
{
	return new KLUSolver();
}

LinearSolver *LinearSolverFactory::createLinearSolver
                                  (const FactoryInput::LinearSolverInput &input) const
{
  LinearSolver* linSol;
  
  switch(input.type){
#ifdef BUILD_LINSOL_MA28
    case FactoryInput::LinearSolverInput::MA28:
      linSol = createMa28Solver();
      break;
#endif

	case FactoryInput::LinearSolverInput::KLU:
		linSol = createKLUSolver();
		break;
    default:
      assert(false);
  }
  
  return linSol;
}

#ifdef BUILD_NLEQ1S
Nleq1sSolver *NonLinearSolverFactory::createNleq1sSolver
                                     (const FactoryInput::NonLinearSolverInput &input) const
{
  return new Nleq1sSolver(input.tolerance);
}
#endif

CMinPackSolver *NonLinearSolverFactory::createCMinPackSolver
                                      (const FactoryInput::NonLinearSolverInput &input) const
{
  return new CMinPackSolver(input.tolerance);
}

NonLinearSolver *NonLinearSolverFactory::createNonLinearSolver(
                                      const FactoryInput::NonLinearSolverInput &input) const
{
  NonLinearSolver *nonLinSol;
  
  switch(input.type){
#ifdef BUILD_NLEQ1S
    case FactoryInput::NonLinearSolverInput::NLEQ1S:
      nonLinSol = createNleq1sSolver(input);
      break;
#endif
    case FactoryInput::NonLinearSolverInput::CMINPACK:
      nonLinSol = createCMinPackSolver(input);
      break;
    default:
      assert(false);
  }
  
  return nonLinSol;
}

NoDaeInitialization *DaeInitializationFactory::createNoDaeInitialization() const
{
  return new NoDaeInitialization();
}

FullDaeInitialization *DaeInitializationFactory::createFullDaeInitialization
                                                  (const LinearSolver::Ptr &linSol,
                                                   const NonLinearSolver::Ptr &nonLinSol,
                                                   const double maxErrorTolerance) const
{
  return new FullDaeInitialization(linSol, nonLinSol, maxErrorTolerance);
}

BlockDaeInitialization *DaeInitializationFactory::createBlockDaeInitialization
                                                    (const LinearSolver::Ptr &linSol,
                                                     const NonLinearSolver::Ptr &nonLinSol,
                                                     const double maxErrorTolerance) const
{
  return new BlockDaeInitialization(linSol, nonLinSol, maxErrorTolerance);
}

#ifdef BUILD_WITH_FMU
FmiDaeInitialization *DaeInitializationFactory::createFmiDaeInitialization() const
{
	return new FmiDaeInitialization();
}
#endif
DaeInitialization *DaeInitializationFactory::createDaeInitialization(
                                            const LinearSolverFactory &linSolFactory,
                                            const NonLinearSolverFactory &nonLinSolFactory,
                                            const FactoryInput::DaeInitializationInput &input) const
{
  DaeInitialization* daeInit;
  
  const LinearSolver::Ptr linSol(linSolFactory.createLinearSolver(input.linSolver));
  const NonLinearSolver::Ptr nonLinSol(nonLinSolFactory.createNonLinearSolver(input.nonLinSolver));
  
  switch(input.type){
    case FactoryInput::DaeInitializationInput::NO:
      daeInit = createNoDaeInitialization();
      break;
    case FactoryInput::DaeInitializationInput::FULL:
      daeInit = createFullDaeInitialization(linSol, nonLinSol, input.maximumErrorTolerance);
      break;
    case FactoryInput::DaeInitializationInput::BLOCK:
      daeInit = createBlockDaeInitialization(linSol, nonLinSol, input.maximumErrorTolerance);
      break;
#ifdef BUILD_WITH_FMU
	case FactoryInput::DaeInitializationInput::FMI:
		daeInit = createFmiDaeInitialization();
		break;
#endif
    default:
      assert(false);
  }
  
  return daeInit;
}

DaeInitialization *DaeInitializationFactory::createDaeInitialization(
                                            const FactoryInput::DaeInitializationInput &input) const
{
  LinearSolverFactory linSolFac;
  NonLinearSolverFactory nonLinSolFac;
  return createDaeInitialization(linSolFac, nonLinSolFac, input);
}