/** 
* @file DaeInitializationFactories.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DaeInitialization                                                    \n
* =====================================================================\n
* This file contains the declarations of factories for classes         \n
* DaeInitialization and Solver                                         \n
* =====================================================================\n
* @author Tjalf Hoffmann, Adrian Caspari 
* @date 17.10.2018
*/

#pragma once

#include "DaeInitialization.hpp"
#include "FullDaeInitialization.hpp"
#include "BlockDaeInitialization.hpp"

#ifdef BUILD_WITH_FMU
#include "FmiDaeInitialization.hpp"
#endif




#include "Solver.hpp"
#ifdef BUILD_LINSOL_MA28
#include "Ma28Solver.hpp"
#endif
#include "KLUSolver.hpp"
#ifdef BUILD_NLEQ1S
#include "Nleq1sSolver.hpp"
#endif
#include "DaeInitializationInput.hpp"
#include "CMinPackSolver.hpp"

class LinearSolverFactory
{
public:
  #ifdef BUILD_LINSOL_MA28
	Ma28Solver *createMa28Solver() const;
  #endif

  KLUSolver *createKLUSolver() const;
  LinearSolver *createLinearSolver(const FactoryInput::LinearSolverInput &input) const;
};

class NonLinearSolverFactory
{
public:
  #ifdef BUILD_NLEQ1S
  Nleq1sSolver *createNleq1sSolver(const FactoryInput::NonLinearSolverInput &input) const;
  #endif
  CMinPackSolver *createCMinPackSolver(const FactoryInput::NonLinearSolverInput &input) const;
  NonLinearSolver *createNonLinearSolver(const FactoryInput::NonLinearSolverInput &input) const;
};

class DaeInitializationFactory
{
public:
  NoDaeInitialization *createNoDaeInitialization() const;
  FullDaeInitialization *createFullDaeInitialization(const LinearSolver::Ptr &linSol,
                                                     const NonLinearSolver::Ptr &nonLinSol,
                                                     const double maxErrorTolerance) const;
  BlockDaeInitialization *createBlockDaeInitialization(const LinearSolver::Ptr &linSol,
                                                       const NonLinearSolver::Ptr &nonLinSol,
                                                       const double maxErrorTolerance) const;
  DaeInitialization *createDaeInitialization(const LinearSolverFactory &linSolFactory,
                                             const NonLinearSolverFactory &nonLinSolFactory,
                                             const FactoryInput::DaeInitializationInput &input) const;
  DaeInitialization *createDaeInitialization(const FactoryInput::DaeInitializationInput &input) const;
  #ifdef BUILD_WITH_FMU
    FmiDaeInitialization *createFmiDaeInitialization() const;
  #endif

};