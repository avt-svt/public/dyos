/**
* @file DaeInitializationInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* DaeInitialization                                                    \n
* =====================================================================\n
* This file contains the declarations of the input structs for creating\n
* DaeInitialization and Solver objects.                                \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 25.10.2012
*/

#pragma once

#include "HaveFmuConfig.hpp"

namespace FactoryInput
{
  struct LinearSolverInput
  {
    enum SolverType{
		KLU
        #ifdef BUILD_LINSOL_MA28
		,
		MA28
        #endif
    };

    SolverType type;
    LinearSolverInput() : type(KLU){}
  };

  struct NonLinearSolverInput
  {
    enum SolverType{
      
      CMINPACK
#ifdef BUILD_NLEQ1S
	  ,
	  NLEQ1S
#endif
    };

    SolverType type;
    double tolerance;
    NonLinearSolverInput() : type(CMINPACK), tolerance(1e-10) {}
  };

  struct DaeInitializationInput
  {
    enum DaeInitializationType{
      NO,
      FULL,
      BLOCK
#ifdef BUILD_WITH_FMU
	  , 
	  FMI
#endif
    };

    DaeInitializationType type;
    LinearSolverInput linSolver;
    NonLinearSolverInput nonLinSolver;
    double maximumErrorTolerance;
    DaeInitializationInput() :
      type(BLOCK), maximumErrorTolerance(1e-10) {}
  };
}//FactoryInput
