/**
* @file GridRefinementFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GridRefinement                                                       \n
* =====================================================================\n
* =====================================================================\n
* @author Fady Assassa
* @date 18.09.2012
*/

#pragma once
#include "GridRefinementInput.hpp"
#include "GridRefinementInterface.hpp"
#include "DyosObject.hpp"

/** @class GridRefinementFactory
 *  @brief creates a GridRefinementInterface from the FactoryInput
 */
class GridRefinementFactory : public DyosObject
{
public:
  GridRefinementInterface::Ptr createRefinementInterface(const FactoryInput::AdaptationInput &input);
};
