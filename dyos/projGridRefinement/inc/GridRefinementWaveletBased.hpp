/**  @file GridRefinementInterface.hpp
*    @brief Wrapping of the Grid refinement interface using a signal based analysis.
*    
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*
*   @author Fady Assassa
*   @date 19.06.2012
*/



#pragma once
#include <vector>
#include "GridRefinementInterface.hpp"
#include "GridRefinementInput.hpp"
#include "SignalAnalysis.hpp"




/** @class GridRefinementWaveletBased 
  * @brief wavelet based grid refinement using the signal analysis
  */
class GridRefinementWaveletBased:public GridRefinementInterface
{
  //! m_signal is the object which is used for the signal analysis.
  SignalAnalysis m_signal;
  //! @brief empty constructor
  GridRefinementWaveletBased(){};
public:
  GridRefinementWaveletBased(FactoryInput::OptionsWaveletRefinemet options);

  virtual void obtainNewMesh(const std::vector<double> &grid,
                             const std::vector<double> &values);
  virtual std::vector<double> getRefinedGrid() const;
  virtual std::vector<double> getRefinedValues() const;
  virtual GridRefinementInterface::type getType()const;
  virtual void addData(std::map<double, double> &data){};
  void writeOutput(std::string fileName)const;
};
