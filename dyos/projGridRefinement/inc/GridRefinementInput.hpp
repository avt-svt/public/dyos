/**
* @file GridRefinementInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GridRefinement                                                       \n
* =====================================================================\n
* =====================================================================\n
* @author Fady Assassa
* @date 17.09.2012
*/

#pragma once
#include <vector>
#include <string>

namespace FactoryInput
{
  /** @struct a struct that contains all information required for refinement using wavelets */
  struct OptionsWaveletRefinemet
  {
    //! type either piecewise constant or linear
    int type;
    //! upperBound is the upper bound of the signal in the time domain
    double upperBound;
    //! lowerBound is the lower bound of the signal in the time domain
    double lowerBound;
    //! thresholdedIndices are the indices which should not be included during refinement
    std::vector<int> thresholdedIndices;
    //!maxRefinementLevel maximum level of refinement. Usually 10
    int maxRefinementLevel;
    //!minRefinementLevel minimum level of refinement. Usually 3
    int minRefinementLevel;
    //!horRefinementDepth horizontal refinement depth. Depth of wavelets considered for refinement in horizontal direction.
    int horRefinementDepth;
    //!verRefinementDepth vertical refinement depth. Depth of wavelets considered for refinement in vertical direction.
    int verRefinementDepth;
    //!etres constant that marks wavelets for elimination.
    double etres;
    //!epsilon constant that marks wavelets for insertion.
    double epsilon;
    // @brief constructor with standard values */
    OptionsWaveletRefinemet():type(1), // piecewise constant
                              upperBound(0.0),
                              lowerBound(0.0),
                              thresholdedIndices(0),
                              maxRefinementLevel(10),
                              minRefinementLevel(3),
                              horRefinementDepth(0),
                              verRefinementDepth(1),
                              etres(1.0e-8),
                              epsilon(0.9){}
  };

  struct SWAdaptationInput
  {
    //! type either piecewise constant (1) or linear (2)
    int type;
    //! maxRefinementLevel maximum level of refinement. Usually 10
    int maxRefinementLevel;
    //! includeTol if SW is larger than the tolerance, a grid point is included in the control grid
    double includeTol;
    //! lBound lower bound of control
    double lBound; 
    //! uBound upper bound of control
    double uBound;

    // @brief constructor with standard values */
    SWAdaptationInput() : type(1), maxRefinementLevel(10), includeTol(1e-3), lBound(0.0), uBound(0.0){}
  };

  /** @struct AdaptationInput
  *  @brief contains the information required for wavelet adaptation 
  */
  struct AdaptationInput
  {
    /** @enum RefinementType specifier for which refinement should be used */
    enum RefinementType
    {
      WaveletBasedRefinement,
      SwitchingFunction
    };
    //!maxAdaptSteps maximum number of adaptation steps
    unsigned maxAdaptSteps;
    RefinementType adaptType;
    OptionsWaveletRefinemet adaptWave;
    SWAdaptationInput swAdapt;

    AdaptationInput():maxAdaptSteps(1),
                      adaptType(WaveletBasedRefinement){};
  };
}// FactoryInput
