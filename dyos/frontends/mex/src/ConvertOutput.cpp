#include "ConvertOutput.hpp"

/**
* @brief checks if a field exists in a given Matlab-struct or otherwise could be created
*
* @param matlabOutput Matlab struct to be checked
* @param fieldname name of the field
* @note raises an Matlab error if field could not be created
* @author Tjalf Hoffmann
*/
void checkOutputField(mxArray *matlabOutput, char *fieldname)
{
  if(mxGetFieldNumber(matlabOutput, fieldname) < 0){
    if(mxAddField(matlabOutput, fieldname) < 0){
      std::string errMsg = "Could not create field ";
      errMsg.append(fieldname);
      mexErrMsgTxt(errMsg.c_str());
    }
  }
}

/**
* @brief puts given string into the field in a given Matlab-struct
*
* @param matlabOutput given Matlab struct
* @param stringOutput given string
* @param fieldname name of the field
* @param index index of the desired element
* @note checks if field exists
* @author Tjalf Hoffmann
*/
void setStringField(mxArray *matlabOutput, std::string stringOutput, char *fieldname, const int index)
{
  mxArray *matlabString = mxCreateString(stringOutput.c_str());
  checkOutputField(matlabOutput, fieldname);
  mxSetField(matlabOutput,index, fieldname, matlabString);
}


/**
* @brief puts given double into the field in a given Matlab-struct
*
* @param matlabOutput given Matlab struct
* @param scalar given double
* @param fieldname name of the field
* @param index index of the desired element
* @note checks if field exists
* @author Tjalf Hoffmann
*/
void setScalarField(mxArray *matlabOutput, double scalar, char *fieldname, const int index)
{
  mxArray *matlabScalar = mxCreateDoubleScalar(scalar);
  checkOutputField(matlabOutput, fieldname);
  mxSetField(matlabOutput, index, fieldname, matlabScalar);
}

/**
* @brief puts given double vector into the field in a given Matlab-struct
*
* @param matlabOutput given Matlab struct
* @param vec given double vector
* @param fieldname name of the field
* @param index index of the desired element
* @note checks if field exists
* @author Tjalf Hoffmann
*/
void setDoubleVectorField(mxArray *matlabOutput, std::vector<double> vec,
                             char *fieldname, const int index)
{
  mxArray *matlabVector = mxCreateDoubleMatrix(1, vec.size(), mxREAL);
  double *matlabVecPtr = mxGetPr(matlabVector);
  if(!vec.empty())
  {
    utils::copy(vec.size(), &vec[0], matlabVecPtr);
  }
  checkOutputField(matlabOutput, fieldname);
  mxSetField(matlabOutput, index, fieldname, matlabVector);
}


/**
* @brief puts given boolean variable into the field in a given Matlab-struct
*
* @param matlabOutput given Matlab struct
* @param flag given boolean variable
* @param fieldname name of the field
* @param index index of the desired element
* @note checks if field exists
* @author Tjalf Hoffmann
*/
void setBoolField(mxArray *matlabOutput, bool flag, char *fieldname, const int index)
{
  mxArray *matlabBool = mxCreateLogicalScalar(flag);
  checkOutputField(matlabOutput, fieldname);
  mxSetField(matlabOutput, index, fieldname, matlabBool);
}

void setDoubleMatrixField(mxArray *matlabOutput, std::vector<std::vector<double> > vecMat,
                             char *fieldname, const int index)
{
  checkOutputField(matlabOutput, fieldname);
  int numCols = 0;
  int numRows = vecMat.size();
  if(!vecMat.empty()){
    numCols = vecMat.front().size();
  } 
  mwSize ndim=2, dims[] = {vecMat.size(), 1};
  mxArray *convertedMatrix = mxCreateDoubleMatrix(numRows, numCols, mxREAL);
  double *matrix = mxGetPr(convertedMatrix);
  for(int i=0; i<numRows; i++){
    if(vecMat[i].size() != numCols){
      mexErrMsgTxt("in setDoubleMatrixField a non rectangle data field is set");
    }
    for(int j=0; j<numCols; j++){
      //matlab stores matrices columnwise
      matrix[j*numRows + i] = vecMat[i][j];
    }
  }
  mxSetField(matlabOutput, index, fieldname, convertedMatrix);
}

void resizeStructField(mxArray *matlabOutput, char *fieldname, const int newSize, const int index)
{
  mxArray *original = mxGetField(matlabOutput, index, fieldname);
  if(original == NULL){
    return;
  }
  
  int numFields = mxGetNumberOfFields(original);
  int numOriginalElements = mxGetNumberOfElements(original);
  mwSize ndims = 2, dim[] = {1, static_cast<size_t>(newSize)};
  const char *dummyname[] ={"dummy"};
  mxArray *newField = mxCreateStructArray(ndims, dim, 1, dummyname);
  for(int i=0; i<numFields; i++){
    const char *subFieldname = mxGetFieldNameByNumber(original, i);
    mxAddField(newField, subFieldname);
    //copy old field
    for(int j=0; j<std::min(numOriginalElements, newSize); j++){
      mxArray *originalField = mxGetField(original, j, subFieldname);
      mxSetField(newField, j, subFieldname, originalField);
    }
    //create new fields (copy of the first struct)
    if(numOriginalElements <= 0){
      mexErrMsgTxt("try to create copies from null pointer\n");
    }
    for(int j=numOriginalElements; j<newSize; j++){
      mxArray *originalField = mxGetField(original, 0, subFieldname);
      mxArray *newSubField = mxDuplicateArray(originalField);
      mxSetField(newField, j, subFieldname, newSubField);
    }
  }
  if(numFields > 0){
    int dummyFieldNumber = mxGetFieldNumber(newField, "dummy");
    mxRemoveField(newField, dummyFieldNumber);
  }
  mxSetField(matlabOutput, index, fieldname, newField);
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct
* @param index index of the desired element
* @note adds field stageGrid to Matlab-struct
* @author Konstanze K�lle, Tjalf Hoffmann
*/
void convertStageOutput(mxArray *matlabOutput, UserOutput::StageOutput dyosOut, const int index)
{
  setBoolField(matlabOutput, dyosOut.treatObjective, "treatObjective", index);
  
  mxArray *integrator = mxGetField(matlabOutput, index, "integrator");
  if(integrator == NULL){
    mexErrMsgTxt("failed to read out field integrator in stageOutput struct");
  }
  convertIntegratorStageOutput(integrator, dyosOut.integrator, 0);
  mxSetField(matlabOutput, index, "integrator", integrator);
  
  mxArray *optimizer = mxGetField(matlabOutput, index, "optimizer");
  if(optimizer != NULL){
    //optimizer is optional field (e.g.
    //mexErrMsgTxt("failed to read out field optimizer in stageOutput struct");
    convertOptimizerStageOutput(optimizer, dyosOut.optimizer, 0);
    mxSetField(matlabOutput, index, "optimizer", optimizer);
  }
  checkOutputField(matlabOutput, "stageGrid");
  setDoubleVectorField(matlabOutput, dyosOut.stageGrid, "stageGrid", index);
  
}

/**
* @brief converts given dyos-struct into Matlab-struct
*
* @param matlabOutput Matlab struct
* @param dyosOut given dyos-struct UserOutput::Output
* @param index index of the desired element
* @author Konstanze K�lle, Tjalf Hoffmann
*/void convertOutput(mxArray *matlabOutput, UserOutput::Output dyosOut, int index)
{
  unsigned numHistory = dyosOut.solutionHistory.size();
  
  if(numHistory > 0){
    checkOutputField(matlabOutput, "solutionHistory");
    mxArray *inputCopy = mxDuplicateArray(matlabOutput);
    mxSetField(matlabOutput, index, "solutionHistory", inputCopy);
    resizeStructField(matlabOutput, "solutionHistory", numHistory, 0);
    mxArray *solHistory = mxGetField(matlabOutput, index, "solutionHistory");
    for(unsigned i=0; i<numHistory; i++){
      convertOutput(solHistory, dyosOut.solutionHistory[i], i);
    }
    mxSetField(matlabOutput, index, "solutionHistory", solHistory);
  }


  int numDyosOutStages = dyosOut.stages.size();
  
  mxArray *stages = mxGetField(matlabOutput, index, "stages");
  if(stages == NULL){
    mexErrMsgTxt("failed to read out field stages in output struct");
  }
  if(numDyosOutStages != mxGetNumberOfElements(stages)){
    mexErrMsgTxt("Mismatch of number of stages in input and output\n"
                 "This is probably a bug - please report this to the developers\n");
  }
  for(int i=0; i<numDyosOutStages; i++){
    convertStageOutput(stages, dyosOut.stages[i], i);
  }
  
  mxArray *optimizerInput = mxGetField(matlabOutput, index, "optimizerInput");
  if(optimizerInput != NULL){
    //optimizerInput is an optional field (e.g. using simulation)
    //mexErrMsgTxt("failed to read out field optimizerInput in output struct");
    mxArray *optimizerOutput = mxDuplicateArray(optimizerInput);
    convertOptimizerOutput(optimizerOutput, dyosOut.optimizerOutput);
    mxAddField(matlabOutput, "optimizerOutput");
    mxSetField(matlabOutput, index, "optimizerOutput", optimizerOutput);
    int fieldNumber = mxGetFieldNumber(matlabOutput, "optimizerInput");
    mxRemoveField(matlabOutput, fieldNumber);
  }
}

void setMap(mxArray *matlabOutput, std::map<int,int> &mapOutput, char *fieldname, const int index)
{
  size_t numRows = mapOutput.size();
  size_t numCols = 2;
  mwSize dims[] = {numRows, numCols};
  mxArray *cell = mxCreateCellArray(2, dims);
  
  std::map<int,int>::iterator iter;
  int rowCounter = 0;
  for(iter=mapOutput.begin(); iter != mapOutput.end(); iter++){
    //Matlab stores column wise
    mxArray *key = mxCreateDoubleScalar(iter->first);
    mxSetCell(cell, rowCounter, key);
    mxArray *value = mxCreateDoubleScalar(iter->second);
    mxSetCell(cell, numRows + rowCounter, value);
    rowCounter ++;
  }
  checkOutputField(matlabOutput, fieldname);
  mxSetField(matlabOutput,index, fieldname, cell);
}

void setMap(mxArray *matlabOutput,
            std::map<std::string,int> &mapOutput,
            char *fieldname, const int index )
{
  size_t numRows = mapOutput.size();
  size_t numCols = 2;
  mwSize dims[] = {numRows, numCols};
  mxArray *cell = mxCreateCellArray(2, dims);
  
  std::map<std::string,int>::iterator iter;
  int rowCounter = 0;
  for(iter=mapOutput.begin(); iter != mapOutput.end(); iter++){
    mxArray *key = mxCreateString(iter->first.c_str());
    mxSetCell(cell, rowCounter , key);
    mxArray *value = mxCreateDoubleScalar(iter->second);
    mxSetCell(cell, numRows + rowCounter, value);
    rowCounter++;
  }
  checkOutputField(matlabOutput, fieldname);
  mxSetField(matlabOutput,index, fieldname, cell);
}