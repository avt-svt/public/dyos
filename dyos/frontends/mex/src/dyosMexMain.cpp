/**
* @file dyosMexMain.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechin, RWTH Aachen            \n
* =====================================================================\n
* DyOS Mex                                                             \n
* =====================================================================\n
* This file contains the main function for the mex file communication. \n
* =====================================================================\n
* @author Falco Jung, Tjalf Hoffmann, Adrian Caspari
* @date 22.03.2017
*/

#include "mex.h"
#include "ConvertInput.hpp"
#include "ConvertOutput.hpp"

#include <iostream>

#include "DyosWithLogger.hpp"
#include "UserInput.hpp"
#include "UserOutput.hpp"

class MexOutputChannel : public OutputChannel
{
public:
	virtual void print(const std::string &message)
	{
		mexPrintf(message.c_str());
		mexEvalString("drawnow;");
	}
	virtual void flush()
	{
		mexEvalString("drawnow;");
	};
};

void mexFunction( int nlhs, mxArray *plhs[], 
                  int nrhs, const mxArray *prhs[] )
     
{
	mexPrintf("Starting DyOS Mex call.\n");
    mexEvalString("drawnow;");

  if(nlhs != 1){
    std::cerr << "Wrong number of outputs. Exactly one output expected." ;
  }
  if(nrhs != 1){
	std::cerr << "Wrong number of inputs. Exactly one input expected." ;
  }
  plhs[0] = mxDuplicateArray(prhs[0]);


  UserInput::Input dyosIn;
  try{
    //copy input to output
    dyosIn = convertInput(prhs[0]);
  }
  catch(DyosMexException e){
	  std::cerr << "An error ocurred during input conversion" ;
  }
  catch(...)
  {
	  std::cerr << "Unknown error occurred during input conversion";
  }

  Logger::Ptr logger(new StandardLogger());

  OutputChannel::Ptr mexOut(new MexOutputChannel());

  OutputChannel::Ptr inputOut(new FileChannel("mexDyosInput.json"));
  inputOut->setVerbosity(OutputChannel::STANDARD_VERBOSITY);
  OutputChannel::Ptr outputOut(new FileChannel("mexDyosOutput.json"));
  outputOut->setVerbosity(OutputChannel::STANDARD_VERBOSITY);

  logger->setLoggingChannel(Logger::DYOS_OUT, mexOut);
  logger->setLoggingChannel(Logger::INTEGRATOR, mexOut);
  logger->setLoggingChannel(Logger::OPTIMIZER, mexOut);
  logger->setLoggingChannel(Logger::ESO, mexOut);
  logger->setLoggingChannel(Logger::INPUT_OUT_JSON, inputOut);
  logger->setLoggingChannel(Logger::FINAL_OUT_JSON, outputOut);
  
  std::cout << "Finished input conversion. \n";
  
  UserOutput::Output dyosOut;

  if(mxGetFieldNumber(prhs[0], "optimizeForwardWith2ndOrderIntegration")>-1){
    std::cout << "Optimize forward with 2nd order integration. \n" ;
	std::cout << "Starting DyOS run. \n";
    dyosOut = optimizeFirstForwardWithSecondOrderOutput(dyosIn, logger);
  }
  else{
	  std::cout << "Starting DyOS run. \n";
	  dyosOut= runDyos(dyosIn,logger);
  }
 
  std::cout << "Finished DyOS run. \n";

  convertOutput(plhs[0], dyosOut, 0);
  std::cout << "Finished output conversion. \n";

  mexPrintf("Finished DyOS Mex call. \n");
  mexEvalString("drawnow;");
}

