#pragma once

#include "mex.h"
#include "UserInput.hpp"

class DyosMexException : public std::exception
{
public:
  DyosMexException(){}
};

//convert Integrator functions
UserInput::EsoInput convertEsoInput(const mxArray *matlabInput);
UserInput::ParameterGridInput convertParameterGridInput(const mxArray *matlabInput, const int index);
UserInput::ParameterInput convertParameterInput(const mxArray *matlabInput, const int index);
UserInput::IntegratorStageInput convertIntegratorStageInput(const mxArray *matlabInput);
UserInput::IntegratorInput convertIntegratorInput(const mxArray *matlabInput);
UserInput::DaeInitializationInput convertDaeInitializationInput(const mxArray *matlabInput);
UserInput::LinearSolverInput convertLinearSolverInput(const mxArray *matlabInput);
UserInput::NonLinearSolverInput convertNonLinearSolverInput(const mxArray *matlabInput);
UserInput::AdaptationInput convertAdaptationInput(const mxArray *matlabInput);
UserInput::WaveletAdaptationInput convertWaveletAdaptationInput(const mxArray *matlabInput);

//convert Optimizer functions
UserInput::ConstraintInput convertConstraintInput(const mxArray *matlabInput, const int index);
UserInput::OptimizerStageInput convertOptimizerStageInput(const mxArray *matlabInput);
UserInput::OptimizerInput convertOptimizerInput(const mxArray *matlabInput);
UserInput::StructureDetectionInput convertStructureDetectionInput(const mxArray *matlabInput);

//general utility functions
UserInput::StageInput convertStageInput(const mxArray *matlabInput, const int index);
UserInput::Input convertInput(const mxArray *matlabInput);

//utility functions
bool existField(const mxArray *matlabInput, char *fieldname);
void checkField(const mxArray *matlabInput, char *fieldname);

std::string convertString(const mxArray *matlabInput, char *fieldname, const int index = 0);

double convertScalar(const mxArray *matlabInput, char *fieldname, const int index = 0);

bool isEmptyField(const mxArray *matlabInput, char *fieldname, const int index = 0);

std::vector<double> convertDoubleVector(const mxArray *matlabInput, char *fieldname, const int index = 0);

bool convertBool(const mxArray *matlabInput, char *fieldname, const int index = 0);

std::map<std::string, std::string> convertMap(const mxArray *matlabInput, char *fieldname, const int index = 0);

