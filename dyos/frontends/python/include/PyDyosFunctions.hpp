/**
* @file PyDyosFunctions.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* PyDyOS - part of the DyOS python interface                           \n
* =====================================================================\n
* Function and namespace definitions for python interface              \n
* =====================================================================\n
* @author Adrian Caspari and Felix Zimmermann
* @date 8.05.2019
*/




#pragma once 

#include <PyDyos.hpp>

/**
* @namespace pyDyosFunctions including all relevant wrapping functions for UserInput and UserOutput
*/
namespace pyDyosFunctions {

	/**
	*@brief getInputFromJson
	*/
	UserInput::Input getInputFromJson(std::string filename) {

		UserInput::Input input;
		convertUserInputFromXmlJson(filename, input, DataFormat::JSON);
		return input;
	}


	/**
	*@brief getOutputFromJson
	*/
	UserOutput::Output getOutputFromJson(std::string filename) {

		UserOutput::Output output;
		convertUserOutputFromXmlJson(filename, output, DataFormat::JSON);
		return output;
	}

	/**
	*@brief convertInputToJson
	*/
	void convertInputToJson(std::string filename, UserInput::Input input) {

		convertUserInputToXmlJson(filename, input, DataFormat::JSON);

	}

	/**
	*@brief convertOutputToJson
	*/
	void convertOutputToJson(std::string filename, UserOutput::Output output) {

		convertUserOutputToXmlJson(filename, output, DataFormat::JSON);

	}


	/**
	* @brief convert python list to std::vector
	*/
	template<typename T> inline std::vector< T > py_list_to_std_vector(const boost::python::list iterable)
	{
		return std::vector< T >(boost::python::stl_input_iterator< T >(iterable), boost::python::stl_input_iterator< T >());
	}

	/**
	* @brief convert std::vector to python list
	*/
	template <class T> inline boost::python::list std_vector_to_py_list(std::vector<T>& vector) {
		typename std::vector<T>::iterator iter;
		boost::python::list list;
		for (iter = vector.begin(); iter != vector.end(); ++iter) {
			list.append(*iter);
		}
		return list;
	}


	/**
	* @brief convert python list to std::map
	*/
	template<typename T, typename U> inline boost::python::list std_map_xx_to_py_list(std::map<T, U>& map_xx) {
		boost::python::list list;

		for (std::map<T, U>::iterator it = map_xx.begin(); it != map_xx.end(); ++it) {

			boost::python::list subList;
			subList.append(it->first);
			subList.append(it->second);
			list.append(subList);
		}

		return list;
	}

	/**
	* @brief convert std::map to python list
	*/
	template<typename T, typename U> inline std::map<T, U> py_list_to_std_map_xx(const boost::python::list list) {

		std::map<T, U> map;
		for (int i = 0; i < boost::python::len(list); i++) {
			map.insert(std::pair<T, U>(boost::python::extract<T>(list[i][0]), boost::python::extract<U>(list[i][1])));
		}

		return map;
	}

	/**
	* @brief convert C-Array T[] to python list
	*/
	template <class T> inline void array_pointer_to_py_list(int size, T* array, boost::python::list &list) {
		for (int i = 0; i < size; i++) {
			list.append(array[i]);
		}
	}

	/**
	* @brief convert C-Array T[] to python list
	*/
	template <class T> inline boost::python::list array_pointer_to_py_list(int size, T* array) {
		boost::python::list list;
		for (int i = 0; i < size; i++) {
			list.append(array[i]);
		}
		return list;
	}

	/**
	* @brief wrap function for UserInput::Input
	*/
	boost::python::list pydyosGetInputStages(UserInput::Input& input) {

		return std_vector_to_py_list(input.stages);

	}

	/**
	* @brief wrap function for UserInput::Input
	*/
	void pydyosSetInputStages(UserInput::Input& inputPy, boost::python::list inputStagesPy) {

		inputPy.stages = py_list_to_std_vector<UserInput::StageInput>(inputStagesPy);

	}


	// ***************** UserInput::ParameterGridInput ********************



	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	boost::python::list get_parameterGridInput_TimePoints(UserInput::ParameterGridInput& parameterGridInput) {

		return std_vector_to_py_list(parameterGridInput.timePoints);

	}

	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	void set_parameterGridInput_TimePoints(UserInput::ParameterGridInput& parameterGridInput, boost::python::list timePoints) {

		parameterGridInput.timePoints = py_list_to_std_vector<double>(timePoints);

	}



	// *************************************


	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	boost::python::list get_parameterGridInput_Values(UserInput::ParameterGridInput& parameterGridInput) {

		return std_vector_to_py_list(parameterGridInput.values);

	}

	/**
	* @brief wrap function for UserInput::ParameterGridInput
	*/
	void set_parameterGridInput_Values(UserInput::ParameterGridInput& parameterGridInput, boost::python::list values) {

		parameterGridInput.values = py_list_to_std_vector<double>(values);

	}


	// ******************* UserInput::ParameterInput ******************


	/**
	* @brief wrap function for UserInput::ParameterInput
	*/
	boost::python::list get_parameterInput_grids(UserInput::ParameterInput& parameterInput) {

		return std_vector_to_py_list(parameterInput.grids);

	}



	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_parameterInput_grids(UserInput::ParameterInput& parameterInput, boost::python::list grids) {

		parameterInput.grids = py_list_to_std_vector<UserInput::ParameterGridInput>(grids);

	}



	// ******************* UserInput::IntegratorStageInput ******************


	/**
	* @brief wrap function for UserInput::ParameterInput
	*/
	boost::python::list get_integratorStageInput_parameters(UserInput::IntegratorStageInput& integratorStageInput) {

		return std_vector_to_py_list(integratorStageInput.parameters);

	}

	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_integratorStageInput_parameters(UserInput::IntegratorStageInput& integratorStageInput, boost::python::list parameters) {

		integratorStageInput.parameters = py_list_to_std_vector<UserInput::ParameterInput>(parameters);

	}



	/**
	* @brief wrap function for UserInput::ParameterInput
	*/
	boost::python::list get_integratorStageInput_explicitPlotGrid(UserInput::IntegratorStageInput& integratorStageInput) {

		return std_vector_to_py_list(integratorStageInput.explicitPlotGrid);

	}

	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_integratorStageInput_explicitPlotGrid(UserInput::IntegratorStageInput& integratorStageInput, boost::python::list explicitPlotGrid) {

		integratorStageInput.explicitPlotGrid = py_list_to_std_vector<double>(explicitPlotGrid);

	}


	// ******************* UserInput::OptimizerStageInput ******************


	/**
	* @brief wrap function for UserInput::OptimizerStageInput
	*/
	boost::python::list get_OptimizerStageInput_constraints(UserInput::OptimizerStageInput& integratorStageInput) {

		return std_vector_to_py_list(integratorStageInput.constraints);

	}

	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void set_OptimizerStageInput_constraints(UserInput::OptimizerStageInput& integratorStageInput, boost::python::list constraints) {

		integratorStageInput.constraints = py_list_to_std_vector<UserInput::ConstraintInput>(constraints);

	}




	// ******************* UserInput::StageMapping ******************


	/**
	* @brief wrap function for UserInput::StageMapping
	*/
	boost::python::list get_StageMapping_stateNameMapping(UserInput::StageMapping& stageMapping) {

		boost::python::list list;

		for (std::map<std::string, std::string>::iterator it = stageMapping.stateNameMapping.begin(); it != stageMapping.stateNameMapping.end(); ++it) {

			boost::python::list subList;
			subList.append(it->first);
			subList.append(it->second);
			list.append(subList);
		}

		return list;

	}



	/**
	* @brief wrap function for UserInput::StageMapping
	*/
	void set_StageMapping_stateNameMapping(UserInput::StageMapping& stageMapping, boost::python::list stateNameMapping) {

		for (int i = 0; i < boost::python::len(stateNameMapping); i++) {

			stageMapping.stateNameMapping.insert(std::pair<std::string, std::string>(boost::python::extract<std::string>(stateNameMapping[i][0]), boost::python::extract<std::string>(stateNameMapping[i][1])));
		}

	}



	// ******************* UserInput::MappingInput ******************


	/**
	* @brief wrap function for UserInput::MappingInput
	*/
	boost::python::list get_MappingInput_stateNameMapping(UserInput::MappingInput& mappingInput) {

		boost::python::list list;

		for (std::map<std::string, std::string>::iterator it = mappingInput.stateNameMapping.begin(); it != mappingInput.stateNameMapping.end(); ++it) {

			boost::python::list subList;
			subList.append(it->first);
			subList.append(it->second);
			list.append(subList);
		}

		return list;

	}



	/**
	* @brief wrap function for UserInput::MappingInput
	*/
	void set_MappingInput_stateNameMapping(UserInput::MappingInput& mappingInput, boost::python::list stateNameMapping) {

		for (int i = 0; i < boost::python::len(stateNameMapping); i++) {

			mappingInput.stateNameMapping.insert(std::pair<std::string, std::string>(boost::python::extract<std::string>(stateNameMapping[i][0]), boost::python::extract<std::string>(stateNameMapping[i][1])));
		}

	}





	// ******************* UserInput::IntegratorInput ******************


	/**
	* @brief wrap function for UserInput::IntegratorInput
	*/
	boost::python::list get_IntegratorInput_integratorOptions(UserInput::IntegratorInput& integratorInput) {

		return std_map_xx_to_py_list(integratorInput.integratorOptions);

	}



	/**
	* @brief wrap function for UserInput::IntegratorInput
	*/
	void set_IntegratorInput_integratorOptions(UserInput::IntegratorInput& integratorInput, boost::python::list integratorOptions) {

		integratorInput.integratorOptions = py_list_to_std_map_xx<std::string, std::string>(integratorOptions);

	}










	// ******************* UserInput::OptimizerInput ******************


	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	boost::python::list get_OptimizerInput_optimizerOptions(UserInput::OptimizerInput& optimizerInput) {

		return std_map_xx_to_py_list(optimizerInput.optimizerOptions);

	}



	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	void set_OptimizerInput_optimizerOptions(UserInput::OptimizerInput& optimizerInput, boost::python::list optimizerOptions) {

		optimizerInput.optimizerOptions = py_list_to_std_map_xx<std::string, std::string>(optimizerOptions);

	}

	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	boost::python::list get_OptimizerInput_globalConstraints(UserInput::OptimizerInput& optimizerInput) {

		return std_vector_to_py_list(optimizerInput.globalConstraints);

	}



	/**
	* @brief wrap function for UserInput::OptimizerInput
	*/
	void set_OptimizerInput_globalConstraints(UserInput::OptimizerInput& optimizerInput, boost::python::list globalConstraints) {

		optimizerInput.globalConstraints = py_list_to_std_vector<UserInput::ConstraintInput>(globalConstraints);

	}





	// ******************* UserInput::ConstraintInput ******************


	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	boost::python::list pydyosGetLagrangeMultiplierVector(UserInput::ConstraintInput& constraintInput) {

		return std_vector_to_py_list(constraintInput.lagrangeMultiplierVector);

	}



	/**
	* @brief wrap function for UserInput::ConstraintInput
	*/
	void pydyosSetLagrangeMultiplierVector(UserInput::ConstraintInput& constraintInput, boost::python::list constraintInputList) {

		constraintInput.lagrangeMultiplierVector = py_list_to_std_vector<double>(constraintInputList);

	}


	// ******************* UserOutput ******************


	// ******************* UserOutput::Output ******************


	/**
	* @brief wrap function for UserOutput::Output
	*/
	boost::python::list get_UserOutput_SolutionHistory(UserOutput::Output& output) {

		return std_vector_to_py_list(output.solutionHistory);

	}

	/**
	* @brief wrap function for UserOutput::Output
	*/
	void set_UserOutput_SolutionHistory(UserOutput::Output& output, boost::python::list solutionHistory) {

		output.solutionHistory = py_list_to_std_vector<UserOutput::Output>(solutionHistory);

	}

	/**
	* @brief wrap function for UserOutput::Output
	*/
	boost::python::list get_UserOutput_stages(UserOutput::Output& output) {

		return std_vector_to_py_list(output.stages);

	}

	/**
	* @brief wrap function for UserOutput::Output
	*/
	void set_UserOutput_stages(UserOutput::Output& output, boost::python::list stages) {

		output.stages = py_list_to_std_vector<UserOutput::StageOutput>(stages);

	}



	// ******************* UserOutput::MappingOutput ******************


	/**
	* @brief wrap function for UserOutput::MappingOutput
	*/
	boost::python::list get_MappingOutput_stateNameMapping(UserOutput::MappingOutput& mappingOutput) {

		return std_map_xx_to_py_list(mappingOutput.stateNameMapping);

	};

	/**
	* @brief wrap function for UserOutput::MappingOutput
	*/
	void set_MappingOutput_stateNameMapping(UserOutput::MappingOutput& mappingOutput, boost::python::list stateNameMapping) {

		mappingOutput.stateNameMapping = py_list_to_std_map_xx<std::string, std::string>(stateNameMapping);

	};


	// ******************* UserOutput::StateGridOutput ******************


	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	boost::python::list get_StateGridOutput_timePoints(UserOutput::StateGridOutput& stateGridOutput) {

		return std_vector_to_py_list(stateGridOutput.timePoints);

	};

	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	void set_StateGridOutput_timePoints(UserOutput::StateGridOutput& stateGridOutput, boost::python::list timePoints) {

		stateGridOutput.timePoints = py_list_to_std_vector<double>(timePoints);

	};


	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	boost::python::list get_StateGridOutput_values(UserOutput::StateGridOutput& stateGridOutput) {

		return std_vector_to_py_list(stateGridOutput.values);

	};

	/**
	* @brief wrap function for UserOutput::StateGridOutput
	*/
	void set_StateGridOutput_values(UserOutput::StateGridOutput& stateGridOutput, boost::python::list values) {

		stateGridOutput.timePoints = py_list_to_std_vector<double>(values);

	};




	// ******************* UserOutput::FirstSensitivityOutput ******************


	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	boost::python::list get_FirstSensitivityOutput_mapParamsCols(UserOutput::FirstSensitivityOutput& firstSensitivityOutput) {

		return std_map_xx_to_py_list(firstSensitivityOutput.mapParamsCols);

	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	void set_FirstSensitivityOutput_mapParamsCols(UserOutput::FirstSensitivityOutput& firstSensitivityOutput, boost::python::list mapParamsCols) {

		firstSensitivityOutput.mapParamsCols = py_list_to_std_map_xx<int, int>(mapParamsCols);

	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	boost::python::list get_FirstSensitivityOutput_mapConstrRows(UserOutput::FirstSensitivityOutput& firstSensitivityOutput) {

		return std_map_xx_to_py_list(firstSensitivityOutput.mapConstrRows);

	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	void set_FirstSensitivityOutput_mapConstrRows(UserOutput::FirstSensitivityOutput& firstSensitivityOutput, boost::python::list mapConstrRows) {

		firstSensitivityOutput.mapConstrRows = py_list_to_std_map_xx<std::string, int>(mapConstrRows);

	};


	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	boost::python::list get_FirstSensitivityOutput_values(UserOutput::FirstSensitivityOutput& firstSensitivityOutput) {

		boost::python::list thislist;

		for (int i = 0; i<firstSensitivityOutput.values.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(firstSensitivityOutput.values[i]);
			thislist.append(sublist);
		}

		return thislist;
	};

	/**
	* @brief wrap function for UserOutput::FirstSensitivityOutput
	*/
	void set_FirstSensitivityOutput_values(UserOutput::FirstSensitivityOutput& firstSensitivityOutput, boost::python::list values) {

		firstSensitivityOutput.values.resize(boost::python::len(values));

		for (int i = 0; i < firstSensitivityOutput.values.size(); i++) {

			firstSensitivityOutput.values[i].resize(boost::python::len(values[i]));
			for (int j = 0; j < firstSensitivityOutput.values[i].size(); j++) {

				firstSensitivityOutput.values[i][j] = boost::python::extract<double>(values[i][j]);

			}
		}

	};




	// ******************* UserOutput::ParameterGridOutput ******************


	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_timePoints(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.timePoints);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_timePoints(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list timePoints) {

		parameterGridOutput.timePoints = py_list_to_std_vector<double>(timePoints);

	};



	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_values(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.values);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_values(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list values) {

		parameterGridOutput.values = py_list_to_std_vector<double>(values);

	};



	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_lagrangeMultipliers(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.lagrangeMultipliers);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_lagrangeMultipliers(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list lagrangeMultipliers) {

		parameterGridOutput.lagrangeMultipliers = py_list_to_std_vector<double>(lagrangeMultipliers);

	};


	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	boost::python::list get_ParameterGridOutput_firstSensitivities(UserOutput::ParameterGridOutput& parameterGridOutput) {

		return std_vector_to_py_list(parameterGridOutput.firstSensitivities);

	};

	/**
	* @brief wrap function for UserOutput::ParameterGridOutput
	*/
	void set_ParameterGridOutput_firstSensitivities(UserOutput::ParameterGridOutput& parameterGridOutput, boost::python::list firstSensitivities) {

		parameterGridOutput.firstSensitivities = py_list_to_std_vector<UserOutput::FirstSensitivityOutput>(firstSensitivities);

	};





	// ******************* UserOutput::ParameterOutput ******************


	/**
	* @brief wrap function for UserOutput::ParameterOutput
	*/
	boost::python::list get_ParameterOutput_grids(UserOutput::ParameterOutput& parameterOutput) {

		return std_vector_to_py_list(parameterOutput.grids);

	};

	/**
	* @brief wrap function for UserOutput::ParameterOutput
	*/
	void set_ParameterOutput_grids(UserOutput::ParameterOutput& parameterOutput, boost::python::list grids) {

		parameterOutput.grids = py_list_to_std_vector<UserOutput::ParameterGridOutput>(grids);

	};



	// ******************* UserOutput::IntegratorStageOutput ******************


	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_duration(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.durations);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_duration(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list durations) {

		integratorStageOutput.durations = py_list_to_std_vector<UserOutput::ParameterOutput>(durations);

	};



	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_parameters(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.parameters);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_parameters(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list parameters) {

		integratorStageOutput.parameters = py_list_to_std_vector<UserOutput::ParameterOutput>(parameters);

	};



	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_states(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.states);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_states(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list states) {

		integratorStageOutput.states = py_list_to_std_vector<UserOutput::StateOutput>(states);

	};



	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	boost::python::list get_IntegratorStageOutput_adjoints(UserOutput::IntegratorStageOutput& integratorStageOutput) {

		return std_vector_to_py_list(integratorStageOutput.adjoints);

	};

	/**
	* @brief wrap function for UserOutput::IntegratorStageOutput
	*/
	void set_IntegratorStageOutput_adjoints(UserOutput::IntegratorStageOutput& integratorStageOutput, boost::python::list adjoints) {

		integratorStageOutput.adjoints = py_list_to_std_vector<UserOutput::StateGridOutput>(adjoints);

	};




	// ******************* UserOutput::ConstraintGridOutput ******************


	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	boost::python::list get_ConstraintGridOutput_timePoints(UserOutput::ConstraintGridOutput& constraintGridOutput) {

		return std_vector_to_py_list(constraintGridOutput.timePoints);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	void set_ConstraintGridOutput_timePoints(UserOutput::ConstraintGridOutput& constraintGridOutput, boost::python::list timePoints) {

		constraintGridOutput.timePoints = py_list_to_std_vector<double>(timePoints);

	};


	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	boost::python::list get_ConstraintGridOutput_values(UserOutput::ConstraintGridOutput& constraintGridOutput) {

		return std_vector_to_py_list(constraintGridOutput.values);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	void set_ConstraintGridOutput_values(UserOutput::ConstraintGridOutput& constraintGridOutput, boost::python::list values) {

		constraintGridOutput.values = py_list_to_std_vector<double>(values);

	};


	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	boost::python::list get_ConstraintGridOutput_lagrangeMultiplier(UserOutput::ConstraintGridOutput& constraintGridOutput) {

		return std_vector_to_py_list(constraintGridOutput.lagrangeMultiplier);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintGridOutput
	*/
	void set_ConstraintGridOutput_lagrangeMultiplier(UserOutput::ConstraintGridOutput& constraintGridOutput, boost::python::list lagrangeMultiplier) {

		constraintGridOutput.lagrangeMultiplier = py_list_to_std_vector<double>(lagrangeMultiplier);

	};



	// ******************* UserOutput::ConstraintOutput ******************


	/**
	* @brief wrap function for UserOutput::ConstraintOutput
	*/
	boost::python::list get_ConstraintOutput_grids(UserOutput::ConstraintOutput& constraintOutput) {

		return std_vector_to_py_list(constraintOutput.grids);

	};

	/**
	* @brief wrap function for UserOutput::ConstraintOutput
	*/
	void set_ConstraintOutput_grids(UserOutput::ConstraintOutput& constraintOutput, boost::python::list grids) {

		constraintOutput.grids = py_list_to_std_vector<UserOutput::ConstraintGridOutput>(grids);

	};





	// ******************* UserOutput::OptimizerStageOutput ******************


	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	boost::python::list get_OptimizerStageOutput_nonlinearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput) {

		return std_vector_to_py_list(optimizerStageOutput.nonlinearConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	void set_OptimizerStageOutput_nonlinearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput, boost::python::list nonlinearConstraints) {

		optimizerStageOutput.nonlinearConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(nonlinearConstraints);

	};


	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	boost::python::list get_OptimizerStageOutput_linearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput) {

		return std_vector_to_py_list(optimizerStageOutput.linearConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerStageOutput
	*/
	void set_OptimizerStageOutput_linearConstraints(UserOutput::OptimizerStageOutput& optimizerStageOutput, boost::python::list linearConstraints) {

		optimizerStageOutput.linearConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(linearConstraints);

	};




	// ******************* UserOutput::StageOutput ******************


	/**
	* @brief wrap function for UserOutput::StageOutput
	*/
	boost::python::list get_StageOutput_stageGrid(UserOutput::StageOutput& stageOutput) {

		return std_vector_to_py_list(stageOutput.stageGrid);

	};

	/**
	* @brief wrap function for UserOutput::StageOutput
	*/
	void set_StageOutput_stageGrid(UserOutput::StageOutput& stageOutput, boost::python::list stageGrid) {

		stageOutput.stageGrid = py_list_to_std_vector<double>(stageGrid);

	};

	GenericEso::Ptr get_StageOutput_GenericEso(UserOutput::StageOutput& stageOutput) {
		return stageOutput.esoPtr;
	}


	// ******************* UserOutput::SecondOrderOutput ******************


	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_indicesHessian(UserOutput::SecondOrderOutput& secondOrderOutput) {

		return std_map_xx_to_py_list(secondOrderOutput.indicesHessian);

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_indicesHessian(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list indicesHessian) {

		secondOrderOutput.indicesHessian = py_list_to_std_map_xx<std::string, int>(indicesHessian);

	};


	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_values(UserOutput::SecondOrderOutput& secondOrderOutput) {

		boost::python::list thislist;

		for (int i = 0; i<secondOrderOutput.values.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(secondOrderOutput.values[i]);
			thislist.append(sublist);
		}

		return thislist;

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_values(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list values) {


		secondOrderOutput.values.resize(boost::python::len(values));

		for (int i = 0; i < secondOrderOutput.values.size(); i++) {

			secondOrderOutput.values[i].resize(boost::python::len(values[i]));
			for (int j = 0; j < secondOrderOutput.values[i].size(); j++) {

				secondOrderOutput.values[i][j] = boost::python::extract<double>(values[i][j]);

			}
		}

	};



	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_constParamHessian(UserOutput::SecondOrderOutput& secondOrderOutput) {

		boost::python::list thislist;

		for (int i = 0; i<secondOrderOutput.constParamHessian.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(secondOrderOutput.constParamHessian[i]);
			thislist.append(sublist);
		}

		return thislist;

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_constParamHessian(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list constParamHessian) {


		secondOrderOutput.constParamHessian.resize(boost::python::len(constParamHessian));

		for (int i = 0; i < secondOrderOutput.constParamHessian.size(); i++) {

			secondOrderOutput.constParamHessian[i].resize(boost::python::len(constParamHessian[i]));
			for (int j = 0; j < secondOrderOutput.constParamHessian[i].size(); j++) {

				secondOrderOutput.constParamHessian[i][j] = boost::python::extract<double>(constParamHessian[i][j]);

			}
		}

	};





	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	boost::python::list get_SecondOrderOutput_compositeAdjoints(UserOutput::SecondOrderOutput& secondOrderOutput) {

		boost::python::list thislist;

		for (int i = 0; i<secondOrderOutput.compositeAdjoints.size(); i++) {
			boost::python::list sublist = std_vector_to_py_list(secondOrderOutput.compositeAdjoints[i]);
			thislist.append(sublist);
		}

		return thislist;

	};

	/**
	* @brief wrap function for UserOutput::SecondOrderOutput
	*/
	void set_SecondOrderOutput_compositeAdjoints(UserOutput::SecondOrderOutput& secondOrderOutput, boost::python::list compositeAdjoints) {


		secondOrderOutput.compositeAdjoints.resize(boost::python::len(compositeAdjoints));

		for (int i = 0; i < secondOrderOutput.compositeAdjoints.size(); i++) {

			secondOrderOutput.compositeAdjoints[i].resize(boost::python::len(compositeAdjoints[i]));
			for (int j = 0; j < secondOrderOutput.compositeAdjoints[i].size(); j++) {

				secondOrderOutput.compositeAdjoints[i][j] = boost::python::extract<double>(compositeAdjoints[i][j]);

			}
		}

	};







	// ******************* UserOutput::OptimizerOutput ******************


	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	boost::python::list get_OptimizerOutput_globalConstraints(UserOutput::OptimizerOutput& optimizerOutput) {

		return std_vector_to_py_list(optimizerOutput.globalConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	void set_OptimizerOutput_globalConstraints(UserOutput::OptimizerOutput& optimizerOutput, boost::python::list globalConstraints) {

		optimizerOutput.globalConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(globalConstraints);

	};


	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	boost::python::list get_OptimizerOutput_linearConstraints(UserOutput::OptimizerOutput& optimizerOutput) {

		return std_vector_to_py_list(optimizerOutput.linearConstraints);

	};

	/**
	* @brief wrap function for UserOutput::OptimizerOutput
	*/
	void set_OptimizerOutput_linearConstraints(UserOutput::OptimizerOutput& optimizerOutput, boost::python::list linearConstraints) {

		optimizerOutput.linearConstraints = py_list_to_std_vector<UserOutput::ConstraintOutput>(linearConstraints);

	};

	// ******************* GenericEso ******************

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getVariableNames(GenericEso &genEso) {
		std::vector<string> variableNames;
		genEso.getVariableNames(variableNames);
		return std_vector_to_py_list(variableNames);
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getResiduals(GenericEso &genEso) {
		EsoIndex n_eq = genEso.getNumEquations();
		EsoDouble* residuals = new EsoDouble[n_eq];
		genEso.getAllResiduals(n_eq, residuals);
		boost::python::list list = array_pointer_to_py_list(n_eq, residuals);
		delete[] residuals;
		return list;
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getJacobian(GenericEso &genEso) {
		EsoIndex n_nz = genEso.getNumNonZeroes();
		EsoIndex n_eq = genEso.getNumEquations();
		EsoIndex n_vars = genEso.getNumVariables();
		std::vector<EsoIndex> rowIdx(n_nz);
		std::vector<EsoIndex> colIdx(n_nz);
		std::vector<EsoDouble> values(n_nz);

		genEso.getJacobianStruct(n_nz, rowIdx.data(), colIdx.data());
		genEso.getJacobianValues(n_nz, values.data());

		std::vector<EsoDouble> rowArray(n_vars);
		boost::python::list rowList;
		boost::python::list jacobian;

	

		// loop over all nonzero entries in the jacobian.
		for (int i = 0; i < n_eq; i++) {
			std::fill(rowArray.begin(), rowArray.end(), 0);
			//rowArray.resize(n_vars, 0.0);
			for (int j = 0; j < n_nz; j++) {
				if (rowIdx[j] == i) {
					rowArray[colIdx[j]] = values[j];
				}
			}

			rowList = array_pointer_to_py_list(n_vars, rowArray.data());
			jacobian.append(rowList);


		}




		return jacobian;
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getDiffJacobian(GenericEso &genEso) {
		EsoIndex n_diff_nz = genEso.getNumDifferentialNonZeroes();
		EsoIndex n_diff_eq = genEso.getNumDiffEquations();

		std::vector<EsoIndex> rowIdx(n_diff_nz);
		std::vector<EsoIndex> colIdx(n_diff_nz);
		std::vector<EsoDouble> values(n_diff_nz);

		genEso.getDiffJacobianStruct(n_diff_nz, rowIdx.data(), colIdx.data());
		genEso.getDiffJacobianValues(n_diff_nz, values.data());

		std::vector<EsoIndex> rowArray(n_diff_eq);

		boost::python::list rowList;
		boost::python::list jacobian;

		int k = 0;
		for (int i = 0; i < n_diff_eq; i++) {
			for (int j = 0; j < n_diff_eq; j++) {
				rowArray[j] = 0;
				if (k < n_diff_nz && rowIdx[k] == i && colIdx[k] == j) {
					rowArray[j] = values[k];
					k++;
				}
			}
			rowList = array_pointer_to_py_list(n_diff_eq, rowArray.data());
			jacobian.append(rowList);

		}


		return jacobian;
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getStateValues(GenericEso &genEso) {
		EsoIndex n_s = genEso.getNumEquations();

		std::vector<EsoDouble> stateValues(n_s, 0);


		genEso.getStateValues(n_s, stateValues.data());


		boost::python::list list = array_pointer_to_py_list(n_s, stateValues.data());


		return list;

	}

	/**
	* @brief wrap function for GenericEso
	*/
	void GenericEso_setStateValues(GenericEso &genEso, boost::python::list states_list) {
		EsoIndex n_s = genEso.getNumStates();
		std::vector<EsoDouble> states = py_list_to_std_vector<EsoDouble>(states_list);

		genEso.setStateValues(n_s, states.data());
	}

	/**
	* @brief wrap function for GenericEso
	*/
	boost::python::list GenericEso_getParameterValues(GenericEso &genEso) {
		EsoIndex n_p = genEso.getNumParameters();
		std::vector<EsoDouble> parameterValues(n_p, 0);

		genEso.getParameterValues(n_p, parameterValues.data());

		boost::python::list list = array_pointer_to_py_list(n_p, parameterValues.data());

		return list;

	}

	/**
	* @brief wrap function for GenericEso
	*/
	void GenericEso_setParameterValues(GenericEso &genEso, boost::python::list states_list) {
		EsoIndex n_s = genEso.getNumStates();
		std::vector<EsoDouble> parameterValues = py_list_to_std_vector<EsoDouble>(states_list);

		genEso.setParameterValues(n_s, parameterValues.data());
	}


	/**
	* @brief wrap function for GenericEso
	*/
	void GenericEso_setParameter(GenericEso &genEso, int idx, double value) {
		int param_idx = genEso.getNumStates() + idx;
		genEso.setVariableValues(1, &value, &param_idx);
	}

	/**
	* @brief wrap function for GenericEso
	*/
	void GenericEso_setState(GenericEso &genEso, int idx, double value) {
		genEso.setVariableValues(1, &value, &idx);
	}

}
