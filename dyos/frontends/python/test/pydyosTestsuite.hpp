/**
* @file TestPydyos.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic TestPydyos - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the pydyos module	                             \n
* =====================================================================\n
* @author Adrian Caspari
* @date 24.05.2018
*/


#include "PyDyos.hpp"
#include "PyDyosFunctions.hpp"
using namespace pyDyosFunctions;

#include "GenericEsoFactory.hpp"
#include <vector>

#include "Array.hpp"
#include "FMIGenericEsoTestConfig.hpp"
using namespace std;

#include "boost/test/unit_test.hpp"
#include "boost/test/tools/floating_point_comparison.hpp""

#include <stdlib.h>
#include <stdio.h>
#include <time.h>


BOOST_AUTO_TEST_SUITE(TestPydyos)



BOOST_AUTO_TEST_CASE(TestInitializationPyDyos) {

	printf("Boost Test TestInitializationPyDyos. \n");

	BOOST_REQUIRE_NO_THROW(PyDyos pydyos(););
}

#ifdef BUILD_WITH_FMU
BOOST_AUTO_TEST_CASE(TestPyDyosFMIGenericEsoGetVariableNames) {

	printf("Boost Test TestPyDyosFMIGenericEsoGetVariableNames. \n");

	Py_Initialize();

	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8);

	vector<string> variableNamesPy;
	boost::python::list list = GenericEso_getVariableNames(*genericEso);
	variableNamesPy = py_list_to_std_vector<string>(list);

	int n = variableNamesPy.size();
	BOOST_CHECK_EQUAL(n, genericEso->getNumVariables());
	vector<string> variableNames;
	genericEso->getVariableNames(variableNames);

	for (int i = 0; i < n; i++)
		BOOST_CHECK_EQUAL(variableNames[i], variableNamesPy[i]);

	delete genericEso;

}
#endif 

#ifdef BUILD_WITH_FMU
BOOST_AUTO_TEST_CASE(TestPyDyosFMIGenericEsoGetResiduals) {

	printf("Boost Test TestPyDyosFMIGenericEsoGetResiduals. \n");

	Py_Initialize();

	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8);

	vector<double> residualsPy;
	boost::python::list list = GenericEso_getResiduals(*genericEso);
	residualsPy = py_list_to_std_vector<double>(list);

	int n = residualsPy.size();
	BOOST_CHECK_EQUAL(n, genericEso->getNumEquations());
	vector<double> residuals(n,0);
	genericEso->getAllResiduals(n, residuals.data());

	for (int i = 0; i < n; i++)
		BOOST_CHECK_EQUAL(residuals[i], residualsPy[i]);

	delete genericEso;

}
#endif 

#ifdef BUILD_WITH_FMU
BOOST_AUTO_TEST_CASE(TestPyDyosFMIGenericEsoGetJacobian) {

	printf("Boost Test TestPyDyosFMIGenericEsoGetJacobian. \n");

	Py_Initialize();

	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8);

	vector<vector<double> > jacobianPy;
	vector<boost::python::list> jacobianRowPy;
	boost::python::list list = GenericEso_getJacobian(*genericEso);
    jacobianRowPy = py_list_to_std_vector<boost::python::list>(list);
	jacobianPy.resize(jacobianRowPy.size());
	for(int i = 0; i<jacobianPy.size(); i++)
		jacobianPy[i] = py_list_to_std_vector<double>(jacobianRowPy[i]);

	int n = jacobianPy.size();
	BOOST_CHECK_EQUAL(n, genericEso->getNumEquations());
	for(int i = 0; i<n; i++)
		BOOST_CHECK_EQUAL(jacobianPy[i].size(), genericEso->getNumVariables());

	int nnz = genericEso->getNumNonZeroes();
	vector<double> values(nnz);
	vector<int> rowIdx(nnz);
	vector<int> colIdx(nnz);
	genericEso->getJacobianStruct(nnz, rowIdx.data(), colIdx.data());
	genericEso->getJacobianValues(nnz, values.data());

	int k = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < genericEso->getNumVariables(); j++) {
			if (k < values.size()) {
				if (i == rowIdx[k] && j == colIdx[k]) {
					BOOST_CHECK_EQUAL(jacobianPy[i][j], values[k]);
					k += 1;
				}
			}
			else
				BOOST_CHECK_EQUAL(jacobianPy[i][j], 0);
		}
	}		

	delete genericEso;

}
#endif 

#ifdef BUILD_WITH_FMU
BOOST_AUTO_TEST_CASE(TestPyDyosFMIGenericEsoGetDiffJacobian) {

	printf("Boost Test TestPyDyosFMIGenericEsoGetDiffJacobian. \n");

	Py_Initialize();

	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8);

	vector<vector<double> > jacobianPy;
	vector<boost::python::list> jacobianRowPy;
	boost::python::list list = GenericEso_getDiffJacobian(*genericEso);
	jacobianRowPy = py_list_to_std_vector<boost::python::list>(list);
	jacobianPy.resize(jacobianRowPy.size());
	for (int i = 0; i<jacobianPy.size(); i++)
		jacobianPy[i] = py_list_to_std_vector<double>(jacobianRowPy[i]);

	int n = jacobianPy.size();
	BOOST_CHECK_EQUAL(n, genericEso->getNumDiffEquations());
	for (int i = 0; i<n; i++)
		BOOST_CHECK_EQUAL(jacobianPy[i].size(), genericEso->getNumDiffEquations());

	int nnz = genericEso->getNumDifferentialNonZeroes();
	vector<double> values(nnz, 0);
	vector<int> rowIdx(nnz, 0);
	vector<int> colIdx(nnz, 0);
	genericEso->getDiffJacobianStruct(nnz, rowIdx.data(), colIdx.data());
	genericEso->getDiffJacobianValues(nnz, values.data());

	int k = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i == rowIdx[k] && j == colIdx[k])
				BOOST_CHECK_EQUAL(jacobianPy[i][j], values[k++]);
			else
				BOOST_CHECK_EQUAL(jacobianPy[i][j], 0);
		}
	}

	delete genericEso;

}
#endif 

#ifdef BUILD_WITH_FMU
BOOST_AUTO_TEST_CASE(TestPyDyosFMIGenericEsoGetStateValues) {
	
	printf("Boost Test TestPyDyosFMIGenericEsoGetStateValues. \n");
	
	Py_Initialize();

	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8);

	vector<double> stateValuesPy;
	boost::python::list list = GenericEso_getStateValues(*genericEso);
	stateValuesPy = py_list_to_std_vector<double>(list);

	int n = stateValuesPy.size();
	BOOST_CHECK_EQUAL(n, genericEso->getNumStates());
	vector<double> stateValues(n,0);
	genericEso->getStateValues(n, stateValues.data());

	for (int i = 0; i < n; i++)
		BOOST_CHECK_EQUAL(stateValues[i], stateValuesPy[i]);

	delete genericEso;
}
#endif 

#ifdef BUILD_WITH_FMU
BOOST_AUTO_TEST_CASE(TestPyDyosFMIGenericEsoSetStateValues) {
	
	printf("Boost Test TestPyDyosFMIGenericEsoSetStateValues \n");

	Py_Initialize();

	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8);

	int n = genericEso->getNumStates();
	vector<double> stateValuesIn(n,0);

	srand(time(NULL));

	for (int i = 0; i < n; i++) {
		stateValuesIn[i] = 100 * static_cast<double>(rand()) / RAND_MAX;
	}

	boost::python::list listIn = std_vector_to_py_list(stateValuesIn);
	GenericEso_setStateValues(*genericEso, listIn);

	vector<double> stateValuesOut(n,0);
	genericEso->getStateValues(n, stateValuesOut.data());

	for (int i = 0; i < n; i++)
		BOOST_CHECK_EQUAL(stateValuesIn[i], stateValuesOut[i]);

	delete genericEso;
}
#endif 

BOOST_AUTO_TEST_SUITE_END()





