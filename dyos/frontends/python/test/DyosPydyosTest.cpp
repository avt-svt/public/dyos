/**
* @file DyosFmiLoaderTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* pydyos part of Dyos                       \n
* =====================================================================\n
* testsuite     for the pydyos module	                             \n
* =====================================================================\n
* @author Adrian Caspari, Felix Zimmermann
* @date 18.12.2018
*/

#define BOOST_TEST_MODULE TEST_pydyos

#include "boost/test/unit_test.hpp"




#include "pydyosTestsuite.hpp"