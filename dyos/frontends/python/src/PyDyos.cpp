/**
* @file PyDyOS.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* PyDyOS - Python Frontend of DyOS                    \n
* =====================================================================\n
* @author Adrian Caspari and Yannic Martin Perez, Felix Zimmermann
* @date 27.04.2018
*/


#include <PyDyos.hpp>
#include <PyDyosFunctions.hpp>

std::ostream& operator<<(std::ostream& os, const boost::python::object& o)
{
	return os << boost::python::extract<std::string>(boost::python::str(o))();
}

/**
* @brief runPyDyOS function to start dyos - runDyos
*/
void PyDyos::runPyDyos()
{

	jsonFile = OutputChannel::Ptr(new FileChannel(this->nameOutputFile));

	logger = Logger::Ptr(new StandardLogger());


	channelIndex = Logger::FINAL_OUT_JSON;

	logger->setLoggingChannel(channelIndex, jsonFile);

	Output = runDyos(Input, logger);

};


/**
* @brief runPyDyOS function to start dyos - runDyos
*/
void printString() {

	std::cout << "print something. \n";

};

UserInput::Input PyDyos::getInput() { return this->Input; };

void PyDyos::setInput(UserInput::Input input) { this->Input = input; };


UserOutput::Output PyDyos::getOutput() { return this->Output; };

void PyDyos::setOutput(UserOutput::Output output) { this->Output = output; };

/**
* @brief convertOutputToInput function convert output to input
*/
UserInput::Input PyDyos::convertOutputToInput(UserOutput::Output output)
{
	return this->ioConversion.o2iConvertOutputToInput(output);
};


/**
*@brief setter for nameOutputFile
*/
void PyDyos::setNameOutputFile(std::string nameOutputFile) {

	this->nameOutputFile = nameOutputFile;
}

/**
*@brief getter for nameOutputFile
*/
std::string PyDyos::getNameOutputFile() {

	return this->nameOutputFile;
}















/**
* @brief pydyos is the main python module exposed 
*/
BOOST_PYTHON_MODULE(pydyos)
{

	// wrappers for std vectors
	boost::python::class_<std::vector<double> >("std::vector<double> ")
		.def(boost::python::vector_indexing_suite<std::vector<double>>())
		;

	boost::python::class_<std::vector<UserInput::StageInput> >("std::vector<UserInput::StageInput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserInput::StageInput>>())
		;

	boost::python::class_<std::vector<UserInput::ParameterGridInput> >("std::vector<UserInput::ParameterGridInput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserInput::ParameterGridInput>>())
		;

	boost::python::class_<std::vector<UserInput::ParameterInput> >("std::vector<UserInput::ParameterInput> ")
		.def(boost::python::vector_indexing_suite<std::vector<UserInput::ParameterInput>>())
		;

	boost::python::class_<std::vector<UserInput::ConstraintInput> >("std::vector<UserInput::ConstraintInput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserInput::ConstraintInput>>())
		;

	boost::python::class_<std::map<std::string,std::string> >("std::map<std::string,std::string>")
		.def(boost::python::map_indexing_suite<std::map<std::string, std::string> >())
		;

	boost::python::class_<std::map<std::string, int> >("std::map<std::string,int>")
		.def(boost::python::map_indexing_suite<std::map<std::string, int> >())
		;

	boost::python::class_<std::map<int, int> >("std::map<int,int>")
		.def(boost::python::map_indexing_suite<std::map<int, int> >())
		;
	
	boost::python::class_<std::vector<UserOutput::Output> >("std::vector<UserOutput::Output>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::Output>>())
		;

	boost::python::class_<std::vector<UserOutput::FirstSensitivityOutput> >("std::vector<UserOutput::FirstSensitivityOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::FirstSensitivityOutput>>())
		;

	boost::python::class_<std::vector<UserOutput::ParameterGridOutput> >("std::vector<UserOutput::ParameterGridOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::ParameterGridOutput>>())
		;

	boost::python::class_<std::vector<UserOutput::ParameterOutput> >("std::vector<UserOutput::ParameterOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::ParameterOutput>>())
		;

	boost::python::class_<std::vector<UserOutput::StateOutput> >("std::vector<UserOutput::StateOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::StateOutput>>())
		;


	boost::python::class_<std::vector<UserOutput::StateGridOutput> >("std::vector<UserOutput::StateGridOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::StateGridOutput>>())
		;

	boost::python::class_<std::vector<UserOutput::ConstraintGridOutput> >("std::vector<UserOutput::ConstraintGridOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::ConstraintGridOutput>>())
		;


	boost::python::class_<std::vector<UserOutput::ConstraintOutput> >("std::vector<UserOutput::ConstraintOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::ConstraintOutput>>())
		;

	boost::python::class_<std::vector<UserOutput::StageOutput> >("std::vector<UserOutput::StageOutput>")
		.def(boost::python::vector_indexing_suite<std::vector<UserOutput::StageOutput>>())
		;

	
	//boost::python::def("printString", printString);

	boost::python::class_<PyDyos>("PyDyos")
		.add_property("Input", &PyDyos::getInput, &PyDyos::setInput)
		.add_property("Output", &PyDyos::getOutput, &PyDyos::setOutput)
		.def("runPyDyos", &PyDyos::runPyDyos)
		.add_property("nameOutputFile",&PyDyos::getNameOutputFile, &PyDyos::setNameOutputFile)
		.def("convertOutputToInput", &PyDyos::convertOutputToInput)
		;

	boost::python::def("getInputFromJson", &pyDyosFunctions::getInputFromJson);
	boost::python::def("getOutputFromJson", &pyDyosFunctions::getOutputFromJson);
	boost::python::def("convertInputToJson", &pyDyosFunctions::convertInputToJson);
	boost::python::def("convertOutputToJson", &pyDyosFunctions::convertOutputToJson);
	
	{
		/**
		* @brief expose Input
		*/
		boost::python::scope in_Input = boost::python::class_<UserInput::Input> ("Input")
			.def_readwrite("stages", &UserInput::Input::stages)
			.def_readwrite("totalEndTimeLowerBound", &UserInput::Input::totalEndTimeLowerBound)
			.def_readwrite("totalEndTimeUpperBound", &UserInput::Input::totalEndTimeUpperBound)
			.def_readwrite("runningMode", &UserInput::Input::runningMode)
			.def_readwrite("integratorInput", &UserInput::Input::integratorInput)
			.def_readwrite("optimizerInput", &UserInput::Input::optimizerInput)
			.def_readwrite("finalIntegration", &UserInput::Input::finalIntegration)
			;


	
		/**
		* @brief expose enum RunningMode
		*/
		boost::python::enum_<UserInput::Input::RunningMode>("runningMode_type")
			.value("SIMULATION", UserInput::Input::SIMULATION)
			.value("SINGLE_SHOOTING", UserInput::Input::SINGLE_SHOOTING)
			.value("MULTIPLE_SHOOTING", UserInput::Input::MULTIPLE_SHOOTING)
			.value("SENSITIVITY_INTEGRATION", UserInput::Input::SENSITIVITY_INTEGRATION);


	}

	{
		/**
		* @brief expose Input Classes EsoInput
		*/
		boost::python::scope in_EsoInput = boost::python::class_<UserInput::EsoInput>("EsoInput")
			.def_readwrite("model", &UserInput::EsoInput::model)
			.def_readwrite("initialModel", &UserInput::EsoInput::initialModel)
			.def_readwrite("type", &UserInput::EsoInput::type)
#ifdef BUILD_WITH_FMU
			.def_readwrite("relativeFmuTolerance", &UserInput::EsoInput::relativeFmuTolerance)
#endif
			;

		/**
		* @brief expose enum EsoType
		*/
                boost::python::enum_<EsoType>("type_type")
                        .value("JADE", EsoType::JADE)
#ifdef BUILD_WITH_FMU
                        .value("FMI", EsoType::FMI)
#endif
#ifdef BUILD_WITH_MADO
					.value("MADO", EsoType::MADO)
#endif
			;

	}

	{
		/**
		* @brief expose Input Classes WaveletAdaptationInput
		*/
		boost::python::class_<UserInput::WaveletAdaptationInput>("WaveletAdaptationInput")
			.def_readwrite("maxRefinementLevel", &UserInput::WaveletAdaptationInput::maxRefinementLevel)
			.def_readwrite("minRefinementLevel", &UserInput::WaveletAdaptationInput::minRefinementLevel)
			.def_readwrite("horRefinementDepth", &UserInput::WaveletAdaptationInput::horRefinementDepth)
			.def_readwrite("verRefinementDepth", &UserInput::WaveletAdaptationInput::verRefinementDepth)
			.def_readwrite("etres", &UserInput::WaveletAdaptationInput::etres)
			.def_readwrite("epsilon", &UserInput::WaveletAdaptationInput::epsilon);
	}


	{
		/**
		* @brief expose Input Classes SWAdaptationInput
		*/
		boost::python::class_<UserInput::SWAdaptationInput>("SWAdaptationInput")
			.def_readwrite("maxRefinementLevel", &UserInput::SWAdaptationInput::maxRefinementLevel)
			.def_readwrite("includeTol", &UserInput::SWAdaptationInput::includeTol);

	}



	{

		/**
		* @brief expose Input Classes AdaptationInput
		*/
		boost::python::scope in_AdaptationInput = boost::python::class_<UserInput::AdaptationInput>("AdaptationInput")
			.def_readwrite("adaptType", &UserInput::AdaptationInput::adaptType)
			.def_readwrite("adaptWave", &UserInput::AdaptationInput::adaptWave)
			.def_readwrite("swAdapt", &UserInput::AdaptationInput::swAdapt)
			.def_readwrite("maxAdaptSteps", &UserInput::AdaptationInput::maxAdaptSteps);


		/**
		* @brief expose enum AdaptationType
		*/
		boost::python::enum_<UserInput::AdaptationInput::AdaptationType>("adaptType_type")
			.value("WAVELET", UserInput::AdaptationInput::WAVELET)
			.value("SWITCHING_FUNCTION", UserInput::AdaptationInput::SWITCHING_FUNCTION);

	}


	{


		/**
		* @brief expose Input Classes StructureDetectionInput
		*/
		boost::python::class_<UserInput::StructureDetectionInput>("StructureDetectionInput")
			.def_readwrite("maxStructureSteps", &UserInput::StructureDetectionInput::maxStructureSteps)
			.def_readwrite("createContinuousGrids", &UserInput::StructureDetectionInput::createContinuousGrids);


	}



	{


		/**
		* @brief expose Input Classes ParameterGridInput
		*/
		boost::python::scope in_ParameterGridInput =  boost::python::class_<UserInput::ParameterGridInput>("ParameterGridInput")
			.def_readwrite("numIntervals", &UserInput::ParameterGridInput::numIntervals)
			.def_readwrite("pcresolution", &UserInput::ParameterGridInput::pcresolution)
			.def_readwrite("timePoints", &UserInput::ParameterGridInput::timePoints)
			.def_readwrite("values", &UserInput::ParameterGridInput::values)
			.def_readwrite("duration", &UserInput::ParameterGridInput::duration)
			.def_readwrite("hasFreeDuration", &UserInput::ParameterGridInput::hasFreeDuration)
			.def_readwrite("type", &UserInput::ParameterGridInput::type)
			.def_readwrite("adapt", &UserInput::ParameterGridInput::adapt);



		




		/**
		* @brief expose enum ApproximationType
		*/
		boost::python::enum_<UserInput::ParameterGridInput::ApproximationType>("type_type")
			.value("PIECEWISE_CONSTANT", UserInput::ParameterGridInput::PIECEWISE_CONSTANT)
			.value("PIECEWISE_LINEAR", UserInput::ParameterGridInput::PIECEWISE_LINEAR);



	}



	{

	

		/**
		* @brief expose Input Classes ParameterInput
		*/
		boost::python::scope in_ParameterInput = boost::python::class_<UserInput::ParameterInput>("ParameterInput")
			.def_readwrite("name", &UserInput::ParameterInput::name)
			.def_readwrite("lowerBound", &UserInput::ParameterInput::lowerBound)
			.def_readwrite("upperBound", &UserInput::ParameterInput::upperBound)
			.def_readwrite("value", &UserInput::ParameterInput::value)
			.def_readwrite("grids", &UserInput::ParameterInput::grids)
			.def_readwrite("paramType", &UserInput::ParameterInput::paramType)
			.def_readwrite("sensType", &UserInput::ParameterInput::sensType);



		/**
		* @brief expose enum ParameterType
		*/
		boost::python::enum_<UserInput::ParameterInput::ParameterType>("paramType_type")
			.value("INITIAL", UserInput::ParameterInput::INITIAL)
			.value("TIME_INVARIANT", UserInput::ParameterInput::TIME_INVARIANT)
			.value("PROFILE", UserInput::ParameterInput::PROFILE)
			.value("DURATION", UserInput::ParameterInput::DURATION);


		/**
		* @brief expose enum ParameterSensitivityType
		*/
		boost::python::enum_<UserInput::ParameterInput::ParameterSensitivityType>("sensType_type")
			.value("FULL", UserInput::ParameterInput::FULL)
			.value("FRACTIONAL", UserInput::ParameterInput::FRACTIONAL)
			.value("NO", UserInput::ParameterInput::NO);


	}


	{

		/**
		* @brief expose Input Classes IntegratorStageInput
		*/
		boost::python::class_<UserInput::IntegratorStageInput>("IntegratorStageInput")
			.def_readwrite("duration", &UserInput::IntegratorStageInput::duration)
			.def_readwrite("parameters", &UserInput::IntegratorStageInput::parameters)
			.def_readwrite("plotGridResolution", &UserInput::IntegratorStageInput::plotGridResolution)
			.def_readwrite("explicitPlotGrid", & UserInput::IntegratorStageInput::explicitPlotGrid);
			;

	}


	{


		/**
		* @brief expose Input Classes ConstraintInput
		*/
		boost::python::scope in_ConstraintInput = boost::python::class_<UserInput::ConstraintInput>("ConstraintInput")
			.def_readwrite("name", &UserInput::ConstraintInput::name)
			.def_readwrite("lowerBound", &UserInput::ConstraintInput::lowerBound)
			.def_readwrite("upperBound", &UserInput::ConstraintInput::upperBound)
			.def_readwrite("timePoint", &UserInput::ConstraintInput::timePoint)
			.def_readwrite("lagrangeMultiplier", &UserInput::ConstraintInput::lagrangeMultiplier)
			.def_readwrite("lagrangeMultiplierVector", &UserInput::ConstraintInput::lagrangeMultiplierVector)
			.def_readwrite("type", &UserInput::ConstraintInput::type)
			.def_readwrite("excludeZero", &UserInput::ConstraintInput::excludeZero);


		/**
		* @brief expose enum ConstraintType
		*/
		boost::python::enum_<UserInput::ConstraintInput::ConstraintType>("type_type")
			.value("PATH", UserInput::ConstraintInput::PATH)
			.value("POINT", UserInput::ConstraintInput::POINT)
			.value("ENDPOINT", UserInput::ConstraintInput::ENDPOINT)
			.value("MULTIPLE_SHOOTING", UserInput::ConstraintInput::MULTIPLE_SHOOTING);

	}

	{

		/**
		* @brief expose Input Classes OptimizerStageInput
		*/
		boost::python::class_<UserInput::OptimizerStageInput>("OptimizerStageInput")
			.def_readwrite("objective", &UserInput::OptimizerStageInput::objective)
			.def_readwrite("constraints", &UserInput::OptimizerStageInput::constraints)
			.def_readwrite("structureDetection", &UserInput::OptimizerStageInput::structureDetection);


	}



	{


		/**
		* @brief expose Input Classes StageMappiny
		*/
		boost::python::class_<UserInput::StageMapping>("StageMapping")
			.def_readwrite("fullStateMapping", &UserInput::StageMapping::fullStateMapping)
			.def_readwrite("stateNameMapping", &UserInput::StageMapping::stateNameMapping)
			;

	}


	{


		/**
		* @brief expose Input Classes StageMappiny
		*/
		boost::python::class_<UserInput::MappingInput>("MappingInput")
			.def_readwrite("fullStateMapping", &UserInput::MappingInput::fullStateMapping)
			.def_readwrite("stateNameMapping", &UserInput::MappingInput::stateNameMapping)
		;

	}


	{

		/**
		* @brief expose Input Classes StageInput
		*/
		boost::python::class_<UserInput::StageInput>("StageInput")
			.def_readwrite("treatObjective", &UserInput::StageInput::treatObjective)
			.def_readwrite("mapping", &UserInput::StageInput::mapping)
			.def_readwrite("eso", &UserInput::StageInput::eso)
			.def_readwrite("integrator", &UserInput::StageInput::integrator)
			.def_readwrite("optimizer", &UserInput::StageInput::optimizer);



	}



	{

		/**
		* @brief expose Input Classes LinearSolverInput
		*/
		boost::python::scope in_LinearSolverInput = boost::python::class_<UserInput::LinearSolverInput>("LinearSolverInput")
			.def_readwrite("type", &UserInput::LinearSolverInput::type);



		/**
		* @brief expose enum LinearSolverType
		*/
		boost::python::enum_<UserInput::LinearSolverInput::SolverType>("type")
			.value("MA28", UserInput::LinearSolverInput::MA28)
			.value("KLU", UserInput::LinearSolverInput::KLU);



	}



	{

		/**
		* @brief expose Input Classes NonLinearSolverInput
		*/
		boost::python::scope in_NonLinearSolverInput = boost::python::class_<UserInput::NonLinearSolverInput>("NonLinearSolverInput")
			.def_readwrite("type", &UserInput::NonLinearSolverInput::type)
			.def_readwrite("tolerance", &UserInput::NonLinearSolverInput::tolerance);



		/**
		* @brief expose enum NonlinearSolverType
		*/
		boost::python::enum_<UserInput::NonLinearSolverInput::SolverType>("type_type")
			.value("NLEQ1S", UserInput::NonLinearSolverInput::NLEQ1S)
			.value("CMINPACK", UserInput::NonLinearSolverInput::CMINPACK);



	}



	{


		/**
		* @brief expose Input Classes DaeInitializationInput
		*/
		boost::python::scope in_DaeInitializationInput = boost::python::class_<UserInput::DaeInitializationInput>("DaeInitializationInput")
			.def_readwrite("type", &UserInput::DaeInitializationInput::type)
			.def_readwrite("linSolver", &UserInput::DaeInitializationInput::linSolver)
			.def_readwrite("nonLinSolver", &UserInput::DaeInitializationInput::nonLinSolver)
			.def_readwrite("maximumErrorTolerance", &UserInput::DaeInitializationInput::maximumErrorTolerance);


		/**
		* @brief expose enum DaeInitializationType
		*/
		boost::python::enum_<UserInput::DaeInitializationInput::DaeInitializationType>("type_type")
			.value("NO", UserInput::DaeInitializationInput::NO)
			.value("FULL", UserInput::DaeInitializationInput::FULL)
			.value("BLOCK", UserInput::DaeInitializationInput::BLOCK)
#ifdef BUILD_WITH_FMU
			.value("FMI", UserInput::DaeInitializationInput::FMI)
#endif
			;


	}


	{

		/**
		* @brief expose Input Classes IntegratorInput
		*/
		boost::python::scope in_IntegratorInputPy = boost::python::class_<UserInput::IntegratorInput>("IntegratorInput")
			.def_readwrite("integratorOptions", &UserInput::IntegratorInput::integratorOptions)
			.def_readwrite("type", &UserInput::IntegratorInput::type)
			.def_readwrite("order", &UserInput::IntegratorInput::order)
			.def_readwrite("daeInit", &UserInput::IntegratorInput::daeInit);

		
		/**
		* @brief expose enum IntegratorType
		*/
		boost::python::enum_<UserInput::IntegratorInput::IntegratorType>("type_type")
			.value("NIXE", UserInput::IntegratorInput::NIXE)
			.value("LIMEX", UserInput::IntegratorInput::LIMEX)
			.value("IDAS", UserInput::IntegratorInput::IDAS);


		/**
		* @brief expose enum IntegrationOrder
		*/
		boost::python::enum_<UserInput::IntegratorInput::IntegrationOrder>("order_type")
			.value("ZEROTH", UserInput::IntegratorInput::ZEROTH)
			.value("FIRST_FORWARD", UserInput::IntegratorInput::FIRST_FORWARD)
			.value("FIRST_REVERSE", UserInput::IntegratorInput::FIRST_REVERSE)
			.value("SECOND_REVERSE", UserInput::IntegratorInput::SECOND_REVERSE);


	}


	{

		/**
		* @brief expose Input Classes AdaptationOptions
		*/
		boost::python::scope in_AdaptationOptions = boost::python::class_<UserInput::AdaptationOptions>("AdaptationOptions")
			.def_readwrite("adaptationThreshold", &UserInput::AdaptationOptions::adaptationThreshold)
			.def_readwrite("intermConstraintViolationTolerance", &UserInput::AdaptationOptions::intermConstraintViolationTolerance)
			.def_readwrite("numOfIntermPoints", &UserInput::AdaptationOptions::numOfIntermPoints)
			.def_readwrite("adaptStrategy", &UserInput::AdaptationOptions::adaptStrategy);

		/**
		* @brief expose enum AdaptiveStrategy
		*/
		boost::python::enum_<UserInput::AdaptationOptions::AdaptiveStrategy>("adaptStrategy_type")
			.value("NOADAPTATION", UserInput::AdaptationOptions::NOADAPTATION)
			.value("ADAPTATION", UserInput::AdaptationOptions::ADAPTATION)
			.value("STRUCTURE_DETECTION", UserInput::AdaptationOptions::STRUCTURE_DETECTION)
			.value("ADAPT_STRUCTURE", UserInput::AdaptationOptions::ADAPT_STRUCTURE);

	}



	{


		/**
		* @brief expose Input Classes OptimizerInput
		*/
		boost::python::scope in_OptimizerInput = boost::python::class_<UserInput::OptimizerInput>("OptimizerInput")
			.def_readwrite("optimizerOptions", &UserInput::OptimizerInput::optimizerOptions)
			.def_readwrite("type", &UserInput::OptimizerInput::type)
			.def_readwrite("globalConstraints", &UserInput::OptimizerInput::globalConstraints)
			.def_readwrite("adaptationOptions", &UserInput::OptimizerInput::adaptationOptions)
			.def_readwrite("optimizationMode", &UserInput::OptimizerInput::optimizationMode);


		/**
		* @brief expose enum OptimizerType
		*/
		boost::python::enum_<UserInput::OptimizerInput::OptimizerType>("type_type")
			.value("SNOPT", UserInput::OptimizerInput::SNOPT)
			.value("IPOPT", UserInput::OptimizerInput::IPOPT)
			.value("NPSOL", UserInput::OptimizerInput::NPSOL)
			.value("FILTER_SQP", UserInput::OptimizerInput::FILTER_SQP)
			.value("SENSITIVITY_INTEGRATION", UserInput::OptimizerInput::SENSITIVITY_INTEGRATION);


		/**
		* @brief expose enum OptimizationMode
		*/
		boost::python::enum_<UserInput::OptimizerInput::OptimizationMode>("optimizationMode_type")
			.value("MINIMIZE", UserInput::OptimizerInput::MINIMIZE)
			.value("MAXIMIZE", UserInput::OptimizerInput::MAXIMIZE);


	}






	{
		/**
		* @brief expose Output
		*/
		boost::python::scope in_Output = boost::python::class_<UserOutput::Output>("Output")
			.def_readwrite("solutionHistory", &UserOutput::Output::solutionHistory)
			.def_readwrite("stages", &UserOutput::Output::stages)
			.def_readwrite("totalEndTimeLowerBound", &UserOutput::Output::totalEndTimeLowerBound)
			.def_readwrite("totalEndTimeUpperBound", &UserOutput::Output::totalEndTimeUpperBound)
			.def_readwrite("solutionTime", &UserOutput::Output::solutionTime)
			.def_readwrite("runningMode", &UserOutput::Output::runningMode)
			.def_readwrite("integratorOutput", &UserOutput::Output::integratorOutput)
			.def_readwrite("optimizerOutput", &UserOutput::Output::optimizerOutput)
			;



		/**
		* @brief expose enum RunningMode
		*/
		boost::python::enum_<UserOutput::Output::RunningMode>("runningMode_type")
			.value("SIMULATION", UserOutput::Output::SIMULATION)
			.value("SINGLE_SHOOTING", UserOutput::Output::SINGLE_SHOOTING)
			.value("MULTIPLE_SHOOTING", UserOutput::Output::MULTIPLE_SHOOTING)
			.value("SENSITIVITY_INTEGRATION", UserOutput::Output::SENSITIVITY_INTEGRATION);


	}


	{
		/**
		* @brief expose Input Classes EsoOutput
		*/
		boost::python::scope in_EsoInput = boost::python::class_<UserOutput::EsoOutput>("EsoOutput")
			.def_readwrite("model", &UserOutput::EsoOutput::model)
			.def_readwrite("type", &UserOutput::EsoOutput::type)
			#ifdef BUILD_WITH_FMU
			.def_readwrite("relativeFmuTolerance", &UserOutput::EsoOutput::relativeFmuTolerance)
			#endif
			;



	}


	{


		/**
		* @brief expose Ouput Classes MappingOutput
		*/
		boost::python::class_<UserOutput::MappingOutput>("MappingOutput")
			.def_readwrite("fullStateMapping", &UserOutput::MappingOutput::fullStateMapping)
			.def_readwrite("stateNameMapping", &UserOutput::MappingOutput::stateNameMapping)
			;

	}

	{
		/**
		* @brief expose Ouput Classes StateGridOutput
		*/
		boost::python::class_<UserOutput::StateGridOutput>("StateGridOutput")
			.def_readwrite("timePoints", &UserOutput::StateGridOutput::timePoints)
			.def_readwrite("values", &UserOutput::StateGridOutput::values)
			;

	}


	{
		/**
		* @brief expose Ouput Classes StateOutput
		*/
		boost::python::class_<UserOutput::StateOutput>("StateOutput")
			.def_readwrite("name", &UserOutput::StateOutput::name)
			.def_readwrite("esoIndex", &UserOutput::StateOutput::esoIndex)
			.def_readwrite("grid", &UserOutput::StateOutput::grid)

			;

	}


	{
		/**
		* @brief expose Ouput Classes StateOutput
		*/
		boost::python::class_<UserOutput::FirstSensitivityOutput>("FirstSensitivityOutput")
			.def_readwrite("mapParamsCols", &UserOutput::FirstSensitivityOutput::mapParamsCols)
			.def_readwrite("mapConstrRows", &UserOutput::FirstSensitivityOutput::mapConstrRows)
			.def_readwrite("values", &UserOutput::FirstSensitivityOutput::values)

			;

	}


	{


		/**
		* @brief expose Output Classes ParameterGridOutput
		*/
		boost::python::scope in_ParameterGridOutput = boost::python::class_<UserOutput::ParameterGridOutput>("ParameterGridOutput")
			.def_readwrite("numIntervals", &UserOutput::ParameterGridOutput::numIntervals)
			.def_readwrite("timePoints", &UserOutput::ParameterGridOutput::timePoints)
			.def_readwrite("values", &UserOutput::ParameterGridOutput::values)
			.def_readwrite("lagrangeMultipliers", &UserOutput::ParameterGridOutput::lagrangeMultipliers)
			.def_readwrite("isFixed", &UserOutput::ParameterGridOutput::isFixed)
			.def_readwrite("duration", &UserOutput::ParameterGridOutput::duration)
			.def_readwrite("hasFreeDuration", &UserOutput::ParameterGridOutput::hasFreeDuration)
			.def_readwrite("type", &UserOutput::ParameterGridOutput::type)
			.def_readwrite("firstSensitivities", &UserOutput::ParameterGridOutput::firstSensitivities);

		/**
		* @brief expose enum ApproximationType
		*/
		boost::python::enum_<UserOutput::ParameterGridOutput::ApproximationType>("type_type")
			.value("PIECEWISE_CONSTANT", UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT)
			.value("PIECEWISE_LINEAR", UserOutput::ParameterGridOutput::PIECEWISE_LINEAR);



	}



	{


		/**
		* @brief expose Input Classes ParameterOutput
		*/
		boost::python::scope in_ParameterOutput = boost::python::class_<UserOutput::ParameterOutput>("ParameterOutput")
			.def_readwrite("name", &UserOutput::ParameterOutput::name)
			.def_readwrite("isOptimizationParameter", &UserOutput::ParameterOutput::isOptimizationParameter)
			.def_readwrite("lowerBound", &UserOutput::ParameterOutput::lowerBound)
			.def_readwrite("upperBound", &UserOutput::ParameterOutput::upperBound)
			.def_readwrite("value", &UserOutput::ParameterOutput::value)
			.def_readwrite("grids", &UserOutput::ParameterOutput::grids)
			.def_readwrite("paramType", &UserOutput::ParameterOutput::paramType)
			.def_readwrite("sensType", &UserOutput::ParameterOutput::sensType)
			.def_readwrite("lagrangeMultiplier", &UserOutput::ParameterOutput::lagrangeMultiplier)
			;



		/**
		* @brief expose enum ParameterType
		*/
		boost::python::enum_<UserOutput::ParameterOutput::ParameterType>("paramType_type")
			.value("INITIAL", UserOutput::ParameterOutput::INITIAL)
			.value("TIME_INVARIANT", UserOutput::ParameterOutput::TIME_INVARIANT)
			.value("PROFILE", UserOutput::ParameterOutput::PROFILE)
			.value("DURATION", UserOutput::ParameterOutput::DURATION);


		/**
		* @brief expose enum ParameterSensitivityType
		*/
		boost::python::enum_<UserOutput::ParameterOutput::ParameterSensitivityType>("sensType_type")
			.value("FULL", UserOutput::ParameterOutput::FULL)
			.value("FRACTIONAL", UserOutput::ParameterOutput::FRACTIONAL)
			.value("NO", UserOutput::ParameterOutput::NO);


	}



	{

		/**
		* @brief expose Output Classes IntegratorStageOutput
		*/
		boost::python::class_<UserOutput::IntegratorStageOutput>("IntegratorStageOutput")
			.def_readwrite("durations", &UserOutput::IntegratorStageOutput::durations)
			.def_readwrite("parameters", &UserOutput::IntegratorStageOutput::parameters)
			.def_readwrite("adjoints", &UserOutput::IntegratorStageOutput::adjoints)
			.def_readwrite("adjoints2ndOrder", &UserOutput::IntegratorStageOutput::adjoints2ndOrder)
			.def_readwrite("states", &UserOutput::IntegratorStageOutput::states)

		;

	}


	{

		/**
		* @brief expose Output Classes ConstraintGridOutput
		*/
		boost::python::scope in_ConstraintGridOutput = boost::python::class_<UserOutput::ConstraintGridOutput>("ConstraintGridOutput")
			.def_readwrite("timePoints", &UserOutput::ConstraintGridOutput::timePoints)
			.def_readwrite("values", &UserOutput::ConstraintGridOutput::values)
			.def_readwrite("lagrangeMultiplier", &UserOutput::ConstraintGridOutput::lagrangeMultiplier);



		/**
		* @brief expose enum ConstraintOutputType
		*/
		boost::python::enum_<UserOutput::ConstraintGridOutput::ConstraintOutputType>("ConstraintOutputType_type")
			.value("UPPER_BOUND", UserOutput::ConstraintGridOutput::UPPER_BOUND)
			.value("LOWER_BOUND", UserOutput::ConstraintGridOutput::LOWER_BOUND)
			.value("INFEASIBLE", UserOutput::ConstraintGridOutput::INFEASIBLE)
			.value("ACTIVE", UserOutput::ConstraintGridOutput::ACTIVE);


	}


	{

		/**
		* @brief expose Output Classes ConstraintOutput
		*/
		boost::python::scope in_ConstraintOutput = boost::python::class_<UserOutput::ConstraintOutput>("ConstraintOutput")
			.def_readwrite("name", &UserOutput::ConstraintOutput::name)
			.def_readwrite("lowerBound", &UserOutput::ConstraintOutput::lowerBound)
			.def_readwrite("upperBound", &UserOutput::ConstraintOutput::upperBound)
			.def_readwrite("grids", &UserOutput::ConstraintOutput::grids)
			.def_readwrite("type", &UserOutput::ConstraintOutput::type);


		/**
		* @brief expose enum OutputConstraintType
		*/
		boost::python::enum_<UserOutput::ConstraintOutput::ConstraintType>("type_type")
			.value("PATH", UserOutput::ConstraintOutput::PATH)
			.value("POINT", UserOutput::ConstraintOutput::POINT)
			.value("ENDPOINT", UserOutput::ConstraintOutput::ENDPOINT);


	}


	{

		/**
		* @brief expose Output Classes OptimizerStageOutput
		*/
		boost::python::class_<UserOutput::OptimizerStageOutput>("OptimizerStageOutput")
			.def_readwrite("objective", &UserOutput::OptimizerStageOutput::objective)
			.def_readwrite("nonlinearConstraints", &UserOutput::OptimizerStageOutput::nonlinearConstraints)
			.def_readwrite("linearConstraints", &UserOutput::OptimizerStageOutput::linearConstraints);


	}



	{

		/**
		* @brief expose Output Classes StageOutput
		*/
		boost::python::class_<UserOutput::StageOutput>("StageOutput")
			.def_readwrite("treatObjective", &UserOutput::StageOutput::treatObjective)
			.def_readwrite("mapping", &UserOutput::StageOutput::mapping)
			.def_readwrite("eso", &UserOutput::StageOutput::eso)
			.def_readwrite("integrator", &UserOutput::StageOutput::integrator)
			.def_readwrite("optimizer", &UserOutput::StageOutput::optimizer)
			.def_readwrite("stageGrid", &UserOutput::StageOutput::stageGrid)
			.add_property("genEso", &pyDyosFunctions::get_StageOutput_GenericEso);


	}


	{


		/**
		* @brief expose Output Classes IntegratorOutput
		*/
		boost::python::scope in_IntegratorOutput = boost::python::class_<UserOutput::IntegratorOutput>("IntegratorOutput")
			.def_readwrite("type", &UserOutput::IntegratorOutput::type)
			.def_readwrite("order", &UserOutput::IntegratorOutput::order);



		/**
		* @brief expose enum OutputIntegratorType
		*/
		boost::python::enum_<UserOutput::IntegratorOutput::IntegratorType>("type_type")
			.value("NIXE", UserOutput::IntegratorOutput::NIXE)
			.value("LIMEX", UserOutput::IntegratorOutput::LIMEX)
			.value("IDAS", UserOutput::IntegratorOutput::IDAS);


		/**
		* @brief expose enum OutputIntegrationOrder
		*/
		boost::python::enum_<UserOutput::IntegratorOutput::IntegrationOrder>("order_type")
			.value("ZEROTH", UserOutput::IntegratorOutput::ZEROTH)
			.value("FIRST_FORWARD", UserOutput::IntegratorOutput::FIRST_FORWARD)
			.value("FIRST_REVERSE", UserOutput::IntegratorOutput::FIRST_REVERSE)
			.value("SECOND_REVERSE", UserOutput::IntegratorOutput::SECOND_REVERSE);


	}



	{


		/**
		* @brief expose Output Classes SecondOrderOutput
		*/
		boost::python::class_<UserOutput::SecondOrderOutput>("SecondOrderOutput")
			.def_readwrite("indicesHessian", &UserOutput::SecondOrderOutput::indicesHessian)
			.def_readwrite("values", &UserOutput::SecondOrderOutput::values)
			.def_readwrite("constParamHessian", &UserOutput::SecondOrderOutput::constParamHessian)
			.def_readwrite("compositeAdjoints", &UserOutput::SecondOrderOutput::compositeAdjoints);



	}


	{

		/**
		* @brief expose Output Classes OptimizerOutput
		*/
		boost::python::scope in_OptimizerOutput = boost::python::class_<UserOutput::OptimizerOutput>("OptimizerOutput")
			.def_readwrite("type", &UserOutput::OptimizerOutput::type)
			.def_readwrite("resultFlag", &UserOutput::OptimizerOutput::resultFlag)
			.def_readwrite("globalConstraints", &UserOutput::OptimizerOutput::globalConstraints)
			.def_readwrite("linearConstraints", &UserOutput::OptimizerOutput::linearConstraints)
			.def_readwrite("objVal", &UserOutput::OptimizerOutput::objVal)
			.def_readwrite("intermConstrVio", &UserOutput::OptimizerOutput::intermConstrVio)
			.def_readwrite("secondOrder", &UserOutput::OptimizerOutput::secondOrder);


		/**
		* @brief expose enum OutputOptimizerType
		*/
		boost::python::enum_<UserOutput::OptimizerOutput::OptimizerType>("type_type")
			.value("SNOPT", UserOutput::OptimizerOutput::SNOPT)
			.value("IPOPT", UserOutput::OptimizerOutput::IPOPT)
			.value("NPSOL", UserOutput::OptimizerOutput::NPSOL)
			.value("FILTER_SQP", UserOutput::OptimizerOutput::FILTER_SQP)
			.value("SENSITIVITY_INTEGRATION", UserOutput::OptimizerOutput::SENSITIVITY_INTEGRATION);


		/**
		* @brief expose enum ResultFlag
		*/
		boost::python::enum_<UserOutput::OptimizerOutput::ResultFlag>("resultFlag_type")
			.value("OK", UserOutput::OptimizerOutput::OK)
			.value("TOO_MANY_ITERATIONS", UserOutput::OptimizerOutput::TOO_MANY_ITERATIONS)
			.value("INFEASIBLE", UserOutput::OptimizerOutput::INFEASIBLE)
			.value("WRONG_GRADIENTS", UserOutput::OptimizerOutput::WRONG_GRADIENTS)
			.value("NOT_OPTIMAL", UserOutput::OptimizerOutput::NOT_OPTIMAL)
			.value("FAILED", UserOutput::OptimizerOutput::FAILED);


	}

	{	

		/**
		* @brief expose class GenericEso
		*/
		boost::python::class_<GenericEso, GenericEso::Ptr, boost::noncopyable>("GenericEso", boost::python::no_init)
			.add_property("variableNames", &pyDyosFunctions::GenericEso_getVariableNames)
			.add_property("jacobian", &pyDyosFunctions::GenericEso_getJacobian)
			.add_property("diffJacobian", &pyDyosFunctions::GenericEso_getDiffJacobian)
			.add_property("residuals", &pyDyosFunctions::GenericEso_getResiduals)
			.add_property("stateValues", &pyDyosFunctions::GenericEso_getStateValues, &pyDyosFunctions::GenericEso_setStateValues)
			.add_property("parameterValues", &pyDyosFunctions::GenericEso_getParameterValues, &pyDyosFunctions::GenericEso_setParameterValues)
			.add_property("numDiffEq", &GenericEso::getNumDiffEquations)
			.add_property("numAlgEq", &GenericEso::getNumAlgEquations)
			.def("setParameter", &pyDyosFunctions::GenericEso_setParameter)
			.def("setState", &pyDyosFunctions::GenericEso_setState)
			.add_property("numStates", &GenericEso::getNumStates)
			.add_property("numParams", &GenericEso::getNumParameters);


		/**
		* @brief expose class GenericEso2ndOrder
		*/
		boost::python::class_<GenericEso2ndOrder, boost::python::bases<GenericEso>, boost::noncopyable>("GenericEso2ndOrder", boost::python::no_init);

		/**
		* @brief expose class FMIGenericEso
		*/
		#ifdef BUILD_WITH_FMU
		boost::python::class_<FMIGenericEso, boost::python::bases<GenericEso2ndOrder>>("FMIGenericEso", boost::python::init<string, double>());
        #endif 

		#ifdef BUILD_WITH_MADO
		boost::python::class_<MADOGenericEso, boost::python::bases<GenericEso2ndOrder>>("MADOGenericEso", boost::python::init<string>());
		#endif 
		/**
		* @brief expose class JadeGenericEso
		*/
		boost::python::class_<JadeGenericEso, boost::python::bases<GenericEso2ndOrder>>("JadeGenericEso", boost::python::init<string>());
	}

}



