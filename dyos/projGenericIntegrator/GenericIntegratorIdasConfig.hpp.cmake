/**
* @file IdasConfig.hpp.cmake
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Configuration Header for Dyos                                          \n
* =====================================================================\n
* configure header         \n
* =====================================================================\n
* @author Adrian Caspari  
* @date 1.08.2018
*/

#pragma once


#cmakedefine KLU_ENABLE "@KLU_ENABLE@"
#cmakedefine OPENMP_ENABLE "@OPENMP_ENABLE@"


