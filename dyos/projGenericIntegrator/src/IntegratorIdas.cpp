/**
* @file IntegratorIdas.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Generic Integrator                                                   \n
* =====================================================================\n
* This file contains dyos integrator idas wrapper                      \n
* =====================================================================\n
* @author Adrian Caspari
* @date 19.05.2018
*/


#include "IntegratorIdas.hpp"
#include "IdasUserFunctions.hpp"
#include "EsoUtilities.hpp"


#include <idas/idas.h>
#include <idas/idas_direct.h>
#include <nvector/nvector_serial.h>
#include "GenericIntegratorIdasConfig.hpp"


#ifdef KLU_ENABLE
#include <sunlinsol/sunlinsol_klu.h> /* access to sparse KLU LinearSolver */
#include <sunmatrix/sunmatrix_sparse.h> /*sunmatrix sparse matrix module*/
#else
#include <sunlinsol/sunlinsol_dense.h> /* access to dense SUNLinearSolver */
#endif


#ifdef OPENMP_ENABLE
#include <nvector/nvector_openmp.h>
#include <omp.h>
#endif

ForwardIntegratorIdas::ForwardIntegratorIdas(const IntegratorMetaData::Ptr &integratorMetaData,
											 const bool calculateSensitivities,
                                             const DaeInitialization::Ptr &daeInit, const IdasOptions idasOptions)
  : m_intmd(integratorMetaData),
    m_daeInit(daeInit),
	m_IntegratorOptions(idasOptions),
	m_calculateSensitivities(calculateSensitivities)
{
}

ForwardIntegratorIdas::ForwardIntegratorIdas(const IntegratorMetaData::Ptr &integratorMetaData,
	const DaeInitialization::Ptr &daeInit)
	: m_intmd(integratorMetaData),
	m_daeInit(daeInit),
	m_IntegratorOptions()
{
}

ForwardIntegratorIdas::~ForwardIntegratorIdas()
{
}

GenericIntegratorForward::ResultFlag ForwardIntegratorIdas::solveInterval()
{
  GenericIntegratorForward::ResultFlag retval = GenericIntegratorForward::solveInterval();
  return retval;
}

int res(double t, N_Vector yy, N_Vector yd, N_Vector resval, void *user_data)
{
	IntegratorMetaData::Ptr intmd = *((IntegratorMetaData::Ptr*)(user_data));
	intmd->setStepCurrentTime(t);
	utils::WrappingArray<double> y(NV_LENGTH_S(yy), NV_DATA_S(yy));
	utils::WrappingArray<double> rhsStates(NV_LENGTH_S(resval), NV_DATA_S(resval));
	intmd->calculateRhsStates(y, rhsStates);
	utils::WrappingArray<double> ydwrap(NV_LENGTH_S(yd), NV_DATA_S(yd));

	CsCscMatrix::Ptr mass_mat = intmd->getMMatrix();
	int nnz = mass_mat->get_matrix_ptr()->nzmax;
	int n = mass_mat->get_matrix_ptr()->n;
	double* Mx = new double[nnz];
	int* Mp = new int[n+1];
	int* Mi = new int[nnz];
	for (int j = 0; j<nnz; j++) {
		Mx[j] = mass_mat->get_matrix_ptr()->x[j];
		Mi[j] = mass_mat->get_matrix_ptr()->i[j];
	}

	for (int j = 0; j < n + 1; j++) 
		Mp[j] = mass_mat->get_matrix_ptr()->p[j];	

	const double* _x = ydwrap.getData();
	double* _y = rhsStates.getData();
	for (int j = 0; j < n; j++)
	{
		for (int p = Mp[j]; p < Mp[j + 1]; p++)
		{
			_y[Mi[p]] += Mx[p] * _x[j];
		}
	}

	delete[] Mx;
	delete[] Mp;
	delete[] Mi;


	return 0;
}



GenericIntegratorForward::SolveStepResult ForwardIntegratorIdas::solveStep(double& starttime,
                                                                           double endtime,
                                                                           utils::Array<double> &finalStates,
                                                                           utils::Array<double> &finalSensitivities)
{
  m_ida_mem = IDACreate();
  SUNLinearSolver LS;
  LS = NULL;


  GenericEso::Ptr eso = m_intmd->getGenericEso();
  int n_states = eso->getNumStates();
  int n_nonzeros = eso->getNumNonZeroes();
  int n_p = m_intmd->getNumCurrentSensitivityParameters();
  bool with_sens = (n_p > 0 && m_calculateSensitivities);
  N_Vector yvec;
  N_Vector ydvec;
  N_Vector* ysensvec;
  N_Vector* ydsensvec;

  initialize(yvec, ydvec, ysensvec, ydsensvec);

  // set differential ID
  N_Vector differential_id = N_VNew_Serial(n_states);
  //setDifferentialID(differential_id);
  double* diff_id_data = NV_DATA_S(differential_id);
  N_VConst(0.0, differential_id);
  int n_diff_states = eso->getNumDifferentialVariables();
  utils::Array<EsoIndex> diff_idx(n_diff_states);
  eso->getDifferentialIndex(n_diff_states, diff_idx.getData());
  for (int k=0; k<n_diff_states; ++k)
    diff_id_data[diff_idx[k]] = 1.0;

  
#ifdef OPENMP_ENABLE
  double* ydata = NV_DATA_OMP(yvec);
#else
  double* ydata = NV_DATA_S(yvec);
#endif


    // std::cout << "Initial values:\n";
  // for (int k=0; k<n_states; ++k)
  //   std::cout << ydata[k] << " " << yddata[k] << std::endl;

  int flag = 0;

 
  flag = IDAInit(m_ida_mem, idas_res, starttime, yvec, ydvec);
 
 
 
  flag = IDASStolerances(m_ida_mem, m_IntegratorOptions.m_relativeTolerance, m_IntegratorOptions.m_absoluteTolerance);
  flag = IDASetUserData(m_ida_mem, &m_intmd);
  flag = IDASetMaxNumSteps(m_ida_mem, m_IntegratorOptions.m_maxSteps);
  flag = IDASetId(m_ida_mem, differential_id);
  flag = IDASetMaxNumStepsIC(m_ida_mem, m_IntegratorOptions.m_maxNumH);

  SUNMatrix A;


#ifdef KLU_ENABLE
  A = SUNSparseMatrix(n_states, n_states, n_nonzeros, CSC_MAT);
  LS = SUNKLU(yvec, A);
#else
  A = SUNDenseMatrix(n_states, n_states);
  LS = SUNDenseLinearSolver(yvec, A);
#endif

  flag = IDADlsSetLinearSolver(m_ida_mem, LS, A);
  

#ifdef KLU_ENABLE
  flag = IDADlsSetJacFn(m_ida_mem, ida_sparse_jac);
#else
  flag = IDADlsSetJacFn(m_ida_mem, ida_dense_jac);
#endif

  if (with_sens) {
    flag = IDASensInit(m_ida_mem, n_p, m_IntegratorOptions.m_sensIntMethod, idas_resS, ysensvec, ydsensvec);
    //flag = IDASensEEtolerances(m_ida_mem);
    flag = IDASetSensMaxNonlinIters(m_ida_mem, m_IntegratorOptions.m_maxNumNonLinItSens);
    utils::Array<double> SabsTolVec(n_p, m_IntegratorOptions.m_absoluteTolerance);
    flag = IDASensSStolerances(m_ida_mem, m_IntegratorOptions.m_relativeTolerance, SabsTolVec.getData());
	flag = IDASetSensErrCon(m_ida_mem, m_IntegratorOptions.m_errorControlSen);
  } 

  flag = IDACalcIC(m_ida_mem, IDA_YA_YDP_INIT, endtime);

  if (eso->getNumConditions()>0)
	  flag = IDARootInit(m_ida_mem, eso->getNumConditions(), &ida_root);

  if (flag) return GenericIntegratorForward::SolveStepFailed;
  flag = IDASolve(m_ida_mem, endtime, &starttime, yvec, ydvec, IDA_NORMAL);

  std::copy(ydata, ydata + n_states, finalStates.getData()); // copy state results
  double tret;
  if (with_sens) {
    IDAGetSens(m_ida_mem, &tret, ysensvec);
    int curr_start_idx = 0;
    for(int k=0; k<n_p; ++k) {
      std::copy(NV_DATA_S(ysensvec[k]), NV_DATA_S(ysensvec[k])+n_states, finalSensitivities.getData() +curr_start_idx);
      curr_start_idx += n_states;
    }
  }
  GenericIntegratorForward::SolveStepResult retval;
  if (flag==IDA_SUCCESS)
    retval = GenericIntegratorForward::SolveStepSuccess;
  else if(flag==IDA_ROOT_RETURN)
    retval = GenericIntegratorForward::SolveStepFoundRoot;//IDA ROOT RETURN
  else
    retval = GenericIntegratorForward::SolveStepFailed;
  N_VDestroy(differential_id);
  N_VDestroy(yvec);
  N_VDestroy(ydvec);
  if (with_sens) {
	  N_VDestroyVectorArray_Serial(ysensvec, n_p);
	  N_VDestroyVectorArray_Serial(ydsensvec, n_p);
  }
  IDAFree(&m_ida_mem);
  SUNLinSolFree(LS);
  SUNMatDestroy(A);

  return retval;

}


/** This function generates the initial values as NVectors from the IntegratorMetaData */
void ForwardIntegratorIdas::initialize(N_Vector& yvec, N_Vector& ydvec, N_Vector*& ysensvec, N_Vector*& ydsensvec) {
  GenericEso::Ptr eso = m_intmd->getGenericEso();
  int n_states = eso->getNumStates();
  int n_eq = eso->getNumEquations();
  assert(n_states==n_eq);
  int n_p = m_intmd->getNumCurrentSensitivityParameters();
  int n_sens = n_states*n_p;

#ifdef OPENMP_ENABLE
  int num_threads = omp_get_max_threads();
  yvec = N_VNew_OpenMP(n_states, num_threads);
  ydvec = N_VNew_OpenMP(n_states, num_threads);
#else
  yvec = N_VNew_Serial(n_states);
  ydvec = N_VNew_Serial(n_states);
#endif

  N_VConst(0.0, ydvec);
  ysensvec = N_VCloneVectorArray(n_p, yvec);
  ydsensvec = N_VCloneVectorArray(n_p, yvec);
  utils::Array<double> yd(n_states);
  //getFullDerivativeValues(eso, yd);
  //std::copy(yd.getData(), yd.getData()+yd.getSize(), NV_DATA_S(ydvec));
  utils::WrappingArray<double> y(n_states, NV_DATA_S(yvec));
  utils::Array<double> sens(n_sens);
  m_intmd->evaluateInitialValues(y, sens);
  int startidx = 0;
  int stopidx  = n_states;
  for (int k=0; k<n_p; ++k) {
    std::copy(sens.getData()+startidx, sens.getData()+stopidx, NV_DATA_S(ysensvec[k]));
    N_VConst(0.0, ydsensvec[k]);
    startidx += n_states;
    stopidx  += n_states;
  }
}


