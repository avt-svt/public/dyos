/**
* @file MetaDaeInterface2ndOrder.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic Integrator - Part of DyOS                                    \n
* =====================================================================\n
* Member definitions of class MetaDaeInterface2ndOrder                 \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 15.06.2012
*/

#include "MetaDaeInterface2ndOrder.hpp"

/**
* @brief constructor
*
* @param[in] integratorMetaData object of type IntegratorMetaData accessing all integration
*                                relevant data
*/
MetaDaeInterface2ndOrder::MetaDaeInterface2ndOrder
                                        (const IntegratorMetaData2ndOrder::Ptr &integratorMD2ndOrder):
MetaSSEDaeInterface(integratorMD2ndOrder, secondRevOrder), 
m_integratorMD2ndOrder(integratorMD2ndOrder)
{
}

/** 
* @copydoc MetaSSEDaeInterface::EvalReverse
*/
void MetaDaeInterface2ndOrder::EvalReverse(double time, double *states, double *parameters,
                                      double *sens, double *adjoints, double *rhsAdjoints,
                                      double *rhsDers, const Nixe::ProblemInfo &problemInfo) const
{
  // calculating rhsAdjoints and rhsDers for 1st order
  MetaSSEDaeInterface::EvalReverse(time, states, parameters, sens, adjoints, rhsAdjoints,
                                   rhsDers, problemInfo);
  
  GenericEso2ndOrder::Ptr genericEso2ndOrder = m_integratorMD2ndOrder->getGenericEso2ndOrder();
  const int numEqns = genericEso2ndOrder->getNumEquations();
  const int numStates = problemInfo.NumStates();
  const int numDers = problemInfo.NumDers();
  const int numParams = problemInfo.NumParams(); // numParams equals numDecisionVariables
  const int numAdjointVecs = problemInfo.NumAdjoints();
  const int numSens = problemInfo.NumSaveSens();

  // conversion of double pointers to arrays
  utils::WrappingArray<double> statesArray(numStates, states);
  utils::WrappingArray<double> sensArray(numStates*numSens,sens);
  utils::WrappingArray<double> adjoints1stAnd2ndArray(numEqns*(numParams+1), adjoints);
  
  // we extract the arrays containing only second order information
  utils::WrappingArray<double> rhsAdjoints2ndArray(numEqns*numParams, rhsAdjoints+numEqns);
  utils::WrappingArray<double> rhsDers2ndArray(numDers-numParams, rhsDers+numParams);

  // calculating rhsAdjoints and rhsDers for 2nd order
  if(numAdjointVecs > 1){
    m_integratorMD2ndOrder->calculateAdjointsReverse2ndOrder(statesArray, sensArray, 
                                                             adjoints1stAnd2ndArray,
                                                             rhsAdjoints2ndArray, rhsDers2ndArray);
  }
}
