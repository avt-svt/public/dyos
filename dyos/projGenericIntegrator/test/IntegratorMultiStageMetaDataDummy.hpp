/** 
* @file IntegratorMultiStageMetaDataDummy.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Multi  Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* This file contains the IntegratorMultiStageMetaDataDummy class       \n
* used to implmement some additional functions to                      \n
* IntegratorMultiStageMetaData that are used only for testing purposes \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 03.04.2011
*/
#pragma once

#include "IntegratorMultiStageMetaData.hpp"

/**
* @class IntegratorMultiStageMetaDataDummy
*
* test class used in MultiStageIntegrationNixe.testsuite
* This class implements a method to retrieve sensitivities at duration
*/
class IntegratorMultiStageMetaDataDummy : public IntegratorMultiStageMetaData
{
public:
  typedef std::shared_ptr<IntegratorMultiStageMetaDataDummy> Ptr;
  /**
  * @brief constructor
  * @param mappings mapping vector used in class
  * @param stages IntegratorMetaData vector used in class
  */ 
  IntegratorMultiStageMetaDataDummy(const std::vector<Mapping::Ptr> mappings,
                                    const std::vector<IntegratorSingleStageMetaData::Ptr> stages)
  {
    m_mapping = mappings;
    m_stages = stages;
    m_currentStageIndex = 0;
    m_isInitialized = false;
    m_intOptData.resize(stages.size());
  }
  
  /**
  * @brief collect sensitivities of all stages 
  * @param sensitivities vector receiving all sensitivities
  */
  void getSensAtStageDurations(std::vector<double> &sensitivities)
  {
    std::vector<double> curSens;
    for(unsigned i=0; i<m_stages.size(); i++){
      m_stages[i]->getCurrentSensitivities(curSens);
      for(unsigned j=0; j<curSens.size(); j++){
        sensitivities.push_back(curSens[j]);
      }
      curSens.clear();
    }
  }
};