/**
* @file IntegratorInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegrator                                                    \n
* =====================================================================\n
* This file contains the declarations of the input structs for creating\n
* GenericIntegrator objects.                                           \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.02.2012
*/

#pragma once

#include "MetaDataInput.hpp"
#include "DaeInitializationInput.hpp"


namespace FactoryInput{

  /**
  * @struct IntegratorInput
  * @brief information provided by user to create a GenericIntegrator object
  */
  struct IntegratorInput{
    /// @brief type of the integrator(Limex, Nixe, etc.)
    enum IntegratorType{
      NIXE, ///integrator type for integrator Nixe
      LIMEX, ///integrator type for integrator Limex
      IDAS
    };
    /// @brief order of integration
    enum IntegrationOrder{
      ZEROTH, ///zeroth order for integration (no sensitivities)
      FIRST_FORWARD, ///first order for forward integration
      FIRST_REVERSE, ///first order for reverse integration (using adjoints)
      SECOND_REVERSE ///second order for integration (automatically reverse)
    };
    ///information about options for the integrator
    std::map<std::string, std::string> integratorOptions;
    ///information for creating an IntegratorMetaData object
    IntegratorMetaDataInput metaDataInput;
    ///infromation for creating a DaeInitialization object
    DaeInitializationInput daeInitInput;
    ///type of the integrator to be created
    IntegratorType type;
    ///order of the integration (for Nixe only)
    IntegrationOrder order;
  };

}// FactoryInput
