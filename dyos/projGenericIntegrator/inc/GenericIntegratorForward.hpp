/**
* @file GenericIntegratorForward.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericIntegratorForward - Part of DyOS                              \n
* =====================================================================\n
* This file contains the declaration of the Integrator class           \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 18.10.2011
*/
#pragma once

//#include <boost/shared_ptr.hpp>
#include <memory>

#include "IntegratorMetaData.hpp"
#include "GenericIntegrator.hpp"
#include "DaeInitialization.hpp"

/**
* @class GenericIntegratorForward
*
* @brief abstract class for forward integrators used for a problem defined as IntegratorMetaData
*        object
*
* Abstract class of the integrator providing functions to integrate and calculate forward
* sensitivities up to first order of an optimization problem defined by a IntegratorMetaData
*/
class GenericIntegratorForward : virtual public GenericIntegrator
{
public:
  /**
  * @brief destructor
  */
  virtual ~GenericIntegratorForward(){};

  virtual ResultFlag initializeEso(GenericEso::Ptr genEso)
  {
    DaeInitialization::Ptr daeInit = getDaeInitializationPtr();
    // consistently initialize algebraic variables
    DaeInitialization::InitializationResult result;
    int n_cond = genEso->getNumConditions();

    if(n_cond > 0){
      utils::Array<int> curr_conditions(n_cond,0);
      genEso->evalConditions(n_cond, curr_conditions.getData());
      genEso->setConditions(n_cond, curr_conditions.getData());
      utils::Array<int> locks(n_cond, 1);
      genEso->setLocks(n_cond, locks.getData());
    }

    result = daeInit->initializeAlgebraicVariables(genEso);
    if(result.flag == DaeInitialization::FAILED){
      m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Generic Integrator: DaeInialization failed. Initialize Algebraic Variables. \n" );
      return FAILED;
    }
    else if(result.flag == DaeInitialization::PRECISION_TOO_LOW){
      std::stringstream ss;
      ss << "Warning: precision of DaeInitialization of algebraic variables too low (";
      ss << result.maxAbsError;
      ss << ")\n";
      m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, ss.str());
    }
    // initialize derivatives of differential variables
    result = daeInit->initializeDaeDerivatives(genEso);
    if(result.flag == DaeInitialization::FAILED){
      m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Generic Integrator: DaeInialization failed. Initialize derivatives. \n" );
      return FAILED;
    }
    else if(result.flag == DaeInitialization::PRECISION_TOO_LOW){
      //retry if first initialization had a too low precision
      result = daeInit->initializeDaeDerivatives(genEso);
      if(result.flag == DaeInitialization::FAILED){
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Generic Integrator: DaeInialization failed. \n" );
        return FAILED;
      }
      else if(result.flag == DaeInitialization::PRECISION_TOO_LOW){
        std::stringstream ss;
        ss << "Warning: precision of DaeInitialization of derivative variables too low (";
        ss << result.maxAbsError;
        ss << ")\n";
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, ss.str());        
      }
    }
    return SUCCESS;
  } 

  /**
  * @copydoc GenericIntegrator::solve
  */
  virtual ResultFlag solve()
  {
    ResultFlag retval;
    IntegratorMetaData::Ptr integratorMetaData = getIntegratorMDPtr();
    integratorMetaData->reset();
    const unsigned numStages = integratorMetaData->getNumStages();

    /*Iteration over all physical stages*/
    for(unsigned i=0; i<numStages; i++){
      if(i!=0){
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "|" );
      }
      integratorMetaData->initializeForFirstIntegration();
      //before initialization make sure all current control parameters are set
      integratorMetaData->setControlsToEso();
      retval = initializeEso(integratorMetaData->getGenericEso());
      integratorMetaData->initializeMatrices();
      if(retval == FAILED){
        return retval;
      }
      //store initial states to observer      
      const unsigned numStates = integratorMetaData->getGenericEso()->getNumStates();
      utils::Array<double> statesArray(numStates);
      integratorMetaData->getGenericEso()->getStateValues(statesArray.getSize(), statesArray.getData());
      integratorMetaData->storeInitialStates(statesArray);

      initializeStageIntegration();
      /*Integration for first interval*/
      retval = solveInterval();
      if (retval == FAILED)
        return retval;
      /*Integration for the next (numIntervals-1) intervals*/
      int numIntervals = integratorMetaData->getNumIntegrationIntervals();
      for(int j=1; j<numIntervals; j++){
        integratorMetaData->initializeForNextIntegration();
        retval = solveInterval();
        if(retval == FAILED){
          return retval;
        }
      }
    }
    m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "\n" );
    return SUCCESS;
  }

  /**
  * @brief integrates one interval from t_0 = 0.0 to t_f = 1.0.
  *
  * After a call to IntegratorMetaData::initializeForNextIntegration(), the data are set for a new
  * integration starting from the actual timepoint to IntegratorMetaData::getEndTime()==1.
  */
  virtual ResultFlag solveInterval()
  {
    IntegratorMetaData::Ptr integratorMetaData = getIntegratorMDPtr();

    while(integratorMetaData->getStepEndTime()<1.0){
      integratorMetaData->initializeForNewIntegrationStep();

      const unsigned numStates = integratorMetaData->getGenericEso()->getNumStates();
      utils::Array<double> statesArray(numStates);
      const unsigned numSens = numStates * integratorMetaData->getNumCurrentSensitivityParameters();
      utils::Array<double> sensArray(numSens);


      SolveStepResult stepretval;
      double startTime = integratorMetaData->getStepStartTime();
      const double endTime = integratorMetaData->getStepEndTime();
      while (startTime + 1e-10 < endTime) {
        integratorMetaData->setControlsToEso();
		//ResultFlag resFlag = initializeEso(integratorMetaData->getGenericEso());
        //if(resFlag == FAILED){
        //  return resFlag;
        //}
        try{
          stepretval = solveStep(startTime, endTime, statesArray, sensArray);
        }
        catch(std::exception &e){
          m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, e.what());
          m_logger->newline(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY);
          return FAILED;
        }catch(...){
          return FAILED;
        }
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "." );
        if (stepretval == SolveStepFailed)
          break;
        //needed to have consistent value in m_integratorMetaData (setting not done by solve)
        integratorMetaData->setStepCurrentTime(startTime);
        // m_integratorMetaData.setStates has to be called because the only state setting function that the
        // integrator accesses are callback functions - so the states are never set with the final time values
        integratorMetaData->setStates(statesArray);
        // set final sensitivities
        integratorMetaData->setSensitivities(sensArray);
        if (stepretval == SolveStepFoundRoot) {
          m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "A ROOT WAS FOUND!!!\n");
          integratorMetaData->foundRoot();
        }

      }
      if (stepretval != SolveStepSuccess) {
        m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Solution step failed!!!\n");
        return FAILED;
      }
    }
    return SUCCESS;
  }

  /**
  * @brief returns a pointer to an object of type IntegratorMetaData
  *
  * The object of type IntegratorMetaData contains all integrator relevant data
  * @return IntegratorMetaData object
  */
  virtual IntegratorMetaData::Ptr getIntegratorMDPtr() const = 0;

  /**
  * @brief returns a pointer to an object of type DaeInitialization
  *
  * @return DaeInitialization object
  */
  virtual DaeInitialization::Ptr getDaeInitializationPtr() const = 0;

  /**
   * @brief Type that is expected to be returned by the implementation of the integrator
   */
  enum SolveStepResult {
    SolveStepSuccess,
    SolveStepFailed,
    SolveStepFoundRoot,
    SolveStepInitializationFailed
  };

  /**
  * @brief actual call to the integrator to do a integration step
  *
  * @param starttime inout; input starttime of integration. On exit, holds endtime of integration
  *                         this must not be the same as endtime in case a root was found.
  * @param endtime   in ; endtime of integration. Integrator will try to integrate up to that point.
  * @param finalStates states computed by the integrator at duration.
  *                    These states have not been set in the IntegratorMetaData
  *                    necessarily
  * @param finalSensitivities sensitivities computed by the integrator at duration
  *                           this parameter will not be changed if integrator
  *                           has computed no sensitivities
  */
  virtual SolveStepResult solveStep(double& starttime,
                                    const double endtime,
                                    utils::Array<double> &finalStates,
                                    utils::Array<double> &finalSensitivities) = 0;

  /**
  * @brief initialize integrator for integration of a new stage
  *
  * overload this function if anything has to be done before integrating a new stage
  */
  virtual void initializeStageIntegration(){}

  virtual std::vector<DyosOutput::StageOutput> getOutput()
  {
    return getIntegratorMDPtr()->getOutput();
  }
  
  virtual void activatePlotGrid()
  {
    getIntegratorMDPtr()->activatePlotGrid();
  }

};
