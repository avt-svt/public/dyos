/** @file IntegratorLimex.hpp
*
*
*    =====================================================================\n
*    &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                     \n
*    =====================================================================\n
*    DyOS - Tool for dynamic optimization                                 \n
*    =====================================================================\n
*    @copydetails LimexLoader
*    =====================================================================\n
*   @author Stefan Jaeger, Klaus Stockmann, Tjalf Hoffmann
*   @date 3.9.2012
*/
#pragma once

//#include <boost/weak_ptr.hpp>
#include <memory>

#include "GenericIntegratorForward.hpp"
#include "LimexLoader.hpp"
#include "Array.hpp"
#include "DaeInitialization.hpp"

//typedef boost::shared_ptr<IntegratorMetaData> IntegratorMetaDataPtr;
/**
* @class IntegratorLimex
*
* @brief Class defining a GenericIntegrator
*
* Derived class of the class GenericIntegrator. With this class, the user is able to call the inte-
* grator LIMEX for forward integration of an optimization problem defined by the
* IntegratorMetaData object provided to the constructor.
*/
class IntegratorLimex : virtual public GenericIntegratorForward
{
public:
  typedef std::shared_ptr<IntegratorLimex> Ptr;
  struct InitialTolerance{
    double relativeTolerance;
    double absoluteTolerance;
    InitialTolerance(){
      relativeTolerance = 5e-6;
      absoluteTolerance = 5e-6;
    }
  };
protected:

	// is true for first interval integration call
	bool m_isFirstIntegrationCall;

  bool m_calculateSensitivities;
  //! number of equations without sensitivities
  int m_numModelEqns;
  //! number of sensitivity parameters for all stages
  int m_numParams;
  //! number of sensitivities
  int m_numAllSens;
  //! stepsize
  double m_hStepIn;
  double m_hStepOut;
  //! array containing the states and all sensitivities
  utils::Array<double> m_y;
  //! array containing the derivatives of the states and all sensitivities
  utils::Array<double> m_ys;
  
  //LimexLoader::LimexOptions m_options;
  //! static pointer to the integrator meta data such that we can access the required integrator information.
  static IntegratorMetaData::Ptr m_staticIntMD;
  //! shared pointer to an object of type IntegratorMetaData accessing all integration relevant data
  IntegratorMetaData::Ptr m_integratorMetaData;

  //! tolerance values to initialize m_options.relTol and m_options.absTol
  InitialTolerance m_initialTolerance;

  LimexLoader m_slimexs;

  DaeInitialization::Ptr m_daeInitialization;

  static void fcn(const int &n, int &nz, const double &t, const double *y, const double *rpar,
                  const int *ipar, double *f, double *M, int *ir, int *ic,
                  int &Info, const int *BlockConv);

  static void jacobian(const int &n, const double &t, const double *y, const double *ys,
                       const double *rpar, const int *ipar,
                       double *Jac, int *ir, int *ic, int &LenJac, int &JacInfo);

  void initOptions(LimexLoader::LimexOptions &options);
  void applyInitialValues(utils::Array<double> &y,
                          utils::Array<double> &ys);
  void setIntegratorTolerance(const double absTol, const double relTol, 
                              LimexLoader::LimexOptions &options);
  
  void printErrorMessage(const int limexRetval);

  void initializeStageIntegration();

public:
  ///@deprecated
  IntegratorLimex(const IntegratorMetaData::Ptr &integratorMetaData,
                  const InitialTolerance &initTol,
                  const bool calculateSensitivities = true);
  IntegratorLimex(const IntegratorMetaData::Ptr &integratorMetaData,
                  const InitialTolerance &initTol,
                  const bool calculateSensitivities,
                  const DaeInitialization::Ptr &daeInit);
  virtual ~IntegratorLimex();
  ResultFlag solveInterval();
  virtual GenericIntegratorForward::SolveStepResult solveStep(double& starttime,
                                                              const double endtime,
                                                              utils::Array<double> &finalStates,
                                                      utils::Array<double> &finalSensitivities);
  IntegratorMetaData::Ptr getIntegratorMDPtr() const;
  DaeInitialization::Ptr getDaeInitializationPtr() const;
};
