/**
* @file UserInput.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* UserInput                                                   \n
* =====================================================================\n
* This file contains the == operators for all structs in UserInput.hpp \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski
* @date 12.12.2012
*/
#include "UserInput.hpp"
using namespace UserInput;
/**
* @brief == operator for std::vector<T>
*
* @param[in] const std::vector<T>& vec1
* @param[in] const std::vector<T>& vec2
* @return bool
*/
template <typename T>
bool operator == (const std::vector<T>& vec1, const std::vector<T>& vec2)
{
  return vec1.size() == vec2.size() && std::equal(vec1.begin(), vec1.end(), vec2.begin());
}
/**
* @brief == operator for EsoInput
*
* @param[in] const EsoInput &other
* @return bool
*/
bool EsoInput::operator==(const EsoInput &other)const
{
    if(!(this->model==other.model))
        return false;
    if(!(this->type==other.type))
        return false;

    return true;
}
/**
* @brief == operator for WaveletAdaptationInput
*
* @param[in] const WaveletAdaptationInput &other
* @return bool
*/
bool WaveletAdaptationInput::operator==(const WaveletAdaptationInput &other)const
{
    if(!(this->maxRefinementLevel==other.maxRefinementLevel))
        return false;
    if(!(this->minRefinementLevel==other.minRefinementLevel))
        return false;
    if(!(this->horRefinementDepth==other.horRefinementDepth))
        return false;
    if(!(this->verRefinementDepth==other.verRefinementDepth))
        return false;
    if(!(this->etres==other.etres))
        return false;
    if(!(this->epsilon==other.epsilon))
        return false;

    return true;
}


/**
* @brief == operator for SWAdaptationInput
*
* @param[in] const SWAdaptationInput &other
* @return bool
*/
bool SWAdaptationInput::operator==(const SWAdaptationInput &other)const
{
    if(!(this->maxRefinementLevel==other.maxRefinementLevel))
        return false;
    if(!(this->includeTol==other.includeTol))
        return false;

    return true;
}
/**
* @brief == operator for AdaptationInput
*
* @param[in] const AdaptationInput &other
* @return bool
*/
bool AdaptationInput::operator==(const AdaptationInput &other)const
{
    if(!(this->adaptType==other.adaptType))
        return false;
    if(!(this->adaptWave==other.adaptWave))
        return false;
    if(!(this->swAdapt==other.swAdapt))
        return false;
    if(!(this->maxAdaptSteps==other.maxAdaptSteps))
        return false;

    return true;
}

/**
* @brief == operator for StructureDetectionInput
*
* @param[in] const StructureDetectionInput &other
* @return bool
*/
bool StructureDetectionInput::operator==(const StructureDetectionInput &other)const
{
    if(!(this->maxStructureSteps==other.maxStructureSteps))
        return false;

    return true;
}

/**
* @brief == operator for ParameterGridInput
*
* @param[in] const ParameterGridInput &other
* @return bool
*/
bool ParameterGridInput::operator==(const ParameterGridInput &other)const
{
  if(!(this->numIntervals==other.numIntervals))
    return false;
  if(!(this->timePoints==other.timePoints))
    return false;
  if(!(this->values==other.values))
    return false;
  if(!(this->upperBounds==other.upperBounds))
    return false;
  if(!(this->lowerBounds==other.lowerBounds))
    return false;
  if(!(this->duration==other.duration))
    return false;
  if(!(this->hasFreeDuration==other.hasFreeDuration))
    return false;
  if(!(this->type==other.type))
    return false;
  if(!(this->adapt==other.adapt))
    return false;
  if(!(this->pcresolution==other.pcresolution))
    return false;

  return true;
}
/**
* @brief == operator for ParameterInput
*
* @param[in] const ParameterInput &other
* @return bool
*/
bool ParameterInput::operator==(const ParameterInput &other)const
{
    if(!(this->name==other.name))
        return false;
    if(!(this->lowerBound==other.lowerBound))
        return false;
    if(!(this->upperBound==other.upperBound))
        return false;
    if(!(this->value==other.value))
        return false;
    if(!(this->grids==other.grids))
        return false;
    if(!(this->paramType==other.paramType))
        return false;
    if(!(this->sensType==other.sensType))
        return false;

    return true;
}
/**
* @brief == operator for IntegratorStageInput
*
* @param[in] const IntegratorStageInput &other
* @return bool
*/
bool IntegratorStageInput::operator==(const IntegratorStageInput &other)const
{
    if(!(this->duration==other.duration))
        return false;
    if(!(this->parameters==other.parameters))
        return false;

    return true;
}
/**
* @brief == operator for ConstraintInput
*
* @param[in] const ConstraintInput &other
* @return bool
*/
bool ConstraintInput::operator==(const ConstraintInput &other)const
{
    if(!(this->name==other.name))
        return false;
    if(!(this->lowerBound==other.lowerBound))
        return false;
    if(!(this->upperBound==other.upperBound))
        return false;
    if(!(this->timePoint==other.timePoint))
        return false;
    if(!(this->lagrangeMultiplier==other.lagrangeMultiplier))
        return false;
    if(!(this->type==other.type))
        return false;

    return true;
}
/**
* @brief == operator for OptimizerStageInput
*
* @param[in] const OptimizerStageInput &other
* @return bool
*/
bool OptimizerStageInput::operator==(const OptimizerStageInput &other)const
{
    if(!(this->objective==other.objective))
        return false;
    if(!(this->constraints==other.constraints))
        return false;
    if(!(this->structureDetection==other.structureDetection))
        return false;

    return true;
}
/**
* @brief == operator for StageMapping
*
* @param[in] const StageMapping &other
* @return bool
*/
bool StageMapping::operator==(const StageMapping &other)const
{
    if(!(this->fullStateMapping==other.fullStateMapping))
        return false;
    if(!(this->stateNameMapping==other.stateNameMapping))
        return false;

    return true;
}
/**
* @brief == operator for StageInput
*
* @param[in] const StageInput &other
* @return bool
*/
bool StageInput::operator==(const StageInput &other)const
{
    if(!(this->treatObjective==other.treatObjective))
        return false;
    if(!(this->mapping==other.mapping))
        return false;
    if(!(this->eso==other.eso))
        return false;
    if(!(this->integrator==other.integrator))
        return false;
    if(!(this->optimizer==other.optimizer))
        return false;

    return true;
}
/**
* @brief == operator for LinearSolverInput
*
* @param[in] const LinearSolverInput &other
* @return bool
*/
bool LinearSolverInput::operator==(const LinearSolverInput &other)const
{
    if(!(this->type==other.type))
        return false;

    return true;
}
bool NonLinearSolverInput::operator==(const NonLinearSolverInput &other)const
{
    if(!(this->type==other.type))
        return false;
    if(!(this->tolerance==other.tolerance))
        return false;

    return true;
}
/**
* @brief == operator for DaeInitializationInput
*
* @param[in] const DaeInitializationInput &other
* @return bool
*/
bool DaeInitializationInput::operator==(const DaeInitializationInput &other)const
{
  if(!(this->type==other.type))
    return false;
  if(!(this->linSolver==other.linSolver))
    return false;
  if(!(this->nonLinSolver==other.nonLinSolver))
    return false;
  if(!(this->maximumErrorTolerance==other.maximumErrorTolerance))
    return false;

  return true;
}

bool IntegratorInput::operator==(const IntegratorInput &other)const
{
  if(!(this->type==other.type))
    return false;
  if(!(this->order==other.order))
    return false;
  if(!(this->daeInit==other.daeInit))
    return false;

  return true;
}

bool AdaptationOptions::operator ==(const AdaptationOptions &other)const
{
  if(!(this->adaptationThreshold==other.adaptationThreshold))
    return false;
  if(!(this->intermConstraintViolationTolerance==other.intermConstraintViolationTolerance))
    return false;
  if(!(this->adaptStrategy==other.adaptStrategy))
    return false;
  if(!(this->numOfIntermPoints==other.numOfIntermPoints))
    return false;

  return true;
}
/**
* @brief == operator for OptimizerInput
*
* @param[in] const OptimizerInput &other
* @return bool
*/
bool OptimizerInput::operator==(const OptimizerInput &other)const
{   
  if(!(this->optimizerOptions==other.optimizerOptions))
    return false;
  if(!(this->type==other.type))
    return false;
  if(!(this->globalConstraints==other.globalConstraints))
    return false;
  if(!(this->optimizationMode == other.optimizationMode))
    return false;

  return true;
}
/**
* @brief == operator for Input
*
* @param[in] const Input &other
* @return bool
*/
bool Input::operator==(const Input &other)const
{
    if(!(this->stages==other.stages))
        return false;
    if(!(this->totalEndTimeLowerBound==other.totalEndTimeLowerBound))
        return false;
    if(!(this->totalEndTimeUpperBound==other.totalEndTimeUpperBound))
        return false;
    if(!(this->runningMode==other.runningMode))
        return false;
    if(!(this->integratorInput==other.integratorInput))
        return false;
    if(!(this->optimizerInput==other.optimizerInput))
        return false;

    return true;
}
