#include <iostream>
#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include <utility>
#include <string>
#include "DyosStructureDetectionDummies.hpp"
#include "DyosAdaptationDummies.hpp"
#include "GridRefinementFactory.hpp"
#include "InputExceptions.hpp"
#include "ConvertToXmlJson.hpp"
using namespace UserInput;

#include "ConvertFromXmlJson.hpp"
#include "ConvertToXmlJson.hpp"
#include "DyosWithLogger.hpp"


BOOST_AUTO_TEST_SUITE(MultiStageSystemTests)

/**
* @test TestDyos - system test of runDyos with the expanded car example in multi stage mode
*
* divide the example in three stages with full state mapping between all stages - first using
* the fullStateMapping flag and then map all variables explicitly
*/
#ifdef BUILD_LIMEX
BOOST_AUTO_TEST_CASE(ExpandedCarExampleMultiStage)
{
  const std::string varNameA = "a";
  const std::string varNameObj = "obj";
  const std::string varNameV = "v";
  const std::string varNameT = "t";
  const std::string varNameDer = "deriv";
  const std::string varNameS = "s";
  StageInput stage1;

  stage1.eso.type = EsoType::JADE;
  stage1.eso.model = "carErweitert";

  stage1.integrator.duration.lowerBound = 1e-3;
  stage1.integrator.duration.upperBound = 10.0;
  stage1.integrator.duration.value = 1.0;
  stage1.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  stage1.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stage1.integrator.parameters.resize(1);
  stage1.integrator.parameters[0].name = varNameA;
  stage1.integrator.parameters[0].lowerBound = -2.0;
  stage1.integrator.parameters[0].upperBound = 2.0;
  stage1.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stage1.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;


  const unsigned numIntervals = 4;
  stage1.integrator.parameters[0].grids.resize(1);
  stage1.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stage1.integrator.parameters[0].grids[0].values.resize(numIntervals, 1.23);
  stage1.integrator.parameters[0].grids[0].values[0] = 2.0;
  stage1.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_CONSTANT;

  stage1.optimizer.objective.name = varNameObj;
  stage1.optimizer.objective.lowerBound = -100000.0;
  stage1.optimizer.objective.upperBound =  100000.0;

  stage1.optimizer.constraints.resize(1);
  stage1.optimizer.constraints[0].name = varNameV;
  stage1.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
  stage1.optimizer.constraints[0].lowerBound = 0.0;
  stage1.optimizer.constraints[0].upperBound = 10.0;

  stage1.mapping.fullStateMapping = true;
  stage1.treatObjective = false;

  StageInput stage2 = stage1;
  stage2.mapping.fullStateMapping = false;
  stage2.mapping.stateNameMapping[varNameT] = varNameT;
  stage2.mapping.stateNameMapping[varNameObj] = varNameObj;
  stage2.mapping.stateNameMapping[varNameV] = varNameV;
  stage2.mapping.stateNameMapping[varNameDer] = varNameDer;
  stage2.mapping.stateNameMapping[varNameS] = varNameS;
  for(unsigned i=0; i<numIntervals; i++){
    stage2.integrator.parameters[0].grids[0].values[i] = -1.73;
  }
  stage2.integrator.duration.sensType = UserInput::ParameterInput::NO;

  StageInput stage3 = stage1;
  stage3.mapping.fullStateMapping = false;
  for(unsigned i=0; i<numIntervals; i++){
    stage3.integrator.parameters[0].grids[0].values[i] = 1.92;
  }
  stage3.integrator.parameters[0].grids[0].values.back() = -2.0;
  stage3.integrator.duration.value = 2.0;

  stage3.treatObjective = true;

  Input input;

  input.stages.push_back(stage1);
  input.stages.push_back(stage2);
  input.stages.push_back(stage3);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
  input.totalEndTimeLowerBound = 4.0;
  input.totalEndTimeUpperBound = 4.0;
  UserOutput::Output output;

  double totalDuration = 0.0;
#ifdef BUILD_FILTERSQP //DO_FILTER_SQP_TEST
  input.integratorInput.order = UserInput::IntegratorInput::SECOND_REVERSE;
  input.optimizerInput.type = UserInput::OptimizerInput::FILTER_SQP;
  output = runDyos(input);

  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
  
  //check if sum of durations satisfy global limits
  for(unsigned i=0; i<output.stages.size(); i++){
    totalDuration += output.stages[i].integrator.durations.front().value;
  }
  BOOST_CHECK_CLOSE(totalDuration, output.totalEndTimeLowerBound, THRESHOLD);
#endif

#ifdef BUILD_SNOPT
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.integratorInput.type = UserInput::IntegratorInput::LIMEX;
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  output = runDyos(input);
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
#endif
#ifdef BUILD_IPOPT
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  input.integratorInput.type = UserInput::IntegratorInput::LIMEX;
  input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;
  output = runDyos(input);
  BOOST_CHECK_EQUAL(output.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);
#endif
  //check if sum of durations satisfy global limits
  totalDuration = 0.0;
  for(unsigned i=0; i<output.stages.size(); i++){
    totalDuration += output.stages[i].integrator.durations.front().value;
  }
  //global lower bound == global upper bound
  BOOST_CHECK_CLOSE(totalDuration, output.totalEndTimeLowerBound, THRESHOLD);
}
#endif



UserInput::Input createInputForMultiStageTest()
{
  StageInput stageInput;
  stageInput.eso.type = EsoType::JADE;
  stageInput.eso.model = "carFreeFinalTimeTwoObjectives";

  stageInput.integrator.duration.lowerBound = 20.0;
  stageInput.integrator.duration.upperBound = 20.0;
  stageInput.integrator.duration.value = 20.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

  stageInput.integrator.parameters.resize(1);
  stageInput.integrator.parameters[0].name = "accel";
  stageInput.integrator.parameters[0].lowerBound = -2.0;
  stageInput.integrator.parameters[0].upperBound = 2.0;
  stageInput.integrator.parameters[0].paramType = UserInput::ParameterInput::PROFILE;
  stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

  const unsigned int numIntervals = 5;
  stageInput.integrator.parameters[0].grids.resize(1);
  stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
  stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals+1, 2.0);
  stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;

  stageInput.optimizer.objective.name = "fuel";
  stageInput.optimizer.objective.lowerBound = 0.0;
  stageInput.optimizer.objective.upperBound = 500.0;

  stageInput.optimizer.constraints.resize(1);
  stageInput.optimizer.constraints[0].name = "velo";
  stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
  stageInput.optimizer.constraints[0].lowerBound = 0.0;
  stageInput.optimizer.constraints[0].upperBound = 10.0;


  Input input;

  input.stages.push_back(stageInput);
  input.stages.back().mapping.fullStateMapping = true;
  
  stageInput.integrator.duration.lowerBound = 5.0;
  stageInput.integrator.duration.upperBound = 5.0;
  stageInput.integrator.duration.value = 5.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
  
  UserInput::ParameterInput initialValueParameter;
  initialValueParameter.name = "velo";
  initialValueParameter.sensType = UserInput::ParameterInput::FRACTIONAL;
  initialValueParameter.paramType = UserInput::ParameterInput::INITIAL;
  initialValueParameter.value = 5.0;
  
  stageInput.integrator.parameters.push_back(initialValueParameter);
  
  input.stages.push_back(stageInput);
  input.stages.back().mapping.fullStateMapping = true;
  
  stageInput.integrator.duration.lowerBound = 0.1;
  stageInput.integrator.duration.upperBound = 100.0;
  stageInput.integrator.duration.value = 15.0;
  stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;
  
  stageInput.integrator.parameters.back().name = "ttime";
  stageInput.integrator.parameters.back().value = 0.0;
  
  UserInput::ConstraintInput constraint;
  constraint.name = "velo";
  constraint.type = UserInput::ConstraintInput::ENDPOINT;
  constraint.lowerBound = 0.0;
  constraint.upperBound = 0.0;
  
  stageInput.optimizer.constraints.push_back(constraint);
  
  
  constraint.name = "dist";
  constraint.lowerBound = 300.0;
  constraint.upperBound = 300.0;
  
  stageInput.optimizer.constraints.push_back(constraint);
  
  stageInput.optimizer.objective.name = "ttime";
  stageInput.optimizer.objective.lowerBound = 0.0;
  stageInput.optimizer.objective.upperBound = 100.0;
  
  input.stages.push_back(stageInput);

  input.runningMode = Input::SINGLE_SHOOTING;
  input.integratorInput.type = UserInput::IntegratorInput::LIMEX;
  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
    input.optimizerInput.type = UserInput::OptimizerInput::IPOPT;

    return input;
}

#ifdef BUILD_LIMEX
BOOST_AUTO_TEST_CASE(TestCustomObjective)

{
    UserInput::Input input=createInputForMultiStageTest();
    input.optimizerInput.optimizerOptions["print_level"] = "0";
    Logger::Ptr logger(new StandardLogger());
    OutputChannel::Ptr   myOutChannel ( new  StandardOut() );

    myOutChannel->setVerbosity( OutputChannel::ERROR_VERBOSITY);
    logger->setLoggingChannel(Logger::LoggingChannel::DYOS_OUT,myOutChannel);
    logger->setLoggingChannel(Logger::LoggingChannel::INTEGRATOR,myOutChannel);
    logger->setLoggingChannel(Logger::LoggingChannel::OPTIMIZER,myOutChannel);
    logger->setLoggingChannel(Logger::LoggingChannel::ESO,myOutChannel);


    UserOutput::Output output;
    //check 1: check enabling custom objective function but with 0 factor for penalty does not change results

    {
        output = runDyos(input,logger);
        //save normal optimization output
        double oldObj=output.optimizerOutput.objVal;


        double originalObjectiveFactor=1;
        double penaltyFactor          =0.0;

        input.stages.at(0).optimizer.customObjectiveInformation.penaltyFactor=0.0;
        input.stages.at(0).optimizer.customObjectiveInformation.originalObjectiveFactor=1.0;
        //dummy penalty that is ignored
        input.stages.at(0).optimizer.customObjectiveInformation.penaltyFunction=
            [](std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<UserInput::ADdouble>> requestedParameterizations)
        {

            std::vector<UserInput::ADdouble> accelValuesOfFirstGrid=requestedParameterizations.at(std::make_pair("accel",0));
            return UserInput::ADdouble(1.0);
        };

        input.stages.at(0).optimizer.customObjectiveInformation.requestedListOfParameterizationGrids.push_back(std::make_tuple("accel",0,6));
        output= runDyos(input,logger);
        BOOST_REQUIRE_CLOSE(oldObj,output.optimizerOutput.objVal,0.01);
    }
    //check 2: change objective function in multiple stages and check if the objective is correctyl computed
    {
        input.stages.at(0).optimizer.customObjectiveInformation.penaltyFactor=1.0;
        input.stages.at(0).optimizer.customObjectiveInformation.originalObjectiveFactor=0.0;

        input.stages.at(1).optimizer.customObjectiveInformation.penaltyFactor=1.0;
        input.stages.at(1).optimizer.customObjectiveInformation.originalObjectiveFactor=0.0;
        input.stages.at(1).optimizer.customObjectiveInformation.penaltyFunction=
            [](std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<UserInput::ADdouble>> requestedParameterizations)
        {
            return UserInput::ADdouble(2.1);
        };
        input.stages.at(2).optimizer.customObjectiveInformation.penaltyFactor=1.0;
        input.stages.at(2).optimizer.customObjectiveInformation.originalObjectiveFactor=0.0;
        input.stages.at(2).optimizer.customObjectiveInformation.penaltyFunction=
            [](std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<UserInput::ADdouble>> requestedParameterizations)
        {
            return UserInput::ADdouble(0.01);
        };
        output=runDyos(input,logger);
        BOOST_CHECK_CLOSE(1.0+2.1+0.01,output.optimizerOutput.objVal,0.01);
    }



    //check 3: check right exception when a grid has Adaptation enabled
    {
        input.optimizerInput.adaptationOptions.adaptStrategy=UserInput::AdaptationOptions::ADAPTATION;
        //make one constraint more difficult so adaptation is triggered
        input.stages[0].optimizer.constraints[0].lowerBound = 2.0;
        input.stages[1].optimizer.constraints[0].lowerBound = 2.0;
        input.stages[2].optimizer.constraints[0].lowerBound = 2.0;


        //throw if adaptation is enabled on a grid that is requested
        input.optimizerInput.adaptationOptions.numOfIntermPoints=2;
        input.stages[0].integrator.parameters[0].grids[0].adapt.maxAdaptSteps=3;
        BOOST_CHECK_THROW(runCreateProblemInputToCheckForExceptions(input,logger),InputException);
    }
    //check 5: throw if structure detection is enabled on a stage with custom objective
    {
        input.stages[0].integrator.parameters[0].grids[0].adapt.maxAdaptSteps=0;
        input.stages[0].optimizer.structureDetection.maxStructureSteps=1;
        BOOST_CHECK_THROW(runCreateProblemInputToCheckForExceptions(input,logger),InputException);
    }

    //check 6: no throw if adaptation is enabled on a grid that is not requested
    {
        input.optimizerInput.adaptationOptions.adaptStrategy=UserInput::AdaptationOptions::ADAPT_STRUCTURE;
        input.stages[0].optimizer.structureDetection.maxStructureSteps=0;
        input.stages[1].optimizer.structureDetection.maxStructureSteps=1;
        input.stages[2].optimizer.structureDetection.maxStructureSteps=1;
        input.stages[1].integrator.parameters[0].grids[0].adapt.maxAdaptSteps=3;
        input.stages[0].integrator.parameters[0].grids[0].adapt.maxAdaptSteps=0;

        BOOST_CHECK_NO_THROW(runDyos(input,logger));
    }
    //check 7: check error is thrown if inputs contains no custom objective for a stage, but requsted parametrizations
    {
        input.stages.at(0).optimizer.customObjectiveInformation.penaltyFunction= {};
        //requstedParametrization still filled from test 1
        BOOST_CHECK_THROW(runCreateProblemInputToCheckForExceptions(input,logger),InputException);
    }
    //reset the harder constraints and disable adaptation
    input.optimizerInput.adaptationOptions.adaptStrategy=UserInput::AdaptationOptions::NOADAPTATION;
    input.stages[0].optimizer.constraints[0].lowerBound = 0.0;
    input.stages[1].optimizer.constraints[0].lowerBound = 0.0;
    input.stages[2].optimizer.constraints[0].lowerBound = 0.0;


    //check 8: check if the objective is actuall optimized for.
    {
        //Put a penalty on divergence from accell=1 in the first stage. All other objectives are constants.
        input.stages.at(0).optimizer.customObjectiveInformation.penaltyFunction=
            [](std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<UserInput::ADdouble>> requestedParameterizations)
        {

            std::vector<UserInput::ADdouble> accelValuesOfFirstGrid=requestedParameterizations.at(std::make_pair("accel",0));
            UserInput::ADdouble obj=0.0;
            for(int i=0; i<6; i++)
                {
                    obj+=pow(accelValuesOfFirstGrid[i]-0.1,2);
                }
            return obj;
        };
        //disable all constraints in stage 2 and 1
        input.stages[1].optimizer.constraints.clear();
        input.stages[0].optimizer.constraints.clear();
        input.optimizerInput.optimizationMode=UserInput::OptimizerInput::OptimizationMode::MINIMIZE;

        output=runDyos((input),logger);
        std::vector<UserOutput::ParameterOutput>::iterator paramOutIt=std::find_if(output.stages[0].integrator.parameters.begin(),output.stages[0].integrator.parameters.end(),[](UserOutput::ParameterOutput in)
        {
            return in.name=="accel";
        });
        double objManual=2.1+0.01;
        for(int i=0; i<paramOutIt->grids[0].values.size(); i++)
            {
                BOOST_CHECK_CLOSE(paramOutIt->grids[0].values.at(i),0.1,0.01);
                objManual+=std::pow(paramOutIt->grids[0].values.at(i)-0.1,1);
            }
        BOOST_CHECK_CLOSE(objManual,output.optimizerOutput.objVal,0.01);
        
        BOOST_CHECK(output.stages.size()==3);
    }

}
#endif


#ifdef BUILD_SNOPT

BOOST_AUTO_TEST_CASE(TestMultiStageDifferentObjective)
{
    UserInput::Input input=createInputForMultiStageTest();
  input.optimizerInput.type = OptimizerInput::SENSITIVITY_INTEGRATION;

  input.optimizerInput.optimizerOptions["verify level"] = "3";
  UserOutput::Output output;
  output = runDyos(input);

  //results were checked with the derivative checker of SNOPT (largest relative error 2.65-05)
  //unfortunately the results are not easily checked
  //all results are printed out with the accuracy of 1e-7
    const double threshold = 2e-4; //in percent so actually 2e-6
  
  //////////////////
  // test stage 1 //
  //////////////////
  //on stage 1 there are only sensitivities for the 6 control parameters
  BOOST_REQUIRE_EQUAL(output.stages.size(), 3);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities.size(), 6);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[0].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[0].values[0].size(), 2);
  BOOST_CHECK_SMALL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[0].values[0][0],
                    THRESHOLD);
  BOOST_CHECK_SMALL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[0].values[0][1],
                    THRESHOLD);

  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[1].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[1].values[0].size(), 2);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[1].values[0][0],
                    1.87307214, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[1].values[0][1],
                    1.92340888, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[2].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[2].values[0].size(), 3);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[2].values[0][0],
                    1.49522099, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[2].values[0][1],
                    3.23938219, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[2].values[0][2],
                    1.83775683, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[3].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[3].values[0].size(), 4);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[3].values[0][0],
                    1.06108420, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[3].values[0][1],
                    2.29882892, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[3].values[0][2],
                    2.88689320, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[3].values[0][3],
                    1.77527044, threshold);
  
  
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values[0].size(), 5);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values[0][0],
                    0.692180732, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values[0][1],
                    1.49960303, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values[0][2],
                    1.88321704, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values[0][3],
                    2.66125124, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[4].values[0][4],
                    1.73373567, threshold);
                    
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values.size(), 2);
  BOOST_REQUIRE_EQUAL(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0].size(), 6);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0][0],
                    0.427225240, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[1][0],
                    24.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0][1],
                    0.925579067, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[1][1],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0][2],
                    1.16235179, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[1][2],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0][3],
                    1.64256699, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[1][3],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0][4],
                    2.52403801, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[1][4],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[0][5],
                    1.70778890, threshold);
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters.front().grids.front().firstSensitivities[5].values[1][5],
                    24.0, THRESHOLD);

  ////////////////////
  //  test stage 2  //
  ////////////////////
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters.size(), 2);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities.size(), 6);
  //since velo has been reset on stage 2 all parameters of stage 1 have no longer any effect on the constraint
  for(unsigned i=0; i<6; i++){
    BOOST_REQUIRE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[1].values.size() >= 1);
    BOOST_REQUIRE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[1].values[0].size() >= 6);
    for(unsigned j=0; j<6; j++){
      BOOST_CHECK_SMALL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[i].values[0][j],
                        THRESHOLD);
    }
  }
  
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[0].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[0].values[0].size(), 8);
  BOOST_CHECK_SMALL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][6],
                    THRESHOLD);
  BOOST_CHECK_SMALL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][7],
                    THRESHOLD);

  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][6],
                    0.489785907, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][7],
                    0.494677133, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[2].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[2].values[0].size(), 9);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][6],
                    0.470959408, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][7],
                    0.962410990, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][8],
                    0.493153058, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[3].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[3].values[0].size(), 10);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][6],
                    0.448782864, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][7],
                    0.917092965, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][8],
                    0.953782531, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][9],
                    0.491697260, threshold);
  
  
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values[0].size(), 11);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][6],
                    0.423983589, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][7],
                    0.866415360, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][8],
                    0.901077499, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][9],
                    0.945637727, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][10],
                    0.490318926, threshold);
                    
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values.size(), 2);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0].size(), 12);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1].size(), 12);
  //objtive derivatives of 1st stage parameters
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][0],
                    24.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][1],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][2],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][3],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][4],
                    48.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][5],
                    24.0, THRESHOLD);
  
  
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][6],
                    0.397308695, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][6],
                    6.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][7],
                    0.811904907, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][7],
                    12.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][8],
                    0.844386280, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][8],
                    12.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][9],
                    0.886143005, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][9],
                    12.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][10],
                    0.938011766, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][10],
                    12.0, THRESHOLD);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][11],
                    0.489024655, threshold);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][11],
                    6.0, THRESHOLD);
  
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities.size(), 6);
  
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[0].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[0].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[0].values[0][0],
                    1.0, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[1].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[1].values[0][0],
                    0.970639828, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[2].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[2].values[0][0],
                    0.933330160, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[3].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[3].values[0][0],
                    0.889381495, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[4].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[4].values[0][0],
                    0.840235196, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[5].values.size(), 2);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[5].values[0].size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[5].values[1].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[5].values[0][0],
                    0.787371866, threshold);
  BOOST_CHECK_SMALL(output.stages[1].integrator.parameters[1].grids.front().firstSensitivities[5].values[1][0], THRESHOLD);
  
  
  ////////////////////
  //  test stage 3  //
  ////////////////////
  //in this stage ttime is reset and the new objective, so all parameters from previous stages should be 0.0
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters.size(), 3);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities.size(), 6);
  //since velo has been reset on stage 2 all parameters of stage 1 have no longer any effect on the constraint
  for(unsigned i=0; i<6; i++){
    BOOST_REQUIRE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[i].values.size() >= 1);
    BOOST_REQUIRE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0].size() >= 6);
    for(unsigned j=0; j<6; j++){
      BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[i].values[0][j],
                        THRESHOLD);
    }
  }
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0].size(), 14);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][6],
                    0.397308695, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][7],
                    0.811904907, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][8],
                    0.844386280, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][9],
                    0.886143005, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][10],
                    0.938011766, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][11],
                    0.489024655, threshold);
  BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][12], THRESHOLD);
  BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[0].values[0][13], THRESHOLD);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0].size(), 14);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][6],
                    0.312985500, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][7],
                    0.639589483, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][8],
                    0.665177140, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][9],
                    0.698071586, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][10],
                    0.738931931, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][11],
                    0.385236034, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][12],
                    1.27480594, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[1].values[0][13],
                    1.38060708, threshold);
  
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0].size(), 15);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][6],
                    0.233561382, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][7],
                    0.477285382, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][8],
                    0.496379841, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][9],
                    0.520926896, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][10],
                    0.551418400, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][11],
                    0.287477410, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][12],
                    0.951307452, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][13],
                    2.26234317, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[2].values[0][14],
                    1.35858629, threshold);

  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0].size(), 16);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][6],
                    0.167365878, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][7],
                    0.342014104, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][8],
                    0.355696849, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][9],
                    0.373286826, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][10],
                    0.395136488, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][11],
                    0.206001131, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][12],
                    0.681689777, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][13],
                    1.62115435, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][14],
                    2.17491103, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[3].values[0][15],
                    1.34265903, threshold);
                    
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0].size(), 17);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][6],
                    0.116486891, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][7],
                    0.238042307, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][8],
                    0.247565517, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][9],
                    0.259808167, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][10],
                    0.275015563, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][11],
                    0.143377083, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][12],
                    0.474457063, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][13],
                    1.12832575, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][14],
                    1.51374120, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][15],
                    2.11444745, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[4].values[0][16],
                    1.33149983, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values.size(), 3);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0].size(), 18);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][6],
                    0.0794436813, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][7],
                    0.162344080, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][8],
                    0.168838879, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][9],
                    0.177188326, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][10],
                    0.187559721, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][11],
                    0.0977827048, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][12],
                    0.323578176, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][13],
                    0.769514497, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][14],
                    1.03236658, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][15],
                    1.44204629, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][16],
                    2.07340766, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[0][17],
                    1.32385413, threshold);
  
  //check derivatives of endpoint constraint dist
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1].size(), 18);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][0],
                    22.7562978, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][1],
                    37.8500901, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][2],
                    28.9496026, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][3],
                    21.3050568, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][4],
                    12.4044752, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][5],
                    2.37110680, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][6],
                    5.31126674, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][7],
                    10.1770948, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][8],
                    9.56200652, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][9],
                    9.00807617, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][10],
                    8.50420007, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][11],
                    4.09462228, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][12],
                    11.4156057, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][13],
                    20.4063570, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][14],
                    16.7907298, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][15],
                    12.6748486, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][16],
                    7.30668278, threshold);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[1][17],
                    1.36626281, threshold);
  
  for(unsigned i=0; i<18; i++){
    BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[0].grids.front().firstSensitivities[5].values[2][i], THRESHOLD);
  }
  
  //check initial value parameter for ttime
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.size(),1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities.size(), 6);
  
  for(unsigned i=0; i<5; i++){
    BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[i].values.size(), 1);
    BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[i].values[0].size(), 1);
    BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[i].values[0][0], THRESHOLD);
  }
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values.size(), 3);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values[0].size(), 1);
  BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values[0][0], THRESHOLD);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values[1].size(), 1);
  BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values[1][0], THRESHOLD);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values[2].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[1].grids.front().firstSensitivities[5].values[2][0], 
                    1.0, THRESHOLD);
  
  
  //check initial value parameter for velo
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.size(),1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities.size(), 6);

  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[0].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[0].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[0].values[0][0],
                    0.787371866, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[1].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[1].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[1].values[0][0],
                    0.620263236, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[2].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[2].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[2].values[0][0],
                    0.462863420, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[3].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[3].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[3].values[0][0],
                    0.331679586, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[4].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[4].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[4].values[0][0],
                    0.230849468, threshold);
  
  
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values.size(), 3);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values[0][0],
                    0.157438587, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values[1].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values[1][0], 
                    10.8583947, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values[2].size(), 1);
  BOOST_CHECK_SMALL(output.stages[2].integrator.parameters[2].grids.front().firstSensitivities[5].values[2][0], THRESHOLD);
  
  //check duration parameter of 3rd stage
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities.size(), 6);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[0].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[0].values[0].size(), 1);
  BOOST_CHECK_SMALL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[0].values[0][0],
                    THRESHOLD);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[1].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[1].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[1].values[0][0],
                    0.240352025, threshold);
  
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[2].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[2].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[2].values[0][0],
                    0.358719157, threshold);
                    
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[3].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[3].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[3].values[0][0],
                    0.385577509, threshold);
                    
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[4].values.size(), 1);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[4].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[4].values[0][0],
                    0.357816659, threshold);
                    
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values.size(), 3);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values[0].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values[0][0],
                    0.305037246, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values[1].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values[1][0],
                    26.0381436, threshold);
  BOOST_REQUIRE_EQUAL(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values[2].size(), 1);
  BOOST_CHECK_CLOSE(output.stages[2].integrator.durations.front().grids.front().firstSensitivities[5].values[2][0],
                    1.0, THRESHOLD);

  ////////////////////////////////////////////////////
  //  test calculation of sensitivity initial value //
  ////////////////////////////////////////////////////

  //set initial value as decision variable (otherwise corresponding sensitivity is initialized with 0) 
  input.stages[1].integrator.parameters[1].sensType = UserInput::ParameterInput::FULL;
  input.stages[1].integrator.parameters[1].lowerBound = 0.0;
  input.stages[1].integrator.parameters[1].upperBound = 10.0;
  
  // increase pcresolution to avoid oscillation of accel
  input.stages[2].integrator.parameters[0].grids[0].pcresolution = 4;


  //use SNOPT sensitivity check 
  input.optimizerInput.type = UserInput::OptimizerInput::SNOPT;
  input.optimizerInput.optimizerOptions["verify level"] = "3";
  
  UserOutput::Output output2;
  output2 = runDyos(input);

  BOOST_CHECK_EQUAL(output2.optimizerOutput.resultFlag, UserOutput::OptimizerOutput::OK);

  //results were checked with the derivative checker of SNOPT (largest relative error 4.95-05)
}
#endif

/**
* @test TestDyos - system test to check whether in multistage duration parameter sensitivities are correctly propagated 
*                  through the stages
*/
//BOOST_AUTO_TEST_CASE(TestCarSensTypeNoOnLastStage)
//{
//  const std::string ttime = "ttime";
//  const std::string accel = "accel";
//  const std::string velo = "velo";
//  const std::string alpha = "alpha";
//  const std::string dist = "dist";
  
//  UserInput::Input input;
  
//  UserInput::EsoInput eso;
//  eso.type = UserInput::EsoInput::JADE;
//  eso.model = "carFreeFinalTime";
  
//  UserInput::StageInput stage1;
//  {
//    stage1.eso = eso;
//    stage1.mapping.fullStateMapping = true;
    
//    UserInput::IntegratorStageInput integratorInput;
//    integratorInput.duration.paramType = UserInput::ParameterInput::DURATION;
//    integratorInput.duration.sensType = UserInput::ParameterInput::FULL;
//    integratorInput.duration.value = 5.0;
//    integratorInput.duration.lowerBound = 0.0;
//    integratorInput.duration.upperBound = 10.0;
    
//    UserInput::ParameterInput accelParam;
//    accelParam.name = accel;
//    accelParam.lowerBound = 0.0;
//    accelParam.upperBound = 2.0;
//    accelParam.paramType = UserInput::ParameterInput::TIME_INVARIANT;
//    accelParam.sensType = UserInput::ParameterInput::NO;
//    accelParam.value = 2.0;
    
//    integratorInput.parameters.push_back(accelParam);
//    stage1.integrator = integratorInput;
    
//    UserInput::OptimizerStageInput optimizerInput;
    
//    UserInput::ConstraintInput veloConstraint;
//    veloConstraint.name = velo;
//    veloConstraint.type = UserInput::ConstraintInput::PATH;
//    veloConstraint.lowerBound = 0.0;
//    veloConstraint.upperBound = 10.0;
    
//    optimizerInput.constraints.push_back(veloConstraint);
//    optimizerInput.objective.name = ttime;
    
//    stage1.optimizer = optimizerInput;
//    stage1.treatObjective = true;
//  }
//  input.stages.push_back(stage1);
  
//  UserInput::StageInput stage2;
//  {
//    stage2.eso = eso;
//    stage2.mapping.fullStateMapping = true;
    
//    UserInput::IntegratorStageInput integratorInput;
    
//    integratorInput.duration.paramType = UserInput::ParameterInput::DURATION;
//    integratorInput.duration.sensType = UserInput::ParameterInput::FRACTIONAL;
//    integratorInput.duration.value = 20.0;
//    integratorInput.duration.lowerBound = 20.0;
//    integratorInput.duration.upperBound = 20.0;
    
//    UserInput::ParameterInput accelParam;
//    accelParam.name = accel;
//    accelParam.lowerBound = -2.0;
//    accelParam.upperBound = 2.0;
//    accelParam.paramType = UserInput::ParameterInput::PROFILE;
//    accelParam.sensType = UserInput::ParameterInput::FULL;
    
//    UserInput::ParameterGridInput grid;
//    grid.numIntervals = 10;
//    grid.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
//    grid.values.resize(grid.numIntervals, 1.98);
    
//    accelParam.grids.push_back(grid);
    
//    integratorInput.parameters.push_back(accelParam);
    
//    stage2.integrator = integratorInput;
    
//    UserInput::OptimizerStageInput optimizerInput;
//    optimizerInput.objective.name = ttime;
    
//    stage2.optimizer = optimizerInput;
//    stage2.treatObjective = true;
//  }
  
//  input.stages.push_back(stage2);
  
//  UserInput::StageInput stage3;
//  {
//    stage3.eso = eso;
//    UserInput::IntegratorStageInput integratorInput;
//    integratorInput.duration.paramType = UserInput::ParameterInput::DURATION;
//    integratorInput.duration.sensType = UserInput::ParameterInput::NO;
//    integratorInput.duration.value = 2.0;
//    integratorInput.duration.upperBound = 2.0;
//    integratorInput.duration.lowerBound = 2.0;
    
//    UserInput::ParameterInput accelParam;
//    accelParam.name = accel;
//    accelParam.paramType = UserInput::ParameterInput::TIME_INVARIANT;
//    accelParam.sensType = UserInput::ParameterInput::NO;
//    accelParam.upperBound = 2.0;
//    accelParam.lowerBound = -2.0;
//    accelParam.value = -2.0;
    
//    integratorInput.parameters.push_back(accelParam);
//    stage3.integrator = integratorInput;
    
//    UserInput::OptimizerStageInput optimizerInput;
//    optimizerInput.objective.name = ttime;
    
//    UserInput::ConstraintInput velConstraint, distConstraint, velConstraintEnd;
    
//    velConstraint.name = velo;
//    velConstraint.type = UserInput::ConstraintInput::PATH;
//    velConstraint.lowerBound = 0.0;
//    velConstraint.upperBound = 10.0;
    
//    distConstraint.name = dist;
//    distConstraint.type = UserInput::ConstraintInput::ENDPOINT;
//    distConstraint.lowerBound = 300.0;
//    distConstraint.upperBound = 300.0;
    
//    velConstraintEnd.name = velo;
//    velConstraint.type = UserInput::ConstraintInput::ENDPOINT;
//    velConstraint.lowerBound = 0.0;
//    velConstraint.upperBound = 0.0;
    
//    optimizerInput.constraints.push_back(velConstraint);
//    optimizerInput.constraints.push_back(distConstraint);
//    optimizerInput.constraints.push_back(velConstraintEnd);
    
//    stage3.optimizer = optimizerInput;
//    stage3.treatObjective = true;
//  }
  
//  input.stages.push_back(stage3);
  
//  input.integratorInput.type = UserInput::IntegratorInput::NIXE;
//  input.integratorInput.order = UserInput::IntegratorInput::FIRST_FORWARD;
  
//  input.optimizerInput.type = UserInput::OptimizerInput::SENSITIVITY_INTEGRATION;
  
//  //the problem setup should provide sensitivities of 1 for the endpoint of ttime for the duration parameters
//  //of stage 1 and 2
//  UserOutput::Output output = runDyos(input);
//  BOOST_REQUIRE(output.stages.size() == 3);
//  BOOST_REQUIRE(!output.stages[2].integrator.durations.empty());
//  BOOST_REQUIRE(!output.stages[2].integrator.durations[0].grids.empty());
//  UserOutput::ParameterGridOutput grid = output.stages[2].integrator.durations[0].grids.front();
//  BOOST_REQUIRE(!grid.firstSensitivities.empty());
  
//  BOOST_REQUIRE(!grid.firstSensitivities.back().mapConstrRows.empty());
//  int timeIndex = grid.firstSensitivities.back().mapConstrRows[ttime];
//  int numVals = grid.firstSensitivities.back().values[timeIndex].size();
//  for(int i=0; i<numVals; i++){
//    BOOST_CHECK_CLOSE(grid.firstSensitivities.back().values[timeIndex][i], 1.0, 1e-6);
//  }
//}

BOOST_AUTO_TEST_SUITE_END()
