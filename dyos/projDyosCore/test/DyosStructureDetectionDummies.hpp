/**
* @file DyosStructureDetectionDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file provides access to protected methods and variables of      \n
* DyosStructureDetection.hpp                                           \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 26.02.2013
*/

#pragma once

#include "DyosStructureDetection.hpp"

/** @class StrucDetectDynOptDummies
  * @brief provides access to protected methods and variables of the class StrucDetectDynOpt
  */
class StrucDetectDynOptDummies : public DyosStructureDetection
{
public:
  //constructor
  StrucDetectDynOptDummies(){};

};