/**
* @file MADOoader.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic FMILoader - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the FMILoader module	                             \n
* =====================================================================\n
* @author Adrian Caspari
* @date 28.11.2017
*/

#pragma once

// Path to fmi.fmu for FmiLoaderTest
#cmakedefine PATH_MADO_LARGE_SYSTEM_TEST_MADO "@PATH_MADO_LARGE_SYSTEM_TEST_MADO@"

