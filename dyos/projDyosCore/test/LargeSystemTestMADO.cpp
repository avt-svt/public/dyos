/**
* @file DyosTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos testsuite                                                       \n
* =====================================================================\n
* This file contains dyos integration tests                            \n
* =====================================================================\n
* @author Fady Assassa
* @date 01.02.2013
*/

#include <iostream>

#include "Dyos.hpp"
#include "DyosWithLogger.hpp"
#include "ConvertToXmlJson.hpp"

#include "MADOLargeSystemTestConfig.hpp"

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE Dyos
#include "boost/test/unit_test.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>
using namespace UserInput;
BOOST_AUTO_TEST_SUITE(LargeSystemTests)


BOOST_AUTO_TEST_CASE(SecondOrderStoppingCriterion)
{
  unsigned numStages = 1;
  Input input;
  StageInput stageInput;

  //set eso input
  {
    EsoInput esoInput;
    esoInput.type = EsoType::MADO;
    esoInput.model = PATH_MADO_LARGE_SYSTEM_TEST_MADO;
    stageInput.eso = esoInput;
  }
  //set integrator stage input
  {
    IntegratorStageInput integratorStageInput;
    //set stage final time
    {
      ParameterInput stageDuration;
      stageDuration.lowerBound = 1000.0 / double(numStages);
      stageDuration.upperBound = 1000.0 / double(numStages);
      stageDuration.value = 1000.0 / double(numStages);
      stageDuration.sensType = UserInput::ParameterInput::NO;
      stageDuration.paramType = UserInput::ParameterInput::DURATION;
      integratorStageInput.duration = stageDuration;
    }

    //set stage parameter vector
    {
      std::vector<ParameterInput> parameters(2);
      parameters[0].name = "FbinCur";
      parameters[0].lowerBound = 0.0;
      parameters[0].upperBound = 5.784;
      parameters[0].paramType = ParameterInput::PROFILE;
      parameters[0].sensType = UserInput::ParameterInput::FULL;

      parameters[1].name = "TwCur";
      parameters[1].lowerBound = 0.020;
      parameters[1].upperBound = 0.1;
      parameters[1].paramType = ParameterInput::PROFILE;
      parameters[1].sensType = UserInput::ParameterInput::FULL;
      //set grid of first and second parameter
      {
        std::vector<ParameterGridInput> grids(1);
        const int numIntervals = 8;
        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.resize(numIntervals+1 , 5.784);
        grids[0].adapt.maxAdaptSteps = 8;
        grids[0].adapt.adaptWave.minRefinementLevel = 1;
        grids[0].adapt.adaptWave.horRefinementDepth = 0;
        grids[0].pcresolution = 2;
        parameters[0].grids = grids;

        grids[0].numIntervals = numIntervals;
        grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
        grids[0].values.clear();
        grids[0].values.resize(grids[0].numIntervals+1, 100e-3);
        grids[0].adapt.maxAdaptSteps = 8;
        parameters[1].grids = grids;
      }
      integratorStageInput.parameters = parameters;
    }

    stageInput.integrator = integratorStageInput;
  }

  //set optimizer stage input
  {
    OptimizerStageInput optimizerStageInput;
    //set objective
    {
      ConstraintInput objective;
      objective.name = "x9";
      optimizerStageInput.objective = objective;
    }

    //set constraints
    {
      std::vector<ConstraintInput> constraints(2);
      constraints[0].name = "x7";
      constraints[0].lowerBound = 60;
      constraints[0].upperBound = 90;
      constraints[0].type = ConstraintInput::PATH;

      constraints[1].name = "x8";
      constraints[1].lowerBound = 0.0;
      constraints[1].upperBound = 5.0;
      constraints[1].type = ConstraintInput::ENDPOINT;
      optimizerStageInput.constraints = constraints;
    }
    stageInput.optimizer = optimizerStageInput;
  }
  stageInput.optimizer.structureDetection.maxStructureSteps = 8;

  for(unsigned i=0; i<numStages; i++){
    input.stages.push_back(stageInput);
    input.stages[i].treatObjective = false;
    if(i<numStages-1)
      input.stages[i].mapping.fullStateMapping = true;
  }
  input.stages.back().treatObjective = true;
  input.optimizerInput.adaptationOptions.adaptationThreshold = 1e-4;
  input.runningMode = Input::SINGLE_SHOOTING; //SINGLE_SHOOTING;
  input.integratorInput.type = IntegratorInput::NIXE;
  input.integratorInput.daeInit.linSolver.type = LinearSolverInput::KLU;
  input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
  input.optimizerInput.type = OptimizerInput::IPOPT;
  input.optimizerInput.adaptationOptions.adaptStrategy = AdaptationOptions::ADAPTATION;    //ADAPTATION;
  input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-6";
  input.integratorInput.integratorOptions["relative tolerance"] = "1.e-6";
  input.integratorInput.integratorOptions["forward sensitivity method"] = "staggered"; // staggered , simultaneous
  input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-5";
  input.optimizerInput.optimizerOptions["linesearch tolerance"] = "0.99";
  input.optimizerInput.optimizerOptions["Major Iterations Limit"] = "250";
  input.optimizerInput.optimizerOptions["Minor Iterations Limit"] = "500";
  input.optimizerInput.optimizerOptions["function precision"] = "1.e-6";
  input.optimizerInput.optimizerOptions["elastic weight"] = "1.e+6";
  input.optimizerInput.optimizerOptions["warm start"];

  UserOutput::Output output;
  output = runDyos(input);
  for(unsigned i=0; i<output.solutionHistory.size(); i++){
    std::stringstream number;
    number << "output_" << i << ".dat";
    std::string filename = number.str();
    convertUserOutputToXmlJson(filename, output.solutionHistory[i], JSON);
  }
}

BOOST_AUTO_TEST_SUITE_END()
