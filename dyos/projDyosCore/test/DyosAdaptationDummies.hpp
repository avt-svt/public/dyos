/**
* @file DyosAdaptationDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file provides access to protected methods and variables of      \n
* DyosAdaptation.hpp.                                                  \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 26.02.2013
*/

#pragma once

#include "DyosAdaptation.hpp"

/** @class AdaptiveDynOptimizationDummies
 *  @brief provides access to protected methods and variables of the class AdaptiveDynOptimization
 */
class AdaptiveDynOptimizationDummies : public DyosGridAdaptation
{
protected:
  std::vector<FactoryInput::ParameterInput> prepareControlStage1(void);

public:
  typedef boost::shared_ptr<AdaptiveDynOptimizationDummies> Ptr;
  //! @brief constructor
  AdaptiveDynOptimizationDummies();
  void adaptControlGrid()
  {
    DyosGridAdaptation::adaptControlGrid();
  };
  void adaptControlGridStage(FactoryInput::IntegratorMetaDataInput &iGrid,
                             std::vector<std::vector<GridRefinementInterface::Ptr> > &refInterface)
  {
    m_refInterface.push_back(refInterface);
    DyosGridAdaptation::adaptControlGridStage(iGrid, 0);
    m_refInterface.pop_back();
  };
  virtual bool stoppingCriterionAdaptation()
  {
    return DyosGridAdaptation::determineObjectiveError();
  };
  virtual void controlGridAdaptationLoop(DyosOutput::Output &out)
  {
    DyosGridAdaptation::controlGridAdaptationLoop(out);
  };
  virtual void printStatus()const{DyosGridAdaptation::printStatus();};
  virtual void run(DyosOutput::Output &out){DyosGridAdaptation::run(out);};
  void setSolutionHistory(std::vector<DyosOutput::Output> &history){m_solutionHistory = history;};
  void setRefStep(const unsigned step){m_curRefStep = step;};
};