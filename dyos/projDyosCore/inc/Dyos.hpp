/**
* @file Dyos.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the user interface for DyOS. The user provides the\n
* input struct UserInput that contains all data needed to run DyOS.    \n
* The user also provides an output struct that is filled with data by  \n
* DyOS and can be evaluated by the user after the call.                \n
* =====================================================================\n
* @author Tjalf Hoffmann, Adrian Caspari, Johannes Faust
* @date 22.03.2018
*/

#pragma once
#include "UserInput.hpp"
#include "UserOutput.hpp"
#include "LinkDll.hpp"

LINKDLL UserOutput::Output runDyos(const struct UserInput::Input &input);

