/**
* @file DyosFormatter.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the declaration of all DyosFormatter classes.     \n
* These classes manage the formatting of all structs used on to Dyos   \n
* level such as UserInput and UserOutput.                              \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 30.11.2012
*/

#pragma once
#include "UserInput.hpp"
#include "UserOutput.hpp"
#include "OutputFormatter.hpp"
#include <string>
#include <map>

class DyosFormatter : public OutputFormatter
{
protected:
  DyosFormatter();
  unsigned m_indentationLength;
public:
  DyosFormatter(const unsigned indentationLength);
};

template<class Key, class Value>
class MapFormatter : public DyosFormatter
{
protected:
  MapFormatter(){};
  std::map<Key,Value> m_map;
public:
  MapFormatter(const std::map<Key,Value> &mapIn, const unsigned indentationLength = 0)
    : DyosFormatter(indentationLength)
  {
    m_map = mapIn;
  }

  virtual void putDataIntoStream()
  {
    const std::string indentation(m_indentationLength, ' ');
    typename std::map<Key,Value>::iterator iter;
    for(iter = m_map.begin(); iter != m_map.end(); iter++){
      m_sstream<<indentation<<iter->first<<"->"<<iter->second<<std::endl;
    }
  }
};


class DoubleVectorMatrixFormatter : public DyosFormatter
{
protected:
  DoubleVectorMatrixFormatter();
  std::vector<std::vector<double> > m_vecMatrix;
public:
  DoubleVectorMatrixFormatter(const std::vector<std::vector<double> > &vecMatrix,
                              const unsigned indentationLength = 0);

  virtual void putDataIntoStream();
};


class EsoOutputFormatter : public DyosFormatter
{
protected:
  EsoOutputFormatter();
  virtual void printEsoType();
  UserOutput::EsoOutput m_esoOut;
public:
  EsoOutputFormatter(const UserOutput::EsoOutput esoOut, const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};


class MappingOutputFormatter : public DyosFormatter
{
protected:
  MappingOutputFormatter();
  virtual void printStateNameMapping();
  UserOutput::MappingOutput m_mappingOut;
public:
  MappingOutputFormatter(const UserOutput::MappingOutput mappingOut, const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class StateGridOutputFormatter : public DyosFormatter
{
protected:
  StateGridOutputFormatter();
  UserOutput::StateGridOutput m_stateGridOut;
public:
  StateGridOutputFormatter(const UserOutput::StateGridOutput stateGridOut,
                           const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class StateOutputFormatter : public DyosFormatter
{
protected:
  StateOutputFormatter();
  UserOutput::StateOutput m_stateOut;
public:
  StateOutputFormatter(const UserOutput::StateOutput stateOut,
                       const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class FirstSensitivityOutputFormatter : public DyosFormatter
{
protected:
  FirstSensitivityOutputFormatter();
  UserOutput::FirstSensitivityOutput m_firstSensOut;
public:
  FirstSensitivityOutputFormatter(const UserOutput::FirstSensitivityOutput firstSensOut,
                                  const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class ParameterGridOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::ParameterGridOutput m_paramGridOut;
  ParameterGridOutputFormatter();
  virtual void printApproximationType();
public:
  ParameterGridOutputFormatter(const UserOutput::ParameterGridOutput paramGridOut,
                               const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class ParameterOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::ParameterOutput m_paramOut;
  ParameterOutputFormatter();
  virtual void printParameterType();
  virtual void printParameterSensitivityType();
public:
  ParameterOutputFormatter(const UserOutput::ParameterOutput paramOut,
                           const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class IntegratorStageOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::IntegratorStageOutput m_intStageOut;
  IntegratorStageOutputFormatter();
public:
  IntegratorStageOutputFormatter(const UserOutput::IntegratorStageOutput intStageOut,
                                 const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class ConstraintGridOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::ConstraintGridOutput m_constraintGridOut;
  ConstraintGridOutputFormatter();
public:
  ConstraintGridOutputFormatter(const UserOutput::ConstraintGridOutput constraintGridOut,
                                const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class ConstraintOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::ConstraintOutput m_constraintOut;
  ConstraintOutputFormatter();
  virtual void printConstraintType();
public:
  ConstraintOutputFormatter(const UserOutput::ConstraintOutput constraintOut,
                            const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class OptimizerStageOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::OptimizerStageOutput m_optStageOut;
  OptimizerStageOutputFormatter();
public:
  OptimizerStageOutputFormatter(const UserOutput::OptimizerStageOutput optStageOut,
                                const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class StageOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::StageOutput m_stageOut;
  StageOutputFormatter();
public:
  StageOutputFormatter(const UserOutput::StageOutput stageOut,
                       const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class IntegratorOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::IntegratorOutput m_intOut;
  IntegratorOutputFormatter();
  virtual void printIntegratorType();
  virtual void printIntegrationOrder();
public:
  IntegratorOutputFormatter(const UserOutput::IntegratorOutput intOut,
                            const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class SecondOrderOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::SecondOrderOutput m_secOrdOut;
  SecondOrderOutputFormatter();
public:
  SecondOrderOutputFormatter(const UserOutput::SecondOrderOutput secOrdOut,
                             unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class OptimizerOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::OptimizerOutput m_optOut;
  OptimizerOutputFormatter();
  virtual void printOptimizerType();
  virtual void printResultFlag();
public:
  OptimizerOutputFormatter(const UserOutput::OptimizerOutput optOut,
                           const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};

class UserOutputFormatter : public DyosFormatter
{
protected:
  UserOutput::Output m_out;
  UserOutputFormatter();
  virtual void printRunningMode();
public:
  UserOutputFormatter(const UserOutput::Output out,
                      const unsigned indentationLength = 0);
  virtual void putDataIntoStream();
};


class StateNamesFormatter : public OutputFormatter
{
protected:
  UserOutput::Output m_out;
  StateNamesFormatter();
public:
  StateNamesFormatter(const UserOutput::Output out);
  virtual void putDataIntoStream();
};

class FinalStatesFormatter : public OutputFormatter
{
protected:
  UserOutput::Output m_out;
  FinalStatesFormatter();
public:
  FinalStatesFormatter(const UserOutput::Output out);
  virtual void putDataIntoStream();
};

class JsonFormatter : public OutputFormatter
{
protected:
  UserOutput::Output m_out;
  JsonFormatter();
public:
  JsonFormatter(const UserOutput::Output out);
  virtual void putDataIntoStream();
};

class XmlFormatter : public OutputFormatter
{
protected:
  UserOutput::Output m_out;
  XmlFormatter();
public:
  XmlFormatter(const UserOutput::Output out);
  virtual void putDataIntoStream();
public:
};

class JsonInputFormatter : public OutputFormatter
{
protected:
  UserInput::Input m_in;
  JsonInputFormatter();
public:
  JsonInputFormatter(const UserInput::Input in);
  virtual void putDataIntoStream();
};

class XmlInputFormatter : public OutputFormatter
{
protected:
  UserInput::Input m_in;
  XmlInputFormatter();
public:
  XmlInputFormatter(const UserInput::Input in);
  virtual void putDataIntoStream();
};

class OptimizationOverviewFormatter : public OutputFormatter
{
protected:
  UserOutput::Output m_out;
  OptimizationOverviewFormatter();
  std::string convertOptInformFlag(const UserOutput::OptimizerOutput::ResultFlag flag);
  unsigned getNumDecVars(const UserOutput::Output &out);
  unsigned getNumDecVars(const std::vector<UserOutput::ParameterOutput> &params);
  double getObjValue(const UserOutput::Output &out);
public:
  OptimizationOverviewFormatter(const UserOutput::Output out);
  virtual void putDataIntoStream();
};