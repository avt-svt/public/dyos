/**
* @file UserInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the input structs and enums that receives the     \n
* problem definition provided by the user. The user information is then\n
* converted into the different module inputs by the dyos input module. \n
* =====================================================================\n
* @author Tjalf Hoffmann, Adrian Caspari, Aron Zingler
* @date 14.09.2019
* @sa Input.hpp EsoInput.hpp MetaDataInput.hpp IntegratorInput.hpp OptimizerInput.hpp
*/




#pragma once

#include "HaveFmuConfig.hpp"
#include "HaveJade2Config.hpp"
#include "HaveMADOConfig.hpp"

#include <string>
#include <vector>
#include <map>
#include <cfloat>
#include <GenericEso.hpp>
#include <functional>
#include "had.h"

#ifndef DYOS_DBL_MAX
#define DYOS_DBL_MAX 1e300
#endif

#ifdef WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT 
#endif

/**
* @namespace UserInput
*
* The Input struct of the GenericEso module has the same structure as
* the input struct of this module. The GenericEso module is an independent
* external module and should have no dependencies with Dyos, so its Input struct
* must remain in the module. On the other hand this header should be independent
* from any other module headers in Dyos to keep the number of headers needed by
* the user as small as possible. To solve any naming conflicts in the input module
* this namespace is introduced.
*/
namespace UserInput
{
//Using DYOS_DBL_MAX instead of DBL_MAX avoids errors due to rounding

  /**
  * @struct EsoInput
  * @brief information provided by user to create a GenericEso object
  */
  struct EsoInput
  {

    std::string model; ///< name of the model to be loaded
    std::string initialModel; ///< name of the model containing the initial equations (for JADE only)
    EsoType type=EsoType::JADE; ///< type of the GenericEso to be created (default: JADE)

#ifdef BUILD_WITH_FMU
	double relativeFmuTolerance=0;
#endif

    /// @brief standard constructor presetting type
    DLLEXPORT bool operator==(const EsoInput &other)const;
  };

    /** @struct WaveletAdaptationInput
   *  @brief contains the information required for wavelet adaptation
   */
  struct WaveletAdaptationInput
  {
    //! maxRefinementLevel maximum level of refinement. Usually 10
    int maxRefinementLevel;
    //! minRefinementLevel minimum level of refinement. Usually 3
    int minRefinementLevel;
    //! horRefinementDepth horizontal refinement depth. Depth of wavelets considered for refinement in horizontal direction.
    int horRefinementDepth;
    //! verRefinementDepth vertical refinement depth. Depth of wavelets considered for refinement in vertical direction.
    int verRefinementDepth;
    //! etres constant that marks wavelets for elimination.
    double etres;
    //! epsilon constant that marks wavelets for insertion.
    double epsilon;
    // @brief constructor with standard values */
    WaveletAdaptationInput(): maxRefinementLevel(10),
                              minRefinementLevel(3),
                              horRefinementDepth(1),
                              verRefinementDepth(1),
                              etres(1.0e-8),
                              epsilon(0.9){}
    DLLEXPORT bool operator==(const WaveletAdaptationInput &other)const;
  };

  /** @struct SWAdaptationInput
   *  @brief contains the information required for the switching fucntion adaptation
   */
  struct SWAdaptationInput
  {
    //! maxRefinementLevel maximum level of refinement. Usually 10
    int maxRefinementLevel;
    //! includeTol if SW is larger than the tolerance, a grid point is included in the control grid
    double includeTol;

    // @brief constructor with standard values */
    SWAdaptationInput() : maxRefinementLevel(10), includeTol(1e-3){}

    DLLEXPORT bool operator==(const SWAdaptationInput &other)const;
  };
  /** @struct AdaptationInput
   *  @brief contains the information required for wavelet adaptation
   */
  struct AdaptationInput
  {
    enum AdaptationType{
      WAVELET,/// wavelet based adpatation
      SWITCHING_FUNCTION /// switching function based adaptation
    };
    //! adaptType specifies the adaptation type
    AdaptationType adaptType;
    //! adaptWave contains the information required for wavelet adaptation
    WaveletAdaptationInput adaptWave;
    //!swAdapt contains the information required for swithcing function based adaptation
    SWAdaptationInput swAdapt;
    //!maxAdaptSteps maximum number of adaptation steps
    unsigned maxAdaptSteps;

    AdaptationInput():adaptType(WAVELET),
                      maxAdaptSteps(1){}
    DLLEXPORT bool operator==(const AdaptationInput &other)const;
  };

  /** @struct StructureDetectionInput
   *  @brief contains the information required for structure detection. Structure
   *  detection takes place within one stage. Not over stage boundaries, thus it is located
   *  in the stage input
   */
  struct StructureDetectionInput
  {
    unsigned maxStructureSteps;
    bool createContinuousGrids;
    StructureDetectionInput() : maxStructureSteps(0), createContinuousGrids(false){}
    DLLEXPORT bool operator==(const StructureDetectionInput &other)const;
  };


  /**
  * @struct ParameterGridInput
  * @brief information provided by user concerning the grid of a parameter
  */
  struct ParameterGridInput
  {
    /// @brief approximation type
    enum ApproximationType
    {
      PIECEWISE_CONSTANT, /// grid is piecewise constant
      PIECEWISE_LINEAR /// grid is piecewise linear
    };

    unsigned numIntervals; ///< number of intervals for the grid
    int pcresolution; ///< resolution of path constraint with respect to the control grid
    std::vector<double> timePoints; ///< explicit grid (equidistant if not set)
    std::vector<double> values; ///< initial values of the parameters
    std::vector<double> lowerBounds; ///< lower bounds defined on the grid, override the global lower bound on the control
    std::vector<double> upperBounds;///< upper bounds defined on the grid

    /// duration of the parameterGrid (does not need to be set for the last grid of the parameter)
    double duration;
    bool hasFreeDuration; ///< flag defining whether duration is fixed or not
    ApproximationType type; ///< approximation type of the grid (default: PIECEWISE_CONSTANT)
    AdaptationInput adapt; /// < contains all information for adaptation
    /// standard constructor presetting type
    ParameterGridInput()
    {
      numIntervals = 0;
      pcresolution = 1;
      duration = 0.0;
      hasFreeDuration = false;
      type = PIECEWISE_CONSTANT;
    }
    DLLEXPORT bool operator==(const ParameterGridInput &other)const;
  };

  /**
  * @struct ParameterInput
  * @brief information provided by user concerning a parameter
  */
  struct ParameterInput
  {
    /// @brief type of the parameter
    enum ParameterType
    {
      INITIAL, ///< defines an initial value parameter
      TIME_INVARIANT, ///< define a time invariant control
      PROFILE, ///< define a control parameter with adapted grid
      DURATION ///< define a final time parameter(set only for duration in IntegratorStageInput
    };

    /// @brief type of the parameter sensitivity information
    enum ParameterSensitivityType
    {
      /// full sensitivity information collected (sensitivities and decision variable, full Hessian)
      FULL,
      /// only partly sensitivity information collected (only mixed Lagrange derivatives, constraint derivatives)
      FRACTIONAL,
      /// no sensitivity information calculated
      NO
    };

    std::string name; ///< variable name of the parameter
    double lowerBound; ///< lower bound of the parameter
    double upperBound; ///< upper bound of the parameter
    double value; ///< starting value of the parameter (for duration, time_invariant, initial)
    std::vector<ParameterGridInput> grids; ///< parameterization grid of the parameter (profile only)
    ParameterType paramType;///< type of the parameter (default: PROFILE)
    ParameterSensitivityType sensType; ///< type of the sensitivity information (default: FULL)
    /// @brief standard constructor presetting paramType and sensType
    ParameterInput()
    {
      name = "";
      lowerBound = -DYOS_DBL_MAX;
      upperBound = DYOS_DBL_MAX;
      value = 0.0;
      paramType = PROFILE;
      sensType = FULL;
    }
    DLLEXPORT bool operator==(const ParameterInput &other)const;
  };

  /**
  * @struct IntegratorStageInput
  * @brief information provided by user concerning an integration of a single stage
  *
  * IntegratorStageInput contains all kinds of information needed for a simulation.
  */
  struct IntegratorStageInput
  {
    ParameterInput duration; ///< duration information (will be converted to a duration parameter)
    std::vector<ParameterInput> parameters; ///< initial value and control parameters
    unsigned plotGridResolution; ///<number intervals on the plotgrid (additional state grid points)
    std::vector<double> explicitPlotGrid;
    IntegratorStageInput()
    {
      plotGridResolution = 1;
    }
    DLLEXPORT bool operator==(const IntegratorStageInput &other)const;
  };

  /**
  * @struct ConstraintInput
  * @brief information provided by user concerning constraints
  */
  struct ConstraintInput
  {
    /// @brief type of the constraint
    enum ConstraintType
    {
      PATH, ///< define a path constraint
      POINT, ///< define a point constraint
      ENDPOINT, ///< define an endpoint constraint
      /**
      * define a multiple shooting constraint
      * these constraints are usually added automatically - use only for warm starts
      * (ie if you want to preset Lagrange Multipliers)
      */
      MULTIPLE_SHOOTING
    };

    std::string name; ///< variable name of the constraint
    double lowerBound; ///< lower bound of the constraint
    double upperBound; ///< upper bound of the constraint
    double timePoint; ///< time point of the constraint (for point constraints only)
    double lagrangeMultiplier; ///< lagrange multiplier of the constraints
    std::vector<double> lagrangeMultiplierVector; ///<lagrangeMultiplier vector (only PATH)
    ConstraintType type; ///< type of the constraint (default: POINT)
    /**
    * path constraint usually include a constraint at point 0.0,
    * if that point is to be excluded, set this flag to true
    */
    bool excludeZero;
    ///@brief standard constructor presetting all attributes
    ConstraintInput()
    {
      name = "";
      lowerBound = -DYOS_DBL_MAX;
      upperBound = DYOS_DBL_MAX;
      timePoint = 0.0;
      lagrangeMultiplier = 0.0;
      type = POINT;
      excludeZero = false;
    }
    DLLEXPORT bool operator==(const ConstraintInput &other)const;
};

//define ADdouble for automatic differentiation for user faced custom objective
//the alias makes it possible to later switch the AD framework without users noticing.
using ADdouble =had::AReal;

/**
* @struct CustomObjectiveInput
* @brief Information for a user configureable penalty to the objective so the new objective is a*objective+b*penalty
*
* penalty can only be a function of the control parameterization variables.
* This feature does not work with adaptation on the same grid as requested parameters or with structure detection on the same stage.
* std::function can not be saved or loaded from files (JSON/XML etc.)
* If no penalty has been set, but parameters are requested, this will lead to an error.
* Second order optimization is not availible together with the custom objective, due to the fact that only the hessian of the Lagrangian
* is computed from the integrator.
*/
struct CustomObjectiveInput
{
    //this struct has no effect if penalty is left empty/not set
    double originalObjectiveFactor=1;
    double penaltyFactor          =0.0;

    std::function<ADdouble(std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<ADdouble>>)>penaltyFunction;
    //if no penalty has been set, this should be empty. This is checked.
    std::vector<std::tuple<std::string /*varName*/, unsigned /*gridNum*/,unsigned /*totalNumberOfParametersInGrid*/>> requestedListOfParameterizationGrids;
  };

  /**
  * @struct OptimizerStageInput
  * @brief information provided by user concerning the optimization of a single stage
  *
  * Some information are already stored in the IntegratorStageInput
  * (such as optimization parameter)
  * Here information is stored that is needed for optimization only
  */
  struct OptimizerStageInput
  {
    ConstraintInput objective; ///< objective information (interpreted as a constraint)
    std::vector<ConstraintInput> constraints; ///< all constraints(path, point and endpoint)
    StructureDetectionInput structureDetection;

    CustomObjectiveInput customObjectiveInformation;

    DLLEXPORT bool operator==(const OptimizerStageInput &other)const;
  };

  /**
  * @struct StageMapping
  * @brief information provided by user concerning mapping in a multi stage problem
  */
  struct StageMapping
  {
    /**
    * @brief if set to true, a full state mapping will be initialized (default = false0
    * @warning if set, the Esos of the stages must be compatible
    */
    bool fullStateMapping;
    std::map<std::string, std::string> stateNameMapping; ///< mapping of one stage to another
    /// standard constructor predefining fullStateMapping
    StageMapping():fullStateMapping(false){};
    bool operator==(const StageMapping &other)const;
  };

    /**
  * @struct MappingInput
  * @brief
  */
  struct MappingInput
  {
    bool fullStateMapping;
    std::map<std::string, std::string> stateNameMapping; ///< mapping of one stage to another
    /// standard constructor predefining fullStateMapping
    MappingInput():fullStateMapping(false){};
  };

  /**
  * @struct StageInput
  * @brief information given by user defining a stage of a multi stage problem
  */
  struct StageInput
  {
    bool treatObjective; ///< flag whether the objective (last stage is expected to be true)
    StageMapping mapping;
    EsoInput eso; ///< eso information
	GenericEso::Ptr esoPtr;
    IntegratorStageInput integrator; ///< integrator information
    OptimizerStageInput optimizer; ///< optimizer information
    StageInput()
    {
      treatObjective = true;
    }
    DLLEXPORT bool operator==(const StageInput &other)const;
  };

  struct LinearSolverInput
  {
    enum SolverType
    {
      MA28,
	  KLU
    };

    SolverType type;
    LinearSolverInput():type(KLU){}
    bool operator==(const LinearSolverInput &other)const;
  };

  struct NonLinearSolverInput
  {
    enum SolverType
    {
      NLEQ1S,
	  CMINPACK
    };

    SolverType type;
    double tolerance;
    NonLinearSolverInput():type(CMINPACK), tolerance(1e-10){}
    bool operator==(const NonLinearSolverInput &other)const;
  };

  struct DaeInitializationInput
  {
    enum DaeInitializationType
    {
      NO,
      FULL,
      BLOCK,
	  #ifdef BUILD_WITH_FMU
	  FMI
	  #endif
    };

    DaeInitializationType type;
    LinearSolverInput linSolver;
    NonLinearSolverInput nonLinSolver;
    double maximumErrorTolerance;
    DaeInitializationInput():type(BLOCK), maximumErrorTolerance(1e-10){}
    bool operator==(const DaeInitializationInput &other)const;
  };

  /**
  * @struct IntegratorInput
  * @brief information given by user concerning the integrator module
  */
  struct IntegratorInput
  {
    ///@brief type of the integrator
    enum IntegratorType
    {
      NIXE, /// select integrator NIXE
      LIMEX, /// select integrator Limex
      IDAS
    };
    ///@brief integration order of the problem (so far needed only by NIXE)
    enum IntegrationOrder
    {
      ZEROTH, ///zeroth order (no sensitivities)
      FIRST_FORWARD, /// forward integration including sensitivities
      FIRST_REVERSE, /// reverse integration with first order derivatives only
      SECOND_REVERSE /// reverse integration including second order derivatives
    };

    ///options of the integrator (like relative tolerance, or abslolute tolerance)
    std::map<std::string, std::string> integratorOptions;
    IntegratorType type; ///< type of the integrator (default: NIXE)
    IntegrationOrder order;///< integration order (NIXE only, default: ZEROTH)
    DaeInitializationInput daeInit;
    ///@brief standard constructor presetting type and order
    IntegratorInput():type(NIXE), order(ZEROTH){}
    bool operator==(const IntegratorInput &other)const;
  };
  /** 
  * @struct 
  * @brief settings for tolerance of adaptation such as tolerances and which strategy is used
  */
  struct AdaptationOptions
  {
    enum AdaptiveStrategy
    {
      NOADAPTATION,
      ADAPTATION,
      STRUCTURE_DETECTION,
      ADAPT_STRUCTURE
    };
    /// adaptationThreshold threshold that checks the change in the objective as a stopping criterion
    double adaptationThreshold; 
    /// intermConstraintViolationTolerance adaptation stops when the intermediate constraint 
    /// violation is below this tolerance
    double intermConstraintViolationTolerance;
    /// numOfIntermPoints are the number of intermediate points between the path constraint 
    /// discretization which are required to determine the intermediate constraint violation
    unsigned numOfIntermPoints;
    AdaptiveStrategy adaptStrategy;
    AdaptationOptions():adaptationThreshold(1e-2),
                        intermConstraintViolationTolerance(0.1),
                        numOfIntermPoints(4),
                        adaptStrategy(NOADAPTATION)
                        {}
    bool operator==(const AdaptationOptions &other)const;

  };

  /**
  * @struct OptimizerInput
  * @brief information given by user concerning the optimizer module
  */
  struct OptimizerInput
  {
    ///@brief type of the optimizer
    enum OptimizerType{
      SNOPT, ///< select optimizer SNOPT
      IPOPT,
      NPSOL,
      FILTER_SQP,
      SENSITIVITY_INTEGRATION
    };
    
    enum OptimizationMode{
      MINIMIZE,
      MAXIMIZE
    };

    std::map<std::string, std::string> optimizerOptions;
    OptimizerType type; ///< type of the optimizer (default: SNOPT)
    std::vector<ConstraintInput> globalConstraints;
    AdaptationOptions adaptationOptions;
    OptimizationMode optimizationMode;
    ///@brief standard constructor pressetting type
    OptimizerInput():type(SNOPT),
                     optimizationMode(MINIMIZE)
                     {}
    bool operator==(const OptimizerInput &other)const;
  };


  /**
  * @struct
  * @brief information given by user containing a complete dyos problem description
  */
  struct Input
  {
    ///@brief mode in which dyos is run
    enum RunningMode{
      SIMULATION, ///< integrate only
      SINGLE_SHOOTING, ///< do an optimization using single shooting
      MULTIPLE_SHOOTING, ///< do an optimization using multiple shooting
      SENSITIVITY_INTEGRATION ///< integrate with sensitivy calculation (optimizer data needed)
    };

    std::vector<StageInput> stages; ///< inputs of all stages - must not be empty
    /// lower bound of the total endtime (default lower double bound)
    double totalEndTimeLowerBound;
    /// upper bound of the total endtime (default upper double bound)
    double totalEndTimeUpperBound;
    RunningMode runningMode; ///< mode of the current dyos run (default: SINGLE_SHOOTING)
	bool finalIntegration;
    IntegratorInput integratorInput; ///< integrator information
    OptimizerInput optimizerInput; ///< optimizer information
    ///@brief standard constructor presetting runningMode and total endtime bounds
    Input()
    {
      totalEndTimeLowerBound = -DYOS_DBL_MAX;
      totalEndTimeUpperBound = DYOS_DBL_MAX;
      runningMode = SINGLE_SHOOTING;
	  finalIntegration = true;
    }
    bool operator==(const Input &other)const;
  };

}//namespace UserInput
