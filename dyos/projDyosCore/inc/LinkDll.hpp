#ifndef DYOS_LINK_DLL_HPP
#define DYOS_LINK_DLL_HPP

/**
* @file LinkDll.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the LINKDLL macro \n
* =====================================================================\n
* @author Ralf Hannemann-Tamas
* @date 14.11.2019
*/

/**
* @def LINKDLL
* @brief defines whether a function is to be imported or exported
*/
#if WIN32
#ifdef MAKE_DYOS_DLL
#define LINKDLL __declspec(dllexport)
#else
#define LINKDLL // we do not define LINKDLL as __declspec(dllimport) since DyOS is 
                // also build as an archive and for DLLs the dllimport is not required 
                // but only nice-to-have
#endif
#else
#define LINKDLL
#endif

#endif // DYOS_LINK_DLL_HPP