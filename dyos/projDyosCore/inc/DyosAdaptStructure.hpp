/**
* @file DyosAdaptStructure.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the definition classes belonging to the           \n
* combination of structure detection and adpatation                    \n
* =====================================================================\n
* @author Fady Assassa
* @date 18.01.2013
*/

#pragma once
#include "DyosInterface.hpp"

/** @class AdaptStructure
 *  @brief formulates an adaptive dynamic optimization problem.
 *  first we solve an adaptation problem, next we do structure detection with adaptation.
 */
// NOTE: The reuse of IterativeDynOpt::m_curRefStep for both Adapt and Structure Detection seems like a bug.
// Since adaptControls is also called from structure detection this means that during the first step (Adaptation)
// maxAdaptSteps is honored, but during structure detection this is compared to the number of structure steps.

class DyosAdaptStructure : public IterativeDynOpt
{
protected:
  bool m_printStructureSteps;
  DyosAdaptStructure(){};
  bool stoppingCriterionAdaptation();
  bool stoppingCriterionStructureDetection();
  void printStatus()const;
public:
  typedef boost::shared_ptr<DyosAdaptStructure> Ptr; ///! boost shared pointer
  virtual void run(DyosOutput::Output &out);
  DyosAdaptStructure(const GenericOptimizer::Ptr &optimizer,
                     const ProblemInput &problemInput);
};
