/**
* @file DyosWithLogger.hpp
*
 =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* ==================================================================*===\n
* This file contains the user interface for DyOS. The user provides the\n
* input struct UserInput that contains all data needed to run DyOS.    \n
* The user also provides an output struct that is filled with data by  \n
* DyOS and can be evaluated by the user after the call.                \n
* In addition the user has to privide a custom Logger he wishes to use.\n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 30.11.2012
*/

#pragma once
#include "UserInput.hpp"
#include "UserOutput.hpp"
#include "Logger.hpp"
#include "LinkDll.hpp"



LINKDLL UserOutput::Output runDyos(const struct UserInput::Input &input,
                                   const Logger::Ptr &logger);


LINKDLL UserOutput::Output optimizeFirstForwardWithSecondOrderOutput
                                  (const struct UserInput::Input &input,
                                   const Logger::Ptr &logger);
void runCreateProblemInputToCheckForExceptions(const struct UserInput::Input &input,const Logger::Ptr &logger);


