/**
* @file DyosAdaptation.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the definition classes belonging to wavelet based \n
* adaptation.                                                          \n
* =====================================================================\n
* @author Fady Assassa
* @date 03.12.2012
*/

#pragma once
#include "DyosInterface.hpp"

/** @class DyosGridAdaptation
 *  @brief solves an adaptive dynamic optimization problem
 *  This class takes care of the correct stopping function.
 */
class DyosGridAdaptation : public IterativeDynOpt
{
protected:
  DyosGridAdaptation(){};
  bool stoppingCriterionAdaptation();
  bool stoppingCriterionStructureDetection();
  void printStatus()const;
public:
  typedef boost::shared_ptr<DyosGridAdaptation> Ptr; ///! boost shared pointer
  virtual void run(DyosOutput::Output &out);
  DyosGridAdaptation(const GenericOptimizer::Ptr &optimizer,
                     const ProblemInput &problemInput);
};
