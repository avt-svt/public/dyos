/**
* @file DyosInterface.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos                                                                 \n
* =====================================================================\n
* This file contains the interface for dynamic optimization and the    \n
* and the iterative loops for adaptive algorithms such as structure    \n
* detection and grid adaptation. it also contains the interface for    \n
* for regular simulation.                                              \n
* =====================================================================\n
* @author Fady Assassa
* @date 04.10.2012
*/

#pragma once
#include "Input.hpp"
#include "DyosOutput.hpp"
#include "GenericOptimizer.hpp"
#include "GenericIntegrator.hpp"
#include "Dyos.hpp"
#include "DyosObject.hpp"
#include "GridRefinementFactory.hpp"
#include <set>


enum ConstraintActivity{
  ACTIVEUPPERBOUND,
  ACTIVELOWERBOUND,
  NOTACTIVE
};

struct IntervalActivity{
  double entry;
  double exit;
  ConstraintActivity activity;
  bool isSuccessive;
  IntervalActivity(): entry(0.0), exit(0.0), activity(NOTACTIVE), isSuccessive(false){}
};

typedef std::map<unsigned, std::vector<DyosOutput::ConstraintOutput> > PathConstraintMap;
typedef std::map<unsigned, std::vector<IntervalActivity> > ActivityMap;
typedef std::vector<std::vector<ConstraintActivity> > ActivityPattern;
typedef std::map<unsigned, std::vector<ConstraintActivity> > ArcPattern;

/** @class DyOS
*   @brief DyOS abstract class required to create a dynamic optimization problem or just an
*          a simulation.
*/
class DyOS : public DyosObject
{
public:
  typedef boost::shared_ptr<DyOS> Ptr;
  virtual void run(DyosOutput::Output &out) = 0;
};

/** @class Simulation
 *  @brief creates a simulation object using an integrator which is provided by a Factory
*/
class Simulation : public DyOS
{
protected:
  //! @brief empty constructor
  Simulation(){};
public:
  typedef boost::shared_ptr<Simulation> Ptr;
  //! m_integrator object to generic integrator
  GenericIntegrator::Ptr m_integrator;

  Simulation(const GenericIntegrator::Ptr &integrator);
  virtual void run(DyosOutput::Output &out);
};

/** @class DynamicOptimization
*  @brief creates a dynamic optimization object using an optimizer which is provided by a factory
*/
class DynamicOptimization : public DyOS
{
protected:
  //! @brief empty constructor
  DynamicOptimization(){};
  //! m_optimizer object to generic optimizer
  GenericOptimizer::Ptr m_optimizer;
public:
  typedef boost::shared_ptr<DynamicOptimization> Ptr;
  DynamicOptimization(const GenericOptimizer::Ptr &optimizer);
  virtual void run(DyosOutput::Output &out);
};

/** @class IterativeDynOpt
  * @brief virtual interface for iterative dynamic optimization (adaptation + structure detection)
  * The class includes all necessary functions that are required to adapt the control grid and
  * to detect the structure as well as their combination.
  * This is done by splitting both in two steps.  (see Assassa's structure detection paper for details)
  * Step 1: Control grid adaptation loop
  * Step 2: Structure detection loop.
  */
class IterativeDynOpt : public DynamicOptimization
{
protected:
  unsigned m_maxNumSteps; //! maximum number of refinement steps
  unsigned m_curRefStep;  //! current refinement step.
  bool m_calculateAdjoints;
  bool m_rigorousStoppingCriterion;

  bool m_isICVfulfilled;
  std::vector<ArcPattern> m_prevArcStructure;
  std::vector<ArcPattern> m_newArcStructure;
  ProblemInput m_problemInput; //! copy of problemInput required for adaptation
  std::vector<DyosOutput::Output> m_solutionHistory; //! solution history of adaptation
  std::vector<ProblemInput> m_inputHistory; //!history of adaption inputs
  ///! Grid refinement interface. Needs to be created and saved so that we don't include points
  /// that we have deleted once
  std::vector<std::vector<std::vector<GridRefinementInterface::Ptr> > > m_refInterface;
  /// switching function of the entire problem
  std::vector<std::map<unsigned, std::vector<std::map<double, double> > > > m_swFunction;

  std::vector<std::set<double> > m_maxIntermConstrVioGrid;
  IterativeDynOpt(){};

  // the stopping criterion is altered depening on the purpose. When we have only adaptation
  // without structure detection, then we stop after the relative change of the
  // objective function is small enough.
  // For adaptive structure detection: we would stop if the control structure remains unchanged
  // for two successive steps
  virtual bool stoppingCriterionAdaptation()= 0;
  virtual bool stoppingCriterionStructureDetection()=0;
  bool determineObjectiveError()const;
  bool determineAdjointError();
  void interpolateAdjoints(std::vector<std::vector<std::vector<double> > > &oldAdjointsInterp,
                           std::vector<std::vector<std::vector<double> > > &newAdjointsInterp)const;
  bool evaluateIntermediateConstraintViolation();

  void calculateAdjointVariables();
  std::vector<std::map<unsigned, std::vector<std::map<double, double> > > > getDfDu();
  std::vector<DyosOutput::StageOutput> calculateRefinedStateTrajectory()const;
  DyosOutput::StateGridOutput getPathConstraintOutput(const std::vector<DyosOutput::StateOutput> output,
                                                      const unsigned esoIndex)const;
  std::map<unsigned, vector<double> > getPathConstraintBounds(
    const std::vector<FactoryInput::ConstraintInput> &constraints)const;

  void addAdditionalConstraintGrid();
  void solveLinearSystemForAlgebraicAdjoints(const IntegratorMetaData::Ptr &imd,
                                                   utils::Array<double> &adjVals)const;
////////////////////////////////////////////////////////////////////////////////
// Step 1: control grid adaptation loop and required subfunctions //////////////
////////////////////////////////////////////////////////////////////////////////
  void controlGridAdaptationLoop(DyosOutput::Output &out);
  void adaptControlGrid();
  void adaptControlGridStage(FactoryInput::IntegratorMetaDataInput &stageData,
                             const unsigned iStage);

////////////////////////////////////////////////////////////////////////////////
// Step 2: structure detection loop with adaptation and required subfunctions //
////////////////////////////////////////////////////////////////////////////////
  void structureDetectionLoop(DyosOutput::Output &out);
  void resetRefinementInterface();
  std::vector<ArcPattern> identifiyControlStructure(const DyosOutput::Output &out,
                                                    ProblemInput &suggestedInput)const;
  std::vector<ArcPattern> firstControlStructure(void)const;

  // functions required to determine path constraint activity
  PathConstraintMap getPathConstraintStage(const std::vector<DyosOutput::ConstraintOutput> &constraintOut,
                                   const std::vector<FactoryInput::ConstraintInput> &constraintInput)const;
  std::vector<unsigned> getPathConstraintIndices(const std::vector<FactoryInput::ConstraintInput> &input)const;
  ActivityMap getPathConstraintActivity(const PathConstraintMap &pathConstraints)const;
  std::vector<ConstraintActivity> checkConstraintAcitivity(
                                   const vector<DyosOutput::ConstraintOutput> &pointConstraint) const;
  std::vector<IntervalActivity> activePathIntervals(const std::vector<ConstraintActivity> &activity,
                                   const std::vector<DyosOutput::ConstraintOutput> &pConstr) const;

  // functions required to determine control activity
  ActivityMap getControlActivity(const std::vector<DyosOutput::ParameterOutput> &params,
                                 const ActivityMap &pPattern)const;
  ActivityPattern checkControlAcitivity(const vector<DyosOutput::ParameterGridOutput> &paramGrid,
                                        const double &lowerBound,
                                        const double &upperBound) const;
  std::vector<IntervalActivity> activeControlIntervals(const DyosOutput::ParameterOutput &param,
                                                       const ActivityPattern &activity) const;
  ActivityMap splitControlActivity(const ActivityMap &pattern)const;

  std::vector<IntervalActivity> rearrangeIntervals(const std::vector<IntervalActivity> &intervals)const;
  std::vector<IntervalActivity> mergeIntervals(const std::vector<IntervalActivity> &intervals)const;

  bool isArcStructureEqual()const;

  // functions required to reformulate the problem
  ArcPattern getArcStructure(const ActivityMap &controlActivity,
                             const ActivityMap &pathActivity,
                             const std::vector<FactoryInput::ParameterInput> &controls)const;
  std::vector<FactoryInput::ParameterInput> formulateMultiGridProblem(const ActivityMap &cActivity,
                                                                      const ActivityMap &pActivity,
                                                const vector<FactoryInput::ParameterInput> &controls)const;
  void setToBounds(const ConstraintActivity &activity,
                   const double upperBound,
                   const double lowerBound,
                         FactoryInput::ParameterizationGridInput &grids)const;
  void setSensSeeking(const std::vector<double> &interpPoints,
                      const std::vector<double> &interpValues,
                      const double elpasedTime,
                      const double upperBound,
                      const double lowerBound,
                            FactoryInput::ParameterizationGridInput &grids)const;
  std::vector<double> getActiveConstraintTimePoints(const PathConstraintMap &pcMap)const;
////////////////////////////////////////////////////////////////////////////////
// help functions
////////////////////////////////////////////////////////////////////////////////
  void mergeGrids(const  std::vector<FactoryInput::ParameterizationGridInput> &grids,
                         std::vector<double> &gridPoints,
                         std::vector<double> &values)const;
  void interpGrid(const  int interpResolution,
                  const  std::vector<double> &gridPoints,
                  const  std::vector<double> &values,
                         std::vector<double> &interpGridPoints,
                         std::vector<double> &interpValues)const;

  IntervalActivity createTimeActivity(const double in,
                                      const double out,
                                      const ConstraintActivity act,
                                      const bool isSucc)const;
  std::vector<double> rescaleGrid(const std::vector<double> grid,
                                  const double gridDuration,
                                  const double previousGridTimes)const;
  int findControlIndex(const std::vector<FactoryInput::ParameterInput> &controls,
                       const unsigned searchIndex)const;

  bool findEntryOrExit(const vector<IntervalActivity> &tia,
                       const double &value)const;
  unsigned findEntryPosition(const vector<IntervalActivity> &tia,
                             const double &value)const;



  void resetLagrangeMultipliers(ProblemInput &input)const;
  virtual void printStatus()const = 0;
public:
  typedef boost::shared_ptr<IterativeDynOpt> Ptr;

  IterativeDynOpt(const GenericOptimizer::Ptr &optimizer,
                  const ProblemInput &problemInput);

  virtual void run(DyosOutput::Output &out);
  ProblemInput getProblemInput(void)const{ return m_problemInput; };
  std::vector<DyosOutput::Output> getSolutionHistory(void)const{return m_solutionHistory;};
};



