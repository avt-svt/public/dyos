/** 
* @file Parameter.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Single Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* Test for class Parameter                                             \n
*                                                                      \n
* =====================================================================\n
* @author Klaus Stockmann
* @date 21.10.2011
*/

#include "boost/test/tools/floating_point_comparison.hpp"

#include "Array.hpp"
#include "cs.h"


#include "Parameter.hpp"
#include "Constraints.hpp"
#include "Control.hpp"
#include "MetaDataTestDummies.hpp"

//! threshold for double comparison testmacro BOOST_CHECK_CLOSE to pass
#define BOOST_TOL_DYOS 1.0e-10

/////////////////////////////////////////////////////////////////////////////////////
// Test Suite
/////////////////////////////////////////////////////////////////////////////////////
BOOST_AUTO_TEST_SUITE(ParameterTest)

/**
* @test ParameterTest - test member functions of class Parmeter 
*
*/ 
BOOST_AUTO_TEST_CASE(TestMemberFunctions)
{

  CarIntegratorMetaData carIntegratorMetaData;
  const double parameterValue = 1.0;
  const bool sensActivity = true;
  const bool withSensitivity = true;
  const unsigned nSens = carIntegratorMetaData.getGenericEso()->getNumEquations();
  const std::vector<double> initVals(nSens, 0.0);
  std::vector<double> initValsInitialValParam(nSens, 0.0);
  const unsigned esoIndex = 0;

  utils::Array<double> initialValues(nSens);
  utils::Array<double> currentSens(nSens);
  utils::Array<double> rhsSens(nSens);

  // construct Control object
  Control *control = new Control;
  control->setEsoIndex(esoIndex);

  ParameterizationGrid::Ptr grid(new ParameterizationGrid(1, PieceWiseConstant));
  grid->setDurationValue(40.0);
  vector<ParameterizationGrid::Ptr> gridPtr(1);
  gridPtr[0] = grid;
  control->setControlGrid(gridPtr);
  control->initializeForFirstIntegration();

  // ControlParameter
  //-----------------
  vector<Parameter::Ptr> cParam = control->getActiveParameter();
    
  cParam[0]->setParameterValue(parameterValue);
  BOOST_CHECK_EQUAL(parameterValue, cParam[0]->getParameterValue());

  cParam[0]->setParameterSensActivity(sensActivity);
  BOOST_CHECK_EQUAL(sensActivity, cParam[0]->getParameterSensActivity());

  cParam[0]->setWithSensitivity(withSensitivity);
  BOOST_CHECK_EQUAL(withSensitivity, cParam[0]->getWithSensitivity());

  GenericEso::Ptr genEso = carIntegratorMetaData.getGenericEso();
  cParam[0]->getInitialValuesForIntegration(initialValues, genEso);
  BOOST_CHECK_EQUAL_COLLECTIONS(initVals.begin(), initVals.end(), initialValues.getData(), initialValues.getData()+nSens);

  // check method getRhsSensForwared1stOrder
  utils::Array<double> rhsSensRef(nSens);
  rhsSensRef[0] = 40.0;
  rhsSensRef[1] = 0.0;
  rhsSensRef[2] = 200.0;

  for (unsigned i=0; i<nSens; i++) currentSens[i] = 1.0;
  carIntegratorMetaData.setStepStartTime(0.0);
  carIntegratorMetaData.setStepEndTime(1.0);
  carIntegratorMetaData.setStepCurrentTime(0.0);
  cParam[0]->setParameterIndex(0);
  const CsCscMatrix::Ptr jacobianFstates = carIntegratorMetaData.getJacobianFstates();
  cParam[0]->getRhsSensForward1stOrder(currentSens, jacobianFstates->get_matrix_ptr(), &carIntegratorMetaData, rhsSens);
    
  // there exist not BOOST_CHECK_CLOSE_COLLECTIONS, so we have to do the comparison entrywise
  for (unsigned i=0; i<nSens; i++) {
    BOOST_CHECK_CLOSE(rhsSens[i], rhsSensRef[i], BOOST_TOL_DYOS);
  }

  // InitialValueParameter
  //----------------------
  InitialValueParameter *ivParam = new InitialValueParameter;
    
  ivParam->setParameterValue(parameterValue);
  BOOST_CHECK_EQUAL(parameterValue, ivParam->getParameterValue());

  ivParam->setParameterSensActivity(sensActivity);
  BOOST_CHECK_EQUAL(sensActivity, ivParam->getParameterSensActivity());

  ivParam->setWithSensitivity(withSensitivity);
  BOOST_CHECK_EQUAL(withSensitivity, ivParam->getWithSensitivity());
  
  // since an initialValue needs to know its position in the initial values vector
  // for evaluation of the initial values the parameter index must be set beforehand.
  // Normally this is done automatically by IntegratorMetaData class
  ivParam->setParameterIndex(0);
  initValsInitialValParam[0] = 1.0;
  ivParam->setStateIndex(0);
  
  genEso = carIntegratorMetaData.getGenericEso();
  ivParam->getInitialValuesForIntegration(initialValues, genEso);
  BOOST_CHECK_EQUAL_COLLECTIONS(initValsInitialValParam.begin(), initValsInitialValParam.end(), 
                                initialValues.getData(), initialValues.getData()+nSens);

  // check method getRhsSensForwared1stOrder
  rhsSensRef[0] = 40.0;
  rhsSensRef[1] = 0.0;
  rhsSensRef[2] = 80.0;

  for (unsigned i=0; i<nSens; i++) currentSens[i] = 1.0;

  ivParam->getRhsSensForward1stOrder(currentSens, jacobianFstates->get_matrix_ptr(), &carIntegratorMetaData, rhsSens);
  // there exist not BOOST_CHECK_CLOSE_COLLECTIONS, so we have to do the comparison entrywise
  for (unsigned i=0; i<nSens; i++) {
    BOOST_CHECK_CLOSE(rhsSens[i], rhsSensRef[i], BOOST_TOL_DYOS);
  }

  // DurationParameter
  //-------------------
  DurationParameter *ftParam = new DurationParameter;

  ftParam->setParameterValue(parameterValue);
  BOOST_CHECK_EQUAL(parameterValue, ftParam->getParameterValue());

  ftParam->setParameterSensActivity(sensActivity);
  BOOST_CHECK_EQUAL(sensActivity, ftParam->getParameterSensActivity());

  ftParam->setWithSensitivity(withSensitivity);
  BOOST_CHECK_EQUAL(withSensitivity, ftParam->getWithSensitivity());

  genEso = carIntegratorMetaData.getGenericEso();
  ftParam->getInitialValuesForIntegration(initialValues, genEso);
  BOOST_CHECK_EQUAL_COLLECTIONS(initVals.begin(), initVals.end(), initialValues.getData(), initialValues.getData()+nSens);

  // check method getRhsSensForwared1stOrder
  rhsSensRef[0] = 29.0;
  rhsSensRef[1] = 12000.0;
  rhsSensRef[2] = 93.0;

  for (unsigned i=0; i<nSens; i++) currentSens[i] = 1.0;

  ftParam->getRhsSensForward1stOrder(currentSens, jacobianFstates->get_matrix_ptr(), &carIntegratorMetaData, rhsSens);
  // there exist not BOOST_CHECK_CLOSE_COLLECTIONS, so we have to do the comparison entrywise
  for (unsigned i=0; i<nSens; i++) {
    BOOST_CHECK_CLOSE(rhsSens[i], rhsSensRef[i], BOOST_TOL_DYOS);
  }
}

BOOST_AUTO_TEST_SUITE_END()




