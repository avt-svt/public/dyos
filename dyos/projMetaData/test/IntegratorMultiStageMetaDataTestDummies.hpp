/** 
* @file IntegratorMultiStageMetaDataTestDummies.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData - Part of DyOS                                              \n
* =====================================================================\n
* Dummyclasses for IntegratorMultiStageMetaData testsuite              \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 27.03.2012
*/
#pragma once
#include "MetaDataTestDummies.hpp"
#include <memory>

/**
* @class MultiStageDummyGenericEso
* @brief dummy class used in IntegatorMultiStageMetaData testsuite
*
* This class provides some attributes that can be set directly in the code of
* the testcases. Eso functions called by the tested function are implemented
* in this dummy class as well.
*/
class MultiStageDummyGenericEso : public DummyGenericEso
{
public:
  typedef std::shared_ptr<MultiStageDummyGenericEso> Ptr;
  EsoIndex m_numEquations; ///< @brief number of equations in the ESO (public access)
  
  /// @brief standard constructor setting number of equations to 4
  MultiStageDummyGenericEso(){m_numEquations = 4;}
  
  /**
  * @copydoc GenericEso::getNumEquations
  * @return value of m_numEquations
  */
  EsoIndex getNumEquations() const {return m_numEquations;}
  
  /**
  * @copydoc GenericEso::getNumStates
  *
  * Since there should be as many states as equations the number of equations is returned
  * @return value of m_numEquations
  */
  EsoIndex getNumStates() const {return m_numEquations;}
  
  /**
  * @copydoc GenericEso::getStateIndex
  *
  * define first numEqns variables as states 
  */
  void getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex)
  {
    assert(n_states == m_numEquations);
    for(int i=0; i<n_states; i++){
      stateIndex[i] = i;
    }
  }
};


/**
* @class TestDummyMapping
* @brief Dummy class used in IntegratorMultiStageMetaData testsuite and in
*        test class TestIntegratorMultiStageMetaData
*
* The Dummy mapping is used to test the assembling of mapping sensitivities in
* a multi stage problem. To identify the stage the corresponding stage the mapping
* class uses a stage multiplier (here the duration of the stage is used).
*/
class TestDummyMapping : public DummyMapping
{
public:
  typedef std::shared_ptr<TestDummyMapping> Ptr;
  
  /**
  * @copydoc Mapping::applyStageMapping
  */
  virtual void applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                                       IntegratorSingleStageMetaData::Ptr currentStage,
                                       vector<double> &multiStageSens)
  {
    unsigned numMappingSens = multiStageSens.size();
    numMappingSens/= lastStage->getGenericEso()->getNumEquations();
    numMappingSens += lastStage->getNumSensitivityParameters();
    numMappingSens *= currentStage->getGenericEso()->getNumEquations();
    multiStageSens.assign(numMappingSens, 0.0);
  }
  
  /**
  * @copydoc Mapping::calculateMappedSensitivitiesRhs
  *
  * This function simply multiplies the input with the duration of the current stage
  */
  void calculateMappedSensitiviesRhs(const IntegratorSingleStageMetaData::Ptr currentStage,
                                                   utils::Array<double> &inputSensitivities,
                                                   utils::Array<double> &rhsSensitivities)
  {
    const double multiplier = currentStage ->getIntegrationGridDuration();
    assert(inputSensitivities.getSize() == rhsSensitivities.getSize());
    for(unsigned i=0; i<inputSensitivities.getSize(); i++){
      rhsSensitivities[i] = multiplier * inputSensitivities[i];
    }
  };
};

/**
* @class IntegratorSingleStageMetaDataDummyForMultiStageTest
* @brief dumnmy class for IntegratorMulitStageMetaData testsuite
*/
class IntegratorSingleStageMetaDataDummyForMultiStageTest : public IntegratorSingleStageMetaData
{
public:
  typedef std::shared_ptr<IntegratorSingleStageMetaDataDummyForMultiStageTest> Ptr;
  double m_testDuration; ///< final time value (public access)
  unsigned m_testNumSensitivityParameters; ///< number of parameters (public access)
  double m_testMultiplier; ///< identifier of the stage, that is used as multiplier
  double m_testCurrentTime; ///< value of current time (public access)
  unsigned m_testTimeIndex;
  
  /**
  * @brief constructor
  *
  * allocate m_jacobianFstates
  * initialize m_completeGridUnscaled with trivial grid [1.0]
  * initialize global duration with parameter value 1.0
  * @param genEso GenericEso used in this class (usually another dummy)
  */
  IntegratorSingleStageMetaDataDummyForMultiStageTest(const GenericEso::Ptr &genEso)
  {
    m_genericEso = genEso;
    
    const int numEquations = genEso->getNumEquations();
    const int numStates = numEquations;
    const int allocForValues = 1;
    const int inCscFormat = 0;
    m_jacobianFstates = CsCscMatrix::Ptr(new CsCscMatrix( cs_spalloc(numEquations, 
                                                                     numStates, 
                                                                     numStates*numEquations, 
                                                                     allocForValues,
                                                                     inCscFormat),
                                                           true));
    m_completeGridUnscaled.resize(1, 1.0);
    m_currentIndexCompleteGridUnscaled = 0;
    m_globalDurationParameter = DurationParameter::Ptr(new DurationParameter());
    m_globalDurationParameter->setParameterValue(1.0);
  };
  
  /**
  * @brief destructor
  *
  * delete sensitivity pointer added with function addParamWithSens
  */
   ~IntegratorSingleStageMetaDataDummyForMultiStageTest()
  {
  };
  
  /**
  * @copydoc IntegratorMetaData::calculateSensitivitiesForward1stOrder
  *
  * multiplies each input with a test multiplier set for this stage
  */
  virtual void calculateSensitivitiesForward1stOrder(
                                             const utils::Array<double> &inputSensitivities,
                                                   utils::Array<double> &rhsSensitivities)
  {
    assert(inputSensitivities.getSize() == rhsSensitivities.getSize());
    for(unsigned i=0; i<inputSensitivities.getSize(); i++){
      rhsSensitivities[i] = m_testMultiplier * inputSensitivities[i];
    }
  };
  
  /**
  * @copydoc IntegratorMetaData::getNumCurrentSensitivityParameters
  *
  * returns the value of m_testNumSensitivityParameters
  */
  int getNumCurrentSensitivityParameters()const{return m_testNumSensitivityParameters;};
  
  /**
  * @copydoc IntegratorSingleStageMetaData::getNumDecisionVariables
  *
  * returns the value of m_testNumSensitivityParameters
  */
  unsigned getNumDecisionVariables()const{return m_testNumSensitivityParameters;};
  
  unsigned getNumSensitivityParameters()const{return m_testNumSensitivityParameters;};
  
  /**
  * @copydoc IntegratorMetaData::getDuration
  *
  * returns the value of m_testDuration
  */
  double getIntegrationGridDuration() const {return m_testDuration;}
  
  /**
  * @copydoc IntegratorMetaData::getCurrentTime
  *
  * returns the value of m_testCurrentTime
  */
  double getStepCurrentTime() const {return m_testCurrentTime;}
  
  double getStageCurrentTimeScaled() const {return m_testCurrentTime/m_testDuration;}

  unsigned getTimeIndex() const{return m_testTimeIndex;}
  
  /**
  * @copydoc IntegratorMetaData::getJacobianFstates
  *
  * set all Fstates jacobian entries to multiplier/numStates.
  */
  CsCscMatrix::Ptr getJacobianFstates()
  {
    //define full states matrix
    const unsigned numEquations = m_genericEso->getNumEquations();
    const unsigned numStates = numEquations;
    unsigned currentCounter = 0;
    for(unsigned i=0; i<numStates; i++){
      for(unsigned j=0; j<numEquations; j++, currentCounter++){
        m_jacobianFstates->get_matrix_ptr()->i[currentCounter] = j;
        //by setting the entry to this value a multiplication of an (1,...,1) input
        //vector would produce the result (m,...,m) while m is the multiplier
        m_jacobianFstates->get_matrix_ptr()->x[currentCounter] = m_testMultiplier/numStates;
      }
      m_jacobianFstates->get_matrix_ptr()->p[i] = i * numEquations;
    }
    m_jacobianFstates->get_matrix_ptr()->p[numStates] = numStates * numEquations;
    return m_jacobianFstates;
  }
  
  /**
  * @brief add a parameter to the vector m_currentParamsWithSens
  *
  * The parameter data are also added at the endpoint of the intOptData struct
  * @param param Parameter to be added
  */
  void addParamWithSens(Parameter::Ptr param)
  {
    param->setParameterIndex(m_currentParamsWithSens.size());
    m_currentParamsWithSens.push_back(param);
    m_intOptData[0][1].numSensParam++;
    m_intOptData[0][1].activeParameterIndices.push_back(param->getParameterIndex());
  };
  
  /**
  * @brief set a new value for the IntOptDataMap
  *
  * @param map new value of the IntOptDataMap
  */
  void setIntOptDataMap(const IntOptDataMap map)
  {
    m_intOptData.resize(1);
    m_intOptData[0] = map;
  }
  
  /**
  * @copydoc IntegratorMetaData::evaluateInitialValues
  *
  * simply multiply the arrays with the stage identifier stored in m_testMultiplier
  */
  void evaluateInitialValues(utils::Array<double> &initialValues, utils::Array<double> &initialSensitivities)
  {
    for(unsigned i=0; i<initialValues.getSize(); i++){
      initialValues[i] = m_testMultiplier;
    }
    for(unsigned i=0; i<initialSensitivities.getSize(); i++){
      initialSensitivities[i] = m_testMultiplier;
    }
  }
  
  /**
  * @copydoc IntegratorMetaData::setCurrentTime
  *
  * set the attribute m_testCurrentTime to time 
  * and set the single point of the complete grid to that value
  */
  void setStepCurrentTime(const double time)
  {
    m_testCurrentTime = time;
    m_completeGridUnscaled.front() = time;
  }
  
  void setIntegrationGrid(const std::vector<double> &grid)
  {
    m_completeGridUnscaled.assign(grid.begin(), grid.end());
  }
  
  double getStageDuration()const
  {
    return m_testDuration;
  }
};

//! shared pointer for dummy mapping class
//typedef boost::shared_ptr<TestDummyMapping> MappingDummyPtr;

/**
* @class TestIntegratorMultiStageMetaData
* @brief test class for IntegratorMultiStageMetaData testsuite
*/
class TestIntegratorMultiStageMetaData : public IntegratorMultiStageMetaData
{
protected:
  /// @brief standard constructor
  TestIntegratorMultiStageMetaData();
public:
  /**
  * @brief constructor
  *
  * @param mappings mappings between stages
  * @param stages stages of the problem
  */
  TestIntegratorMultiStageMetaData(const std::vector<Mapping::Ptr> mappings, 
                                   const std::vector<IntegratorSingleStageMetaData::Ptr> stages)
  {
    m_mapping = mappings;
    m_stages = stages;
    m_currentStageIndex = 0;
    m_intOptData.resize(stages.size());
  };
  
  ///@brief destructor
  ~TestIntegratorMultiStageMetaData(){};
  
  /**
  * @brief returns the current stage index
  * @return current stage index
  */
  unsigned getCurrentStageIndex()const{return m_currentStageIndex;};
  
  /**
  * @brief set the current stage index
  *
  * if GenericEso is used in this class the mapping sensitivities vector will be resized
  * according to the number of mapping parameters
  * @param newIndex new value for the stage index
  */
  void setCurrentStageIndex(const unsigned newIndex){
    m_currentStageIndex = newIndex;
    if(getGenericEso() != NULL)
      m_mappingSensitivities.resize(getNumMappingParameters() * getGenericEso()->getNumEquations());
  };
  
  /**
  * @brief resizes the attribute m_mappingSensitivities
  * @param newSize size to which the vector is resized
  */
  void resizeMappingSensVector(const unsigned newSize){m_mappingSensitivities.resize(newSize);};
  
  /**
  * @brief assign a given vector to m_mappingSensitivities
  * @param newMappingSens vector m_mappingSensitivities is assigned to
  */
  void setMappingSensVector(const std::vector<double> &newMappingSens)
  {
    m_mappingSensitivities.assign(newMappingSens.begin(), newMappingSens.end());
  }
  
  /**
  * @brief calls addIntOptDataPoint
  *
  * since the function addIntOptDataPoint is protected it can be tested using this function
  */
  void testAddIntOptDataPoint(){addIntOptDataPoint(m_stages[m_currentStageIndex]->getTimeIndex());}
  
  /** 
  * @brief returns the multi stage mapping sensitivities
  * @param[out] curSens the multi stage sensitivities at the current time point
  */
  void getCurrentSensitivities(std::vector<double> &curSens) const
  {
    curSens.assign(m_mappingSensitivities.begin(), m_mappingSensitivities.end());
  };
};

/**
* @brief creates a dummy class of the multi stage meta data with three stages
*
* The testobject has three stages
* Each stage a global duration with sensitivity information (value 1.2)
* Each stage has 2 controls defined.
* The first control has only one paramterization grid of 2 intervals, (value 1.0 bounds [2.3,4.1])
* the second control has 2 parameterization grids of 3 intervals each. (value 2.0 bounds [0.0, 8.7])
* The duration of the first grid of the second control is 0.7
* Eso model used is GenericEsoTest.
* 
* @return pointer to the created TestIntegratorMultiStageMetaData
* @sa createIntegratorSingleStageMetaDataTestObject
*/
TestIntegratorMultiStageMetaData *createIntegratorMultiStageMetaDataTestObject()
{
  // prepare single stages of multi stage problem.
  const int numStages = 3;

  std::vector<IntegratorSingleStageMetaData::Ptr> stages;
  stages.reserve(numStages);
  for(int i=0; i<numStages; i++){
    bool noUserGrid = false;
    bool freeGlobalDuration = true;
    IntegratorSingleStageMetaData::Ptr issmd(
      createIntegratorSingleStageMetaDataTestObject(noUserGrid, freeGlobalDuration));
    stages.push_back(issmd);
  }
  // number of mappings
  const int numMapps = numStages - 1;


  std::vector<Mapping::Ptr> mappings;
  mappings.reserve(numMapps);
  for(int i=0; i<numMapps; i++){
    TestDummyMapping::Ptr DummyMap(new TestDummyMapping());
    mappings.push_back(DummyMap);
  }

  TestIntegratorMultiStageMetaData *imsmd = new TestIntegratorMultiStageMetaData(mappings, stages);
  return imsmd; 
}

/**
* @class IntegratorSSMetaDataForwardingTestDummy
* @brief test dummy for forwarding test in the IntegratorMultiStageMetaData testsuite
*
* The IntegratorMultiStageMetaData class has many functions that simply forward the call
* to the current stage. To test the forwarding behavior this class stores whether a function
* has been called and if so which function.
*/
class IntegratorSSMetaDataForwardingTestDummy : public IntegratorSingleStageMetaData
{
protected:
  /**
  * @brief signature of the stage
  *
  * some functions are const and are not allowed to change the vlue of m_calledFunction
  * These function set the signature in the return values for the testsuite to check.
  */
  unsigned m_stageSignature;
public:
  IntegratorSSMetaDataForwardingTestDummy()
  {
    m_massMatrix = CsCscMatrix::Ptr(new CsCscMatrix(new cs(), true));
  }

  /**
  * @enum CalledFunction
  * @brief types for all non constant member functions
  */
  enum CalledFunction{
    NoFunctionCalled, ///< default value meaning that no function has been called lately
    SetStepStartTime, ///< last function called was setStartTime
    SetStepEndTime, ///< last function called was setEndTime
    SetStepCurrentTime, ///< last function called was setCurrentTime
    GetIntegrationGridDuration, ///< last function called was getDuration
    CalculateRhsStates, ///< last function called was calculateRhsStates
    SetStates, ///< last function called was setStates
    InitializeForNewIntegrationStep, ///< last function called was initializeForNewIntegrationStep
    InitializeForNextIntegration, ///< last function called was initializeForNextIntegrationStep
    GetNumIntegrationIntervals, ///< last function called was getNumIntegrationIntervals
    SetAllInitialValuesFree, ///< last function called was setAllInitialValuesFree
    GetGenericEso, ///< last function called was getGenericEso
    GetNumStateNonZeroes, ///< last function called was getNumStateNonZeroes
    GetGlobalDurationParameter, ///< last function called was getGlobalDuration
    GetJacobianFup, ///< last function called was getJacobianFup
    GetJacobianFstates ///< last function called was getJacobianFstates
  };
  CalledFunction m_calledFunction; ///< stores the last non const function called
  
  /**
  * @brief set the signature of the stage
  * @param signature signature to be set
  */
  void setSignature(const unsigned signature){
    m_stageSignature = signature;
    m_massMatrix->get_matrix_ptr()->nz = signature;
  }

 /**
  * @brief set function index to SetStartTime;
  * @param startTime dummy parameter
  */
  virtual void setStepStartTime(const double startTime){m_calledFunction = SetStepStartTime;}

 /**
  * @brief set function index to GetStartTime;
  * @return stageSignature
  */
  virtual double getStepStartTime()const
  {
    return double(m_stageSignature);
  }
  
 /**
  * @brief set function index to SetEndTime;
  * @param endTime dummy parameter
  */
  virtual void setStepEndTime(const double endTime){m_calledFunction = SetStepEndTime;}

  /**
  * @brief set function index to GetEndTime;
  * @return stageSignature
  */
  virtual double getStepEndTime()const
  {
    return double(m_stageSignature);
  }

   /**
  * @brief set function index to SetCurrentTime;
  * @param time dummy parameter
  */
  virtual void setStepCurrentTime(const double time){m_calledFunction = SetStepCurrentTime;}
  
 /**
  * @brief set function index to GetCurrentTime;
  * @return m_stageSignature
  */
  virtual double getStepCurrentTime()const
  {
    return double(m_stageSignature);
  }
  
 /**
  * @brief set function index to GetDuration;
  * @return m_stageSignature
  */
  virtual double getIntegrationGridDuration()const
  {
    return double(m_stageSignature);
  }
  
  // Integrator Interface methods
  
 /**
  * @brief set function index to CalculateRhsStates;
  * @param inputStates dummy parameter
  * @param rhsStates dummy parameter
  */
  virtual void calculateRhsStates(const utils::Array<double> &inputStates,
                                        utils::Array<double> &rhsStates)
  {m_calledFunction = CalculateRhsStates;}
  
  /**
  * @brief set function index to SetStates;
  * @param states dummy parameter
  */
  virtual void setStates(const utils::Array<double> &states){m_calledFunction = SetStates;}
  
 /**
  * @brief set function index to InitializeForNewIntegrationStep;
  */
  virtual void initializeForNewIntegrationStep()
  {m_calledFunction = InitializeForNewIntegrationStep;}
  
   /**
  * @brief set function index to InitializeForNextIntegration;
  */
  virtual void initializeForNextIntegration(){m_calledFunction = InitializeForNextIntegration;}
  
  /**
  * @brief set function index to GetMMatrix;
  * return uninitialized cs_sparse struct with signature in field nz
  */
  virtual CsCscMatrix::Ptr getMMatrix(void)const
  {
    return m_massMatrix;
  }

 /**
  * @brief set function index to GetNumIntegrationIntervals;
  * @return m_stageSignature
  */
  virtual int getNumIntegrationIntervals() const{return m_stageSignature;}

 /**
  * @brief set function index to SetAllInitialValuesFree;
  */
  virtual void setAllInitialValuesFree(){m_calledFunction = SetAllInitialValuesFree;}
 
  /**
  * @brief set function index to GetGenericEso;
  * @return dummy (not initialized genericEso pointer)
  */
  virtual GenericEso::Ptr getGenericEso()
  {
    m_calledFunction = GetGenericEso;
    return m_genericEso;
  }
  
  /**
  * @brief set function index to GetNumStateNonZeroes;
  * @return dummy value (0)
  */
  virtual int getNumStateNonZeroes()
  {
    m_calledFunction = GetNumStateNonZeroes;
    return 0;
  }
  
  /**
  * @brief set function index to GetGlobalDuration;
  * @return dummy (not initialized DurationParameter pointer
  */
  virtual DurationParameter::Ptr getGlobalDurationParameter()
  {
    m_calledFunction = GetGlobalDurationParameter;
    return m_globalDurationParameter;
  }
  
  // Parameter interface methods
  // no differentiation between time independent and control parameters
  
  /**
  * @brief set function index to GetJacobianFup;
  * @return dummy value (NULL)
  */
  virtual CsCscMatrix::Ptr getJacobianFup()
  {
    m_calledFunction = GetJacobianFup;
    CsCscMatrix::Ptr dummy;
    return dummy;
  }
  
  /**
  * @brief set function index to GetJacobianFstates;
  * @return dummy value (NULL)
  */
  virtual CsCscMatrix::Ptr getJacobianFstates()
  {
    m_calledFunction = GetJacobianFstates;
    CsCscMatrix::Ptr dummy;
    return dummy;
  }
};

/**
* @class IntegratorSingleStageMetaDataDummyForMultiStageTestWithDuration
* @brief in this testclass the global final time can be set. All other data are irrelevant
*/
class IntegratorSingleStageMetaDataDummyForMultiStageTestWithDuration : public IntegratorSingleStageMetaData
{
  public:
  
  /// @brief destructor
  ~IntegratorSingleStageMetaDataDummyForMultiStageTestWithDuration()
  {
  };
  
  /**
  * @brief constructor setting the global final time
  * @param globalDuration global final time to be set 
  */
  IntegratorSingleStageMetaDataDummyForMultiStageTestWithDuration(const DurationParameter::Ptr &globalDuration)
  {
    m_globalDurationParameter = globalDuration;
    m_gridDurations.push_back(globalDuration);
  }
};
