#include "MetaDataExtern.hpp"

/**
* @brief finds the time index of the given IntOptDataMap
*
* @param[in] IntOptDataMap structure
* @param[in] double timepoint
*/
unsigned findTimeIndex(IntOptDataMap &optData, const double timePoint){
  //set timeIndex to maximum unsigned value (-1 will be casted that way)
  unsigned timeIndex = -1;
  IntOptDataMap::iterator iter;
  const double threshold = 1e-12;
  for(iter = optData.begin(); iter != optData.end(); iter++){
    if(iter->second.timePoint + threshold >= timePoint &&
       iter->second.timePoint - threshold <= timePoint){
       timeIndex = iter->first;
       break;
    }
  }
  return timeIndex;
}