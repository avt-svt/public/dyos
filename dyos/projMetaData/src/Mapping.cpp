/** 
* @file Mapping.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Mapping - Part of DyOS Meta Data                                     \n
* =====================================================================\n
* This file contains the definitions of the memberfunctions of class   \n
* Mapping according to the UML Diagramm METAESO_8                      \n
* =====================================================================\n
* @author Fady Assassa, Tjalf Hoffmann
* @date 22.03.2012
*/

#include "Mapping.hpp"
#include <assert.h>
#include "cs.h"


/**
* @brief standard constructor 
*/
Mapping::Mapping()
{
}

/**
* @brief standard destructor 
*/
Mapping::~Mapping()
{
}

bool Mapping::isDecoupled()
{
  return true;
}

///////////////////// single shooting
/**
* @copydoc Mapping::applyStageMapping
*/
void SingleShootingMapping::applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage, 
                                              IntegratorSingleStageMetaData::Ptr currentStage,
                                              std::vector<double> &multiStageSens)
{
  // all states are mapped continously. DAE model is in both stages identical
  fullStateMapping(lastStage, currentStage);
  
  mapSensitivities(lastStage, currentStage, multiStageSens);
}

/**
* @brief maps the states assuming identical DAE model in both stages. Full mapping
* @param[in] lastStage is the last stage integrated
* @param[out] currentStage is the stage that is to be integrated
*/
void SingleShootingMapping::fullStateMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                                             IntegratorSingleStageMetaData::Ptr currentStage)
{
  // get Eso such that we can map the data from one stage to the other
  const GenericEso::Ptr lastEso = lastStage->getGenericEso();
  GenericEso::Ptr currentEso = currentStage->getGenericEso();
  // we go through the mapping vector. It seems to be a very naive implementation. However, it is easy
  // to understand
  const unsigned mapSize = m_varIndexMap.size();
  //assert(mapSize>0);
  // save eso indices in a eso friendly way. Required for reading and writing in eso
  utils::Array<int> lastEsoIndex(mapSize);
  std::vector<int> currentEsoIndex(mapSize);

  // iterate through mapping and iterate through number of eso indices
  iiMap::iterator it;
  int i;
  for( it = m_varIndexMap.begin(), i = 0; it != m_varIndexMap.end(); it++, i++){
    lastEsoIndex[i] = (*it).first;
    currentEsoIndex[i] = (*it).second;
  }
  // read out the current values of the eso for the mapping vector obtained previously
  utils::Array<double> lastEsoVals(mapSize);
  lastEso->getVariableValues(mapSize, lastEsoVals.getData(), lastEsoIndex.getData());
  // write the last vals into the current eso.
  int *currentEsoIndexPtr = 0;
  if(!currentEsoIndex.empty()){
    currentEsoIndexPtr = &currentEsoIndex[0];
  }
  currentEso->setVariableValues(mapSize, lastEsoVals.getData(), currentEsoIndexPtr);
  // in addition set mapped differential states to the initialValueParameters of the stage
  currentStage->setMappingValues(lastEsoVals, currentEsoIndex);
}
/* fullSensitivityMapping is outcommented, since we have a more general function 
   mapSensitivities. However, we don't want to delete this one, since it is easy to understand
   and a back-up.*/
/**
* @brief maps the sensitivties assuming identical DAE model and full state mapping
* @param[in] lastStage is the last stage integrated
* @param[out] multiStageSens Sensitivities that are carried over from one stage to the next
*/
/*
void SingleShootingMapping::fullSensitivityMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                                                   std::vector<double> &multiStageSens)
{
  // get sensitivity information of last stage
  int lastNumParams = lastStage->getNumCurrentSensitivityParameters();
  GenericEso::Ptr lastEso = lastStage->getGenericEso();
  // calculate number of states. This is equal to alg. + diff. states for full mapping
  int numMaps = m_varIndexMap.size();
  std::vector<double> lastSens(numMaps * lastNumParams);
  // read current in sensitivities in last stage
  lastStage->getCurrentSensitivities(lastSens);
  // write sensitivities in multi stage sensitivity array
  unsigned lastSizeSens = multiStageSens.size();
  unsigned newSizeSens = lastSizeSens + numMaps * lastNumParams;
  multiStageSens.resize(newSizeSens);
  for (unsigned i = lastSizeSens; i < newSizeSens; i++){
    multiStageSens[i] = lastSens[i - lastSizeSens];
  }
}*/

/**
* @brief maps the sensitivities of last stage to the current stage according to the mapping
*
* the integrator expect a full sensitivity matrix with respect to the states of the current stage
* All unmapped states produce zero rows. A permutation of the sensitivities might be necessary if
* an equation is mapped to a different equation index.
* @param[in] lastStage is the last stage integrated
* @param[in] currentStage is the stage that is to be integrated
* @param[out] multiStageSens Sensitivities that are carried over from one stage to the next
*                            on entry the length of the vector is 
*                               numOldMappingParams*lastStage.numEquations
*                            on exit the length of the vector is 
*                               (numOldMappingParams+lastStage.numParams)*currentStage.numEquations
*/
void SingleShootingMapping::mapSensitivities(const IntegratorSingleStageMetaData::Ptr lastStage,
                                                   IntegratorSingleStageMetaData::Ptr currentStage,
                                                   std::vector<double> &multiStageSens)
{
  std::vector<double> stageSensitivities;
  lastStage->getCurrentSensitivities(stageSensitivities);
  // if stage sensitivities are empty, no sensitivities have been set by the integrator
  // in that case nothing is to be done in this function
  if(stageSensitivities.empty()){
    return;
  }
  std::vector<double> multiStageSensCopy;
  multiStageSensCopy.assign(multiStageSens.begin(), multiStageSens.end());
  
  const unsigned numOldSens = multiStageSens.size();
  const unsigned numEquationsLastStage = lastStage->getGenericEso()->getNumEquations();
  assert((numOldSens % numEquationsLastStage) == 0);
  const unsigned numOldMappingParams = numOldSens / numEquationsLastStage;
  
  const unsigned numEquationsCurrentStage = currentStage->getGenericEso()->getNumEquations();
  const unsigned numParamsLastStage = lastStage->getNumSensitivityParameters();
  const unsigned numNewSens = (numOldMappingParams + numParamsLastStage)*numEquationsCurrentStage;
  
  // resize and reset for output
  multiStageSens.assign(numNewSens, 0.0);
  

  // sort in the mapping sensitivities into the new order according to the mapping
  assert(stageSensitivities.size() == numParamsLastStage * numEquationsLastStage);
  
  const unsigned numStatesLastStage = lastStage->getGenericEso()->getNumStates();
  std::vector<int> esoIndicesOfStatesLastStage(numStatesLastStage);
  lastStage->getGenericEso()->getStateIndex(numStatesLastStage, &esoIndicesOfStatesLastStage[0]);
  for(unsigned i=0; i<numStatesLastStage; i++){
    const unsigned oldStateIndex = lastStage->getGenericEso()->getStateIndexOfVariable(
                                                            esoIndicesOfStatesLastStage[i]);
    
    // copy data for the state if it is mapped only (and not overridden by a initialValueParameter)
    if(m_varIndexMap.count(esoIndicesOfStatesLastStage[i]) > 0){
      const int mappedEsoIndex = m_varIndexMap[esoIndicesOfStatesLastStage[i]];
      const unsigned mappedStateIndex = currentStage->getGenericEso()
                                              ->getStateIndexOfVariable(mappedEsoIndex);
      
      std::vector<InitialValueParameter::Ptr> initials;
      currentStage->getInitialValueParameter(initials);
      for(unsigned j=0; j<initials.size(); j++){
        if(initials[j]->getEsoIndex() == mappedEsoIndex 
           && initials[j]->getUserFlag() == true){
          //in case of setting an initial value parameter, the states are no longer connected
          //in the sense of a state mapping. So erase the entry of the mapping.
          m_varIndexMap.erase(esoIndicesOfStatesLastStage[i]);
        }
      }
      //skip copying of data if entry is erased
      if(m_varIndexMap.count(esoIndicesOfStatesLastStage[i]) <= 0){
        continue;
      }
      // first sort in the old mapping sensitivities
      // (copied to new positions, since the order of the states may have changed)
      for(unsigned j=0; j<numOldMappingParams; j++){
        // Comment C1: matrix is sorted columnwise so first get index of the data block
        // of the column and then add the index of the block entry(state index)
        const unsigned inputElementIndex = j * numEquationsLastStage + oldStateIndex;
        const unsigned outputElementIndex = j * numEquationsCurrentStage + mappedStateIndex;
        multiStageSens[outputElementIndex] = multiStageSensCopy[inputElementIndex];
      }
      
      // then sort in the new mapping sensitivities retrieved out of the last stage
      // store the number of entries of the blocks belonging to the old parameters
      const unsigned numElementsOldMappingBlocks = numOldMappingParams * numEquationsCurrentStage;
      for(unsigned j=0; j<numParamsLastStage; j++){
        // see comment C1
        const unsigned inputElementIndex = j * numEquationsLastStage + oldStateIndex;
        unsigned outputElementIndex = j * numEquationsCurrentStage + mappedStateIndex;
        // for the output the elements in the blocks of the old mapping parameters
        // have to be taken into account
        outputElementIndex += numElementsOldMappingBlocks;
        multiStageSens[outputElementIndex] = stageSensitivities[inputElementIndex];
      }
    }// if
  }// for
}

/**
* @copydoc Mapping::setIndexMaps
*/
void SingleShootingMapping::setIndexMaps(const iiMap varIndexMap, const iiMap eqnIndexMap)
{
  // copy of the content
  m_varIndexMap = varIndexMap;
  m_equationIndexMap = eqnIndexMap;
}

/**
* @copydoc iiMap Mapping::getEquationsVectorMap(const iiMap &equationsMap)
*/
iiMap SingleShootingMapping::getEquationsVectorMap(const iiMap &equationsMap)
                          
{
  iiMap equationsVectorMap;
  iiMap equationsMapCopy = equationsMap;
  iiMap::iterator iter;
  for(iter = equationsMapCopy.begin(); iter != equationsMapCopy.end(); iter++){
    if(m_equationIndexMap.count(iter->second) > 0){
      equationsVectorMap[iter->first] = m_equationIndexMap[iter->second];
    }
  }
  return equationsVectorMap;
}

/**
* @copydoc Mapping::getReverseEquationsVectorMap
*/
iiMap SingleShootingMapping::getReverseEquationsVectorMap
                          (const iiMap &equationsMap)
{
  iiMap equationsVectorMap;
  iiMap equationsMapCopy = equationsMap;
  iiMap::iterator iter1;
  for(iter1 =equationsMapCopy.begin(); iter1!=equationsMapCopy.end(); iter1++){
    iiMap::iterator iter2;
    for(iter2 = m_equationIndexMap.begin(); iter2!=m_equationIndexMap.end(); iter2++){
      if(iter2->second == iter1->second){
        equationsVectorMap[iter1->second] = iter2->first;
        break;
      }
    }
  }
  return equationsVectorMap;
}

bool SingleShootingMapping::isDecoupled()
{
  return m_equationIndexMap.empty();
}

/**
* @brief calculate the mapping from any stage to another
*
* If a mapping of two not adjacent stages is needed, the mapping of the first
* to the last stage must be calculated using all stages between them.
* @param[in] mappings mappings of all stages
* @param[in] numEquationsCurrentStage
* @param[in] currentStage index of the stage of which the indices are to be mapped
* @param[in] targetStage index of the stage which the indices are to be mapped to
* @sa iiMap Mapping::getReverseEquationsVectorMap,
*     iiMap Mapping::getEquationsVectorMap(const iiMap &equationsMap)
*/
iiMap Mapping::getEquationsVectorMap(const std::vector<Mapping::Ptr> mappings,
                               const unsigned numEquationsCurrentStage,
                               const unsigned currentStage,
                               const unsigned targetStage)
{
  iiMap equationsVectorMap;
  for(unsigned i=0; i<numEquationsCurrentStage; i++){
    equationsVectorMap[i] = i;
  }
  
  assert(mappings.size() >= currentStage);
  assert(mappings.size() >= targetStage);
  if(currentStage > targetStage){
    for(unsigned i=currentStage; i>targetStage; i--){
      equationsVectorMap = mappings[i-1]->getReverseEquationsVectorMap(equationsVectorMap);
    }
  }
  else{
    for(unsigned i=currentStage; i<targetStage; i++){
      equationsVectorMap = mappings[i]->getEquationsVectorMap(equationsVectorMap);
    }
  }
  return equationsVectorMap;
}

void MultipleShootingMapping::applyStageMapping(const IntegratorSingleStageMetaData::Ptr lastStage,
                                                      IntegratorSingleStageMetaData::Ptr currentStage,
                                                      std::vector<double> &multiStageSens)
{}

void MultipleShootingMapping::setIndexMaps(const iiMap varIndexMap, const iiMap eqnIndexMap)
{}

iiMap MultipleShootingMapping::getEquationsVectorMap(const iiMap &equationsMap)
{
  iiMap emptyMap;
  return emptyMap;
}

iiMap MultipleShootingMapping::getReverseEquationsVectorMap(const iiMap &equationsMap)
{
  iiMap emptyMap;
  return emptyMap;
}
