/**
* @file OptimizerMultiStageMetaData.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the OptimizerMultiStageMetaData class             \n
* according to MetaEso8.vsd                                            \n
* =====================================================================\n
* @author Klaus Stockmann, Tjalf Hoffmann
* @date 13.09.2012
*/

#include <cassert>
#include <vector>

#include "cs.h"

#include "OptimizerMultiStageMetaData.hpp"

/**
* @brief standard constructor
*/
OptimizerMultiStageMetaData::OptimizerMultiStageMetaData()
{
}

/**
* @brief copy constructor
*/
OptimizerMultiStageMetaData::OptimizerMultiStageMetaData(const OptimizerMultiStageMetaData &copy)
{
  m_intMSMD = copy.m_intMSMD;
  //optProbDim has to allocate its own memory for linJac and nonLinJac
  //first copy all data (pointers are overridden later)
  m_optProbDim = copy.m_optProbDim;

  //allocate nonLinJac
  if(m_optProbDim.nonLinJacDecVar != nullptr){
    CsTripletMatrix::Ptr nonLinJac = copy.m_optProbDim.nonLinJacDecVar;
    int nnz = nonLinJac->get_nnz();
    std::vector<double> values(static_cast<size_t>(nnz));
    m_optProbDim.nonLinJacDecVar = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(nonLinJac->get_n_rows(),
                                                                            nonLinJac->get_n_cols(),
                                                                            nnz,
                                                                            nonLinJac->get_matrix_ptr()->i,
                                                                            nonLinJac->get_matrix_ptr()->p,
                                                                            values.data()));
  //allocate linJac
  if(m_optProbDim.linJac->get_matrix_ptr() != nullptr){
    CsTripletMatrix::Ptr linJac = copy.m_optProbDim.linJac;
    int nnz = linJac->get_nnz();
    std::vector<double> values(static_cast<size_t>(nnz));
    m_optProbDim.linJac = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(linJac->get_n_rows(),
                                                                   linJac->get_n_cols(),
                                                                   nnz,
                                                                   linJac->get_matrix_ptr()->i,
                                                                   linJac->get_matrix_ptr()->p,
                                                                   values.data()));
    }
  }

  m_optSSMDVec = copy.m_optSSMDVec;
  m_totalTimeData = copy.m_totalTimeData;
  m_treatObjective = copy.m_treatObjective;
  m_globalConstraints = copy.m_globalConstraints;
}

/**
* @brief constructor setting the optimizerMetaData
*
* @param[in] optSSMDVec vector of pointers to OptimizerSingleStageMetaData objects
* @param[in] intMSMD pointer to IntegratorMultiStageMetaData object
* @param[in] StageIndependentConstraints vector of StageIndependentConstraints
* @param[in] treatObjective vector of length equal to the number of stages. The boolean values
*                           specify, if an individual stage contributes to the ojective function value
*/
OptimizerMultiStageMetaData::OptimizerMultiStageMetaData(std::vector<OptimizerSingleStageMetaData::Ptr> optSSMDVec,
                                                         IntegratorMultiStageMetaData::Ptr intMSMD,
                                                         std::vector<bool> treatObjective)
{
  assert(optSSMDVec.size()>0);
  assert(intMSMD);
  assert(optSSMDVec.size() == treatObjective.size());


  m_optSSMDVec = optSSMDVec;
  m_intMSMD = intMSMD;
  m_treatObjective = treatObjective;

  // assemble optimizer problem dimensions (so far sum up of quantities in individual stages, no
  // entities due to stage mapping considered)
  m_optProbDim.numOptVars = 0;
  m_optProbDim.numLinConstraints = 0;
  m_optProbDim.numNonLinConstraints = 0;
  m_optProbDim.numConstParams = 0;
  m_optProbDim.nonLinObjIndex = 0; // applicable to multi-stage? -> Antwort faas: nein!

  const size_t nStages = m_optSSMDVec.size();
  for(unsigned i=0; i<nStages; i++) {
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    m_optProbDim.numOptVars += optProbDimStage.numOptVars;
    m_optProbDim.numConstParams += optProbDimStage.numConstParams;
    m_optProbDim.numLinConstraints += optProbDimStage.numLinConstraints;
    m_optProbDim.numNonLinConstraints += optProbDimStage.numNonLinConstraints;
  }


  assembleNonLinJacobian();
  assembleLinJacobian();

  // initialize m_totalTimeData. Remark: if isSet is 'false', no bounds are required
  m_totalTimeData.isSet = false;
}

/**
 * @brief destructor; destroys CSparse matrices in m_optProbDim (storing linear and nonlinear Jacobians)
 */
OptimizerMultiStageMetaData::~OptimizerMultiStageMetaData()
{
}

/**
* @copydoc OptimizerMetaData::setLagrangeMultipliers
* In multi-stage case, it is assumed that input vector 'values' contains lagrange
* multipliers of nonlinear constraints and of linear constraints.
* First, distribute lagrange multipliers of nonlinear constraints to individual
* stages where they are stored. The last entries in the input vector contain the
* lagrange multipliers for the linear constraints. They are stored in attribute
* m_linConstraintsLagrangeMultipliers.
*/
void OptimizerMultiStageMetaData::setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                                         const utils::Array<double> &linMultipliers)
{
  // input vector 'values' is assumed to contain lagrange multipliers of nonlinear
  // and linear constraints
  assert(nonLinMultipliers.getSize() == m_optProbDim.numNonLinConstraints);
  assert(linMultipliers.getSize() == m_optProbDim.numLinConstraints);

  const unsigned nStages = m_optSSMDVec.size();


  unsigned linMultIndex = 0, nonLinMultIndex = 0;
  for(unsigned i=0; i<nStages; i++) {
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    const unsigned numNonLinConstr = optProbDimStage.numNonLinConstraints;
    const unsigned numLinConstr = optProbDimStage.numLinConstraints;
    assert(nonLinMultipliers.getSize() >= nonLinMultIndex + numNonLinConstr);
    assert(linMultipliers.getSize() >= linMultIndex + numLinConstr);
    utils::Array<double> nonLinMultipliersStage(numNonLinConstr);
    for(unsigned j=0; j<numNonLinConstr; j++){
      nonLinMultipliersStage[j] = nonLinMultipliers[j + nonLinMultIndex];
    }
    utils::Array<double> linMultipliersStage(numLinConstr);
    for(unsigned j=0; j<numLinConstr; j++){
      linMultipliersStage[j] = linMultipliers[j + linMultIndex];
    }
    m_optSSMDVec[i]->setLagrangeMultipliers(nonLinMultipliersStage,
                                            linMultipliersStage);
    nonLinMultIndex += numNonLinConstr;
    linMultIndex += numLinConstr;
  }
  
  //todo: set lagrangeMultipliers for global constraints
  //also in updatAdjoints global constraints are not handled yet

}


/**
* @copydoc OptimizerMetaData::getLagrangeMultipliers
* Put lagrange multipliers of nonlinear and linear constraints into output vector value.
* First, collect lagrange multipliers of nonlinear constraints from individual stages where
* they are stored and put them into one large vector. Then append lagrange multipliers of
* linear (multi-stage) constraints.
*/
void OptimizerMultiStageMetaData::getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                                         utils::Array<double> &linMultipliers) const
{
  assert(nonLinMultipliers.getSize() == m_optProbDim.numNonLinConstraints);
  assert(linMultipliers.getSize() == m_optProbDim.numLinConstraints);

  const unsigned nStages = m_optSSMDVec.size();

  unsigned nonLinMultIndex = 0, linMultIndex = 0;
  // collect vector of vector containing lagrange multipliers of individual stages
  for(unsigned i=0; i<nStages; i++) {
     OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    const unsigned numNonLinConstr = optProbDimStage.numNonLinConstraints;
    const unsigned numLinConstr = optProbDimStage.numLinConstraints;
    assert(nonLinMultipliers.getSize() >= nonLinMultIndex + numNonLinConstr);
    assert(linMultipliers.getSize() >= linMultIndex + numLinConstr);

    utils::WrappingArray<double> nonLinMultipliersCurrentStage(numNonLinConstr,
                                                               nonLinMultipliers.getData()
                                                                 + nonLinMultIndex);
    utils::WrappingArray<double> linMultipliersCurrentStage(numLinConstr,
                                                            linMultipliers.getData()
                                                                 + linMultIndex);
    m_optSSMDVec[i]->getLagrangeMultipliers(nonLinMultipliersCurrentStage,
                                            linMultipliersCurrentStage);
    nonLinMultIndex += numNonLinConstr;
    linMultIndex += numLinConstr;
  }
  
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
    nonLinMultipliers[nonLinMultIndex + i] = m_globalConstraints[i]->getLagrangeMultiplier();
  }

  // append lagrange multipliers of global constraints
  assert(linMultipliers.getSize() >= linMultIndex + m_linConstraintsLagrangeMultipliers.size());
  for(unsigned i=0; i<m_linConstraintsLagrangeMultipliers.size(); i++) {
    linMultipliers[i+linMultIndex] = m_linConstraintsLagrangeMultipliers[i];
  }
}

/**
* @copydoc OptimizerMetaData::setDecVarLagrangeMultipliers
*/
void OptimizerMultiStageMetaData::setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier)
{
  unsigned numDecVarsLastStages = 0;
  for(unsigned i=0; i<m_optSSMDVec.size(); i++){
    unsigned numStageDecVars = m_optSSMDVec[i]->getOptProbDim().numOptVars;
    utils::Array<double> stageLagrangeMultipliers(numStageDecVars);
    for(unsigned j=0; j<numStageDecVars; j++){
      assert(numDecVarsLastStages + j < lagrangeMultiplier.getSize());
      stageLagrangeMultipliers[j] = lagrangeMultiplier[numDecVarsLastStages + j];
    }
    m_optSSMDVec[i]->setDecVarLagrangeMultipliers(stageLagrangeMultipliers);
    numDecVarsLastStages += numStageDecVars;
  }
}

void OptimizerMultiStageMetaData::setObjectiveLagrangeMultiplier(const double lagrangeMultiplier)
{
  for(unsigned i=0; i<m_optSSMDVec.size(); i++){
    if(m_treatObjective[i]){
      m_optSSMDVec[i]->setObjectiveLagrangeMultiplier(lagrangeMultiplier);
    }
    else{
      m_optSSMDVec[i]->setObjectiveLagrangeMultiplier(0.0);
    }
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinObjValue
* collect nonlinear objective values from individual stages and sum up all objective values
* contributing to the total objective value (according to (boolean) attribute m_treatObjective)
*/
double OptimizerMultiStageMetaData::getNonLinObjValue() const
{
  double totalNonLinObjective = 0.0;
  const unsigned nStages = m_optSSMDVec.size();
  assert(nStages == m_treatObjective.size());

  unsigned nTreatObjective = 0;

  for(unsigned i=0; i<nStages; i++) {
    if(m_treatObjective[i] == true) {
      nTreatObjective++;
      totalNonLinObjective = totalNonLinObjective + m_optSSMDVec[i]->getNonLinObjValue();
    }
  }

  return totalNonLinObjective;
}

/**
* @brief setter for attribute m_constraints
* @remarks not yet implemented. This method will be responsible for the distribution
*          of stage independent constraints to individual stages.
* @param[in] constraints new value for m_constraints
*/
void OptimizerMultiStageMetaData::setConstraints(const std::vector<StatePointConstraint> &)
{
}


/**
* @brief update nonlinear constraints
*/
void OptimizerMultiStageMetaData::updateNonLinJacobianDecVarForNewGlobalConstraints
                                  (const std::vector<GlobalConstraint::Ptr> &globalConstraints,
                                   int jacRowsOffset)
{
  int numNonZeroesDecVarOld = 0;
  if(m_optProbDim.nonLinJacDecVar.get() != NULL){
    numNonZeroesDecVarOld =  m_optProbDim.nonLinJacDecVar->get_nnz();
  }
  int numNonZeroesDecVar = 0;
  std::vector<int> colIndexDecVar, rowIndexDecVar;
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
    std::vector<unsigned> jacColIndDecVar;
    jacColIndDecVar = m_globalConstraints[i]->getJacobianDecVarColIndices(m_optSSMDVec);
    colIndexDecVar.insert(colIndexDecVar.end(), jacColIndDecVar.begin(), jacColIndDecVar.end());
    rowIndexDecVar.insert(rowIndexDecVar.end(), jacColIndDecVar.size(), jacRowsOffset + i);
    numNonZeroesDecVar += jacColIndDecVar.size();
  }
  numNonZeroesDecVar += numNonZeroesDecVarOld;
  utils::Array<int> rowIndexDecVarTotal(numNonZeroesDecVar);
  utils::Array<int> colIndexDecVarTotal(numNonZeroesDecVar);
  utils::Array<double> valuesDecVar(numNonZeroesDecVar, 0.0);
  if(m_optProbDim.nonLinJacDecVar.get() != NULL){
    //copy old data
    std::copy(m_optProbDim.nonLinJacDecVar->get_matrix_ptr()->i,
              m_optProbDim.nonLinJacDecVar->get_matrix_ptr()->i + numNonZeroesDecVarOld,
              rowIndexDecVarTotal.getData());
    std::copy(m_optProbDim.nonLinJacDecVar->get_matrix_ptr()->p,
              m_optProbDim.nonLinJacDecVar->get_matrix_ptr()->p + numNonZeroesDecVarOld,
              colIndexDecVarTotal.getData());
  }
  std::copy(rowIndexDecVar.begin(), rowIndexDecVar.end(),
            rowIndexDecVarTotal.getData() + numNonZeroesDecVarOld);
  std::copy(colIndexDecVar.begin(), colIndexDecVar.end(),
            colIndexDecVarTotal.getData() + numNonZeroesDecVarOld);
  CsTripletMatrix::Ptr globalNonLinJacDecVar(new SortedCsTripletMatrix(m_optProbDim.numNonLinConstraints,
                                                                 m_optProbDim.numOptVars,
                                                                 numNonZeroesDecVar,
                                                                 rowIndexDecVarTotal.getData(),
                                                                 colIndexDecVarTotal.getData(),
                                                                 valuesDecVar.getData()));
  m_optProbDim.nonLinJacDecVar = globalNonLinJacDecVar;

  
}


void OptimizerMultiStageMetaData::updateNonLinJacobianConstParamForNewGlobalConstraints
                                  (const std::vector<GlobalConstraint::Ptr> &globalConstraints,
                                   int jacRowsOffset)
{
  int numNonZeroesConstParamOld = 0;
  if(m_optProbDim.nonLinJacConstPar.get() != NULL){
    numNonZeroesConstParamOld =  m_optProbDim.nonLinJacConstPar->get_nnz();
  }
  int  numNonZeroesConstParam = 0;
  std::vector<int> colIndexConstParam, rowIndexConstParam;
  for(unsigned i=0; i<m_globalConstraints.size(); i++){    
    std::vector<unsigned> jacColIndConstParam;
    jacColIndConstParam = m_globalConstraints[i]->getJacobianConstParamColIndices(m_optSSMDVec);
    colIndexConstParam.insert(colIndexConstParam.end(),
                              jacColIndConstParam.begin(),
                              jacColIndConstParam.end());
    rowIndexConstParam.insert(rowIndexConstParam.end(),
                              jacColIndConstParam.size(),
                              jacRowsOffset + i);
    numNonZeroesConstParam += jacColIndConstParam.size();
  }

  numNonZeroesConstParam += numNonZeroesConstParamOld;
  utils::Array<int> rowIndexConstParamTotal(numNonZeroesConstParam);
  utils::Array<int> colIndexConstParamTotal(numNonZeroesConstParam);
  utils::Array<double> valuesConstParam(numNonZeroesConstParam, 0.0);
  if(m_optProbDim.nonLinJacConstPar.get() != NULL){
    //copy old data
    std::copy(m_optProbDim.nonLinJacConstPar->get_matrix_ptr()->i,
              m_optProbDim.nonLinJacConstPar->get_matrix_ptr()->i + numNonZeroesConstParamOld,
              rowIndexConstParamTotal.getData());
    std::copy(m_optProbDim.nonLinJacConstPar->get_matrix_ptr()->p,
              m_optProbDim.nonLinJacConstPar->get_matrix_ptr()->p + numNonZeroesConstParamOld,
              colIndexConstParamTotal.getData());
  }
  std::copy(rowIndexConstParam.begin(), rowIndexConstParam.end(),
            rowIndexConstParamTotal.getData() + numNonZeroesConstParamOld);
  std::copy(colIndexConstParam.begin(), colIndexConstParam.end(),
            colIndexConstParamTotal.getData() + numNonZeroesConstParamOld);
  CsTripletMatrix::Ptr globalNonLinJacConstParam(new SortedCsTripletMatrix
                                                            (m_optProbDim.numNonLinConstraints,
                                                             m_optProbDim.numConstParams,
                                                             numNonZeroesConstParam,
                                                             rowIndexConstParamTotal.getData(),
                                                             colIndexConstParamTotal.getData(),
                                                             valuesConstParam.getData()));

  m_optProbDim.nonLinJacConstPar = globalNonLinJacConstParam;
}

/**
* @brief setter for global constraints
*
* @param globalConstraints global constraints to be set
*/
void OptimizerMultiStageMetaData::setGlobalConstraints
                            (const std::vector<GlobalConstraint::Ptr> &globalConstraints)
{
  if(globalConstraints.empty()){
    return;
  }
  m_globalConstraints.assign(globalConstraints.begin(), globalConstraints.end());
  //add all set constraints
  unsigned constraintIndexOffset = 0;
  for(unsigned i=0; i<m_optSSMDVec.size(); i++){
    constraintIndexOffset += m_optSSMDVec[i]->getOptProbDim().numNonLinConstraints;
    constraintIndexOffset++;  //index of objective is reserved for last index, so skip this index
  }
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
    m_globalConstraints[i]->addConstraint(m_optSSMDVec, constraintIndexOffset + i);
  }
  
  int jacRowsOffset = m_optProbDim.numNonLinConstraints;
  m_optProbDim.numNonLinConstraints += globalConstraints.size();
  updateNonLinJacobianDecVarForNewGlobalConstraints(globalConstraints, jacRowsOffset);
  updateNonLinJacobianConstParamForNewGlobalConstraints(globalConstraints, jacRowsOffset);
}

/**
* @copydoc OptimizerMetaData::getDecVarBounds
* collect decision variable bounds from individual stages where they are stored and put
* them into one large vector
*/
void OptimizerMultiStageMetaData::getDecVarBounds(utils::Array<double> &lowerBounds,
                                                  utils::Array<double> &upperBounds) const
{
  const unsigned nStages = m_optSSMDVec.size();
  unsigned boundsIndex = 0;
  // collect vector of vector containing lower- and upper bounds of individual stages
  for(unsigned i=0; i<nStages; i++) {
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    const unsigned nConstraintsCurrentStage = optProbDimStage.numOptVars;
    assert(lowerBounds.getSize() >= boundsIndex + nConstraintsCurrentStage);
    assert(upperBounds.getSize() >= boundsIndex + nConstraintsCurrentStage);
    utils::WrappingArray<double> stageLowerBounds(nConstraintsCurrentStage,
                                                  lowerBounds.getData() + boundsIndex);
    utils::WrappingArray<double> stageUpperBounds(nConstraintsCurrentStage,
                                                  upperBounds.getData() + boundsIndex);
    m_optSSMDVec[i]->getDecVarBounds(stageLowerBounds, stageUpperBounds);
    boundsIndex += nConstraintsCurrentStage;
  }
}


/**
* @copydoc OptimizerMetaData::getDecVarValues
* collect decision variable values from individual stages where they are stored and put
* them into one large vector
*/
void OptimizerMultiStageMetaData::getDecVarValues(utils::Array<double> &values) const
{
  const unsigned nStages = m_optSSMDVec.size();

  unsigned valuesIndex = 0;
  // collect vector of vector containing decision variable values of individual stages
  for(unsigned i=0; i<nStages; i++) {
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    const unsigned numOptVarsCurrentStage = optProbDimStage.numOptVars;
    assert(values.getSize() >= valuesIndex + numOptVarsCurrentStage);
    utils::WrappingArray<double> decVarValuesCurrentStage(numOptVarsCurrentStage,
                                                          values.getData() + valuesIndex);
    m_optSSMDVec[i]->getDecVarValues(decVarValuesCurrentStage);
    valuesIndex += numOptVarsCurrentStage;
  }
}


/**
* @copydoc OptimizerMetaData::setDecVarValues
* distribute decision variable values as resulting from optimization to individual
* stages where they are stored
*/
void OptimizerMultiStageMetaData::setDecVarValues(const utils::Array<const double> &decVars)
{
  const unsigned nStages = m_optSSMDVec.size();

  unsigned decVarsIndex = 0;
  // distribute input vector to individual stages
  for(unsigned i=0; i<nStages; i++) {
    const unsigned numOptVarsStage = m_optSSMDVec[i]->getOptProbDim().numOptVars;

    // Wrapping Array not possible since decVars is const
    // maybe implement a ConstWrappingArray?
    utils::Array<double> decVarValuesStage(numOptVarsStage);
    for(unsigned j=0; j<numOptVarsStage; j++){
      assert(decVars.getSize() > decVarsIndex + j);
      decVarValuesStage[j] = decVars[j+decVarsIndex];
    }
    utils::WrappingArray<const double> constValues(numOptVarsStage, decVarValuesStage.getData());
    m_optSSMDVec[i]->setDecVarValues(constValues);
    decVarsIndex += numOptVarsStage;
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinConstraintBounds
* collect constraints bounds from individual stages where they are stored and put
* them into one large vector
*/
void OptimizerMultiStageMetaData::getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                            utils::Array<double> &upperBounds) const
{
  assert(lowerBounds.getSize() == m_optProbDim.numNonLinConstraints);
  assert(upperBounds.getSize() == m_optProbDim.numNonLinConstraints);
  const unsigned nStages = m_optSSMDVec.size();

  unsigned boundsIndex = 0;
  // collect vector of vector containing for lower- and upper bounds of individual stages
  for(unsigned i=0; i<nStages; i++) {
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    const unsigned nConstraintsCurrentStage = optProbDimStage.numNonLinConstraints;
    utils::WrappingArray<double> lowerBoundsCurrentStage(nConstraintsCurrentStage,
                                                         lowerBounds.getData() + boundsIndex),
                                 upperBoundsCurrentStage(nConstraintsCurrentStage,
                                                         upperBounds.getData() + boundsIndex);
    m_optSSMDVec[i]->getNonLinConstraintBounds(lowerBoundsCurrentStage, upperBoundsCurrentStage);
    boundsIndex += nConstraintsCurrentStage;
  }
  
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
    lowerBounds[boundsIndex + i] = m_globalConstraints[i]->getLowerBound();
    upperBounds[boundsIndex + i] = m_globalConstraints[i]->getUpperBound();
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinConstraintValues
* collect values of nonlinear constraints from individual stages where they are stored and put
* them into one large vector
*/
void OptimizerMultiStageMetaData::getNonLinConstraintValues(utils::Array<double> &values) const
{
  const unsigned nStages = m_optSSMDVec.size();

  unsigned valuesIndex = 0;
  // collect vector of vector containing nonlinear constraint values of individual stages
  for(unsigned i=0; i<nStages; i++) {
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[i]->getOptProbDim();
    const unsigned numNonLinConstraintCurrentStage = optProbDimStage.numNonLinConstraints;
    assert(values.getSize() >= numNonLinConstraintCurrentStage + valuesIndex);
    utils::WrappingArray<double> nonLinConstraintValuesCurrentStage(numNonLinConstraintCurrentStage,
                                                                    values.getData() + valuesIndex);
    m_optSSMDVec[i]->getNonLinConstraintValues(nonLinConstraintValuesCurrentStage);
    valuesIndex += numNonLinConstraintCurrentStage;
  }
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
   values[valuesIndex + i] = m_globalConstraints[i]->evaluateConstraint(m_optSSMDVec);
  }
}


/**
* @brief collect multi stage gradient information
*
* The constraint derivatives of the mapped parameters are extracted from the MS OptIntData struct.
* Only gradients of parameters referring to varIndices are extracted. This function does nothing
* if called for the first stage, since the first stage doesn't contain any multistage gradient
* information at all.
*
* @param[in,out] globalDerivativeMatrix matrix in which the block of derivatives is inserted
* @param[in] stageIndex index of the stage to which the constraint derivatives belong
* @param[in] varIndices indices of the parameters which gradients are to be collected
*/
void OptimizerMultiStageMetaData::collectMultiStageGradients(CsTripletMatrix::Ptr globalDerivativeMatrix,
                                                             const unsigned stageIndex,
                                                             std::vector<unsigned> &varIndices) const
{
  if(stageIndex>0 && !m_intMSMD->getMappings()[stageIndex-1]->isDecoupled()){
    std::vector<IntOptDataMap> msOptData = m_intMSMD->getIntOptData();

    //create a full matrix with dimensions
    unsigned numCols = 0;
    const unsigned numRows = m_optSSMDVec[stageIndex]->getOptProbDim().numNonLinConstraints;
    unsigned curRowIndex = 0;
    for(unsigned j=0; j<stageIndex; j++){
      OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[j]->getOptProbDim();
      numCols += optProbDimStage.numOptVars;
      curRowIndex += optProbDimStage.numNonLinConstraints;
    }
    CsTripletMatrix::Ptr msGradients;
    OptimizerSingleStageMetaData::constructFullCooMatrix(msGradients, numRows, numCols);

    unsigned objectiveIndex = m_optSSMDVec[stageIndex]->getObjectiveData().getEsoIndex();

    OptimizerSingleStageMetaData::retrieveNonLinConstGradients(msGradients,
                                                               varIndices,
                                                               msOptData[stageIndex],
                                                               objectiveIndex);
    globalDerivativeMatrix->setSubMatrix(msGradients, curRowIndex, 0);
  }
}
/**
* @brief collect single stage gradient information
*
* The constraint derivatives of the stage parameters are extracted from the SS OptIntData struct.
* Only gradients of parameters referring to varIndices are extracted.
*
* @param[in,out] globalDerivativeMatrix matrix in which the block of derivatives is inserted
* @param[in] stageIndex index of the stage to which the constraint derivatives belong
* @param[in] varIndices indices of the parameters which gradients are to be collected
*/
void OptimizerMultiStageMetaData::collectSingleStageGradients(CsTripletMatrix::Ptr globalDerivativeMatrix,
                                                              const unsigned stageIndex,
                                                              std::vector<unsigned> &varIndices) const
{
  // col index starts with sum of decision variables of all previous stages
  int curColIndex = 0;
  // row index starts with sum of constraints of all previous stages
  int curRowIndex = 0;
  for(unsigned j=0; j<stageIndex; j++){
    OptimizerProblemDimensions optProbDimStage = m_optSSMDVec[j]->getOptProbDim();
    curRowIndex += optProbDimStage.numNonLinConstraints;
    curColIndex += optProbDimStage.numOptVars;
  }

  std::vector<IntOptDataMap> ssOptData = m_intMSMD->getIntOptDataStages();
  //** collect gradient information of the single stages **
  CsTripletMatrix::Ptr ssGradients = m_optSSMDVec[stageIndex]->getOptProbDim().nonLinJacDecVar;
  //ssGradients->get_matrix_ptr()->x = new double[ssGradients->get_nnz()];


  const unsigned objectiveIndex = m_optSSMDVec[stageIndex]->getObjectiveData().getEsoIndex();
  OptimizerSingleStageMetaData::retrieveNonLinConstGradients(ssGradients,
                                                             varIndices,
                                                             ssOptData[stageIndex],
                                                             objectiveIndex);
  globalDerivativeMatrix->setSubMatrix(ssGradients, curRowIndex, curColIndex);
}
/**
* @brief unite the decision variable indices of all previous stages
*
* The indices retrieved from stages after the first are increased by the number of parameters
* from stage 0 to stage n-1, n being the index of the stage
* @param[out] decVarIndices vector receiving all decision variable indices
                            - will cleared and resized
* @param[in] stageIndex indices are collected from stage with index 0 to stageIndex-1
*/
void OptimizerMultiStageMetaData::getDecVarIndicesOfPreviousStages
                                          (std::vector<unsigned> &decVarIndices,
                                           const unsigned stageIndex) const
{
  unsigned numParametersPrevStage = 0;
  decVarIndices.clear();
  for(unsigned j=0; j<stageIndex; j++){
    std::vector<unsigned> decVarIndicesStage;
    m_optSSMDVec[j]->getDecisionVariableIndices(decVarIndicesStage);
    for(unsigned k=0; k<decVarIndicesStage.size(); k++){
      decVarIndices.push_back(decVarIndicesStage[k] + numParametersPrevStage);
    }
    numParametersPrevStage += m_optSSMDVec[j]->getOptProbDim().numOptVars;
    numParametersPrevStage += m_optSSMDVec[j]->getOptProbDim().numConstParams;
  }
}

/**
* @brief unite the  constant parameter indices of all previous stages
*
* The indices retrieved from stages after the first are increased by the number of parameters
* from stage 0 to stage n-1, n being the index of the stage
* @param[out] decVarIndices vector receiving all decision variable indices
                            - will cleared and resized
* @param[in] stageIndex indices are collected from stage with index 0 to stageIndex-1
*/
void OptimizerMultiStageMetaData::getConstParamIndicesOfPreviousStages
                                          (std::vector<unsigned> &constParamIndices,
                                           const unsigned stageIndex) const
{
  unsigned numParametersPrevStage = 0;
  constParamIndices.clear();
  for(unsigned j=0; j<stageIndex; j++){
    std::vector<unsigned> constParamIndicesStage;
    m_optSSMDVec[j]->getConstantParameterIndices(constParamIndicesStage);
    for(unsigned k=0; k<constParamIndicesStage.size(); k++){
      constParamIndices.push_back(constParamIndicesStage[k] + numParametersPrevStage);
    }
    numParametersPrevStage += m_optSSMDVec[j]->getOptProbDim().numOptVars;
    numParametersPrevStage += m_optSSMDVec[j]->getOptProbDim().numConstParams;
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinConstraintDerivatives
* @author kafr, tjho
* @brief get the constraint sensitivities
* @param[out] derivativeMatrix csparse matrix containing constraint sensitivities
*/
void OptimizerMultiStageMetaData::getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const
{
  // we assume that the derivativeMatrix is sparse but all non zero elements
  // are allocated
  for(int i=0; i<derivativeMatrix->get_nnz(); i++){
    derivativeMatrix->get_matrix_ptr()->x[i] = 0.0;
  }
  const int numStages = m_intMSMD->getNumStages();
  std::vector<unsigned> decVarIndices;

  for(int i=0; i<numStages; i++){
    const unsigned stageIndex = i;
    //use decision variable indices of all previous stages
    getDecVarIndicesOfPreviousStages(decVarIndices, stageIndex);
    collectMultiStageGradients(derivativeMatrix, stageIndex, decVarIndices);

    // use decision variable indices of current stage
    m_optSSMDVec[i]->getDecisionVariableIndices(decVarIndices);
    collectSingleStageGradients(derivativeMatrix, stageIndex, decVarIndices);
  }
  getDecVarIndicesOfPreviousStages(decVarIndices, numStages);
  unsigned constraintIndexOffset = m_optProbDim.numNonLinConstraints - m_globalConstraints.size();
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
    m_globalConstraints[i]->evaluateConstraintSens(m_optSSMDVec,
                                                   derivativeMatrix,
                                                   decVarIndices,
                                                   constraintIndexOffset + i);
  }
}

/**
* @copydoc OptimizerMetaData::getNonLinConstraintDerivativesConstParam
*/
void OptimizerMultiStageMetaData::getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &derivativeMatrix) const
{
  // we assume that the derivativeMatrix is sparse but all non zero elements
  // are allocated
  for(int i=0; i<derivativeMatrix->get_nnz(); i++){
    derivativeMatrix->get_matrix_ptr()->x[i] = 0.0;
  }
  const int numStages = m_intMSMD->getNumStages();
  std::vector<unsigned> constParamIndices;

  for(int i=0; i<numStages; i++){
    const unsigned stageIndex = i;
    //use decision variable indices of all previous stages
    getConstParamIndicesOfPreviousStages(constParamIndices, stageIndex);
    collectMultiStageGradients(derivativeMatrix, stageIndex, constParamIndices);

    // use decision variable indices of current stage
    m_optSSMDVec[i]->getConstantParameterIndices(constParamIndices);
    collectSingleStageGradients(derivativeMatrix, stageIndex, constParamIndices);
  }
  getConstParamIndicesOfPreviousStages(constParamIndices, numStages);
  unsigned constraintIndexOffset = m_optProbDim.numNonLinConstraints - m_globalConstraints.size();
  for(unsigned i=0; i<m_globalConstraints.size(); i++){
    m_globalConstraints[i]->evaluateConstraintSens(m_optSSMDVec,
                                                   derivativeMatrix,
                                                   constParamIndices,
                                                   constraintIndexOffset + i);
  }
}
/**
* @copydoc OptimizerMetaData::getNonLinObjDerivative
* @author faas, kafr
* @brief get the sensitivities for the objective function
* @param[out] values vector containing the sensitivities for the objective function
*/
void OptimizerMultiStageMetaData::getNonLinObjDerivative(utils::Array<double> &values) const
{
  const int numStages = m_intMSMD->getNumStages();
  std::vector<IntOptDataMap> ssOptData = m_intMSMD->getIntOptDataStages();
  std::vector<IntOptDataMap> msOptData = m_intMSMD->getIntOptData();
  // this variables sums the parameters of the single and multi stage at each stage i
  // it is required to calculate the offset for the variable values
  // we need to collect all derivatives of the objective functions that are added up
    // In more detail, ssOptData contains the derivatives of each stage objective with respect to the parameters
    // of that stage. But the objective function of a stage also depends on parameters/decision variables of previous stages
    // this information is saved in msOptData.
    for(unsigned i=0; i<values.getSize(); i++)
        {
    values[i] = 0.0;
  }
  for(int i=0; i<numStages; i++){
    // vector that contains sensitivity information of both the multi stage and the single stage
    std::vector<double> objDerMS(0);
    if(m_treatObjective[i]){
      objDerMS = assembleMSObjDer(i, msOptData);
      assembleSSObjDer(i, ssOptData, objDerMS);
    }
    // now we write everything we have in the objective gradient vector provided by the user
    for(unsigned j=0; j<objDerMS.size(); j++){
      assert(j<values.getSize());
      values[j] += objDerMS[j];
    }
  }
}


///////////// additions for multi-stage problems //////////////////////////////
/**
* @brief get linear constraint values
*
* the linear constraints consist of the linear constraints of the stages and the total time
* T = sum(t_i, i=0..nStage-1).
*
* @param[out] values values of linear constraints
*/
void OptimizerMultiStageMetaData::getLinConstraintValues(utils::Array<double> &values) const
{
  assert(values.getSize() == m_optProbDim.numLinConstraints);

  unsigned valuesIndex = 0;
  for(unsigned i=0; i<m_optSSMDVec.size(); i++){
    unsigned numLinConstraintsStage = m_optSSMDVec[i]->getOptProbDim().numLinConstraints;
    utils::WrappingArray<double> valuesStage(numLinConstraintsStage,
                                             values.getData() + valuesIndex);
    m_optSSMDVec[i]->getLinConstraintValues(valuesStage);
    valuesIndex += numLinConstraintsStage;
  }
  if(m_totalTimeData.isSet) {
    assert(values.getSize() > valuesIndex);
    values[valuesIndex] = m_intMSMD->getMsDurationVal() - m_totalTimeData.constantDurationsValue;
  }
}

/**
* @brief get bounds of linear constraints
* @param[out] lowerBounds lower bounds of linear constraints
* @param[out] upperBounds upper bounds of linear constraints
* @remarks So far only total time constraint implemented.
*/
void OptimizerMultiStageMetaData::getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                                         utils::Array<double> &upperBounds) const
{
  assert(lowerBounds.getSize() == m_optProbDim.numLinConstraints);
  assert(upperBounds.getSize() == m_optProbDim.numLinConstraints);

  unsigned boundIndex = 0;
  for(unsigned i=0; i<m_optSSMDVec.size(); i++){
    unsigned numLinConstraintsStage = m_optSSMDVec[i]->getOptProbDim().numLinConstraints;
    assert(lowerBounds.getSize() >= numLinConstraintsStage + boundIndex);
    assert(upperBounds.getSize() >= numLinConstraintsStage + boundIndex);
    utils::WrappingArray<double> lowerBoundsStage(numLinConstraintsStage,
                                                  lowerBounds.getData() + boundIndex);
    utils::WrappingArray<double> upperBoundsStage(numLinConstraintsStage,
                                                  upperBounds.getData() + boundIndex);
    m_optSSMDVec[i]->getLinConstraintBounds(lowerBoundsStage, upperBoundsStage);
    boundIndex += numLinConstraintsStage;
  }

  if(m_totalTimeData.isSet) {
    assert(lowerBounds.getSize() > boundIndex);
    assert(upperBounds.getSize() >boundIndex);
    lowerBounds[boundIndex] = m_totalTimeData.lowerBound - m_totalTimeData.constantDurationsValue;
    upperBounds[boundIndex] = m_totalTimeData.upperBound - m_totalTimeData.constantDurationsValue;
  }
}


/**
* @brief get a deep copy of derivatives of linear constraints stored in m_optProbDim.linJac
* @param[out] values values of linear constraints
*/
void OptimizerMultiStageMetaData::getLinConstraintDerivatives(CsTripletMatrix::Ptr linDerivativeMatrix)
{
  assert(linDerivativeMatrix->get_matrix_ptr() == NULL);

//  linDerivativeMatrix = csutil_copy(m_optProbDim.linJac);
  linDerivativeMatrix = CsTripletMatrix::Ptr(new CsTripletMatrix(m_optProbDim.linJac->get_matrix_ptr(),
                                                                  false));

}


/**
* @brief setter for attribute m_totalTimeData
* may be called only once to set the constraint for the total final time. The default is (as set in
* the constructor) that there is no constraint for the total final time. If this method is
* called, a constraint is set and the number of linear constraints is increased by one.
* this function also checks that the total time bounds are consistent with the final time
* bounds on the individual stages
* @param[in] lowerBound lower bound for total time (i.e. sum end times of all stages)
* @param[in] upperBound upper bound for total time (i.e. sum end times of all stages)
*/
void OptimizerMultiStageMetaData::setTotalTimeConstraint(const double lowerBound,
                                                         const double upperBound,
                                                         const double constantDurationsValue)
{
  assert(m_totalTimeData.isSet == false); // make sure the total time is set only once (isSet==false is the
                                          // default value assigned in the constructor)

  assert(lowerBound <= upperBound);

  m_totalTimeData.lowerBound = lowerBound;
  m_totalTimeData.upperBound = upperBound;
  m_totalTimeData.constantDurationsValue = constantDurationsValue;
  m_totalTimeData.isSet = true;

  m_optProbDim.numLinConstraints += 1;
  m_linConstraintsLagrangeMultipliers.push_back(0.0);
  //reassemble linear Jacobian
  assembleLinJacobian();

  // check if total time bounds are consistent with final time bounds on individual stages
  double sumLowerBounds, sumUpperBounds;
  m_intMSMD->getSumOfDurationBounds(sumLowerBounds, sumUpperBounds);
  assert(sumLowerBounds <= upperBound);
  assert(sumUpperBounds >= lowerBound);

}


/** @brief assembles the multistage information of the objective derviative for each stage
*   @param[in] stageIter the current iterate of the stage. This function is within a for loop over all stages.
*   @param[in] msOptData contains the derivative information obtained from the multi stage integrator data
*   @return values of the objective derivative
*   The objective derivative for multi stage has the same size as all previous multi stage parameters.
*   Thus, the vector that is returned contains all values.
*/
std::vector<double> OptimizerMultiStageMetaData::assembleMSObjDer(const int stageIter,
                                                                  const std::vector<IntOptDataMap> msOptData) const
{
  std::vector<double> values(0);
  if(stageIter != 0){ // first stage does not contain multistage information of the objective
    IntOptDataMap::const_iterator msLast = msOptData[stageIter].end();
    msLast--; // we are only interested with the sensitivities at the final time
    const int numParamMS = (*msLast).second.numSensParam;
    const unsigned numConstraints = (*msLast).second.nonLinConstIndices.size();
    std::vector<unsigned> decVarInd;
    unsigned numSensParamsPreviousStage = 0;
            //collect decVarIndicies from all previous and current stage
            for(int i=0; i<stageIter; i++)
                {
      std::vector<unsigned> decVarIndStage;
      m_optSSMDVec[i]->getDecisionVariableIndices(decVarIndStage);
                    for(unsigned j=0; j<decVarIndStage.size(); j++)
                        {
        decVarInd.push_back(decVarIndStage[j]+numSensParamsPreviousStage);
      }
      numSensParamsPreviousStage += m_optSSMDVec[i]->getOptProbDim().numOptVars;
      numSensParamsPreviousStage += m_optSSMDVec[i]->getOptProbDim().numConstParams;
    }

            //for all parameters combined from all previous and the current stage
      for(int j=0; j<numParamMS; j++)
      {
      // copy only values of decision variables
      std::vector<unsigned>::iterator foundIndex;
                    // can the jth active parameter be found in the decision variables?
                    // if not it is not a decision variable and must be skipped.
      foundIndex = std::find(decVarInd.begin(), decVarInd.end(),
                             (*msLast).second.activeParameterIndices[j]);
                    if(foundIndex != decVarInd.end())
                        {
                            //the singe stage objective function can be "custom"
                            //that means it is composed of the "old" objective function and a penalty
                            // f_new=a*f_old+b*penalty(decision_variables of stage i)
                            // Because the integrator computes the gradients with respect to the old objective, we have to add the
                            // factor a manually.
                            // The b*penalty term only contains decision values from within the stage, so this is handeled inside SSOptimizerMetaData, see assembleSSobjDer.
                            if(m_optSSMDVec[stageIter]->getHasCustomObj())
                                {
                                    double factor=m_optSSMDVec[stageIter]->getCustomObjectiveOriginalFactor();
                                    //we know that the objective is the last "constraint"
                                    values.push_back(factor*(*msLast).second.nonLinConstGradients[numParamMS*(numConstraints-1) + j]);
                                }
                            else
                                {
        //we know that the objective is the last "constraint" 
        values.push_back((*msLast).second.nonLinConstGradients[numParamMS*(numConstraints-1) + j]);
                                }
      }
    }
  }
  return values;
}

/** @brief assembles the single stage information of the objective derviative for each stage
*   @param[in] stageIter the current iterate of the stage. This function is within a for loop
*              over all stages.
*   @param[in] ssOptData contains the derivative information obtained from the single stage
*              integrator data
*   @param[out] objDerMs contains the derivatives of the multi stage and is appended by the
*               values of the single stage
*/
void OptimizerMultiStageMetaData::assembleSSObjDer(const int stageIter,
                                                   const std::vector<IntOptDataMap> ssOptData,
                                                   std::vector<double> &objDerMS) const
{
  IntOptDataMap::const_iterator ssLast = ssOptData[stageIter].end();
  ssLast--;
  // the number of sensitivity parameters in the current stage. Excluding the number
  // coming from the multi stage
  const int numParamSS = (*ssLast).second.numSensParam;
  // this vector contains only the derivative of the objective functions of the parameters in stage i
  // so only of the current stage. The derivatieve of the objective to the parameters of the previous stage(s) are currently in objVecMS
  // we will append the vector by the result from objVecSSLocal
  utils::Array<double> objDerSSLocal(numParamSS);
  m_optSSMDVec[stageIter]->getNonLinObjDerivative(objDerSSLocal);

  const unsigned numParamMS = objDerMS.size();
  // append the vector by the local (single stage) sensitivity information of the objective function
  objDerMS.reserve(numParamMS+numParamSS);

  std::vector<unsigned> decVarInd;
  m_optSSMDVec[stageIter]->getDecisionVariableIndices(decVarInd);
  for(int j=0; j<numParamSS; j++){
    // copy only values of decision variables
    std::vector<unsigned>::iterator foundIndex;
    foundIndex = std::find(decVarInd.begin(), decVarInd.end(),
                         (*ssLast).second.activeParameterIndices[j]);
    if(foundIndex != decVarInd.end()){
      objDerMS.push_back(objDerSSLocal[j]);
    }
  }
}

/**
* @brief assemble sparsity data of nonLinJacobian
*
* nonLinJacobian is a sparse matrix having blocks on the diagonal equal to the
* nonLinJacobian structs of the stages. All entries above these blocks are
* structually zero and all entries below are structually nonzero. The dimension
* of the matrix is numNonLinConstraints x numOptVars
*
* @sa OptimizerMulitStageMetaData::m_optProbDim
*/
void OptimizerMultiStageMetaData::assembleNonLinJacobian()
{
  const unsigned numConstraints = m_optProbDim.numNonLinConstraints;
  const unsigned numParameters = m_optProbDim.numOptVars;

  // in multi stage we get a lower triangular block structure with the sparse nonLinJac structures
  // on the diagonal and full sub-diagonals, so the number of nonzeroes must be calculated via the 
  // old stages
  unsigned numNonZeroes = 0;
  const unsigned numStages = m_intMSMD->getNumStages();
  
  // determine the nnz
  // 1) diagonal blocks (sparse)
  numNonZeroes = 0;
  for (unsigned i=0; i<numStages; i++) {
    OptimizerProblemDimensions stageOptProbDim = m_optSSMDVec[i]->getOptProbDim();
    numNonZeroes += stageOptProbDim.nonLinJacDecVar->get_nnz(); 
  }

  // 2) subdiagonal blocks (full)
  //number of parameters in all lower stages
  unsigned numStageParameters = m_optSSMDVec[0]->getOptProbDim().numOptVars;
  std::vector<Mapping::Ptr> mappings = m_intMSMD->getMappings();
  for (unsigned i=1; i<numStages; i++) {
    OptimizerProblemDimensions stageOptProbDim = m_optSSMDVec[i]->getOptProbDim();
    //if stages are decoupled, no mixed sensitivities are calculated
    if (!mappings[i-1]->isDecoupled()) {
      numNonZeroes += numStageParameters * stageOptProbDim.numNonLinConstraints;
    }
    numStageParameters += stageOptProbDim.numOptVars;
  }
  


  utils::Array<double> values(numNonZeroes, 0.0);
  utils::Array<int> rowIndices(numNonZeroes, 0);
  utils::Array<int> colIndices(numNonZeroes, 0);

  // fill in the lower triangular block structure
  // 1) diagonal blocks (sparse)
  unsigned rowOffset = 0; 
  unsigned colOffset = 0;
  unsigned numNonZeroesStage = 0;
  unsigned counter = 0;
  int *rowIndicesStage, *colIndicesStage;
  for (unsigned i=0; i<numStages; i++) {
    OptimizerProblemDimensions stageOptProbDim = m_optSSMDVec[i]->getOptProbDim();
    numNonZeroesStage = stageOptProbDim.nonLinJacDecVar->get_nnz();
    rowIndicesStage = stageOptProbDim.nonLinJacDecVar->get_matrix_ptr()->i;
    colIndicesStage = stageOptProbDim.nonLinJacDecVar->get_matrix_ptr()->p;
    for (unsigned j=0; j<numNonZeroesStage; j++) {
      rowIndices[counter] = rowIndicesStage[j] + rowOffset;
      colIndices[counter] = colIndicesStage[j] + colOffset;
      counter++;
    }
    rowOffset += stageOptProbDim.nonLinJacDecVar->get_n_rows(); 
    colOffset += stageOptProbDim.nonLinJacDecVar->get_n_cols();
  }
  
  // 2) subdiagonal blocks (full) excluding decoupled stages
  OptimizerProblemDimensions stageOptProbDim = m_optSSMDVec[0]->getOptProbDim();
  rowOffset = stageOptProbDim.numNonLinConstraints;
  numStageParameters = stageOptProbDim.numOptVars;
  for (unsigned i=1; i<numStages; i++) {
    stageOptProbDim = m_optSSMDVec[i]->getOptProbDim();
    if(! mappings[i-1]->isDecoupled()){
      const unsigned numStageConstraints = stageOptProbDim.numNonLinConstraints;
      for (unsigned j=0; j<numStageConstraints; j++) {
        for (unsigned k=0; k<numStageParameters; k++) {
          rowIndices[counter] = j + rowOffset;
          colIndices[counter] = k;
          counter++;
        }
      }
    }
    rowOffset += stageOptProbDim.numNonLinConstraints;
    numStageParameters += stageOptProbDim.numOptVars;
  }
 //Do not be confused by values never getting changed from zero. In contrast to 
 //"assembleLinJacobian", function "assembleNonLinJacobian" does only assemble 
 //the sparsity pattern.
  m_optProbDim.nonLinJacDecVar = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(numConstraints,
                                                                              numParameters,
                                                                              numNonZeroes,
                                                                              rowIndices.getData(),
                                                                              colIndices.getData(),
                                                                              values.getData()));

}
/**
* @brief assemble data of linJacobian
*
* linJacobian is a sparse matrix with dimensions numLinConstraints x numOptVars.
* here a dummy jacobian is constructed with numLinConstraints = 0.
* The actual structure and values are later assembled in the optimizer factory.
*/
void OptimizerMultiStageMetaData::assembleLinJacobian()
{
  // this is a call from the OptimizerMultiStageMetaData constructor: returns a
  // dummy linJac of size 0 x numNonLinConstraints
  if(m_optProbDim.linJac.get() == NULL) {
    const unsigned numConstraints = m_optProbDim.numLinConstraints;
    const unsigned numParameters = m_optProbDim.numOptVars;
    unsigned numNonZeroes = 0;
    const unsigned numStages = m_optSSMDVec.size();
    for(unsigned i=0; i<numStages; i++){
      numNonZeroes += m_optSSMDVec[i]->getOptProbDim().linJac->get_nnz();
    }

    utils::Array<int> rowIndices(numNonZeroes);
    utils::Array<int> colIndices(numNonZeroes);
    utils::Array<double> values(numNonZeroes);
    
  //merge all linear matrices into linJac
    unsigned rowOffset = 0;
    unsigned colOffset = 0;
    unsigned entryIndex = 0;
    for(unsigned i=0; i<numStages; i++){
      CsTripletMatrix::Ptr linJacStage = m_optSSMDVec[i]->getOptProbDim().linJac;
      assert(linJacStage->get_nnz()>=0); //expected to be in coo format
      for(int j=0; j<linJacStage->get_nnz(); j++, entryIndex++){
        assert(entryIndex < numNonZeroes);
        rowIndices[entryIndex] = linJacStage->get_matrix_ptr()->i[j] + rowOffset;
        colIndices[entryIndex] = linJacStage->get_matrix_ptr()->p[j] + colOffset;
        values[entryIndex] = linJacStage->get_matrix_ptr()->x[j];
      }
      colOffset += linJacStage->get_n_cols();
      rowOffset += linJacStage->get_n_rows();
    }
    
    m_optProbDim.linJac = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(numConstraints,
                                                                         numParameters,
                                                                         numNonZeroes,
                                                                         rowIndices.getData(),
                                                                         colIndices.getData(),
                                                                         values.getData()));
  }
  // this is a call from setTotalTimeConstraint: one line is appended to a previously existing linJac
  else if(m_totalTimeData.isSet){
    assert(m_optProbDim.linJac->get_nnz() > -1); // matrix in coo format expected
    // make a copy of existing linJac
    const unsigned numConstraints = m_optProbDim.numLinConstraints;
    const unsigned numParameters = m_optProbDim.numOptVars;
    const unsigned numNonZeroes = m_optProbDim.linJac->get_nnz();
    
    int newNumNonZeroes = 2*numNonZeroes;
    utils::Array<int> rowIndices(newNumNonZeroes);
    utils::Array<int> colIndices(newNumNonZeroes);
    utils::Array<double> values(newNumNonZeroes);

    
    // insert old linJac
    for(unsigned i=0; i<numNonZeroes; i++) {
      rowIndices[i] = m_optProbDim.linJac->get_matrix_ptr()->i[i];
      colIndices[i] = m_optProbDim.linJac->get_matrix_ptr()->p[i];
      values[i] = m_optProbDim.linJac->get_matrix_ptr()->x[i];
    }
    // add line for total time constraint

    // assert that in the linear Jacobian are free final time parameters only,
    // so free final time indices are all current indices of the linear Jacobian.
    // In oldLinJac there is one  linear constraint for each free final time,
    // so we need to add the same number of nonzeroes for the global final time constraint
    const int currRowInd = m_optProbDim.numLinConstraints - 1;
    const double val = 1.0; // dT/dt_j = d(sum(t_i, i=0, nStage-1))/dt_j is '1.0' in
                            // stage j (if time is free), '0.0' else
    int offsetCSmat = numNonZeroes;
    for(unsigned i=0; i<numNonZeroes; i++, offsetCSmat ++) {
      rowIndices[offsetCSmat] = currRowInd;
      colIndices[offsetCSmat] = m_optProbDim.linJac->get_matrix_ptr()->p[i];
      values[offsetCSmat] = val;
    }
    
    CsTripletMatrix::Ptr newLinJac = CsTripletMatrix::Ptr(new SortedCsTripletMatrix(numConstraints,
                                                                              numParameters,
                                                                              newNumNonZeroes,
                                                                              rowIndices.getData(),
                                                                              colIndices.getData(),
                                                                              values.getData()));
    m_optProbDim.linJac = newLinJac;
  }
}
/**
* @brief getter for structure StageOutput
*
* @param[in] StageInput structure
* @param[in] StageOutput structure
* @param[in] OptimizerOutput structure
*/
std::vector <DyosOutput::StageOutput>  OptimizerMultiStageMetaData::getOutput()
{
  std::vector<DyosOutput::StageOutput> output = m_intMSMD->getOutput();
  assert(output.size() == m_optSSMDVec.size());
  for(unsigned i=0; i<output.size(); i++)
  {
    output[i].optimizer = m_optSSMDVec[i]->getOutput().front().optimizer;
  }
  return output;
}


OptimizerMSMetaData2ndOrder::OptimizerMSMetaData2ndOrder
                             (std::vector<OptimizerSingleStageMetaData::Ptr> optSSMDVec,
                              IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                              std::vector<bool> treatObjective)
                              : OptimizerMultiStageMetaData(optSSMDVec, intMSMD, treatObjective)
{
  m_integratorMetaData2ndOrder = intMSMD;
}

OptimizerMSMetaData2ndOrder::~OptimizerMSMetaData2ndOrder()
{
}
void OptimizerMSMetaData2ndOrder::getNonLinConstraintHessian(CsTripletMatrix::Ptr &hessianMatrix,
                                                             const std::vector<unsigned> &indices) const
{

    // Second order custom objective can not be implemented without major refactoring and rework, since
    // every stage can have a different objective, but Lagrangian is here not split up in the different stages.
    // Second order + custom objective is checked and prevented in user input. So at this point it would be a implementation bug 
	// if we reach this code portion with a custom objective.
    for(size_t stage=0; stage < this->m_optSSMDVec.size(); stage++)
        {
            assert(!this->m_optSSMDVec[stage]->getHasCustomObj());
        }
  std::vector<double> lagrangeDers, lagrangeDers2ndOrder;
  m_integratorMetaData2ndOrder->getLagrangeDers(lagrangeDers);
  const unsigned numSensParam = m_intMSMD->getNumSensitivityParameters();
    //lagrangeDers has [0.. numSensParam-1] first order and second order begins after numSensParam.
  lagrangeDers2ndOrder.assign(lagrangeDers.begin() + numSensParam, lagrangeDers.end());
  std::vector<unsigned> decVarIndices;
  this->getDecVarIndicesOfPreviousStages(decVarIndices, static_cast<unsigned>(m_optSSMDVec.size()));
  OptimizerSSMetaData2ndOrder::extractValuesToMatrix(hessianMatrix,
                                                     indices,
                                                     decVarIndices,
                                                     numSensParam,
                                                     numSensParam,
                                                     lagrangeDers2ndOrder);
  /*OptimizerSSMetaData2ndOrder::extractValuesToMatrix(hessianMatrix,
                                                     indices,
                                                     numSensParam,
                                                     numSensParam,
                                                     lagrangeDers2ndOrder);*/
}


/**
* @copydoc OptimizerMetaData2ndOrder::getNonLinConstraintHessionDecVar
*/
void OptimizerMSMetaData2ndOrder::getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &hessianMatrix) const
{
  std::vector<unsigned> decisionVariableIndices;
  getDecVarIndicesOfPreviousStages(decisionVariableIndices, m_optSSMDVec.size());
  getNonLinConstraintHessian(hessianMatrix, decisionVariableIndices);
}
void OptimizerMSMetaData2ndOrder::getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &hessianMatrix) const
{
  std::vector<unsigned> constantParameterIndices;
  getConstParamIndicesOfPreviousStages(constantParameterIndices, m_optSSMDVec.size());

  getNonLinConstraintHessian(hessianMatrix, constantParameterIndices);
}
void OptimizerMSMetaData2ndOrder::getAdjoints(std::vector<double> &adjoints1stOrder,
                                              CsTripletMatrix::Ptr &adjoints2ndOrder,
                                              const std::vector<unsigned> &indices) const
{
  const unsigned numStates = m_intMSMD->getGenericEso()->getNumStates();
  const unsigned numSensParam = m_intMSMD->getNumSensitivityParameters();

  std::vector<double> adjoints;
  m_integratorMetaData2ndOrder->getAdjoints(adjoints);

  assert(adjoints.size() == numStates * (1 + numSensParam));

  adjoints1stOrder.assign(adjoints.begin(), adjoints.begin() + numStates);

  std::vector<double> adjoints2ndOrderVec;
  adjoints2ndOrderVec.assign(adjoints.begin() + numStates, adjoints.end());
  
  std::vector<unsigned> rowIndices(numStates);
  for(unsigned i=0; i<numStates; i++){
    rowIndices[i] = i;
  }

  OptimizerSSMetaData2ndOrder::extractValuesToMatrix(adjoints2ndOrder,
                                                     indices,
                                                     rowIndices,
                                                     numStates,
                                                     numSensParam,
                                                     adjoints2ndOrderVec);
}
void OptimizerMSMetaData2ndOrder::getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                                    CsTripletMatrix::Ptr &adjoints2ndOrder) const
{
  std::vector<unsigned> decisionVariableIndices;
  getDecVarIndicesOfPreviousStages(decisionVariableIndices, m_optSSMDVec.size());

  getAdjoints(adjoints1stOrder, adjoints2ndOrder, decisionVariableIndices);
}
void OptimizerMSMetaData2ndOrder::getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                                        CsTripletMatrix::Ptr &adjoints2ndOrder) const
{
  std::vector<unsigned> constantParameterIndices;
  getConstParamIndicesOfPreviousStages(constantParameterIndices, m_optSSMDVec.size());

  getAdjoints(adjoints1stOrder, adjoints2ndOrder, constantParameterIndices);
}
