/**
* @file OptimizerSingleStageMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Data - Part of DyOS                                             \n
* =====================================================================\n
* This file contains the OptimizerSingleStageMetaData class            \n
* according to MetaEso8.vsd                                            \n
* =====================================================================\n
* @author Tjalf Hoffmann, Aron Zingler
* @date 14.09.2019
*/

#pragma once

#include "OptimizerMetaData.hpp"
#include "IntegratorSingleStageMetaData.hpp"
#include "Constraints.hpp"
#include "Control.hpp"
#include "UtilityFunctions.hpp"

#include "had.h"
using ADdouble =had::AReal;
using ADdouble =UserInput::ADdouble;

//#include <boost/shared_ptr.hpp>
#include <memory>

/**
* @class OptimizerSingleStageMetaData
* @brief class implementing a single stage case of the OptimizerMetaData interface
*
* @author Kathrin Frankl, Fady Assassa, Tjalf Hoffmann
* @date 15.12.2011
*/
class OptimizerSingleStageMetaData : virtual public OptimizerMetaData
{
public:
  typedef std::shared_ptr<OptimizerSingleStageMetaData> Ptr;

  DyosOutput::OptimizerStageOutput  getStageOutput();
protected:
  //! point constraints for the states
  std::vector<StatePointConstraint> m_constraints;
  //! variable representing the value of the objective function
  StatePointConstraint m_objective;
    //! If a modified objective is to be considered
    bool m_hasCustomObj=false;
    //! An optional user given function that computes the penalty for a new objective function to be used instead of m_objetive
    //! Has the general form weight_old*m_objective+weight_penalty*g(p) where p are some requested control parametrization parameters
    std::function<ADdouble(std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<ADdouble>>)>m_customObjPenalty;
    //! Factor to be inserted for weight_old in the formula of the new objective function
    double m_customObjPenaltyFactor;//default=0;
    //! Factor to be inserted for weight_penalty in the formula of the new objective function
    double m_customObjOriginalFactor;//default=1;
    //! A list of all parameterization values that are needed in the custom objective function (in the penalization)
    //! Each entry is a tuple:
    //! The first entry is the variable name as in the genericEso
    //! The second entry is the index of the parametrization grid
    //! The third  entry is the numbers of parameters that are in tha parametrization grid ( only whole parametrizationGrids can be requested) and is used to check userInput errors
    std::vector<std::tuple<std::string /*varName*/, unsigned /*gridNum*/,unsigned /*numOfParameters*/>> m_customObjectiveFunctionRequestedListOfParameterization;

  std::vector<double> m_linConLagrangeMultiplier;

  //! pointer to the corresponding IntegratorMetaData object
  IntegratorSingleStageMetaData::Ptr m_integratorMetaData;

  //help functions
  virtual void calculateNonLinJacDecVarStructure(IntOptDataMap &intOptData);
  virtual void calculateNonLinJacConstParStructure();
  virtual void calculateLinJacStructure();

public:
    //extract the values of control parameterization for given list of variable names and grid numbers
    std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<ADdouble>> extractValuesOfParameterization(const std::vector<std::tuple<std::string /*variableName*/, unsigned /*gridNum*/,unsigned /*numOfParameters*/>>& requestListParameterization) const;
    //extract Control Parameters for given list of variable names and grid numbers
    std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<ControlParameter::Ptr>> extractParameterization(const std::vector<std::tuple<std::string /*variableName*/, unsigned /*gridNum*/,unsigned /*numOfParameters*/>>& requestListParameterization) const;
  OptimizerSingleStageMetaData();
  OptimizerSingleStageMetaData(const IntegratorSingleStageMetaData::Ptr &integratorMetaData);
  /** @brief standard empty destructor */
  virtual ~OptimizerSingleStageMetaData(){};

  // methods for factory use only.
  virtual void setConstraints(const std::vector<StatePointConstraint> &constraints);
  virtual void setObjective(const StatePointConstraint &objective);
    //sets new obbjective = originalFactor*originalObjectiveFunction+penaltyFactor*penaltyTerm(as a function of parametrization);
    virtual void setAndEnableCustomObjective(double originalFactor,double penaltyFactor,const std::vector<std::tuple<std::string, unsigned, unsigned> >& customObjectiveFunctionRequestedListOfParameterization, std::function<ADdouble(std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<ADdouble>/*the parameterization values in order*/>)>customObjPenalty);
    virtual void setCustomObjectivePenaltyFactor(double penaltyFactor);
    virtual void setCustomObjectiveOriginalFactor(double originalFactor);
    //specifies a list of parameterization to be considered in the customObjective. Parameterizations are always 'loaded/requested' in whole grids. The syntax is variable name, gridNumber, number of intervals in that grid.
    virtual void setCustomObjectiveRequestedListOfParameterization(const std::vector<std::tuple<std::string, unsigned, unsigned> >& customObjectiveFunctionRequestedListOfParameterization);
    double getCustomObjectiveOriginalFactor() const;
    bool getHasCustomObj() const;
  virtual void calculateOptimizerProblemDimensions();

  //methods of the OptimizerMetaData interface
  virtual void getDecVarBounds(utils::Array<double> &lowerBounds,
                               utils::Array<double> &upperBounds) const;
  virtual void getDecVarValues(utils::Array<double> &values) const;
  virtual void setDecVarValues(const utils::Array<const double> &values);
  virtual void getNonLinConstraintBounds(utils::Array<double> &lowerBounds,
                                         utils::Array<double> &upperBounds) const;
  virtual void getNonLinConstraintValues(utils::Array<double> &values) const;
  virtual void getNonLinConstraintDerivativesDecVar(CsTripletMatrix::Ptr &derivativeMatrix) const;
  virtual void getNonLinConstraintDerivativesConstParam(CsTripletMatrix::Ptr &nonLinConstGradients) const;
  virtual double getNonLinObjValue() const;
    double getModelNonLinObjValue() const;
  virtual void getNonLinObjDerivative(utils::Array<double> &values) const;
  virtual void setLagrangeMultipliers(const utils::Array<double> &nonLinMultipliers,
                                      const utils::Array<double> &linMultipliers);
  virtual void getLagrangeMultipliers(utils::Array<double> &nonLinMultipliers,
                                      utils::Array<double> &linMultipliers) const;
  virtual void setDecVarLagrangeMultipliers(const utils::Array<double> &lagrangeMultiplier);
  virtual void setObjectiveLagrangeMultiplier(const double lagrangeMultiplier);
  virtual void getLinConstraintValues(utils::Array<double> &values) const;
  virtual void getLinConstraintBounds(utils::Array<double> &lowerBounds,
                                      utils::Array<double> &upperBounds) const;
  virtual void addConstraint(StatePointConstraint &constraint, unsigned constraintIndex);

  std::vector<IntOptDataMap> getIntegrationOptData() const;

  virtual StatePointConstraint getObjectiveData(void);
  
  
  virtual void getDecisionVariableIndices(std::vector<unsigned> &decVarInd) const;
    virtual void getDecisionVariablesEsoIndices(std::vector<int> &decVarEsoInd) const;
  virtual void getConstantParameterIndices(std::vector<unsigned> &constParamInd) const;
  
  
  //static help functions (also used by other modules (eq OptimizerMultiStageMetaData)
  static void retrieveNonLinConstGradients(CsTripletMatrix::Ptr nonLinConstGradients,
                                           std::vector<unsigned> &varIndices,
                                           IntOptDataMap &intOptData,
                                           const unsigned objectiveIndex);
  static void constructFullCooMatrix(CsTripletMatrix::Ptr &matrix, const unsigned numRows, const unsigned numCols);
  static void constructSparseCooMatrix(CsTripletMatrix::Ptr &matrix,
                                       const std::vector<int> rowIndices,
                                       const std::vector<int> colIndices,
                                       const unsigned numRows,
                                       const unsigned numCols);
  
  std::vector <DyosOutput::StageOutput> getOutput();
};

class OptimizerSSMetaData2ndOrder : public virtual OptimizerMetaData2ndOrder,
                                    public virtual OptimizerSingleStageMetaData
{
public:
  typedef std::shared_ptr<OptimizerSSMetaData2ndOrder> Ptr;
  OptimizerSSMetaData2ndOrder(const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData);
  virtual ~OptimizerSSMetaData2ndOrder();

protected:
  OptimizerSSMetaData2ndOrder(){}
  
  IntegratorSSMetaData2ndOrder::Ptr m_integratorMetaData2ndOrder;
  
  virtual void getNonLinConstraintHessian(CsTripletMatrix::Ptr &hessianMatrix,
                                          const std::vector<unsigned> &indices) const;
  virtual void getAdjoints(std::vector<double> &adjoints1stOrder,
                           CsTripletMatrix::Ptr &adjoints2ndOrder,
                           std::vector<unsigned> &indices) const;

public:
    /// This is currently disabled, because the hessian of the objective would need to change, however, currently we do not have the hessian of the objective seperate from the hessian of the constraints, but only the hessian of the lagrangian.
    /// To enable custom objective for second order optimization, the adjoint calculation of the objective hessian needs to be requested from NIXE (currently the only second order integrator) and the information has to be passed throught.
    /// This would require extensive changes in integratorMetaData (see setAdjointsAndLagrangeDerivatives) the second stage Nixe interfaces.
    /// Note: LagrangeDerivatives seem to partially mean the second derivatives in the second order integrators.
    /// If we did not want to use the original objective as part of our new objective, then setting the lagrange multiplier to zero would be an option.
    virtual void setAndEnableCustomObjective(double originalFactor,double penaltyFactor,const std::vector<std::tuple<std::string, unsigned, unsigned> >& customObjectiveFunctionRequestedListOfParameterization, std::function<ADdouble(std::map<std::pair<std::string /*variableName*/,unsigned/*gridNum*/>,std::vector<ADdouble>/*the parameterization values in order*/>)>customObjPenalty);
  virtual void getNonLinConstraintHessianDecVar(CsTripletMatrix::Ptr &hessianMatrix) const;
  virtual void getNonLinConstraintHessianConstParam(CsTripletMatrix::Ptr &hessianMatrix) const;
  virtual void getAdjointsDecVar(std::vector<double> &adjoints1stOrder,
                                 CsTripletMatrix::Ptr &adjoints2ndOrder) const;
  virtual void getAdjointsConstParam(std::vector<double> &adjoints1stOrder,
                                     CsTripletMatrix::Ptr &adjoints2ndOrder) const;

  static void extractValuesToMatrix(CsTripletMatrix::Ptr &matrix,
                                    const std::vector<unsigned> &indices,
                                    const unsigned numRows,
                                    const unsigned numColsTotal,
                                    const std::vector<double> &values);
                                    
  static void extractValuesToMatrix(CsTripletMatrix::Ptr &matrix,
                                  const std::vector<unsigned> &colIndices,
                                  const std::vector<unsigned> &rowIndices,
                                  const unsigned numRows,
                                  const unsigned numColsTotal,
                                  const std::vector<double> &values);
  //static void getAdjoints
};
