/** 
* @file ParameterizationGrid.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaSingleStageEso - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the classdefinitions of the ParameterizationGrid  \n
*                          according to the UML Diagramm METAESO_2     \n
* =====================================================================\n
* @author Fady Assassa, Aron Zingler
* @date 14.09.2019
*/
#pragma once
#include <vector>
#include <boost/shared_ptr.hpp>
#include "Parameter.hpp"
#include "MetaDataOutput.hpp"
#include "MetaDataExtern.hpp"
#include "GenericEso.hpp"
#include "DyosObject.hpp"

/** 
* @enum ControlType
* @brief enumeration of possible control types
*/
enum ControlType
{
  PieceWiseConstant = 1,
  PieceWiseLinear = 2
};


/**
* @class ParameterizationGrid
*
* @brief Defining the ParameterizationGrid required by the control class of the optimization problem
*
* This is the ParameterizationGrid for the grids from 1 to n-1. The last Grid hast to be of type 
* LastParameterizationGrid, since the DurationParameter is handeld outside of the scope of the 
* ParameterizationGrid or rather the Control.
*/
class ParameterizationGrid : public DyosObject
{
protected:
  //! discretization grid
  std::vector<double> m_discretizationGrid;
    //! flag to signify if individual bounds have been set
    bool m_hasIndividualParameterBounds=false;
  //! path constraint resolution (PCRESOLUTION)
  int m_pcresolution;
  //! discretization type
  ControlType m_controlApproximation;
  //! parameterization of the Grid (this needs to be changed to Array?)
  std::vector<ControlParameter::Ptr> m_parameterization;
  //! final time parameter which can be free for optimization
  DurationParameter::Ptr m_duration;
  //! current active ControlParameter;
  vector<unsigned> m_activeParameterIndices;

  int getControlOrder() const;
public:
  typedef boost::shared_ptr<ParameterizationGrid> Ptr;
  ParameterizationGrid();
  // constructor for equidistant discretization
  ParameterizationGrid(const unsigned numberOfIntervals, 
                       const ControlType controlApproximation);

  ParameterizationGrid(const ParameterizationGrid *pg);
  // destructor
  virtual ~ParameterizationGrid();

  // setters
  void setAllParameterBounds(const double lowerbound,const double upperbound);
    void setAllParameterBoundsIndividually(const std::vector<double>& lowerBounds,const std::vector<double>& upperBounds);
  void setDiscretizationGrid(const std::vector<double> discretizationGrid); 
  void setControlType(const ControlType controlApproximation);
    // required for integration
  void setActiveParameterIndex(const vector<unsigned> activeIndices);
  void setAllParametersWithoutSens(void);
  void setAllParametersNotDecisionVars(void);
  void setAllParameterizationValues(const std::vector<double> values);
  void setAllControlParameter(const std::vector<ControlParameter::Ptr> &parameter);
  void setPCResolution(const int resolution);
  

  void refineGrid();
  // getters
  double getDurationValue(void) const;

    bool getHaveIndividualBoundsBeenSet()const;
  int getPCresolution(void) const;
  std::vector<double> getDiscretizationGrid(void)const;
  std::vector<double> getConstraintGrid(void)const;
  ControlType getControlType(void)const;
    // required for integration
  vector<unsigned> getActiveParameterIndex(void)const;
  ControlParameter::Ptr getControlParameter(const unsigned index) const;
  std::vector<ControlParameter::Ptr> getAllControlParameter(void)const;
  DurationParameter::Ptr getDurationPointer(void) const;

  double evalSpline(const double timePoint);
    std::vector<unsigned> findParametersResponsibleForTimePoint(const double localTimePoint) const;
    double evalSplineBetweenParameters(const double timePoint, std::vector<unsigned> parametersBetweenWhichToEvaluate= {}) const;
  double evaldSpline(const double timePoint, unsigned paramIndex);

  // virtual functions
  virtual void setDurationValue(const double);
  /** @brief This function does nothing for the ParameterizationGrid*/
    virtual void setDurationPointer(const DurationParameter::Ptr &duration)
    {
        m_duration = duration;
    };
  DyosOutput::ParameterGridOutput createParameterGridOutput(IntOptDataMap iODP,
                                                            GenericEso::Ptr &genEso);
};

class OriginalParameterizationGrid : public ParameterizationGrid
{
  protected:
    std::vector<std::pair<unsigned,ParameterizationGrid::Ptr> > m_parameterizationGrids;

  public:
    typedef boost::shared_ptr<OriginalParameterizationGrid> Ptr;
    OriginalParameterizationGrid(const ParameterizationGrid* pg):ParameterizationGrid(pg){};
    void update();
    double evaldSpline(const double originalLocalTime,
                       const unsigned paramIndex,
                       const vector<unsigned> activeIndices)const;
    double getSplineDerivative(const vector<unsigned> &activeIndices)const;
    void insertParameterizationGrid(const unsigned gridIndex,
                                    const ParameterizationGrid::Ptr insertedGrid1,
                                    const ParameterizationGrid::Ptr insertedGrid2 = boost::shared_ptr<ParameterizationGrid>());
    void changeIndices();
    unsigned getNumParameterizationGrids()const;
    unsigned getNumParameters()const;
    double sumUpDurations(const unsigned gridIndex)const;
};
