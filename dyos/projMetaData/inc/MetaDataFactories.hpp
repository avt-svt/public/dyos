/** 
* @file MetaDataFactories.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaData                                                             \n
* =====================================================================\n
* This file contains the declarations of factories for classes         \n
* IntegratorMetaData, OptimizerMetaData, Parameter and Control         \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 15.02.2012
*/
#pragma once

#include <boost/shared_ptr.hpp>

#include "MetaDataInput.hpp"
#include "GenericEsoFactory.hpp"
#include "Control.hpp"
#include "Parameter.hpp"
#include "IntegratorMetaData.hpp"
#include "IntegratorSingleStageMetaData.hpp"
#include "IntegratorMultiStageMetaData.hpp"
#include "OptimizerSingleStageMetaData.hpp"
#include "OptimizerMultiStageMetaData.hpp"
#include "Constraints.hpp"
#include "OptimizerMetaData.hpp"
#include "Mapping.hpp"
#include "DyosObject.hpp"

/**
* @class ControlFactory
* @brief factory class for creating Control objects
*/
class ControlFactory : public DyosObject
{
public:
  Control createControl(const FactoryInput::ParameterInput &input) const;
  Control createTimeInvariantParameter(const FactoryInput::ParameterInput &input) const;
  std::vector<Control> createControlVector
                      (const std::vector<FactoryInput::ParameterInput> &inputVector) const;
protected:
  FactoryInput::ParameterInput mergeInputGridsIntoOne
                      (const FactoryInput::ParameterInput &input) const;
};

/**
* @class ParameterFactory
* @brief factory class for creating Parameter objects
*/
class ParameterFactory : public DyosObject
{
protected:
  void setSensTypeInformation(Parameter::Ptr p,
                        const FactoryInput::ParameterInput::ParameterSensitivityType sensType) const;
public:
  DurationParameter::Ptr createDurationParameter(const FactoryInput::ParameterInput &input) const;
  InitialValueParameter::Ptr createInitialValueParameter(const FactoryInput::ParameterInput &input,
                                                     const GenericEso::Ptr &genEso) const;
  std::vector<InitialValueParameter::Ptr> createInitialValueParameterVector
                                (const std::vector<FactoryInput::ParameterInput> &inputVector,
                                 const GenericEso::Ptr &genEso) const;
 };

/**
* @class MappingFactory
* @brief factory class for creating Mapping objects
*/
class MappingFactory : public DyosObject
{
public:
  Mapping::Ptr createMapping(const struct FactoryInput::MappingInput &input)const;
  std::vector<Mapping::Ptr> createMappingVector
                            (const std::vector<FactoryInput::MappingInput> &inputVector)const;
  SingleShootingMapping::Ptr createSingleShootingMapping
                            (const struct FactoryInput::MappingInput &input)const;
  MultipleShootingMapping::Ptr createMultipleShootingMapping
                            (const struct FactoryInput::MappingInput &input)const;
};

/**
* @class IntegratorMetaDataFactory
* @brief factory class for creating IntegratorMetaData objects
*/
class IntegratorMetaDataFactory : public DyosObject
{
protected:
  std::vector<std::vector<double> > m_problemGrid;
  
  void setProblemStageGrid(const std::vector<Control> &controls, const double duration,
    const std::vector<double> &userGrid);

public:
  IntegratorMetaData::Ptr createIntegratorMetaData
                            (const struct FactoryInput::IntegratorMetaDataInput &input);
  IntegratorMetaData2ndOrder::Ptr createIntegratorMetaData2ndOrder
                            (const struct FactoryInput::IntegratorMetaDataInput &input);
  IntegratorSingleStageMetaData::Ptr createIntegratorSingleStageMetaData
                            (const struct FactoryInput::IntegratorMetaDataInput &input);
  IntegratorSSMetaData2ndOrder::Ptr createIntegratorSSMetaData2ndOrder
                            (const struct FactoryInput::IntegratorMetaDataInput &input);
  IntegratorSingleStageMetaData::Ptr createIntegratorSingleStageMetaData
                            (const struct FactoryInput::IntegratorMetaDataInput &input,
                             const ControlFactory &controlFactory,
                             const ParameterFactory &paramFactory,
                             const GenericEsoFactory &genEsoFactory);
  IntegratorSSMetaData2ndOrder::Ptr createIntegratorSSMetaData2ndOrder
                            (const struct FactoryInput::IntegratorMetaDataInput &input,
                             const ControlFactory &controlFactory,
                             const ParameterFactory &paramFactory,
                             const GenericEsoFactory &genEsoFactory);
  IntegratorSingleStageMetaData::Ptr createIntegratorSingleStageMetaData
                            (const std::vector<Control> &controls,
                             const std::vector<InitialValueParameter::Ptr> &initials,
                             const DurationParameter::Ptr &globalDuration,
                             const std::vector<double> &userGrid,
                             const GenericEso::Ptr &genEso,
                             const unsigned plotGridResolution,
                             const std::vector<double> &explicitPlotGrid);
  IntegratorSSMetaData2ndOrder::Ptr createIntegratorSSMetaData2ndOrder
                            (const std::vector<Control> &controls,
                             const std::vector<InitialValueParameter::Ptr> &initials,
                             const DurationParameter::Ptr &globalDuration,
                             const std::vector<double> &userGrid,
                             const GenericEso2ndOrder::Ptr &genEso,
                             const unsigned plotGridResolution,
                             const std::vector<double> &explicitPlotGrid);
  std::vector<IntegratorSingleStageMetaData::Ptr> createIntegratorSingleStageMetaDataVector
                            (const std::vector<FactoryInput::IntegratorMetaDataInput> &input);
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> createIntegratorSSMetaData2ndOrderVector
                            (const std::vector<FactoryInput::IntegratorMetaDataInput> &input);

  IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaDataWithSingleStage
                            (const struct FactoryInput::IntegratorMetaDataInput &input);
  IntegratorMSMetaData2ndOrder::Ptr createIntegratorMSMetaData2ndOrderWithSingleStage
                            (const struct FactoryInput::IntegratorMetaDataInput &input);

  IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaDataWithSingleStage
                            (const IntegratorSingleStageMetaData::Ptr &stage) const;
  IntegratorMSMetaData2ndOrder::Ptr createIntegratorMSMetaData2ndOrderWithSingleStage
                            (const IntegratorSSMetaData2ndOrder::Ptr &stage) const;

  IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaData
                            (const FactoryInput::IntegratorMetaDataInput &input);
  IntegratorMSMetaData2ndOrder::Ptr createIntegratorMSMetaData2ndOrder
                            (const FactoryInput::IntegratorMetaDataInput &input);

  IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaData
                            (const FactoryInput::IntegratorMetaDataInput &input,
                             const std::vector<IntegratorSingleStageMetaData::Ptr> &stages) const;
  IntegratorMSMetaData2ndOrder::Ptr createIntegratorMSMetaData2ndOrder
                            (const FactoryInput::IntegratorMetaDataInput &input,
                             const std::vector<IntegratorSSMetaData2ndOrder::Ptr> &stages) const;

  IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaData
                            (const struct FactoryInput::IntegratorMetaDataInput &input,
                             const MappingFactory &mappingFactory);
  IntegratorMSMetaData2ndOrder::Ptr createIntegratorMSMetaData2ndOrder
                            (const struct FactoryInput::IntegratorMetaDataInput &input,
                             const MappingFactory &mappingFactory);

  IntegratorMultiStageMetaData::Ptr createIntegratorMultiStageMetaData
                            (const std::vector<Mapping::Ptr> &mappings,
                             const std::vector<IntegratorSingleStageMetaData::Ptr> &stages) const;
  IntegratorMSMetaData2ndOrder::Ptr createIntegratorMSMetaData2ndOrder
                            (const std::vector<Mapping::Ptr> &mappings,
                             const std::vector<IntegratorSSMetaData2ndOrder::Ptr> &stages) const;
  
  std::vector<std::vector<double> > getProblemGrid() const;
};

/**
* @class ConstraintFactory
* @brief factory class for creating StatePointConstraint objects
*/
class ConstraintFactory : public DyosObject
{
public:
  StatePointConstraint createConstraint(const struct FactoryInput::ConstraintInput &input) const;
  std::vector<StatePointConstraint> createConstraintVector
                      (const std::vector<FactoryInput::ConstraintInput> &inputVector,
                       const std::vector<double> &constraintGrid) const;
  virtual void removeDoubleConstraints(std::vector<FactoryInput::ConstraintInput> &constraints)const;
  std::vector<GlobalConstraint::Ptr> createGlobalConstraintVector
                      (const std::vector<FactoryInput::ConstraintInput> &inputVector,
                       const std::vector<IntegratorSingleStageMetaData::Ptr> &stages)const;
  std::vector<GlobalConstraint::Ptr> createMultipleShootingConstraintVector
                      (const FactoryInput::ConstraintInput &input,
                       const std::vector<IntegratorSingleStageMetaData::Ptr> &stages)const;
};

/**
* @class OptimizerMetaDataFactory
* @brief factory class for creating OptimizerMetaData objects
*/
class OptimizerMetaDataFactory : public DyosObject
{
public:
  OptimizerSingleStageMetaData::Ptr createOptimizerSingleStageMetaData(
                                const FactoryInput::OptimizerMetaDataInput &input,
                                const IntegratorSingleStageMetaData::Ptr &integratorMetaData,
                                const std::vector<double> &constraintGrid) const;
  OptimizerSSMetaData2ndOrder::Ptr createOptimizerSSMetaData2ndOrder(
                                const FactoryInput::OptimizerMetaDataInput &input,
                                const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData,
                                const std::vector<double> &constraintGrid) const;

  OptimizerSingleStageMetaData::Ptr createOptimizerSingleStageMetaData(
                                const FactoryInput::OptimizerMetaDataInput &input,
                                const ConstraintFactory &constFactory,
                                const IntegratorSingleStageMetaData::Ptr &integratorMetaData,
                                const std::vector<double> &constraintGrid) const;
  OptimizerSSMetaData2ndOrder::Ptr createOptimizerSSMetaData2ndOrder(
                                const FactoryInput::OptimizerMetaDataInput &input,
                                const ConstraintFactory &constFactory,
                                const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData,
                                const std::vector<double> &constraintGrid) const;

  OptimizerSingleStageMetaData::Ptr createOptimizerSingleStageMetaData(
                                const std::vector<StatePointConstraint> &constraints,
                                const StatePointConstraint &objective,
                                const IntegratorSingleStageMetaData::Ptr &integratorMetaData) const;
  OptimizerSSMetaData2ndOrder::Ptr createOptimizerSSMetaData2ndOrder(
                                const std::vector<StatePointConstraint> &constraints,
                                const StatePointConstraint &objective,
                                const IntegratorSSMetaData2ndOrder::Ptr &integratorMetaData) const;

  OptimizerMultiStageMetaData::Ptr createOptimizerMultiStageMetaData(
                       const struct FactoryInput::OptimizerMetaDataInput &input,
                       const std::vector<IntegratorSingleStageMetaData::Ptr> &integratorMetaDataStages,
                       const IntegratorMultiStageMetaData::Ptr intMSMD,
                       const std::vector<std::vector<double> > &constraintGrids) const;
  OptimizerMSMetaData2ndOrder::Ptr createOptimizerMSMetaData2ndOrder(
                       const struct FactoryInput::OptimizerMetaDataInput &input,
                       const std::vector<IntegratorSSMetaData2ndOrder::Ptr> &integratorMetaDataStages,
                       const IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                       const std::vector<std::vector<double> > &constraintGrids) const;

  OptimizerMultiStageMetaData::Ptr createOptimizerMultiStageMetaData(
                       const struct FactoryInput::OptimizerMetaDataInput &input,
                       const OptimizerMetaDataFactory &stageFactory,
                       const ConstraintFactory &constraintFactory,
                       const std::vector<IntegratorSingleStageMetaData::Ptr> integratorMetaDataStages,
                       const IntegratorMultiStageMetaData::Ptr intMSMD,
                       const std::vector<std::vector<double> > &constraintGrids) const;
  OptimizerMSMetaData2ndOrder::Ptr createOptimizerMSMetaData2ndOrder(
                       const struct FactoryInput::OptimizerMetaDataInput &input,
                       const OptimizerMetaDataFactory &stageFactory,
                       const ConstraintFactory &constraintFactory,
                       const std::vector<IntegratorSSMetaData2ndOrder::Ptr> integratorMetaDataStages,
                       const IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                       const std::vector<std::vector<double> > &constraintGrids) const;

  OptimizerMultiStageMetaData::Ptr createOptimizerMultiStageMetaData(
                       const std::vector<OptimizerSingleStageMetaData::Ptr> &stages,
                       const IntegratorMultiStageMetaData::Ptr intMSMD,
                       const std::vector<bool> &treatObjective) const;
  OptimizerMSMetaData2ndOrder::Ptr createOptimizerMSMetaData2ndOrder(
                       const std::vector<OptimizerSingleStageMetaData::Ptr> &stages,
                       const IntegratorMSMetaData2ndOrder::Ptr intMSMD,
                       const std::vector<bool> &treatObjective) const;
};