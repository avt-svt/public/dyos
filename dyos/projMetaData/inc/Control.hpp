/** 
* @file Control.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* MetaSingleStageEso - Part of DyOS                                    \n
* =====================================================================\n
* This file contains the classdefinitions of the Controls              \n
*                          according to the UML Diagramm METAESO_2     \n
* =====================================================================\n
* @author Fady Assassa, Aron Zingler
* @date 14.09.2019
*/
#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include "ParameterizationGrid.hpp"
#include "Array.hpp"
#include "DyosObject.hpp"


//! (shared) pointer to ParameterizationGrid
//typedef boost::shared_ptr<ParameterizationGrid> ParameterizationGridPtr;

/**
* @class Control
*
* @brief Defining controls of the optimization problem. Type, discretization and Grid.
*/
class Control : public DyosObject
{
protected:
  //! index of correspondong variable
  unsigned m_esoIndex;
  //! lower bound of the constraint
  double m_lowerBound;
  //! upper bound of the constraint
  double m_upperBound;
  //! we need to define a grid.each control can have multiple grid Classes due to structure detection.
  std::vector<ParameterizationGrid::Ptr> m_parameterizationGrid;
  std::vector<OriginalParameterizationGrid::Ptr> m_originalParameterizationGrid;
  //! currenct active grid index
  unsigned m_activeGridIndex;
  //! time point when parameter activity changes.
  double m_nextTimePoint;
  //! time point when parameter activity changes in reverse mode.
  double m_nextReverseTimePoint;
  //! attribute to check it the function is time invariant and can be adapted or not.
  bool m_isTimeInvariant;
  //! gridpoints of the parameterization grids and inserted additional timepoints - scaled
  std::vector<std::vector<double> > m_controlGrids;

protected:
  /// @brief sets the next time point
    void setNextTimePoint(const double nextPoint)
    {
        m_nextTimePoint = nextPoint;
    };
  double getPreviousTimePoint(void) const;
  void setControlPointerForParameters(void);
    double transformTotalTimeToLocalTime(const double totalTime) const; //for currentGrid
    double transformTotalTimeToLocalTime(const double totalTime,unsigned gridIndex) const;
  void splitGrid(const DurationParameter::Ptr &insertedDuration, unsigned gridIndex);
  void getSplittedDiscretizationGrids(std::vector<double> &discretizationGrid1,
                                      std::vector<double> &discretizationGrid2,
                                const unsigned gridIndex,
                                const double insertedDurationValue,
                                      double &splittingFactor)const;
  void getSplittedControlParameters(std::vector<ControlParameter::Ptr> &parameters1,
                                    std::vector<ControlParameter::Ptr> &parameters2,
                                     const unsigned gridIndex,
                                     const unsigned numPointsGrid,
                                     const double splittingFactor)const;
  
  double getOriginalLocalTime(const double localTime,
                                     const ParameterizationGrid::Ptr &currentGrid, 
                                     const OriginalParameterizationGrid::Ptr &currentOriginalGrid)const;
  unsigned getCurrentIndex()const;
  std::vector<unsigned> getActiveInd(const ParameterizationGrid::Ptr &currentGrid)const;
public:
  Control();
  ~Control();
  Control(const Control& c);

  void setEsoIndex(const unsigned esoIndex);
  unsigned getEsoIndex() const;
  void setLowerBound(const double lowerBound);
  double getLowerBound() const;
  void setUpperBound(const double upperBound);
  double getUpperBound() const;
  bool getIsInvariant() const;
  void setIsInvariant(const bool isInvariant);

  std::vector<double> getControlGridScaled(void)const; // returns the scaled control grid over all stages
  std::vector<double> getControlGridUnscaled(void) const; // returns the control grid over all stages
  std::vector<double> getControlGrid(unsigned gridIndex)const;
  // sets the control grid. However, a final time should be given before.
  void setControlGrid(const std::vector<ParameterizationGrid::Ptr> &controlGrids); 
  // sets the control grid and final times
  //void setControlGrid(const std::vector<ParameterizationGrid> controlGrids, const std::vector<double> durations); 
  double getTotalDuration(void)const; 
  std::vector<double> getFinalTimeVectorAbsolute(void)const;
  double getElapsedDurations(unsigned activeGridIndex)const; // sums up all final times prior to the index
  std::vector<double> getStageLength(void) const;

  ParameterizationGrid::Ptr getParameterizationGrid(const unsigned index) const;
  
  /**
  * @brief returns the size of the vector m_parameterizationGrid
  *
  * @return number of parameterization grids
  */
    unsigned getNumberOfGrids(void)const
    {
        return m_parameterizationGrid.size();
    };


  void setControlWithoutSens(void);
  void unsetIsDecisionVariable(void);

  
  // required for assembling the RHS 
  //! Make sure to call the functions involved in the RHS in the right order.
  double calculateCurrentControlValue(const double localTime)const; // returns the current control value
    //! Used for custom obj function, calculates value of control for global time points meaning for time points scaled with the stage duration
    std::vector<double> calculateControlValues(const std::vector<double>& globalTimePoints)const ;
    double calculateControlValue(unsigned originalGridIndex, double timePointRelativeToOriginalParametrizationGrid);
  vector<Parameter::Ptr> getActiveParameter(void)const;
  vector<Parameter::Ptr> getActiveParameter(const double currentGlobalTime)const;
    std::vector<std::vector<Parameter::Ptr>> getActiveControlParameterFromGivenAbsoluteTimes(const std::vector<double>& requestedGlobalTimePoints)const;
    std::pair<std::vector<unsigned>,std::vector<std::vector<Parameter::Ptr>>> getActiveGridIndiciesAndControlParameterFromGivenAbsoluteTimes(std::vector<double> requestedGlobalTimePoints) const;
  DurationParameter::Ptr getActiveDurationParameter(void)const;
  DurationParameter::Ptr getActiveDurationParameter(const double currentGlobalTime)const;
  bool isCurrentDuration(const double integratorDuration)const;
  // this function needs to be called first to determine a parameter change
  bool checkParameterChange(const double time)const;
  bool checkParameterChangeReverse(const double time)const;
  void changeActiveParameter(void); // changes the active parameter to the next one in the horizon
    std::vector<double> getAllValuesOfOriginalParametersForGrid(unsigned originalGridIndex) const;
    double calculateControlValueForOriginalGridIndex(unsigned originalGridIndex, double timePointRelativeToOriginalParametrizationGrid);
    std::vector<ControlParameter::Ptr> getOriginalParametersForGrid(unsigned originalGridIndex) const ;
  void changeActiveParameterReverse();
  double getCurrentDSplineValue(const double localTime, const unsigned paramIndex)const;
  double getControlDerivative()const;
  /**
  * @brief getter for the attribute m_activeGridIndex
  *
  * @return the index to the active grid
  */
    unsigned getActiveGridIndex(void)const
    {
        return m_activeGridIndex;
    };
  /// @brief sets the active grid index
    void setActiveGridIndex(const int activeGridIndex)
    {
        m_activeGridIndex = activeGridIndex;
    };
  /**
  * @brief getter for the attribute m_nextTimePoint
  *
  * @return the next time point
  */
    double getNextTimePoint(void)const
    {
        return m_nextTimePoint;
    };
  void initializeForFirstIntegration(void);
  void setReverseTimePoint();
  
  void insertDurationParameter(const DurationParameter::Ptr &duration,
                               unsigned gridIndex);
  
  void setGridPointsVector(const std::vector<double> &additionalTimepoints);
  
  DyosOutput::ParameterOutput getOutput(GenericEso::Ptr &genPtr,
                                        IntOptDataMap iODP);
};
