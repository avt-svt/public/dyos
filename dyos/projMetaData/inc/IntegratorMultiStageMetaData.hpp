/**
* @file IntegratorMultiStageMetaData.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Meta Multi  Stage ESO - Part of DyOS                                 \n
* =====================================================================\n
* This file contains the IntegratorMultiStageMetaData class            \n
* and the IntegratorMSMetaData2ndOrder class                           \n
* =====================================================================\n
* @author Tjalf Hoffmann, Fady Assassa
* @date 20.09.2012
*/
#pragma once

#include "IntegratorMetaData.hpp"
#include "IntegratorSingleStageMetaData.hpp"
#include "Mapping.hpp"

/**
* @class IntegratorMultiStageMetaData
*
* @brief class defining a complete multi stage problem
*
* This class can provide all information needed to solve a multi stage problem.
*/
class IntegratorMultiStageMetaData : public virtual IntegratorMetaData
{
protected:
  //! We need to define N-1 Mapping if we have N stages.
  std::vector<Mapping::Ptr> m_mapping;
  //! N stages with the information of the IntegratorSingleStageMetaData
  std::vector<IntegratorSingleStageMetaData::Ptr> m_stages;
  //! m_mappingSensitivities, sensitivities of the multi stage prior to m_currentStageIndex
  std::vector<double> m_mappingSensitivities;
  //! current stage index during integration.
  unsigned m_currentStageIndex;
  //! flag stating whether initializeForFirstIntegration has been called
  bool m_isInitialized;
  //! flag stating whether initializeAtDuration has been called before
  bool m_isInitializedBackwards;

protected:

  virtual void getConstraintSensitivities(const std::vector<unsigned> &equationIndices,
                                          const unsigned numParams,
                                          const unsigned numEqn,
                                                utils::Array<double>  &sensitivities);
  virtual void copyAdjointsAndLagrangeDersToPreviousStage();
  void addIntOptDataPoint(const unsigned timeIndex);
  int getNumMappingParameters() const;
  std::vector<unsigned> getParameterIndicesOfPreviousStages(const int esoIndex,
                                                            const unsigned stageIndex);
  void updateParameterOutput(DyosOutput::ParameterOutput &parameterOutput,
                                    const std::vector<unsigned> &parameterIndices,
                                    const unsigned stageIndex);
  void adjustMultistageParameterStructure(std::vector<DyosOutput::ParameterOutput> &parameters,
                                          std::vector<DyosOutput::ParameterOutput> &parametersNextStage,
                                          const unsigned stageIndex);
public:
  typedef std::shared_ptr<IntegratorMultiStageMetaData> Ptr;
  IntegratorMultiStageMetaData();
  IntegratorMultiStageMetaData(std::vector<Mapping::Ptr> mappings,
                               std::vector<IntegratorSingleStageMetaData::Ptr> stages);

  ~IntegratorMultiStageMetaData();

  void initializeForFirstIntegration();
  virtual void initializeMatrices();
  virtual void initializeForNextIntegration();
  virtual void initializeForPreviousIntegrationBlock();
  virtual void initializeForNewIntegrationStep();
  virtual void initializeForNewBackwardsIntegrationStep();
  virtual void initializeAtStageDuration();

  virtual void setStepStartTime(const double startTime);
  virtual double getStepStartTime() const;
  virtual void setStepEndTime(const double endTime);
  virtual double getStepEndTime() const;
  virtual void setStepCurrentTime(const double time);
  virtual double getStepCurrentTime() const;

  virtual unsigned getNumStages() const;

  virtual void resetGolbalDurationParameter(void);

  virtual void calculateSensitivitiesForward1stOrder(const utils::Array<double> &inputSensitivities,
                                                           utils::Array<double> &rhsSensitivities);
  virtual void calculateRhsStates(const utils::Array<double> &inputStates,
                                      utils::Array<double> &rhsStates);
  virtual void calculateAdjointsReverse1stOrder(const utils::Array<double> &states,
                                                const utils::Array<double> &adjoints,
                                                      utils::Array<double> &rhsAdjoints,
                                                      utils::Array<double> &rhsDers);
  virtual void calculateRhsAdjoints(const utils::Array<double> &adjoints,
                                          utils::Array<double> &rhsAdjoints);
                                          
  virtual void calculateRhsStatesDtime(utils::Array<double> &rhsDTime);
  virtual void setStates(const utils::Array<double> &states);
  virtual void storeInitialStates(const utils::Array<double> &states);
  virtual void setSensitivities(const utils::Array<double> &sens);
  virtual void setAdjointsAndLagrangeDerivatives(const utils::Array<double> &adjoints,
                                                 const utils::Array<double> &lagrangeDerivatives);
  virtual void setInitialLagrangeDerivatives();
  virtual void updateAdjoints();

  virtual CsCscMatrix::Ptr getMMatrix(void) const;
  virtual CsTripletMatrix::Ptr getMMatrixCoo(void) const;

  virtual void evaluateInitialValues(utils::Array<double> &initialStates,
                                     utils::Array<double> &initialSensitivities);

  virtual int getNumCurrentSensitivityParameters() const;
  virtual void getSensitivityParameters(std::vector<Parameter::Ptr> &sensParams) const;

  virtual int getNumIntegrationIntervals() const;

  virtual void setAllInitialValuesFree();

  virtual GenericEso::Ptr getGenericEso();

  virtual int getNumStateNonZeroes();

  virtual double getStageDuration() const;

  virtual void setControlsToEso();

  virtual unsigned getNumSensitivityParameters() const;

  virtual std::vector<IntOptDataMap> getIntOptDataStages(void);
  // we need to ask ourself which function are required in the interface.
  // Now all are implemented such that one can create
  // an instance of the IntegratorMultiStageMetaData
  virtual double getIntegrationGridDuration()const;
  virtual CsCscMatrix::Ptr getJacobianFstates();
  virtual CsCscMatrix::Ptr getJacobianFup();
  virtual void getCurrentSensitivities(std::vector<double> &curSens) const;
  virtual void getAdjoints(std::vector<double> &adjoints) const;
  virtual void getLagrangeDers(std::vector<double> &LagrangeDers) const;
  virtual double getMsDurationVal() const;
  virtual void getSumOfDurationBounds(double &sumLowerBounds, double &sumUpperBounds) const;
  virtual std::vector<std::vector<int> > getFreeDurationIndices() const;
  virtual double getStageCurrentTime()const;
  virtual void reset();
  
  virtual std::vector<Mapping::Ptr> getMappings() const;

  // switching stuff
  virtual void foundRoot();
  
  virtual void activatePlotGrid();

  std::vector<DyosOutput::StageOutput> getOutput();
};

/**
* @class IntegratorMSMetaData2ndOrder
* @brief implementation of IntegratorMetaData2ndOrder for multi stage
*/
class IntegratorMSMetaData2ndOrder : public virtual IntegratorMultiStageMetaData,
                                     public virtual IntegratorMetaData2ndOrder
{
protected:

  //! Modelserver object of the stage
  std::vector<IntegratorSSMetaData2ndOrder::Ptr> m_stages2ndOrder;

  virtual void copyAdjointsAndLagrangeDersToPreviousStage();
public:
  typedef std::shared_ptr<IntegratorMSMetaData2ndOrder> Ptr;
  //void  getOutput(std::vector <UserInput::StageInput> input, std::vector <UserOutput::StageOutput> &output);
  IntegratorMSMetaData2ndOrder(std::vector<Mapping::Ptr> mappings,
                               std::vector<IntegratorSSMetaData2ndOrder::Ptr> stages);

  ~IntegratorMSMetaData2ndOrder();

  virtual void initializeAtStageDuration();

  virtual void getAdjoints(std::vector<double> &adjoints) const;
  virtual void updateAdjoints();
  virtual void setInitialLagrangeDerivatives();

  virtual void setAdjointsAndLagrangeDerivatives(const utils::Array<double> &adjoints,
                                                 const utils::Array<double> &lagrangeDerivatives);

  virtual void calculateRhsLagrangeDers2ndOrder(utils::Array<double> &adjoints1stOrder,
                                                utils::Array<double> &adjoints2ndOrder,
                                                Eso2ndOrderEvaluation &input,
                                                utils::Array<double> &rhsDers2ndOrder);

  virtual void calculateAdjointsReverse2ndOrder(utils::Array<double> &states,
                                                utils::Array<double> &sens,
                                                utils::Array<double> &adjoints1stAnd2nd,
                                                utils::Array<double> &rhsAdjoints2nd,
                                                utils::Array<double> &rhsDers2nd);

  virtual GenericEso2ndOrder::Ptr getGenericEso2ndOrder() const;
};
