/**
* @file TestIOConversions.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Dyos testsuite                                                       \n
* =====================================================================\n
* This file contains tests for I/O Conversions                         \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi
* @date 26.03.2012
*/

#include <iostream>
//#include "Dyos.hpp"
#include <boost/lexical_cast.hpp>
#include "InputOutputConversions.hpp"
#include "ConvertFromXmlJson.hpp"
#include "InputOutputConfig.hpp"

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE_2 InputOutput
#include "boost/test/unit_test.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>

using namespace UserInput;
using namespace UserOutput;
#define TEST_OBJECT_NAME "carErweitert"

BOOST_AUTO_TEST_SUITE(DyosIOTests)

/**
* @test DyosIOTests - system test of Input/Output Conversion functions
*/

// testing convert function i2oConvertEso
BOOST_AUTO_TEST_CASE(Testi2oConvertEso)
{
  IOConversions ioConverter;
  UserInput::EsoInput esoInput;
  esoInput.type = EsoType::JADE;
  esoInput.model = TEST_OBJECT_NAME;
  // test case Jade
  UserOutput::EsoOutput output = ioConverter.i2oConvertEso(esoInput);
  BOOST_CHECK_EQUAL(output.type, EsoType::JADE);
}

// testing convert function i2oConvertRunningMode
BOOST_AUTO_TEST_CASE(Testi2oConvertRunningMode)
{
  IOConversions ioConverter;
  UserOutput::Output output;
  UserInput::Input input;
  input.runningMode = UserInput::Input::SIMULATION;
  output.runningMode = ioConverter.i2oConvertRunningMode(input);
  // test case SIMULATION
  BOOST_CHECK_EQUAL(output.runningMode, UserOutput::Output::SIMULATION);
  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  output.runningMode = ioConverter.i2oConvertRunningMode(input);
  // test case SINGLE_SHOOTING
  BOOST_CHECK_EQUAL(output.runningMode, UserOutput::Output::SINGLE_SHOOTING);
}

// testing convert function i2oConvertApproximationType
BOOST_AUTO_TEST_CASE(Testi2oConvertApproximationType)
{
  IOConversions ioConverter;
  UserOutput::ParameterGridOutput output;
  UserInput::ParameterGridInput input;
  input.type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
  output.type = ioConverter.i2oConvertApproximationType(input);
  // test case PIECEWISE_CONSTANT
  BOOST_CHECK_EQUAL(output.type, UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT);
  input.type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
  output.type = ioConverter.i2oConvertApproximationType(input);
  // test case PIECEWISE_LINEAR
  BOOST_CHECK_EQUAL(output.type, UserOutput::ParameterGridOutput::PIECEWISE_LINEAR);
}

// testing convert function i2oConvertInputToOutput
BOOST_AUTO_TEST_CASE(Testi2oConvertInputToOutput)
{
  IOConversions ioConverter;
  UserOutput::Output output;
  UserInput::Input input;
  input.runningMode = UserInput::Input::SINGLE_SHOOTING;
  input.stages.resize(3);
  for(unsigned i=0; i<input.stages.size(); i++)
  {
	input.stages[i].treatObjective = true;
    input.stages[i].eso.type = EsoType::JADE;
    input.stages[i].eso.model = TEST_OBJECT_NAME;
    
    input.stages[i].integrator.duration.name = "Test";
    input.stages[i].integrator.duration.grids.resize(3);
    for(unsigned j=0; j<input.stages[i].integrator.duration.grids.size(); j++)
    {
      input.stages[i].integrator.duration.grids[j].duration = (i+1)*(j+1);
      input.stages[i].integrator.duration.grids[j].hasFreeDuration = false;
      input.stages[i].integrator.duration.grids[j].numIntervals = 5;
      input.stages[i].integrator.duration.grids[j].type = UserInput::ParameterGridInput::PIECEWISE_CONSTANT;
      input.stages[i].integrator.duration.grids[j].values.resize(3);
      for(unsigned k=0; k<input.stages[i].integrator.duration.grids[j].values.size(); k++)
      {
        input.stages[i].integrator.duration.grids[j].values[k] = (i+1)*(j+1)*(k+1);
      }
    }
  }
  BOOST_CHECK_NO_THROW(output = ioConverter.i2oConvertInputToOutput(input));
}

// testing convert function i2oConvertMapping
BOOST_AUTO_TEST_CASE(Testi2oConvertMapping)
{
  IOConversions ioConverter;
  UserInput::StageMapping mappingInput;
  UserOutput::MappingOutput mappingOutput;
  mappingInput.fullStateMapping = true;
  std::pair<std::string, std::string> pair;
  for(unsigned i=0; i != 10; i++)
  {
    std::string str = std::to_string(i);
    pair.first = "FirstStateName" + str;
    pair.second = "SecontStateName" + str;
    mappingInput.stateNameMapping.insert(pair);
  }
  mappingOutput = ioConverter.i2oConvertMapping(mappingInput);
  BOOST_CHECK_EQUAL(mappingOutput.fullStateMapping, true);
  std::map<std::string, std::string>::iterator iter;
  unsigned i=0;
  for(iter = mappingOutput.stateNameMapping.begin(); iter != mappingOutput.stateNameMapping.end(); iter++)
  {
    std::string strFirst = iter->first;
    std::string strSecond = iter->second;
    std::string str = std::to_string(i);
    BOOST_CHECK_EQUAL(strFirst.c_str(), "FirstStateName" + str);
    BOOST_CHECK_EQUAL(strSecond.c_str(), "SecontStateName" + str);
    i++;
  }
}

// testing convert function i2oParameterInput
BOOST_AUTO_TEST_CASE(Testi2oParameterInput)
{
  IOConversions ioConverter;
  UserInput::ParameterInput input;
  UserOutput::ParameterOutput output;
  input.name = "testName";
  input.value = 1.0;
  input.lowerBound = 0.0;
  input.upperBound = 10.0;
  input.paramType = UserInput::ParameterInput::INITIAL;
  input.sensType = UserInput::ParameterInput::FULL;
  ioConverter.i2oParameterInput(input, output);
  BOOST_CHECK_EQUAL(output.name, input.name);
  BOOST_CHECK_EQUAL(output.value, input.value);
  BOOST_CHECK_EQUAL(output.lowerBound, input.lowerBound);
  BOOST_CHECK_EQUAL(output.upperBound, input.upperBound);
  BOOST_CHECK_EQUAL(output.paramType, UserOutput::ParameterOutput::INITIAL);
  BOOST_CHECK_EQUAL(output.sensType, UserOutput::ParameterOutput::FULL);
}

// testing convert function i2oConvertStageVector
BOOST_AUTO_TEST_CASE(Testi2oConvertStageVector)
{
  IOConversions ioConverter;
  UserInput::StageInput input;
  UserOutput::StageOutput output;
  input.eso.model = "testModel";
  input.eso.type = EsoType::JADE;
  input.mapping.fullStateMapping = false;
  input.treatObjective = true;
  output = ioConverter.i2oConvertStageVector(input);
  BOOST_CHECK_EQUAL(output.eso.model, input.eso.model);
  BOOST_CHECK_EQUAL(output.eso.type, EsoType::JADE);
  BOOST_CHECK_EQUAL(output.mapping.fullStateMapping, false);
  BOOST_CHECK_EQUAL(output.treatObjective, true);
}

// testing convert function i2oConvertIntegrator
BOOST_AUTO_TEST_CASE(Testi2oConvertIntegrator)
{
  IOConversions ioConverter;
  UserInput::IntegratorInput input;
  UserOutput::IntegratorOutput output;
  input.type = UserInput::IntegratorInput::LIMEX;
  input.order = UserInput::IntegratorInput::FIRST_FORWARD;
  output = ioConverter.i2oConvertIntegrator(input);
  BOOST_CHECK_EQUAL(output.type, UserOutput::IntegratorOutput::LIMEX);
  BOOST_CHECK_EQUAL(output.order, UserOutput::IntegratorOutput::FIRST_FORWARD);
}

// testing convert function i2oConvertConstraint
BOOST_AUTO_TEST_CASE(Testi2oConvertConstraint)
{
  IOConversions ioConverter;
  UserInput::ConstraintInput input;
  UserOutput::ConstraintOutput output;
  std::vector<DyosOutput::ConstraintOutput> mdOptVect;
  input.lagrangeMultiplier = 0.0;
  input.lowerBound = 1.0;
  input.upperBound = 10.0;
  input.name = "testName";
  input.timePoint = 1.1;
  // case POINT
  input.type = UserInput::ConstraintInput::POINT;
  output = ioConverter.i2oConvertConstraint(input, mdOptVect);
  BOOST_CHECK_EQUAL(output.lowerBound, 1.0);
  BOOST_CHECK_EQUAL(output.upperBound, 10.0);
  BOOST_CHECK_EQUAL(output.name, "testName");
  BOOST_CHECK_EQUAL(output.type, UserOutput::ConstraintOutput::POINT);
  // case ENDPOINT
  input.type = UserInput::ConstraintInput::ENDPOINT;
  output = ioConverter.i2oConvertConstraint(input, mdOptVect);
  BOOST_CHECK_EQUAL(output.type, UserOutput::ConstraintOutput::ENDPOINT);
  // case PATH
  input.type = UserInput::ConstraintInput::PATH;
  output = ioConverter.i2oConvertConstraint(input, mdOptVect);
  BOOST_CHECK_EQUAL(output.type, UserOutput::ConstraintOutput::PATH);
}

// testing convert function o2iConvertEso
BOOST_AUTO_TEST_CASE(Testo2iConvertEso)
{
  IOConversions ioConverter;
  UserOutput::EsoOutput esoOutput;
  esoOutput.type = EsoType::JADE;
  esoOutput.model = TEST_OBJECT_NAME;
  // test case Jade
  UserInput::EsoInput output = ioConverter.o2iConvertEso(esoOutput);
  BOOST_CHECK_EQUAL(output.type, EsoType::JADE);
}

// testing convert function o2iConvertRunningMode
BOOST_AUTO_TEST_CASE(Testo2iConvertRunningMode)
{
  IOConversions ioConverter;
  UserOutput::Output input;
  UserInput::Input output;
  input.runningMode = UserOutput::Output::SIMULATION;
  output.runningMode = ioConverter.o2iConvertRunningMode(input);
  // test case SIMULATION
  BOOST_CHECK_EQUAL(output.runningMode, UserInput::Input::SIMULATION);
  input.runningMode = UserOutput::Output::SINGLE_SHOOTING;
  output.runningMode = ioConverter.o2iConvertRunningMode(input);
  // test case SINGLE_SHOOTING
  BOOST_CHECK_EQUAL(output.runningMode, UserInput::Input::SINGLE_SHOOTING);
}

// testing convert function o2iConvertApproximationType
BOOST_AUTO_TEST_CASE(Testo2iConvertApproximationType)
{
  IOConversions ioConverter;
  UserOutput::ParameterGridOutput input;
  UserInput::ParameterGridInput output;
  input.type = UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT;
  output.type = ioConverter.o2iConvertApproximationType(input);
  // test case PIECEWISE_CONSTANT
  BOOST_CHECK_EQUAL(output.type, UserInput::ParameterGridInput::PIECEWISE_CONSTANT);
  input.type = UserOutput::ParameterGridOutput::PIECEWISE_LINEAR;
  output.type = ioConverter.o2iConvertApproximationType(input);
  // test case PIECEWISE_LINEAR
  BOOST_CHECK_EQUAL(output.type, UserInput::ParameterGridInput::PIECEWISE_LINEAR);
}

// testing convert function convertUserOutputFromXmlJson and o2iConvertOutputToInput
// Json is generated in DyosSystemTests/WOBatchNixeWithPWL
BOOST_AUTO_TEST_CASE(Testo2iConvertOutputToInput)
{
  IOConversions ioConverter;
  UserOutput::Output input;
  UserInput::Input output;
  convertUserOutputFromXmlJson(PATH_TO_woJadeout_JSON, input, JSON);
  BOOST_CHECK_NO_THROW(output = ioConverter.o2iConvertOutputToInput(input));
  BOOST_CHECK_CLOSE(output.stages[0].integrator.duration.value, 1000, 1E-6); 
  BOOST_CHECK_CLOSE(output.stages[0].integrator.parameters[1].grids[0].values[0], 0.030407287862477615, 1E-6);
}


BOOST_AUTO_TEST_SUITE_END()
