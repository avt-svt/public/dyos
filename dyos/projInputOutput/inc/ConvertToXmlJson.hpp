/**
* @file ConvertToXmlJson.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertToXmlJson                                                     \n
* =====================================================================\n
* This file contains the declaration of the two function that convert  \n
* either UserOutput::Output or UserInput::Input into an Xml/Json-file  \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski
* @date 28.11.2012
*/

#pragma once
#include "UserInput.hpp"
#include "UserOutput.hpp"
#include "InputExceptions.hpp"

#include <boost/property_tree/ptree.hpp>

//enum to specify the DataFormat in which UserOutput::Output or UserInput::Input is converted
enum DataFormat{
  XML,
  JSON
};

void convertUserInputToXmlJson(std::string filename, UserInput::Input input, DataFormat dataFormat);
void convertUserOutputToXmlJson(std::string filename, UserOutput::Output output, DataFormat dataFormat);

/**
* @brief create boost::propertyTree::ptree, call several subfunctions, write to given stream
*
* @param[in,out] stream output stream to which UserOutput::Output is written
* @param[in] output
* @param[in] dataFormat Fornat in which the output is written (XML or JSON)
*/
void createPropertyTree_Output(
            std::basic_ostream<typename boost::property_tree::ptree::key_type::value_type> &stream,
      const UserOutput::Output &output, DataFormat dataFormat);


/**
* @brief create boost::propertyTree::ptree, call several subfunctions, write xml-file "convertedUserInput.xml"
*
* @param[in] name of the file to which Input-data is written
* @param[in] UserInput::Input input
* @param[in] DataFormat of the file to which Input-data is written
*/
void createPropertyTree_Input(
            std::basic_ostream<typename boost::property_tree::ptree::key_type::value_type> &stream,
      const UserInput::Input &input, DataFormat dataFormat);