/**
* @file ConvertFromXmlJson.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* ConvertFromXmlJson                                                   \n
* =====================================================================\n
* This file contains the declaration of the two functions that convert \n
* an Xml/Json-file into either UserOutput::Output or UserInput::Input  \n
* =====================================================================\n
* @author Ann-Kathrin Dombrowski
* @date 03.12.2012
*/



#pragma once
#include "UserInput.hpp"
#include "InputExceptions.hpp"
#include "ConvertToXmlJson.hpp"

void convertUserInputFromXmlJson(std::string filename, UserInput::Input &input, DataFormat dataFormat);
void convertUserOutputFromXmlJson(std::string filename, UserOutput::Output &output, DataFormat dataFormat);
