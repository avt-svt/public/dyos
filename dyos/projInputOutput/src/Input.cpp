/**
* @file Input.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Input                                                                \n
* =====================================================================\n
* This file contains all definitions of UserInput functions            \n
* =====================================================================\n
* @author Tjalf Hoffmann, Adrian Caspari
* @date 22.03.2018
*/

#include <cassert>
#include <algorithm>
#include <boost/lexical_cast.hpp>

#include "GenericEsoFactory.hpp"
#include "EsoUtilities.hpp"
#include "MetaDataFactories.hpp"
#include "Control.hpp"
#include "Input.hpp"
#include <functional>
#include <tuple>
#include <string>
#include "TripletMatrix.hpp"



/**
  * @brief return the currently used GenericEso pointer
  *
  * if model name is different from the currently used Eso,
  * that is if a new GenericEso object is needed, the old object will be
  * discarded and a new object will be created
  * @param[in] eso EsoInput struct providing information needed to create the GenericEso object
  * @return pointer to the currently used GenericEso object
  */
GenericEso::Ptr InputConverter::getGenericEsoPtr(const FactoryInput::EsoInput &eso)
{
  if(eso.model != m_model){
    m_model = eso.model;
    GenericEsoFactory genEsoFac;
    StdCapture capture;
    capture.beginCapture();
    m_genEso = GenericEso::Ptr(genEsoFac.create(eso));
    capture.endCapture();
    m_logger->sendMessage(Logger::ESO, OutputChannel::STANDARD_VERBOSITY, capture.getCapture());
    m_logger->newlineAndFlush(Logger::ESO, OutputChannel::STANDARD_VERBOSITY);
    
  }
  return m_genEso;
}

/**
* @brief checks whether any constraint already is defined as a parameter or not
*
* @param[in] constraints vector of constraints to be checked
* @param[in] imdInput input struct containing information about all parameters
* @param[in] eso input struct needed for function getEsoIndex
* @throw DefinedConstraintForParameterException
*/
void InputConverter::checkConstraintNames(const std::vector<UserInput::ConstraintInput> &constraints,
                                          const FactoryInput::IntegratorMetaDataInput &imdInput,
                                          const GenericEso::Ptr &eso)
{

  std::vector<std::string> esoNames;
  getEsoNames(esoNames, eso);
  for(unsigned i=0; i<constraints.size(); i++){
    const unsigned constraintIndex = getEsoIndex(esoNames, constraints[i].name);
    for(unsigned j=0; j<imdInput.controls.size(); j++){
      if(constraintIndex == imdInput.controls[j].esoIndex){
        throw DefinedConstraintForParameterException(constraintIndex);
      }
    }
  }
}

void InputConverter::checkParameterNames(const std::vector<UserInput::ParameterInput> &parameters)
{
  for(unsigned i=0; i<parameters.size(); i++){
    std::string name = parameters[i].name;
    for(unsigned j=i+1; j<parameters.size(); j++){
      if(name == parameters[j].name){
        throw DefinedParameterTwiceException(name);
      }
    }
  }
}

void InputConverter::checkObjectiveName(const std::vector<UserInput::ConstraintInput> &constraints,
                                        const UserInput::ConstraintInput &objective)
{
  for(unsigned i=0; i<constraints.size(); i++){
    if(constraints[i].name == objective.name){
      throw DefinedConstraintForObjectiveException();
    }
  }
}

void InputConverter::checkConstraintForIndependency(const UserInput::ConstraintInput &constraint,
                                                    const FactoryInput::IntegratorMetaDataInput &imdInput)
{
  if(constraint.type != UserInput::ConstraintInput::ENDPOINT){
    return;
  }
  GenericEso::Ptr esoPtr = imdInput.esoPtr;
  unsigned numNonZeroes = esoPtr->getNumNonZeroes();
  unsigned numEquations = esoPtr->getNumEquations();
  unsigned numVariables = esoPtr->getNumVariables();
  
  //collect control indices
  std::vector<unsigned> controlIndices;
  for(unsigned i=0; i<imdInput.controls.size(); i++){
    if(imdInput.controls[i].sensType == FactoryInput::ParameterInput::FULL){
      controlIndices.push_back(imdInput.controls[i].esoIndex);
    }
  }
  

  
  unsigned constraintEsoIndex = esoPtr->getEsoIndexOfVariable(constraint.name);
  unsigned numAlgEquations = esoPtr->getNumAlgEquations();
  unsigned numAlgVariables = esoPtr->getNumAlgebraicVariables();
  unsigned numDiffEquations = esoPtr->getNumDiffEquations();
  unsigned numDiffVariables = esoPtr->getNumDifferentialVariables();
  utils::Array<EsoIndex> algEqIndices(numAlgEquations), algVarIndices(numAlgVariables);
  utils::Array<EsoIndex> diffEqIndices(numDiffEquations), diffVarIndices(numDiffVariables);
  esoPtr->getAlgEquationIndex(numAlgEquations, algEqIndices.getData());
  esoPtr->getAlgebraicIndex(numAlgVariables, algVarIndices.getData());
  esoPtr->getDiffEquationIndex(numDiffEquations, diffEqIndices.getData());
  esoPtr->getDifferentialIndex(numDiffVariables, diffVarIndices.getData());
  
  //if constraint refers to an algebraic variable, check whether it depends directly on a control
  EsoIndex *found = std::find(algVarIndices.getData(), algVarIndices.getData() + numAlgVariables,
                              constraintEsoIndex);
  if(found == algVarIndices.getData() + numAlgVariables){
    return;
  }
  
  assert(numNonZeroes > 0);
  utils::Array<EsoIndex> rowIndices(numNonZeroes), colIndices(numNonZeroes);
  esoPtr->getJacobianStruct(numNonZeroes, rowIndices.getData(), colIndices.getData());
  
  //cut out differential equations
  std::vector<EsoIndex> reducedRowIndices, reducedColIndices;
  for(unsigned i=0; i<numNonZeroes; i++){
    found = std::find(diffEqIndices.getData(), diffEqIndices.getData() + numDiffEquations, rowIndices[i]);
    if(found == diffEqIndices.getData() + numDiffEquations){
      reducedRowIndices.push_back(rowIndices[i]);
      reducedColIndices.push_back(colIndices[i]);
    }
  }
  
  //now reduce matrix to minimum col size (erase empty colums)
  
  for(int i=numVariables-1; i>=0; i--){
    std::vector<int>::iterator found = std::find(reducedColIndices.begin(), reducedColIndices.end(), i);
    if(found == reducedColIndices.end()){
      numVariables--;
      for(unsigned j=0; j<reducedColIndices.size(); j++){
        if(reducedColIndices[j] > i){
          reducedColIndices[j]--;
        }
      }
      for(unsigned j=0; j<diffVarIndices.getSize(); j++){
        if(diffVarIndices[j] == i){
          diffVarIndices[j] = -1;
        }
        if(diffVarIndices[j] > i){
          diffVarIndices[j]--;
        }
      }
      for(unsigned j=0; j<controlIndices.size(); j++){
        if(controlIndices[j] == unsigned(i)){
          controlIndices[j] = -1;
        }
        if(controlIndices[j] > unsigned(i)){
          controlIndices[j]--;
        }
      }
      if(constraintEsoIndex > unsigned(i)){
        constraintEsoIndex--;
      }
    }
  }
  assert(!reducedRowIndices.empty() && !reducedColIndices.empty());
  utils::Array<double> values(reducedRowIndices.size(),1.0);
  CsTripletMatrix::Ptr jacobian(new CsTripletMatrix(numEquations, numVariables,
                                                    reducedRowIndices.size(),
                                                    &reducedRowIndices[0],
                                &reducedColIndices[0],
      values.getData()));
  
  csd* blockDecomp = jacobian->compress()->get_dulmange_mendelsohn_permutation(0);
  
  int permutatedColIndex = std::find(blockDecomp->q, blockDecomp->q + numVariables, constraintEsoIndex) - blockDecomp->q;
  assert(permutatedColIndex < int(numVariables));
  int blockIndex = 0;
  while(permutatedColIndex >= blockDecomp->s[blockIndex]){
    blockIndex++;
    assert(blockIndex < blockDecomp->nb + 1);
  }
  //blockIndex now points on the block successing the block containing the constraint index
  blockIndex--;
  assert(blockIndex >= 0);
  
  int blockColSize = blockDecomp->s[blockIndex + 1] - blockDecomp->s[blockIndex];
  int startVarIndex = blockDecomp->s[blockIndex];
  
  //if block depends on differential variable, hte constraint is OK
  for(int i=0; i<blockColSize; i++){
    assert(startVarIndex + i < int(numVariables));
    EsoIndex* foundDiffVarIndex = std::find(diffVarIndices.getData(),
                                            diffVarIndices.getData() + numDiffVariables,
                                            blockDecomp->q[startVarIndex + i]);

    if(foundDiffVarIndex != diffVarIndices.getData() + numDiffVariables){
      //found a differential variable in the block
      return;
    }
  }
  for(int i=0; i<blockColSize; i++){
    assert(startVarIndex + i < int(numVariables));
    std::vector<unsigned>::iterator foundControlIndex = std::find(controlIndices.begin(),
                                                                  controlIndices.end(),
                                                                  blockDecomp->q[startVarIndex + i]);
    if(foundControlIndex != controlIndices.end()){
      throw EndpointConstraintDirectlyDependsOnControlException();
    }
  }
}

/**
* @brief check if explicit plot grid are values within [0.0, 1.0] and in ascending order
* @params explicitPlotGrid values to be checked
* @throw ExplicitPlotGridNotInBoundsException
* @throw ExplicitPlotGridNotOrderedException
*/
void InputConverter::checkExplicitPlotGrid(const std::vector<double> &explicitPlotGrid)
{
  double previousValue = 0.0;
  for(unsigned i=0; i<explicitPlotGrid.size(); i++){
    if(explicitPlotGrid[i] < 0.0 ||explicitPlotGrid[i] > 1.0){
      throw ExplicitPlotGridNotInBoundsException();
    }
    if(i>0 && explicitPlotGrid[i] <= previousValue){
      throw ExplicitPlotGridNotOrderedException();
    }
    previousValue = explicitPlotGrid[i];
  }
}

/**
* @brief extract all variable names from the currents stage eso
* @param[out] esoNames on exit contains all variable names of the current stage
* @param[in] eso input struct used to create a GenericEso object (to retrieve the eso indices)
* @throw MissingModelNameException
*/
void InputConverter::getEsoNames(std::vector<std::string> &esoNames,
                                 const GenericEso::Ptr &esoPtr)
{
  /*if(eso.model == ""){
    throw MissingModelNameException();
  }*/
  
  esoNames.resize(esoPtr->getNumVariables());
  esoPtr->getVariableNames(esoNames);
}
/**
* @brief convert given name into corresponding eso index
*
* @param[in] esoNames all variable names of the current stage
* @param[in] name name to be converted
* @return the eso index corresponding to the given name
* @throw MissingVariableNameException, VariableNotFoundException
*/
unsigned InputConverter::getEsoIndex(const std::vector<std::string> &esoNames,
                                     const std::string &name)
{
  if(name == ""){
    throw MissingVariableNameException();
  }
  const unsigned numVars = esoNames.size();

  for(unsigned i=0; i<numVars; i++){
    if(name == esoNames[i]){
      return i;
    }
  }

  throw VariableNotFoundException(name);
}

/**
* @brief retrieve grid points specified by point constraints (and therefore by user)
*        that are to be added to the integration grid
*
* @param[in] constraints vector of ConstraintInput struct
* @return set of additional user grid points (sorted)
*/
std::vector<double> InputConverter::getUserGrid(const std::vector<UserInput::ConstraintInput> &constraints) const
{
  std::set<double> set;

  for(unsigned i=0; i< constraints.size(); i++){
    if(constraints[i].type == UserInput::ConstraintInput::POINT){
      assert(constraints[i].timePoint >= 0);
      set.insert(constraints[i].timePoint);
    }
  }

  std::vector<double> grid;
  grid.assign(set.begin(), set.end());
  std::sort(grid.begin(), grid.end());
  return grid;
}

/**
* @brief calculate the complete integration grid (scaled)
*
* @param[in] input input struct containing information about the user grid
*            and all parameterization grids (only Single Stage!!!!)
* @return vector containing the complete integration grid
* @sa getUserGrid
*/
std::vector<double> InputConverter::getCompleteGrid(const FactoryInput::IntegratorMetaDataInput &input) const
{
  std::set<double> set;

  set.insert(input.userGrid.begin(), input.userGrid.end());

  ControlFactory controlFactory;
  std::vector<Control> c = controlFactory.createControlVector(input.controls);

  for(unsigned i=0; i<c.size(); i++){
    std::vector<double> controlGrid = c[i].getControlGridUnscaled();
    set.insert(controlGrid.begin(), controlGrid.end());
  }

  std::vector<double> grid;
  grid.assign(set.begin(), set.end());

  //constraint grid is scaled
  for(unsigned i=0; i<grid.size(); i++){
    grid[i] /= input.duration.value;
  }
  return grid;
}


/**
* @brief convert UserInput::EsoInput to EsoInput
*
* @param input EsoInput to be converted
* @return converted EsoInput
*/
struct FactoryInput::EsoInput InputConverter::convertEsoInput(const UserInput::EsoInput &input)
{
  if(input.model == ""){
    throw MissingModelNameException();
  }


  FactoryInput::EsoInput convertedInput;
  convertedInput.model = input.model;
  convertedInput.type = input.type;
  convertedInput.initialModel = input.initialModel;

#ifdef BUILD_WITH_FMU
  if (convertedInput.type == EsoType::FMI) {
    convertedInput.relFmuTolerance = input.relativeFmuTolerance;
  }
#endif //BUILD_WITH_FMU


  return convertedInput;
}

/**
* @brief convert UserInput::ParameterGridInput::ApproximationType into ControlType
*
* @param type Approximation type to be converted
* @return converted ControlType
*/
ControlType InputConverter::convertApproximationType
(const UserInput::ParameterGridInput::ApproximationType type) const
{
  ControlType returnType;
  switch(type){
  case UserInput::ParameterGridInput::PIECEWISE_CONSTANT:
    returnType = PieceWiseConstant;
    break;
  case UserInput::ParameterGridInput::PIECEWISE_LINEAR:
    returnType = PieceWiseLinear;
    break;
  default:
    assert(false);
  }

  return returnType;
}

/**
* @brief convert UserInput::ParameterGridInput into ParameterizationGridInput
*
* @param input ParameterGridInput to be converted
* @return converted ParmeterizationGridInput
* @throw NoGridDefinedException
*/
struct FactoryInput::ParameterizationGridInput InputConverter::convertParamGridInput
    (const UserInput::ParameterGridInput &input,
     const double lowerBound,
     const double upperBound)
    {
      FactoryInput::ParameterizationGridInput convertedInput;

      convertedInput.duration = input.duration;
      convertedInput.hasFreeDuration = input.hasFreeDuration;
      convertedInput.approxType = convertApproximationType(input.type);

      if(!input.timePoints.empty()){
        //check if the grid is monotonous and starts with 0.0 and ends with 1.0
        if(input.timePoints.front() != 0.0){
          throw WrongGridStructureException("parameterGridInput.timePoints.front() != 0.0");
        }
        for(unsigned i=0; i<input.timePoints.size()-1; i++){
          if(input.timePoints[i] >= input.timePoints[i+1]){
            throw WrongGridStructureException("parameterGridInput.timePoints["+boost::lexical_cast<std::string>(i)+
                                              "] >= parameterGridInput.timePoints["+boost::lexical_cast<std::string>(i+1)+"]");
          }
        }
        if(input.timePoints.back() != 1.0){
          throw WrongGridStructureException("parameterGridInput.timePoints.back() != 1.0");
        }
        convertedInput.timePoints = input.timePoints;
      }
      else{
        if(input.numIntervals == 0){
          throw NoGridDefinedException();
        }
        // we create an equidistant grid, if no explicit time points are given.
        convertedInput.timePoints.resize(input.numIntervals);
        for (unsigned i=0; i<input.numIntervals; i++){
          convertedInput.timePoints[i] = double(i)/input.numIntervals;
        }
        convertedInput.timePoints.push_back(1.0);
      }
    if((!input.upperBounds.empty() || !input.lowerBounds.empty()) && (input.adapt.maxAdaptSteps!=0))
        {
            //Individual bounds have not been tested for ADAPTATION procedures
            m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::ERROR_VERBOSITY, " Sincere warning: It has been detected that individual bounds have been set on grid level and adapt.maxAdaptSteps!=0. Individual bounds have not been implemented or tested for use with adaptation on the same parameter. The individual bounds will be ignored and the bounds on the whole parameter used!  If you have a need for this feature please contact a developer. " );
            // See also MetaDataFactories::createControl for how the individual bounds are forwarded.
        }

    //some individual bounds have been set
    if(!input.upperBounds.empty() || !input.lowerBounds.empty())
        {
            //both upper and lower bounds individually set?
            if(!input.upperBounds.empty() && !input.lowerBounds.empty())
                {
                    convertedInput.upperBounds=input.upperBounds;
                    convertedInput.lowerBounds=input.lowerBounds;
                }
            //only lower bounds set individually
            else if(input.upperBounds.empty())
                {
                    convertedInput.lowerBounds=input.lowerBounds;
                    convertedInput.upperBounds.resize(input.lowerBounds.size(),upperBound);
                }
            //only upper bounds individually set
            else if(input.lowerBounds.empty())
                {
                    convertedInput.upperBounds=input.upperBounds;
                    convertedInput.lowerBounds.resize(input.upperBounds.size(),lowerBound);
                }

            std::string typestring;
            unsigned neededSize;
            switch(input.type)
                {
                case UserInput::ParameterGridInput::PIECEWISE_CONSTANT:

                    typestring="PIECEWISE_CONSTANT";
                    neededSize=convertedInput.timePoints.size()-1;
                    break;
                case UserInput::ParameterGridInput::PIECEWISE_LINEAR:

                    typestring="PIECEWISE_LINEAR";
                    neededSize=convertedInput.timePoints.size();
                    break;
                default:
                    assert(false);
                }


            if(input.upperBounds.size()!=input.lowerBounds.size())
                throw WrongNumberOfParameterBoundValuesDefinedException(typestring,"lowerBounds",input.lowerBounds.size(),"upperBounds",input.upperBounds.size());
            if(input.upperBounds.size()!=neededSize)
                throw WrongNumberOfParameterBoundValuesDefinedException(typestring,"upperBounds",input.upperBounds.size(),"neededSize", neededSize);
        }
    if(!input.values.empty())
        {
        convertedInput.values = input.values;
        //check correctness of values size
        switch(input.type){
        case UserInput::ParameterGridInput::PIECEWISE_CONSTANT:
          if(input.values.size() != convertedInput.timePoints.size()-1){
            throw WrongNumberOfParameterValuesDefinedException("PIECEWISE_CONSTANT",input.values.size());
          }
          break;
        case UserInput::ParameterGridInput::PIECEWISE_LINEAR:
          if(input.values.size() != convertedInput.timePoints.size()){
            throw WrongNumberOfParameterValuesDefinedException("PIECEWISE_LINEAR", input.values.size());
          }
          break;
        default:
          assert(false);
        }//switch
      }//if(!values.empty)
    else if(input.lowerBounds.empty())
        {
        double meanValue = (upperBound - lowerBound) / 2 + lowerBound;
        switch(input.type){
        case UserInput::ParameterGridInput::PIECEWISE_CONSTANT:
          convertedInput.values.resize(convertedInput.timePoints.size()-1, meanValue);
          break;
        case UserInput::ParameterGridInput::PIECEWISE_LINEAR:
          convertedInput.values.resize(convertedInput.timePoints.size(), meanValue);
          break;
        default:
          assert(false);
        }//switch
        }//elseif(lowerBounds.empty)
    else
        {
            // use individual bounds if given!
            convertedInput.values.resize(convertedInput.lowerBounds.size());
            for(int i=0; i<convertedInput.lowerBounds.size(); i++)
                {

                    convertedInput.values[i]=0.5*(convertedInput.lowerBounds[i]+convertedInput.upperBounds[i]);
                }
        }



      convertedInput.pcresolution = input.pcresolution;
      convertedInput.adapt = convertAdaptationInput(input.adapt, convertedInput, lowerBound, upperBound);

      return convertedInput;
    }

    /**
* @brief convert UserInput::ParameterInput::ParameterSensitivityType
         into ParameterInput::ParameterSensitivityType

* @param type ParameterSensitivityType to be converted
* @return converted ParameterSensitivityType
*/
    FactoryInput::ParameterInput::ParameterSensitivityType InputConverter::convertParamSensType
    (const UserInput::ParameterInput::ParameterSensitivityType type) const
    {
      FactoryInput::ParameterInput::ParameterSensitivityType returnType;
      switch(type){
      case UserInput::ParameterInput::FULL:
        returnType = FactoryInput::ParameterInput::FULL;
        break;
      case UserInput::ParameterInput::FRACTIONAL:
        returnType = FactoryInput::ParameterInput::FRACTIONAL;
        break;
      case UserInput::ParameterInput::NO:
        returnType = FactoryInput::ParameterInput::NO;
        break;
      default:
        assert(false);
      }

      return returnType;
    }

    /**
* @brief covnert UserInput::ParameterInput::ParameterType into ParameterType::ParameterType
*
* @param type ParameterType to be converted
* @return converted ParameterType
*/
    FactoryInput::ParameterInput::ParameterType InputConverter::convertParamType
    (const UserInput::ParameterInput::ParameterType type) const
    {
      FactoryInput::ParameterInput::ParameterType returnType;
      switch(type){
      case UserInput::ParameterInput::INITIAL:
        returnType = FactoryInput::ParameterInput::INITIAL;
        break;
      case UserInput::ParameterInput::TIME_INVARIANT:
        returnType = FactoryInput::ParameterInput::TIME_INVARIANT;
        break;
      case UserInput::ParameterInput::PROFILE:
        returnType = FactoryInput::ParameterInput::CONTROL;
        break;
      case UserInput::ParameterInput::DURATION:
        returnType = FactoryInput::ParameterInput::DURATION;
        break;
      default:
        assert(false);
      }

      return returnType;
    }

    /**
* @brief convert UserInput::ParameterInput for a duration into a ParameterInput
*
* In addition to the functionality convertParameterInput the type is set to DURATION
* @param[in] input ParameterInput to be converted
* @return converted ParameterInput
*/
    struct FactoryInput::ParameterInput InputConverter::convertParameterInputForDuration
        (const UserInput::ParameterInput &input)
        {
          FactoryInput::ParameterInput durationInput = convertParameterInput(input);

          if(durationInput.type != FactoryInput::ParameterInput::DURATION){
            throw TypeOfDurationParameterNotSetToDURATIONExeption();
          }

          return durationInput;
        }

        /**
* @brief convert UserInput::ParameterInput to ParameterInput
*
* @param[in] input ParameterInput to be converted
* @return converted ParameterInput
*/
        struct FactoryInput::ParameterInput InputConverter::convertParameterInput(const UserInput::ParameterInput &input)
        {
          FactoryInput::ParameterInput paramInput;
          if(input.lowerBound==input.upperBound && input.sensType == UserInput::ParameterInput::FULL){
            paramInput.sensType=convertParamSensType(UserInput::ParameterInput::FRACTIONAL);
            m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::STANDARD_VERBOSITY, "Warning: parameterInput.lowerBound == parameterInput.upperBound\nparamInput.sensType is set to FRACTIONAL\n" );
          }
          else
            paramInput.sensType = convertParamSensType(input.sensType);

          paramInput.lowerBound = input.lowerBound;
          paramInput.upperBound = input.upperBound;
          paramInput.value = input.value;
          paramInput.type = convertParamType(input.paramType);

          // we expect the input.grids not to be empty, if a control parameter is handled
          const unsigned numGrids = input.grids.size();
          paramInput.grids.resize(numGrids);
          for(unsigned i=0; i<numGrids; i++){
            try{
              paramInput.grids[i] = convertParamGridInput(input.grids[i], paramInput.lowerBound, paramInput.upperBound);
            }
            catch(InputException e){
              std::string message = "For parameter: ";
              message.append(input.name);
              message.append("\n");
              message.append(e.what());
              throw InputException(message);
            }
          }

          return paramInput;
        }

        /**
* @brief convert UserInput::Input::RunningMode to FactoryInput::MappingInput::MappingType
*
* @param[in] input running mode to be converted
* @return converted FactoryInput::MappingInput::MappingType
*/
        FactoryInput::MappingInput::MappingType InputConverter::convertMappingType
        (const UserInput::Input::RunningMode &mappingType) const
        {
          FactoryInput::MappingInput::MappingType convertedType;
          switch(mappingType){
          case UserInput::Input::SIMULATION:
          case UserInput::Input::SINGLE_SHOOTING:
            convertedType = FactoryInput::MappingInput::SINGLE_SHOOTING;
            break;
          case UserInput::Input::MULTIPLE_SHOOTING:
            convertedType = FactoryInput::MappingInput::MULTIPLE_SHOOTING;
            break;
          default:
            assert(false);
          }
          return convertedType;
        }


        /**
* @brief convert UserInput::StageMapping to FactoryInput::MappingInput
*
* @param[in] input running mode to be converted
* @param[in] UserInput::StageMapping mappingInput
* @param[in] first FactoryInput::IntegratorMetaDataInput
* @param[in] second FactoryInput::IntegratorMetaDataInput
* @return converted FactoryInput::MappingInput::MappingType
*/
        FactoryInput::MappingInput InputConverter::convertMappingInput(const UserInput::Input::RunningMode &mode,
                                                                       const UserInput::StageMapping mappingInput,
                                                                       const FactoryInput::IntegratorMetaDataInput &imdInputStage1,
                                                                       const FactoryInput::IntegratorMetaDataInput &imdInputStage2)
        {
          FactoryInput::MappingInput convertedInput;
          const GenericEso::Ptr esoStage1Ptr = imdInputStage1.esoPtr;
          const GenericEso::Ptr esoStage2Ptr = imdInputStage2.esoPtr;

          convertedInput.type = convertMappingType(mode);
          // assuming the models are identical the states of the first stage
          // are simply mapped to the same indices
          if(mappingInput.fullStateMapping)
          {
            const unsigned numStates = esoStage1Ptr->getNumStates();
            if(numStates != (unsigned)esoStage2Ptr->getNumStates()){
              throw IncompatibleStagesForFullStateMappingException("Numbers of states", numStates,
                                                                   (unsigned)esoStage2Ptr->getNumStates());
            }
            std::vector<int> stateIndices(numStates);
            esoStage1Ptr->getStateIndex(numStates, &stateIndices[0]);
            for(unsigned i=0; i<numStates; i++){
              convertedInput.varIndexMap[stateIndices[i]] = stateIndices[i];
            }
            const unsigned numEquations = esoStage1Ptr->getNumEquations();
            if(numEquations != (unsigned)esoStage2Ptr->getNumEquations()){
              throw IncompatibleStagesForFullStateMappingException("Numbers of equations", numEquations,
                                                                   (unsigned)esoStage2Ptr->getNumEquations());
            }
            for(unsigned i=0; i<numEquations; i++){
              convertedInput.eqnIndexMap[i] = i;
            }
          }
          else{
            std::map<std::string, std::string>::iterator stateMapIt;
            std::map<std::string, std::string> snMap = mappingInput.stateNameMapping;
            std::vector<std::string> esoNamesToBeMapped, esoNamesMapped;
            getEsoNames(esoNamesToBeMapped, imdInputStage1.esoPtr);
            getEsoNames(esoNamesMapped, imdInputStage2.esoPtr);
            for(stateMapIt = snMap.begin(); stateMapIt != snMap.end(); stateMapIt++){
              const int esoIndexToBeMapped = getEsoIndex(esoNamesToBeMapped,stateMapIt->first);
              const int mappedEsoIndex = getEsoIndex(esoNamesMapped, stateMapIt->second);
              convertedInput.varIndexMap[esoIndexToBeMapped] = mappedEsoIndex;

              const int eqnIndexToBeMapped = getEquationIndexOfVariable(esoIndexToBeMapped,
                                                                        imdInputStage1.esoPtr);
              const int mappedEqnIndex = getEquationIndexOfVariable(mappedEsoIndex,
                                                                    imdInputStage2.esoPtr);
              convertedInput.eqnIndexMap[eqnIndexToBeMapped] = mappedEqnIndex;
            }
          }
          return convertedInput;
        }

        /**
* @brief create mapping input vector
*
* @param[in] input running mode to be converted
* @param[in] vector of converted FactoryInput::IntegratorMetaDataInput stages
* @param[in] vector of converted UserInput::StageInput stages
* @return created mapping vector
*/
        std::vector<FactoryInput::MappingInput> InputConverter::createMappingInputVector
        (const UserInput::Input::RunningMode &mode,
         const std::vector<FactoryInput::IntegratorMetaDataInput> &convertedStages,
         const std::vector<UserInput::StageInput> &stages)
        {
          assert(!convertedStages.empty());
          assert(convertedStages.size() == stages.size());
          const unsigned numMappings = stages.size()-1;
          std::vector<FactoryInput::MappingInput> mappingVector(numMappings);
          for(unsigned i=0; i<numMappings; i++){
            mappingVector[i] = convertMappingInput(mode,
                                                   stages[i].mapping,
                                                   convertedStages[i],
                                                   convertedStages[i+1]);
          }
          return mappingVector;
        }

        /**
* @brief convert UserInput::IntegratorInput::IntegrationOrder
*        into IntegratorInput::IntegrationOrder
*
* @param[in] order IntegrationOrder to be converted
* @return converted IntegrationOrder
*/
        FactoryInput::IntegratorInput::IntegrationOrder InputConverter::convertIntegrationOrder
        (const UserInput::IntegratorInput::IntegrationOrder order) const
        {
          FactoryInput::IntegratorInput::IntegrationOrder returnOrder;
          switch(order){
          case UserInput::IntegratorInput::ZEROTH:
            returnOrder = FactoryInput::IntegratorInput::ZEROTH;
            break;
          case UserInput::IntegratorInput::FIRST_FORWARD:
            returnOrder = FactoryInput::IntegratorInput::FIRST_FORWARD;
            break;
          case UserInput::IntegratorInput::FIRST_REVERSE:
            returnOrder = FactoryInput::IntegratorInput::FIRST_REVERSE;
            break;
          case UserInput::IntegratorInput::SECOND_REVERSE:
            returnOrder = FactoryInput::IntegratorInput::SECOND_REVERSE;
            break;
          default:
            assert(false);
          }

          return returnOrder;
        }

        /**
* @brief convert UserInput::IntegratorInput::IntegratorType into IntegratorInput::IntegratorType
*
* @param[in] type IntegratorType to be converted
* @return converted IntegratorType
*/
        FactoryInput::IntegratorInput::IntegratorType InputConverter::convertIntegratorType
        (const UserInput::IntegratorInput::IntegratorType type) const
        {
          FactoryInput::IntegratorInput::IntegratorType returnType;
          switch(type){
          case UserInput::IntegratorInput::LIMEX:
            returnType = FactoryInput::IntegratorInput::LIMEX;
            break;
          case UserInput::IntegratorInput::NIXE:
            returnType = FactoryInput::IntegratorInput::NIXE;
            break;
          case UserInput::IntegratorInput::IDAS:
            returnType = FactoryInput::IntegratorInput::IDAS;
            break;
          default:
            assert(false);
          }

          return returnType;
        }

        /**
* @brief create an IntegratorMetaDataInput struct out of an IntegratorInput
*
* @param[in] input IntegratorStageInput containing all information needed to create the
*              IntegratorMetaDataInput struct for one single stage
* @param[in] eso EsoInptut needed for call of function getEsoIndex
* @return created IntegratorMetaDataInput struct
*/
        struct FactoryInput::IntegratorMetaDataInput InputConverter::createIntegratorMetaDataInputStage
            (const UserInput::IntegratorStageInput &input,
             const GenericEso::Ptr esoPtr)
            {
              FactoryInput::IntegratorMetaDataInput metaDataInput;
              metaDataInput.esoPtr = esoPtr;
              metaDataInput.duration = convertParameterInputForDuration(input.duration);
              metaDataInput.plotGridResolution = input.plotGridResolution;
              metaDataInput.explicitPlotGrid = input.explicitPlotGrid;
              checkExplicitPlotGrid(input.explicitPlotGrid);

              if(input.plotGridResolution == 0){
                throw SetZeroForPlotGridResolutionException();
              }
              if(input.plotGridResolution > 1e4){
                std::stringstream sstr;
                sstr<<"Warning: set "<<input.plotGridResolution<<" as plot grid resolution. This might slow down the final ";
                sstr<<"state integration significantly.";
                m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY,sstr.str());
              }

              const unsigned numParams = input.parameters.size();
              checkParameterNames(input.parameters);
              std::vector<std::string> esoNames;
              getEsoNames(esoNames, metaDataInput.esoPtr);
              for(unsigned i=0; i<numParams; i++){
                FactoryInput::ParameterInput currentParamInput;
                currentParamInput = convertParameterInput(input.parameters[i]);
                //eso index is not set during conversion
                currentParamInput.esoIndex = getEsoIndex(esoNames, input.parameters[i].name);

                std::vector<double> timepoints(2);
                std::vector<double> values(1);
                FactoryInput::ParameterizationGridInput gridinput;
                switch(currentParamInput.type){
                case FactoryInput::ParameterInput::CONTROL:
                  // at least one grid has tp be set for a control
                  if(currentParamInput.grids.empty()){
                    throw NoGridsDefinedOnControlException();
                  }
                  // duration must be properly set to global final time, because input module currently
                  // constructs controls in order to get the complete parameterization grid.
                  currentParamInput.grids.back().duration = metaDataInput.duration.value;
                  //last duration on grid must be duration and not global final time value
                  if(currentParamInput.grids.size() > 1){
                    double gridDurationSum = 0.0;
                    for(unsigned j=0; j<currentParamInput.grids.size() - 1; j++){
                      gridDurationSum += currentParamInput.grids[j].duration;
                    }
                    currentParamInput.grids.back().duration -= gridDurationSum;
                  }
                  metaDataInput.controls.push_back(currentParamInput);
                  break;
                case FactoryInput::ParameterInput::TIME_INVARIANT:
                  currentParamInput.grids.clear(); // make sure grid has not been set yet
                  gridinput.approxType = PieceWiseConstant;
                  timepoints[0] = 0.0;
                  timepoints[1] = metaDataInput.duration.value;
                  values[0] = currentParamInput.value;
                 //this second value is never used and prevents checks on right value lenght e.g. for custom objective.
                 //   values[1] = currentParamInput.value;
                  gridinput.timePoints = timepoints;
                  gridinput.values = values;
                  gridinput.hasFreeDuration = false;
                  currentParamInput.grids.push_back(gridinput);
                  currentParamInput.grids.back().duration = metaDataInput.duration.value;
                  metaDataInput.controls.push_back(currentParamInput);
                  break;
                case FactoryInput::ParameterInput::INITIAL:
                  currentParamInput.grids.clear();
                  metaDataInput.initials.push_back(currentParamInput);
                  break;
                default:
                  assert(false);
                }// switch
              }
              // for

              return metaDataInput;
            }




            /**
* @brief create an IntegratorMetaDataInput struct out of stage input vector
*
* @param[in] input the running mode of input
* @param[in] vector of stage input structures
* @return created IntegratorMetaDataInput struct
*/
            struct FactoryInput::IntegratorMetaDataInput InputConverter::createIntegratorMetaDataInput
                (const UserInput::Input::RunningMode &mode,
                 const UserInput::IntegratorInput::IntegrationOrder order,
                 const std::vector<UserInput::StageInput> &stageInputVector)
                {
                  FactoryInput::IntegratorMetaDataInput metaDataInput;
                  //metaDataInput.mappings
                  assert(stageInputVector.size()>0);
                  if(!stageInputVector.back().mapping.stateNameMapping.empty()){
                    throw LastStageHasMappingDefinedException();
                  }
                  else if(stageInputVector.back().mapping.fullStateMapping){
                    throw LastStageHasMappingDefinedException();
                  }
                  metaDataInput.stages.resize(stageInputVector.size());
                  GenericEsoFactory genEsoFac;
                  for(unsigned i=0; i<stageInputVector.size(); i++){
                    GenericEso::Ptr &esoPtr = const_cast<GenericEso::Ptr&>(stageInputVector[i].esoPtr);
                    const UserInput::EsoInput &esoInput = stageInputVector[i].eso;
                    if (esoPtr == 0 || esoInput.model != esoPtr->getModel()) {
                      const FactoryInput::EsoInput eso = convertEsoInput(esoInput);
                      if (order == UserInput::IntegratorInput::SECOND_REVERSE)
                        esoPtr = genEsoFac.create2ndOrder(eso);
                      else
                        esoPtr = genEsoFac.create(eso);
                    }
                    metaDataInput.stages[i] = createIntegratorMetaDataInputStage
                        (stageInputVector[i].integrator, esoPtr);
                  }
                  metaDataInput.mappings = createMappingInputVector(mode, metaDataInput.stages, stageInputVector);
                  return metaDataInput;
                }

                /**
* @brief convert UserInput::LinearSolverInput::SolverType to FactoryInput::LinearSolverInput::SolverType 
*
* @param[in] type of linear solver input
* @return converted factory linear solver input type
*/
                FactoryInput::LinearSolverInput::SolverType InputConverter::convertSolverType(
                    const UserInput::LinearSolverInput::SolverType &type) const
                {
                  FactoryInput::LinearSolverInput::SolverType convertedType;
                  switch(type){
#ifdef BUILD_LINSOL_MA28
                  case UserInput::LinearSolverInput::MA28:
                    convertedType = FactoryInput::LinearSolverInput::MA28;
                    break;
#endif

                  case UserInput::LinearSolverInput::KLU:
                    convertedType = FactoryInput::LinearSolverInput::KLU;
                    break;
                  default:
                    assert(false);
                  }
                  return convertedType;
                }

                /**
* @brief convert UserInput::LinearSolverInput to FactoryInput::LinearSolverInput
*
* @param[in] user linear solver input
* @return converted factory linear solver input 
*/
                FactoryInput::LinearSolverInput InputConverter::convertLinearSolverInput(
                    const UserInput::LinearSolverInput &input) const
                {
                  FactoryInput::LinearSolverInput convertedInput;
                  convertedInput.type = convertSolverType(input.type);
                  return convertedInput;
                }

                /**
* @brief convert UserInput::NonLinearSolverInput::SolverType to FactoryInput::NonLinearSolverInput::SolverType 
*
* @param[in] user nonlinear solver input type
* @return converted factory nonlinear solver input type
*/
                FactoryInput::NonLinearSolverInput::SolverType InputConverter::convertSolverType(
                    const UserInput::NonLinearSolverInput::SolverType &type) const
                {
                  FactoryInput::NonLinearSolverInput::SolverType convertedType;
                  switch(type){
#ifdef BUILD_NLEQ1S
                  case UserInput::NonLinearSolverInput::NLEQ1S:
                    convertedType = FactoryInput::NonLinearSolverInput::NLEQ1S;
                    break;
#endif

                  case UserInput::NonLinearSolverInput::CMINPACK:
                    convertedType = FactoryInput::NonLinearSolverInput::CMINPACK;
                    break;
                  default:
                    assert(false);
                  }
                  return convertedType;
                }

                /**
* @brief convert UserInput::NonLinearSolverInput to FactoryInput::NonLinearSolverInput
*
* @param[in] user nonlinear solver input
* @return converted factory nonlinear solver input
*/
                FactoryInput::NonLinearSolverInput InputConverter::convertNonLinearSolverInput(
                    const UserInput::NonLinearSolverInput &input) const
                {
                  FactoryInput::NonLinearSolverInput convertedInput;
                  convertedInput.type = convertSolverType(input.type);
                  convertedInput.tolerance = input.tolerance;
                  return convertedInput;
                }

                /**
* @brief convert UserInput::DaeInitializationInput::DaeInitializationType to FactoryInput::DaeInitializationInput::DaeInitializationType
*
* @param[in] user dae initialization input type
* @return converted factory dae initialization input type
*/
                FactoryInput::DaeInitializationInput::DaeInitializationType
                InputConverter::convertDaeInitializationType(
                    const UserInput::DaeInitializationInput::DaeInitializationType &type) const
                {
                  FactoryInput::DaeInitializationInput::DaeInitializationType convertedType;
                  switch(type){
                  case UserInput::DaeInitializationInput::NO:
                    convertedType = FactoryInput::DaeInitializationInput::NO;
                    break;
                  case UserInput::DaeInitializationInput::FULL:
                    convertedType = FactoryInput::DaeInitializationInput::FULL;
                    break;
                  case UserInput::DaeInitializationInput::BLOCK:
                    convertedType = FactoryInput::DaeInitializationInput::BLOCK;
                    break;
#ifdef BUILD_WITH_FMU
                  case UserInput::DaeInitializationInput::FMI:
                    convertedType = FactoryInput::DaeInitializationInput::FMI;
                    break;
#endif
                  default:
                    assert(false);
                  }
                  return convertedType;
                }

                /**
* @brief convert UserInput::DaeInitializationInput to FactoryInput::DaeInitializationInput
*
* @param[in] user dae initialization input
* @return converted factory dae initialization input
*/
                FactoryInput::DaeInitializationInput InputConverter::convertDaeInitializationInput(
                    const UserInput::DaeInitializationInput &input) const
                {
                  FactoryInput::DaeInitializationInput convertedInput;
                  convertedInput.linSolver = convertLinearSolverInput(input.linSolver);
                  convertedInput.nonLinSolver = convertNonLinearSolverInput(input.nonLinSolver);
                  convertedInput.type = convertDaeInitializationType(input.type);
                  convertedInput.maximumErrorTolerance = input.maximumErrorTolerance;
                  return convertedInput;
                }

                /**
* @brief convert UserInput::IntegratorInput into IntegratorInput
*
* @param[in] input IntegratorInput to be converted
* @param[in] eso EsoInput needed for call of createIntegratorMetaDataInput
* @return converted IntegratorInput
*/
                struct FactoryInput::IntegratorInput InputConverter::createIntegratorInput
                    (const UserInput::Input::RunningMode &mode,
                     const UserInput::IntegratorInput &input,
                     const std::vector<UserInput::StageInput> &stageInputVector)
                    {
                      FactoryInput::IntegratorInput integratorInput;

                      integratorInput.integratorOptions = input.integratorOptions;
                      integratorInput.order = convertIntegrationOrder(input.order);
                      integratorInput.type = convertIntegratorType(input.type);
                      integratorInput.daeInitInput = convertDaeInitializationInput(input.daeInit);

                      integratorInput.metaDataInput = createIntegratorMetaDataInput(mode, input.order, stageInputVector);

                      return integratorInput;
                    }

                    /**
* @brief convert UserInput::ConstraintInput::ConstraintType to FactoryInput::ConstraintInput::ConstraintType
*
* @param[in] user input constraint type
* @return converted factory input constraint type
*/
                    FactoryInput::ConstraintInput::ConstraintType InputConverter::convertConstraintType(
                        const UserInput::ConstraintInput::ConstraintType type) const
                    {
                      FactoryInput::ConstraintInput::ConstraintType convertedType;
                      switch(type){
                      case UserInput::ConstraintInput::ENDPOINT:
                        convertedType = FactoryInput::ConstraintInput::ENDPOINT;
                        break;
                      case UserInput::ConstraintInput::PATH:
                        convertedType = FactoryInput::ConstraintInput::PATH;
                        break;
                      case UserInput::ConstraintInput::POINT:
                        convertedType = FactoryInput::ConstraintInput::POINT;
                        break;
                      case UserInput::ConstraintInput::MULTIPLE_SHOOTING:
                        convertedType = FactoryInput::ConstraintInput::MULTIPLE_SHOOTING;
                        break;
                      default:
                        assert(false);
                      }
                      return convertedType;
                    }

                    /**
* @brief convert UserInput::ConstraintInput into ConstraintInput
*
* @param[in] input ConstraintInput to be converted
* @param[in] imdInput IntegratorMetaDataInput needed for call of function getEquationIndex
* @param[in] eso EsoInput needed for calls to getEsoIndex and getEquationIndex
* @return converted ConstraintInput
*/
                    struct FactoryInput::ConstraintInput InputConverter::convertConstraintInput
                        (const UserInput::ConstraintInput &input,
                         const FactoryInput::IntegratorMetaDataInput &imdInput,
                         const GenericEso::Ptr &eso)
                        {
                          FactoryInput::ConstraintInput constraint;
                          std::vector<std::string> esoNames;
                          getEsoNames(esoNames,eso);
                          constraint.esoIndex = getEsoIndex(esoNames, input.name);
                          constraint.lowerBound = input.lowerBound;
                          constraint.upperBound = input.upperBound;
                          constraint.timePoint = input.timePoint;
                          if(constraint.timePoint < 0.0 || constraint.timePoint > 1.0){
                            throw TimePointNotAllowedException();
                          }
                          constraint.lagrangeMultiplier = input.lagrangeMultiplier;
                          if( !input.lagrangeMultiplierVector.empty()){
                            switch(input.type){
                            case UserInput::ConstraintInput::POINT:
                            case UserInput::ConstraintInput::ENDPOINT:
                              throw LagrangeMultiplierVectorForNonPathConstraintDefinedException();
                              break;
                            case UserInput::ConstraintInput::PATH:
                              if(getCompleteGrid(imdInput).size() != input.lagrangeMultiplierVector.size()+1){
                                throw WrongNumberOfLagrangeMultipliersException();
                              }
                              break;
                            case UserInput::ConstraintInput::MULTIPLE_SHOOTING:
                              break;
                            default:
                              assert(false);
                            }
                          }
                          constraint.lagrangeMultiplierVector = input.lagrangeMultiplierVector;
                          constraint.type = convertConstraintType(input.type);
                          constraint.excludeZero = input.excludeZero;
                          return constraint;
                        }

                        /**
* @brief create a vector of ConstraintInputs out of a ConstraintInput of a path constraint
*
* The Constraint class does not distinguish path constraints from point constraints. A path
* constraint is a vector of point constraints with a point constraint defined for each point
* on the entire integration grid.
* @param[in] constraint ConstraintInput to be copied for the path constraint
* @param[in] completeGrid all timepoints of the complete integration grid
* @return ConstraintInput vector representing the path constraint
*/
                        std::vector<FactoryInput::ConstraintInput> InputConverter::createPathConstraint
                        (const FactoryInput::ConstraintInput constraint,
                         const std::vector<double> &completeGrid) const
                        {
                          //gridpoint at 0.0 is left out for path constraints at the moment
                          const unsigned numConstraintPoints = completeGrid.size() - 1;
                          std::vector<FactoryInput::ConstraintInput> pathConstraint(numConstraintPoints);
                          for(unsigned i=0; i<numConstraintPoints; i++){
                            pathConstraint[i] = constraint;
                            pathConstraint[i].timePoint = completeGrid[i+1];
                          }
                          return pathConstraint;
                        }

                        /**
* @brief convert a vector of UserInput::ConstraintInput structs into a ConstraintInput vector
* @param[in] inputVector ConstraintInput vector to be converted
* @param[in] imdInput IntegratorMetaDataInput needed for calls of function convertConstraintInput
* @param[in] eso EsoInput needed for calls of function convertConstraintInput
* @param[in] completeGrid needed for calls of function createPathConstraint
* @return converted ConstraintInput vector
*/
                        std::vector<FactoryInput::ConstraintInput> InputConverter::convertConstraintInputVector(
                            const std::vector<UserInput::ConstraintInput> &inputVector,
                            const FactoryInput::IntegratorMetaDataInput &imdInput,
                            const GenericEso::Ptr &eso)
                        {
                          std::vector<FactoryInput::ConstraintInput> constraints(inputVector.size());
                          for(unsigned i=0; i<inputVector.size(); i++){
                            constraints[i] = convertConstraintInput(inputVector[i], imdInput, eso);
                          }
                          return constraints;
                        }

                        /**
* @brief merge constraints with multiple occurrence
*
* The constraint vector may contain (by defining path and point constraints for
* the same variable mainly) constraints that have the same time point and the
* same eso index. In that case the constraints are merged into one point
* constraint using the strictest bounds.
* @param[in,out] constraints ConstraintInput vector in which the constraints with
*                multiple occurrence are merged
*/
                        void InputConverter::removeDoubleConstraints(std::vector<FactoryInput::ConstraintInput> &constraints) const
                        {
                          for(unsigned i=0; i<constraints.size(); i++){
                            for(unsigned j=i+1; j<constraints.size(); j++){
                              if(constraints[i].esoIndex == constraints[j].esoIndex){
                                //! @todo use a "close" operator instead of "=="
                                if(constraints[i].timePoint == constraints[j].timePoint){
                                  constraints[i].lowerBound = max(constraints[i].lowerBound, constraints[j].lowerBound);
                                  constraints[i].upperBound = min(constraints[i].upperBound, constraints[j].upperBound);
                                  constraints.erase(constraints.begin() + j);
                                }
                              }
                            }
                          }
                        }

                        /**
* @brief create an OptimizerMetaDataInput struct out of an UserInput::OptimizerInput
*
* @param[in] input OptimizerInput used to create the OptimizerMetaDataInput
* @param[in,out] imdInput IntegratorMetaDataInput used for calls of functions
*                         getCompleteGrid and convertConstraintInputVector.
*                         Also the field imdInput.userGrid is set in this function.
* @param[in] lastInput eso EsoInput needed for calls of functions checkConstraintNames and
*                      convertConstraintInputVector
* @param[in] treatObjective test objective
* @return created factory optimizer metadata input
*/
#include "UserInput.hpp"
                        FactoryInput::OptimizerMetaDataInput InputConverter::createOptimizerMetaDataInputStage
                        (const UserInput::OptimizerStageInput &input,
                         FactoryInput::IntegratorMetaDataInput &imdInput,
                         const UserInput::OptimizerStageInput &lastInput,
                         const bool treatObjective)
                        {
                          FactoryInput::OptimizerMetaDataInput optMetaDataInput;

                          std::vector<double> userGrid = getUserGrid(input.constraints);
                          imdInput.userGrid = userGrid;

                          checkConstraintNames(input.constraints, imdInput, imdInput.esoPtr);
                          // check if endpoint constraints (especially the objective) are directly dependent on controls only
                          for(unsigned i=0; i<input.constraints.size(); i++){
                            checkConstraintForIndependency(input.constraints[i], imdInput);
                          }

                          //in check functions only endpoint constraints are checked. So make sure the objective has type ENDPOINT
                          UserInput::ConstraintInput objectiveCopy = input.objective;
                          objectiveCopy.type = UserInput::ConstraintInput::ENDPOINT;
                          checkConstraintForIndependency(objectiveCopy, imdInput);
                          std::vector<FactoryInput::ConstraintInput> constraints = convertConstraintInputVector(input.constraints,
                                                                                                                imdInput,
                                                                                                                imdInput.esoPtr);

                          optMetaDataInput.constraints = constraints;

                          if(treatObjective){
                            optMetaDataInput.objective = convertConstraintInput(input.objective, imdInput, imdInput.esoPtr);
                          }
                          checkObjectiveName(input.constraints, input.objective);
                          optMetaDataInput.objective.timePoint = 1.0;

    //check custom objective function with penalty information
    if(input.customObjectiveInformation.penaltyFunction)//custom penalty was set
        {
			
            //check requestedListOfParameters has only valid requests inside
            for( const std::tuple<std::string /*varName*/, unsigned /*gridNum*/,unsigned /*totalNumberOfParametersInGrid*/>& requestedTuple: input.customObjectiveInformation.requestedListOfParameterizationGrids)
                {
                    //check maxStructureSteps=0 for stage
                    if( input.structureDetection.maxStructureSteps!=0)
                        {
                            throw InputException("Stages with custom objective functions can not use structure detection when parameterizations are requested!");

                        }
                    std::string varName=std::get<0>(requestedTuple);
                    unsigned gridNum   =std::get<1>(requestedTuple);
                    unsigned paramGridSize=std::get<2>(requestedTuple);
                    unsigned esoIndex=imdInput.esoPtr->getEsoIndexOfVariable(varName);
                    if(esoIndex==-1)//not found
                        {
                            throw VariableNotFoundException(varName);
                        }
                    std::vector<FactoryInput::ParameterInput>::const_iterator requestedControl=std::find_if(imdInput.controls.begin(),imdInput.controls.end(),[esoIndex](FactoryInput::ParameterInput& r)
                    {
                        return r.esoIndex==esoIndex;
                    });

                    //User penalizes specific parameterization values in the vector of all such values in the parameterization grid
                    //Adaptation would mean, that the user can no longer refer the specific parameterization values by their index since additional parameters would be inserted.
                    //To enable a custom penalty together with adaptation, the request of control values at specific time points would be required. The request is already mostly prepatered, but
                    //extracting the derivatives of a control value with respect to an parameterization value from OriginalParameterization would need to be implemented. Note that the use of relative times
                    //together with the possiblity of free durations of parameterization grids makes this rather complicated.
                    if(requestedControl->grids[gridNum].adapt.maxAdaptSteps>1)
                        {
                            throw InputException("Grids used in custom penalty can not be adaptively refined");
                        }
                    if(requestedControl->type!=FactoryInput::ParameterInput::ParameterType::CONTROL && requestedControl->type!=FactoryInput::ParameterInput::ParameterType::TIME_INVARIANT
                            &&requestedControl->type!=FactoryInput::ParameterInput::ParameterType::CONTINUOUS_CONTROL)
                        {
                            throw InputException("Penalty can only use control parameterization values.");
                        }
                    if(requestedControl->grids.size()<=gridNum)
                        {
                            throw InputException("For custom objective/penalty: requested grid index of variable '"+ varName + "' is out of range.");
                        }
                    if(requestedControl->grids[gridNum].values.size()!=paramGridSize)
                        {
                                std::string typeName;
                                switch (requestedControl->type)
                                {
                                        case (FactoryInput::ParameterInput::TIME_INVARIANT):
                                                typeName = "Time-Invariant";
                                                break;
                                        case (FactoryInput::ParameterInput::ParameterType::CONTROL):
                                                typeName = "Control";
                                        case (FactoryInput::ParameterInput::ParameterType::CONTINUOUS_CONTROL):
                                                typeName = "Continous-Control";
                                                break;
                                            default:
                                                typeName = "Unknown";
                                }

                            throw InputException("For custom objective/penalty: requested grid size of variable '"+ varName +"' of grid with index " + std::to_string(gridNum) + " is not equal to the actual number of parameterization values of " + std::to_string(requestedControl->grids[gridNum].values.size()) + "(requested was a grid of size "+std::to_string(paramGridSize)+"). "+"ParamType is"+ typeName);
                        }
                }
        }
    else
        {
            if(!input.customObjectiveInformation.requestedListOfParameterizationGrids.empty())
                {
                    throw InputException("Requested parameterization values for custom objective/penalty without setting a custom penalty. Note that custom  penalty  functions can not be loaded from JSON/XML files." );
                }
        }
    optMetaDataInput.customObjectiveInformation.penalty=input.customObjectiveInformation.penaltyFunction;
    optMetaDataInput.customObjectiveInformation.requestedListOfParameterizationGrids=input.customObjectiveInformation.requestedListOfParameterizationGrids;
    optMetaDataInput.customObjectiveInformation.penaltyFactor=input.customObjectiveInformation.penaltyFactor;
    optMetaDataInput.customObjectiveInformation.originalObjectiveFactor=input.customObjectiveInformation.originalObjectiveFactor;
                          optMetaDataInput.structureDetection.maxStructureSteps = input.structureDetection.maxStructureSteps;
                          optMetaDataInput.structureDetection.createContinuousGrids = input.structureDetection.createContinuousGrids;



                          return optMetaDataInput;
                        }

                        /**
* @brief create an OptimizerMetaDataInput struct out of an UserInput::OptimizerInput
*
* @param[in] input OptimizerInput used to create the OptimizerMetaDataInput
* @param[in,out] imdInput IntegratorMetaDataInput used for calls of functions
*                         getCompleteGrid and convertConstraintInputVector.
*                         Also the field imdInput.userGrid is set in this function.
* @param[in] stage input vector
* @return created factory optimizer metadata input
*/
                        FactoryInput::OptimizerMetaDataInput InputConverter::createOptimizerMetaDataInput(
                            const UserInput::OptimizerInput &input,
                            FactoryInput::IntegratorMetaDataInput &imdInput,
                            const std::vector<UserInput::StageInput> &stageInputVector)
                        {
                          FactoryInput::OptimizerMetaDataInput optMetaDataInput;

                          // convert global constraints
                          // it is not yet clear, how global constraints are to be identified with
                          // Variables and equations. Since global constraints are not implemented yet
                          // just use first stage as dummy eso and an empty dummy vector for completeGrid
                          std::vector<FactoryInput::ConstraintInput> constraints = convertConstraintInputVector(input.globalConstraints,
                                                                                                                imdInput,
                                                                                                                imdInput.stages.front().esoPtr);

                          optMetaDataInput.constraints = constraints;

                          const unsigned numStages = stageInputVector.size();
                          optMetaDataInput.treatObjective.resize(numStages);
                          optMetaDataInput.stages.resize(numStages);

                          UserInput::OptimizerStageInput dummyOptimizerStageForObjective;
                          if(input.type != UserInput::OptimizerInput::SENSITIVITY_INTEGRATION){
                            //do not check for sensitivity integrations
                            bool foundObjective = false;
                            for(unsigned i=0; i<numStages; i++){
                              if(stageInputVector[i].treatObjective){
                                optMetaDataInput.treatObjective[i] = true;
                                dummyOptimizerStageForObjective = stageInputVector[i].optimizer;
                                foundObjective = true;
                              }
                            }
                            if(!foundObjective){
                              throw NoObjectiveDefinedException();
                            }
                          }
                          bool foundConstraint = false;
                          for(unsigned i=0; i<numStages; i++){
                            if(!stageInputVector[i].optimizer.constraints.empty()){
                              foundConstraint = true;
                              break;
                            }
                          }
                          if(!foundConstraint && constraints.empty()){
                            throw NoConstraintDefinedException();
                          }

                          for(unsigned i=0; i<numStages; i++){
                            optMetaDataInput.stages[i] = createOptimizerMetaDataInputStage(stageInputVector[i].optimizer,
                                                                                           imdInput.stages[i],
                                                                                           dummyOptimizerStageForObjective,
                                                                                           stageInputVector[i].treatObjective);
                          }
                          return optMetaDataInput;
                        }

                        /**
* @brief convert UserInput::OptimizerInput::OptimizerType into OptimizerInput::OptimizerType
*
* @param[in] type OptimizerType to be converted
* @return converted OptimizerType
*/
                        FactoryInput::OptimizerInput::OptimizerType InputConverter::convertOptimizerType
                        (const UserInput::OptimizerInput::OptimizerType &type) const
                        {
                          FactoryInput::OptimizerInput::OptimizerType convertedType;
                          switch(type){
                          case UserInput::OptimizerInput::SNOPT:
                            convertedType = FactoryInput::OptimizerInput::SNOPT;
                            break;
                          case UserInput::OptimizerInput::IPOPT:
                            convertedType = FactoryInput::OptimizerInput::IPOPT;
                            break;
                          case UserInput::OptimizerInput::NPSOL:
                            convertedType = FactoryInput::OptimizerInput::NPSOL;
                            break;
                          case UserInput::OptimizerInput::FILTER_SQP:
                            convertedType = FactoryInput::OptimizerInput::FILTER_SQP;
                            break;
                          case UserInput::OptimizerInput::SENSITIVITY_INTEGRATION:
                            convertedType = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
                            break;
                          default:
                            assert(false);
                          }

                          return convertedType;
                        }

                        /**
* @brief create an OptimizerInput out of an UserInput::OptimizerInput and an IntegratorInput
*
* @param[in] optimizerInput used to create the OptimizerInput
* @param[in,out] integratorInput IntegratorInput used to create the OptimizerMetaDataInput
*                                needed to create the OptimizerInput
*                                field metaDataInput will be changed in function
*                                createOptimizerMetaDataInput
* @param[in] esoInput EsoInput needed for calls of function createOptimizerMetaDataInput
* @return created OptimizerInput
*/
                        FactoryInput::OptimizerInput InputConverter::createOptimizerInput
                        (const UserInput::OptimizerInput &optimizerInput,
                         FactoryInput::IntegratorInput &integratorInput,
                         const std::vector<UserInput::StageInput> &stageInputVector)
                        {
                          FactoryInput::OptimizerInput createdOptimizerInput;
                          createdOptimizerInput.optimizerOptions = optimizerInput.optimizerOptions;

                          createdOptimizerInput.type = convertOptimizerType(optimizerInput.type);
                          if(integratorInput.order == FactoryInput::IntegratorInput::SECOND_REVERSE)
                          {
                             bool invalid=false;
                             for(int i=0;i<stageInputVector.size()&&!invalid;i++)
							 {
								if(stageInputVector[i].optimizer.customObjectiveInformation.penaltyFunction)
                                {
								   invalid=true;
								   throw  InputException("Second order is not supported together with custom objective." );
								}
							 }
				          }
                          if(createdOptimizerInput.type == FactoryInput::OptimizerInput::FINAL_INTEGRATION
                             && integratorInput.order == FactoryInput::IntegratorInput::SECOND_REVERSE){
                            //does that make sense? - tjho
                            m_logger->sendMessage(Logger::DYOS_OUT, OutputChannel::WARNING_VERBOSITY,
                                                  "Warning: Second order integration selected without second reverse order.");
                          }
                          if(optimizerInput.optimizationMode == UserInput::OptimizerInput::MINIMIZE){
                            createdOptimizerInput.minimize = true;
                          }
                          else if(optimizerInput.optimizationMode == UserInput::OptimizerInput::MAXIMIZE){
                            createdOptimizerInput.minimize = false;
                          }
                          else{
                            assert(false);
                          }
                          createdOptimizerInput.adaptationOptions.adaptStrategy = convertAdaptiveStrategy(optimizerInput.adaptationOptions.adaptStrategy);
                          createdOptimizerInput.adaptationOptions.adaptationThreshold = optimizerInput.adaptationOptions.adaptationThreshold;
                          createdOptimizerInput.adaptationOptions.intermConstraintViolationTolerance = optimizerInput.adaptationOptions.intermConstraintViolationTolerance;
                          createdOptimizerInput.adaptationOptions.numOfIntermPoints = optimizerInput.adaptationOptions.numOfIntermPoints;

                          createdOptimizerInput.metaDataInput = createOptimizerMetaDataInput(optimizerInput,
                                                                                             integratorInput.metaDataInput,
                                                                                             stageInputVector);
                          createdOptimizerInput.integratorInput = integratorInput;

                          return createdOptimizerInput;
                        }
                        /** @brief converts the adaptation strategy
*   @param[in] input adaptive strategy in the user input
*   @return adaptive strategy in FactoryInput format
*/
                        FactoryInput::AdaptationOptions::AdaptiveStrategy InputConverter::convertAdaptiveStrategy(
                            const UserInput::AdaptationOptions::AdaptiveStrategy input) const
                        {
                          FactoryInput::AdaptationOptions::AdaptiveStrategy out;
                          switch(input)
                          {
                          case UserInput::AdaptationOptions::NOADAPTATION:
                            out = FactoryInput::AdaptationOptions::NOADAPTATION;
                            break;
                          case UserInput::AdaptationOptions::ADAPTATION:
                            out = FactoryInput::AdaptationOptions::ADAPTATION;
                            break;
                          case UserInput::AdaptationOptions::STRUCTURE_DETECTION:
                            out = FactoryInput::AdaptationOptions::STRUCTURE_DETECTION;
                            break;
                          case UserInput::AdaptationOptions::ADAPT_STRUCTURE:
                            out = FactoryInput::AdaptationOptions::ADAPT_STRUCTURE;
                            break;
                          default:
                            assert(false);
                            break;
                          }
                          return out;
                        }
                        /**
* @brief converts the adaptation input
*
* @param[in] adaptInput used to convert the adaptation input
* @param[in] gridInput required to identify the approximation type
* @param[in] lowerBound of the corresponding control
* @param[in] upperBound of the corresponding control
* @param[in] esoInput EsoInput needed for calls of function createOptimizerMetaDataInput
* @return created AdaptationInput in FactoryInput namespace
*/
                        FactoryInput::AdaptationInput InputConverter::convertAdaptationInput(
                            const UserInput::AdaptationInput &adaptInput,
                            const FactoryInput::ParameterizationGridInput gridInput,
                            const double lowerBound,
                            const double upperBound) const
                        {
                          FactoryInput::AdaptationInput adaptConvertedInput;
                          adaptConvertedInput.maxAdaptSteps = adaptInput.maxAdaptSteps;
                          if (adaptInput.adaptType == UserInput::AdaptationInput::WAVELET){
                            adaptConvertedInput.adaptType = FactoryInput::AdaptationInput::WaveletBasedRefinement;

                            adaptConvertedInput.adaptWave = convertWaveletInput(adaptInput.adaptWave, lowerBound, upperBound);
                            switch(gridInput.approxType)
                            {
                            case PieceWiseConstant:
                              adaptConvertedInput.adaptWave.type = 1;
                              break;
                            case PieceWiseLinear:
                              adaptConvertedInput.adaptWave.type = 2;
                              break;
                            default:
                              assert(false);
                              break;
                            }
                          }else if(adaptInput.adaptType == UserInput::AdaptationInput::SWITCHING_FUNCTION){
                            adaptConvertedInput.adaptType = FactoryInput::AdaptationInput::SwitchingFunction;
                            adaptConvertedInput.swAdapt.includeTol = adaptInput.swAdapt.includeTol;
                            adaptConvertedInput.swAdapt.maxRefinementLevel = adaptInput.swAdapt.maxRefinementLevel;
                            adaptConvertedInput.swAdapt.lBound = lowerBound;
                            adaptConvertedInput.swAdapt.uBound = upperBound;
                            switch(gridInput.approxType)
                            {
                            case PieceWiseConstant:
                              adaptConvertedInput.swAdapt.type = 1;
                              break;
                            case PieceWiseLinear:
                              adaptConvertedInput.swAdapt.type = 2;
                              break;
                            default:
                              assert(false);
                              break;
                            }
                          }else{
                            assert(false);
                          }

                          return adaptConvertedInput;
                        }
                        /** @brief converts the options for the wavelets
  * @param[in] waveInput options of the wavelet adaptation
  * @param[in] lowerBound of the control
  * @param[in] upperBound of the control
  * @return converted factory input options wavelet refinement
  */
                        FactoryInput::OptionsWaveletRefinemet InputConverter::convertWaveletInput(
                            const UserInput::WaveletAdaptationInput waveInput,
                            const double lowerBound,
                            const double upperBound) const
                        {
                          FactoryInput::OptionsWaveletRefinemet waveOutput;
                          waveOutput.upperBound = upperBound;
                          waveOutput.lowerBound = lowerBound;
                          waveOutput.maxRefinementLevel = waveInput.maxRefinementLevel;
                          waveOutput.minRefinementLevel = waveInput.minRefinementLevel;
                          waveOutput.horRefinementDepth = waveInput.horRefinementDepth;
                          waveOutput.verRefinementDepth = waveInput.verRefinementDepth;
                          waveOutput.etres = waveInput.etres;
                          waveOutput.epsilon = waveInput.epsilon;
                          return waveOutput;
                        }


                        std::vector<std::string> InputConverter::getDifferentialVariableNames
                        (const FactoryInput::IntegratorMetaDataInput &input)
                        {
                          std::vector<std::string> diffVarNames;
                          assert(!input.stages.empty());
                          //check if all stages have the same eso models (so we could apply full state mapping)
                          for(unsigned i=1; i<input.stages.size(); i++){
                            assert(input.stages[i].esoPtr->getModel() == input.stages.front().esoPtr->getModel());
                            assert(input.stages[i].esoPtr->getType() == input.stages.front().esoPtr->getType());
                          }
                          StdCapture capture;
                          capture.beginCapture();
                          GenericEso::Ptr eso = input.stages.front().esoPtr;
                          capture.endCapture();
                          m_logger->sendMessage(Logger::ESO, OutputChannel::STANDARD_VERBOSITY, capture.getCapture());
                          m_logger->newlineAndFlush(Logger::ESO, OutputChannel::STANDARD_VERBOSITY);

                          EsoIndex numDiffVars = eso->getNumDifferentialVariables();
                          utils::Array<EsoIndex> diffVarIndices(numDiffVars);
                          eso->getDifferentialIndex(numDiffVars, diffVarIndices.getData());
                          for(EsoIndex i=0; i<numDiffVars; i++){
                            diffVarNames.push_back(eso->getVariableNameOfIndex(diffVarIndices[i]));
                          }

                          return diffVarNames;
                        }

                        void InputConverter::addShootingConstraints(UserInput::Input &input,
                                                                    std::vector<std::string> &diffVarNames)
                        {
                          for(unsigned i=0; i<diffVarNames.size(); i++){
                            UserInput::ConstraintInput constraintInput;
                            constraintInput.type = UserInput::ConstraintInput::MULTIPLE_SHOOTING;
                            constraintInput.name = diffVarNames[i];
                            input.optimizerInput.globalConstraints.push_back(constraintInput);
                          }
                        }

                        void InputConverter::addInitialValueParameters(FactoryInput::IntegratorMetaDataInput &input,
                                                                       std::vector<std::string> &diffVarNames)
                        {
                          //get variable bounds
                          GenericEso::Ptr eso = input.stages.front().esoPtr;
                          const EsoIndex numVars = eso->getNumVariables();
                          utils::Array<EsoDouble> lowerBounds(numVars), upperBounds(numVars);
                          eso->getAllBounds(numVars, lowerBounds.getData(), upperBounds.getData());
                          for(unsigned i=0; i<diffVarNames.size(); i++){
                            FactoryInput::ParameterInput parameter;
                            parameter.esoIndex = eso->getEsoIndexOfVariable(diffVarNames[i]);
                            EsoDouble value = 0.0;
                            EsoIndex index = parameter.esoIndex;
                            eso->getVariableValues(1, &value, &index);
                            parameter.value = value;
                            parameter.sensType = FactoryInput::ParameterInput::FULL;
                            parameter.type = FactoryInput::ParameterInput::INITIAL;

                            //add parameter bounds
                            parameter.lowerBound = lowerBounds[parameter.esoIndex];
                            parameter.upperBound = upperBounds[parameter.esoIndex];
                            for(unsigned j=1; j<input.stages.size(); j++){
                              input.stages[j].initials.push_back(parameter);
                              //make sure that added parameter is unique
                              for(unsigned k=0; k<input.stages[j].initials.size()-1; k++){
                                if(input.stages[j].initials[k].esoIndex == parameter.esoIndex){
                                  input.stages[j].initials.pop_back();
                                  break;
                                }
                              }
                            }
                          }
                        }

                        /**
* @brief convert UserInput::Input into ProblemInput
*
* At the end of this function the static GenericEso pointer used by several
* functions of the Input module is deleted to prevent the system to crash when the
* process is cleaning up the static data.
* @param input Input struct to be converted
* @return converted ProblemInput
*/
                        ProblemInput InputConverter::convertInput(const UserInput::Input &input)
                        {
                          ProblemInput convertedInput;
                          //need copy for adding constraints in case of MULTIPLE_SHOOTING
                          UserInput::Input inputCopy = input;

                          FactoryInput::IntegratorInput integratorInput = createIntegratorInput(input.runningMode,
                                                                                                input.integratorInput,
                                                                                                input.stages);

                          convertedInput.integrator.metaDataInput = integratorInput.metaDataInput;

                          if(input.runningMode != UserInput::Input::SIMULATION &&
                             input.integratorInput.order == UserInput::IntegratorInput::ZEROTH){
                            integratorInput.order = FactoryInput::IntegratorInput::FIRST_FORWARD;
                          }




                          if(input.runningMode == UserInput::Input::SENSITIVITY_INTEGRATION){
                            // make sure input settings are correct (to enable users simply
                            // use sensitivity integration as running mode without bothering too much about
                            // other settings - but make sure that this is not MULTIPLE SHOOTING
                            inputCopy.optimizerInput.type = UserInput::OptimizerInput::SENSITIVITY_INTEGRATION;
                            for(unsigned i=0; i<inputCopy.stages.size(); i++){
                              inputCopy.stages[i].treatObjective = false;
                            }
                          }


                          switch(input.runningMode)
                          {
                          case UserInput::Input::SIMULATION:
                            convertedInput.integrator = integratorInput;
                            convertedInput.type = ProblemInput::SIMULATION;
                            if(integratorInput.order == FactoryInput::IntegratorInput::SECOND_REVERSE){
                              convertedInput.type = ProblemInput::OPTIMIZATION;
                              convertedInput.optimizer = createOptimizerInput(input.optimizerInput,
                                                                              integratorInput,
                                                                              input.stages);
                              convertedInput.optimizer.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
                            }
                            //convertedInput.optimizer.type = FactoryInput::OptimizerInput::FINAL_INTEGRATION;
                            break;
                          case UserInput::Input::MULTIPLE_SHOOTING:
                          {
                            std::vector<std::string> names = getDifferentialVariableNames(integratorInput.metaDataInput);
                            addShootingConstraints(inputCopy, names);
                            addInitialValueParameters(integratorInput.metaDataInput, names);
                          }
                            //no break - proceed with steps of SINGLE_SHOOTING
                          case UserInput::Input::SENSITIVITY_INTEGRATION:
                            //must remain empty, because MULTIPLE_SHOOTING will go on with SINGLE_SHOOTING steps,
                            //as will the case SENSITIVITY_INTEGRATION - so make any actions required for that case
                            //before entering this switch/case block
                          case UserInput::Input::SINGLE_SHOOTING:
                            convertedInput.optimizer = createOptimizerInput(inputCopy.optimizerInput,
                                                                            integratorInput,
                                                                            inputCopy.stages);
                            if(   input.totalEndTimeLowerBound > -DYOS_DBL_MAX
                                  && input.totalEndTimeUpperBound < DYOS_DBL_MAX){
                              convertedInput.optimizer.metaDataInput.hasTotalEndTime = true;
                              //subtract values of constant stage durations
                              convertedInput.optimizer.metaDataInput.totalEndTimeLowerBound = input.totalEndTimeLowerBound;
                              convertedInput.optimizer.metaDataInput.totalEndTimeUpperBound = input.totalEndTimeUpperBound;
                              for(unsigned i=0; i< input.stages.size(); i++){
                                if(input.stages[i].integrator.duration.sensType == UserInput::ParameterInput::NO){
                                  double currentValue = input.stages[i].integrator.duration.value;
                                  convertedInput.optimizer.metaDataInput.constantDurationsValue += currentValue;
                                }
                              }
                            }
                            else{
                              convertedInput.optimizer.metaDataInput.hasTotalEndTime = false;
                            }
                            convertedInput.type = ProblemInput::OPTIMIZATION;
                            break;
                          default:
                            assert(false);
                            break;
                          }
                          if  (input.runningMode != UserInput::Input::SIMULATION
                               && input.integratorInput.order == UserInput::IntegratorInput::ZEROTH){
                            convertedInput.integrator.order = FactoryInput::IntegratorInput::FIRST_FORWARD;
                          }
                          return convertedInput;
                        }
