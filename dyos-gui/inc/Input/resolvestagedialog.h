#ifndef RESOLVESTAGEDIALOG_H
#define RESOLVESTAGEDIALOG_H

// Qt includes
#include <QDialog>

// DyOS includes
#include "Dyos.hpp"

// GUI includes
#include "utility.h"
#include "stagetabwidget.h"

// Forward declarations
namespace Ui { class ResolveStageDialog; }

class ResolveStageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ResolveStageDialog(UserInput::StageInput *stage, QWidget *parent = nullptr);
    ~ResolveStageDialog();

    // convenience methods
    static bool resolveModelStrings(UserInput::Input *input, QWidget *parent = nullptr);

public slots:
    void updateModel();
    void updateUI();
    // functions called by UI elements
    void on_ResolveStageDialog_accepted();
    void on_esoTypeComboBox_activated(int i);
    void on_esoChoosePathButton_clicked();
    void on_checkModelButton_clicked();

private:
    Ui::ResolveStageDialog *ui;
    UserInput::StageInput *stageToResolve; ///< a pointer to the stage which is currently resolved
    UserInput::StageInput stageCopy;
};

#endif // RESOLVESTAGEDIALOG_H