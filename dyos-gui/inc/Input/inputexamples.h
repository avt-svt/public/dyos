#ifndef INPUTEXAMPLES_H
#define INPUTEXAMPLES_H

// Qt includes
#include <QDialog>
#include <QInputDialog>

// DyOS includes
#include "UserInput.hpp"
#include "FMILargeSystemTestConfig.hpp"

/**
 * @brief These are a few example inputs that can be loaded directly from
 * the GUI
 * 
 */
class InputExamples
{
public:
    static UserInput::Input example1();
    static UserInput::Input example2();
    static UserInput::Input example3();
};

#endif // INPUTEXAMPLES_H