#ifndef GLOBALTABWIDGET_H
#define GLOBALTABWIDGET_H

// Qt includes
#include <QtCore>
#include <QMessageBox>

// DyOS includes
#include "Dyos.hpp"
#include "Input.hpp"
#include "OptimizerOptions.hpp"
#include "SNOPTOptions.hpp"
#include "NPSOLOptions.hpp"

// GUI includes
#include "inputwidget.h"
#include "generictabwidget.h"
#include "mapadapter.h"
#include "addoptiondialog.h"

// Forward declarations
namespace Ui { class GlobalTabWidget; }

/**
 * @brief the widget for the integrator tab in the input tab of the dyos GUI
 * 
 */
class GlobalTabWidget : public GenericTabWidget
{
    Q_OBJECT

public:
    explicit GlobalTabWidget(QWidget *parent = nullptr);
    ~GlobalTabWidget();

public slots:
    // UI methods
    void updateUI();
    // functions called by UI elements
        // General
    void on_finalIntegrationCheckBox_stateChanged(int);
    void on_runningModeComboBox_currentIndexChanged(int);
    void on_totalEndTimeUpperBoundLineEdit_editingFinished();
    void on_totalEndTimeLowerBoundLineEdit_editingFinished();
        // Integrator
    void on_integratorTypeComboBox_currentIndexChanged(int);
    void on_integrationOrderComboBox_currentIndexChanged(int);
    void on_addIntegratorOptionButton_clicked();
    void on_removeIntegratorOptionButton_clicked();
    void on_integratorOptionsTable_itemSelectionChanged();
    void on_integratorOptionsTable_cellChanged(int row, int column);
    void on_daeTypeComboBox_currentIndexChanged(int index);
    void on_linearSolverTypeComboBox_currentIndexChanged(int index);
    void on_nonLinearSolverTypeComboBox_currentIndexChanged(int index);
    void on_toleranceLineEdit_editingFinished();
    void on_maxErrorToleranceLineEdit_editingFinished();
        // Optimizer
    void on_optimizerTypeComboBox_currentIndexChanged(int);
    void on_optimizationModeComboBox_currentIndexChanged(int);
    void on_addOptimizerOptionButton_clicked(); 
    void on_removeOptimizerOptionButton_clicked(); 
    void on_optimizerOptionsTable_itemSelectionChanged(); 
    void on_optimizerOptionsTable_cellChanged(int row, int column);
	void on_adaptationThresholdLineEdit_editingFinished();
	void on_intermConstraintViolationToleranceLineEdit_editingFinished();
	void on_numOfIntermPointsLineEdit_editingFinished();
	void on_adaptiveStrategyComboBox_currentIndexChanged(int index);

private:
    Ui::GlobalTabWidget *ui;
    bool initialized = false; 
};

#endif // GLOBALTABWIDGET_H
