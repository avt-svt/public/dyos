#ifndef ADDELEMENTDIALOG_H
#define ADDELEMENTDIALOG_H

// Qt includes
#include <QDialog>
#include <QStringList>
#include <QRegExp>
#include <vector>
#include <QListWidgetItem>

// Forward declarations
namespace Ui { class AddElementDialog; }

class AddElementDialog : public QDialog
{
    Q_OBJECT

private slots:
    void on_filterQline_textChanged(const QString &arg1);
    void on_namesListWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    QStringList namesList;
    Ui::AddElementDialog *ui;

public:
    ~AddElementDialog();
    explicit AddElementDialog(QWidget *parent = nullptr);
    void updateSelectNameWindow(QStringList namesList);
    void changeSelectionModeToSingle();
    std::vector<std::string> getSelectedNames();
};

#endif // ADDELEMENTDIALOG_H
