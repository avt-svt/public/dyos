#ifndef PARAMETERGRIDDIALOG_H
#define PARAMETERGRIDDIALOG_H

// DyOS includes
#include "Dyos.hpp"
#include "Input.hpp"

// Qt includes
#include <QDialog>
#include <QDebug>
#include <QWidget>
#include <QtCharts>
#include <QStringList>

// Forward declarations
namespace Ui { class ParameterGridDialog; }

class ParameterGridDialog : public QDialog
{
    Q_OBJECT

private slots:
    void on_timePointsAddPushButton_clicked();
    void on_timePointsDeletePushButton_clicked();
    void on_gridTypeComboBox_currentIndexChanged(int index);
    void on_addEquidistantPointspushButton_clicked();
    void on_numintervalsQlineEdit_textEdited(const QString &arg1);
    void on_pcresolutionQlineEdit_textEdited(const QString &arg1);
    void changeInput(int row, int column);
    void on_fillValuesPushButton_clicked();
    void on_adaptationTypeComboBox_currentIndexChanged(int index);
    void on_maxRefinementLevelLineEdit_editingFinished();
    void on_minRefinementLevelLineEdit_editingFinished();
    void on_horRefinementDepthLineEdit_editingFinished();
    void on_verRefinementDepthLineEdit_editingFinished();
    void on_eliminationLineEdit_editingFinished();
    void on_insertionLineEdit_editingFinished();
    void on_swMaxRefinementLevelLineEdit_editingFinished();
    void on_includeToleranceLineEdit_editingFinished();

private:
    Ui::ParameterGridDialog *ui;
    bool initialized = true ;
    bool isTheSmallest(int row);
    bool isTheBiggest(int row);
	void updateUI();
    void addRow();

public:
    explicit ParameterGridDialog(QWidget *parent = nullptr);
    ~ParameterGridDialog();
	void updateGridEditor(UserInput::ParameterGridInput grid);
	QStringList getWaveletAdaptionInfo();
	QStringList getSWAdaptionInfo();
    QStringList defaultWaveletAdaptionInfo = {"10","3","1","1","1e-8","0.9"};
	QStringList defaultSWAdaptionInfo = {"10","1e-3"};
    int adaptationType();
	int getMaxAdaptSteps();
    int numIntervals();
    int gridType();
    int pcResolution();
    std::vector<double> timePoints();
    std::vector<double> values();
    std::vector<double> getInputVector(int column);

};

#endif // PARAMETERGRIDDIALOG_H
