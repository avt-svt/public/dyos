#ifndef OPTIMIZERTABWIDGET_H
#define OPTIMIZERTABWIDGET_H

// Qt includes
#include <QtCore>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QErrorMessage>
#include <QShortcut>
#include <QStringList>

// DyOS includes
#include "Dyos.hpp"
#include "Input.hpp"

// GUI includes
#include "inputwidget.h"
#include "generictabwidget.h"
#include "addelementdialog.h"
#include "utility.h"

// Forward declarations
namespace Ui { class OptimizerTabWidget; }

/**
 * @brief the widget for the integrator tab in the input tab of the dyos GUI
 * 
 */
class OptimizerTabWidget : public GenericTabWidget
{
    Q_OBJECT

public:
    explicit OptimizerTabWidget(QWidget *parent = nullptr);
    ~OptimizerTabWidget();

public slots:
    // UI methods
    void updateUI();
    // functions called by UI elements
    void changeInput(int row, int column);
    void comboBoxIndexChanged(int index);
    void excludeZeroStateChanged(int value);

private slots:
    void on_addConstraintPushButton_clicked();
    void on_deleteConstraintPushButton_clicked();
    void on_namelineEdit_editingFinished();
    void on_selectObjectiveName_clicked();

private:
    auto* createTypeComboBox(int row);
    auto* createExcludeZeroCheckBox(int row);
    Ui::OptimizerTabWidget *ui;
    QStringList constraintsNamesList;
    int CurrentStageIndex = -1;                 // initialize current stage parameter
    bool initialized = false ;                  // is Constraint table initialized 
    enum columns {                              // set columns title
        NAME , TIMEPOINT, LOWERBOUND , UPPERBOUND  ,  CONSTRAINTTYPE , CONSTRAINTEXCLUDEZERO
    };
    QStringList ConstraintTypeComboBoxList;     // initialize Constraint Type list 
};

#endif // OPTIMIZERTABWIDGET_H
