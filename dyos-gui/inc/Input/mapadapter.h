#ifndef MAPADAPTER_H
#define MAPADAPTER_H

// Qt includes
#include <QtCore>
#include <QStandardItemModel>
#include <QFileDialog>
#include <QMessageBox>
#include <QTableWidget>

class MapAdapter {
public: 
    MapAdapter(QWidget *parent=nullptr);

    static void refreshTable(std::map<std::string, std::string> & map, QTableWidget * table, bool enabled = true);
    void addOption(std::string key,std::string value,std::map<std::string, std::string> & map); 
    void removeOption(std::map<std::string, std::string> & map, QTableWidget * table); 
    void emplaceOption(int row, int column, std::map<std::string, std::string> & map, QTableWidget * table); 

private:
    QWidget *parent;
}; 

#endif // MAPADAPTER_H