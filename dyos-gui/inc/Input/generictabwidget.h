#ifndef GENERICTABWIDGET_H
#define GENERICTABWIDGET_H

// Qt includes
#include <QtCore>
#include <QWidget>

// DyOS includes
// ...

// GUI includes
#include "inputwidget.h"

// Forward declarations
// ...

/**
 * @brief       A superclass for the four different sub tabs of the Input Tab.
 * 
 * A few members and functions would be identical in all the sub tabs of the
 * Input Tab, therefore this class serves as a common superclass for them.
 * 
 * The classes main two responsibilities are the connection to the InputWidget
 * which the sub tabs need, and also the hover explanation texts.
 */
class GenericTabWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GenericTabWidget(QWidget *parent = nullptr);
    ~GenericTabWidget();

signals:
    /** @brief   This signal can be emitted by the sub tabs if they
     * need to update the UI of the InputWidget in addition to their own. */
    void updateGlobalUI();

public slots:
    void setParentPointer(InputWidget *newParent);
    void installHoverFilterOnElements();
    // UI methods
    virtual void updateUI() = 0;    // has to be implemented by sub tabs

protected:
    InputWidget *inputWidget;   ///< pointer to parent widget
};

#endif // GENERICTABWIDGET_H
