#ifndef ADDOPTIONDIALOG_H
#define ADDOPTIONDIALOG_H

//Dyos includes
#include <OptimizerOptions.hpp>

// Qt includes
#include <QDialog>
#include <QStringList>
#include <QRegExp>
#include <vector>
#include <map>
#include <QListWidgetItem>

namespace Ui { class AddOptionDialog; }

class AddOptionDialog : public QDialog
{
    Q_OBJECT

private slots:
    void on_optionsListWidget_itemClicked(QListWidgetItem *item);
	void on_filterLineEdit_textChanged(const QString &arg1);
    void on_keyLineEdit_textChanged(const QString &arg1);
    void on_valueLineEdit_editingFinished();
    void on_okButton_clicked();
    void on_cancelButton_clicked();

private:
    enum NumberType{INTEGER, DBL, NO_NUMBER, POS_INTEGER, POS_DBL, EMPTY};
    int valueType;
	OptimizerOptions* currentOptions;
    QList<QString> optionslist;
    Ui::AddOptionDialog *ui;

public:
    explicit AddOptionDialog(QWidget *parent = nullptr);
    ~AddOptionDialog();
    void updateSelectOptionWindow(OptimizerOptions* options);
    QString getKey();
    QString getValue();
};

#endif // ADDOPTIONDIALOG_H
