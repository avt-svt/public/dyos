#ifndef INTEGRATORTABWIDGET_H
#define INTEGRATORTABWIDGET_H
#define NOMINMAX

// Qt includes
#include <QtCore>
#include <QComboBox>
#include <QPushButton>
#include <QErrorMessage>
#include <QShortcut>
#include <QStringList>

//c++ includes
#include <map>

// DyOS includes
#include "Dyos.hpp"
#include "Input.hpp"

// GUI includes
#include "inputwidget.h"
#include "generictabwidget.h"
#include "parametergriddialog.h"
#include "addelementdialog.h"
#include "utility.h"

// Forward declarations
namespace Ui { class IntegratorTabWidget; }

/**
 * @brief the widget for the integrator tab in the input tab of the dyos GUI
 * 
 */
class IntegratorTabWidget : public GenericTabWidget
{
    Q_OBJECT

public:
    explicit IntegratorTabWidget(QWidget *parent = nullptr);
    ~IntegratorTabWidget();

public slots:
    // UI methods
    void updateUI();
	void gridPushButtonClicked(bool checked);
	// functions called by UI elements
    void changeInput(int row, int column);
    void comboBoxIndexChanged(int index);
    
    

private slots:
    void on_addParameterPushButton_clicked();
    void on_deletePushButton_clicked();
    void on_addVariablePushButton_clicked();
    void on_plotGridResolutionSpinBox_valueChanged(int value);

private:
    auto* createTableWidgetItem(QString text,int index,int column,bool editable);
    auto* createTypeComboBox(int rowNumber,int columnNumber,QStringList ParameterTypeList,int CurrentIndex);
    void insertNewParameterRow(int index,int insertionLocation,bool editable);
    void createGridPushButton(int row,int column,bool hasGrid,bool editable);
    
    Ui::IntegratorTabWidget *ui;
    
    void createGridPushButton(int row,int column,bool hasGrid);
    void insertNewGrid(int parameterNumber);
    void updateParameterGridInput(std::vector<double> valueVector,std::vector<double> timePointsVector,int gridType,int pcResolution,int numIntervals,int parameterNumber,int GridNumber);

    
    std::map<string, string> identificationMap;
    std::map<string, UserInput::ParameterInput> variablesMap;
    QStringList existingParameters;
    QStringList existingVariables;
    QStringList parametersNamesList;
    QStringList variablesNamesList;
    bool initialized = false ;
    enum colums
    {
       NAME ,PARAMETERINPUTTYPE ,VALUE , LOWERBOUND , UPPERBOUND ,   PARAMETERTYPE ,PARAMETERSENSITIVITYTYPE ,GRID
    };
    QStringList ParameterTypeList;
    QStringList ParameterSensitivityTypeList;
};

#endif // INTEGRATORTABWIDGET_H
