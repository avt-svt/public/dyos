#ifndef INPUTWIDGET_H
#define INPUTWIDGET_H

// Qt includes
#include <QtCore>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QTableWidget>

// DyOS includes
#include "Dyos.hpp"
#include "Input.hpp"

// Forward declarations
namespace Ui { class InputWidget; }
class MainWindow;
class GlobalTabWidget;
class StageTabWidget;
class IntegratorTabWidget;
class OptimizerTabWidget;

/**
 * @brief the widget for the input tab of the dyos GUI
 * 
 */
class InputWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InputWidget(QWidget *parent = nullptr);
    ~InputWidget();

    // methods for the managed input object
    void setInput(const UserInput::Input &newInput);
    const UserInput::Input& getInput(bool isSaved = false);
    UserInput::Input &accessInput();
    UserInput::StageInput &accessStage();
    void resetInput(bool skipCheck = false);
    void checkInput(const UserInput::Input &inputToCheck);
    void setInputSaved();
    // convenience methods
    static bool okayAndCancelDialog(QWidget *parent, std::string prompt);
    void setCorrectMapping();

public slots:
    // UI methods
    void updateUI();

private slots:
    bool unsavedChangesDialog();
    void addStage();
    void removeCurrentStage();
    bool eventFilter(QObject *watched, QEvent *event);
    void installHoverFilterOnElements();
    // functions called by UI elements
    void on_tabWidget_currentChanged(int index);
    void on_stageComboBox_activated(int index);
    void on_addStageButton_clicked();
    void on_removeStageButton_clicked();
    void on_checkInputButton_clicked();
    void on_resetInputButton_clicked();

private:
    Ui::InputWidget *ui;

    UserInput::Input input;             ///< current input object
    UserInput::Input lastSavedInput;    ///< the last saved state of the input
    int stageIndex;                     ///< index of currently displayed stage

    enum InputSubTab {                  ///< enum for the different tabs and their order
        GLOBAL,
        STAGE,
        INTEGRATOR,
        OPTIMIZER
    };
};

#endif // INPUTWIDGET_H
