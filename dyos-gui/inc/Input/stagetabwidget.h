#ifndef STAGETABWIDGET_H
#define STAGETABWIDGET_H
#define NOMINMAX

// Qt includes
#include <QtCore>
#include <QFileDialog>
#include <QErrorMessage>
#include <QDialogButtonBox>

// DyOS includes
#include "Dyos.hpp"
#include "Input.hpp"

// GUI includes
#include "inputwidget.h"
#include "generictabwidget.h"
#include "utility.h"
#include "mapadapter.h"

// Forward declarations
namespace Ui { class StageTabWidget; }

/**
 * @brief the widget for the integrator tab in the input tab of the dyos GUI
 * 
 */
class StageTabWidget : public GenericTabWidget
{
    Q_OBJECT

public:
    explicit StageTabWidget(QWidget *parent = nullptr);
    ~StageTabWidget();

    static bool chooseModelPath(EsoType type, QString *returnedPath);
    void loadModel();
    bool checkModel();
    bool checkModel(const UserInput::EsoInput& eso);

public slots:
    void updateUI();
    // functions called by UI elements
    void on_esoTypeComboBox_activated(int);
    void on_esoModelLineEdit_returnPressed();
    void on_esoChoosePathButton_clicked();
    void on_esoLoadModelButton_clicked();
    void on_treatObjectiveCheckBox_stateChanged(int arg1);

private:
    Ui::StageTabWidget *ui;
};

#endif // STAGETABWIDGET_H
