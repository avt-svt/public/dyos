#ifndef DYOSTHREAD_H
#define DYOSTHREAD_H

// Qt includes
#include <QtCore>
#include <QMutex>

// DyOS includes
#include "Dyos.hpp"

// GUI includes
#include "threadlogstream.h"

class RunWidget;

/**
 * @brief the class holding the thread which is executing dyos
 * 
 * The class also manages the logstream responsible for sending DyOS'
 * output to a LineEdit
 * 
 */
class DyosThread : public QThread 
{
    Q_OBJECT

public:
    DyosThread(UserInput::Input *inputPointer, QMutex *inputMutex);

    // connects a text edit to which DyOS' standard output is sent
    void connectTextEdit(QTextEdit *textEdit);
    // runs DyOS
    void run();

signals:
    // signal containing output is sent once DyOS finishes
    void sendOutput(const UserOutput::Output &output);

private:
    ThreadLogStream logStream;          ///< log stream to capture standard output and send it to a LineEdit
    UserInput::Input *inputPointer;     ///< pointer to the input object in the RUN tab
    QMutex *inputMutex;                 ///< pointer to the mutex to avoid simultaneous access
    UserOutput::Output localOutput;     ///< local copy of the input that DyOS operates on
    UserInput::Input localInput;        ///< local copy of the output that DyOS operates on
};

#endif // DYOSTHREAD_H