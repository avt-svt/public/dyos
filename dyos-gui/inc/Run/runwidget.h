#ifndef RUNWIDGET_H
#define RUNWIDGET_H

// Qt includes
#include <QWidget>
#include <QObject>
#include <QMutex>

// DyOS includes
#include "Dyos.hpp"
#include "UserOutput.hpp"
#include "DyosFormatter.hpp"

// GUI includes
#include "mainwindow.h"
#include "dyosthread.h"
#include "inputwidget.h"
#include "outputwidget.h"
#include "qjsonmodel.h"
#include "utility.h"

// forward declarations
namespace Ui { class RunWidget; }
class DyosThread;

/**
 * @brief the widget for the run tab of the dyos GUI
 * 
 */
class RunWidget : public QWidget
{
    Q_OBJECT

public:
    explicit RunWidget(QWidget *parent = nullptr);
    ~RunWidget();

public slots:
    // main dyos method
    void runDyos();

    // UI methods
    void updateUI();

private:
    Ui::RunWidget *ui;

    InputWidget *inputWidget;       ///< a pointer to the input widget for optaining the input 
    UserInput::Input input;         ///< current input object
    QMutex inputMutex;              ///< mutex to prevent simultaneous access to the input
    
	DyosThread *dyosThread;         ///< thread which runs dyos
    QJsonModel *qJsonModel;         ///< model used by the input summary

    friend MainWindow;
};

#endif // RUNWIDGET_H
