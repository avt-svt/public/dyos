#ifndef ThreadLogStream_H
#define ThreadLogStream_H

// std includes
#include <iostream>
#include <streambuf>
#include <string>

// Qt includes
#include <QScrollBar>
#include <QTextEdit>
#include <QDateTime>

/**
 * @brief this class is able to send the content of an std::ostream to a Qt UI element
 * using the signals and slots mechanism of Qt
 * 
 */
class ThreadLogStream : public QObject, public std::basic_streambuf<char>
{
    Q_OBJECT

public:
    ThreadLogStream(std::ostream &stream);
    ~ThreadLogStream();

protected:
    // append a single character
    virtual int_type overflow(int_type v);
    // append several characters
    virtual std::streamsize xsputn(const char *p, std::streamsize n);

signals:
    // signal which sends a string to all connected UI elements 
    void sendString(const QString& str);

private:
    std::ostream &stream;       // stream which is observed
    std::streambuf *oldBuffer;  // old buffer, which is reconnected at the end
    std::string string;         // internal string for buffering
};

#endif // ThreadLogStream_H