#ifndef OUTPUTWIDGET_H
#define OUTPUTWIDGET_H

// std includes
#include <string>
#include <algorithm>

// Qt includes
#include <QWidget>
#include <QtCharts>

// DyOS includes
#include "Dyos.hpp"
#include "DyosFormatter.hpp"

// GUI includes
#include "mainwindow.h"
#include "qjsonmodel.h"
#include "utility.h"
#include "visualizer.h"
#include "advancedoutput.h"

// Forward declarations
namespace Ui { class OutputWidget; }

/**
 * @brief the widget for the output tab of the dyos GUI
 * 
 */
class OutputWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OutputWidget(QWidget *parent = nullptr);
    ~OutputWidget();

public slots:
    // method for changing the currently used output
    void setOutput(const UserOutput::Output &output);
    const UserOutput::Output& getOutput();

private slots:
    // UI methods
    void updateUI();
    
	// functions called by UI elements
	void on_outputListWidgetState_itemChanged(QListWidgetItem *item);       // check item from data list to updateUI and createChart
    void on_outputListWidgetParameter_itemChanged(QListWidgetItem *item);   // check item from data list to updateUI and createChart
    void on_outputListWidgetConstraint_itemChanged(QListWidgetItem *item);  // check item from data list to updateUI and createChart
    void on_selectAllStates_stateChanged();                                 // (de-)select all checkboxes in corresponding listView
    void on_selectAllParameters_stateChanged();                             // (de-)select all checkboxes in corresponding listView
    void on_selectAllConstraints_stateChanged();                            // (de-)select all checkboxes in corresponding listView

    // functions called by functions
    void updateLegendState(int i);                                          // fill custom chart legend
    void updateLegendParam(int i);                                          // fill custom chart legend
    void updateLegendConst(int i);                                          // fill custom chart legend
    void updateLegendObjec(int i);                                          // fill custom chart legend
    void updateOverviewTime();                                              // write time values (calculation time, stage endtimes) on the GUI

    // functions called by UI elements - tools
    void on_buttonAdvanced_clicked();                                       // open window with entire json output
    void on_buttonShowGrid_clicked();                                       // toggles the gridlines in the chart on/off
    void on_buttonShowStageShader_clicked();                                // toggles the stagesahding in the chart on/off

private:
    Ui::OutputWidget *ui;
    AdvancedOutput *advancedOutput;

    UserOutput::Output output; ///< current output object
	Visualizer vis; ///< visualizer

    // List for displaying the correct legend entries
    std::vector<std::string> listLegendEntries;
    QList<QWidget*> legendLabels;

    // List for displaying the correct time labels
    QList<QWidget*> timeLabels;

    friend MainWindow;
};

#endif // OUTPUTWIDGET_H
