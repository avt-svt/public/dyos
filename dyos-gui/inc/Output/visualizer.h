#ifndef VISUALIZER_H
#define VISUALIZER_H

// std includes
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

// Qt includes
#include <QWidget>
#include <QtCharts>

// DyOS includes
#include "Dyos.hpp"
#include "DyosFormatter.hpp"

// GUI includes
#include "mainwindow.h"

QT_CHARTS_USE_NAMESPACE

class Visualizer : public QObject
{
    Q_OBJECT

public:
    explicit Visualizer();
    ~Visualizer();
    void setOutput(UserOutput::Output output);

    // General functions (sorted by calling order)
    QLineSeries * createLineSeries  (std::vector<double> vectorX, std::vector<double> vectorY);
    QChartView  * createChartView   (QLineSeries *series);                      // (for testing purposes) creates a chartview with any series
    QChartView  * createChartView   (QBarSeries *series);                       // (for testing purposes) or future use if different charttypes are wanted

    // specific functions for dyos objects (sorted by calling order)
    QChart * createChartState       (QChart *chart, int i);
    QChart * createChartParameter   (QChart *chart, int i);
    QChart * createChartConstraint  (QChart *chart, int i);
    QChart * chartMarkStages        (QChart *chart, QLineSeries *series);       // mark stages, color background and label x axis
    QChart * chartMarkConstraints   (QChart *chart, int i, std::pair<double,double> timePoints); // mark chosen constraints i in the chart
    QChart * chartMarkObjectives    (QChart *chart, QLineSeries *series, int i, std::pair<double,double> timePoints); // mark chosen objective i in the chart
    // support functions to make code more readable
	QChart * drawPath    (UserOutput::ConstraintOutput *constraint, QChart *chart, std::pair<double, double> timePoints);
    QChart * drawPoint   (UserOutput::ConstraintOutput *constraint, QChart *chart, std::pair<double, double> timePoints, int stageIndex);
    QChart * drawEndpoint(UserOutput::ConstraintOutput *constraint, QChart *chart, std::pair<double, double> timePoints);

    // optical functions for the chart/UI
    void      assignColors   (std::map<std::string, QColor>& map);     // gives each unique variable with in a map a unique color
    QChart *  toggleGridLines(QChart *chart);                          // function that toggles the gridlines

    // Hardcoded examples for testing
    QLineSeries * createLineSeries1();
    QBarSeries  * createBarSeries1();
    QChartView  * example1();

//private:
    UserOutput::Output output; ///< current output object
    
    // Maps for displaying the correct output categories
    std::map<int, std::pair<int,int> > itemToState;
    std::map<int, std::pair<int,int> > itemToParameter;
    std::map<int, std::pair<int,int> > itemToConstraint;

    // Maps for displaying the same colors for identical variables
    std::map<std::string, QColor> mapVariableToColor;

    // global settings for output used by UI elements
    bool showGridLines    = true;
    bool showStageShading = true;
};

#endif // VISUALIZER_H
