#ifndef ADVANCEDOUTPUT_H
#define ADVANCEDOUTPUT_H

// Qt includes
#include <QWidget>

// DyOS includes
#include "Dyos.hpp"
#include "DyosFormatter.hpp"

// GUI includes
#include "mainwindow.h"
#include "utility.h"
#include "qjsonmodel.h"

namespace Ui { class AdvancedOutput; }

/**
 * @brief the widget for the advanced output tab of the dyos GUI
 * 
 */
class AdvancedOutput : public QDialog
{
    Q_OBJECT

public:
    explicit AdvancedOutput(QWidget *parent = nullptr);
    ~AdvancedOutput();

public slots:
    // method for changing the currently used output
    void setOutput(const UserOutput::Output &output);

private:
    Ui::AdvancedOutput *ui;

    UserOutput::Output output;  ///< current output object
    QJsonModel qJsonModel;      ///< model used by the output summary

    friend MainWindow;
};

#endif // ADVANCEDOUTPUT_H
