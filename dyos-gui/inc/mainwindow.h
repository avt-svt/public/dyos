#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Qt includes
#include <QtGlobal>
#include <QMainWindow>

// GUI includes
#include "inputexamples.h"
#include "gui_io.h"
// Dyos includes
#include "InputOutputConversions.hpp"

// Forward declarations
namespace Ui { class MainWindow; }

/**
 * @brief   The main window of the application.
 * 
 * The main window is responsible for high level operations,
 * e.g. menu actions (which can also be triggered by shortcuts)
 * or saving/loading data from files.
 * 
 * All the actions are declared in the mainwindow.ui file, where their
 * shortcut key combination is aslo set. The functions ("slots") with the
 * "on_" prefix are automatically called when these actions are triggered.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    // UI methods
    void updateUI();
    // Functions called by actions
    void on_actionNew_Input_triggered();
    void on_actionSave_triggered();
    void on_actionOpen_triggered();
	void on_actionConvert_to_Input_triggered();
	void on_actionConvert_to_Output_triggered();
    void on_actionRun_Current_Input_triggered();
    void on_actionExample1_triggered();
    void on_actionExample2_triggered();
    void on_actionExample3_triggered();

private:
    Ui::MainWindow *ui;
	UserInput::Input startupInput;
	IOConversions IOconverter;

    enum TabIndex   /// Enum for the different main tabs and their order
    {
        INPUT,
        RUN,
        OUTPUT
    };
};

#endif // MAINWINDOW_H
