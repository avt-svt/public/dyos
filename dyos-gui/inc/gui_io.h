#ifndef GUI_IO_H
#define GUI_IO_H

// this line fixes a compiler bug on windows
#define NOMINMAX

// Qt includes
#include <QtCore>
#include <QFileDialog>
#include <QErrorMessage>

// Boost includes
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

// DyOS includes
#include "Dyos.hpp"
#include "ConvertToXmlJson.hpp"
#include "ConvertFromXmlJson.hpp"

// GUI includes
#include "resolvestagedialog.h"

/**
 * @brief   The class handeling all the input/output for text files.
 * 
 * The methods in this class are used to save and open text files for the input
 * and output objects in the GUI.
 * 
 * They use system dialogs to ask the user for a valid file path,
 * and they use DyOS own text conversion routines (for json and xml)
 * to transform the objects into text and back.
 */
class GUI_IO : public QObject
{
    Q_OBJECT

    // generic IO routines
    template<typename DataObject>
    static bool saveData(const DataObject& data, void (*saveFunction)(std::string, DataObject, DataFormat),
                         QString settingsKey, QString windowTitle, QString fileDescription, QWidget *parent = nullptr);

    template<typename DataObject>
    static bool openData(DataObject *returnedData, void (*saveFunction)(std::string, DataObject&, DataFormat),
                         QString settingsKey, QString windowTitle, QString fileDescription, QWidget *parent = nullptr);

public:
    // specific IO routines
    static bool saveInput(const UserInput::Input& inputToSave, QWidget *parent = nullptr);
    static bool saveOutput(const UserOutput::Output& outputToSave, QWidget *parent = nullptr);
    static bool openInput(UserInput::Input *returnedInput, QWidget *parent = nullptr);
    static bool openOutput(UserOutput::Output *returnedOutput, QWidget *parent = nullptr);
};

#endif // GUI_IO_H