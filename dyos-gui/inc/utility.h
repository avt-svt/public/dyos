#ifndef UTILITY_H
#define UTILITY_H

// this line fixes a compiler bug on windows
#define NOMINMAX

/**
 * @file    utility.h  
 * @brief   This file contains a bunch of helper functions which are needed
 * in more than one place, but logically don't belong to any widget. It's more
 * like a toolbelt.
 */

#include <QModelIndex>
#include <QTreeView>
void expandChildren(const QModelIndex &index, QTreeView *view, int depth=1);

#include "Dyos.hpp"
#include "Input.hpp"
#include "GenericEso.hpp"
#include "EsoInput.hpp"
#include "GenericEsoFactory.hpp"
GenericEso::Ptr getEsoPtr(const UserInput::EsoInput& eso, UserInput::IntegratorInput::IntegrationOrder order = UserInput::IntegratorInput::FIRST_FORWARD);

#endif // UTILITY_H