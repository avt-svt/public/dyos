#include "threadlogstream.h"

/**
 * @brief   Construct a new Thread Log Stream object
 * 
 * @param outputStream      The stream which should be observed
 */
ThreadLogStream::ThreadLogStream(std::ostream &outputStream) : stream(outputStream)
{
    // remember old buffer for later
    oldBuffer = stream.rdbuf();
    // associate this object as a new stream buffer
    stream.rdbuf(this);
}

ThreadLogStream::~ThreadLogStream()
{
    // output anything that is left in the stream
    if (!string.empty()) {
        emit sendString(QString::fromStdString(string));
    }
    // restore the old buffer
    stream.rdbuf(oldBuffer);
}

/**
 * @brief     Append character to the internal string.
 * 
 * If a newline character is encountered, send the whole line to the UI element
 * 
 * @param character                         The character which is appended
 * @return ThreadLogStream::int_type
 */
ThreadLogStream::int_type ThreadLogStream::overflow(ThreadLogStream::int_type character)
{
    if (character == '\n') {
        // send whole line to UI element
        sendString(QString::fromStdString(string));
        string.erase(string.begin(),string.end());
    } else {
        string += character;
    }
    return character;
}

/**
 * @brief       Append several characters to the internal string.
 * 
 * Afterwards, the internal string is examined for all newline characters,
 * and the parts inbetween are separated and sent to the UI element
 * 
 * @param cstring
 * @param n 
 * @return std::streamsize 
 */
std::streamsize ThreadLogStream::xsputn(const char *cstring, std::streamsize n)
{
    // add everything to the internal string
    string.append(cstring,cstring+n);
    // check string for any newline characters, and send the strings inbetween them
    long pos = 0;
    while (pos != static_cast<long>(std::string::npos)) {
        pos = static_cast<long>(string.find('\n'));
        if (pos != static_cast<long>(std::string::npos)) {
            std::string tmp(string.begin(), string.begin() + pos);
            sendString(QString::fromStdString(tmp));
            string.erase(string.begin(), string.begin() + pos + 1);
        }
    }
    return n;
}