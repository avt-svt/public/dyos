#include "dyosthread.h"

/**
 * @brief   Construct a new Dyos Thread object
 * 
 * @param extInput          Pointer to the cached input object in the RUN tab
 * @param extInputMutex     Pointer to the mutex to avoid simultaneous access
 */
DyosThread::DyosThread(UserInput::Input *extInput, 
                       QMutex *extInputMutex) :
    logStream(std::cout),
    inputPointer(extInput),
    inputMutex(extInputMutex)
{
}

/**
 * @brief       Connects a text edit to which DyOS' standard output is sent
 * 
 * @param textEdit      The QTextEdit which should be connected
 */
void DyosThread::connectTextEdit(QTextEdit* textEdit)
{
    connect(&logStream, SIGNAL(sendString(const QString&)), textEdit, SLOT(append(const QString&)));
}

/**
 * @brief   Runs dyos on local copies of the input and output
 * 
 */
void DyosThread::run() {
    // copy current input from run tab in a thread safe way
    inputMutex->lock();
    localInput = *inputPointer;
    inputMutex->unlock();

    // run Dyos on temporary copies of input and output
    localOutput = runDyos(localInput);

    // send output to run tab in a thread safe way
    sendOutput(localOutput);
}