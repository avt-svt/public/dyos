#include "runwidget.h"
#include "ui_runwidget.h"

RunWidget::RunWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RunWidget),
    dyosThread(new DyosThread(&input,&inputMutex)),
    qJsonModel(new QJsonModel),
    inputWidget(0)
{
    ui->setupUi(this);
    // connect thread output to output text edit
    dyosThread->connectTextEdit(ui->outputTextEdit);
    // connect the tree view to the model
    ui->inputTreeView->setModel(qJsonModel);
    // connect signals and slots for UI elements
    connect(ui->runPushButton, SIGNAL(clicked()), this, SLOT(runDyos()));
    // update the UI after initialisation
    updateUI();
}

RunWidget::~RunWidget()
{
    delete ui;
    delete dyosThread;
    delete qJsonModel;
}

/**
 * @brief gets the current input from the input tab
 * and updates the contents of the tree view
 * 
 */
void RunWidget::updateUI()
{
    // this method can only work if the inputWidget pointer has already been connected
    if (!inputWidget) return;

    inputMutex.lock();
        // update the cached input
        input = inputWidget->getInput();
        // convert input to JSON
        JsonInputFormatter jsonInputFormatter(input);                               
    inputMutex.unlock();

    qJsonModel->loadJson(QByteArray(jsonInputFormatter.toString().c_str()));    // feed JSON to model of tree view
    expandChildren(qJsonModel->index(0,0,QModelIndex()),ui->inputTreeView,3);   // expand some items in the tree view for better visability
} 

/**
 * @brief main method which runs dyos with the current input
 * 
 */
void RunWidget::runDyos()
{
    // threads can't be safely interrupted, therefore we must
    // wait until dyos finishes before restarting it
    if (!dyosThread->isRunning()) {
        // clear text edit
        ui->outputTextEdit->clear();

        // the dyos thread already knows the memory location of the cached
        // UserInput::Input object and the UserOutput::Output object
        // to which it will write the result upon completion.
        // these locations were passed to the thread in its constructor.
        dyosThread->start();
    }
}