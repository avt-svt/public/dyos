#include "utility.h"

/**
 * @brief   This function recursively expands the nodes in a QTreeView up to a specified depth.
 * 
 * This functionality will be added to the QTreeView class in Qt 5.13,
 * which is still fairly new, which is why we included our own implementation here.
 * 
 * @param index     The QModelIndex of the root item to start expanding
 * @param view      The QTreeView in which to expand the nodes
 * @param depth     The depth up to which the nodes should be expanded (recursion parameter)
 */
void expandChildren(const QModelIndex &index, QTreeView *view, int depth)
{
    if (!index.isValid() || depth<=0) return;   // end of recursion

    int childCount = index.model()->rowCount(index);
    for (int i = 0; i < childCount; i++) {
        const QModelIndex &child = index.child(i, 0);
        expandChildren(child, view, depth-1);
    }

    // expand the node
    view->expand(index);
}

/**
 * @brief   This method tries to obtain the eso pointer described in an EsoInput object.
 * 
 * This procedure is more or less copied from some section in the original DyOS code,
 * and may not be the best way of obtaining the eso pointer. But, because the group
 * which designed the GUI lacks a deeper understanding of the eso system, this does the trick.
 * 
 * @param eso                   The EsoInput object containing the path information of the eso model
 * @param order                 The integration order
 * @return GenericEso::Ptr      A pointer to the GenericEso object
 */
GenericEso::Ptr getEsoPtr(const UserInput::EsoInput& eso, UserInput::IntegratorInput::IntegrationOrder order)
{
    // try converting 
    InputConverter inputConverter;
    FactoryInput::EsoInput factoryEso = inputConverter.convertEsoInput(eso);
    // try creating
    GenericEso::Ptr esoPtr;
    GenericEsoFactory genEsoFac;
    if (order == UserInput::IntegratorInput::SECOND_REVERSE) {
        esoPtr = genEsoFac.create2ndOrder(factoryEso);
    } else {
        esoPtr = genEsoFac.create(factoryEso);
    }
    // if the pointer is still invalid, and the methods above have not thrown an exception themselves, throw an exception
    if (!esoPtr) throw std::runtime_error("An unknown error occured while trying to read an eso model.");

    return esoPtr;
}


