#include "mainwindow.h"
#include "ui_mainwindow.h"


/**
 * @brief   Construct a new MainWindow object.
 * 
 * During contruction, the connections between the tabs will be made.
 * 
 * @param parent    The parent widget
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	ui->tabWidget->setCurrentIndex(0);
	// save the startup input
	startupInput = ui->Input->accessInput();
    // make connections between the tabs
    ui->Run->inputWidget = ui->Input;
    connect(ui->Run->dyosThread,    SIGNAL(sendOutput(const UserOutput::Output&)),
            ui->Output,             SLOT(setOutput(const UserOutput::Output&)));
    // connect signals and slots for UI elements
    connect(ui->tabWidget,  SIGNAL(currentChanged(int)),
            this,           SLOT(updateUI()));
    // update the UI after initialisation
    updateUI();
} 

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief   Updates the entire UI, as well as the UI of the child tabs.
 * 
 * The function needs to enable and disable certain menu items,
 * depending on whether they should be available in a given tab.
 * 
 */
void MainWindow::updateUI()
{
    // update UI of child tabs
    TabIndex index = TabIndex(ui->tabWidget->currentIndex());
    switch (index) {
    case INPUT:
        ui->Input->updateUI();
        break;
    case RUN:
        ui->Run->updateUI();
        break;
    case OUTPUT:
        ui->Output->updateUI();
        break;
    }

    // enable actions according to tab
    ui->actionNew_Input->setEnabled(index==INPUT);
    ui->actionSave->setEnabled(index==INPUT || index==OUTPUT);
    ui->actionOpen->setEnabled(index==INPUT || index==OUTPUT);
    ui->actionConvert_to_Input->setEnabled(index == OUTPUT);
	ui->actionConvert_to_Output->setEnabled(index == INPUT);
}

/**
 * @brief   Creates a new empty input
 * 
 */
void MainWindow::on_actionNew_Input_triggered()
{
    ui->Input->resetInput();
}

/**
 * @brief   Loads an input or output text file.
 * 
 */
void MainWindow::on_actionOpen_triggered()
{
    bool ok;
    UserInput::Input openedInput;
    UserOutput::Output openedOutput;

    TabIndex index = TabIndex(ui->tabWidget->currentIndex());
    switch (index) {
    case INPUT:
        // open an input
        ok = GUI_IO::openInput(&openedInput,this);
        if (ok) ui->Input->setInput(openedInput);
        break;
    case RUN:
        // should not be possible
        break;
    case OUTPUT:
        // open an output
        ok = GUI_IO::openOutput(&openedOutput,this);
		if (ok) { 
		    ui->Output->setOutput(openedOutput);
        }
        break;
    }

    // this action obviously has consequences on the UI, therefore update it
    updateUI();
}
/**
 * @brief   Saves an input or output text file.
 * 
 */
void MainWindow::on_actionSave_triggered()
{
    bool ok;
    TabIndex index = TabIndex(ui->tabWidget->currentIndex());
    switch (index) {
    case INPUT:
        // save current input
        ok = GUI_IO::saveInput(ui->Input->getInput(),this);
        if (ok) ui->Input->setInputSaved();
        break;
    case RUN:
        // should not be possible
        break;
    case OUTPUT:
        // save current output
        GUI_IO::saveOutput(ui->Output->getOutput(),this);
        break;
    }
}
/**
 * @brief   converts the Output into Input
 *
 */
void MainWindow::on_actionConvert_to_Input_triggered()
{
	if (ui->Output->getOutput() == UserOutput::Output()) {
		QErrorMessage::qtHandler()->showMessage(tr("Current output is empty !"));
		return;
	}
	// ask user what he wants to do and return his answer
	ui->Input->setInput(IOconverter.o2iConvertOutputToInput(ui->Output->getOutput()));
}
/**
 * @brief   converts the Input into Output
 *
 */
void MainWindow::on_actionConvert_to_Output_triggered()
{
	if (ui->Input->accessInput() == startupInput) {
		QErrorMessage::qtHandler()->showMessage(tr("Current input is empty !"));
		return;
	}
	ui->Output->setOutput(IOconverter.i2oConvertInputToOutput(ui->Input->getInput()));
}


/**
 * @brief   Runs the current input
 * 
 * This action can be triggered from anywhere within the GUI.
 * After it has been triggered, it switches to the Run tab and
 * runs DyOS with the input that is currently in the Input tab.
 * 
 */
void MainWindow::on_actionRun_Current_Input_triggered()
{
    // change to run tab
 	ui->tabWidget->setCurrentIndex(RUN);
    // call run dyos function in run widget
    ui->Run->runDyos();
}

/**
 * @brief Loads an input example.
 * 
 */
void MainWindow::on_actionExample1_triggered()
{
    ui->Input->setInput(InputExamples::example1());
}

/**
 * @brief Loads an input example.
 * 
 */
void MainWindow::on_actionExample2_triggered()
{
    ui->Input->setInput(InputExamples::example2());
}

/**
 * @brief Loads an input example.
 * 
 */
void MainWindow::on_actionExample3_triggered()
{
    ui->Input->setInput(InputExamples::example3());
}
