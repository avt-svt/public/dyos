#include "outputwidget.h"
#include "ui_outputwidget.h"

OutputWidget::OutputWidget(QWidget *parent) : QWidget(parent),
                                              ui(new Ui::OutputWidget),
                                              advancedOutput(new AdvancedOutput(this))
{
    ui->setupUi(this);
    // create empty chart
    ui->splitter->insertWidget(1,new QChartView()); // Chart widget has position #1 (horizontal) in the splitter
    // update the UI after initialisation
    updateUI();
}

OutputWidget::~OutputWidget()
{
    delete advancedOutput;
    delete ui;
}

/**
 * @brief this function is called whenever a selection
 * of the data changes, and it calls the corresponding drawChart function
 * 
 * Loops over all three lists (states, parameters, constraints) to draw each selected item one after another.
 * In the end the old chart is deleted and replaced by the updated one.
 *
 */
void OutputWidget::updateUI()
{
    // initializing phase
    vis.setOutput(output);        // update ouput in visualizer class
    listLegendEntries.clear();    // clear custom legend
    QChart *chart = new QChart(); // make a brand new chart

    // clear entries in custom legend
    while (legendLabels.size() > 0)
    {
        QWidget *widget = legendLabels.takeLast();
        ui->chartLegend->removeWidget(widget);
        widget->deleteLater();
    }

    // loop over entire list of states
    for (int i = 0; i < ui->outputListWidgetState->count(); i++)
    {
        QListWidgetItem *item = ui->outputListWidgetState->item(i); // look at one item at a time
        // check if current item is checked, otherwise ignore it
        if (item->checkState() == Qt::Checked)
        {
            chart = vis.createChartState(chart, i);
            updateLegendState(i);
        }
    }

    // loop over entire list of parameters
    for (int i = 0; i < ui->outputListWidgetParameter->count(); i++)
    {
        QListWidgetItem *item = ui->outputListWidgetParameter->item(i); // look at one item at a time
        // check if current item is checked, otherwise ignore it
        if (item->checkState() == Qt::Checked)
        {
            chart = vis.createChartParameter(chart, i);
            updateLegendParam(i);
        }
    }

    // loop over entire list of constraints
    for (int i = 0; i < ui->outputListWidgetConstraint->count(); i++)
    {
        QListWidgetItem *item = ui->outputListWidgetConstraint->item(i); // look at one item at a time
        // check if current item is checked, otherwise ignore it
        if (item->checkState() == Qt::Checked)
        {
            chart = vis.createChartConstraint(chart, i);

            int constrIndex = vis.itemToConstraint[i].second;
            if (constrIndex != INT_MAX)   // index != INT_MAX -> it is a constraint, if true -> it is a objective
            {
                updateLegendConst(i);
            }
            else    // it is a objective
            {
                updateLegendObjec(i);
            }
            
        }
    }

    chart = vis.toggleGridLines(chart);
    chart->legend()->setVisible(false);     // hide QChart standard legend, because it is far too cluttered

    // replace old chart
    /*  Attention chart widget has position #1 (horizontal) in the splitter
    #0 layoutData(with outputListWidgetState, outputListWidgetParameter, outputListWidgetConstraint)
    #1 chart
    #2 layoutTools*/
    QChartView *chartView = (QChartView *)ui->splitter->widget(1); // the widget(...) function returns a QWidget pointer, it needs to be cast into a QChartView pointer
    if (!chartView)
        return;                            // return if cast is unsuccessfull, otherwise we get a segmentation fault if the 3rd widget in the splitter doesn't exist or isn't a QChartView!
    QChart *oldChart = chartView->chart(); // remember old chart for later
    chartView->setChart(chart);
    chartView->setRenderHint(QPainter::Antialiasing); // make chart view pretty

    // delete old chart to avoid memory leaks (this is described in the Qt documentation, it is a necessary step in this case)
    if (oldChart)
        delete oldChart;
}

/**
 * @brief change the current output displayed in the output widget
 *
 * The three lists (states, parameters, constraints) are populaterd one after another.
 * Spacers with the stage numbering are added, and each item gets labeled with further information.
 * For each stage the single objective is appended to the constraint list.
 * C++ maps are used to save the stage and index for each variable.
 * 
 * How the mapping works: (all maps are saved in the visualizer object vis)
 * //itemToState[0] is a stage label, leave it empty
 * itemToState[1] = std::make_pair(0,0);    // remember that the item with index 1 in the list belongs to stage 0 state 0.
 * itemToState[2] = std::make_pair(0,1);    // remember that the item with index 2 in the list belongs to stage 0 state 1.
 * itemToState[3] = std::make_pair(0,2);    // remember that the item with index 3 in the list belongs to stage 0 state 2.
 * itemToState[4] = std::make_pair(0,3);    // remember that the item with index 4 in the list belongs to stage 0 state 3.
 * //itemToState[5] is a stage label, leave it empty
 * ... same for the other two maps
 * 
 * @param newOutput 
 */
void OutputWidget::setOutput(const UserOutput::Output &newOutput)
{
    output = newOutput;

    // set output in advanced output window (if present)
    if (advancedOutput) advancedOutput->setOutput(output);

    // reset selectAllButtons
    ui->selectAllStates->setCheckState(Qt::Unchecked);
    ui->selectAllParameters->setCheckState(Qt::Unchecked);
    ui->selectAllConstraints->setCheckState(Qt::Unchecked);

    // clear list widget
    ui->outputListWidgetState->clear();
    ui->outputListWidgetParameter->clear();
    ui->outputListWidgetConstraint->clear();

    // clear maps of old items
    vis.itemToState.clear();
    vis.itemToParameter.clear();
    vis.itemToConstraint.clear();

    // write stage times and calculation time to the overview sidebar
    updateOverviewTime();

    // temporary variables
    std::string title;                                                     // used for the spacers between stages
    int countItemsState = -1, countItemsParam = -1, countItemsConstr = -1; // -1 because a spacer for stage 0 is always added

    for (int i = 0; i < output.stages.size(); i++)
    {
        std::string title = "Stage " + std::to_string(i+1);

        // populate list with states
        QListWidgetItem *item = new QListWidgetItem(tr(title.c_str()), ui->outputListWidgetState); //spacer between stages for better overview
        item->setFlags(item->flags() & (~Qt::ItemIsSelectable));
        countItemsState++; // add 1 to the item total of the corresponding list
        for (int j = 0; j < output.stages[i].integrator.states.size(); j++)
        {
            UserOutput::StateOutput *state = &output.stages[i].integrator.states[j];
            // create item in list and set nametag (variable "name (number of data points)"")
            std::string label;
            label = state->name;
            label.append("	(");
            label.append(std::to_string(state->grid.values.size()));
            label.append(" data points)");   // returns for example label = "ttime (14 datapoints)"
            QListWidgetItem *item = new QListWidgetItem(tr(label.c_str()), ui->outputListWidgetState);

            // turn item into a checkbox
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(Qt::Unchecked);
            countItemsState++;                                       // add 1 to the item total of the corresponding list
            vis.itemToState[countItemsState] = std::make_pair(i, j); // remember that the item with index countItemsState in the list belongs to stage i state j.

            // add item to mapVariableToColor for consistent coloring of the same variable
            vis.mapVariableToColor[state->name] = QColor();          // actual color is assigned later
        }

        // populate list with parameters
        item = new QListWidgetItem(tr(title.c_str()), ui->outputListWidgetParameter); //spacer between stages for better overview
        item->setFlags(item->flags() & (~Qt::ItemIsSelectable));
        countItemsParam++; // add 1 to the item total of the corresponding list
        for (int j = 0; j < output.stages[i].integrator.parameters.size(); j++)
        {
            UserOutput::ParameterOutput *parameter = &output.stages[i].integrator.parameters[j];
            // create item in list and set nametag (variable "name (number of data points)"")
            std::string label;
            // append rest of string
            UserOutput::ParameterOutput::ParameterType type = parameter->paramType;
            if (type == UserOutput::ParameterOutput::INITIAL)
            {
                // Special case -> display variable "   name (Initial = %value)"    
                label.append("      " + parameter->name + "                 (");
                label.append("Initial = ");
                label.append(std::to_string(parameter->value));
                label.append(" )"); // returns for example label = "velo (Initial = 0 )"
                QListWidgetItem *item = new QListWidgetItem(tr(label.c_str()), ui->outputListWidgetParameter);

                // make it uncheckable
                item->setFlags(item->flags() & (~Qt::ItemIsSelectable));
            }
            else
            {
                // regular case
                // create item in list and set nametag (variable "name (number of data points)"")
                label.append(parameter->name + "	(");
                label.append(std::to_string(parameter->grids[0].values.size()));
                label.append(" data points)"); // returns for example label = "ttime (14 datapoints)"
                QListWidgetItem *item = new QListWidgetItem(tr(label.c_str()), ui->outputListWidgetParameter);

                // turn item into a checkbox
                item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
                item->setCheckState(Qt::Unchecked);
            }
            countItemsParam++;                                           // add 1 to the item total of the corresponding list
			vis.itemToParameter[countItemsParam] = std::make_pair(i, j); // remember that the item with index countItemsParam in the list belongs to stage i parameter j.

            // add item to mapVariableToColor for consistent coloring of the same variable
			vis.mapVariableToColor[parameter->name] = QColor(); // actual color is assigned later
        }

        // populate list with optimizer nonlinearConstraints
        item = new QListWidgetItem(tr(title.c_str()), ui->outputListWidgetConstraint); //spacer between stages for better overview
        item->setFlags(item->flags() & (~Qt::ItemIsSelectable));
        countItemsConstr++; // add 1 to the item total of the corresponding list
        for (int j = 0; j < output.stages[i].optimizer.nonlinearConstraints.size(); j++)
        {
            UserOutput::ConstraintOutput *constraint = &output.stages[i].optimizer.nonlinearConstraints[j];
            UserOutput::ConstraintOutput::ConstraintType type = constraint->type;
            // create item in list and set nametag (variable "name (Type Constraint)"")
            std::string label, constraintType;
            label = constraint->name;
            label.append("	(");
            switch (type) {
                case UserOutput::ConstraintOutput::PATH:
                    constraintType = "Path";
                    break;
                case UserOutput::ConstraintOutput::POINT:
                    constraintType = "Point";
                    break;
                case UserOutput::ConstraintOutput::ENDPOINT:
                    constraintType = "Endpoint";
                    break;
                default:
                    QErrorMessage::qtHandler()->showMessage(tr("ERROR: Congrats you reached the default in the switch type case! This most likely means, that a new constraint type was added, which needs to be included in here (GUI->outputwidget.cpp)as well!\n"));
                    break;
            }
            label.append(constraintType);
            label.append(" Constraint)");   // returns for example label = "velo (Endpoint)"
            QListWidgetItem *item = new QListWidgetItem(tr(label.c_str()), ui->outputListWidgetConstraint);

            // turn item into a checkbox
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(Qt::Unchecked);
            countItemsConstr++;                                            // add 1 to the item total of the corresponding list
            vis.itemToConstraint[countItemsConstr] = std::make_pair(i, j); // remember that the item with index countItemsConstr in the list belongs to stage i constraint j.
        }

        // populate list with optimizer objectives (each stage only has one objective
        // and is grouped together with the constraints in the same list and stages)
        {
            UserOutput::ConstraintOutput *objective = &output.stages[i].optimizer.objective;
            UserOutput::ConstraintOutput::ConstraintType type = objective->type;
            // create item in list and set nametag (variable "name (Type Objective)"")
            std::string label = objective->name;
            std::string objectiveType;
            label.append("	(");
            switch (type) {
                case UserOutput::ConstraintOutput::PATH:
                    objectiveType = "Path";
                    break;
                case UserOutput::ConstraintOutput::POINT:
                    objectiveType = "Point";
                    break;
                case UserOutput::ConstraintOutput::ENDPOINT:
                    objectiveType = "Endpoint";
                    break;
                default:
                    QErrorMessage::qtHandler()->showMessage(tr("ERROR: Congrats you reached the default in the switch type case! This most likely means, that a new constraint type was added, which needs to be included in here (GUI->outputwidget.cpp)as well!\n"));
                    break;
            }
            label.append(objectiveType);
            label.append(" Objective)");   // returns for example label = "velo (Endpoint)"
            item = new QListWidgetItem(tr(label.c_str()), ui->outputListWidgetConstraint);

            // turn item into a checkbox
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(Qt::Unchecked);
            countItemsConstr++;                                                  // add 1 to the item total of the corresponding list
            // very unclean solution, in the future objectives should be split from constraints
            vis.itemToConstraint[countItemsConstr] = std::make_pair(i, INT_MAX); // INT_MAX marks an "exception", so that the createChart and markConstraint functions draw the right thing
        }

        vis.assignColors(vis.mapVariableToColor);
    }

    updateUI();
}

/**
 * @brief returns the current output displayed in the output widget
 * 
 * @return UserOutput::Output 
 */
const UserOutput::Output& OutputWidget::getOutput()
{
    return output;
}

/**
 * @brief this function is called every time the user checks or unchecks an item in the outputListWidgetState
 *
 * @param item
 */
void OutputWidget::on_outputListWidgetState_itemChanged(QListWidgetItem *item)
{
    updateUI();
}

/**
 * @brief this function is called every time the user checks or unchecks an item in the outputListWidgetState
 *
 * @param item
 */
void OutputWidget::on_outputListWidgetParameter_itemChanged(QListWidgetItem *item)
{
    updateUI();
}

/**
 * @brief this function is called every time the user checks or unchecks an item in the outputListWidgetState
 *
 * @param item
 */
void OutputWidget::on_outputListWidgetConstraint_itemChanged(QListWidgetItem *item)
{
    updateUI();
}


/**
 * @brief builds the custom legend for state of the chart
 *
 * @param i the element in the state list
 */
void OutputWidget::updateLegendState(int i)
 {
    // find state data belonging to this list item
    int stageIndex = vis.itemToState[i].first;
    int stateIndex = vis.itemToState[i].second;
    UserOutput::StateOutput *state = &output.stages[stageIndex].integrator.states[stateIndex];
    
    // create label for legend in GUI
    std::string label = state->name;
    if (std::find(listLegendEntries.begin(), listLegendEntries.end(), label) == listLegendEntries.end())
    {
        // Element not in vector.

		// get right color in RGB
        QColor qcolor = vis.mapVariableToColor[label];
        int r, g, b, a;
        qcolor.getRgb(&r, &g, &b, &a);

        // using html commands
        QString labelText = "<b><p style='color:rgb(";
		labelText.append(std::to_string(r).c_str());
		labelText.append(",");
		labelText.append(std::to_string(g).c_str());
		labelText.append(",");
		labelText.append(std::to_string(b).c_str());
		labelText.append(");'>");
		labelText.append("- ");
		labelText.append(label.c_str());
		labelText.append("</p></b>");

        QWidget *entry = new QLabel(labelText, this);

        // tweak font and size
        QFont f("Calibri", 10);
        entry->setFont(f);

        // add entry to legend
        ui->chartLegend->addWidget(entry);

        // append list to vector list
        listLegendEntries.push_back(label);
        legendLabels.append(entry);
    }
    else
    {
        // Element already in vector -> no need to create duplicate entry
    }
 }

 /**
 * @brief builds the custom legend for parameter of the chart
 *
 * @param i the element in the parameter list
 */
void OutputWidget::updateLegendParam(int i)
 {
    // find parameter data belonging to this list item
    int stageIndex = vis.itemToParameter[i].first;
    int paramIndex = vis.itemToParameter[i].second;
    UserOutput::ParameterOutput *parameter = &output.stages[stageIndex].integrator.parameters[paramIndex];
    
    // create label for legend in GUI
    std::string label = parameter->name;
    if (std::find(listLegendEntries.begin(), listLegendEntries.end(), label) == listLegendEntries.end())
    {
        // Element not in vector.

		// get right color in RGB
        QColor qcolor = vis.mapVariableToColor[label];
        int r, g, b, a;
        qcolor.getRgb(&r, &g, &b, &a);

        // using html commands
        QString labelText = "<b><p style='color:rgb(";
        labelText.append(std::to_string(r).c_str());
        labelText.append(",");
		labelText.append(std::to_string(g).c_str());
        labelText.append(",");
		labelText.append(std::to_string(b).c_str());
        labelText.append(");'>");
		labelText.append("- ");
		labelText.append(label.c_str());
		labelText.append("</p></b>");

        QWidget *entry = new QLabel(labelText, this);

        // tweak font and size
        QFont f("Calibri", 10);
        entry->setFont(f);

        // add entry to legend
        ui->chartLegend->addWidget(entry);

        // append list to vector list
        listLegendEntries.push_back(label);
        legendLabels.append(entry);
    }
    else
    {
        // Element already in vector -> no need to create duplicate entry
    }
 }

 /**
 * @brief builds the custom legend for constraint of the chart
 *
 * @param i the element in the constraint list
 */
void OutputWidget::updateLegendConst(int i)
 {
    // find parameter data belonging to this list item
    int stageIndex  = vis.itemToConstraint[i].first;
    int constrIndex = vis.itemToConstraint[i].second;
    UserOutput::ConstraintOutput *constraint = &output.stages[stageIndex].optimizer.nonlinearConstraints[constrIndex];
    
    // create label for legend in GUI
    std::string label = constraint->name;
    if (std::find(listLegendEntries.begin(), listLegendEntries.end(), label) == listLegendEntries.end())
    {
        // Element not in vector.

		// get right color in RGB
        QColor qcolor = vis.mapVariableToColor[label];
        int r, g, b, a;
        qcolor.getRgb(&r, &g, &b, &a);

        // using html commands
        QString labelText = "<b><p style='color:rgb(";
        labelText.append(std::to_string(r).c_str());
        labelText.append(",");
		labelText.append(std::to_string(g).c_str());
        labelText.append(",");
		labelText.append(std::to_string(b).c_str());
        labelText.append(");'>");
		labelText.append("- ");
		labelText.append(label.c_str());
		labelText.append("</p></b>");

        QWidget *entry = new QLabel(labelText, this);

        // tweak font and size
        QFont f("Calibri", 10);
        entry->setFont(f);

        // add entry to legend
        ui->chartLegend->addWidget(entry);

        // append list to vector list
        listLegendEntries.push_back(label);
        legendLabels.append(entry);
    }
    else
    {
        // Element already in vector -> no need to create duplicate entry
    }
 }

 /**
 * @brief builds the custom legend for objective of the chart
 *
 * @param i the element in the objective list
 */
void OutputWidget::updateLegendObjec(int i)
 {
    // find parameter data belonging to this list item
    int stageIndex  = vis.itemToConstraint[i].first;
    UserOutput::ConstraintOutput *objective = &output.stages[stageIndex].optimizer.objective;
    
    // create label for legend in GUI
    std::string label = objective->name;
    if (std::find(listLegendEntries.begin(), listLegendEntries.end(), label) == listLegendEntries.end())
    {
        // Element not in vector.

		// get right color in RGB
        QColor qcolor = vis.mapVariableToColor[label];
        int r, g, b, a;
        qcolor.getRgb(&r, &g, &b, &a);

        // using html commands
        QString labelText = "<b><p style='color:rgb(";
        labelText.append(std::to_string(r).c_str());
        labelText.append(",");
		labelText.append(std::to_string(g).c_str());
        labelText.append(",");
		labelText.append(std::to_string(b).c_str());
        labelText.append(");'>");
		labelText.append("- ");
		labelText.append(label.c_str());
		labelText.append("</p></b>");

        QWidget *entry = new QLabel(labelText, this);

        // tweak font and size
        QFont f("Calibri", 10);
        entry->setFont(f);

        // add entry to legend
        ui->chartLegend->addWidget(entry);

        // append list to vector list
        listLegendEntries.push_back(label);
        legendLabels.append(entry);
    }
    else
    {
        // Element already in vector -> no need to create duplicate entry
    }
}

 /**
 * @brief write time values (calculation time, stage endtimes) on the GUI (right side)
 *
 */
void OutputWidget::updateOverviewTime()
{
    // clear sidebar of previous elements
    while (timeLabels.size() > 0)
    {
        QWidget *widget = timeLabels.takeLast();
        ui->chartStageTimes->removeWidget(widget);
        widget->deleteLater();
    }

    // fill with stage times
    std::vector<double> stageDurations;
    double totalTime = 0;
    for (int i = 0; i < output.stages.size(); i++)
    {
        // get duration for this stage
        double duration = output.stages[i].integrator.durations[0].value;
        totalTime += duration;

        // create label for duration of this stage
        std::string label = "Stage " + std::to_string(i + 1) + ": \t\t";
        label.append(std::to_string(duration));

        QWidget *entry = new QLabel(QString::fromStdString(label), this);
        // tweak font and size
        QFont font("Calibri", 10);
        entry->setFont(font);
        // add entry to legend
        ui->chartStageTimes->addWidget(entry);
        // append list to vector list
        timeLabels.append(entry);
    }

    // create label for total time
    QString labelText = tr("Total Time: ") + tr("\t");
    labelText.append(QString::number(totalTime));
    QWidget *label = new QLabel(labelText, this);
    QFont font("Calibri", 10);
    // tweak font and size
    label->setFont(font);
    // add entry to legend
    ui->chartStageTimes->addWidget(label);
    // append list to vector list
    timeLabels.append(label);

    // fill with new output elements - calculation time
    ui->labelCalculationTime->setText(QString::number(output.solutionTime));
}

/**
 * @brief this function opens a window with the entire json output
 */
void OutputWidget::on_buttonAdvanced_clicked()
{
    advancedOutput->show();
}

/**
 * @brief this function (de-)select all checkboxes in the state listView
 */
void OutputWidget::on_selectAllStates_stateChanged()
{
    if (ui->selectAllStates->isChecked())
    {
        // loop over entire list of states and check everything
        QListWidgetItem *item;
        for (int i = 0; i < ui->outputListWidgetState->count(); i++)
        {
            item = ui->outputListWidgetState->item(i); // look at one item at a time
            if (item->flags() == 53) //53 is the Qt constant for the flag checkable=true
            {
                item->setCheckState(Qt::Checked);
            }
        }
    }
    else
    {
        // loop over entire list of states and UNcheck everything
        QListWidgetItem *item;
        for (int i = 0; i < ui->outputListWidgetState->count(); i++)
        {
            item = ui->outputListWidgetState->item(i); // look at one item at a time
            // if (item->flags() == Qt::ItemIsUserCheckable)
            if (item->flags() == 53) //53 is the Qt constant for the flag checkable=true
                item->setCheckState(Qt::Unchecked);
        }
    }
}

/**
 * @brief this function (de-)select all checkboxes in the parameter listView
 */
void OutputWidget::on_selectAllParameters_stateChanged()
{
    QListWidgetItem *item;
    if (ui->selectAllParameters->isChecked())
    {
        for (int i = 0; i < ui->outputListWidgetParameter->count(); i++) // loop over entire list of states and check everything
        {
            item = ui->outputListWidgetParameter->item(i); // look at one item at a time
            if (item->flags() == 53)                       //53 is the Qt constant for the flag checkable=true
                item->setCheckState(Qt::Checked);
        }
    }
    else
    {
        for (int i = 0; i < ui->outputListWidgetParameter->count(); i++) // loop over entire list of states and UNcheck everything
        {
            item = ui->outputListWidgetParameter->item(i); // look at one item at a time
            if (item->flags() == 53)                       //53 is the Qt constant for the flag checkable=true
                item->setCheckState(Qt::Unchecked);
        }
    }
}

/**
 * @brief this function (de-)select all checkboxes in the constraints listView
 */
void OutputWidget::on_selectAllConstraints_stateChanged()
{
    QListWidgetItem *item;
    if (ui->selectAllConstraints->isChecked())
    {
        for (int i = 0; i < ui->outputListWidgetConstraint->count(); i++) // loop over entire list of states and check everything
        {
            item = ui->outputListWidgetConstraint->item(i); // look at one item at a time
            if (item->flags() == 53)                        //53 is the Qt constant for the flag checkable=true
                item->setCheckState(Qt::Checked);
        }
    }
    else
    {
        for (int i = 0; i < ui->outputListWidgetConstraint->count(); i++) // loop over entire list of states and UNcheck everything
        {
            item = ui->outputListWidgetConstraint->item(i); // look at one item at a time
            if (item->flags() == 53)                        //53 is the Qt constant for the flag checkable=true
                item->setCheckState(Qt::Unchecked);
        }
    }
}

/**
 * @brief toggles the gridlines in the chart on/off
 * 
 */
void OutputWidget::on_buttonShowGrid_clicked()
{
    if (vis.showGridLines == true) {
        vis.showGridLines = false;
    } else {
        vis.showGridLines = true;
    }
    updateUI();
}

/**
 * @brief toggles the stagesahding in the chart on/off
 * 
 */
void OutputWidget::on_buttonShowStageShader_clicked()
{
    if (vis.showStageShading == true) {
        vis.showStageShading = false;
    } else {
        vis.showStageShading = true;
    }
    updateUI();
}