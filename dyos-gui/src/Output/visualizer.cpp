#include "visualizer.h"

Visualizer::Visualizer()
{}

Visualizer::~Visualizer()
{}

/**
 * @brief Setter function for the visualizer class, sets the output variable
 * 
 * @param newOutput gets output from dyos thread or loading
 */
void Visualizer::setOutput(UserOutput::Output newOutput)
{
    output = newOutput;
}

/**
 * @brief creates a QLineSeries * which is used to build a chart.
 * 
 * @param vectorX vector containing the values on the x axis
 * @param vectorY vector containing the values on the y axis
 * 
 * @return QLineSeries * series
 */
QLineSeries * Visualizer::createLineSeries(std::vector<double> vectorX, std::vector<double> vectorY)
{
    QLineSeries * series = new QLineSeries();

    for (int i = 0; i < vectorY.size (); ++i) {
        series->append(vectorX[i], vectorY[i]);
    }

    return series;
}

/**
 * @brief creates a QChartView * which is needed for the QWidget.
 * (this overloadec function creats a line chart based on a line series)
 * 
 * @param series QLineSeries which is used to create a line chart
 * 
 * @return QChartView * chartView
 */
QChartView * Visualizer::createChartView(QLineSeries *series)
{
    QChart * chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("Testfunction: accel - grids_0");
    chart->setAnimationOptions(QChart::AllAnimations);

    QChartView * chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    return chartView;
}

/**
 * @brief creates a QChartView * which is needed for the QWidget.
 * (this overloadec function creats a bar chart based on a bar series)
 * 
 * @param series QBarSeries which is used to create a bar chart
 * 
 * @return QChartView * chartView
 */
QChartView * Visualizer::createChartView(QBarSeries *series)
{
    QChart * chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Values by runs");
    chart->setAnimationOptions(QChart::AllAnimations);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    return chartView;
}

// ----------------------- specific functions for dyos objects ----------------------- //

/**
 * @brief this function is called whenever the selection
 * of the states changes, and it creates the whole chart from scratch.
 * This function also shifts the timepoints to fit together the data.
 *
 * @param chart object where a new series with state values is added
 * @param i counter to find selected object in the itemToState map
 * 
 * @return QChart * chart
 */
QChart *Visualizer::createChartState(QChart *chart, int i)
{
    UserOutput::StateOutput *state;
    std::vector<double> shiftedTimePoints;
    std::vector<double> values;
    std::string name;

    // find state data belonging to this list item
    int stageIndex = itemToState[i].first;
    int stateIndex = itemToState[i].second;

    state = &output.stages[stageIndex].integrator.states[stateIndex];
    // adjust time points by adding all the durations of the previous stages (if there are any)
    shiftedTimePoints = state->grid.timePoints;
    for (int l = 0; l < stageIndex; l++)
    {
        for (int m = 0; m < shiftedTimePoints.size(); m++)
        {
            double duration = output.stages[l].integrator.durations[0].value;
            shiftedTimePoints[m] += duration;
        }
    }
    values = state->grid.values;
    name = state->name;

    // now state points to the correct StateOutput object
    // -> create a line series from it and add it to the chart
    QLineSeries *series = createLineSeries(shiftedTimePoints, values);
    series->setName(tr(name.c_str()));
    chart->addSeries(series);

    // Customize series (set line color, width) use mapVariableToColor to color the lineseries
    // find pen color belonging to this variable
    QColor qcolor = mapVariableToColor[name];
    QPen pen(qcolor);
    //pen.setWidth(5);
    series->setPen(pen);

    chart = chartMarkStages(chart, series); // make chart pretty

    return chart;
}

/**
 * @brief this function is called whenever the selection
 * of the parameter changes, and it creates the whole chart from scratch.
 * This function also shifts the timepoints to fit together the data.
 * 
 * Furthermore if the case is PIECEWISE_CONSTANT, the vector has to be fitted.
 *
 * @param chart object where a new series with parameter values is added
 * @param i counter to find selected object in the itemToParameter map
 * 
 * @return QChart * chart
 */
QChart *Visualizer::createChartParameter(QChart *chart, int i)
{
    UserOutput::ParameterOutput *parameter;
    std::vector<double> shiftedTimePoints;
    std::vector<double> values;
    std::string name;

    // find parameter data belonging to this list item
    int stageIndex = itemToParameter[i].first;
    int paramIndex = itemToParameter[i].second;

    parameter = &output.stages[stageIndex].integrator.parameters[paramIndex];

    // adjust time points to scale from [0,1] to [0,duration]
    shiftedTimePoints = parameter->grids[0].timePoints;

	if (shiftedTimePoints.size() == 0)
	{
		// vector is empty -> nothing to do
		QErrorMessage::qtHandler()->showMessage(tr("Warning: createChartParameter() - the value that you selected is empty, nothing will be displayed.\n"));
	}
	else
	{
		for (int m = 0; m < shiftedTimePoints.size(); m++)
		{
			double duration = parameter->grids[0].duration; // equivalent to output.stages[l].integrator.durations[0].value;
			shiftedTimePoints[m] *= duration;
		}
		// adjust time points by adding all the durations of the previous stages (if there are any)
		for (int l = 0; l < stageIndex; l++)
		{
			for (int m = 0; m < shiftedTimePoints.size(); m++)
			{
				double duration = output.stages[l].integrator.durations[0].value;
				shiftedTimePoints[m] += duration;
			}
		}
		values = parameter->grids[0].values;
		name   = parameter->name;

		// in case it is PIECEWISE_CONSTANT the vectos have to be manipulated
		// to fake a piecewise constant line chart
		UserOutput::ParameterGridOutput::ApproximationType type = parameter->grids[0].type;
		std::vector<double> tempTime = shiftedTimePoints;
		std::vector<double> tempValue = values;

		if (type == UserOutput::ParameterGridOutput::PIECEWISE_CONSTANT)
		{
			// double old vector sizes
			shiftedTimePoints.resize((shiftedTimePoints.size() - 1) * 2);
			values.resize(values.size() * 2);

			// double each timePoint, but add numeric limit, so that no two value correspond to the same time
			for (int i = 0; i < tempTime.size(); i++)
			{
				if (i == 0)
				{
					shiftedTimePoints[0] = tempTime[0];
				}
				else if (i == tempTime.size() - 1)
				{
					shiftedTimePoints[2 * i - 1] = tempTime[i];
				}
				else
				{
					shiftedTimePoints[2 * i - 1] = tempTime[i];
					shiftedTimePoints[2 * i]     = tempTime[i] + std::numeric_limits<double>::min();
				}

				// double each value to draw horizontal steps
				for (int i = 0; i < tempTime.size() - 1; i++)
				{
					values[2 * i]     = tempValue[i];
					values[2 * i + 1] = tempValue[i];
				}
			}
		}
		else
		{
			// no need to do anything -> standard is a linear plot
		}

		// now state points to the correct StateOutput object
		// -> create a line series from it and add it to the chart
		QLineSeries *series = createLineSeries(shiftedTimePoints, values);
		series->setName(tr(name.c_str()));
		chart->addSeries(series);

		// Customize series (set line color, width) use mapVariableToColor to color the lineseries
		// find pen color belonging to this variable
		QColor qcolor = mapVariableToColor[name];
		QPen pen(qcolor);
		//pen.setWidth(3);
		series->setPen(pen);

		chart = chartMarkStages(chart, series); // make chart pretty
	}

    return chart;
}

/**
 * @brief this function is called whenever the selection
 * of the constraints changes, and it creates the whole chart from scratch.
 * 
 * This function also shifts the timepoints to fit together the data.
 * Furthermore it plots constraints and objectives,
 * to differ these two types INT_MAX is used as constrIndex.
 *
 * @param chart object where a new series with constraint values is added
 * @param i counter to find selected object in the itemToConstraint map
 * 
 * @return QChart * chart
 */
QChart *Visualizer::createChartConstraint(QChart *chart, int i)
{
    std::vector<double> shiftedTimePoints;
    std::vector<double> values;
    std::string name;

    // find parameter data belonging to this list item
    int stageIndex  = itemToConstraint[i].first;
    int constrIndex = itemToConstraint[i].second;

    // constraints and objectives are combined, the INT_MAX signals, that the object is an objective and not a constraint
    // objectives have to be adressed differnetly (in the "else{}"" part)
    if (constrIndex != INT_MAX)   // index != INT_MAX -> it is a constraint, if true -> it is a objective
    {
        UserOutput::ConstraintOutput *constraint = &output.stages[stageIndex].optimizer.nonlinearConstraints[constrIndex];
        // adjust time points to scale from [0,1] to [0,duration]
        shiftedTimePoints = constraint->grids[0].timePoints;
        for (int m = 0; m < shiftedTimePoints.size(); m++)
        {
            double duration = output.stages[stageIndex].integrator.durations[0].value;
            shiftedTimePoints[m] *= duration;
        }

        // adjust time points by adding all the durations of the previous stages (if there are any)
        for (int l = 0; l < stageIndex; l++)
        {
            for (int m = 0; m < shiftedTimePoints.size(); m++)
            {
                double duration = output.stages[l].integrator.durations[0].value;
                shiftedTimePoints[m] += duration;
            }
        }
        values = constraint->grids[0].values;
        name   = constraint->name;

        // now state points to the correct StateOutput object
        // -> create a line series from it and add it to the chart
        QLineSeries *series = createLineSeries(shiftedTimePoints, values);
        series->setName(tr(name.c_str()));
        chart->addSeries(series);

        // Customize series (set line color, width) use mapVariableToColor to color the lineseries
        // find pen color belonging to this variable
        QColor qcolor = mapVariableToColor[name];
        QPen pen(qcolor);
        //pen.setWidth(5);
        series->setPen(pen);

        // Mark contraints
        std::pair<double, double> timePoints; // stage begin and end time
        timePoints = std::make_pair(shiftedTimePoints.front(), shiftedTimePoints.back());
        chart = chartMarkConstraints(chart, i, timePoints); // mark chosen constraints

        // Adding stage markers to the bottom axis
        // (this step has to be last, because createDefaultAxis(...) has to be run
        // once every line series was addded!)
        chart = chartMarkStages(chart, series);
    }
    else if (constrIndex == INT_MAX) // index != INT_MAX -> it is a constraint, if true -> it is a objective
    {
        UserOutput::ConstraintOutput *objective = &output.stages[stageIndex].optimizer.objective;
        // adjust time points to scale from [0,1] to [0,duration]
        shiftedTimePoints = objective->grids[0].timePoints;
        for (int m = 0; m < shiftedTimePoints.size(); m++)
        {
            double duration = output.stages[stageIndex].integrator.durations[0].value;
            shiftedTimePoints[m] *= duration;
        }

        // adjust time points by adding all the durations of the previous stages (if there are any)
        for (int l = 0; l < stageIndex; l++)
        {
            for (int m = 0; m < shiftedTimePoints.size(); m++)
            {
                double duration = output.stages[l].integrator.durations[0].value;
                shiftedTimePoints[m] += duration;
            }
        }
        values = objective->grids[0].values;
        name   = objective->name;

        // now state points to the correct StateOutput object
        // -> create a line series from it and add it to the chart
        QLineSeries *series = createLineSeries(shiftedTimePoints, values);
        series->setName(tr(name.c_str()));
        chart->addSeries(series);

        // Customize series (set line color, width) use mapVariableToColor to color the lineseries
        // find pen color belonging to this variable
        QColor qcolor = mapVariableToColor[name];
        QPen pen(qcolor);
        //pen.setWidth(5);
        series->setPen(pen);

        // Mark contraints
        std::pair<double, double> timePoints; // stage begin and end time
        timePoints = std::make_pair(shiftedTimePoints.front(), shiftedTimePoints.back());
        chart = chartMarkObjectives(chart, series, i, timePoints); // mark chosen objectives

        // Adding stage markers to the bottom axis
        // (this step has to be last, because createDefaultAxis(...) has to be run
        // once every line series was addded!)
        chart = chartMarkStages(chart, series);
    }
    else
    {
        QErrorMessage::qtHandler()->showMessage(tr("ERROR: createChartConstraint() - a problem with the enum 'itemToConstraint' occured. The itemToConstraint[i].second is not in the expected region.\n"));
    }

    return chart;
}

/**
 * @brief this function is called whenever a new chart is drawn,
 * mark stages, color background and label x axis.
 *
 * @param chart object where the stages will the highlighted
 * @param series used to add labels with stage numbering
 * 
 * @return QChart * chart
 */
QChart *Visualizer::chartMarkStages(QChart *chart, QLineSeries *series)
{
    chart->createDefaultAxes(); // Creating default axes has to be done last, otherwise the old labels and other custom functions will be reset!
    chart->setAnimationOptions(QChart::AllAnimations);

    // Customize entire plot area background       // can be used in a future function - where background color can be configured
    // RWTH dunkel Blau QColor(0,84,159,255) 100% 75% 50% 25% 10%
    // immer heller nach rechts QColor(64,127,183,255) QColor(142,186,229,255) QColor(199,221,242,255) QColor(232,241,250,255)
    /*QLinearGradient plotAreaGradient;
    plotAreaGradient.setStart(QPointF(0, 1));
    plotAreaGradient.setFinalStop(QPointF(1, 0));
    plotAreaGradient.setColorAt(0.0, QColor(199,221,242,255));
    plotAreaGradient.setColorAt(1.0, QColor(199,221,242,255));
    plotAreaGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    chart->setPlotAreaBackgroundBrush(plotAreaGradient);
    chart->setPlotAreaBackgroundVisible(true);*/

    // Customize axis - add label stages - WARNING has to be done after createDefaultAxes(), otherwise all changes will be reset
    QCategoryAxis *axisX = new QCategoryAxis();
    QCategoryAxis *axisY = new QCategoryAxis();
    std::string label;
    double timePoint = 0;
    for (int i = 0; i < output.stages.size(); i++)
    {
        label = "Stage " + std::to_string(i+1);
        QString qlabel = QString::fromStdString(label);
        timePoint += output.stages[i].integrator.durations[0].value;

        axisX->append(qlabel, timePoint);
    }

    // Customize shades
    if (showStageShading == true) {
        axisX->setShadesPen(Qt::NoPen);
	    axisX->setShadesBrush(QBrush(QColor(0, 0, 0, 7)));  // color each second stage background gray
        axisX->setShadesVisible(true); // show regions along x axis
    }
    
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    return chart;
}

/**
 * @brief this function is called whenever a new chart is drawn,
 * mark chosen constraints i in the chart
 * 
 * Three cases PATH, POINT, ENDPOINT are drawn each with their own visualization.
 * PATH     A tube with dashed boundary lines
 * POINT    A boxplot placed at a defined time, with median=dyos_solution, whiskers=boundaries
 * ENDPOINT A boxplot placed at the stage end,  with median=dyos_solution, whiskers=boundaries
 *
 * @param chart object where the stages will the highlighted
 * @param i used to find the correct item in the itemToConstraint map
 * @param timePoints hold the start and endtime of the stage
 * 
 * @return QChart * chart
 */
QChart * Visualizer::chartMarkConstraints(QChart *chart, int i, std::pair<double,double> timePoints)
{
    // find parameter data belonging to this list item
    int stageIndex  = itemToConstraint[i].first;
    int constrIndex = itemToConstraint[i].second;

    UserOutput::ConstraintOutput *constraint = &output.stages[stageIndex].optimizer.nonlinearConstraints[constrIndex];
    UserOutput::ConstraintOutput::ConstraintType type = constraint->type;

    // general boundaries for time
    double timeStart  = timePoints.first;       // stage begin and end time
    double timeEnd    = timePoints.second;      // stage begin and end time
    // general boundaries for values
    double lowerBound = constraint->lowerBound; // stage value lower and upper boundaries
    double upperBound = constraint->upperBound; // stage value lower and upper boundaries

    switch (type) {
        case UserOutput::ConstraintOutput::PATH: // PATH
            // draw a rectangle into the chart - showing lower and upper bound over the stage
            chart = drawPath(constraint, chart, timePoints);

            break;
        case UserOutput::ConstraintOutput::POINT: // POINT
            // draw a box plot where the point constraint is located in time and values
            chart = drawPoint(constraint, chart, timePoints, stageIndex);

            break;
        case UserOutput::ConstraintOutput::ENDPOINT: // ENDPOINT
            // draw a box plot where the point constraint is located at the stage end and values
            // calculated value from dyos is the "median", the "whiskers" are the boundaries
            chart = drawEndpoint(constraint, chart, timePoints);

            break;
        default:
            QErrorMessage::qtHandler()->showMessage(tr("ERROR: Congrats you reached the default in the switch type case! This most likely means, that a new constraint type was added,which needs to be included in here (GUI->outputwidget.cpp)as well!\n"));
            break;
    }

    return chart;
}

/**
 * @brief this function is called whenever a new chart is drawn,
 * mark chosen objective i in the chart
 * 
 * Three cases PATH, POINT, ENDPOINT are drawn each with their own visualization.
 * PATH     A tube with dashed boundary lines
 * POINT    A boxplot placed at a defined time, with median=dyos_solution, whiskers=boundaries
 * ENDPOINT A boxplot placed at the stage end,  with median=dyos_solution, whiskers=boundaries
 *
 * @param chart object where the stages will the highlighted
 * @param series used to add objective markers
 * @param i used to find the correct item in the itemToConstraint map
 * @param timePoints hold the start and endtime of the stage
 * 
 * @return QChart * chart
 */
QChart * Visualizer::chartMarkObjectives(QChart *chart, QLineSeries *series, int i, std::pair<double,double> timePoints)
{
    // find parameter data belonging to this list item
    int stageIndex  = itemToConstraint[i].first;

    UserOutput::ConstraintOutput *objective = &output.stages[stageIndex].optimizer.objective;
    UserOutput::ConstraintOutput::ConstraintType type = objective->type;

    // general boundaries for time
    double timeStart  = timePoints.first;       // stage begin and end time
    double timeEnd    = timePoints.second;      // stage begin and end time
    // general boundaries for values
    double lowerBound = objective->lowerBound; // stage value lower and upper boundaries
    double upperBound = objective->upperBound; // stage value lower and upper boundaries

    switch (type) {
        case UserOutput::ConstraintOutput::PATH: // PATH
            // draw a rectangle into the chart - showing lower and upper bound over the stage
            chart = drawPath(objective, chart, timePoints);

            break;
        case UserOutput::ConstraintOutput::POINT: // POINT
            // draw a box plot where the point constraint is located in time and values
            chart = drawPoint(objective, chart, timePoints, stageIndex);
            
            break;
        case UserOutput::ConstraintOutput::ENDPOINT: // ENDPOINT
            // draw a box plot where the point constraint is located at the stage end and values
            // calculated value from dyos is the "median", the "whiskers" are the boundaries

            chart = drawEndpoint(objective, chart, timePoints);

            break;
        default:
            QErrorMessage::qtHandler()->showMessage(tr("ERROR: Congrats you reached the default in the switch type case! This most likely means, that a new constraint type was added,which needs to be included in here (GUI->outputwidget.cpp)as well!\n"));
            break;
    }

    return chart;
}

/**
 * @brief draws the constrainttype PATH - a tube
 * 
 * Both constraints and objectives can be drawn with this function.
 * Three cases PATH, POINT, ENDPOINT are drawn each with their own visualization.
 * PATH     A tube with dashed boundary lines
 * POINT    A boxplot placed at a defined time, with median=dyos_solution, whiskers=boundaries
 * ENDPOINT A boxplot placed at the stage end,  with median=dyos_solution, whiskers=boundaries
 *
 * @param constraint pointer to the constraint
 * @param chart the object where the markers are added
 * @param timePoints hold the start and endtime of the stage
 * 
 * @return QChart * chart
 */
QChart * Visualizer::drawPath(UserOutput::ConstraintOutput *constraint, QChart *chart, std::pair<double,double> timePoints)
{
    std::vector<double> vTime{ 0, 0 };
    std::vector<double> vBoundaries{ 0, 0 };
    QLineSeries *series0 = new QLineSeries();
	QLineSeries *series1 = new QLineSeries();
	QAreaSeries *area;
    QLinearGradient gradient;
    std::string name;
    QColor qcolor;
    QPen pen;

    // general boundaries for time
    double timeStart  = timePoints.first;       // stage begin and end time
    double timeEnd    = timePoints.second;      // stage begin and end time
    // general boundaries for values
    double lowerBound = constraint->lowerBound; // stage value lower and upper boundaries
    double upperBound = constraint->upperBound; // stage value lower and upper boundaries

    // convert needed data pair into vector for createLineSeries function
    vTime = {timeStart, timeEnd};
    vBoundaries = {lowerBound, lowerBound};
    series0 = createLineSeries(vTime, vBoundaries);
    vBoundaries = {upperBound, upperBound};
    series1 = createLineSeries(vTime, vBoundaries);

    // create area from boundary series
    area = new QAreaSeries(series0, series1);

    // Customize series (set line color, width) use mapVariableToColor to color the lineseries
    // find pen color belonging to this variable
    name = constraint->name;
    qcolor = mapVariableToColor[name];

    // draw dashed boundary lines
    pen.setColor(qcolor);
    pen.setStyle(Qt::DashLine);
    series0->setPen(pen);
    series1->setPen(pen);
    chart->addSeries(series0);
    chart->addSeries(series1);

    // color boundary zone in stage according to the colormap
    qcolor.setAlpha(10);           // make the color transparent = reduce alpha value
    pen.setColor(Qt::transparent); // hide borders, otherwise it would draw boundaries on the left and right
    area->setPen(pen);
    area->setBrush(QBrush(qcolor));

    chart->addSeries(area);

    return chart;
}

/**
 * @brief draws the constrainttype POINT - a "boxplot"
 * 
 * Both constraints and objectives can be drawn with this function.
 * Three cases PATH, POINT, ENDPOINT are drawn each with their own visualization.
 * PATH     A tube with dashed boundary lines
 * POINT    A boxplot placed at a defined time, with median=dyos_solution, whiskers=boundaries
 * ENDPOINT A boxplot placed at the stage end,  with median=dyos_solution, whiskers=boundaries
 *
 * @param constraint pointer to the constraint
 * @param chart the object where the markers are added
 * @param timePoints hold the start and endtime of the stage
 * 
 * @return QChart * chart
 */
QChart *Visualizer::drawPoint(UserOutput::ConstraintOutput *constraint, QChart *chart, std::pair<double, double> timePoints, int stageIndex)
{
    std::vector<double> shiftedTimePoints;
    double duration;

	std::vector<double> vTime{ 0, 0 };
	std::vector<double> vBoundaries{ 0, 0 };
	QLineSeries *series0 = new QLineSeries();
	QLineSeries *series1 = new QLineSeries();
	QAreaSeries *area;
	QLinearGradient gradient;
	std::string name;
	QColor qcolor;
	QPen pen;

	// general boundaries for time
	double timeStart = timePoints.first;       // stage begin and end time
	double timeEnd = timePoints.second;      // stage begin and end time
	// general boundaries for values
	double lowerBound = constraint->lowerBound; // stage value lower and upper boundaries
	double upperBound = constraint->upperBound; // stage value lower and upper boundaries

    // get timepoint
    // adjust time points to scale from [0,1] to [0,duration]
    shiftedTimePoints = constraint->grids[0].timePoints;
    duration = output.stages[stageIndex].integrator.durations[0].value;
    shiftedTimePoints[0] *= duration;

    // adjust time points by adding all the durations of the previous stages (if there are any)
    for (int l = 0; l < stageIndex; l++)
    {
        duration = output.stages[l].integrator.durations[0].value;
        shiftedTimePoints[0] += duration;
    }

    // convert needed data pair into vector for createLineSeries function
    vTime = {shiftedTimePoints[0] - std::numeric_limits<double>::min(), shiftedTimePoints[0]};
    vBoundaries = {lowerBound, upperBound};
    series0 = createLineSeries(vTime, vBoundaries);

    // Customize series (set line color, width) use mapVariableToColor to color the lineseries
    // find pen color belonging to this variable
    name = constraint->name;
    qcolor = mapVariableToColor[name];

    // draw dashed boundary lines
    pen.setColor(qcolor);
    pen.setStyle(Qt::DashLine);
    series0->setPen(pen);

    chart->addSeries(series0);

    // hacky solution to add boundary whiskers, as standard qt functuions don't make it possible to add a boxplot
    // get axis ranges to scale whiskers
    // QLogValueAxis::min(); QLogValueAxis::max();              // TO DO

	// check if boundaries are the same, the a X is shown instead of the whiskers
	if (lowerBound == upperBound)
	{
		// create the X symbol
		vTime = { timeEnd - 1, timeEnd + 1 };
		vBoundaries = { lowerBound - 5, lowerBound + 5 };
		series0 = createLineSeries(vTime, vBoundaries);
		vBoundaries = { lowerBound + 5, lowerBound - 5 };
		series1 = createLineSeries(vTime, vBoundaries);

		// draw dashed boundary lines
		pen.setColor(qcolor);
		pen.setStyle(Qt::DashLine);
		series0->setPen(pen);
		series1->setPen(pen);

		chart->addSeries(series0);
		chart->addSeries(series1);
		return chart;
	}

    // add markers for boundary ends
    // create lower "whiskers" by adding to short series at the lower boundary value
    vTime = {shiftedTimePoints[0] - 1, shiftedTimePoints[0] + 1};
    vBoundaries = {lowerBound, lowerBound};
    series0 = createLineSeries(vTime, vBoundaries);
    vBoundaries = {lowerBound + 5, lowerBound + 5};
    series1 = createLineSeries(vTime, vBoundaries);
    // create area from boundary series
    area = new QAreaSeries(series0, series1);
    area->setBrush(QBrush(qcolor));
    chart->addSeries(area);
    // create upper "whiskers" by adding to short series at the upper boundary value
    vTime = {shiftedTimePoints[0] - 1, shiftedTimePoints[0] + 1};
    vBoundaries = {upperBound - 5, upperBound - 5};
    series0 = createLineSeries(vTime, vBoundaries);
    vBoundaries = {upperBound, upperBound};
    series1 = createLineSeries(vTime, vBoundaries);
    // create area from boundary series
    area = new QAreaSeries(series0, series1);
    area->setBrush(QBrush(qcolor));
    chart->addSeries(area);

    return chart;
}

/**
 * @brief draws the constrainttype ENDPOINT - a "boxplot" at the stageend
 * 
 * Both constraints and objectives can be drawn with this function.
 * Three cases PATH, POINT, ENDPOINT are drawn each with their own visualization.
 * PATH     A tube with dashed boundary lines
 * POINT    A boxplot placed at a defined time, with median=dyos_solution, whiskers=boundaries
 * ENDPOINT A boxplot placed at the stage end,  with median=dyos_solution, whiskers=boundaries
 *
 * @param constraint pointer to the constraint
 * @param chart the object where the markers are added
 * @param timePoints hold the start and endtime of the stage
 * 
 * @return QChart * chart
 */
QChart *Visualizer::drawEndpoint(UserOutput::ConstraintOutput *constraint, QChart *chart, std::pair<double, double> timePoints)
{
	std::vector<double> vTime{ 0, 0 };
	std::vector<double> vBoundaries{ 0, 0 };
	QLineSeries *series0 = new QLineSeries();
	QLineSeries *series1 = new QLineSeries();
	QAreaSeries *area;
	QLinearGradient gradient;
	std::string name;
	QColor qcolor;
	QPen pen;

	// general boundaries for time
	double timeStart = timePoints.first;       // stage begin and end time
	double timeEnd = timePoints.second;      // stage begin and end time
	// general boundaries for values
	double lowerBound = constraint->lowerBound; // stage value lower and upper boundaries
	double upperBound = constraint->upperBound; // stage value lower and upper boundaries

    // convert needed data pair into vector for createLineSeries function
    vTime = {timeEnd - std::numeric_limits<double>::min(), timeEnd};
    vBoundaries = {lowerBound, upperBound};
    series0 = createLineSeries(vTime, vBoundaries);

    // Customize series (set line color, width) use mapVariableToColor to color the lineseries
    // find pen color belonging to this variable
    name = constraint->name;
    qcolor = mapVariableToColor[name];

    // draw dashed boundary lines
    pen.setColor(qcolor);
    pen.setStyle(Qt::DashLine);
    series0->setPen(pen);

    chart->addSeries(series0);

    // hacky solution to add boundary whiskers, as standard qt functuions don't make it possible to add a boxplot
    // get axis ranges to scale whiskers
    // QLogValueAxis::min(); QLogValueAxis::max();              // TO DO

    // check if boundaries are the same, the a X is shown instead of the whiskers
    if (lowerBound == upperBound)
    {
        // create the X symbol
        vTime = {timeEnd - 1, timeEnd + 1};
        vBoundaries = {lowerBound - 5, lowerBound + 5};
        series0 = createLineSeries(vTime, vBoundaries);
        vBoundaries = {lowerBound + 5, lowerBound - 5};
        series1 = createLineSeries(vTime, vBoundaries);

        // draw dashed boundary lines
        pen.setColor(qcolor);
        pen.setStyle(Qt::DashLine);
        series0->setPen(pen);
        series1->setPen(pen);

        chart->addSeries(series0);
        chart->addSeries(series1);
		return chart;
    }

    // add markers for boundary ends
    // create lower "whiskers" by adding to short series at the lower boundary value
    vTime = {timeEnd - 1, timeEnd + 1};
    vBoundaries = {lowerBound, lowerBound};
    series0 = createLineSeries(vTime, vBoundaries);
    vBoundaries = {lowerBound + 5, lowerBound + 5};
    series1 = createLineSeries(vTime, vBoundaries);
    // create area from boundary series
    area = new QAreaSeries(series0, series1);
    area->setBrush(QBrush(qcolor));
    chart->addSeries(area);
    // create upper "whiskers" by adding to short series at the upper boundary value
    vTime = {timeEnd - 1, timeEnd + 1};
    vBoundaries = {upperBound - 5, upperBound - 5};
    series0 = createLineSeries(vTime, vBoundaries);
    vBoundaries = {upperBound, upperBound};
    series1 = createLineSeries(vTime, vBoundaries);
    // create area from boundary series
    area = new QAreaSeries(series0, series1);
    area->setBrush(QBrush(qcolor));
    chart->addSeries(area);

    return chart;
}

/**
 * @brief takes in a color map and assigns unique colors to all entries
 * 
 * @param map 
 */
void Visualizer::assignColors(std::map<std::string, QColor>& map)
{
    const int numOfDifferentColors = 10;

    int colorIndex=0;
    for (auto it = map.begin(); it!=map.end();it++) {
        colorIndex = colorIndex % numOfDifferentColors; // loop around after all colors are used
        QColor color;
        switch (colorIndex++) {
        case 0:
            color = QColor(0,97,101,255);   // RWTH Petrol
            break;
        case 1:
            color = QColor(0,84,159,255);   // RWTH Blau Primaerfarbe
            break;
        case 2:
            color = QColor(227,0,102,255);  // RWTH Magenta
            break;
        case 3:
            color = QColor(87,171,39,255);  // RWTH Gruen
            break;
        case 4:
            color = QColor(246,168,0,255);  // RWTH Orange
            break;
        case 5:
            color = QColor(255,237,0,255);  // RWTH Gelb
            break;
        case 6:
            color = QColor(204,7,30,255);   // RWTH Rot
            break;
        case 7:
            color = QColor(97,33,88,255);   // RWTH Violett
            break;
        case 8:
            color = QColor(161,16,53,255);  // RWTH Bordeaux
            break;
        case 9:
            color = QColor(122,111,172,255);// RWTH Lila
            break;
        default:
            color = QColor(0,0,0,255);      // Schwarz
            break;
        }

        // assign color
        it->second = color;
    } 
}

/**
 * @brief toggles the gridlines in the chart on/off
 * 
 * Has to be called after all series have been added in OutputWidget::updateUI() to the chart.
 * 
 */
QChart * Visualizer::toggleGridLines(QChart *chart)
{
    // Customize gridlines - toggle only nonzero gridlines
    QList<QAbstractAxis *> axesListHorizontal = chart->axes(Qt::Horizontal);
    QList<QAbstractAxis *> axesListVertical   = chart->axes(Qt::Vertical);
    for (auto it = axesListHorizontal.begin(); it != axesListHorizontal.end(); it++)
        (*it)->setGridLineVisible(showGridLines);
    for (auto it = axesListVertical.begin(); it != axesListVertical.end(); it++)
        (*it)->setGridLineVisible(showGridLines);

    return chart;
}

// ----------------------- HARDCODED EXAMPLES FOR TESTING ----------------------- //

/**
 * @brief takes in a color map and assigns unique colors to all entries
 * 
 * @return QChart * chart
 */
QChartView * Visualizer::example1()
{
    QLineSeries *series = new QLineSeries();
    series->append(0*20, 7.5204537130704627e-08);
    series->append(0.20000000000000001*20, -3.3418154857722501e-08);
    series->append(0.40000000000000002*20, -5.150929252466608e-08);
    series->append(0.59999999999999998*20, 9.4650352030757748e-08);
    series->append(0.80000000000000004*20, -6.2100017746043408e-08);
    series->append(1*20, 3.6910301615695932e-08);

    QChart * chart = new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("accel - grids_0");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    return chartView;
}

/**
 * @brief creates a hardcoded example QLineSeries, used for quickly assembling a line chart
 * 
 * @return QChart * chart
 */
QLineSeries * Visualizer::createLineSeries1()
{
    QLineSeries *series = new QLineSeries();
    series->append(0*20, 7.5204537130704627e-08);
    series->append(0.20000000000000001*20, -3.3418154857722501e-08);
    series->append(0.40000000000000002*20, -5.150929252466608e-08);
    series->append(0.59999999999999998*20, 9.4650352030757748e-08);
    series->append(0.80000000000000004*20, -6.2100017746043408e-08);
    series->append(1*20, 3.6910301615695932e-08);

    return series;
}

/**
 * @brief creates a hardcoded example QBarSeries, used for quickly assembling a bar chart
 * 
 * @return QChart * chart
 */
QBarSeries * Visualizer::createBarSeries1()
{
    QBarSet *set0 = new QBarSet("Name 1");
    QBarSet *set1 = new QBarSet("Name 2");
    QBarSet *set2 = new QBarSet("Name 3");

    *set0 << 283 << 341 << 313 << 338 << 346 << 335;
    *set1 << 250 << 315 << 282 << 307 << 303 << 330;
    *set2 << 294 << 246 << 257 << 319 << 300 << 325;

    QBarSeries *series = new QBarSeries();
    series->append(set0);
    series->append(set1);
    series->append(set2);

    return series;
}