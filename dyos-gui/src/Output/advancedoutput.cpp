#include "advancedoutput.h"
#include "ui_advancedoutput.h"

AdvancedOutput::AdvancedOutput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdvancedOutput)
{
    ui->setupUi(this);
    // connect the tree view to the model
    ui->outputTreeView->setModel(&qJsonModel);
}

AdvancedOutput::~AdvancedOutput()
{
    delete ui;
}

/**
 * @brief change the current output displayed in the advanced output widget
 * 
 * @param newOutput 
 */
void AdvancedOutput::setOutput(const UserOutput::Output &newOutput) 
{
    output = newOutput;

    // change output summary view
    JsonFormatter jsonFormatter(output);                                        // convert output to JSON
    qJsonModel.loadJson(QByteArray(jsonFormatter.toString().c_str()));         // feed JSON to model of tree view
    expandChildren(qJsonModel.index(0,0,QModelIndex()),ui->outputTreeView,3);  // expand some items in the tree view for better visability
}