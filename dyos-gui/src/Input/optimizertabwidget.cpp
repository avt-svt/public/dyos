#include "optimizertabwidget.h"
#include "ui_optimizertabwidget.h"

OptimizerTabWidget::OptimizerTabWidget(QWidget *parent) :
    GenericTabWidget(parent),
    ui(new Ui::OptimizerTabWidget)
{
    ui->setupUi(this);
    
    QStringList ColumnsTitles;
    //create 6 columns in the table
    ui->constraintTable->setColumnCount(6);
    //set columns titles 
    ColumnsTitles << "Name" << "Time Point" << "Lower Bound" << "Upper Bound" << "Constraint Type" << "Exclude Zero" ;
    ui->constraintTable->setHorizontalHeaderLabels(ColumnsTitles);
    //set the ConstraintType ComboBox list
	ConstraintTypeComboBoxList <<"PATH" << "POINT" << "ENDPOINT";
    //connect table cells signals with the changeInput method to handle changes
    connect(ui->constraintTable, SIGNAL(cellChanged(int, int)), this, SLOT(changeInput(int, int)));
}

OptimizerTabWidget::~OptimizerTabWidget()
{
    delete ui;
}

/**
 * @brief create a comboBox with a unique property
 * 
 * @param row parameter number
 * @return auto* 
 */
auto* OptimizerTabWidget::createTypeComboBox(int row)
{
    // *** create ComboBox-widget/Drop-down for ConstraintType list ***
    QComboBox* ConstraintTypeComboBox = new QComboBox();                                                                // declare and initialize the ComboBox
    ConstraintTypeComboBox->setWhatsThis(tr("This is a dropdown menu which lets you choose the constraint type"));      // set whatisthis text
    ConstraintTypeComboBox->setProperty("row", row);                                                                    // give the ComboBox a unique index
    ConstraintTypeComboBox->addItems(ConstraintTypeComboBoxList);                                                       // add ConstraintType list to the ComboBox
    connect(ConstraintTypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxIndexChanged(int)));            // connect the ComboBox currentIndexChanged signal with the comboBoxIndexChanged method to handle changes
    return ConstraintTypeComboBox;
}
/**
 * @brief create a checkbox widget with a unique property
 * 
 * @param row parameter number
 * @return auto* 
 */
auto* OptimizerTabWidget::createExcludeZeroCheckBox(int row)
{
    
    QCheckBox *checkBox = new QCheckBox();                                                         // declare and initialize the CheckBox
    checkBox->setProperty("row", row);                                                             // give the CheckBox a unique index
    checkBox->setWhatsThis(tr("this is a checkbox that lets you set exclude zero to true/false")); // set whatisthis text
    connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(excludeZeroStateChanged(int)));           // connect the CheckBox stateChanged signal with the excludeZeroStateChanged method to handle changes
    return checkBox;
}

/**
 * @brief updates every UI element that is currently displayed
 * 
 */
void OptimizerTabWidget::updateUI()
{
    //get current stage input
    UserInput::StageInput* CurrentstageInput = &inputWidget->accessStage();
    
    // load current eso model
    try {
        UserInput::EsoInput eso;
		eso.type = CurrentstageInput->eso.type;
		eso.model = CurrentstageInput->eso.model;
        GenericEso::Ptr esoPtr = getEsoPtr(eso,inputWidget->accessInput().integratorInput.order);

        // prefill variables 
        const int diffNum = esoPtr->getNumDifferentialVariables();
        const int algNum = esoPtr->getNumAlgebraicVariables();
        std::vector<EsoIndex> diffIndices(diffNum);
        std::vector<EsoIndex> algIndices(algNum);
        esoPtr->getDifferentialIndex(diffNum, diffIndices.data());
        esoPtr->getAlgebraicIndex(algNum, algIndices.data());
        constraintsNamesList.clear();
        // add differential variables names
        for (int i=0;i<diffNum;i++) {
            constraintsNamesList << QString::fromStdString(esoPtr->getVariableNameOfIndex(diffIndices[i]));
        }
        // add algebraic variables names
        for (int i=0;i<algNum;i++) {
            constraintsNamesList << QString::fromStdString(esoPtr->getVariableNameOfIndex(algIndices[i]));
        }

    }   catch (std::exception &e) {
        QErrorMessage::qtHandler()->showMessage(tr(e.what()));
    }   catch (...) {
        QErrorMessage::qtHandler()->showMessage(tr("An unknown error has occured!"));
    }

    //insert Objective values in the UI
	ui->namelineEdit->setText(QString::fromStdString(CurrentstageInput->optimizer.objective.name));

    //clear table
    initialized = false;
    ui->constraintTable->setRowCount(0);
    ui->constraintTable->setSortingEnabled(false);

    //insert ALL OTHER Constraint in the table
    for (int i=0;i< CurrentstageInput->optimizer.constraints.size();i++) 
    {
        int currentRowNumber;   
        currentRowNumber = ui->constraintTable->rowCount();
        // insert a new row in the constraint table
        ui->constraintTable->insertRow(ui->constraintTable->rowCount());
        
		
        // set NAME column text
		QTableWidgetItem* widget = new QTableWidgetItem(QString::fromStdString(CurrentstageInput->optimizer.constraints.at(i).name));
        widget->setData(Qt::UserRole, i);
        widget->setFlags(widget->flags() & (~Qt::ItemIsEditable));
	    ui->constraintTable->setItem(currentRowNumber, NAME, widget );
        
		
        // set LOWERBOUND value
        ui->constraintTable->setItem(currentRowNumber,LOWERBOUND,new QTableWidgetItem(QString::number(CurrentstageInput->optimizer.constraints.at(i).lowerBound)));
        
		
        // set UPPERBOUND value
        ui->constraintTable->setItem(currentRowNumber,UPPERBOUND,new QTableWidgetItem(QString::number(CurrentstageInput->optimizer.constraints.at(i).upperBound)));

		// set excludeZero checkBox
		QCheckBox *checkBox  = createExcludeZeroCheckBox(currentRowNumber);                          // create the checkBox widget
		QWidget *checkBoxWidget  = new QWidget();                                                    // declare and initialize the CheckBox
        QHBoxLayout *layoutCheckBox = new QHBoxLayout(checkBoxWidget );                              // create a layer with reference to the widget
        layoutCheckBox->addWidget(checkBox);                                                         // set the CheckBox in the layer
        layoutCheckBox->setAlignment(Qt::AlignCenter);                                               // Center the CheckBox
        layoutCheckBox->setContentsMargins(0,0,0,0);
        checkBox->setChecked(CurrentstageInput->optimizer.constraints.at(i).excludeZero);                         // set the CheckBox based on the stage input value(true/flase)
        ui->constraintTable->setCellWidget(currentRowNumber,CONSTRAINTEXCLUDEZERO, checkBoxWidget);                     // insert the CheckBox in the current cell (row,column, QWidget)
        
        // set TYPE comboBox
        QComboBox* ConstraintTypeComboBox = createTypeComboBox(currentRowNumber); 
        ui->constraintTable->setCellWidget(currentRowNumber,CONSTRAINTTYPE, ConstraintTypeComboBox);                    // insert the ComboBox in the current cell (row,column, QWidget)
        ConstraintTypeComboBox->setCurrentIndex(CurrentstageInput->optimizer.constraints.at(i).type);                   // set the ComboBox index based on the stage input type
        
        //create time point widget
        QTableWidgetItem* timePointPWidget = new QTableWidgetItem();
        //switch based on the constraint type
        switch (CurrentstageInput->optimizer.constraints.at(i).type)
        {
        //constraint has a PATH type
        case 0:
            //set time point value only in the ui to nothing 
            timePointPWidget->setText("");
            timePointPWidget->setFlags(timePointPWidget->flags() & (~Qt::ItemIsEnabled));                                //set WidgetItem to not editable 
            
            break;
        //constraint has a POINT type
        case 1:
            // set time point value
            timePointPWidget->setText(QString::number(CurrentstageInput->optimizer.constraints.at(i).timePoint)); //set value equal to the userInput value  
            break;
        //constraint has an ENDPOINT type
        case 2:
            // set time point value only in the ui to 1 and not editable
            timePointPWidget->setText("1");
            timePointPWidget->setFlags(timePointPWidget->flags() & (~Qt::ItemIsEnabled));                                //set WidgetItem to not editable 
        default:
            break;
        }
        ui->constraintTable->setItem(currentRowNumber,TIMEPOINT,timePointPWidget);     
    }
    ui->constraintTable->setSortingEnabled(true);
    initialized = true;
    
    // this function turns on the hover explanations for any widgets which have a "whatsThis" text
    installHoverFilterOnElements();
} 

void OptimizerTabWidget::excludeZeroStateChanged(int arg1)
{   
    // creat a QCheckBox widget to receive sender parameters
    QCheckBox* ExcludezeroCheckBox = qobject_cast<QCheckBox*>(sender());
    // get QCheckBox index
    int rowIndex    = ExcludezeroCheckBox->property("row").toInt();
    //update Constraint excludeZero parameter
    inputWidget->accessStage().optimizer.constraints.at(rowIndex).excludeZero = arg1;
}

/**
 * @brief change the old parameter input value with the new insert one in a specific widget
 * 
 * @param row row number of the widget
 * @param column column number of the widget
 */
void OptimizerTabWidget::changeInput(int row, int column)
{
    //first check if the table is initialized 
    if(initialized)
    {
        QTableWidgetItem* currentSelectedWidgetItem = ui->constraintTable->item(row,NAME);            // get the NAME item
        int constraintIndex = currentSelectedWidgetItem->data(Qt::UserRole).toInt();                  // get constraint index in the userInput
        bool isNumber;      
        double insertedValue = ui->constraintTable->item(row,column)->text().toDouble(&isNumber);     // convert the inserted value from QString to double
        // test if its a number 
        if(isNumber){  
            switch (column)
	        {   // update TIMEPOINT
                case 1: 
	            	inputWidget->accessStage().optimizer.constraints.at(constraintIndex).timePoint= insertedValue;
                    break;  
                // update LOWERBOUND
	            case 2:	
	            	inputWidget->accessStage().optimizer.constraints.at(constraintIndex).lowerBound= insertedValue;
	            	break;   
                // update UPPERBOUND
	            case 3:
	            	inputWidget->accessStage().optimizer.constraints.at(constraintIndex).upperBound= insertedValue;
	            	break;   
	            default:
	            	break;
	        }
        }else{
            ui->constraintTable->item(row,column)->setText("0");
        }
    }

}
void OptimizerTabWidget::comboBoxIndexChanged(int index)
{
    // creat a QComboBox widget to receive sender parameters
    QComboBox* combo = qobject_cast<QComboBox*>(sender());
    if(initialized){
        // get ComboBox index  
        int row = combo->property("row").toInt();
        // change the constraint input type with the new selected type from the ComboBox                                                                                                               
        inputWidget->accessStage().optimizer.constraints.at(row).type = UserInput::ConstraintInput::ConstraintType(index);          
        updateUI();
    }
}

void OptimizerTabWidget::on_deleteConstraintPushButton_clicked()
{   
    //get all setected items
    QModelIndexList select = ui->constraintTable->selectionModel()->selectedIndexes();                 
    vector<int> sortingVector;

    for (int i = 0; i <select.count() ; i++){
        //get selected item row number
	    int rowNumber = select.at(i).row();      
        // get the NAME item                                                      
        QTableWidgetItem* currentSelectedWidgetItem = ui->constraintTable->item(rowNumber,NAME); 
        //get constraint index in the stage input       
        int constraintIndex = currentSelectedWidgetItem->data(Qt::UserRole).toInt();                    
	    //test if the index doesnt exist if yes add it to the vector
        if(std::find(sortingVector.begin(), sortingVector.end(), constraintIndex) == sortingVector.end()){
            //create a vector contains all constraints that we want to delete index
            sortingVector.push_back(constraintIndex);                                                     
        }
	}
    if(sortingVector.size() > 0){
        //sort  all the indexes from bigger to smaller
        sort(sortingVector.begin(), sortingVector.end(), greater<int>());
        //delete all the selected constraints from the userInput and update the ui
        for (int i = 0; i <sortingVector.size() ; i++){
            //delete the constraint from the user input
            inputWidget->accessStage().optimizer.constraints.erase (inputWidget->accessStage().optimizer.constraints.begin() + sortingVector[i]);
        }
        updateUI();
    }
}

void OptimizerTabWidget::on_addConstraintPushButton_clicked()
{
    //open the constraint select names window
    AddElementDialog selectNameWindow(this);
    selectNameWindow.setWindowTitle("Name Picker");
    selectNameWindow.updateSelectNameWindow(constraintsNamesList);
    int action = selectNameWindow.exec();
    
    if(action == QDialog::Rejected)
        return;
    //get selected items
    std::vector<std::string> selectedConstraintName = selectNameWindow.getSelectedNames();
	for (int i = 0; i < selectedConstraintName.size(); i++) {
        //create a new ConstraintInput
        UserInput::ConstraintInput constraint;
		constraint.name = selectedConstraintName[i];
        //insert the constraint in the user input
        inputWidget->accessStage().optimizer.constraints.push_back(constraint);
    }
	updateUI();
}

void OptimizerTabWidget::on_namelineEdit_editingFinished()
{	//test if the inserted name is a constraint name
    for (int i = 0; i < constraintsNamesList.size(); i++)
    {   
        if (constraintsNamesList[i] == ui->namelineEdit->text())
        {
    	    //change the name string in the userInput with the new one
	        inputWidget->accessStage().optimizer.objective.name = ui->namelineEdit->text().toStdString();
            break;
        }
    }
    //reset Qline text to the old one
    ui->namelineEdit->setText(QString::fromStdString(inputWidget->accessStage().optimizer.objective.name));    
}

void OptimizerTabWidget::on_selectObjectiveName_clicked()
{
    //open the Objective select name window
    AddElementDialog selectNameWindow(this);
        selectNameWindow.setWindowTitle("Objective Name Picker");
        selectNameWindow.updateSelectNameWindow(constraintsNamesList);
		selectNameWindow.changeSelectionModeToSingle();
        int action = selectNameWindow.exec();

        if(action == QDialog::Rejected)
            return;
        //get selected name
        std::vector<std::string> selectedConstraintName = selectNameWindow.getSelectedNames();
        if (selectedConstraintName.size() == 0) {
		    return;
	    }
        //update the ui and change the objective name in the user input
        ui->namelineEdit->setText(QString::fromStdString(selectedConstraintName[0]));
		inputWidget->accessStage().optimizer.objective.name = selectedConstraintName[0];
		updateUI();
}
