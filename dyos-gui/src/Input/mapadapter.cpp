#include "mapadapter.h"

/**
 * @brief updates every UI element that is currently displayed
 * Constructs objects from the MapAdapter class
 */
MapAdapter::MapAdapter(QWidget *parent) :
    parent(parent)
{}

/**
 * @brief 	Displays the content of a map inside a QTableWidget
 * 
 * @param map 		The map to display
 * @param table 	The table in which the map is displayed
 * @param enabled	(optional) Setting this false will grey out the table. In any case it is read-only
 */
void MapAdapter::refreshTable(std::map<std::string, std::string> & map, QTableWidget * table, bool enabled)
{
	table->setEditTriggers(enabled ? QAbstractItemView::AllEditTriggers : QAbstractItemView::NoEditTriggers);
	table->setRowCount(0); 
	for (auto& it : map) {
		QString TableKey = QString::fromStdString(it.first);
		QString TableValue = QString::fromStdString(it.second);
		table->insertRow(table->rowCount());
		// set key
		QTableWidgetItem *item = new QTableWidgetItem(TableKey);
		if (enabled) item->setFlags(item->flags() & (~Qt::ItemIsEditable));
		table->setItem(table->rowCount() - 1, 0, item); 
		// set value
		table->setItem(table->rowCount() - 1, 1, new QTableWidgetItem(TableValue));
	}
	table->setEnabled(enabled); 
}

/**
 * @brief 	Adds an option to chosen table
 * 
 * Also checks for duplicate keys and asks the user whether he wants to update the value
 * for the key.
 * 
 */
void MapAdapter::addOption(std::string key, std::string value, std::map<std::string, std::string> & map)
{
    if (map.empty() == true) {
		map.insert({ key, value });
	}
	else {   // Check if key already exists in map
		std::map<std::string, std::string>::const_iterator itr = map.find(key);
		if (itr == map.end()) {
			map.insert({ key, value });
		}
		else {
			// QMessageBox showing if the key does already exist; 
			QMessageBox::StandardButton reply =
				QMessageBox::question(parent, "Error", "The entered Key already exists, do you want to overwrite the old value ?",
					QMessageBox::StandardButtons(QMessageBox::Yes | QMessageBox::No), QMessageBox::Yes);
			if (reply == QMessageBox::Yes) {
				// Value for key is emplaced
				map.erase(key);
				map.insert({ key,value });
			}
			else {
				// New value is deleted
				return;
			}
		}
	} 
}

/**
 * @brief 	Removes key and value from chosen map in "input"
 * 
 */
void MapAdapter::removeOption(std::map<std::string, std::string> & map, QTableWidget * table) {
    int row = table->currentIndex().row(); 
    // Get key-string to find pair in map 
    std::string key = table->item(row,0)->data(Qt::DisplayRole).toString().toStdString(); 
    map.erase(key); 
} 

/**
 * @brief 	Changes value for a row chosen cell in the gui by the user
 * 
 */

/**
 * @brief 	Changes value for a row chosen cell in the gui by the user
 * 
 * @param row 		The row of the 
 * @param column 
 * @param map 
 * @param table 
 */
void MapAdapter::emplaceOption(int row, int column, std::map<std::string, std::string> & map, QTableWidget * table) {
    if (column == 0) return;
    std::string value, key;
    key = table->item(row,0)->data(0).toString().toStdString(); 
    value = table->item(row,1)->data(0).toString().toStdString(); 
    map.erase(key); 
    map.insert({key,value});
}