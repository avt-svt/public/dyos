#include "resolvestagedialog.h"
#include "ui_resolvestagedialog.h"

ResolveStageDialog::ResolveStageDialog(UserInput::StageInput *stageToResolve, QWidget *parent) :
    QDialog(parent),
    stageToResolve(stageToResolve),
    ui(new Ui::ResolveStageDialog)
{
    ui->setupUi(this);
    this->setFixedSize(this->width(),this->height());
    // initialize copy
    stageCopy = *stageToResolve;
    // connect signals and slots for UI elements
    connect(ui->esoModelLineEdit, SIGNAL(returnPressed()), ui->checkModelButton, SLOT(animateClick()));
    // update the UI
    updateUI();
}

ResolveStageDialog::~ResolveStageDialog()
{}

/**
 * @brief a static convenience method which resolves ALL stages of a given input
 * 
 * @param input -> pointer to the input which is to be resolved
 * @param parent -> (optional) parent widget
 * @return true -> all stages were successfully resolved
 * @return false -> at least one stage was not successfully resolved, the input remains unchanged
 */
bool ResolveStageDialog::resolveModelStrings(UserInput::Input *input, QWidget *parent)
{
    UserInput::Input inputToResolve = *input;

    // use a map such that all broken stages which are identical only have to be resolved once
    std::map<std::string, UserInput::StageInput *> uniqueStagesToFix;

    // loop over all stages of the input and check their model
    for (auto& stage : inputToResolve.stages) {
        try {
            getEsoPtr(stage.eso,inputToResolve.integratorInput.order);
            // model for this stage seems valid!
        } catch (...) {
            // save broken model
            uniqueStagesToFix[stage.eso.model] = &stage;
        }
    }

    // resolve all unique broken stages
    for (auto& keyValue : uniqueStagesToFix) {
        ResolveStageDialog resolve(keyValue.second,parent);
        if (!resolve.exec()) {
            // couldn't resolve one of the stages, so quit
            return false; 
        }
    }

    // fix all stages
    for (auto& stage : inputToResolve.stages) {
        UserInput::StageInput *fixedStage = uniqueStagesToFix[stage.eso.model];
        if (fixedStage) {
            stage.eso = fixedStage->eso;
        } // else: stage is already fixed
    }

    // every stage was successfully resolved, therefore perform changes
    *input = inputToResolve;
    return true;
}

/**
 * @brief updates the current stageCopy with the contents of the LineEdit and updates the UI
 * 
 */
void ResolveStageDialog::updateModel()
{
    stageCopy.eso.type  = EsoType(ui->esoTypeComboBox->currentIndex());
    stageCopy.eso.model = ui->esoModelLineEdit->text().toStdString();
    updateUI();
}

/**
 * @brief updates the UI according to the state of the current stageCopy
 * 
 */
void ResolveStageDialog::updateUI()
{
    // setup UI according to chosen eso type
    switch (stageCopy.eso.type) {
    case EsoType::JADE:
        ui->esoChoosePathButton->setEnabled(true);
        ui->esoModelLineEdit->setEnabled(true);
        ui->esoModelLineEdit->setReadOnly(false);
        break;
    case EsoType::FMI:
        ui->esoChoosePathButton->setEnabled(true);
        ui->esoModelLineEdit->setEnabled(true);
        ui->esoModelLineEdit->setReadOnly(true);
        break;
    default:
        ui->esoChoosePathButton->setEnabled(false);
        ui->esoModelLineEdit->setEnabled(false);
        break;
    }

    // display stageCopy content
    ui->esoTypeComboBox->setCurrentIndex(to_int(stageCopy.eso.type));
    ui->esoModelLineEdit->setText(QString(stageCopy.eso.model.c_str()));

    try {
        getEsoPtr(stageCopy.eso);
        // apparently the string is valid
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
        ui->result->setText(tr("The current model is valid!\n\nThe GUI is not able to check whether it is compatible with the other settings in the input file ... take care."));
    } catch (...) {
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        ui->result->setText(tr("The current model is still invalid!\n\nPressing \"Cancel\" stops opening the input file."));
    }
}

/**
 * @brief this method is run once the user is able to press "OK" in the dialog
 * 
 */
void ResolveStageDialog::on_ResolveStageDialog_accepted()
{
    // changes should take effect in the actual stage
    *stageToResolve = stageCopy;
}

void ResolveStageDialog::on_esoTypeComboBox_activated(int i)
{
    updateModel();
}

void ResolveStageDialog::on_esoChoosePathButton_clicked()
{
    QString path;
    EsoType type = EsoType(ui->esoTypeComboBox->currentIndex());
    bool ok = StageTabWidget::chooseModelPath(type,&path);
    if (ok) ui->esoModelLineEdit->setText(path);
    updateModel();
}

void ResolveStageDialog::on_checkModelButton_clicked()
{
    updateModel();
}
