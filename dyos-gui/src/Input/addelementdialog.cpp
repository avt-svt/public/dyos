#include "addelementdialog.h"
#include "ui_addelementdialog.h"

AddElementDialog::AddElementDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddElementDialog)
{
    ui->setupUi(this);
}

AddElementDialog::~AddElementDialog()
{
    delete ui;
}
/**
 * @brief change the selection mode to one element only(default multiple selection)
 * 
 */
void AddElementDialog::changeSelectionModeToSingle(){
    //change widget selection mode 
    ui->namesListWidget->setSelectionMode(QAbstractItemView::SingleSelection);
}
/**
 * @brief update the select name editor based on the parameter names list
 * 
 * @param List 
 */
void AddElementDialog::updateSelectNameWindow(QStringList List)
{
    //update namesList
	namesList = List ;
    //insert all names in the widget
    ui->namesListWidget->addItems(namesList);

}

void AddElementDialog::on_filterQline_textChanged(const QString &arg1)
{   
    //filter all names list and update the ui with the new filtred name list
    QRegExp regExp(arg1, Qt::CaseInsensitive,QRegExp::Wildcard);
    ui->namesListWidget->clear();
    ui->namesListWidget->addItems(namesList.filter(regExp));

}
/**
 * @brief return the selected names
 * 
 * @return std::vector<std::string> 
 */
std::vector<std::string> AddElementDialog::getSelectedNames()
{	
    //get all selected items
    QModelIndexList select = ui->namesListWidget->selectionModel()->selectedRows();
	std::vector<std::string> selectedNamesVector;

    for (int i = 0; i <select.count() ; i++){
        //get item row number
        int rowNumber = select.at(i).row();
        //get the item name and inserted in the   selectedNamesVector                                                          
        QListWidgetItem* currentSelectedItem = ui->namesListWidget->item(rowNumber);             
        selectedNamesVector.push_back(currentSelectedItem->text().toStdString());
    }
	return selectedNamesVector;

}

void AddElementDialog::on_namesListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    accept();
}

