#include "inputexamples.h"

/**
 * @brief The standard car example with 3 stages
 * 
 * This example was given to the GUI development group as a benchmark case,
 * and only small adjustments have been made.
 * 
 * @return UserInput::Input 
 */
UserInput::Input InputExamples::example1() {
    using namespace UserInput;
    
    Input input;
    StageInput stageInput;

    // - Global Tab
    input.runningMode = Input::SINGLE_SHOOTING;
    input.integratorInput.type = IntegratorInput::LIMEX;
    input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
    input.optimizerInput.type = OptimizerInput::SNOPT;
    std::map<std::string, std::string> optimizerOptions;
    optimizerOptions.insert(std::make_pair<std::string, std::string>("Major Iterations Limit", "200"));
    input.optimizerInput.optimizerOptions = optimizerOptions;

    // Stage 1

    // - Stage Tab
    {
        stageInput.eso.type = EsoType::JADE;
        stageInput.eso.model = "carFreeFinalTimeTwoObjectives";
        stageInput.treatObjective = false; 
        stageInput.mapping.fullStateMapping = true;
    }

    // - Integrator Tab
    {
        stageInput.integrator.duration.lowerBound = 20.0;
        stageInput.integrator.duration.upperBound = 20.0;
        stageInput.integrator.duration.value = 20.0;
        stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;
        stageInput.integrator.duration.paramType = UserInput::ParameterInput::DURATION;

        stageInput.integrator.parameters.resize(1);
        stageInput.integrator.parameters[0].name = "accel";
        stageInput.integrator.parameters[0].lowerBound = -2.0;
        stageInput.integrator.parameters[0].upperBound = 2.0;
        stageInput.integrator.parameters[0].paramType = ParameterInput::PROFILE;
        stageInput.integrator.parameters[0].sensType = UserInput::ParameterInput::FULL;

        const unsigned int numIntervals = 5;
        stageInput.integrator.parameters[0].grids.resize(1);
        stageInput.integrator.parameters[0].grids[0].numIntervals = numIntervals;
        stageInput.integrator.parameters[0].grids[0].values.resize(numIntervals+1, 2.0);
        stageInput.integrator.parameters[0].grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
    }

    // - Optimizer Tab
    {
        stageInput.optimizer.objective.name = "fuel";
        stageInput.optimizer.objective.lowerBound = 0.0;
        stageInput.optimizer.objective.upperBound = 500.0;

        stageInput.optimizer.constraints.resize(1);
        stageInput.optimizer.constraints[0].name = "velo";
        stageInput.optimizer.constraints[0].type = UserInput::ConstraintInput::PATH;
        stageInput.optimizer.constraints[0].lowerBound = 0.0;
        stageInput.optimizer.constraints[0].upperBound = 10.0;
    }
    input.stages.push_back(stageInput);

    // Stage 2 (based on Stage 1)

    // - Integrator Tab
    {
        stageInput.integrator.duration.lowerBound = 5.0;
        stageInput.integrator.duration.upperBound = 5.0;
        stageInput.integrator.duration.value = 5.0;
        stageInput.integrator.duration.sensType = UserInput::ParameterInput::NO;

        UserInput::ParameterInput initialValueParameter;
        initialValueParameter.name = "velo";
        initialValueParameter.sensType = UserInput::ParameterInput::FRACTIONAL;
        initialValueParameter.paramType = UserInput::ParameterInput::INITIAL;
        initialValueParameter.value = 5.0;
        stageInput.integrator.parameters.push_back(initialValueParameter);
    }
    input.stages.push_back(stageInput);

    // Stage 3 (based on Stage 2)

    // - Stage Tab
    {
        stageInput.mapping.fullStateMapping = false;
        stageInput.treatObjective = true;
    }

    // - Integrator Tab
    {
        stageInput.integrator.duration.lowerBound = 0.1;
        stageInput.integrator.duration.upperBound = 100.0;
        stageInput.integrator.duration.value = 15.0;
        stageInput.integrator.duration.sensType = UserInput::ParameterInput::FULL;

        stageInput.integrator.parameters.back().name = "ttime";
        stageInput.integrator.parameters.back().value = 0.0;
    }

    // - Optimizer Tab
    {
        UserInput::ConstraintInput constraint;
        constraint.name = "velo";
        constraint.type = UserInput::ConstraintInput::ENDPOINT;
        constraint.lowerBound = 0.0;
        constraint.upperBound = 0.0;
        stageInput.optimizer.constraints.push_back(constraint);

        constraint.name = "dist";
        constraint.lowerBound = 300.0;
        constraint.upperBound = 300.0;
        stageInput.optimizer.constraints.push_back(constraint);

        stageInput.optimizer.objective.name = "ttime";
        stageInput.optimizer.objective.lowerBound = 0.0;
        stageInput.optimizer.objective.upperBound = 100.0;
    }
    input.stages.push_back(stageInput);

    return input;
}

/**
 * @brief a wobatch example with an adjustable number of stages
 * 
 * @return UserInput::Input 
 */
UserInput::Input InputExamples::example2() {
    using namespace UserInput;
    
    Input input;
    StageInput stageInput;

    // - Global Tab
    input.runningMode = Input::SINGLE_SHOOTING; 
    input.integratorInput.type = IntegratorInput::NIXE;
    input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
    input.integratorInput.integratorOptions["absolute tolerance"] = "1.e-4";
    input.integratorInput.integratorOptions["relative tolerance"] = "1.e-4";    
    input.optimizerInput.type = OptimizerInput::IPOPT;
    input.optimizerInput.optimizerOptions["major optimality tolerance"] = "1.e-4";

    // Get the desired number of stages from the user
    int nStages;
    bool ok;
    nStages = QInputDialog::getInt(nullptr,"Enter Number of Stages","Please enter a number of stages for this example",1,1,20,1,&ok);
    if (!ok) {
        Input input;
        input.stages.resize(1);
        return input;
    }

    // Stages

    for (int i=0;i<nStages;i++) {
        // - Stage Tab
        stageInput.treatObjective = false; 
        stageInput.mapping.fullStateMapping = true; 

        // - Stage Tab
        {
            EsoInput esoInput;
            esoInput.type = EsoType::FMI;
            esoInput.model = PATH_FMI_LARGE_SYSTEM_TEST_FMI;
            stageInput.eso = esoInput;
        }

        // - Integrator Tab
        {
            IntegratorStageInput integratorStageInput;

            // Duration
            {
                ParameterInput stageDuration;
                stageDuration.lowerBound = 1000.0;
                stageDuration.upperBound = 1000.0;
                stageDuration.value = 1000.0 / nStages; 
                stageDuration.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
                stageDuration.paramType = UserInput::ParameterInput::DURATION;
                integratorStageInput.duration = stageDuration;
            }

            // Parameters 
            {
                std::vector<ParameterInput> parameters(2);
                parameters[0].name = "FbinCur";
                parameters[0].lowerBound = 0.0;
                parameters[0].upperBound = 5.784;
                parameters[0].paramType = ParameterInput::PROFILE;
                parameters[0].sensType = UserInput::ParameterInput::FULL;

                parameters[1].name = "TwCur";
                parameters[1].lowerBound = 0.020;
                parameters[1].upperBound = 0.1;
                parameters[1].paramType = ParameterInput::PROFILE;
                parameters[1].sensType = UserInput::ParameterInput::FULL;
                
                // Grids
                {
                    std::vector<ParameterGridInput> grids(1);
                    const int numIntervals = 8;
                    grids[0].numIntervals = numIntervals;
                    grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
                    grids[0].values.resize(numIntervals + 1, 5.784);
                    grids[0].adapt.maxAdaptSteps = 4;
                    grids[0].adapt.adaptWave.minRefinementLevel = 1;
                    grids[0].adapt.adaptWave.horRefinementDepth = 0;
                    grids[0].pcresolution = 2;
                    parameters[0].grids = grids;

                    grids[0].numIntervals = numIntervals;
                    grids[0].type = ParameterGridInput::PIECEWISE_LINEAR;
                    grids[0].values.clear();
                    grids[0].values.resize(grids[0].numIntervals + 1, 100e-3);
                    grids[0].adapt.maxAdaptSteps = 4;
                    parameters[1].grids = grids;
                }
                integratorStageInput.parameters = parameters;
            }
            stageInput.integrator = integratorStageInput;
        }

        // - Optimizer Tab
        {
            OptimizerStageInput optimizerStageInput;

            // Objective
            {
                ConstraintInput objective;
                objective.name = "X9";
                optimizerStageInput.objective = objective;
            }

            // Constraints
            {
                std::vector<ConstraintInput> constraints(2);
                constraints[0].name = "X7";
                constraints[0].lowerBound = 60;
                constraints[0].upperBound = 90;
                constraints[0].type = ConstraintInput::PATH;

                constraints[1].name = "X8";
                constraints[1].lowerBound = 0.0;
                constraints[1].upperBound = 5.0;
                constraints[1].type = ConstraintInput::ENDPOINT;
                optimizerStageInput.constraints = constraints;
            }
            stageInput.optimizer = optimizerStageInput;
        }
        input.stages.push_back(stageInput);
    }

    // Set special settings for last stage
    input.stages.back().treatObjective = true;
    input.stages.back().mapping.fullStateMapping = false;

    return input;
}

/**
 * @brief A WOBatch example with an adjustable number of stages
 * 
 * @return UserInput::Input 
 */
UserInput::Input InputExamples::example3() {
    using namespace UserInput;
    
    Input input;
    StageInput stageInput;

    // - Global Tab
    input.finalIntegration = true;
    input.runningMode = Input::SINGLE_SHOOTING; 
    input.integratorInput.type = IntegratorInput::LIMEX;
    input.integratorInput.order = IntegratorInput::FIRST_FORWARD;
    input.optimizerInput.type = OptimizerInput::SNOPT;
    input.optimizerInput.optimizationMode = OptimizerInput::MINIMIZE;
    input.optimizerInput.optimizerOptions["major iterations limit"] = "200";

    // Stages

    // General information which is constant over all stages:
    {
        EsoInput eso;
        eso.model = "carFreeFinalTimeTwoObjectives";
        eso.type = EsoType::JADE;
        stageInput.eso = eso;

        stageInput.treatObjective = true;
        stageInput.mapping.fullStateMapping = true;
    }

    {
        stageInput.integrator.plotGridResolution = 1;
        ParameterInput duration;
        duration.value = 5;
        duration.lowerBound = 0;
        duration.paramType = ParameterInput::DURATION;
        duration.sensType = ParameterInput::FULL;
        stageInput.integrator.duration = duration;
        
        ParameterInput parameter;
        parameter.name = "accel";
        parameter.lowerBound = -10;
        parameter.upperBound = 7;
        parameter.paramType = ParameterInput::PROFILE;
        parameter.sensType = ParameterInput::FULL;

        ParameterGridInput grid;
        grid.type = ParameterGridInput::PIECEWISE_LINEAR;
        grid.pcresolution = 1;
        parameter.grids.push_back(grid);

        stageInput.integrator.parameters.push_back(parameter);
    }

    {
        stageInput.optimizer.objective.name = "ttime";

        std::vector<ConstraintInput> constraints(2);
        constraints[0].name = "velo";
        constraints[0].type = ConstraintInput::PATH;
        constraints[0].lowerBound = 0;
        constraints[1].name = "dist";
        constraints[1].type = ConstraintInput::ENDPOINT;

        stageInput.optimizer.constraints = constraints;
    } 

    // Stage 1: Driving through Aachen
    {
        const int n = 5;
        const double speed = 13.8;
        const double distance = 3600;

        stageInput.integrator.parameters[0].grids[0].numIntervals = n;
        stageInput.integrator.parameters[0].grids[0].values.resize(n+1, 2);

        stageInput.optimizer.constraints[0].upperBound = speed;
        stageInput.optimizer.constraints[1].lowerBound = distance;
        stageInput.optimizer.constraints[1].upperBound = distance;

        input.stages.push_back(stageInput);
    }

    // Stage 2: Driving on A4 towards construction work
    {
        const int n = 20;
        const double speed = 41.7;
        const double distance = 9400;

        stageInput.integrator.parameters[0].grids[0].numIntervals = n;
        stageInput.integrator.parameters[0].grids[0].values.resize(n+1, 2);

        stageInput.optimizer.constraints[0].upperBound = speed;
        stageInput.optimizer.constraints[1].lowerBound = distance;
        stageInput.optimizer.constraints[1].upperBound = distance;

        input.stages.push_back(stageInput);
    }

    // Stage 3: Construction work
    {
        const int n = 20;
        const double speed = 22.2;
        const double distance = 10400;

        stageInput.integrator.parameters[0].grids[0].numIntervals = n;
        stageInput.integrator.parameters[0].grids[0].values.resize(n+1, 2);

        stageInput.optimizer.constraints[0].upperBound = speed;
        stageInput.optimizer.constraints[1].lowerBound = distance;
        stageInput.optimizer.constraints[1].upperBound = distance;

        input.stages.push_back(stageInput);
    }

    // Stage 4: Rest of Autobahn
    {
        const int n = 20;
        const double speed = 41.7;
        const double distance = 16200;

        stageInput.integrator.parameters[0].grids[0].numIntervals = n;
        stageInput.integrator.parameters[0].grids[0].values.resize(n+1, 2);

        stageInput.optimizer.constraints[0].upperBound = speed;
        stageInput.optimizer.constraints[1].lowerBound = distance;
        stageInput.optimizer.constraints[1].upperBound = distance;

        input.stages.push_back(stageInput);
    }

    // Stage 5: In Eschweiler
    {
        const int n = 5;
        const double speed = 13.8;
        const double distance = 17900;

        stageInput.integrator.parameters[0].grids[0].numIntervals = n;
        stageInput.integrator.parameters[0].grids[0].values.resize(n+1, 2);

        stageInput.optimizer.constraints[0].upperBound = speed;
        stageInput.optimizer.constraints[1].lowerBound = distance;
        stageInput.optimizer.constraints[1].upperBound = distance;

        // car has to stop
        ConstraintInput stopConstraint;
        stopConstraint.name = "velo";
        stopConstraint.type = ConstraintInput::ENDPOINT;
        stopConstraint.lowerBound = 0;
        stopConstraint.upperBound = 0;
        stageInput.optimizer.constraints.push_back(stopConstraint);

        input.stages.push_back(stageInput);
    }

    input.stages.back().mapping.fullStateMapping = false;

    return input;
}
