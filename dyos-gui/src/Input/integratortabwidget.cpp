#include "integratortabwidget.h"
#include "ui_integratortabwidget.h"


IntegratorTabWidget::IntegratorTabWidget(QWidget *parent) :
    GenericTabWidget(parent),
    ui(new Ui::IntegratorTabWidget)
{
    ui->setupUi(this);
    QStringList titles;
    ui->parameterTable->setColumnCount(8);
    titles <<"Name"<< "Type" <<  "Value"<< "Lower Bound" << "Upper Bound" << "Parameter Type" << "Sensitivity" << "Grid";
    ui-> parameterTable->setHorizontalHeaderLabels(titles);
    ParameterTypeList<<"INITIAL" << "TIME_INVARIANT" << "PROFILE" << "DURATION";
    ParameterSensitivityTypeList<<"FULL"<<"FRACTIONAL"<<"NO";
    
    
    connect(ui->parameterTable, SIGNAL(cellChanged(int, int)), this, SLOT(changeInput(int, int)));
   
}

IntegratorTabWidget::~IntegratorTabWidget()
{
    delete ui;
}

/**
 * @brief create a new table widget
 * 
 * @param text 
 * @param index 
 * @param column 
 * @param editable 
 * @return auto* 
 */
auto *IntegratorTabWidget::createTableWidgetItem(QString text, int index, int column, bool editable)
{
    QTableWidgetItem *WidgetItem = new QTableWidgetItem(text); //create a WidgetItem
    WidgetItem->setData(Qt::UserRole, index);
    if (!editable)
    {
        switch (column)
        {
        case 0:
            WidgetItem->setFlags(WidgetItem->flags() & (~Qt::ItemIsEditable)); //set WidgetItem to not editable
            break;
        default:
            WidgetItem->setFlags(WidgetItem->flags() & (~Qt::ItemIsEnabled)); //set WidgetItem not editable and grayed out
            break;
        }
    }
    return WidgetItem;
}

/**
 * @brief create a comboBox widget
 * 
 * @param row 
 * @param column 
 * @param ParameterTypeList 
 * @param CurrentIndex 
 * @return auto* 
 */
auto *IntegratorTabWidget::createTypeComboBox(int row, int column, QStringList ParameterTypeList, int CurrentIndex)
{

    QComboBox *typeComboBox = new QComboBox();
    typeComboBox->setProperty("row", row);
    typeComboBox->setProperty("column", column);
    typeComboBox->addItems(ParameterTypeList);
    typeComboBox->setCurrentIndex(CurrentIndex);
    connect(typeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxIndexChanged(int)));
    return typeComboBox;
}

/**
 * @brief updates every UI element that is currently displayed
 * 
 */
void IntegratorTabWidget::updateUI()
{
    // get current stage
	UserInput::StageInput* stageInput = &inputWidget->accessStage();

    // update plot grid resolution spin box
    ui->plotGridResolutionSpinBox->setValue(stageInput->integrator.plotGridResolution);

    //clear table
    identificationMap.clear();
    existingParameters.clear();
    existingVariables.clear();
    initialized = false;
    ui->parameterTable->setRowCount(0);
    ui->parameterTable->setSortingEnabled(false);
    //get all parameters/Variables
    try
    {
        // get currently selected type and model
        UserInput::EsoInput eso;
		eso.type = stageInput->eso.type;
		eso.model = stageInput->eso.model;
        GenericEso::Ptr esoPtr = getEsoPtr(eso,inputWidget->accessInput().integratorInput.order);

        // at this point we can be confident that the provided model is valid
        // prefill parameters and variables
        const int num = esoPtr->getNumVariables();
        const int statesNum = esoPtr->getNumStates();
        const int diffNum = esoPtr->getNumDifferentialVariables();
        const int algNum = esoPtr->getNumAlgebraicVariables();
        const int paraNum = esoPtr->getNumParameters();

        std::vector<EsoIndex> varIndices(num);
        std::vector<EsoIndex> diffIndices(diffNum);
        std::vector<EsoIndex> algIndices(algNum);
        std::vector<EsoIndex> paraIndices(paraNum);

        std::vector<std::string> variableNames;
        esoPtr->getVariableNames(variableNames);
        esoPtr->getDifferentialIndex(diffNum, diffIndices.data());
        esoPtr->getAlgebraicIndex(algNum, algIndices.data());
        esoPtr->getParameterIndex(paraNum, paraIndices.data());

        std::vector<EsoDouble> statesInitials(statesNum);
        esoPtr->getInitialStateValues(statesNum, statesInitials.data());

        std::vector<EsoDouble> paraValues(paraNum);
        esoPtr->getParameterValues(paraNum, paraValues.data());
        // add differential variables to the identificationMap
        variablesNamesList.clear();
        for (int i = 0; i < diffNum; i++)
        {
            identificationMap.insert({esoPtr->getVariableNameOfIndex(diffIndices[i]), "differentialVariable"});
            UserInput::ParameterInput parameter;
            const int stateIndex = esoPtr->getStateIndexOfVariable(diffIndices[i]);
            parameter.name = esoPtr->getVariableNameOfIndex(diffIndices[i]);
            parameter.value = statesInitials[stateIndex];
            parameter.paramType = UserInput::ParameterInput::ParameterType::INITIAL;
            parameter.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
            variablesMap.insert({esoPtr->getVariableNameOfIndex(diffIndices[i]), parameter});
            variablesNamesList << QString::fromStdString(esoPtr->getVariableNameOfIndex(diffIndices[i]));
        }
        // add algebraic variables to the identificationMap
        for (int i = 0; i < algNum; i++)
        {
            identificationMap.insert({esoPtr->getVariableNameOfIndex(algIndices[i]), "algebraicVariable"});
            UserInput::ParameterInput parameter;
            const int stateIndex = esoPtr->getStateIndexOfVariable(algIndices[i]);
            parameter.name = esoPtr->getVariableNameOfIndex(algIndices[i]);
            parameter.value = statesInitials[stateIndex];
            parameter.paramType = UserInput::ParameterInput::ParameterType::INITIAL;
            parameter.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
            variablesMap.insert({esoPtr->getVariableNameOfIndex(algIndices[i]), parameter});
        }
        // add parameters to the identificationMap
        parametersNamesList.clear();
        for (int i = 0; i < paraNum; i++)
        {
            identificationMap.insert({esoPtr->getVariableNameOfIndex(paraIndices[i]), "parameter"});
            parametersNamesList << QString::fromStdString(esoPtr->getVariableNameOfIndex(paraIndices[i]));
        }
    }
    catch (std::exception &e)
    {
        QErrorMessage::qtHandler()->showMessage(tr(e.what()));
    }
    catch (...)
    {
        QErrorMessage::qtHandler()->showMessage(tr("An unknown error has occured!"));
    }
    //create the row of DURATION in the table
    identificationMap.insert({"Duration", "duration"});
    int rowsNumber;
    ui->parameterTable->insertRow(ui->parameterTable->rowCount());
    rowsNumber = ui->parameterTable->rowCount() - 1;
    //create a durationNameWidgetItem
    QTableWidgetItem *durationNameWidgetItem = new QTableWidgetItem("Duration"); 
    //set durationNameWidgetItem to read-only              
    durationNameWidgetItem->setFlags(durationNameWidgetItem->flags() & (~Qt::ItemIsEditable)); 
    //insert duration input type widget
    ui->parameterTable->setItem(rowsNumber, PARAMETERINPUTTYPE, createTableWidgetItem(QString::fromStdString("Duration"), rowsNumber, PARAMETERINPUTTYPE, false));
    //insert duration name/lower bound/upper bound/value columns
    ui->parameterTable->setItem(rowsNumber, NAME, durationNameWidgetItem);
    ui->parameterTable->setItem(rowsNumber, LOWERBOUND, new QTableWidgetItem(QString::number(stageInput->integrator.duration.lowerBound)));
    ui->parameterTable->setItem(rowsNumber, UPPERBOUND, new QTableWidgetItem(QString::number(stageInput->integrator.duration.upperBound)));
    ui->parameterTable->setItem(rowsNumber, VALUE, new QTableWidgetItem(QString::number(stageInput->integrator.duration.value)));
    //create duration parametertype and insert it in the current cell
    ui->parameterTable->setItem(rowsNumber, PARAMETERTYPE, createTableWidgetItem("DURATION", rowsNumber, PARAMETERTYPE, false));
    //create duration sensitivity type comboBox
    QComboBox *durationParametersensitivityTypeComboBox = createTypeComboBox(rowsNumber, PARAMETERSENSITIVITYTYPE, ParameterSensitivityTypeList, stageInput->integrator.duration.sensType);
    durationParametersensitivityTypeComboBox->setWhatsThis(tr("This is a dropdown menu which lets you choose the sensitivity type of the duration."));
    ui->parameterTable->setCellWidget(rowsNumber, PARAMETERSENSITIVITYTYPE, durationParametersensitivityTypeComboBox);
    //create empty grid widget for duration
    ui->parameterTable->setItem(rowsNumber, GRID, createTableWidgetItem("", rowsNumber, GRID, false));
    //insert ALL OTHER PARAMETERS in the table
    for (int i = 0; i < stageInput->integrator.parameters.size(); i++)
    {
        UserInput::ParameterInput *currentParameter = &stageInput->integrator.parameters.at(i);
        std::string identificator = identificationMap[currentParameter->name];
        int insertionLocation = ui->parameterTable->rowCount();
        if (identificator == "differentialVariable")
        {   
            //add the parameter name to the existing parameters list
            existingVariables << QString::fromStdString(currentParameter->name);
            //insert the name/lower bound/upper bound/value widgets
            insertNewParameterRow(i, insertionLocation, true);
            //insert parameter input type widget
            ui->parameterTable->setItem(insertionLocation, PARAMETERINPUTTYPE, createTableWidgetItem(QString::fromStdString("Differential"), PARAMETERINPUTTYPE, insertionLocation, false));
            //insert the parameter type 
            ui->parameterTable->setItem(insertionLocation, PARAMETERTYPE, createTableWidgetItem("INITIAL", insertionLocation, PARAMETERTYPE, false));
            //create COMBOBOX for parametersensitivitytype and insert it in the current cell
            QComboBox *sensitivityTypeCombobox = createTypeComboBox(insertionLocation, PARAMETERSENSITIVITYTYPE, ParameterSensitivityTypeList, currentParameter->sensType);
            //set whatisthis text
            sensitivityTypeCombobox->setWhatsThis(tr("This is a dropdown menu which lets you choose the sensitivity type of the current differential variable."));
            ui->parameterTable->setCellWidget(insertionLocation, PARAMETERSENSITIVITYTYPE, sensitivityTypeCombobox);
            //insert the parameter grid
            ui->parameterTable->setItem(insertionLocation, GRID, createTableWidgetItem("", insertionLocation, GRID, false));
        }
        else if (identificator == "parameter")
        {   
            //add the parameter name to the existing parameters list
            existingParameters << QString::fromStdString(currentParameter->name);
            //insert the name/lower bound/upper bound/value widgets
            insertNewParameterRow(i, insertionLocation, true);
            //insert parameter input type widget
            ui->parameterTable->setItem(insertionLocation, PARAMETERINPUTTYPE, createTableWidgetItem(QString::fromStdString("Parameter"), insertionLocation, PARAMETERINPUTTYPE, false));
            //create COMBOBOX for parametertype and insert it in the current cell
            QComboBox *typeComboBox = createTypeComboBox(insertionLocation, PARAMETERTYPE, ParameterTypeList, currentParameter->paramType);
            typeComboBox->setWhatsThis(tr("This is a dropdown menu which lets you choose the type of the current parameter."));
            QStandardItemModel *model = qobject_cast<QStandardItemModel *>(typeComboBox->model());
            //disable elements number 0 and 3 from the dropdown list
            model->item(0)->setEnabled(false);
            model->item(3)->setEnabled(false);
            //insert the parameter type 
            ui->parameterTable->setCellWidget(insertionLocation, PARAMETERTYPE, typeComboBox);
            //create COMBOBOX for parametersensitivitytype and insert it in the current cell
            QComboBox *sensitivityTypeCombobox = createTypeComboBox(insertionLocation, PARAMETERSENSITIVITYTYPE, ParameterSensitivityTypeList, currentParameter->sensType);
            //set whatisthis text
            sensitivityTypeCombobox->setWhatsThis(tr("This is a dropdown menu which lets you choose the sensitivity type of the current parameter."));
            ui->parameterTable->setCellWidget(insertionLocation, PARAMETERSENSITIVITYTYPE, sensitivityTypeCombobox);
            //create the grid pushbutton
            bool hasGrid = currentParameter->grids.size() > 0;
            if (currentParameter->paramType == 2)
            {
                createGridPushButton(insertionLocation, GRID, hasGrid, true);
                //insert a new empty and not editable value cell
                ui->parameterTable->setItem(insertionLocation, VALUE, createTableWidgetItem("", insertionLocation, VALUE, false));
            }
            else
            {
                // insert a not editabel grid widget
                createGridPushButton(insertionLocation, GRID, hasGrid, false);
            }
        }
        else
        {   
            //insert the name/lower bound/upper bound/value widgets
            insertNewParameterRow(i, insertionLocation, false);
            if (identificator == "algebraicVariable")
            {
                ui->parameterTable->setItem(insertionLocation, PARAMETERINPUTTYPE, createTableWidgetItem(QString::fromStdString("Algebraic"), PARAMETERINPUTTYPE, insertionLocation, false));
            }
            //the parameter doesnt exist in the eso default parameters
            else
            {
                ui->parameterTable->setItem(insertionLocation, PARAMETERINPUTTYPE, createTableWidgetItem(QString::fromStdString("Unknown"), PARAMETERINPUTTYPE, insertionLocation, false));
            }
            //insert the parameter type
            ui->parameterTable->setItem(insertionLocation, PARAMETERTYPE, createTableWidgetItem(ParameterTypeList[currentParameter->paramType], insertionLocation, PARAMETERTYPE, false));
            //insert the parameter sensitivity type
            ui->parameterTable->setItem(insertionLocation, PARAMETERSENSITIVITYTYPE, createTableWidgetItem(ParameterSensitivityTypeList[currentParameter->sensType], insertionLocation, PARAMETERSENSITIVITYTYPE, false));
            //insert the parameter grid
            ui->parameterTable->setItem(insertionLocation, GRID, createTableWidgetItem("", insertionLocation, GRID, false));
        }
    }

    ui->parameterTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->parameterTable->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
    ui->parameterTable->horizontalHeader()->setSectionResizeMode(7, QHeaderView::ResizeToContents);
    ui->parameterTable->setSortingEnabled(true);
    initialized = true;
    // this function turns on the hover explanations for any widgets which have a "whatsThis" text
    installHoverFilterOnElements();
}

/**
 * @brief create and insert new parameter name,lower bound , upper bound and value cells
 * 
 * @param index parameter number
 * @param insertionLocation the row number
 * @param editable can it be edited by the user
 */
void IntegratorTabWidget::insertNewParameterRow(int index, int insertionLocation, bool editable)
{
	UserInput::ParameterInput* currentParameter = &inputWidget->accessStage().integrator.parameters.at(index);
    ui->parameterTable->insertRow(ui->parameterTable->rowCount());
    //insert the parameter name
    ui->parameterTable->setItem(insertionLocation, NAME, createTableWidgetItem(QString::fromStdString(currentParameter->name), insertionLocation, NAME, false));
    //insert the parameter lower bound
    ui->parameterTable->setItem(insertionLocation, LOWERBOUND, createTableWidgetItem(QString::number(currentParameter->lowerBound), insertionLocation, LOWERBOUND, editable));
    //insert the parameter upper bound
    ui->parameterTable->setItem(insertionLocation, UPPERBOUND, createTableWidgetItem(QString::number(currentParameter->upperBound), insertionLocation, UPPERBOUND, editable));
    //insert the parameter value
    ui->parameterTable->setItem(insertionLocation, VALUE, createTableWidgetItem(QString::number(currentParameter->value), insertionLocation, VALUE, editable));
}

/**
 * @brief create the grid Pushbuttom widget
 * 
 * @param row 
 * @param column 
 * @param hasGrid 
 * @param editable 
 */
void IntegratorTabWidget::createGridPushButton(int row, int column, bool hasGrid, bool editable)
{
    QPushButton *gridPushButton = new QPushButton();
    if (hasGrid)
    {
        gridPushButton->setText("Edit Grid");
        gridPushButton->setWhatsThis(tr("This is a button that lets you edit the grid of the current parameter."));
    }
    else
    {
        gridPushButton->setText("Add Grid");
        gridPushButton->setWhatsThis(tr("This is a button that lets you add a new grid to the current parameter."));
    }
    //add the parameter number to the property of the new widget
    gridPushButton->setProperty("row", row);
    //set it as editable or not
    gridPushButton->setEnabled(editable);
    //insert widget in a cell
    ui->parameterTable->setCellWidget(row, column, gridPushButton);
    //connect the signel of the new widget with a function
    connect(gridPushButton, SIGNAL(clicked(bool)), this, SLOT(gridPushButtonClicked(bool)));
}

void IntegratorTabWidget::gridPushButtonClicked(bool checked)
{
    int action, numIntervals, gridType, pcResolution,adaptationType;
    std::vector<double> valuesVector, timePointsVector;
	QStringList waveletAdaptionInfo, swAdaptionInfo;
    //get widget properties
    QPushButton *pushButton = qobject_cast<QPushButton *>(sender());
    int row = pushButton->property("row").toInt();
    int parameterNumber = row - 1;
    
    //open the grid editor window
    ParameterGridDialog gridWindow(this);
    QString parameterName = ui->parameterTable->item(row, NAME)->text();
    QString gridWindowTitle = parameterName + " grid editor";
    gridWindow.setWindowTitle(gridWindowTitle);
    
	//test if a grid exist for the current parameter
	int parameterGridSize = inputWidget->accessStage().integrator.parameters.at(row - 1).grids.size();
	if (parameterGridSize != 0)
	{
        //get the grid values from the parameter input
        UserInput::ParameterGridInput currentGrid = inputWidget->accessStage().integrator.parameters.at(parameterNumber).grids.at(0) ;
		//update the grid editor window with the colected values
        gridWindow.updateGridEditor(currentGrid); //insert grid Values in the Values Qlistwidget
    }

    action = gridWindow.exec();

    if (action == QDialog::Rejected)
        return;
    //get all the grid editor window values
    numIntervals = gridWindow.numIntervals();
    timePointsVector = gridWindow.getInputVector(0);
    valuesVector = gridWindow.getInputVector(1);
    gridType = gridWindow.gridType();
    pcResolution = gridWindow.pcResolution();
	adaptationType = gridWindow.adaptationType();
    //test if the new inserted values make sense 
    if (gridType == 0)
    {
        if (valuesVector.size() != numIntervals)
        {
            QErrorMessage::qtHandler()->showMessage(tr("ERROR the number of intervals doesnt match the values vector size"));
            return;
        }
    }
    else
    {
        if (valuesVector.size() != (numIntervals + 1))
        {
            QErrorMessage::qtHandler()->showMessage(tr("ERROR the number of intervals doesnt match the values vector size"));
            return;
        }
    }
    //if a grid doesnt exist make new one and update it
    if (parameterGridSize == 0)
    {
        insertNewGrid(parameterNumber);
        pushButton->setText("Edit Grid");
    }

    //get the parameter input grid 
	UserInput::ParameterGridInput *grid = &inputWidget->accessStage().integrator.parameters.at(parameterNumber).grids.at(0);
	//update the parameter input grid 
	grid->values = valuesVector;
	grid->timePoints = timePointsVector;
	grid->type = UserInput::ParameterGridInput::ApproximationType(gridType);
	grid->pcresolution = pcResolution;
	grid->numIntervals = numIntervals;
	//update the parameter input grid adaptation
	grid->adapt.adaptType = UserInput::AdaptationInput::AdaptationType(adaptationType);
    grid->adapt.maxAdaptSteps = gridWindow.getMaxAdaptSteps();
	switch (adaptationType)	//get adaptation type
	{
	case 0:
		//insert wavelet adaptation information in the parameter input grid
		waveletAdaptionInfo = gridWindow.getWaveletAdaptionInfo();
		grid->adapt.adaptWave.maxRefinementLevel = waveletAdaptionInfo[0].toInt();
		grid->adapt.adaptWave.minRefinementLevel = waveletAdaptionInfo[1].toInt();
		grid->adapt.adaptWave.horRefinementDepth = waveletAdaptionInfo[2].toInt();
		grid->adapt.adaptWave.verRefinementDepth = waveletAdaptionInfo[3].toInt();
		grid->adapt.adaptWave.etres = waveletAdaptionInfo[4].toDouble();
		grid->adapt.adaptWave.epsilon = waveletAdaptionInfo[5].toDouble();
		break;
	default:
		//insert switching fucntion adaptation information in the parameter input grid
		swAdaptionInfo = gridWindow.getSWAdaptionInfo();
		grid->adapt.swAdapt.maxRefinementLevel = swAdaptionInfo[0].toInt();
		grid->adapt.swAdapt.includeTol = swAdaptionInfo[1].toDouble();
		break;
	}
}

/**
 * @brief insert a new default grid to the given userInput parameter
 * 
 * @param parameterNumber 
 */
void IntegratorTabWidget::insertNewGrid(int parameterNumber) 
{
    //create a new grid input parameter with default values
	UserInput::ParameterGridInput newGrid;
	newGrid.numIntervals = 5;
	newGrid.values.resize(6, 0);
	newGrid.type = UserInput::ParameterGridInput::PIECEWISE_LINEAR;
    //add the new ParameterGridInput to the parameter
	inputWidget->accessStage().integrator.parameters[parameterNumber].grids.push_back(newGrid);
}

/**
 * @brief change the old parameter input value with the new insert one in a specific widget
 * 
 * @param row row number of the widget
 * @param column column number of the widget
 */
void IntegratorTabWidget::changeInput(int row, int column)
{
    if (initialized)
    {
        bool isNumber;
        double insertedValue = ui->parameterTable->item(row, column)->text().toDouble(&isNumber);
        if (isNumber)
        {
            //Change the duration
		    QTableWidgetItem* widgetItem = ui->parameterTable->item(row, column); // get the item at row, col
		    int globaleIndex = widgetItem->data(Qt::UserRole).toInt();
            if(globaleIndex == 0){
                switch (column){
                    // update VALUE
                    case 2: 
	            		inputWidget->accessStage().integrator.duration.value = insertedValue;
                        break;
                    // update LOWERBOUND
	            	case 3:
	            		inputWidget->accessStage().integrator.duration.lowerBound = insertedValue;
	            		break;
                    // update UPPERBOUND
	            	case 4: 
	            		inputWidget->accessStage().integrator.duration.upperBound = insertedValue;
	            		break;
	            	default:
	            		break;
	            }
            }
            //Change the parameters
            else
            {
                int parameterIndex = globaleIndex - 1;
                switch (column)
	            {
                    case 2: // update VALUE
		    	        inputWidget->accessStage().integrator.parameters.at(parameterIndex).value= insertedValue;
                        break;

		            case 3:	// update LOWERBOUND
		    	        inputWidget->accessStage().integrator.parameters.at(parameterIndex).lowerBound= insertedValue;
		    	        break;

		            case 4: // update UPPERBOUND
		    	        inputWidget->accessStage().integrator.parameters.at(parameterIndex).upperBound= insertedValue;
		    	        break;
		            default:
		    	        break;
		        }
            }
        }
        else
        {
            ui->parameterTable->item(row, column)->setText("0");
        }
    }
}

void IntegratorTabWidget::comboBoxIndexChanged(int index){

    QComboBox* combo = qobject_cast<QComboBox*>(sender());
    if (combo){
        int row    = combo->property("row").toInt();
        int column = combo->property("column").toInt()-5;
       //change Duration ComboBox
        if(row == 0){
            switch (column){
                //change parameterType
                case 0:                
                inputWidget->accessStage().integrator.duration.paramType = UserInput::ParameterInput::ParameterType(index);
                break;
                //change parameterSensitivityType
                case 1:
                inputWidget->accessStage().integrator.duration.sensType = UserInput::ParameterInput::ParameterSensitivityType(index);
                break;
            default:
                // GUITODO
                break;
            }
        }
        //change parameters ComboBox
        else
        {   
            int parameterNumber = row -1;
			
            switch (column){
                //change parameterType
                case 0:                
                inputWidget->accessStage().integrator.parameters.at(parameterNumber).paramType = UserInput::ParameterInput::ParameterType(index);
					switch (index){    
                    case 1 :
					inputWidget->accessStage().integrator.parameters.at(parameterNumber).grids.clear();
                    case 2 : 
                    updateUI();
                    break;

                default:
                    break;
                }
                break;
                //change parameterSensitivityType
                case 1:
                inputWidget->accessStage().integrator.parameters.at(parameterNumber).sensType = UserInput::ParameterInput::ParameterSensitivityType(index);
                break;
            default:
                // GUITODO
                break;
            }
        }
    }
}

void IntegratorTabWidget::on_deletePushButton_clicked()
{
    QModelIndexList select = ui->parameterTable->selectionModel()->selectedIndexes();
    vector<int> sortingVector;

    for (int i = 0; i < select.count(); i++)
    {
        //get selected items row number
        int rowNumber = select.at(i).row();
        //get the widget item at row, col                                                             
        QTableWidgetItem *currentSelectedWidgetItem = ui->parameterTable->item(rowNumber, NAME);   
        //get selected parameterInput type
        std::string identificator = identificationMap[currentSelectedWidgetItem->text().toStdString()];

        if (identificator != "duration")
        {   //get parameter index in the stage input
            int parameterIndex = currentSelectedWidgetItem->data(Qt::UserRole).toInt() - 1;  
            //create a vector contains all parameters numbers that we want to delete           
            if (std::find(sortingVector.begin(), sortingVector.end(), parameterIndex) == sortingVector.end())
            {   
                sortingVector.push_back(parameterIndex);                                                
            }
        }
        else
        {
            QErrorMessage::qtHandler()->showMessage(tr("Only parameters and variables can be deleted"));
        }
    }
    if (sortingVector.size() > 0)
    {   
        //sort the vector to make
        sort(sortingVector.begin(), sortingVector.end(), greater<int>());
        //delete the parameters
        for (int i = 0; i <sortingVector.size() ; i++){
            inputWidget->accessStage().integrator.parameters.erase (inputWidget->accessStage().integrator.parameters.begin() + sortingVector[i]);
        }
        updateUI();
    }
}
// ...

void IntegratorTabWidget::on_addParameterPushButton_clicked()
{
    //create the dialog window
    AddElementDialog selectNameWindow(this);
    selectNameWindow.setWindowTitle("Name Picker");
    //update the windowQlistwidget with the parametersNamesList
    selectNameWindow.updateSelectNameWindow(parametersNamesList);

    int action = selectNameWindow.exec();
    //if the window closed/canceled do nothing
    if (action == QDialog::Rejected)
        return;
    //get selected items
    std::vector<std::string> selectedParameterNames = selectNameWindow.getSelectedNames();
    //walk through all the elements of the vector
    for (int i = 0; i < selectedParameterNames.size(); i++)
    {
        //test if the selected item already exist in the parameters of userInput
        if ((!existingParameters.contains(QString::fromStdString(selectedParameterNames[i]))) && (selectedParameterNames[i] != ""))
        {
            //create and insert the new parameter
            UserInput::ParameterInput parameter;
            parameter.name = selectedParameterNames[i];
            parameter.value = 0;
            parameter.paramType = UserInput::ParameterInput::ParameterType::TIME_INVARIANT;
	        parameter.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
            inputWidget->accessStage().integrator.parameters.push_back(parameter);
	        updateUI();
        }else{
            qDebug() << "The selected parameter already exist" ;
        }
    }
}

void IntegratorTabWidget::on_addVariablePushButton_clicked()
{
    //create the dialog window
    AddElementDialog selectNameWindow(this);
    selectNameWindow.setWindowTitle("Name Picker");
    //update the windowQlistwidget with the variablesNamesList
    selectNameWindow.updateSelectNameWindow(variablesNamesList);

    int action = selectNameWindow.exec();
    //if the window closed/canceled do nothing
    if (action == QDialog::Rejected)
        return;
    //get selected items
    std::vector<std::string> selectedVariableNames = selectNameWindow.getSelectedNames();
    for (int i = 0; i < selectedVariableNames.size(); i++)
    {
        //test if the selected variable item already exist in the parameters of userInput
        if((!existingVariables.contains(QString::fromStdString(selectedVariableNames[i])))&& (selectedVariableNames[i] != "") ){
        //insert the already created Variable in the variablesMap
        inputWidget->accessStage().integrator.parameters.push_back(variablesMap[selectedVariableNames[i]]);
        updateUI();
        }else{
            qDebug() << "The selected variable already exist" ;
        }
    }
}

void IntegratorTabWidget::on_plotGridResolutionSpinBox_valueChanged(int value)
{   
    //change the userInput plot grid resolution with the typed value
    inputWidget->accessStage().integrator.plotGridResolution = value;
}