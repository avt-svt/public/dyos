#include "generictabwidget.h"

/**
 * @brief   Construct a new GenericTabWidget object.
 * 
 * The inputWidget pointer is first set to 0, since it is not actually
 * possible to pass any arguments to this contructor.
 * Therefore the pointer must be set by the parent widget IMMEDIATELY
 * after the sub tab was constructed, otherwise its behaviour is undefined
 * and it may crash.
 * 
 * @param parent    (optional) The parent widget
 */
GenericTabWidget::GenericTabWidget(QWidget *parent) :
    QWidget(parent),
    inputWidget(0)
{}

GenericTabWidget::~GenericTabWidget()
{}

/**
 * @brief   Pass a pointer to the parent widget.
 * 
 * Through this pointer, the parent widget is able to make changes to the input
 * object, e.g. by using the accessInput(...) method of InputWidget.
 * 
 * @param newParent     The adress of the InputWidget
 */
void GenericTabWidget::setParentPointer(InputWidget *newParent)
{
	inputWidget = newParent;
}

/**
 * @brief   Installs the InputWidget as hover event filter on all child widgets.
 * 
 * The overall mechanism of this is better explained in the actual documentation.
 * 
 * This function will check any child widget of the sub tab, and see if the
 * widget has a non-empty "Whats This" text. If that is the case, the InputWidget
 * will be installed as event filter of the child widget, which will mean that
 * its "Whats This" text will always be displayed in the side bar once the cursor
 * hovers over it.
 * 
 */
void GenericTabWidget::installHoverFilterOnElements()
{
    // inputWidget pointer has to be set first
    if (!inputWidget) {
        qWarning() << "Could not install hover event filter!";
        return;
    }

    // get a list of all child widgets
    QList<QWidget*> widgets = this->findChildren<QWidget*>();
 
    // install the filter on every eligible widgets
    foreach (QWidget *widget, widgets) {
        if (widget->whatsThis() != "") {    // check if explanation is available
            widget->installEventFilter((QObject*)inputWidget);
        } 
    }
}
