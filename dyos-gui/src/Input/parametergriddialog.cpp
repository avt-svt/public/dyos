#include "parametergriddialog.h"
#include "ui_parametergriddialog.h"

ParameterGridDialog::ParameterGridDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ParameterGridDialog)
{
    ui->setupUi(this);
	// create empty chart
	ui->splitter->addWidget(new QChartView());
	QList<int> list = ui->splitter->sizes();
	list.replace(0, this->height() / 0.6);
	list.replace(1, this->height() / 0.4);
	ui->splitter->setSizes(list);
	//hide switching fucntion adaptation widget
	ui->swAdaptationWidget->setVisible(false);
	//connect table changes actions with the changeInput function
	connect(ui->gridTableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(changeInput(int, int)));
}
ParameterGridDialog::~ParameterGridDialog()
{
	delete ui;
}

/**
 * @brief updates every UI element that is currently displayed
 * 
 */
void ParameterGridDialog::updateUI()
{
	// make a brand new chart
	QChart *chart = new QChart();

	std::vector<double> timePointsVector;
	std::vector<double> valuesVector;
	std::string name = "test";
	//get number of intervals value
	double numintervalsValue = (double)(numIntervals());
	//try to get time Points Vector from table cells
	timePointsVector = timePoints();
	//try to get values Vector from table cells
	valuesVector = values();
	//*** this is used to create timePointsVector automatically from the  numbers of interval Value *** (if the Grid table missing some values)
	if (gridType() == 0) //test for the piecewise type 0 = piecewise_constant / 1 = piecewise_linear
	{
		if (timePointsVector.size() < (numintervalsValue * 2))
		{ //test if the timePointsVector match the numbers of interval(no = the Grid table missing somevalues)
			double intervalValue = 1 / numintervalsValue;
			double pointValue = 0;
			for (int i = 0; i < numintervalsValue; i++)
			{

				if (i != 0)
				{
					timePointsVector.push_back(pointValue - (1e-300)); //add 2 time the automatically calculated points with very small diff(walk around:cant plot piecewise_constant directly)
					timePointsVector.push_back(pointValue);
				}
				else
				{
					timePointsVector.push_back(pointValue); // if i = 0 we add 0 to the vector (interval [0,1])
				}
				pointValue += intervalValue; // create next time point value
			}
			timePointsVector.push_back(1); // add 1 to the vector
		}
	}
	else
	{
		if (timePointsVector.size() < numintervalsValue + 1 && numintervalsValue != 0)
		{ // test if the timePointsVector match the numbers of interval(no = the Grid table missing somevalues)
			double intervalValue = 1 / numintervalsValue;
			double pointValue = 0;
			for (int i = 0; i < numintervalsValue; i++) // add all the automatically calculated time points to the vector
			{
				if (i != 0)
				{
					timePointsVector.push_back(pointValue);
				}
				else
				{
					timePointsVector.push_back(pointValue); // if i=0 add 0 as the first time point value
				}
				pointValue += intervalValue;
			}
			timePointsVector.push_back(1); // add 1 as the last time point value
		}
	}

	//assert that both vectors have the same size otherways no plotting
	if (valuesVector.size() != timePointsVector.size())
	{
		QChartView *chartView = (QChartView *)ui->splitter->widget(1); // the widget(...) function returns a QWidget pointer, it needs to be cast into a QChartView pointer
		if (!chartView)
			return;
		chartView->chart()->removeAllSeries();
		return;
	}
	// now state points to the correct StateOutput object
	// -> create a line series from it and add it to the chart
	QLineSeries *series = new QLineSeries();
	for (int i = 0; i < timePointsVector.size(); ++i)
	{
		series->append(timePointsVector[i], valuesVector[i]);
	}
	//series->setName(tr(name.c_str()));
	chart->addSeries(series);

	// make chart pretty
	chart->createDefaultAxes();
	chart->setTitle("Plot");
	chart->setAnimationOptions(QChart::AllAnimations);

	QChartView *chartView = (QChartView *)ui->splitter->widget(1); // the widget(...) function returns a QWidget pointer, it needs to be cast into a QChartView pointer
	if (!chartView)
		return;							   // return if cast is unsuccessfull, otherwise we get a segmentation fault if the 3rd widget in the splitter doesn't exist or isn't a QChartView!
	QChart *oldChart = chartView->chart(); // remember old chart for later
	chartView->setChart(chart);

	// make chart view pretty
	chartView->setRenderHint(QPainter::Antialiasing);

	// delete old chart to avoid memory leaks (this is described in the Qt documentation, it is a necessary step in this case)
	if (oldChart)
		delete oldChart;
}

// return the number of invertals value
int ParameterGridDialog::numIntervals()
{
	return ui->numintervalsQlineEdit->text().toInt();
}

// return the time points vector for the plot != time points vector of USERINPUT
std::vector<double> ParameterGridDialog::timePoints()
{
	std::vector<double> timePointsVector;
	std::vector<double> timePointsVectordefault;

	int rowsNumber = ui->gridTableWidget->rowCount();
	if (gridType() == 0)
	{	
		//piecewise constant
		for (int i = 0; i < rowsNumber; i += 2)
		{
			if (ui->gridTableWidget->item(i, 0)->text() != "")
			{
				if ((i == 0) || i == (rowsNumber - 1))
				{
					//insert current table widget value to the vector
					timePointsVector.push_back(ui->gridTableWidget->item(i, 0)->text().toDouble());
				}
				else
				{	
					//insert current table widget value 2 times with small difference to the vector
					timePointsVector.push_back(ui->gridTableWidget->item(i, 0)->text().toDouble() - (1e-300));
					timePointsVector.push_back(ui->gridTableWidget->item(i, 0)->text().toDouble());
				}
			}
			else
			{
				return timePointsVectordefault;
			}
		}
	}
	// piecewise linear
	else
	{
		for (int i = 0; i < rowsNumber; i++)
		{
			if (ui->gridTableWidget->item(i, 0)->text() != "")
			{	
				//insert current table widget value to the vector
				timePointsVector.push_back(ui->gridTableWidget->item(i, 0)->text().toDouble());
			}
			else
			{
				return timePointsVectordefault;
			}
		}
	}

	return timePointsVector;
}
// return the values vector for the plot !=  values vector of USERINPUT
std::vector<double> ParameterGridDialog::values()
{
	std::vector<double> gridValuesVector;
	std::vector<double> gridValuesVectorDefault;
	int rowsNumber = ui->gridTableWidget->rowCount();
	//if piecewise_constant insert the same value of each iteration 2 times
	if (gridType() == 0)
	{
		for (int i = 1; i < rowsNumber; i += 2)
		{
			if (ui->gridTableWidget->item(i, 1)->text() != "")
			{
				gridValuesVector.push_back(ui->gridTableWidget->item(i, 1)->text().toDouble());
				gridValuesVector.push_back(ui->gridTableWidget->item(i, 1)->text().toDouble());
				// if there is a missing cell value return a vector with size of 0
			}
			else
			{
				return gridValuesVectorDefault;
			}
		}
		// if piecewise_linear insert the same value of each iteration 1 time
	}
	else
	{
		for (int i = 0; i < rowsNumber; i++)
		{
			if (ui->gridTableWidget->item(i, 1)->text() != "")
			{
				gridValuesVector.push_back(ui->gridTableWidget->item(i, 1)->text().toDouble());
			}
			// if there is a missing cell value return a vector with size of 0
			else
			{
				return gridValuesVectorDefault;
			}
		}
	}
	return gridValuesVector;
}
/**
 * @brief return the grid Type (0 = piecewise_constant / 1 = piecewise_linear)
 * 
 * @return int 
 */
int ParameterGridDialog::gridType()
{
	return ui->gridTypeComboBox->currentIndex();
}
/**
 * @brief return the grid Type (0 = piecewise_constant / 1 = piecewise_linear)
 *
 * @return int
 */
int ParameterGridDialog::adaptationType()
{
	return ui->adaptationTypeComboBox->currentIndex();
}
/**
 * @brief return Resolution value
 * 
 * @return int 
 */
int ParameterGridDialog::pcResolution()
{
	return ui->pcresolutionQlineEdit->text().toInt();
}
/**
 * @brief update the grid Editor ui with the parameter grid values
 * 
 * @param valuesVector 
 * @param timePointsVector 
 * @param selectedGridType 
 * @param pcResolution 
 * @param numIntervals 
 */
void ParameterGridDialog::updateGridEditor(UserInput::ParameterGridInput grid)
{	
	std::vector<double> valuesVector = grid.values;
	std::vector<double> timePointsVector = grid.timePoints;
	int selectedGridType = grid.type;
	int pcResolution = grid.pcresolution;
    int numIntervals = grid.numIntervals; 
	initialized = false;
	//insert the values in the ui
	ui->gridTypeComboBox->setCurrentIndex(selectedGridType);
	ui->numintervalsQlineEdit->insert(QString::number(numIntervals));
	ui->pcresolutionQlineEdit->insert(QString::number(pcResolution));
	int valuesVectorSize = valuesVector.size();
	//building the table cells
	if (valuesVectorSize != 0)
	{	
		if (selectedGridType == 0)	//piecewise constant
		{	
			//insert default row
			ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
			int index = ui->gridTableWidget->rowCount() - 1;
			//create a table widget and set its color to gray and read only
			QTableWidgetItem *readonlyWidgetDefault = new QTableWidgetItem();
			readonlyWidgetDefault->setBackground(Qt::lightGray);
			readonlyWidgetDefault->setFlags(readonlyWidgetDefault->flags() & (~Qt::ItemIsEnabled));

			if (timePointsVector.size() != 0)
			{	
				ui->gridTableWidget->setItem(index, 0, new QTableWidgetItem(QString::number(timePointsVector[0])));
			}
			else
			{
				QTableWidgetItem *editableWidgetDefault = new QTableWidgetItem();
				ui->gridTableWidget->setItem(index, 0, editableWidgetDefault);
			}
			ui->gridTableWidget->setItem(index, 1, readonlyWidgetDefault);
			//insert grid values and time points  in the table
			for (int i = 0; i < valuesVector.size(); i++)
			{
				//insert 1st row
				ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
				int index = ui->gridTableWidget->rowCount() - 1;
				QTableWidgetItem *readonlyWidgetFirstRow = new QTableWidgetItem();
				readonlyWidgetFirstRow->setBackground(Qt::lightGray);
				readonlyWidgetFirstRow->setFlags(readonlyWidgetFirstRow->flags() & (~Qt::ItemIsEnabled)); //set to read-only
				ui->gridTableWidget->setItem(index, 0, readonlyWidgetFirstRow);
				ui->gridTableWidget->setItem(index, 1, new QTableWidgetItem(QString::number(valuesVector[i])));

				//insert 2nd row
				ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
				index = ui->gridTableWidget->rowCount() - 1;
				QTableWidgetItem *readonlyWidgetSecondRow = new QTableWidgetItem();
				readonlyWidgetSecondRow->setBackground(Qt::lightGray);
				readonlyWidgetSecondRow->setFlags(readonlyWidgetSecondRow->flags() & (~Qt::ItemIsEnabled)); //set to read-only
				if (timePointsVector.size() != 0)
				{
					//insert the time point in the current cell
					ui->gridTableWidget->setItem(index, 0, new QTableWidgetItem(QString::number(timePointsVector[i + 1])));
				}
				else
				{	
					QTableWidgetItem *editableWidgetSecondRow = new QTableWidgetItem();
					ui->gridTableWidget->setItem(index, 0, editableWidgetSecondRow);
				}
				//insert grid values in the current cell
				ui->gridTableWidget->setItem(index, 1, readonlyWidgetSecondRow);
			}
		}
		//piecewise linear
		else
		{	//insert grid values and time points in the table
			for (int i = 0; i < valuesVector.size(); i++)
			{	
				ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
				int index = ui->gridTableWidget->rowCount() - 1;
				if (timePointsVector.size() != 0)
				{	
					//insert the time point in the current cell
					ui->gridTableWidget->setItem(index, 0, new QTableWidgetItem(QString::number(timePointsVector[i])));
				}
				else
				{	
					QTableWidgetItem *editableWidgetSecondRow = new QTableWidgetItem();
					ui->gridTableWidget->setItem(index, 0, editableWidgetSecondRow);
				}
				//insert grid values in the current cell
				ui->gridTableWidget->setItem(index, 1, new QTableWidgetItem(QString::number(valuesVector[i])));
			}
		}
	}
	//insert adaptation type
	ui->adaptationTypeComboBox->setCurrentIndex(grid.adapt.adaptType);
	ui->maxNumberofAdaptationStepsSpinBox->setValue(grid.adapt.maxAdaptSteps);
	switch (grid.adapt.adaptType)
	{
	//insert the wavelet adaptation information
	case 0:
		ui->maxRefinementLevelLineEdit->setText(QString::number(grid.adapt.adaptWave.maxRefinementLevel));
		ui->minRefinementLevelLineEdit->setText(QString::number(grid.adapt.adaptWave.minRefinementLevel));
		ui->horRefinementDepthLineEdit->setText(QString::number(grid.adapt.adaptWave.horRefinementDepth));
		ui->verRefinementDepthLineEdit->setText(QString::number(grid.adapt.adaptWave.verRefinementDepth));
		ui->eliminationLineEdit->setText(QString::number(grid.adapt.adaptWave.etres));
		ui->insertionLineEdit->setText(QString::number(grid.adapt.adaptWave.epsilon));
		break;
	//insert the switching fucntion adaptation information 
	default:
		ui->swMaxRefinementLevelLineEdit->setText(QString::number(grid.adapt.swAdapt.maxRefinementLevel));
		ui->includeToleranceLineEdit->setText(QString::number(grid.adapt.swAdapt.includeTol));
		break;
	}

	initialized = true;
	updateUI();
}

void ParameterGridDialog::on_timePointsAddPushButton_clicked()
{

	initialized = false;
	//add new new row
	ui->numintervalsQlineEdit->setText(QString::number(numIntervals() + 1));
	addRow();
	updateUI();
	initialized = true;
}

void ParameterGridDialog::on_timePointsDeletePushButton_clicked()
{	
	if (ui->gridTableWidget->rowCount() != 0)
	{
		initialized = false;
		//get timePointslist size
		int TableRowsNumber = ui->gridTableWidget->rowCount(); 
		if (gridType() == 0)	
		{	//piecewise constant
			if (TableRowsNumber > 3)
			{	
				//delete the last 2 rows																	
				ui->gridTableWidget->removeRow(ui->gridTableWidget->rowCount() - 1); 
				ui->gridTableWidget->removeRow(ui->gridTableWidget->rowCount() - 1);
			}
			else
			{	
				//delete all rows
				ui->gridTableWidget->setRowCount(0); 
			}
		}
		else	//piecewise linear
		{
			if (TableRowsNumber > 2)
			{	
				//delete the last row
				ui->gridTableWidget->removeRow(TableRowsNumber - 1); 
			}
			else
			{	
				//delete all rows
				ui->gridTableWidget->setRowCount(0);
			}
		}
		//update number of intervals value
		ui->numintervalsQlineEdit->setText(QString::number(numIntervals() - 1));
		updateUI();
		initialized = true;
	}
}
/**
 * @brief change the table cell value
 * 
 * @param row 
 * @param column 
 */
void ParameterGridDialog::changeInput(int row, int column)
{
	if (initialized)
	{	
		bool number;
		//get the typed value
		QTableWidgetItem *widgetItem = ui->gridTableWidget->item(row, column);
		double insertedValue = widgetItem->text().toDouble(&number);
		if (number)
		{
			switch (column) 
			{
			//check if the typed value is valid
			case 0:	
				if (insertedValue < 0 || insertedValue > 1 || isTheSmallest(row) || isTheBiggest(row))
				{	
					//overwrite the typed text
					widgetItem->setText("");
				}
				break;
			default:
				break;
			}
		}
		else
		{	
			//overwrite the typed text
			widgetItem->setText("");
		}
		updateUI();
	}
}

void ParameterGridDialog::on_gridTypeComboBox_currentIndexChanged(int index)
{
	if (initialized)
	{	
		//get the table inserted values 
		std::vector<QString> oldValues;
		switch (index)
		{
		//piecewise linear
		case 1: 
			for (int i = 1; i < ui->gridTableWidget->rowCount(); i += 2)
			{	
				//get the cell value and inserted in the oldValues vector
				QString valueAsString = ui->gridTableWidget->item(i, 1)->text();
				oldValues.push_back(valueAsString);
			}
			break;

		//piecewise constant
		default:
			for (int i = 0; i < ui->gridTableWidget->rowCount(); i++)
			{
				//get the cell value and inserted in the oldValues vector
				QString valueAsString = ui->gridTableWidget->item(i, 1)->text();
				oldValues.push_back(valueAsString);
			}
			break;
		}
		//delete all rows
		ui->gridTableWidget->setRowCount(0);
		//add all rows
		for (int i = 0; i < ui->numintervalsQlineEdit->text().toInt(); i++)
		{	
			addRow();
		}

		initialized = false;
		switch (index)
		{
		//piecewise constant
		case 0:	
			for (int i = 0; i < oldValues.size() - 1 && oldValues.size() != 0; i++)
			{	
				//insert the old value in the current cell
				QTableWidgetItem *widgetItem = ui->gridTableWidget->item((i * 2) + 1, 1);
				widgetItem->setText(oldValues[i]);
			}
			break;

		//piecewise linear
		default:
			for (int i = 0; i < oldValues.size(); i++)
			{
				//insert the old value in the current cell
				QTableWidgetItem *widgetItem = ui->gridTableWidget->item(i, 1);
				widgetItem->setText(oldValues[i]);
			}
			break;
		}
		initialized = true;
		updateUI();
	}
}

// return vector of USERINPUT (0 = time_point Vector  or  1 = values Vector)
std::vector<double> ParameterGridDialog::getInputVector(int column)
{
	std::vector<double> vector;
	std::vector<double> vectorDefault;
	int rowsNumber = ui->gridTableWidget->rowCount();
	//if piecewise_constant insert the value of each iteration
	if (gridType() == 0)
	{
		//only 2n || 2n-1 cells will be checked
		for (int i = column; i < rowsNumber; i += 2)
		{
			if (ui->gridTableWidget->item(i, column)->text() != "")
			{
				vector.push_back(ui->gridTableWidget->item(i, column)->text().toDouble());
				// if there is a missing cell value return a vector with size of 0
			}
			else
			{
				return vectorDefault;
			}
		}
		// if piecewise_linear insert the value of each iteration
	}
	else
	{
		for (int i = 0; i < rowsNumber; i++)
		{
			if (ui->gridTableWidget->item(i, column)->text() != "")
			{
				vector.push_back(ui->gridTableWidget->item(i, column)->text().toDouble());
			}
			// if there is a missing cell value return a vector with size of 0
			else
			{
				return vectorDefault;
			}
		}
	}
	return vector;
}

void ParameterGridDialog::on_addEquidistantPointspushButton_clicked()
{

	double numintervalsValue = (double)(numIntervals());
	//assert that the table isnt empty
	if (ui->gridTableWidget->rowCount() == 0)
	{
		return;
	}
	initialized = false;
	if (gridType() == 0) //piecewise_constant
	{
		//calculate interval size
		double intervalSize = 1 / numintervalsValue;
		double pointValue = 0;
		for (int i = 0; i < numintervalsValue; i++)
		{
			if (i != 0)
			{
				//set cell time point value to the automatically calculated value
				ui->gridTableWidget->item(i * 2, 0)->setText(QString::number(pointValue)); 
			}
			else
			{
				//set first cell time point value to 0
				ui->gridTableWidget->item(i, 0)->setText(QString::number(0));
			}
			//calculate next time point value
			pointValue += intervalSize; 
		}
		//set last cell time point value to 1
		ui->gridTableWidget->item(numintervalsValue * 2, 0)->setText(QString::number(1)); 
	}
	else //piecewise_linear
	{
		double intervalSize = 1 / numintervalsValue;
		double pointValue = 0;
		//add all the calculated time points to the vector
		for (int i = 0; i < numintervalsValue; i++) 
		{
			if (i != 0)
			{	
				//set cell time point value to the automatically calculated value
				ui->gridTableWidget->item(i, 0)->setText(QString::number(pointValue));
			}
			else
			{
				//set the first time point value to 0
				ui->gridTableWidget->item(i, 0)->setText(QString::number(0)); 
			}
			//calculate next time point value
			pointValue += intervalSize;
		}
		//set the last time point value to 1
		ui->gridTableWidget->item(numintervalsValue, 0)->setText(QString::number(1));
	}
	initialized = true;
	updateUI();
}

void ParameterGridDialog::on_numintervalsQlineEdit_textEdited(const QString &arg1)
{
	if (initialized)
	{
		bool isNumber;
		//get typed value
		int numberIntervels = arg1.toInt(&isNumber, 10);
		if (isNumber)
		{
			//delete all rows
			ui->gridTableWidget->setRowCount(0);
			//insert all rows
			for (int i = 0; i < arg1.toInt(); i++)
			{
				addRow();
			}
			updateUI();
		}
		else
		{
			//overwrite the typed text
			ui->numintervalsQlineEdit->setText("");
		}
	}
}
/**
 * @brief insert new row in the table based on the grid type
 * 
 */
void ParameterGridDialog::addRow()
{
	initialized = false;
	if (gridType() == 0)	//piecewise constant
	{	
		if (ui->gridTableWidget->rowCount() == 0)
		{
			//create and insert default row
			ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
			int index = ui->gridTableWidget->rowCount() - 1;
			//make first column of the row editable and the second readonly
			QTableWidgetItem *editableWidgetDefault = new QTableWidgetItem();
			QTableWidgetItem *readonlyWidgetDefault = new QTableWidgetItem();
			readonlyWidgetDefault->setFlags(readonlyWidgetDefault->flags() & (~Qt::ItemIsEnabled)); //set to read-only
			readonlyWidgetDefault->setBackground(Qt::lightGray);
			ui->gridTableWidget->setItem(index, 0, editableWidgetDefault);
			ui->gridTableWidget->setItem(index, 1, readonlyWidgetDefault);
		}

		//insert the 1st row
		ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
		//make first column of the row readonly and the second editable
		int index = ui->gridTableWidget->rowCount() - 1;
		QTableWidgetItem *editableWidget = new QTableWidgetItem();
		QTableWidgetItem *readonlyWidget = new QTableWidgetItem();
		readonlyWidget->setFlags(readonlyWidget->flags() & (~Qt::ItemIsEnabled)); //set to read-only
		readonlyWidget->setBackground(Qt::lightGray);
		ui->gridTableWidget->setItem(index, 0, readonlyWidget);
		ui->gridTableWidget->setItem(index, 1, editableWidget);

		//insert the 2nd row
		ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount());
		//make first column of the row editable and the second readonly
		index = ui->gridTableWidget->rowCount() - 1;
		QTableWidgetItem *editableWidgetSecondRow = new QTableWidgetItem();
		QTableWidgetItem *readonlyWidgetSecondRow = new QTableWidgetItem();
		readonlyWidgetSecondRow->setBackground(Qt::lightGray);
		readonlyWidgetSecondRow->setFlags(readonlyWidgetSecondRow->flags() & (~Qt::ItemIsEnabled)); //set to read-only
		ui->gridTableWidget->setItem(index, 0, editableWidgetSecondRow);
		ui->gridTableWidget->setItem(index, 1, readonlyWidgetSecondRow);
	}
	else //piecewise linear
	{
		if (ui->gridTableWidget->rowCount() == 0)
		{
			//insert default row
			ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount()); 
			int index = ui->gridTableWidget->rowCount() - 1;
			QTableWidgetItem *defaultWidget1 = new QTableWidgetItem();
			QTableWidgetItem *defaultWidget2 = new QTableWidgetItem();
			ui->gridTableWidget->setItem(index, 0, defaultWidget1);
			ui->gridTableWidget->setItem(index, 1, defaultWidget2);
		}
		//and a new row
		ui->gridTableWidget->insertRow(ui->gridTableWidget->rowCount()); 
		int index = ui->gridTableWidget->rowCount() - 1;
		QTableWidgetItem *Widget1 = new QTableWidgetItem();
		QTableWidgetItem *Widget2 = new QTableWidgetItem();
		ui->gridTableWidget->setItem(index, 0, Widget1);
		ui->gridTableWidget->setItem(index, 1, Widget2);
	}
	initialized = true;
}
/**
 * @brief check if the typed value is small than the next inserted value in the table
 * 
 * @param row 
 * @return true 
 * @return false 
 */
bool ParameterGridDialog::isTheSmallest(int row)
{
	//get the typed value
	double currentSmallestValue = ui->gridTableWidget->item(row, 0)->text().toDouble();
	bool isNumber;
	//check all the next inserted values
	for (int i = ui->gridTableWidget->rowCount() - 1; i > row; i--)
	{	
		//get current cell value
		QTableWidgetItem *widgetItem = ui->gridTableWidget->item(i, 0);
		double value = widgetItem->text().toDouble(&isNumber);
		if (value <= currentSmallestValue && isNumber)
		{
			return true;
		}
	}
	return false;
}

/**
 * @brief check if the typed value is bigger than the previous inserted value in the table
 * 
 * @param row 
 * @return true 
 * @return false 
 */
bool ParameterGridDialog::isTheBiggest(int row)
{	
	//get the typed value
	double currentBiggestValue = ui->gridTableWidget->item(row, 0)->text().toDouble();
	bool isNumber;
	//check all the next inserted values
	for (int i = 0; i < row; i++)
	{
		//get current cell value
		QTableWidgetItem *widgetItem = ui->gridTableWidget->item(i, 0);
		double value = widgetItem->text().toDouble(&isNumber);
		if (value >= currentBiggestValue && isNumber)
		{
			return true;
		}
	}
	return false;
}

void ParameterGridDialog::on_pcresolutionQlineEdit_textEdited(const QString &arg1)
{
	bool isNumber;
	//get the insert value
	int pcresolutionValue = arg1.toInt(&isNumber, 10);
	if (!isNumber)
	{
		//overwrite the typed text
		ui->pcresolutionQlineEdit->setText("");
	}
}

void ParameterGridDialog::on_fillValuesPushButton_clicked()
{
	bool isNumber;
	//get the insert value
	double value = ui->valuesLineEdit->text().toDouble(&isNumber);
	if (!isNumber)
	{
		//overwrite the typed text
		ui->valuesLineEdit->setText("");
		return;
	}
	initialized = false;
	switch (gridType())
	{
	//piecewise constant
	case 0:
		for (int i = 0; i < numIntervals(); i++)
		{	
			//insert the typed value in all the table grid value column
			QTableWidgetItem *widgetItem = ui->gridTableWidget->item((i * 2) + 1, 1);
			widgetItem->setText(QString::number(value));
		}
		break;
	//piecewise linear
	default:
		for (int i = 0; i < ui->gridTableWidget->rowCount(); i++)
		{
			//insert the typed value in all the table grid value column
			QTableWidgetItem *widgetItem = ui->gridTableWidget->item(i, 1);
			widgetItem->setText(QString::number(value));
		}
		break;
	}
	initialized = true;
	updateUI();
}

void ParameterGridDialog::on_adaptationTypeComboBox_currentIndexChanged(int index)
{
    switch (index) {
		//set wavelet adaptation widget to visible
        case 0:
            ui->waveletAdaptationWidget->setVisible(true);
            ui->swAdaptationWidget->setVisible(false);
			//set default values
			ui->maxRefinementLevelLineEdit->setText(defaultWaveletAdaptionInfo[0]);
			ui->minRefinementLevelLineEdit->setText(defaultWaveletAdaptionInfo[1]);
			ui->horRefinementDepthLineEdit->setText(defaultWaveletAdaptionInfo[2]);
			ui->verRefinementDepthLineEdit->setText(defaultWaveletAdaptionInfo[3]);
			ui->eliminationLineEdit->setText(defaultWaveletAdaptionInfo[4]);
			ui->insertionLineEdit->setText(defaultWaveletAdaptionInfo[5]);
        break;
		//set switching fucntion adaptation widget to visible 
        default:
            ui->waveletAdaptationWidget->setVisible(false);
            ui->swAdaptationWidget->setVisible(true);
			//set default values
			ui->swMaxRefinementLevelLineEdit->setText(defaultSWAdaptionInfo[0]);
			ui->includeToleranceLineEdit->setText(defaultSWAdaptionInfo[1]);
        break;
    }
}
/**
 * @brief return the wavelet adaptation information
 *
 * @return QStringList
 */
QStringList ParameterGridDialog::getWaveletAdaptionInfo()
{	
	QStringList WaveletAdaptionInfo;
	//insert all the wavelet adaptation information from the UI in the list
	WaveletAdaptionInfo.append(ui->maxRefinementLevelLineEdit->text());
	WaveletAdaptionInfo.append(ui->minRefinementLevelLineEdit->text());
	WaveletAdaptionInfo.append(ui->horRefinementDepthLineEdit->text());
	WaveletAdaptionInfo.append(ui->verRefinementDepthLineEdit->text());
	WaveletAdaptionInfo.append(ui->eliminationLineEdit->text());
	WaveletAdaptionInfo.append(ui->insertionLineEdit->text());

	return WaveletAdaptionInfo;
}
/**
 * @brief return the switching fucntion adaptation information 
 *
 * @return QStringList
 */
QStringList ParameterGridDialog::getSWAdaptionInfo()
{
	QStringList swAdaptionInfo;
	//insert all the switching fucntion information from the UI in the list
	swAdaptionInfo.append(ui->swMaxRefinementLevelLineEdit->text());
	swAdaptionInfo.append(ui->includeToleranceLineEdit->text());

	return swAdaptionInfo;
}
int ParameterGridDialog::getMaxAdaptSteps(){
	return ui->maxNumberofAdaptationStepsSpinBox->value();
}

void ParameterGridDialog::on_maxRefinementLevelLineEdit_editingFinished()
{
	bool isInt;
	//get typed value
	int value = ui->maxRefinementLevelLineEdit->text().toInt(&isInt);
	if (!isInt)
	{
		//overwrite the typed text with the default value
		ui->maxRefinementLevelLineEdit->setText(defaultWaveletAdaptionInfo[0]);
	}
}

void ParameterGridDialog::on_minRefinementLevelLineEdit_editingFinished()
{
	bool isInt;
	//get typed value
	int value = ui->minRefinementLevelLineEdit->text().toInt(&isInt);
	if (!isInt)
	{
		//overwrite the typed text with the default value
		ui->minRefinementLevelLineEdit->setText(defaultWaveletAdaptionInfo[1]);
	}
}

void ParameterGridDialog::on_horRefinementDepthLineEdit_editingFinished()
{
	bool isInt;
	//get typed value
	int value = ui->horRefinementDepthLineEdit->text().toInt(&isInt);
	if (!isInt)
	{	
		//overwrite the typed text with the default value
		ui->horRefinementDepthLineEdit->setText(defaultWaveletAdaptionInfo[2]);
	}
}

void ParameterGridDialog::on_verRefinementDepthLineEdit_editingFinished()
{
	bool isInt;
	//get typed value
	int value = ui->verRefinementDepthLineEdit->text().toInt(&isInt);
	if (!isInt)
	{
		//overwrite the typed text with the default value
		ui->verRefinementDepthLineEdit->setText(defaultWaveletAdaptionInfo[3]);
	}
}

void ParameterGridDialog::on_eliminationLineEdit_editingFinished()
{
	bool isDouble;
	//get typed value
	int value = ui->eliminationLineEdit->text().toDouble(&isDouble);
	if (!isDouble)
	{	
		//overwrite the typed text with the default value
		ui->eliminationLineEdit->setText(defaultWaveletAdaptionInfo[4]);
	}
}

void ParameterGridDialog::on_insertionLineEdit_editingFinished()
{
	bool isDouble;
	//get typed value
	int value = ui->insertionLineEdit->text().toDouble(&isDouble);
	if (!isDouble)
	{	
		//overwrite the typed text with the default value
		ui->insertionLineEdit->setText(defaultWaveletAdaptionInfo[5]);
	}
}

void ParameterGridDialog::on_swMaxRefinementLevelLineEdit_editingFinished()
{
	bool isInt;
	//get typed value
	int value = ui->swMaxRefinementLevelLineEdit->text().toInt(&isInt);
	if (!isInt)
	{	
		//overwrite the typed text with the default value
		ui->swMaxRefinementLevelLineEdit->setText(defaultSWAdaptionInfo[0]);
	}
}

void ParameterGridDialog::on_includeToleranceLineEdit_editingFinished()
{
	bool isDouble;
	//get typed value
	int value = ui->includeToleranceLineEdit->text().toDouble(&isDouble);
	if (!isDouble)
	{
		//overwrite the typed text with the default value
		ui->includeToleranceLineEdit->setText(defaultSWAdaptionInfo[1]);
	}
}
