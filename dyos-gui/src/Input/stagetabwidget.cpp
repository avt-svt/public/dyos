#include "stagetabwidget.h"
#include "ui_stagetabwidget.h"

/**
 * @brief       Construct a new StageTabWidget object.
 * 
 * @param parent    (optional) The parent widget
 */
StageTabWidget::StageTabWidget(QWidget *parent) :
    GenericTabWidget(parent),
    ui(new Ui::StageTabWidget)
{
    ui->setupUi(this);
}

StageTabWidget::~StageTabWidget()
{
    delete ui;
}

/**
 * @brief   A static method for choosing an eso model path on the system.
 * 
 * @param type              The eso type for the path (influences input behavior) 
 * @param returnedPath      A pointer in which to store the path if everything goes well
 * @return true             A path was chosen
 * @return false            No path was chosen (e.g. the user cancelled the dialog)
 */
bool StageTabWidget::chooseModelPath(EsoType type, QString *returnedPath)
{
    // retrieve last used directory
    QSettings settings;
    QString dirToOpenString;
    
    switch (type) {
    case EsoType::JADE:
        dirToOpenString = settings.value("directories/jade", QDir::homePath()).toString();
        break;
    case EsoType::JADE2:
        QErrorMessage::qtHandler()->showMessage(tr("JADE 2 is not officially supported by the GUI. Proceed at your own risk!"));
        dirToOpenString = settings.value("directories/jade2", QDir::homePath()).toString();
        break;
    case EsoType::FMI:
        dirToOpenString = settings.value("directories/fmi", QDir::homePath()).toString();
        break;
    } 

    // convert string to directory
    QDir dirToOpen;
    dirToOpen.setPath(dirToOpenString);
    if (!dirToOpen.exists()) dirToOpen = QDir::home();  // fall back to home path if directory does not exist anymore

    // set directory
    QFileDialog dialog;
    dialog.setDirectory(dirToOpen);

    switch (type) {
    case EsoType::FMI:  // GUITODO: can only select directories for now
        dialog.setFileMode(QFileDialog::FileMode::Directory);
        break;
    case EsoType::JADE2:
        QErrorMessage::qtHandler()->showMessage(tr("JADE 2 is not officially supported by the GUI. Proceed at your own risk!"));
        dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
        dialog.setNameFilter(tr("Dynamic Libraries (*.dll *.so *.dylib)"));
        break;
    case EsoType::JADE:
        dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
        dialog.setNameFilter(tr("Dynamic Libraries (*.dll *.so *.dylib)"));
        break;
    }

    // present dialog
    if (dialog.exec()) {
        QString path = dialog.selectedFiles().at(0);
        *returnedPath = path;

        // save the directory for next time
        QFileInfo fileToSave(path);
        switch (type) {
        case EsoType::JADE:
            settings.setValue("directories/jade", fileToSave.path());
            break;
        case EsoType::JADE2:
            settings.setValue("directories/jade2", fileToSave.path());
            break;
        case EsoType::FMI:
            settings.setValue("directories/fmi", fileToSave.path());
            break;            
        }
        return true;
    }
    return false;
}

/**
 * @brief   Loads a model into the GUI.
 * 
 * This method is called when the user pressed the "Load Model" button,
 * and it will attempt to load the model into the GUI. The function
 * will check whether the model string belongs to a valid eso model, and
 * it fill prefill some of the parameters in the Integrator Tab.
 * 
 */
void StageTabWidget::loadModel()
{
    QDialog dialog(this);
	QFormLayout form(&dialog);
	QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, &dialog);
	form.addRow(new QLabel("Warning! Loading a new model will overwrite all settings in the Integrator and Optimizer Tabs!"));
	form.addRow(&buttonBox);
	QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
	QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));

	if (dialog.exec() == QDialog::Accepted) {
        try {
            UserInput::EsoInput eso;
            eso.type = EsoType(ui->esoTypeComboBox->currentIndex());
            eso.model = ui->esoModelLineEdit->text().toStdString();
            GenericEso::Ptr esoPtr = getEsoPtr(eso,inputWidget->accessInput().integratorInput.order);

            // at this point we can be confident that the provided model is valid
	        UserInput::StageInput *stage = &inputWidget->accessStage();
            stage->eso = eso;
            
            // prefill parameters and variables
            stage->integrator = UserInput::IntegratorStageInput();
            stage->integrator.duration.paramType = UserInput::ParameterInput::ParameterType::DURATION;

            const int paraNum = esoPtr->getNumParameters();
            std::vector<EsoIndex> paraIndices(paraNum);
            std::vector<EsoDouble> paraValues(paraNum);
            esoPtr->getParameterIndex(paraNum, paraIndices.data());
            esoPtr->getParameterValues(paraNum, paraValues.data());

            // add parameters
            for (int i=0;i<paraNum;i++) {
                UserInput::ParameterInput parameter;
                parameter.name = esoPtr->getVariableNameOfIndex(paraIndices[i]);
                parameter.value = paraValues[i];
                parameter.paramType = UserInput::ParameterInput::ParameterType::TIME_INVARIANT;
                parameter.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;
                stage->integrator.parameters.push_back(parameter);
            }

            // set standard duration to 5 and NO
            stage->integrator.duration.value = 5;
            stage->integrator.duration.sensType = UserInput::ParameterInput::ParameterSensitivityType::NO;

            // reset otimizer settings as well
            stage->optimizer = UserInput::OptimizerStageInput();

            // update global UI, because Integrator and Optimizer tabs will be available now
            updateGlobalUI();

        } catch (std::exception &e) {
            QErrorMessage::qtHandler()->showMessage(tr(e.what()));
        } catch (...) {
            QErrorMessage::qtHandler()->showMessage(tr("An unknown error has occured!"));
        }
    }
}

/**
 * @brief   Checks whether the current eso model is valid or not 
 * 
 */
bool StageTabWidget::checkModel() 
{
    return checkModel(inputWidget->accessStage().eso);
}

/**
 * @brief   Checks whether an eso model is valid or not 
 * 
 */
bool StageTabWidget::checkModel(const UserInput::EsoInput& eso) 
{
    try {
        GenericEso::Ptr esoPtr = getEsoPtr(eso,inputWidget->accessInput().integratorInput.order);
        // eso pointer could be obtained, so we assume the model string is valid
        return true;
    } catch (...) {
        return false;
    }
}

/**
 * @brief   Updates every UI element that is currently displayed
 * 
 * The function also contains explanations for the different eso model types,
 * and what kind of model path they require.
 */
void StageTabWidget::updateUI()
{
    // get pointer to current StageInput object
	UserInput::StageInput *stage = &inputWidget->accessStage();
    // Eso
    {
        // Disable JADE 2 in dropdown menu
        QStandardItemModel *model = qobject_cast<QStandardItemModel*>(ui->esoTypeComboBox->model());
        Q_ASSERT(model != nullptr);	// safety check
        model->item(1)->setEnabled(false);

        // Setup UI according to eso type about to be loaded
        switch (EsoType(ui->esoTypeComboBox->currentIndex())) {
        case EsoType::JADE:
            ui->esoExplanationLabel->setText("The JADE interface requires a dynamic library as input. You can select the full path of the library, or just type in its name if it is saved in the same folder as the GUI executable.");
            ui->esoChoosePathButton->setEnabled(true);
            ui->esoModelLineEdit->setEnabled(true);
            ui->esoModelLineEdit->setReadOnly(false);
            break;
        case EsoType::FMI:
            ui->esoExplanationLabel->setText("The FMI interface requires a folder containing a modelDescription.xml file as input. You must specify the full path of the folder.");
            ui->esoChoosePathButton->setEnabled(true);
            ui->esoModelLineEdit->setEnabled(true);
            ui->esoModelLineEdit->setReadOnly(true);
            break;
        default:
            ui->esoExplanationLabel->setText("The currently used interface is not fully supported by the DyOS GUI.");
            ui->esoChoosePathButton->setEnabled(false);
            ui->esoModelLineEdit->setEnabled(false);
            break;
        }

        // Show current type and model
        switch (stage->eso.type) {
        case EsoType::JADE:
            ui->esoInfoLabel->setText(QString("Type: JADE \nModel: ") + QString::fromStdString(stage->eso.model));
            break;
        case EsoType::FMI:
            ui->esoInfoLabel->setText(QString("Type: FMI  \nModel: ") + QString::fromStdString(stage->eso.model));
            break;
        default:
            ui->esoInfoLabel->setText(QString("Type: unsupported  \nModel: ") + QString::fromStdString(stage->eso.model));
            break;
        }
    }

    // Refresh state mapping table
    MapAdapter::refreshTable(inputWidget->accessStage().mapping.stateNameMapping, ui->stateNameMappingTable, false);

    // Other
    {
        ui->treatObjectiveCheckBox->setChecked(stage->treatObjective);
        ui->fullStateMappingCheckBox->setChecked(stage->mapping.fullStateMapping);
    }
}

/**
 * @brief all methods which handle events from ui elements
 * 
 */
void StageTabWidget::on_esoTypeComboBox_activated(int index) 
{
	updateUI();
}

void StageTabWidget::on_esoModelLineEdit_returnPressed()
{
    ui->esoLoadModelButton->animateClick();
}

void StageTabWidget::on_esoChoosePathButton_clicked()
{
    QString path;
    EsoType type = EsoType(ui->esoTypeComboBox->currentIndex());
    bool ok = chooseModelPath(type,&path);
    if (ok) ui->esoModelLineEdit->setText(path);
}

void StageTabWidget::on_esoLoadModelButton_clicked()
{
    loadModel();
}

void StageTabWidget::on_treatObjectiveCheckBox_stateChanged(int arg1)
{
	UserInput::StageInput *stage = &inputWidget->accessStage();
    stage->treatObjective = arg1;
}
