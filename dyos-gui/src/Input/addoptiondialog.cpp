#include "addoptiondialog.h"
#include "ui_addoptiondialog.h"

AddOptionDialog::AddOptionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddOptionDialog)
{
    ui->setupUi(this);
}

AddOptionDialog::~AddOptionDialog()
{
    delete ui;
}

void AddOptionDialog::on_optionsListWidget_itemClicked(QListWidgetItem *item)
{
	// change ui key text to the text of the selected item
	ui->keyLineEdit->setText(item->text());
}

/**
 * @brief update the select name editor with the available Options
 * 
 * @param options 
 */
void AddOptionDialog::updateSelectOptionWindow(OptimizerOptions *options)
{
	currentOptions = options;
	// create the options list
	for (auto it = currentOptions->validOptions.begin(); it != currentOptions->validOptions.end(); it++)
	{
		optionslist.push_back(QString::fromStdString(it->first));
	}
	// insert the options list in the listWidget
	ui->optionsListWidget->addItems(optionslist);
}

void AddOptionDialog::on_filterLineEdit_textChanged(const QString &arg1)
{
	QRegExp regExp(arg1, Qt::CaseInsensitive, QRegExp::Wildcard);
	ui->optionsListWidget->clear();
	ui->optionsListWidget->addItems(optionslist.filter(regExp));
}

void AddOptionDialog::on_keyLineEdit_textChanged(const QString &arg1)
{
	if (currentOptions->validOptions.find(arg1.toStdString()) != currentOptions->validOptions.end())
	{
		// switch based on the value type of the selected key (INTEGER, DBL, NO_NUMBER, POS_INTEGER, POS_DBL, EMPTY)
		int type = currentOptions->validOptions[arg1.toStdString()].at(0);
		switch (NumberType(type))
		{
		case INTEGER:
			//change info Text
			ui->textLabel->setText("The typed value must be an integer.");
			ui->valueLineEdit->setEnabled(true);
			ui->valueLineEdit->setText("");
			valueType = INTEGER;
			break;
		case DBL:
			//change info Text
			ui->textLabel->setText("The typed value must be a double.");
			ui->valueLineEdit->setEnabled(true);
			ui->valueLineEdit->setText("");
			valueType = DBL;
			break;
		case NO_NUMBER:
			//change info Text
			ui->textLabel->setText("The typed value must be a text.");
			ui->valueLineEdit->setEnabled(true);
			ui->valueLineEdit->setText("");
			valueType = NO_NUMBER;
			break;
		case POS_INTEGER:
			//change info Text
			ui->textLabel->setText("The typed value must be a positive integer.");
			ui->valueLineEdit->setEnabled(true);
			ui->valueLineEdit->setText("");
			valueType = POS_INTEGER;
			break;
		case POS_DBL:
			//change info Text
			ui->textLabel->setText("The typed value must be a positive double.");
			ui->valueLineEdit->setEnabled(true);
			ui->valueLineEdit->setText("");
			valueType = POS_DBL;
			break;
		case EMPTY:
			//change info Text
			ui->textLabel->setText("No value is needed.");
			ui->valueLineEdit->setEnabled(false);
			ui->valueLineEdit->setText("");
			valueType = EMPTY;
			break;
		default:
			break;
		}
	}
	else
	{	
		valueType = EMPTY;
		ui->textLabel->setText("");
		ui->valueLineEdit->setText("");
		ui->valueLineEdit->setEnabled(true);
		ui->optionsListWidget->clear();
		ui->filterLineEdit->clear();
		ui->optionsListWidget->addItems(optionslist);
	}
}

void AddOptionDialog::on_valueLineEdit_editingFinished()
{ 	// switch based on the value type of the selected key (INTEGER, DBL, NO_NUMBER, POS_INTEGER, POS_DBL, EMPTY)
	switch (valueType)
	{
	case INTEGER:
	{
		bool isInteger;
		int value = ui->valueLineEdit->text().toInt(&isInteger);
		if (!isInteger)
		{	
			//overwrite the typed text
			ui->valueLineEdit->setText("");
			//change info Text
			ui->textLabel->setText("The typed value wasnt an integer.");
		}
		break;
	}
	case DBL:
	{
		bool isDbl;
		double value = ui->valueLineEdit->text().toDouble(&isDbl);
		if (!isDbl)
		{	
			//overwrite the typed text
			ui->valueLineEdit->setText("");
			//change info Text
			ui->textLabel->setText("The typed value wasnt a double.");
		}
		break;
	}
	case NO_NUMBER:
	{
		bool notNumber;
		double value = ui->valueLineEdit->text().toDouble(&notNumber);
		if (notNumber)
		{	
			//overwrite the typed text
			ui->valueLineEdit->setText("");
			//change info Text
			ui->textLabel->setText("The typed value wasnt a text.");
		}
		break;
	}
	case POS_INTEGER:
	{
		bool isPosInteger;
		int value = ui->valueLineEdit->text().toInt(&isPosInteger);
		if (!isPosInteger || value < 0)
		{
			//overwrite the typed text
			ui->valueLineEdit->setText("");
			//change info Text
			ui->textLabel->setText("The typed value wasnt a positive integer.");
		}
		break;
	}
	case POS_DBL:
	{
		bool isPosDbl;
		double value = ui->valueLineEdit->text().toDouble(&isPosDbl);
		if (!isPosDbl || value < 0)
		{
			//overwrite the typed text
			ui->valueLineEdit->setText("");
			//change info Text
			ui->textLabel->setText("The typed value wasnt a positive double.");
		}
		break;
	}
	default:
		break;
	}
}

void AddOptionDialog::on_okButton_clicked()
{
	on_valueLineEdit_editingFinished();
	// test if the option key exist
	if (ui->keyLineEdit->text() == "")
	{
		return;
	}
	// test if the option value is valid
	if (valueType == EMPTY || (valueType != EMPTY && ui->valueLineEdit->text() != ""))
	{
		accept();
	}
}

void AddOptionDialog::on_cancelButton_clicked()
{	
	//close window
	reject();
}
/**
 * @brief return the option key
 * 
 * @return QString 
 */
QString AddOptionDialog::getKey()
{
	return ui->keyLineEdit->text();
}
/**
 * @brief return the option value
 * 
 * @return QString 
 */
QString AddOptionDialog::getValue()
{
	return ui->valueLineEdit->text();
}
