#include "inputwidget.h"
#include "ui_inputwidget.h"

/**
 * @brief Construct a new InputWidget
 * 
 * The constructor also installs all the necessary connection to the
 * child tabs/widgets. It also installs the hover filter in all its own
 * UI elements and in the (static) UI elements of the child tabs.
 * 
 * The input object and all its meta parameters are also reset,
 * but without triggering the "Unsaved Changes" dialog!
 * 
 * @param parent 	The parent widget
 */
InputWidget::InputWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InputWidget)
{
    ui->setupUi(this);
	// get a list of all child widgets
	QList<QWidget*> widgets = this->findChildren<QWidget*>();
	// look for four child tabs
    foreach (QWidget *widget, widgets) {
		GenericTabWidget *child = qobject_cast<GenericTabWidget*>(widget);
        if (child) {
			// set parent pointer
            child->setParentPointer(this);
			// tell children to install hover filter on their UI elements
			child->installHoverFilterOnElements();
			// connect their global update functions
			connect(child, SIGNAL(updateGlobalUI()), this, SLOT(updateUI()));
        } 
    }
	// install hover event filter on own UI elements
	this->installHoverFilterOnElements();
	// reset the input, which will set additional meta parameters
	resetInput(true);
}

InputWidget::~InputWidget()
{
    delete ui;
}

/**
 * @brief	A setter method for the input object.
 * 
 * This method also checks for unsaved changes and mark all
 * changes as "saved changes" in the end.
 * 
 * @param newInput	A const reference to the new input object
 */
void InputWidget::setInput(const UserInput::Input &newInput) 
{
	// check for unsaved changes first
	if (!unsavedChangesDialog()) return;

	// perform change
	input = newInput;

	// reset input meta parameters
	stageIndex = 0;

	// mark changes as "saved changes"
	setInputSaved();

	updateUI();
}

/**
 * @brief 	Get a read-only reference to the input object.
 * 
 * Essentially this method is used whenever one wants to obtain a "copy"
 * of the current input object, e.g. when the DyOS process starts. 
 * It returns a constant reference instead of a copy to reduce overhead,
 * but it's essentially equivalent.
 * 
 * @param isSaved						(optional) Setting this to "true" will also set the input to be saved
 * @return const UserInput::Input& 		A constant reference to the current input object
 */
const UserInput::Input& InputWidget::getInput(bool isSaved)
{
	if (isSaved) setInputSaved();
	return input;
}

/**
 * @brief 	Access method for the input object.
 * 
 * This method grants read and write access to the current UserInput::Input object,
 * and it is used by e.g. the child tabs to perform their changes.
 * 
 * @return UserInput::Input& 	A reference of the current input object
 */
UserInput::Input& InputWidget::accessInput()
{
	return input; 
}

/**
 * @brief 	A convenience access method similar to "accessInput" for the currently displayed stage. 
 * 
 * @return UserInput::StageInput& 	A reference to the currently displayed stage
 */
UserInput::StageInput& InputWidget::accessStage()
{
	return input.stages.at(stageIndex);
}

/**
 * @brief 	Resets the input to a blank one.
 * 
 * The method also resets the meta parameters such as the "stageIndex",
 * and marks all changes to be "saved changes".
 * 
 * @param skipCheck 	(optional) Setting this to true will skip the check for unsaved changes
 */
void InputWidget::resetInput(bool skipCheck)
{
	// check for unsaved changes first
	if (!skipCheck) {
		if (!unsavedChangesDialog()) return;
	}

	// reset the input object with a new one
	input = UserInput::Input();

	// add one empty stage
	UserInput::StageInput stageInput;
	input.stages.push_back(stageInput);

	// set the correct stage mapping
	setCorrectMapping();

	// select the only available stage
	stageIndex = 0;

	// mark all changes as saved changes
	setInputSaved();
	
	updateUI();
}

/**
 * @brief 	Checks the current input using DyOS "convertInput" method.
 * 
 * The method will display the content of any exceptions thrown during
 * the "convertInput" method. These exceptions will contain information about
 * the inconsistencies and errors inside the current input object.
 * 
 * There are also some "assert()" statements inside the "convertInput" method,
 * which can not be caught and handled, therefore we must warn the user up front
 * that the GUI may crash.
 * 
 */
void InputWidget::checkInput(const UserInput::Input &inputToCheck) 
{
	bool accepted = okayAndCancelDialog(this, "This check uses Dyos' internal convertInput() method, which can crash for some invalid inputs, when an assert() statement is triggered. \n\nThis will close the GUI and any unsaved changes will be lost. Do you still want to perform an input check?");
	
	if (accepted) {
		QMessageBox message(this);
		try {
			// using dyos's internal checking methods
			InputConverter inputConverter;
			inputConverter.convertInput(inputToCheck);
			message.setIcon(QMessageBox::Icon::NoIcon);
			message.setText("The input seems fine to me!");
		} catch (std::exception &e) {
			message.setIcon(QMessageBox::Icon::Critical);
			message.setText(e.what());
		} catch (...) {
			message.setIcon(QMessageBox::Icon::Critical);
			message.setText("An unknown error has occured!");
		}
		message.exec();
	}
}

/**
 * @brief 	This method can be used to notify the widget that the input was
 * saved. It will use the current input as the "lastSavedInput", which
 * will effectively mark all current changes as "saved" changes.
 * 
 */
void InputWidget::setInputSaved()
{
	lastSavedInput = input;
}

/**
 * @brief 	Sets the stage mapping to true for all stages except for the last one.
 * 
 */
void InputWidget::setCorrectMapping()
{
	for (int i=0;i<input.stages.size()-1;i++) input.stages.at(i).mapping.fullStateMapping = true;
	input.stages.back().mapping.fullStateMapping = false;
}

/**
 * @brief 	A convenience method for asking the user some question which
 * he can accept or cancel.
 * 
 * @param parent 	Pointer to parent widget
 * @param prompt 	Text of the dialog
 * @return true 	User selected "accept"
 * @return false 	User cancelled
 */
bool InputWidget::okayAndCancelDialog(QWidget *parent, std::string prompt) 
{
	QDialog dialog(parent);
	QFormLayout form(&dialog);
	QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal, &dialog);
	form.addRow(new QLabel(prompt.c_str()));
	form.addRow(&buttonBox);
	QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
	QObject::connect(&buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));

	dialog.setWindowFlags(dialog.windowFlags() & (~Qt::WindowContextHelpButtonHint));
	dialog.layout()->setSizeConstraint(QLayout::SetFixedSize);
	return dialog.exec();
}

/**
 * @brief 	Checks whether there are unsaved changes and asks the user
 * whether he wants to overwrite them.
 * 
 * @return true 	User selected "okay"
 * @return false 	User cancelled
 */
bool InputWidget::unsavedChangesDialog()
{
	// return immediately if there are no unsaved changes
	if ((lastSavedInput == input) || (lastSavedInput == UserInput::Input())) {
		return true;	// proceed
	}

	// ask user what he wants to do and return his answer
	std::string prompt("You have unsaved changes. Do you still want to proceed?");
	return okayAndCancelDialog(this, prompt);
}

/**
 * @brief adds a new stage based on the currently selected one
 * 
 */
void InputWidget::addStage()
{
	// duplicate current stage
	UserInput::StageInput newStage = accessStage();

	// add new stage at the correct position in the vector
	input.stages.insert(input.stages.begin() + stageIndex + 1, newStage);
	// change to newly created stage
	stageIndex++;

	setCorrectMapping();
	updateUI();
}

/**
 * @brief removes the currently selected stage
 * 
 */
void InputWidget::removeCurrentStage()
{
	// don't allow to remove the last stage
	if (input.stages.size() == 1) return;
	// erase current stage
	input.stages.erase(input.stages.begin() + stageIndex);
	// select another stage
	if (stageIndex > 0) stageIndex--;

	setCorrectMapping();
	updateUI();
}

/**
 * @brief handles hover events from some child widgets
 * in order to display the correct explanations
 * 
 */
bool InputWidget::eventFilter(QObject *object, QEvent *event)
{
	// only handle hover events, the rest is immediately ignored
	if (event->type() != QEvent::Enter && event->type() != QEvent::Leave) return false;

	// cast object to a widget
	QWidget *widget = qobject_cast<QWidget *>(object);
	if (!widget) return false;

	// change explanation text
	if (event->type() == QEvent::Enter) {
		QString explanation = widget->whatsThis();
		if (explanation != "") {
			ui->explanationLabel->setEnabled(true);
			ui->explanationLabel->setText(explanation);
			return true;
		}
	} 

	// clear explanation text
	ui->explanationLabel->setEnabled(false);
	ui->explanationLabel->setText(tr("Hover over UI elements to show explanations."));
	return true;
}

/**
 * @brief sets the hover event filter
 * for all widgets which should display their
 * "whatsThis" text in the explanation label
 * 
 */
void InputWidget::installHoverFilterOnElements()
{
	// the tabbar needs its event filter installed programmatically, because it can not be set in Qt Creator, the "whatsThis" property is not accessible
	ui->tabWidget->tabBar()->installEventFilter(this);

    // get a list of all child widgets
    QList<QWidget*> widgets = this->findChildren<QWidget*>();
 
    // install the filter on every eligible widgets
    foreach (QWidget *widget, widgets) {
        if (widget->whatsThis() != "") {    // check if explanation is available
            widget->installEventFilter(this);
        } 
    }
}

/**
 * @brief updates every UI element that is currently displayed
 * 
 */
void InputWidget::updateUI()
{
	// update the tab widget
	{
		int index = ui->tabWidget->currentIndex();

		// Set all tabs to be enabled first
		QString tabbarWhatsThis = tr("This is a tabbar.");
		ui->tabWidget->setTabEnabled(GLOBAL,true);
		ui->tabWidget->setTabEnabled(STAGE,true);
		ui->tabWidget->setTabEnabled(INTEGRATOR,true);
		ui->tabWidget->setTabEnabled(OPTIMIZER,true);

		// Disable Integrator and Optimizer tab if model is not valid
		if (!ui->stageTab->checkModel()) {
			tabbarWhatsThis.append(tr("\n\nTo enable the integrator and optimizer tabs, a valid model has to be loaded for this stage, using the stage tab."));
			if (index == 2 || index == 3) {	// change to a valid tab 
				ui->tabWidget->setCurrentIndex(GLOBAL);
			}
			ui->tabWidget->setTabEnabled(INTEGRATOR,false);
			ui->tabWidget->setTabEnabled(OPTIMIZER,false);
		}

		ui->tabWidget->tabBar()->setWhatsThis(tabbarWhatsThis);
	}

	// update anything in the stage group box
	{
		ui->stageComboBox->clear();
		for (int i=0;i<input.stages.size();i++) {
			ui->stageComboBox->insertItem(i,QString("Stage ") + QString::number(i+1));
		}
		ui->stageComboBox->setCurrentIndex(stageIndex);
		ui->removeStageButton->setEnabled(input.stages.size()>1);
	}

	// update child tabs
	{
		int tab = ui->tabWidget->currentIndex();
		switch (tab) {
		case GLOBAL:
			ui->globalTab->updateUI();
			break;

		case STAGE:
			ui->stageTab->updateUI();
			break;

		case INTEGRATOR:
			ui->integratorTab->updateUI();
			break;

		case OPTIMIZER:
			ui->optimizerTab->updateUI();
			break;

		default:
			// This can only be triggered if someone added another tab
			qFatal("Unknown new tab could not be updated!");
			break;
		}
	}
}

/**
 * @brief all methods which make handle events from ui elements
 * 
 */
void InputWidget::on_tabWidget_currentChanged(int index)
{
	updateUI();
}

void InputWidget::on_stageComboBox_activated(int index)
{
	stageIndex = index;
	// update the whole UI because this action will influence other UI elements
	updateUI();
}

void InputWidget::on_addStageButton_clicked()
{
	addStage();
}

void InputWidget::on_removeStageButton_clicked()
{
	removeCurrentStage();
}

void InputWidget::on_checkInputButton_clicked()
{
	checkInput(input);
}

void InputWidget::on_resetInputButton_clicked()
{
	bool accepted = okayAndCancelDialog(this, "Are you sure you want to reset the input? Unsaved changes will be lost.");

	if (accepted) {
		resetInput();
	}
}