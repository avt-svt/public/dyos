#include "globaltabwidget.h"
#include "ui_globaltabwidget.h"

GlobalTabWidget::GlobalTabWidget(QWidget *parent) :
    GenericTabWidget(parent),
    ui(new Ui::GlobalTabWidget)
{
    ui->setupUi(this);
}

GlobalTabWidget::~GlobalTabWidget()
{
    delete ui;
}

/**
 * @brief updates every UI element that is currently displayed
 * 
 */
void GlobalTabWidget::updateUI()
{   
    // Bool variable to prevent seg fault from emplaceOption() 
    initialized = false; 
    // Update values
    {
        // General
        ui->finalIntegrationCheckBox->setChecked(inputWidget->accessInput().finalIntegration);
        ui->runningModeComboBox->setCurrentIndex(inputWidget->accessInput().runningMode);
        ui->totalEndTimeUpperBoundLineEdit->setText(QString::number(inputWidget->accessInput().totalEndTimeUpperBound));
        ui->totalEndTimeLowerBoundLineEdit->setText(QString::number(inputWidget->accessInput().totalEndTimeLowerBound));
        // Integrator
        ui->integratorTypeComboBox->setCurrentIndex(inputWidget->accessInput().integratorInput.type);
        ui->integrationOrderComboBox->setCurrentIndex(inputWidget->accessInput().integratorInput.order);
        ui->removeIntegratorOptionButton->setEnabled(ui->integratorOptionsTable->currentIndex().isValid());
		// Integrator Dae
		ui->daeTypeComboBox->setCurrentIndex(inputWidget->accessInput().integratorInput.daeInit.type);
		ui->linearSolverTypeComboBox->setCurrentIndex(inputWidget->accessInput().integratorInput.daeInit.linSolver.type);
		ui->nonLinearSolverTypeComboBox->setCurrentIndex(inputWidget->accessInput().integratorInput.daeInit.nonLinSolver.type);
		ui->toleranceLineEdit->setText(QString::number(inputWidget->accessInput().integratorInput.daeInit.nonLinSolver.tolerance));
		ui->maxErrorToleranceLineEdit->setText(QString::number(inputWidget->accessInput().integratorInput.daeInit.maximumErrorTolerance));
		// Optimizer
        ui->optimizerTypeComboBox->setCurrentIndex(inputWidget->accessInput().optimizerInput.type);
        ui->optimizationModeComboBox->setCurrentIndex(inputWidget->accessInput().optimizerInput.optimizationMode);
        ui->removeOptimizerOptionButton->setEnabled(ui->optimizerOptionsTable->currentIndex().isValid());
		ui->adaptiveStrategyComboBox->setCurrentIndex(inputWidget->accessInput().optimizerInput.adaptationOptions.adaptStrategy);
		ui->adaptationThresholdLineEdit->setText(QString::number(inputWidget->accessInput().optimizerInput.adaptationOptions.adaptationThreshold));
		ui->intermConstraintViolationToleranceLineEdit->setText(QString::number(inputWidget->accessInput().optimizerInput.adaptationOptions.intermConstraintViolationTolerance));
		ui->numOfIntermPointsLineEdit->setText(QString::number(inputWidget->accessInput().optimizerInput.adaptationOptions.numOfIntermPoints));
	} 

    // Special setup
    {
        // General
        QStandardItemModel *model = qobject_cast<QStandardItemModel*>(ui->runningModeComboBox->model());
        Q_ASSERT(model != nullptr);	// safety check
        model->item(UserInput::Input::RunningMode::MULTIPLE_SHOOTING)->setEnabled(false);
        // Integrator
        model = qobject_cast<QStandardItemModel*>(ui->integrationOrderComboBox->model());
        Q_ASSERT(model != nullptr);	// safety check
        model->item(UserInput::IntegratorInput::IntegrationOrder::FIRST_REVERSE)->setEnabled(false);
        model->item(UserInput::IntegratorInput::IntegrationOrder::SECOND_REVERSE)->setEnabled(false);
    }
    
    // IntegratorOptions
    MapAdapter::refreshTable(inputWidget->accessInput().integratorInput.integratorOptions,  ui->integratorOptionsTable);

    // OptimizerOptions
    MapAdapter::refreshTable(inputWidget->accessInput().optimizerInput.optimizerOptions,    ui->optimizerOptionsTable);

    // initialization completed
    initialized = true; 
}

/**
 * @brief all methods which handle events from ui elements
 * 
 */
void GlobalTabWidget::on_finalIntegrationCheckBox_stateChanged(int arg1)
{
    inputWidget->accessInput().finalIntegration = arg1;
}

void GlobalTabWidget::on_runningModeComboBox_currentIndexChanged(int index)
{
    inputWidget->accessInput().runningMode = UserInput::Input::RunningMode(index);
}

void GlobalTabWidget::on_totalEndTimeUpperBoundLineEdit_editingFinished()
{
    bool ok;
    double value = ui->totalEndTimeUpperBoundLineEdit->text().toDouble(&ok);
    if (ok) {
        inputWidget->accessInput().totalEndTimeUpperBound = value;
    } else {
        updateUI();
    }
}

void GlobalTabWidget::on_totalEndTimeLowerBoundLineEdit_editingFinished()
{
    bool ok;
    double value = ui->totalEndTimeLowerBoundLineEdit->text().toDouble(&ok);
    if (ok) {
        inputWidget->accessInput().totalEndTimeLowerBound = value;
    } else {
        updateUI();
    }
}

// Integrator
void GlobalTabWidget::on_integratorTypeComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().integratorInput.type = UserInput::IntegratorInput::IntegratorType(index);
}

void GlobalTabWidget::on_integrationOrderComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().integratorInput.order = UserInput::IntegratorInput::IntegrationOrder(index);
}

void GlobalTabWidget::on_addIntegratorOptionButton_clicked()
{
    AddOptionDialog addOptionWindow(this);
    addOptionWindow.setWindowTitle("Integrator Option Picker");

    OptimizerOptions *options;
	options = options = new OptimizerOptions();
	addOptionWindow.updateSelectOptionWindow(options);
    int action = addOptionWindow.exec();
    
    if(action == QDialog::Rejected)
        return;
    //  get key and value
    std::string key = addOptionWindow.getKey().toStdString();
	std::string value = addOptionWindow.getValue().toStdString();
    // add key and value
	MapAdapter mapadapter(this);
    mapadapter.addOption(key,value, inputWidget->accessInput().integratorInput.integratorOptions);
	updateUI();
}

void GlobalTabWidget::on_removeIntegratorOptionButton_clicked() 
{
    MapAdapter mapadapter(this);
    mapadapter.removeOption(inputWidget->accessInput().integratorInput.integratorOptions,ui->integratorOptionsTable); 
    updateUI();
}

void GlobalTabWidget::on_integratorOptionsTable_itemSelectionChanged() 
{
    ui->removeIntegratorOptionButton->setEnabled(ui->integratorOptionsTable->currentIndex().isValid());
}

void GlobalTabWidget::on_integratorOptionsTable_cellChanged(int row, int column) 
{
    if (initialized) 
    {
        MapAdapter mapadapter(this); 
        mapadapter.emplaceOption(row, column, inputWidget->accessInput().integratorInput.integratorOptions,ui->integratorOptionsTable); 
    }
}

void GlobalTabWidget::on_daeTypeComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().integratorInput.daeInit.type = UserInput::DaeInitializationInput::DaeInitializationType(index);
}

void GlobalTabWidget::on_linearSolverTypeComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().integratorInput.daeInit.linSolver.type = UserInput::LinearSolverInput::SolverType(index);
}

void GlobalTabWidget::on_nonLinearSolverTypeComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().integratorInput.daeInit.nonLinSolver.type = UserInput::NonLinearSolverInput::SolverType(index);
}

void GlobalTabWidget::on_toleranceLineEdit_editingFinished()
{
	bool isDouble;
	double value = ui->toleranceLineEdit->text().toDouble(&isDouble);
	if (isDouble) {
		inputWidget->accessInput().integratorInput.daeInit.nonLinSolver.tolerance = value;
	}
	else {
		ui->toleranceLineEdit->setText("");
	}
}

void GlobalTabWidget::on_maxErrorToleranceLineEdit_editingFinished()
{
	bool isDouble;
	double value = ui->maxErrorToleranceLineEdit->text().toDouble(&isDouble);
	if (isDouble) {
		inputWidget->accessInput().integratorInput.daeInit.maximumErrorTolerance = value;
	}
	else {
		ui->toleranceLineEdit->setText("");
	}
}

// Optimizer
void GlobalTabWidget::on_optimizerTypeComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().optimizerInput.type = UserInput::OptimizerInput::OptimizerType(index);
}

void GlobalTabWidget::on_optimizationModeComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().optimizerInput.optimizationMode = UserInput::OptimizerInput::OptimizationMode(index);
}

void GlobalTabWidget::on_addOptimizerOptionButton_clicked() 
{	
    AddOptionDialog addOptionWindow(this);
    addOptionWindow.setWindowTitle("Optimizer Option Picker");	
    OptimizerOptions *options;
    // check what optimizer type is selected
	switch (ui->optimizerTypeComboBox->currentIndex())
    {
    case 0 :
        options = new SNOPTOptions();
        break;
	case 2 :
		options = new NPSOLOptions();
        break;
    default:
        options = new OptimizerOptions();
        break;
    }
    //update the window (based on the selected optimizer type) with the new valid options
    addOptionWindow.updateSelectOptionWindow(options);
    //open the AddOptionDialog window
    int action = addOptionWindow.exec();
    //if window closed or canceled do nothing
    if(action == QDialog::Rejected)
        return;
    //get key and value
    std::string key = addOptionWindow.getKey().toStdString();
	std::string value = addOptionWindow.getValue().toStdString();
    // add key and value
	MapAdapter mapadapter(this);
    mapadapter.addOption(key,value, inputWidget->accessInput().optimizerInput.optimizerOptions);
    updateUI();
}

void GlobalTabWidget::on_removeOptimizerOptionButton_clicked()
{
    MapAdapter mapadapter(this);
    mapadapter.removeOption(inputWidget->accessInput().optimizerInput.optimizerOptions,ui->optimizerOptionsTable); 
    updateUI();
}

void GlobalTabWidget::on_optimizerOptionsTable_itemSelectionChanged() 
{
    ui->removeOptimizerOptionButton->setEnabled(ui->optimizerOptionsTable->currentIndex().isValid());
}

void GlobalTabWidget::on_optimizerOptionsTable_cellChanged(int row, int column) 
{
    if (initialized) 
    {
        MapAdapter mapadapter(this); 
        mapadapter.emplaceOption(row, column, inputWidget->accessInput().optimizerInput.optimizerOptions, ui->optimizerOptionsTable); 
    }
}

void GlobalTabWidget::on_adaptationThresholdLineEdit_editingFinished()
{
	bool ok;
	double value = ui->adaptationThresholdLineEdit->text().toDouble(&ok);
	if (ok) {
		inputWidget->accessInput().optimizerInput.adaptationOptions.adaptationThreshold = value;
	}
	else {
		updateUI();
	}
}

void GlobalTabWidget::on_intermConstraintViolationToleranceLineEdit_editingFinished()
{
	bool ok;
	double value = ui->intermConstraintViolationToleranceLineEdit->text().toDouble(&ok);
	if (ok) {
		inputWidget->accessInput().optimizerInput.adaptationOptions.intermConstraintViolationTolerance = value;
	}
	else {
		updateUI();
	}
}

void GlobalTabWidget::on_numOfIntermPointsLineEdit_editingFinished()
{
	bool ok;
	unsigned value = ui->numOfIntermPointsLineEdit->text().toUInt(&ok);
	if (ok) {
		inputWidget->accessInput().optimizerInput.adaptationOptions.numOfIntermPoints = value;
	}
	else {
		updateUI();
	}
}

void GlobalTabWidget::on_adaptiveStrategyComboBox_currentIndexChanged(int index)
{
	inputWidget->accessInput().optimizerInput.adaptationOptions.adaptStrategy = UserInput::AdaptationOptions::AdaptiveStrategy(index);
}


