/**
 * \mainpage
 * 
 * Welcome to the Doygen Documentation for the DyOS GUI!
 * 
 * This GUI was developed in 2019 by four CES students for their software engineering
 * internship at the AVT.SVT. A more fleshed out documentation in form of a Latex document
 * (or Latex code) should also be contained somewhere in this repository, so please read
 * that one if you are trying to understand some of the broader concepts behind the GUI.
 * 
 * The Doxygen documentation aims to describe the purpose of the different functions and 
 * classes directly, without going into much detail about their relations to each other.
 * It also tries to describe some of the input parameters of functions, etc.
 * 
 * If that is what you're after, you've come to the right place!
 * 
 * @authors   Paul Orschau, Anas Hamrouni, Florian Berthold, Steffen Thevis
 * @version   1.0
 * @date      2019-10-13
 */

// Qt includes
#include <QApplication>
#include <QErrorMessage>
#include <QDebug>

// DyOS includes
#include "Dyos.hpp"

// GUI includes
#include "mainwindow.h"

// These macros are necessary to use the input and output objects in "signals and slots" connections
Q_DECLARE_METATYPE(UserInput::Input)
Q_DECLARE_METATYPE(UserOutput::Output)

void myMessageOutput(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
  if (!msg.startsWith("QWindowsWindow::setGeometry")) {
    QErrorMessage::qtHandler()->showMessage(msg);
  }
}

/**
 * @brief the main function of the application
 * 
 * Simply opens the main window.
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	QApplication::windowIcon();
    // Some general information about the application, needed to store settings
    a.setApplicationName("DyOS GUI");
    a.setOrganizationName("AVT.SVT");
    a.setOrganizationDomain("avt.rwth-aachen.de");

    // Display certain messages in a popup window
    QErrorMessage::qtHandler();
    qInstallMessageHandler(myMessageOutput);

    // Register meta types, such that signals and slots work with these objects
    qRegisterMetaType<UserInput::Input>();
    qRegisterMetaType<UserOutput::Output>();

    // Creating the main window
    MainWindow w;
    w.show();

    // Running the app
    return a.exec();
}
