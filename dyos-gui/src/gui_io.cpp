#include "gui_io.h"

/**
 * @brief   This static function opens a dialog to save some generic data to a text file.
 * 
 * @tparam DataObject           This template parameter determines the type of object which is to be saved, e.g. "UserInput::Input" or "UserOutput::Output"
 * @param data                  The actual object which should be saved
 * @param saveFunction          A pointer to the DyOS function which is used to save the object
 * @param settingsKey           The key under which the last used location is saved, e.g. "input" or "output"
 * @param windowTitle           The title for the dialog window
 * @param fileDescription       The description of the file type to be saved, e.g. "Input Files" or "Output Files"
 * @param parent                A parent widget for the dialog window (optional)
 * @return true                 The function was successful
 * @return false                The function failed and data was not saved
 */
template<typename DataObject>
bool GUI_IO::saveData(const DataObject& data, void (*saveFunction)(std::string, DataObject, DataFormat),
                      QString settingsKey, QString windowTitle, QString fileDescription, QWidget *parent)
{
    // retrieve last used directory
    QSettings settings;
    QString key = tr("directories/") + settingsKey;
    QString dirToOpen = settings.value(key, QDir::homePath()).toString();

    // get file name
    QString filename = QFileDialog::getSaveFileName(parent, windowTitle, dirToOpen, fileDescription + tr(" (*.json *.xml)"));

    // return immediately if user pressed cancel
    if (filename.isEmpty()) return false;

    // obtain path
    boost::filesystem::path path(filename.toLocal8Bit().constData());

    // determine data format
    DataFormat format;
    if (boost::iequals(path.extension().string(), std::string(".json"))) {
        format = DataFormat::JSON;
    } else if (boost::iequals(path.extension().string(), std::string(".xml"))) {
        format = DataFormat::XML;
    } else {
        QErrorMessage::qtHandler()->showMessage(tr("Unsupported file extension!"));
        return false;
    }

    // try saving the data
    try {
        saveFunction(path.string(),data,format);
        // remember the directory for next time
        QDir dirToSave;
        settings.setValue(key, dirToSave.absoluteFilePath(filename));

    } catch (std::exception &e) {
        QErrorMessage::qtHandler()->showMessage(tr(e.what()));
        return false;
    } catch (...) {
        QErrorMessage::qtHandler()->showMessage(tr("An unknown error has occured!"));
        return false;
    }

    return true;
}


/**
 * @brief   This static function opens a dialog to open some generic data from a text file.
 * 
 * @tparam DataObject           This template parameter determines the type of object which is to be opened, e.g. "UserInput::Input" or "UserOutput::Output"
 * @param data                  The place where the opened data should be stored
 * @param saveFunction          A function pointer which is used to open the object
 * @param settingsKey           The key under which the last used location is saved, e.g. "input" or "output"
 * @param windowTitle           The title for the dialog window
 * @param fileDescription       The description of the file type to be opened, e.g. "Input Files" or "Output Files"
 * @param parent                A parent widget for the dialog window (optional)
 * @return true                 The function was successful
 * @return false                The function failed and the "returnedData" remains unchanged
 */
template<typename DataObject>
bool GUI_IO::openData(DataObject *returnedData, void (*loadFunction)(std::string, DataObject&, DataFormat),
                      QString settingsKey, QString windowTitle, QString fileDescription, QWidget *parent)
{
    DataObject openedData;  // object which will ultimately be returned in the end

    // retrieve last used directory
    QSettings settings;
    QString key = tr("directories/") + settingsKey;
    QString dirToOpen = settings.value(key, QDir::homePath()).toString();

    // get file name
    QString filename = QFileDialog::getOpenFileName(parent, windowTitle, dirToOpen, fileDescription + tr(" (*.json *.xml)"));

    // return immediately if user pressed cancel
    if (filename.isEmpty()) return false;

    // obtain path
    boost::filesystem::path path(filename.toLocal8Bit().constData());

    // determine data format
    DataFormat format;
    if (boost::iequals(path.extension().string(), std::string(".json"))) {
        format = DataFormat::JSON;
    } else if (boost::iequals(path.extension().string(), std::string(".xml"))) {
        format = DataFormat::XML;
    } else {
        QErrorMessage::qtHandler()->showMessage(tr("Unsupported file extension!"));
        return false;
    }

    // try opening the data
    try {
        loadFunction(path.string(),openedData,format);
        // save the directory for next time
        QDir dirToSave;
        settings.setValue(key, dirToSave.absoluteFilePath(filename));

    } catch (std::exception &e) {
        QErrorMessage::qtHandler()->showMessage(tr(e.what()));
        return false;
    } catch (...) {
        QErrorMessage::qtHandler()->showMessage(tr("An unknown error has occured!"));
        return false;
    }

    // everything went well, so we can save the new data
    *returnedData = openedData;
    return true;
}

/**
 * @brief   A specialization of "saveData" to save an input.
 * 
 * @param inputToSave   The input which is to be saved
 * @param parent        A parent widget (optional)
 * @return true             The function was successful
 * @return false            The function failed and the input was not saved
 */
bool GUI_IO::saveInput(const UserInput::Input& inputToSave, QWidget *parent)
{
    return saveData<UserInput::Input>(inputToSave, convertUserInputToXmlJson, "input", "Save Input", "Input Files", parent);
}

/**
 * @brief   A specialization of "saveData" to save an output.
 * 
 * @param outputToSave  The output which is to be saved
 * @param parent        A parent widget (optional)
 * @return true         The function was successful
 * @return false        The function failed and the output was not saved
*/
bool GUI_IO::saveOutput(const UserOutput::Output& outputToSave, QWidget *parent)
{
    return saveData<UserOutput::Output>(outputToSave, convertUserOutputToXmlJson, "output", "Save Output", "Output Files", parent);
}

/**
 * @brief   A specialization of "openData" to open an input. The function also performs a model check.
 * 
 * @param returnedInput     The space in which the input is to be stored
 * @param parent            A parent widget (optional)
 * @return true             The function was successful
 * @return false            The function failed and the "returnedInput" remains unchanged
 */
bool GUI_IO::openInput(UserInput::Input *returnedInput, QWidget *parent)
{
    UserInput::Input openedInput;   // input which will ultimately be returned in the end

    // retrieve input
    bool ok = openData<UserInput::Input>(&openedInput, convertUserInputFromXmlJson, "input", "Open Input", "Input Files", parent);
    if (!ok) return false;

    // check the input for incompatible models
    ok = ResolveStageDialog::resolveModelStrings(&openedInput,parent);
    if (!ok) return false;

    // everything went well, so we can save the new input
    *returnedInput = openedInput;
    return true;
}

/**
 * @brief   A specialization of "openData" to open an output.
 * 
 * @param returnedOutput    The space in which the output is to be stored
 * @param parent            A parent widget (optional)
 * @return true             The function was successful
 * @return false            The function failed and the "returnedOutput" remains unchanged
 */
bool GUI_IO::openOutput(UserOutput::Output *returnedOutput, QWidget *parent)
{
    return openData<UserOutput::Output>(returnedOutput, convertUserOutputFromXmlJson, "output", "Open Output", "Output Files", parent);
}