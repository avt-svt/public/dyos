\section{Design}\label{sec:Design}

\subsection{Key Principles}\label{sec:KeyPrinciples}

From our analysis of the requirements of this project, we isolated a few principles which we believe to be most important. These principles were taken into account for any decision we had to make in the design process.

\begin{itemize}
    \item \textbf{Future Proof:} DyOS is a piece of \textit{scientific} software, and therefore bound to change a lot. Newly developed features in DyOS will require new input options or produce new output data, and it is essential that these new options can easily be integrated in the GUI. We want to avoid the GUI becoming stale and outdated \textit{at all costs}, because outdated software will be used and maintained even less, making this effect almost "self-perpetuating".
    
    \item \textbf{Avoid Duplicated DyOS Code:} Some functionality which is already implemented in DyOS should always be directly referenced instead of copied. This will ensure that a lot of aspects of the GUI will automatically stay up to date with DyOS, as long as DyOS stays consistent and up-to-date \textit{internally}.
    
    \item \textbf{Scaleable:} Our group was only required to test DyOS with two smaller benchmark cases, but we also had to keep in mind that DyOS may be used with models containing hundreds or thousands of variables. One good example of the application of this principle are the "ComboBox" elements throughout the GUI, which are only used when the number of options can't become too large in different usage scenarios.
\end{itemize}



\subsection{Concept}\label{sec:Concept}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/concept1}
\end{figure}

Something that became obvious to us at the very start of this project, was the clear structure that DyOS has. There is an input, this input is plugged into the \lstinline{runDyos(...)} function, and out comes an output. It's almost like a mathematical transformation, which maps one \lstinline{UserInput::Input} object to one \lstinline{UserOutput::Output} object.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/concept2}
\end{figure}

This led to some very natural design decisions. The three steps – "input", "running DyOS" and "output" – became obvious candidates for the three main tabs which the final version of our GUI has. It's a clear pipeline, which very intuitively leads the user through the entire process behind using DyOS – from start to finish.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/concept3}
\end{figure}

When we thought about the different features the GUI has to provide, we understood that the data, such as the input and output data, will have to move through our GUI through more than just this standard pipeline. Additional routes for different use cases should be possible, e.g. a user may want to save a given input for later use, or open an older output to take a look at it. Visualizing the different pathways the data can take, as well as the actions the user may want to perform on the data, leads to the following chart:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/concept4}
\end{figure}

As stated in the section about our key principles (\ref{sec:KeyPrinciples}), these actions should be realized using DyOS' own code whenever possible. We identified quite a lot of actions which could rely entirely on DyOS functions, and those actions are highlighted here:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/concept5}
\end{figure}

This is great, because it means that all possible use cases involving only these green arrows will automatically stay up-to-date with DyOS, as long as DyOS remains up-to-date \textit{internally}! Here are a few examples of such use cases:

\begin{itemize}
    \item A user receives an input file from a colleague, opens it in the DyOS GUI, runs a calculation and interprets the result in the output tab.
    
    \item A developer uses the GUI to debug DyOS' code, using the console output and a variety of standard benchmark inputs. These inputs could be saved in a collection of input files, or be permanently coded into the GUI, which make them available with just a keyboard shortcut. 
    
    \item A user uses "the old-fashioned way" (a C++ program) to generate an input file containing a \textit{completely new input option}, which has not been implemented in the GUI yet. Still, the user will be able to open this input file in the GUI, even \textit{inspect the new option} in the input summary of the run tab! This is possible because this summary is equivalent to a JSON file of the input. She can then run a calculation like with any other supported input, and look at the output graphs.
    
    \item A user is not satisfied with the (fairly basic) output in the DyOS GUI, and would like to customize the appearance of the plots. The user can save the output to a JSON file, and has written a script to extract the desired data into a file which can be opened e.g. by GNUPLOT. 
\end{itemize}

We are very satisfied that we were able to achieve such a high flexibility, at least when it comes to input and output files, and we hope that this will even motivate a lot of developers at the AVT.SVT to consider using the GUI on a day-to-day basis, to benefit especially from the output tab, which allows for a quick sanity check of the results after DyOS was run.



\subsection{Model View Controller}\label{sec:ModelViewController}

\begin{figure}[H]
\centering
\includegraphics[width=5cm]{Resources/concept6}
\caption{Software Desing Pattern - Model View Controller}
\end{figure}

The GUI uses the standard software design pattern "Model View Controller" (MVC) for data management, manipulation and representation. In our case, the "model" consists of either a \lstinline{UserInput::Input} or \lstinline{UserOutput::Output} object, while the "view" and "controller" are combined into several Qt classes and custom subclasses, such as e.g. the \lstinline{InputWidget}, which displays the data in the model and performs changes by the user. Specific UI elements, like a \lstinline{QComboBox}, can be thought of as delegates.

Because the data in the \lstinline{UserInput::Input} object is very heterogeneous, unlike e.g. songs in a song library, or student data in a universities' database, the data can not easily be indexed by simply a "row" and "column". When we e.g. talk about \lstinline{UserInput::Input::totalEndTimeUpperBound}, it does not make sense to assign a "row" and "column" to this data entry. Therefore we decided not to subclass \lstinline{QAbstractItemModel}, which is usually the standard approach to MVC described here: \url{https://doc.qt.io/qt-5/model-view-programming.html}

Instead we chose to use the \lstinline{UserInput::Input} and \lstinline{UserOutput::Output} classes as models \textit{directly}, which makes our implementation a bit more clunky in certain parts, but ultimately easier to understand and more lightweight. The actual \textit{instances} of these models are saved in very few places in the GUI, and most of the time we simply access them with pointers. They only behave as "true" models in the \lstinline{InputWidget} and \lstinline{OutputWidget} classes, where their content will be displayed.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/concept7}
\caption{Data flow between the widgets}
\end{figure}

The main few places in which these full models are stored can be seen in the diagram above, as well as the few actions which will copy a model to another spot. These actions are:

\begin{enumerate}
    \item When \lstinline{RunWidget::updateUI()} is called, the current input object from the Input Tab is copied over to the Run Tab. This happens e.g. whenever the user switches from any other tab to the Run Tab.
    
    \item When \lstinline{RunWidget::runDyos()} is executed, the currently cached input object from the Run Tab is copied to the DyosThread. This happens e.g. when the user presses the "Run DyOS" button, or uses the "Run Current Input" action ({\tt Strg+R}).
    
    \item Whenever DyOS is finished with its calculation, the \lstinline{DyosThread::sendOutput(...)} signal is emitted by the DyosThread. This signal is connected to the \lstinline{OutputWidget::setOutput(...)} slot. By using Qt's "signals and slots" mechanism we ensure that the transfer is thread-safe, and this transfer can even happen while the user has already switched to the Output Tab. 
    
\end{enumerate}



\subsection{Prototype}\label{sec:Prototype}

\subsubsection{Early drafts}

Our group designed some very early drafts of the GUI on paper, to make our individual visions for the GUI concrete and to better communicate with the AVT.SVT as well. Here are some of those drawings:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/conceptInput}
\caption{Early Concept Input Tab}
\end{figure}

As you can see in this early concept of the Input Tab, we knew very early on that we would need a few sub tabs to represent the many input options. At this point we did not understand DyOS well enough to realize that we would need a stage selection box on the right, and we also later moved the position of the help texts to the side bar, since not all that much space would be required. Still, it is remarkable how close this very early drawing comes to the final product, which can be seen in section \ref{sec:InputTab}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/conceptRun}
\caption{Early Concept Run Tab}
\end{figure}

A greater deviation from the early concepts can be seen in the Run Tab, because we also did not understand a lot of key aspects about how DyOS would be executed in the end, e.g. we still assumed that we would generate an input file in the first tab, and then specify input and output file paths for DyOS to operate with. This turned out to be a false assumption, and in the final version of the GUI, the two path boxes are gone. Instead we were able to include a nice input summary, which shows the true and complete content of an input object which is passed on to DyOS. The "Start" button obviously also made it into the final product (which can be seen in section \ref{sec:RunTab}), but now it is called "Run DyOS".

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/conceptOutput}
\caption{Early Concept Output Tab}
\end{figure}

The final version of the output tab is also roughly equivalent to the first drafts we made, we still have the selection of the plots on the left, the graphs in the center, and a few buttons and other infos on the right. In our final version of the tab (which can be seen in section \ref{sec:OutputTab}), the different plottable items are grouped into three categories, which was a step that only became obvious once we were more familiar with DyOS' output.