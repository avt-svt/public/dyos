\section{User Documentation}

\subsection{Installation}

The GUI is installed alongside DyOS, therefore it makes a lot of sense to reference DyOS' own manual at this point in the document. The DyOS manual explains the complete installation in section 4.1 (at least as of right now).

In addition to all the steps in the manual, we also have to consider the Qt integration. To compile the GUI, you can download the latest version of Qt here: \url{https://www.qt.io}

After installing Qt, you can specify the \lstinline{Qt5_DIR} option in CMake to complete the installation. The path should look similar to this:

{\tt /Users/paulorschau/Qt/5.12.3/clang\_64/lib/cmake/Qt5}

The path above should contain a {\tt Qt5Config.cmake} file, which CMake needs to integrate Qt into the build process.

Lastly, the "BUILD\_GUI" build option in CMake has to be set to {\tt true}, this can either be done in the CMake GUI or on the command line.



\subsection{Tutorial}\label{sec:Tutorial}

In this section we will describe the functionality of the GUI by guiding the user step-by-step through an example calculation. The same example was also used in the final presentation of the GUI (see \ref{sec:Phase2}).



\subsubsection{Story and Mathematical Model}

An exemplary use-case for the DyOS GUI. \\
A user wants to optimize his travel time from location A to location B with an overall distance of 17.9 kilometers, with A and B being urban locations with connection to the highway.  \\
Starting in A, his maximum velocity is limited to 50 km/h in cities (3.6 km), as he reaches the highway, the fastest the car can go is 150 km/h (5.8 km). On the highway, a the user might get into a traffic jam, he can only drive 80 km/h (1 km). After that there is again no speed limit until reaching the exit (5.8 km). Leaving the highway, the speed limit is 50 km/h again until the final destination is reached (3 km). \\
We use the following data: 
\begin{enumerate}
    \item part: $\nu_{max} =  50 \frac{km}{h}, d = 3.6 km $ 
    \item part: $\nu_{max} = 150 \frac{km}{h}, d = 5.8 km $
    \item part: $\nu_{max} =  80 \frac{km}{h}, d = 1 km $
    \item part: $\nu_{max} = 150 \frac{km}{h}, d = 5.8 km $
    \item part: $\nu_{max} =  50 \frac{km}{h}, d = 1.7 km $
\end{enumerate}

As DyOS needs a model to optimize, we choose "CarFreeFinalTimeTwoObjectives", which was already used in the original DyOS manual.
This model is very simple, as the car can only accelerate and break.
It consists of the following formulas: 

\begin{itemize}
    \item min $obj(t_f)$
    \item $\dot{d} = \nu$
    \item $\dot{\nu} = a - \alpha * \nu^2 $
    \item $a = k$ $*$ exp$(-\nu^2)$
\end{itemize}

With "d" being the distance "$\nu$" the velocity, "a" the acceleration "$\alpha$" a constant for air resistance and k has to chosen so that a maximum velocity is not exceeded. 
Because the user wants to calculate the minimum time to get to his destination by car, the objective for this optimization is going to be the final time $t_f$. 



\subsubsection{Creating the Input}

To start an optimization run, we first need to enter our model and all our chosen constraints into the GUI. After opening the GUI, the user will already find himself in the "Input" tab, where all the settings for the optimization have to be made. This tab has several sub tabs, which are all responsible for different aspects of the input. 

\begin{itemize}
    \item The "Global" tab handles global settings, e.g. the type of integrator and optimizer to use, or whether DyOS should run in a "single shooting" or "multiple shooting" mode.
    
    \item The "Stage" tab is responsible for overall settings regarding a stage, e.g. what kind of eso model is used for the stage. 

    \item The "Integrator" tab manages the parameter input, it is where e.g. the values of parameters can be chosen or a grid for a parameter that changes with time can be created.
    
    \item The "Optimizer" tab manages all the constraints one can set for the different state variables, and it is also the place where one can choose the objective that should be minimized.
\end{itemize}

While the settings in the "Global" are obviously global, all the other settings in the three other tabs are tied to a specific stage, so the content of these tabs will change if the user switches to a different stage. These stages can be added and removed in the box to the right, which we will see later on in the tutorial.


\paragraph{Global Tab}

We start with the "Global" tab,  which contains the general settings for the optimization. For this example, the "Final Integration" checkbox stays checked, also the "Running Mode" does not change from "Single Shooting." Note that upper and lower bound for the "Total End Time" can also be seen, but are not editable in our implementation.

Moving on to the global integrator settings, we change the integrator to "Limex" and the "Integration Order" to "First Forward". The "Integration Options" stay empty for this example, but in theory, the user could enter any key value pair he desires after pressing "Add Option" and typing them into the dialog window that opens up. The window will suggest available integrator options and their data format once these available options have been implemented in DyOS.

Scrolling further down, the user can find the global settings for the optimizer. "Optimizer Type" and "Optimization Mode" do not change in this case, since the user wants to \textit{minimize} the final time, not \textit{maximize} it. "SNOPT" has many different options to choose from, which are shown upon pressing the "Add Option" button. As an example we will add a "major iterations limit" of 200, to put an upper bound on the computation time if the optimization does not converge quickly enough. This can be done by choosing the option in the list to the left, and entering "200" as a desired value. Confirming our choice will add the option to the table.


\paragraph{Stage Tab}

After we've set all options in the global tab, we can now move on to the "Stage" sub tab of the "Input" tab, where the user can start by choosing the format of his desired eso model. Right now, the GUI supports the FMI and JADE formats, and a short explanation about how to import these models is also shown in the tab. The user can proceed by looking for his model file on disk, and load it into the GUI. As already mentioned, we are using the standard "CarFreeFinalTimeTwoObjectives" model, which is compiled alongside the GUI and in the "JADE" eso format. The dynamic library file can be found in the build directory by default. These files have different names according to one's operating system, e.g. the file may be called {\tt carFreeFinalTimeTwoObjectives.dll} (Windows) or \\{\tt libCarFreeFinalTimeTwoObjectives.dylib} (MacOS).


\paragraph{Integrator Tab}

Next up is the interesting part: The setup of integration and optimization options. Since the "Add Stage" button duplicates the stage the user is currently located in, we are going to do integration and optimization setups stage-by-stage. Beginning with the integrator in stage 1, the user can see that the parameters have already been preset. The top parameter is "Duration", which we don't have to change much, except for setting the lower bound to 0 and setting the sensitivity to full. 

Coming up next is the acceleration, "accel". Because a real car does not have an infinite acceleration, we should change the lower bound to -10 (a value for maximum break capacity of a normal car) and the upper bound to 7 (a value for the acceleration for a normal car).

Also, the parameter type is changed to "Profile", because the acceleration will vary across time and not stay constant. The sensitivity is changed to "Full", and a grid has to be added, using the "Add Grid" button. We need the grid because the acceleration should change across time, and DyOS will actually find the optimal values the acceleration should take in all intervals of the grid.

The integrator settings should look something like this: 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Resources/tutorialscreenshots/intstage1.png}
\end{figure}{}

Regarding the grid, we set 5 intervals, while setting the grid type to "Piecewise linear" and setting the initial guess for each interval to 2, by pressing the "Fill Values Column with" button after entering a "2" in the text field next to it. This initial guess will be changed by DyOS to the optimal values later on. We finish off by confirming the grid. Note that the grid is going to change slightly for stage 2, because the car has to travel a longer distance, which means we should give DyOS more points to optimize.

The grid should look something like this: 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Resources/tutorialscreenshots/editgridstage1.png}
\end{figure}{}

"$\alpha$" and "tf" are not needed in the integration settings for this example, so they can be removed by marking them and clicking on the "Delete Selected" button.

\paragraph{Optimizer Tab}

Moving on to the optimizer, the user finds a button in the top of the window where the objective of optimization can be chosen. As the goal is to optimize the time one needs to reach location "B", "ttime" is chosen, which is a variable in this particular eso model that corresponds to the duration. 

Now, new constraints for the optimization can be added. For this example we need one constraint for "velo", a path constraint, with a lower bound of 0 and an upper bound of 13.8 as maximum velocity. We also need a constraint for "dist", an endpoint constraint, which we specify with both bounds being 3600.

"velo" describes the range of velocity the optimizer is allowed to use and "dist" is the reached distance at the end of the stage. Note that for this example the values have to be changed to SI-units, so the velocity changes to $\frac{m}{s}$ with a factor of $\frac{1000}{3600}$. The distance in kilometers changes to meters.

The optimizer for the first stage should look like this: 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Resources/tutorialscreenshots/optstage1.png}
\end{figure}{}

\paragraph{Rest of example}

Next up, the second stage can be added by pushing the "Add Stage" button in the top right. Going back into the integrator tab for stage 2, the grid is changed to 20 intervals, also with the initial value of 2. Staying in stage 2, the optimizer has to be changed, as the maximum velocity for the highway is different, so the upper bound for the velocity changes to 41.7$\frac{m}{s}$ (which is equal to 150 $\frac{km}{h}$). The distance increases by 5800 meters, so we have to add 5800 meters to the overall distance, as we do not want to start in location "A" again. We end up with an overall value of 9400 meters. 

After this the grids in the integrator tab should look like this: 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Resources/tutorialscreenshots/editgridstage2.png}
    \caption{Input Tab - grid settings}
    \label{fig:stage2grid}
\end{figure}{}

Next up is the traffic jam, which stops us from driving at maximum speed for 1 km. We add the next stage by pushing the "Add Stage" button. The possible speed is 80 $\frac{km}{h}$. Values are added as explained before, the distance is added to the overall distance, the velocity receives the upper bound 22.2 $\frac{m}{s}$, the grid can stay the same as in stage 2.  

Adding another stage, we have to change the values by adding 5800 meters of distance and setting the value for velocity to 41.7. The grid stays the same. The overall distance is now 16200 meters.

For the fifth and last stage, in the integrator the grid has to change back to the original grid from stage 1 (5 intervals, "Piecewise linear", Fill Values with 2). In the optimizer, the distance constraint has to be changed to the final reached distance, as at the end of stage 5, location "B" is reached. Also we want to simulate the driver breaking at the end, so a second "velo" constraint with a value of 0 and the "Endpoint" constraint type is added only for the last stage. With finishing this the input is done and ready to be computed.

Stage 5 of the optimizer should look like this: 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Resources/tutorialscreenshots/optstage5.png}
    \caption{Input Tab - finished settings}
    \label{fig:stage1grid}
\end{figure}{}



\subsubsection{Saving and Sharing Input Files}\label{sec:SavingSharing}

After finishing an input, the user may want to save it or even share it at some point. This can easily be done by navigating to the menu and using the "Save" option. Alternatively the common key combination {\tt Ctrl+S} can be used. Note that the user has to be in the Input tab to save the input.

After this the user can choose a name for the file and save it at a wanted location on his computer. Loading a file is very similar, just navigating in the file menu to the "Open" option or pressing {\tt Ctrl+O}.

Supposing the file is shared from a Windows computer to a Mac. Opening up the file will cause a message to be shown, that the eso model used on the Windows computer is not known. This is because the file on the other computer most likely had a different file path, and also most likely a different file extension ({\tt .dylib} instead of {\tt .dll}). The GUI asks the user to link a local version of the model, and the user is only allowed to select the identical model that was in the input originally. Selecting a different model is possible, because we can not check if two models correspond to each other, but the behavior of the GUI will be undefinded in that case, and errors are certain to occur.

Note that this link has to be corrected for all eso models, so the message will show as many times as there are stages with unique eso models.



\subsubsection{Running DyOS}

Now changing to the the "Run" tab, the user can see a tree-diagram on the left side, the window with information from the command line on the right and the "Run Dyos" button right above.

As the button is pretty self explanatory, the first thing to look at should be the tree on the left. It shows all the information from the input, even the inaccessible options that are not yet implemented in the Input Tab, and standard values set by DyOS, as well as every change that is done by the user. A final check or a comparison to a JSON document used as input can be done here. 

The command line window shows the information sent to the users console. Progress messages and other information given by DyOS can be seen here after pressing the "Run Dyos" button.



\subsubsection{Viewing the Output}\label{sec:ViewOutput}

\paragraph{Loading Output Files}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUserEmpty.PNG}
\caption{Empty Output Tab - no Output Object has been loaded yet}
\end{figure}

The user has two ways to load an output into the GUI, one way is to load an already saved output file from a previous run (compare loading of input file \ref{sec:SavingSharing}). The more standard approach would be to run DyOS and switching to the output tab after the solution has been calculated. Once an output object is present, the tables on the left should look similar to this:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUserLoaded.PNG}
\caption{Loaded Output Tab - the Output Object has been loaded}
\end{figure}

\paragraph{Plotting the Output}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUserAll.PNG}
\caption{Output Tab - with all data plotted}\label{sec:OutputUserAll}
\end{figure}

Part of the \lstinline{OutputWidget} are the three columns, which handle different parts of the output. The left column with the data and constraints (Data Settings \ref{sec:DataSettings}) is responsible for selecting what part of the output will be plotted. The middle column is the main graph (Visualizer  \ref{sec:Visualizer}), which plots all the selected data. The right side column (Plot Settings \ref{sec:PlotSettings}) toggles a few general aspects of the plot, and also lists a few general information (stage times) and the legend for the graph.

A separate window is used for the output summary, where the entire output can be viewed, which otherwise is not easily presentable in the space of the main window. More later in Output Summary (\ref{sec:OutputSummary}).

Because the foundation of the GUI are splitters from Qt, the columns can be dynamically resized:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputFullWindowed.PNG}
\caption{Here the output of the included "CarExample" is plotted}
\end{figure}

\paragraph{Output Data Selection}

As you can see in figure \ref{sec:OutputUserAll}, the same axis scaling is used for all graphs. Therefore the user can filter out the data he wants to see in the left column.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUserVelo.PNG}
\caption{Output Tab - with just velocity and acceleration}
\label{fig:CarExampleOutput}
\end{figure}

For our car example, we can see that the results for the given constraints are as expected - to reach the destination in the shortest amount of time, the car has to travel as fast as possible! Here it accelerates as fast as possible to reach the speed limits, and breaks as hard as it can to reach the reduced speed in the traffic jam.

To visualize the three different constraint types we have three different markers:

\paragraph{Output Constraint Markers}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotValues.PNG}
\caption{Clean Output of only the velocity}
\end{figure}

There are three different constraint types for DyOS: Path, Point and Endpoint Constraints.

A Path Constraint sets an upper and lower boundary for a certain variable over an entire stage. This boundary is marked by the corresponding area and can be seen in this example for 5 stages:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotConstraintPath.PNG}
\caption{Constraint Type Path - Area}
\end{figure}

A Point Constraint also enforces a lower and upper boundary, but at a certain point in time. These bounds are visualized with "whiskers", where the concept originally came from box plots.

An Endpoint Constraint is similar to the above Point Constraint, whereas the time for Endpoint constraint is set to the stage end. These bounds are also visualized with "whiskers".

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotConstraintPoint.PNG}
\caption{Constraint Type Point - "Whisker" for two Point Constraints}
\end{figure}

When the upper and lower boundary for a (End-)Point Constraint are the same, the plot draws a cross to show that these two values are exactly the same and not just very close to each other.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotConstraintCross.PNG}
\caption{Constraint Type (End-)Point - Cross}
\end{figure}

\paragraph{Output Plot Settings}\label{sec:PlotSettings}

\begin{figure}[H]
   \begin{minipage}{0.48\textwidth}
     \centering
     \includegraphics[width=1\linewidth]{Resources/Output/OutputGridOff.PNG}
     \caption{Output Grid Off}
   \end{minipage}\hfill
   \begin{minipage}{0.48\textwidth}
     \centering
     \includegraphics[width=1\linewidth]{Resources/Output/OutputGridAndShadingOff.PNG}
     \caption{Output Grid \& shading Off}
   \end{minipage}
\end{figure}

For now we have implemented two general design switches for the graph. One turns the grid on/off, the other the shading of the different stages. Later on, when more plotting tools have been added, the user may want to start saving the plot for future use. These suggestions for future features are also collected in section \ref{sec:FutureOutlook}.

\paragraph{Advanced Output Summary}\label{sec:OutputSummary}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputSummary.PNG}
\caption{The "true" Output Object contents.}\label{sec:OutputSummaryScreen}
\end{figure}

As mentioned before in the Data Selection chapter (\ref{sec:DataSettings}), we have extracted the values from the Output Object where it makes the most sense to plot it.

Looking at the example output in figure \ref{sec:OutputSummaryScreen}, the entire Output Object is listed containing all input settings, calculated solutions and further logs. The user can use this window to check what input settings had been used in a file which has been loaded, or just check if the plot is correct. The \lstinline{advancedOutput} class converts the internal DyOS Output Object into a more user friendly JSON object (see \ref{sec:JSONConversion}) which can be cleanly visualized in a QTreeView.



\subsubsection{Saving and Sharing Output Files}

After running an optimization with DyOS, the procedure to save the ouput file is the same as when saving an input file, only that the Output Tab has to be opened. Please refer to chapter "Saving and Sharing Input Files" (\ref{sec:SavingSharing}).