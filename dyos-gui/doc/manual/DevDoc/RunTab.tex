\subsection{Run Tab}\label{sec:RunTab}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenRunTab}
\caption{Run Tab - After Startup}
\end{figure}

\subsubsection{Overview}

The Run Tab takes the \lstinline{UserInput::Input} object that was configured in the Input Tab and runs DyOS with this input. After DyOS is done, the produced output is automatically sent to the Output Tab.

Let's take a look at all the classes involved in the Run Tab:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/runUML}
\caption{UML for the Run Tab}
\end{figure}

The highest level class in the Run tab is of course the \lstinline{RunWidget} (\ref{sec:RunWidget}), which contains the logic for everything that's visible in the tab. It has a connection to the \lstinline{InputWidget} (\ref{sec:InputWidget}) through a pointer, so that it is able to obtain the current input object.

The execution of DyOS has to take place in another thread, otherwise the GUI would have to freeze. Therefore we have \lstinline{DyosThread} (\ref{sec:DyosThread}), which is a subclass of \lstinline{QThread}. This is where all the action takes place, and the main \lstinline{runDyos(...)} function is actually called.

And lastly we also have the class \lstinline{ThreadLogStream} (\ref{sec:ThreadLogStream}), which is able to send the output of DyOS to a \lstinline{QTextEdit}.



\subsubsection{Overall Functionality – {\tt RunWidget}} \label{sec:RunWidget}

The \lstinline{RunWidget} class is fairly compact, because it does not require a lot of logic. 

In the constructor of the class, new \lstinline{DyosThread} and \lstinline{QJsonModel} objects are created. The thread must be connected to the \lstinline{QTextEdit}, so that it can send its command line output to it. We also have to connect the JSON model to the \lstinline{QTreeView}, since that is where the content of the model should be displayed.

The \lstinline{updateUI()} routine of this class is rather small compared to others, it only has to fetch the current input object from the Input Tab, but also do so in a thread safe way. It then converts the input to a JSON string, using DyOS' own \lstinline{JsonInputFormatter} class, which ensures that this action is forward compatible, as described in \ref{sec:Concept}. The \lstinline{QJsonModel} object can receive this  JSON string, and present the content in the connected \lstinline{QTreeView}. We also expand the top three hierarchy levels of the tree view right away, so that the user does not always have to expand so many tree items.

A nice room for improvement in \lstinline{updateUI()} would be to do all these steps only if the input from the Input Tab and the cached input in the Run Tab are \textit{actually different from each other}, this way the tree view would keep its expanded state when the user switches e.g. to the Output Tab and back. However, if we try to do this, we run into the same problem as with the "unsaved changes" dialog in the Input Tab, which is described in section \ref{sec:UnsavedChangesProblems}. We need all \lstinline{operator=(...)} functions for all input components to actually compare every member variable, and not just some.

The \lstinline{RunWidget::runDyos()} routine is executed when the user presses the "Run DyOS" button, it is one of the most important routines in the GUI. However, since DyOS must run in a separate thread, the \textit{actual} \lstinline{runDyos()} routine is contained in \lstinline{DyosThread::run()}, and because of the way \lstinline{QThread} is implemented, calling \lstinline{start()} on the thread actually executes this \lstinline{run()} routine.

One major limitation of our current implementation is the fact that DyOS can not be interrupted once we set it going. The user has to close the entire GUI and open it back up again, which is not very elegant. More details about this problem and the reasons behind this limitation are described in section \ref{sec:ThreadInterruptProblems}.



\subsubsection{Multithreading – {\tt DyosThread}} \label{sec:DyosThread}

The \lstinline{DyosThread} class is also fairly simple, even though it has an important job. While from the point of view of the GUI, this thread does very little work, if you consider DyOS as a whole, simply \textit{everything} happens in this thread!

For the command line output, the thread is able to connect its own instance of \lstinline{ThreadLogStream} to any other UI element which has a "slot" that accepts \lstinline{QString} objects. In our case, this UI element is \lstinline{QTextEdit}, and the slot is \lstinline{QTextEdit::append(const QString&)}, which will result in DyOS' standard output being displayed in the text field. The instance of \lstinline{ThreadLogStream} is initialized with \lstinline{std::cout}, which is the output stream we want to pipe through.

The \lstinline{run()} function, which overwrites the corresponding function in \lstinline{QThread}, is the part of this class which is actually executed on a separate thread, all the other functions will still be executed on the main thread. In this \lstinline{run()} function, we have to grab the cached input object in the Run Tab, and do so in a thread safe way. We do not want to create a race condition, which may corrupt the input object, therefore we use \lstinline{QMutex} objects to ensure sequential access to the input object.

DyOS then operates on local copies of input and output objects, and upon completion, the output is sent to the Output Tab via the signals and slots mechanism, since it is already thread safe by design.



\subsubsection{Command Line Output – {\tt ThreadLogStream}} \label{sec:ThreadLogStream}

The \lstinline{ThreadLogStream} class is able to send the command line output of DyOS to any UI element which has a "slot" that accepts \lstinline{QString} objects. It does so by replacing the \lstinline{std::streambuf} inside of the \lstinline{std::ostream} it should observe, in our case \lstinline{std::cout}. It can then go ahead and forward all lines of text which are sent to \lstinline{std::cout} to any object which is connected to its \lstinline{sendString(...)} signal.

Unfortunately the output does not yet work for all different types of optimizers, as described in section \ref{sec:CommandOutputProblems}.