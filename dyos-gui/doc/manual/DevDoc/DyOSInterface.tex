\subsection{DyOS Interface}

As we described in the "Concept" section (\ref{sec:Concept}), one of our main design principles was to use DyOS' internal classes and functions for areas where it makes sense. We will try to sum up the overlap between the GUI code and DyOS main code in this subsection.



\subsubsection{Core Functionality}

Of course, a couple of components of DyOS are absolutely \textit{vital} for the GUI, and it comes almost without saying that the GUI makes heavy use of them. These components include:

\begin{itemize}
    \item Anything in the {\tt UserInput} and {\tt UserOutput} namespaces, e.g. the \lstinline{UserInput::Input} class and all of its subcomponents.
    
    \item The main \lstinline{UserOutput::Output runDyos(const struct UserInput::Input &input);} method.
\end{itemize}

The components for the input and output are essentially used everywhere, and e.g. changing the existing structure of the input object obviously breaks the GUI. The \lstinline{runDyos(...)} function is located in the \lstinline{DyosThread} class, since DyOS must be called from a separate thread (see section \ref{sec:DyosThread}).



\subsubsection{JSON and XML Conversion Routines}\label{sec:JSONConversion}

We also make heavy use of several text conversion routines, which turn input or output objects into a JSON or XML text format and back. These routines include:

\begin{itemize}
    \item \lstinline{convertUserInputFromXmlJson(...)}, \lstinline{convertUserOutputFromXmlJson(...)}, \lstinline{convertUserInputToXmlJson(...)} and \lstinline{convertUserOutputToXmlJson(...)} ... all of which are used in our \lstinline{GUI_IO} class (see section \ref{sec:InputOutput}). These functions take an input/output object as well as a file directory as arguments, so they will create/read files on disk directly.
    
    \item The \lstinline{JsonInputFormatter} class, which produces a JSON string from an input object, and the \lstinline{JsonFormatter} class, which does the same for an output object.
\end{itemize}

While the most obvious use case for these function is of course the saving and loading of text files, we also take advantage of them for the "input summary" in the Run Tab (\ref{sec:RunWidget}) and the "advanced output" window in the Output Tab (\ref{sec:OutputWidget}). In both of these cases we are able to take the produced JSON string, which e.g. contains \textit{every} input option, not just the ones we designed the GUI around, and display them in a view which accepts \textit{any general} JSON string as data (see \ref{sec:QJsonModel}). The advantages that come from this approach were already discussed in the concept section (\ref{sec:Concept}).



\subsubsection{Eso Model Input}\label{sec:EsoPointer}

It made a lot of sense to use the content of the loaded eso model to intelligently prefill some information about parameters and other variables in the input tab. If the user specifies such a model, and if it is valid, the information inside the model can be used, mostly in the Integrator and Optimizer sub tabs of the Input tab.

Examples of such information are the names of the parameters and variables, as well as their (initial) values. It also gives you a distinction between "parameters", "algebraic variables" and "differential variables", all of which can be treated differently in the GUI.

The system to read these models and use its provided information are already present inside the \lstinline{GenericEso} submodule of DyOS, which has the ability to accept an \lstinline{UserInput::EsoInput} object and return all kinds of useful information about the mathematical model through a \lstinline{GenericEso::Ptr} pointer.

Our implementation for obtaining the eso pointer can be found in the {\tt utility.h} file, since the functionality is needed in several places of the GUI, but does not belong to a specific widget. The function \\
\lstinline{GenericEso::Ptr getEsoPtr(const UserInput::EsoInput& eso, ...)} \\
does everything that's necessary.

Often times we will also create the pointer without doing anything with it, just to check whether e.g. the model string/path in an \lstinline{EsoInput} is valid.

Places which use this eso pointer include:

\begin{itemize}
    \item \lstinline{ResolveStageDialog}, \lstinline{StageTabWidget}: To check whether the current eso model is valid.
    \item \lstinline{StageTabWidget}: To prefill some 
    \item \lstinline{IntegratorTabWidget}: To display all the valid parameters and differential variables, for which \lstinline{ParameterInput} objects can be added to the input. Also to get the parameter's values, and the variable's initial values set in the model.
    \item \lstinline{OptimizerTabWidget}: To display all the state variables, for which \lstinline{ConstraintInput} objects can be added to the input.
\end{itemize}



\subsubsection{Integrator and Optimizer Options}

One advantage a GUI has over a standard command line program is the amount of information it can provide to the user. In the standard version of DyOS, and for certain parts of the input, the user has to essentially know a few strings \textit{by heart} to set options, not just their exact spelling, but also whether the keywords are written in upper or lower case.

For the global integrator and optimizer options, the user was previously able to enter any key-value pair of strings he desired, and only if the key was a valid option that DyOS is looking for would the option take an effect. The user also did not receive any direct feedback on whether he has entered the data for the chosen key in the correct format, e.g. he did not receive any warning if he entered "-10" for an option that only accepted positive integers.

We implemented a class \lstinline{AddOptionDialog}, which is able to present the user with the same keys that DyOS may be looking for, and also checks the data for its correct format. However, these options are merely suggestions, and the user is still able to input whatever she likes, because we don't want the GUI to be too restrictive e.g. when developers are using it to test out a new integrator option.

To suggest the options and their data format, we currently only make use of the \lstinline{OptimizerOptions} class and its specialised subclasses, which contain the member \lstinline{validOptions} and \lstinline{enum NumberType}. Similar classes for the integrator options are not available in DyOS yet, but the AVT.SVT should consider implementing them, because it would free the way for more options that are presented in the GUI. Also, not all optimizers are supported with their options, therefore those should be added as well.
