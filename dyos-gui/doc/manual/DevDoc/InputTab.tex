\subsection{Input Tab}\label{sec:InputTab}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenInputTab}
\caption{The Input Tab after startup}
\end{figure}

\subsubsection{Overview}

The Input Tab manages a \lstinline{UserInput::Input} Object, such that it can be freshly created, saved, loaded, checked for errors, viewed and edited.

Let's take a look at all the classes involved in the Input tab:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/inputUML}
\caption{Class diagram for the Input Tab}
\end{figure}

The highest level class for the Input tab is of course the \lstinline{InputWidget} (\ref{sec:InputWidget}), as it is the home of the \lstinline{UserInput::Input} object that is currently loaded and manipulated in the GUI. It is this input object that will later be passed on to the DyOS process to start the calculation.

Part of the \lstinline{InputWidget} are four sub-tabs, which handle different parts of the input. The \lstinline{GlobalTabWidget} (\ref{sec:GlobalTabWidget}) is responsible for global settings, e.g. the global integrator and optimizer settings. The other three sub-tabs each handle settings which are tied to one of the different stages, therefore their appearance changes when the user selects a different stage on the right. The \lstinline{StageTabWidget} (\ref{sec:StageTabWidget}) is responsible for overall settings for the stage, e.g. the eso model settings. The \lstinline{IntegratorTabWidget} (\ref{sec:IntegratorTabWidget}) is responsible for the parameter input, and the \lstinline{OptimizerTabWidget} (\ref{sec:OptimizerTabWidget}) is responsible for the constraint input. This 1:1 correspondence between the sub-tabs and parts of the \lstinline{UserInput::Input} object can be seen in this diagram:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/inputUML2}
\caption{Correspondence between GUI and DyOS objects}
\end{figure}

Some of the functionality of the sub-tabs overlaps, therefore we created a superclass \lstinline{GenericTabWidget}, which e.g. handles the connection to the input object, as well as some of the hover explanation functionality.

A variety of modal dialog windows are used by the different sub-tabs, whenever the user has to modify data which is not easily presentable in the space of the main window. Examples of this include the \lstinline{AddOptionDialog} (\ref{sec:AddOptionDialog}) for picking integrator or optimizer options, or the \lstinline{ParameterGridDialog} (\ref{sec:ParameterGridDialog}) for editing a parameter grid.

The class \lstinline{InputExamples} contains a few example inputs, which can be loaded into the GUI using just a keyboard shortcut (e.g. {\tt Strg+1} for the CarExample) or using the "Examples" menu in the menu bar.



\subsubsection{Overall Functionality – {\tt InputWidget}}\label{sec:InputWidget}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenInputWidget}
\end{figure}

The \lstinline{InputWidget} class is the home of the \lstinline{UserInput::Input} object, which is the most important model object in our GUI, and also the beating heart of the Input Tab. Using this class, the input object can be manipulated, and a few special actions can be performed on the input. It also is responsible for creating and deleting stages in the input.

Quite a few methods of the class are used to manage the input object, these functions include

\lstinline{void setInput(const UserInput::Input &newInput);} \\
\lstinline{const UserInput::Input& getInput(...);} \\
\lstinline{void resetInput(...);} \\
\lstinline{void checkInput(const UserInput::Input &inputToCheck);}

... and their implementation is very straightforward. The purpose of these subtly different methods is described in the Doxygen documentation.

We also have a convenience method, \lstinline{UserInput::StageInput& accessStage()}, which is used by most sub tabs to access the currently visible stage to read and write data to it. To access the entire input, which is e.g. necessary in the global tab, the method \lstinline{UserInput::Input& InputWidget::accessInput()} can be used.

One thing to keep in mind is that \lstinline{resetInput(...)} also adds one empty stage to the new input, since our GUI can not cope with input objects with zero stages. The reason behind this is described in more detail in section \ref{sec:AssertProblems}. The method will also reset some of the "meta" parameters, e.g. the currently displayed stage index, since it also has to be set back to zero once a fresh input is created.

We also have a working implementation of an "unsaved changes" warning dialog, which relies on remembering the last state in which the input was definitely saved, and then comparing the current input to this "known to be already saved" input (\lstinline{lastSavedInput}). This feature does not always work as expected, however the cause for this lies outside of our control, as described in \ref{sec:UnsavedChangesProblems}. 

The implementation of \lstinline{void checkInput(const UserInput::Input &)} has to account for the fact that DyOS may suddenly crash when its internal methods are used, due to a few \lstinline{assert(...)} statements. This problem is described in more detail in section \ref{sec:AssertProblems}.

One rather complicated and technical mechanism is the hover explanation text, which is displayed in the side bar of the Input Tab. This mechanism is described in section \ref{sec:GenericTabWidget}.



\subsubsection{Common Aspects of Sub Tabs – {\tt GenericTabWidget}}\label{sec:GenericTabWidget}

A few members and functions would be identical in all the sub tabs of the Input Tab, therefore we have created a common superclass for them.

First of all, any sub tab needs a way to access the current input object, which is always stored in the \lstinline{InputWidget} (see \ref{sec:InputWidget}). This is done through a pointer, \lstinline{inputWidget}, which will first point to null after a \lstinline{GenericTabWidget} got created. The implementation assumes that the pointer is connected \textit{immediately} after the widget is created, in our case this is done in the constructor of \lstinline{InputWidget}. At first glance this may seem like a poor implementation, because the behaviour of the sub tabs is undefined in this brief moment before \lstinline{void setParentPointer(...)} is called. You may ask yourself why we didn't just add the parent pointer as an argument in the constructor of \lstinline{GenericTabWidget}. You can see this "flaw" in a few other places of the GUI as well, but the reason behind it, is that there simply is no place \textit{in the code} where these constructors are actually called! A lot of widgets are created in the automatically generated files, e.g. {\tt ui\_globaltabwidget.h}, and there is no way (known to us) to pass arguments to the constructors of these widgets.

One rather complicated and technical mechanism is the hover explanation text, which is displayed in the side bar of the Input Tab. This feature is implemented using a so-called "event filter", which can be used to do something whenever a certain "event" is sent by a UI element. Any subclass of \lstinline{QObject} is able to serve as an event filter, and the mechanism is further described here: \url{https://doc.qt.io/archives/qt-4.8/eventsandfilters.html} \\
In our case, the \lstinline{InputWidget} class itself acts as the event filter. It will intercept any \lstinline{QEvent::Enter} and \lstinline{QEvent::Leave} events from any object that installs the \lstinline{InputWidget} as one of its event filters. These events are automatically emitted by any Qt element once the mouse cursor enters or leaves their frame on the screen. The \lstinline{InputWidget} can then inspect the sender of such an event, and display its "What's This" text, which is part of any subclass of \lstinline{QWidget}, in the sidebar. The implemetation for this can be seen in the \lstinline{bool InputWidget::eventFilter(...)} method. 

In order for this to work, all UI elements which should display a hover text in the sidebar have to conform to two things: 

\begin{enumerate}
    \item The elements have to be a subclass of \lstinline{QWidget}, and they should have a "What's This" text containing the hover explanation. This text can be added in Qt Creator for any \textit{static} UI element, and in the code using the \lstinline{setWhatsThis(...)} member function of \lstinline{QWidget}. 

    \item At some point during the runtime of the GUI, these objects have to install the \lstinline{InputWidget} as their event filter. For convenience, this step is done automatically for all \textit{static} UI elements (defined in {\tt .ui} files) which have a "What's This" text. This covers all explanations which are entered in Qt Creator, and the mechanism behind this can be seen in the function \lstinline{void installHoverFilterOnElements()}. \\
    For \textit{dynamic} UI elements (created programmatically), this step has to be done \textit{manually!} Either by calling the member function \\\lstinline{installEventFilter(...)} of the element directly, and providing a pointer to the \lstinline{InputWidget}, or by just calling \\\lstinline{void installHoverFilterOnElements()} again (after the "What's This" text is already set!), which will do the installation of the event filter again for \textit{all} child widgets.
\end{enumerate}



\subsubsection{Global Settings – {\tt GlobalTabWidget}}\label{sec:GlobalTabWidget}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenGlobalTabWidget}
\end{figure}

The Global Tab contains settings which have a global effect, which means they do not affect just one single stage, but all of them. Most settings in the tab are realized just by static UI elements, which are manipulated in Qt Creator, but which also have to be connected through the code in order to work as expected.

Let's take the "Running Mode" \lstinline{ComboBox} as an example. We use this UI element to directly control the value of \lstinline{Input::runningMode}, which can have any of the values of the \lstinline{enum RunningMode}. In order for our \lstinline{ComboBox} to work, it is \textbf{essential} that it contains \textbf{all} enum values as entries, and also \textit{in the correct order}.

In order for the box to display the correct value of \lstinline{Input::runningMode}, we have to adjust the displayed index every time \lstinline{updateUI(...)} is executed. We do this by adding the following line to \lstinline{updateUI(...)}: \\
\lstinline{ui->runningModeComboBox->setCurrentIndex(} \\
\lstinline{inputWidget->accessInput().runningMode);} \\
This will ensure that the correct entry of the box is displayed.

Next we have to make sure that when the user changes the index in the box, the value in the input is changed. This is done by adding the function \\\lstinline{void on_runningModeComboBox_currentIndexChanged(...)} to the class, which is automatically called every time the index of the box is changed. In this function we simply make the desired change to the input object, in our case like this: \\
\lstinline{inputWidget->accessInput().runningMode} \\
\lstinline{= UserInput::Input::RunningMode(index);}

\paragraph{Option pickers}\label{sec:AddOptionDialog}

In this tab, the user can also add specified options to the integrator/optimizer (like absolute tolerance, major iterations limit ...) using the corresponding add options picker.

Clicking the add option button will open the option picker dialog window, that contains a list widget, a key text box and a value text box. Some options don't need a value, that is why the user has to provide at least the key to be able to add the option the options map of the input object.

Some of the optimizer types like SNOPT and NPSOL have a predefined map of available options and the expected value type \lstinline{std::map<std::string, std::vector<NumberType> > validOptions}, we use this to update the dialog window content using the \lstinline{updateSelectOptionWindow(...)} function, so the user can easily choose one of them. Choosing an option from the list will show a text explaining the selected key-value type (integer, double ...) and the user will be forced to type a value that matches the required type or it will be deleted with the \lstinline{on_valueLineEdit_editingFinished()} function.

Accepting the picker dialog window will call the \lstinline{addOption(std::string key,std::string value,std::map<std::string, std::string> & map)} with the inserted parameters. This function will first test if the typed key already exist in the input object integrator/optimizer map, notify the user and wait for confirmation before overriding the option, otherwise it will be added directly using \lstinline|map.insert({ key,value })|.



\subsubsection{Stage and Eso Model Settings – {\tt StageTabWidget}}\label{sec:StageTabWidget}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenStageTabWidet}
\end{figure}

In the Stage Tab, the overall settings which are relevant for a stage can be found. Most of the stage settings concern the eso model, but there are also some other settings for mapping the different states to each other between stages. Note that our GUI only truly supports "full state mapping", and we enforce this by setting the mapping automatically in the background (see \lstinline{InputWidget::setCorrectMapping()}), leaving some UI elements greyed out (and not functional). This was part of the requirements given to us, otherwise we may have implemented some more detailed mapping options. In the current implementation however, some assumptions we made, particularly in the Output Tab, will break down, for example if one state with a specific name is not present in \textit{all} stages of an output.

The supported eso formats can be chosen in a \lstinline{ComboBox}, and an explanation on how to correctly load an eso model of the chosen format will be displayed. Some eso formats expect a folder to be chosen (e.g. FMU), others expect a file (e.g. JADE). The last used directories for each eso format are also kept in the application settings (using the \lstinline{QSettings} class), which means the GUI can suggest the last used directory if the user wants to open another eso model from disk. 

This is one of the very few areas in the Input Tab where the user is not allowed to manipulate the value inside the input object \textit{directly!} The user can not manipulate the model string, because parts of the GUI rely on this string to always be pointing to a \textit{valid} eso model, otherwise the Integrator and Optimizer Tabs (\ref{sec:IntegratorTabWidget} and \ref{sec:OptimizerTabWidget}) will not necessarily work as expected. For this reason, the user has to "load" the model into the GUI, which just means the GUI will \textit{confirm} that the model is valid before the string inside the input object is actually overwritten. Only if a valid eso model is present, the \lstinline{InputWidget} class will enable the two last entries in the tab bar, so the user can access them.

In order to perform this step and "load" a new model, the user has to specify an eso model in the text field, in the form which is required by the format. In case of a \lstinline{JADE} model for example, the user is also allowed to just type in the name of the dynamic library file (even without file extension), so the user could write e.g. "CarFreeFinalTimeTwoObjectives" into the text field, since that model is also compiled as a standard example in the GUI.

Once the "Load Model" button is pressed, a variety of things happen:

First of all, the string which is now in the text field is checked for validity. This is done through the \lstinline{GenericEso} interface, which is also mentioned in section \ref{sec:EsoPointer}.

Since the user will change the underlying mathematical model for this input stage, including possibly different variable names and so on, the settings which may still be present in the Integrator and Optimizer Tabs are rendered useless. Therefore, we must overwrite all settings which were made in these tabs. We did not implement a system which could intelligently detect whether the new model contains the exact same variable names, and even then it may not be desired to keep these settings, since the variable names are not necessarily a good indicator for the variable's \textit{meaning.} Since the user may not be aware that his changes are about to be lost forever when pressing the button, we issue a warning beforehand.

Next, the GUI will prefill some of the information from the new eso model in the Integrator Tab, mostly the "parameters" and the "duration". As a last step, the signal \lstinline{updateGlobalUI()} is emitted, which should always be connected to the \lstinline{updateUI()} routine of the \lstinline{InputWidget} (or perhaps \lstinline{MainWindow}, it doesn't make a difference in this case). This step is necessary, because the last two entries in the sub tab bar may have been greyed out before, but will become enabled now once a valid eso model is present. The sub tab bar is managed by the \lstinline{InputWidget} class.


\paragraph{Stage resolving – {\tt ResolveStageDialog}}\label{sec:ResolveStageDialog}

A problem concerning the eso model arises when the user tries to open an input text file. If this text file was created by another user or colleague, who uses a different computer with different file paths, a once valid eso model can suddenly turn invalid, because the user may not even have the same eso model available on his disk. Also, while user A may have {\tt CarFreeFinalTimeTwoObjectives.dll} on his Windows machine, user B may have a \\{\tt libCarFreeFinalTimeTwoObjectives.dylib} on his Mac.

To tackle this problem and encourage the sharing of input files, we designed the \lstinline{ResolveStageDialog} class, which will confront the user with this problem and give him the chance to fix the eso model path according to what works on his machine. If the user fails to provide a valid eso model, the input will not be opened and the user ends up where she started.

This class is mentioned here, since the process behind this solution, and also the implementation, are very similar to the eso model loading in the \lstinline{StageTabWidget}. It even uses the \lstinline{static} method \lstinline{StageTabWidget::chooseModelPath(...)}.



\subsubsection{Parameter Settings – {\tt IntegratorTabWidget}}\label{sec:IntegratorTabWidget}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenIntegratorTabWidget}
\end{figure}

First of all, the integrator tab can be accessed only when a model is loaded into the stage tab. In this tab, there is one spin box, one table that contains all the parameters which are relevant to the currently selected stage number (top-right) and three buttons.

Before inserting elements in the table, we create a local identification map \lstinline{std::map<string, string> identificationMap} using the eso model to obtain all possible parameter names of the currently loaded model and we classify them in 3 types (algebraic variables, differential variables, and parameters) because each one has its unique insertion and view mode in the table.

The insertion of parameters in the table is based on the content of the input object created previously in the stage tab (contains the current stage integrator parameters). So basically, we insert the duration row first, then we just read the names of the parameters one by one, we find out using \lstinline{identificationMap} what type this parameter has and we insert its values into the table using the corresponding parameter type insertion mode.

When inserting integrator elements, we use the same \lstinline{insertNewParameterRow(int index,int insertionLocation,bool editable)} function to fill the first columns (Name, Value, Lower Bound and Upper Bound) and the same function to create the parameter sensitivity drop-down menu widget and inserted in the sensitivity column \lstinline{createTypeComboBox(int row, int column, QStringList ParameterTypeList, int CurrentIndex)} but for the rest of the columns, each parameter insertion mode has some unique modification.

When inserting the duration row, we set the type and parameter type text to "Duration" , the differential variable row insertion has "Differential" as a predefined type and "INITIAL" as parameter type, the algebraic variable row insertion has "Algebraic" as a predefined type and "INITIAL" as parameter type. All these columns are set to be greyed out using \lstinline{WidgetItem->setFlags(WidgetItem->flags() & (~Qt::ItemIsEnabled))} and they are not editable.

When inserting a parameter row, we set the type to "parameter" and we insert in the parameter type column a drop-down menu widget that contains as elements: INITIAL (greyed out), TIME-INVARIANT, PROFILE, DURATION (greyed out).

Grid is the last column of the table. It is only needed by parameters, that is why during parameter row insertion we make one more test to check the parameter type:
\begin{itemize}
\item if parameter Type = TIME-INVARIANT, we create a greyed out button and insert it into the grid column.
\item if parameter Type = PROFILE, we create a button and insert it into the grid column and we change the value column to blank without changing the parameter value within the input object (visually empty only).
\end{itemize}

While reading the eso model we create a local map \lstinline{std::map<string, UserInput::ParameterInput> variablesMap} that contains the names of the variables as key and \lstinline{UserInput::ParameterInput} with default values, because there are 2 different types of variables(Algebraic and Differential), this will make it easier when we want to add variables to the table later. For the parameters we make a local list \lstinline{QStringList parametersNamesList} that contains all the names of parameters. After loading the eso model input and while inserting elements in the table we make another local list that contains all inserted elements names(QStringList existingParameters \& QStringList existingVariables), these lists will be used later to check if a parameter/variable is already inserted in the table.

\label{sec:ParameterGridDialog}

When clicking on the add Grid button a signal that is connected to the  \\* \lstinline{gridPushButtonClicked()} function will occur. This function will open a new window that contains all the necessary grid parameters in the left (if the parameter has already a grid these text boxes will be updated to match the grid values of the parameter using \lstinline{updateGridEditor(currentGridValuesVector, currenttimePointsVector, gridTypeIndex, pcResolutionValue, numIntervalsValue)} function otherways they will be blank). To the right there is a plot based on the inserted grid time points and values to the right. Closing the grid editor window will do nothing but accepting it will insert the typed values to the grid of the corresponding parameter using \lstinline{updateParameterGridInput(valuesVector, timePointsVector, gridType, pcResolution, numIntervals,} \\* \lstinline{parameterNumber, 0)} function.

Both of the add new parameter/variable buttons' click signals open the same dialog window with a different list of names, that need to be passed into the \lstinline{updateSelectNameWindow(parametersNamesList or variablesNamesList)} function to update the window content.

\begin{itemize}
\item  For each selected name from the add variable window, we use the previously created \lstinline{variablesMap} to get the corresponding \lstinline{UserInput::ParameterInput}.
\item  For each selected name from the add parameter window, we create the (UserInput::ParameterInput) based on the default parameters.
\end{itemize}

After generating every \lstinline{UserInput::ParameterInput} we insert it into the input object and at the end when we are done inserting all the new parameters/variables, we call the \lstinline{updateUI()} function to do the table insertion again.

When clicking on the delete button a signal that is connected to the \\* \lstinline{on_deletePushButton_clicked()} slot will occur. This function will collect using qt predefined method \lstinline{QModelIndexList select = ui->parameterTable->selectionModel()->selectedIndexes()} all the selected table elements indexes ,so we just need to delete them from tthe input object. After deleting all the parameters/variables, we call the \lstinline{updateUI()} function.

Changing anything in the table will occur a signal \lstinline{cellChanged(int row, int column)} that is connected to the \lstinline{changeInput(int row, int column)} function. Because the user can change table rows order, this will cause the parameter index in the table to be different from the parameter location number in the input object, that is why we use the row and column to get the parameter location number in the input object \lstinline{ui->parameterTable->item(row, column)->data(Qt::UserRole).toInt()} and based on these pieces of information we can update the corresponding parameter with the new typed value.

Changing the drop-down menu index (in parameter type/sensitivity type) will occur a signal which is connected to \lstinline{comboBoxIndexChanged(int index)} and each comboBox widget has its parameter location number in the input object as a property \lstinline{combo->property("row").toInt()},so using this we can get and update the corresponding parameter value in the input object. In the other hand, parameter type drop-down menu changes will also call the \lstinline{updateUI()} function in the end to make the necessary changes like adding the grid button.

The plot grid resolution spin box changes occur a signal connected to the \\* \lstinline{on_plotGridResolutionSpinBox_valueChanged(int value)} function which update the plot grid resolution value in the input object with the new typed one.



\subsubsection{Constraint Settings – {\tt OptimizerTabWidget}}\label{sec:OptimizerTabWidget}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/screenOptimizerTabWidget}
\end{figure}

First of all the optimizer tab can be accessed only when a model is loaded into the stage tab. In this tab, there is a one not editable text box that shows the objective name, one table that contains all the constraints and three buttons. Objective and constraints are relevant to the currently selected stage number(top-right).

Before inserting elements in the table, we create a local list \lstinline{QStringList  constraintsNamesList} using the eso model to obtain all possible constraints names of the currently loaded model.

The insertion of constraints in the table is based on the content of the input object created previously in the stage tab (contains the current stage optimizer parameters). So basically, we just read the \lstinline{UserInput::ConstraintInput} one by one and we insert its values into the corresponding table column only in time point column insertion, we make an extra switch test based on the current constraint type:
\begin{itemize}
\item \lstinline{type = PATH} we make time point column visually blank.
\item \lstinline{type = POINT} we insert the constraint input time point value in the table.
\item \lstinline{type = ENDPOINT} we set column value to 1 without changing the constraint input time point value (only visual 1).
\end{itemize}

Both of add select objective name and add constraint buttons click signal are connected to two different functions, that open the same dialog window and update it with the \lstinline{constraintsNamesList} created previously, the only difference:
\begin{itemize}
\item in \lstinline{void on_selectObjectiveName_clicked()} function we make the user only able to select one name ,that will be showen in the text box \lstinline{ui->namelineEdit->setText(QString::fromStdString(selectedConstraintName[0]))}and we update the input objective name with the new one \lstinline{optimizer.objective.name = selectedConstraintName[0]}
\item in \lstinline{void on_addConstraintPushButton_clicked()} we get all selected names and we create for each one a \lstinline{UserInput::ConstraintInput} with default values and we push it to the input optimizer constraints vector.
\end{itemize}

When clicking on the delete button a signal that is connected to the \\* \lstinline{on_deletePushButton_clicked()} slot will occur.This function will collect using qt predefined method \lstinline{QModelIndexList select = ui->parameterTable->selectionModel()->selectedIndexes()} all the selected table elements indexes, so we just need to delete them.

After inserting/deleting all the selected constraints into/from the input object or changing a constraint type from the drop-down menu an \lstinline{updateUI()} function will be called, to do the table insertion again and make the necessary adjustments.