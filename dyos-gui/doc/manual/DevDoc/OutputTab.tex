\subsection{Output Tab}\label{sec:OutputTab}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputOverview.PNG}
\caption{Overview of the Output Tab - output of the included "CarExample"}
\end{figure}

\subsubsection{Overview}

The Output Tab is capable of displaying the content of a single \lstinline{UserOutput::Output} object, that could have either been a direct result of a simulation run in the Run Tab, or loaded into the GUI from a text file. The output object can not be manipulated by the tab, it is only read.

Let's take a look at all the classes involved in the Output tab:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUML.png}
\caption{Overview of the Output UML}
\end{figure}

The highest level class for the Output tab is of course the \lstinline{OutputWidget} (\ref{sec:OutputWidget}), as it is the home of the \lstinline{UserOutput::Output} object that is currently loaded in the GUI. This class focuses on the UI elements which are visible in the tab.

The class \lstinline{Visualizer} contains all the needed functionality to generate all elements visible in the chart. More later in (\ref{sec:Visualizer}).

And lastly, the \lstinline{AdvancedOutput} class is used to show a small dialog window, which contains every aspect of the output object in its JSON format. This makes use of the JsonFormatter provided by DyOS.

In chapter \ref{sec:ViewOutput} already explained how the Output Tab has been designed. The main three dynamic columns have been created by dividing the widget into a horizontal layout which uses a splitter. See the screenshot from Qt Creator, on the right hand side you can see the structure of the different layouts.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/QTOutputWidget.PNG}
\caption{Screenshot from QTCreator}
\end{figure}

To add the main plotting area in the middle, a new layout has to be added in the runtime, so that the plot can be refreshed once a new selection has been made. Here a extract of the class constructor:

First we create a empty chart, where the chart widget has position \#1 (horizontal) in the splitter:\\
\lstinline{ui->splitter->insertWidget(1,new QChartView());}\\
\lstinline{updateUI();}\\
In the end we update the UI after the initialisation.

\subsubsection{Overall Functionality – {\tt OutputWidget}}\label{sec:OutputWidget}

As described in chapter \ref{sec:Concept}, some of these operations are implemented using DyOS' very own methods, so actions such as saving, loading, creating and checking the input are always up to date with the newest Input options for DyOS, \textbf{as long as the IO routines are kept up to date!}

The data which can be plotted is stored in the \lstinline{UserOutput::Output} object which can be seen in this diagram:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUML2.png}
\caption{Overview of the Output UML}\label{sec:OutputUML2}
\end{figure}

Most of the operations are part of the \lstinline{OutputWidget} class, since it makes sense to implement these functions close to the home of the output object. These functions include:

\lstinline{void setOutput(const UserOutput::Output &newOutput);} \\
\lstinline{UserOutput::Output getOutput();} \\

The UI elements of the \lstinline{OutputWidget} class call following functions for the data selection. To check item from data list to updateUI and createChart:

\lstinline{void on_outputListWidgetState_itemChanged(QListWidgetItem *item);} \\
\lstinline{void on_outputListWidgetParameter_itemChanged(QListWidgetItem *item);}\\
\lstinline{void on_outputListWidgetConstraint_itemChanged(QListWidgetItem *item);}\\

To (de-)select all checkboxes in corresponding listView:

\lstinline{void on_selectAllStates_stateChanged();} \\
\lstinline{void on_selectAllParameters_stateChanged();} \\
\lstinline{void on_selectAllConstraints_stateChanged();} \\

And these are supporting functions called for refreshing the graph:

\lstinline{void updateLegendState(int i);} \\
\lstinline{void updateLegendParam(int i);} \\
\lstinline{void updateLegendConst(int i);} \\
\lstinline{void updateLegendObjec(int i);} \\

A bit different is this function, which writes the time values (calculation time, stage end times) on the right side of the GUI:

\lstinline{void updateOverviewTime();}

To view the entire Output Object which also includes all the input settings we have included a separate window, which can be opened with:

\lstinline{void on_buttonAdvanced_clicked();} \\

Also the the toggle buttons for a few global functions for the graph are located here:

\lstinline{void on_buttonShowGrid_clicked();} \\
\lstinline{void on_buttonShowStageShader_clicked();}

When loading the output data and listing it in the three separate scroll boxes QLists and std::vectors are used to save the pointers to each label and the corresponding pointer to the exact data.
This process relies heavily on the \lstinline{Visualizer} class, where it makes the most sense to store and manipulate most the lists.
Only a few lists are directly stored on the \lstinline{OutputWidget} class:

\lstinline{std::vector<std::string> listLegendEntries;} \\
\lstinline{QList<QWidget*> legendLabels;} \\
\lstinline{QList<QWidget*> timeLabels;}

Because the data which can be accessed through these lists is directly used and shown in the main Output Window.

Part of the \lstinline{OutputWidget} class is a \lstinline{Visualizer vis} object, which is used as a constructor the the graph. More will be explained in Chapter \ref{sec:Visualizer} Visualizer.

Now going into detail how the most important functions work:

\paragraph{OutputWidget::UpdateUI()}

Like in every main widget the output widget also has the \lstinline{updateUI()} routine:

\begin{verbatim}
void OutputWidget::updateUI()
{
    // initializing phase
    vis.SetOutput(output);        // update ouput in visualizer class
    listLegendEntries.clear();    // clear custom legend
    QChart *chart = new QChart(); // make a brand new chart

    // clear entries in custom legend
    while (legendLabels.size() > 0){  [...]  }
\end{verbatim}

For each data type the updateUI() routine has to iterate through corresponding lists to create the entries and save the pointers. If a item has been checked then the corresponding function is called to refresh the chart with the data from the state, parameter, or constraint box.

\begin{verbatim}
    // loop over entire list of states
    for (int i = 0; i < ui->outputListWidgetState->count(); i++)
    {
        // look at one item at a time
        QListWidgetItem *item = ui->outputListWidgetState->item(i);
        // check if current item is checked, otherwise ignore it
        if (item->checkState() == Qt::Checked)
        {
            chart = vis.createChartState(chart, i);
            updateLegendState(i);
        }
    }

    // loop over entire list of parameters
    [...]
    // loop over entire list of constraints
    [...]
\end{verbatim}

It is important to use our own custom legend and not the QCharts standard one, because otherwise the legend will be far too cluttered. The standard legend adds an entry for each selected variable, even if they are identical, furthermore our markers are made up of multiple smaller line plots, which all would be listed as well.

\begin{verbatim}
    chart->legend()->setVisible(false);
\end{verbatim}

Afterwards the middle column with the plot needs to be refreshed. The old chart widget needs to be deleted to avoid memory leaks (QT documentation explicitly says that the programmer has to do it) and a new one inserted at the same location. Attention chart widget has position \#1 (horizontal) in the splitter:

\#0 layoutData(with outputListWidgetState, outputListWidgetParameter, outputListWidgetConstraint)\\
\#1 is the chart\\
\#2 are the layout tools)

\begin{verbatim}
    // replace old chart
    QChartView *chartView = (QChartView *)ui->splitter->widget(1);
    if (!chartView) return;
    QChart *oldChart = chartView->chart();
    chartView->setChart(chart);

    // delete old chart to avoid memory leaks
    if (oldChart) delete oldChart;
}
\end{verbatim}



\paragraph{OutputWidget::setOutput(...)}

\lstinline{setOutput()} changes the current output displayed in the output widget. The three lists (states, parameters, constraints) are populated one after another. Spacers with the stage numbering are added, and each item gets labeled with further information. For each stage the single objective is appended to the constraint list. C++ maps are used to save the stage and index for each variable.

How the mapping works: (all maps are saved in the visualizer object vis) \\
\lstinline{itemToState[0]} is a stage label, leave it empty \\
\lstinline{itemToState[1] = std::make_pair(0,0);} remember that the item with index 1 in the list belongs to stage 0 state 0. \\
\lstinline{itemToState[2] = std::make_pair(0,1);} remember that the item with index 2 in the list belongs to stage 0 state 1. \\
\lstinline{itemToState[3] = std::make_pair(0,2);} remember that the item with index 3 in the list belongs to stage 0 state 2. \\
\lstinline{itemToState[4] = std::make_pair(0,3);} remember that the item with index 4 in the list belongs to stage 0 state 3. \\
\lstinline{itemToState[5]} is a stage label, leave it empty \\
... same for the other two maps \\

Now looking into the entire process of setting the output object, quiet a few tasks have to be done. At first for the preparation phase the output has to be updated for the advancedOutput window. Afterwards the GUI elements are resetted (the selectAll buttons, the above mentioned lists and maps). And finally the stage and calculation times are updated in the overview sidebar.

\begin{verbatim}
void OutputWidget::setOutput(const UserOutput::Output &newOutput) {
    output = newOutput;
    // set output in advanced output window
    if (advancedOutput) advancedOutput->setOutput(output);
    
    ui->selectAllStates->setCheckState(Qt::Unchecked);  // reset selectAllButtons
    [...]

    ui->outputListWidgetState->clear(); // clear list widget
    [...]

    vis.itemToState.clear(); // clear maps of old items
    [...]

    updateOverviewTime(); // write stage times and calculation time
\end{verbatim}

After the preparation phase the lists are updated to keep track of all the elements and how to access the content of the output object. For each of the three lists there are own loops to create the corresponding elements. The outer loop loops around all the stages of the solution. The states have the "basic" loop, as parameters and constraints, contain different types, which have to be handled differently.

Populate list with states:

\begin{verbatim}
    for (int i = 0; i < output.stages.size(); i++) {
        [... A spacer for each stage is added for better overview]
        
        for (int j = 0; j < output.stages[i].integrator.states.size(); j++) {
            UserOutput::StateOutput *state = &output.stages[i].integrator.states[j];
            
            [... create item in list and set nametag]

            QListWidgetItem *item = new QListWidgetItem(tr(label.c_str()),
                                                ui->outputListWidgetState);

            [... turn item into a checkbox]
\end{verbatim}

Save that the item with index \lstinline{countItemsState} in the list belongs to stage i state j.

\begin{verbatim}
            vis.itemToState[countItemsState] = std::make_pair(i, j);
\end{verbatim}

Add item to mapVariableToColor for consistent coloring of the same variable. The actual color is assigned later.

\begin{verbatim}
            vis.mapVariableToColor[state->name] = QColor();
        }
\end{verbatim}

For the parameters the loop follows the same scheme as the one for the states, but as there are also initial values saved in the \lstinline{output.stages[i].integrator.parameters[j]} we have to differ between two types and set the elements accordingly in the scroll box:

\begin{verbatim}
        if (type == UserOutput::ParameterOutput::INITIAL) {
            // Special case -> display label "   name (Initial = %value)"

            // make it uncheckable
            item->setFlags(item->flags() & (~Qt::ItemIsSelectable));
        } else {
            // regular case
            [... create item in list and set nametag]
            [... turn item into a checkbox like with states]
        }
\end{verbatim}

For the constraints the loop follows the same scheme as the ones before, but as there are three different constraint types saved in the \lstinline{output.stages[i].optimizer.nonlinearConstraints[j]} we have to differ between these types and set the elements accordingly in the scroll box:

\begin{verbatim}
        UserOutput::ConstraintOutput *constraint =
                         &output.stages[i].optimizer.nonlinearConstraints[j];
        UserOutput::ConstraintOutput::ConstraintType type = constraint->type;

        switch (type) {
            case UserOutput::ConstraintOutput::PATH:
                constraintType = "Path";
                break;
            case UserOutput::ConstraintOutput::POINT:
                constraintType = "Point";
                break;
            case UserOutput::ConstraintOutput::ENDPOINT:
                constraintType = "Endpoint";
                break;
            default:
                [... QErrorMessage]
        }
        label.append(constraintType);
        [... create item in list and set nametag]
        [... turn item into a checkbox like with states]
    }
\end{verbatim}

Together with the constraints the same list is populated with optimizer objectives (each stage only has one objective) as the output object saves these in a similar style.

In the End of the functions \lstinline{vis.assignColors()} is called to set consistent colors for all data points in the graph, as each stage and variable is saved as a separate line series with QT.

\begin{verbatim}
    vis.assignColors(vis.mapVariableToColor);
}
updateUI();
\end{verbatim}

And after calling the \lstinline{updateUI()} routine the setting and updating of the output tab is done.

\paragraph{OutputWidget::Other Functions}

The other functions are trivial UI functions from QT for selecting or pushing buttons, which then call the functions which are closer to DyOS and work directly with the output object. Therefor please consult the code, or the QT documentation directly for learning and understanding the basics of QT.



\subsubsection{Data Selection}\label{sec:DataSettings}

The output object that DyOS produces contains a variety of different data, not just the simulation results, but also meta information, like how long the simulation took and so on. Overall, just like the input object, it is a very heterogeneous data structure, and not every component can be visualized in the same way.

We have decided to focus on a smaller subset of the output object, so the Output Tab only shows data that is plottable over time, with very few exceptions. More details on the chosen input settings for the simulation, or more in depth solution values can be viewed in the Output Summary (\ref{sec:OutputSummaryScreen1}), which contains every aspect of the output in its JSON form.

The three scroll boxes on the left side are a 1:1 mapping of the structure of the Output Object. Figure (\ref{sec:OutputUML2}) again for better explanation.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{Resources/Output/OutputUML2.png}
    \caption{Overview if the Output UML}
\end{figure}

- "Data States" holds all integrator variables\\
- "Data Parameters" holds all optimizer parameters\\
- "Given Constraints \& Objectives" holds all constraints and the optimization objective set in the input\\

When selecting a variable from a certain stage, a piece wise linear or piece wise constant (dependant on the input) graph is plotted.
The different stages are split in the selection boxes by the stage labels and in the graph by the shaded areas and a secondary horizontal axis, which is part of the QChart (see chapter Visualizer \ref{sec:Visualizer} for more).




\subsubsection{Visualizer – {\tt Visualizer}}\label{sec:Visualizer}

As the plotting of the different data types, markers and so on takes quite a lot of code, we have extracted these functions to a separate class. The \lstinline{Visualizer} Class follows the design pattern of a builder:

\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{Resources/Output/DesignPatternBuilder.png}
\caption{Source: https://de.wikipedia.org/wiki/Erbauer\_(Entwurfsmuster)}
\end{figure}

Functions to create a QT Chart Widget are specialized for each data type:

\lstinline{QChart * createChartState(...);} \\
\lstinline{QChart * createChartParameter(...);} \\
\lstinline{QChart * createChartConstraint(...);}

These three always call \lstinline{chartMarkStages())} at the end to render the shading of each second stage.

\lstinline{QChart * chartMarkStages(...); }

For the constraints, the selected data type influences the needed function to draw the right markers.

\lstinline{QChart * chartMarkConstraints(...);} \\
\lstinline{QChart * chartMarkObjectives(...);}

The \lstinline{chartMark...()} functions use three routines to draw the three constraint types:

\lstinline{QChart * drawPath(...);} \\
\lstinline{QChart * drawPoint(...);} \\
\lstinline{QChart * drawEndpoint(...);}

To adapt the output object data which is made up of vectors to the input needed for creating a chart, we have created the function \lstinline{createLineSeries()}. It just takes the values of the vectos and adds them as X and Y values to a QLineSeries object.

\lstinline{QLineSeries * createLineSeries  (...);}

The \lstinline{OutputWidget} uses two functions of the visualizer class to change the appearance of the plot and set a constant color map for all variables. As these are functions concerning the optics, we included these in the \lstinline{Visualizer} class.

Gives each unique variable with in a map a unique color:

\lstinline{void assignColors(std::map<std::string, QColor>& map);}

Function that toggles the gridlines:

\lstinline{QChart *  toggleGridLines(QChart *chart);}
    


To help understand the calling order of the functions, see the following figure:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputUMLVisualizer.png}
\caption{Hierarchy of the functions from the Visualizer class}\label{sec:OutputCallOrder}
\end{figure}

Furthermore the \lstinline{Visualizer} Class includs three maps for displaying the correct output categories. We are saving the element number mapped to the stage number and index of the element.

\lstinline{std::map<int, std::pair<int,int> > itemToState;;} \\
\lstinline{std::map<int, std::pair<int,int> > itemToParameter;} \\
\lstinline{std::map<int, std::pair<int,int> > itemToConstraint;}

A map for displaying the same colors for identical variables is needed as well, saving the variable name and mapping it to a color:

\lstinline{std::map<std::string, QColor> mapVariableToColor;}

The \lstinline{Visualizer} also holds the general settings for the plot, which are used when creating the charts. As a standard these settings are set:

\lstinline{bool showGridLines    = true;} \\
\lstinline{bool showStageShading = true;}

\paragraph{Visualizer::createChartState/Parameter/Constraint()}\label{sec:createChart}

This function is called whenever the selection of the states changes, and it creates the whole chart from scratch. It also shifts the timepoints to fit together with the data, as the output of DyOS is not consistent. The parameters are QT chart object where a new series with the state values is added and the counter i to find selected object in the itemToState map. After adding the line graph of the selected state a QChart* is returned.

At first the time points of the output might need to be shifted, as the time will otherwise start from 0 again in the plot. This is done by adding all the durations of the previous stages (if there are any).

\begin{verbatim}
std::vector<double> shiftedTimePoints;
shiftedTimePoints = state->grid.timePoints;
for (int l = 0; l < stageIndex; l++)
{
    for (int m = 0; m < shiftedTimePoints.size(); m++)
    {
        double duration = output.stages[l].integrator.durations[0].value;
        shiftedTimePoints[m] += duration;
    }
}
\end{verbatim}

Afterwards the values are saved a QT compatible line series is created from it and added to the chart.

\begin{verbatim}
QLineSeries *series = createLineSeries(shiftedTimePoints, values);
series->setName(tr(name.c_str()));
chart->addSeries(series);
\end{verbatim}

In the end the color for the line series is set, by using the mapping in \lstinline{mapVariableToColor} and the stages are shaded by calling \lstinline{chartMarkStages(chart, series)}.


For parameters the function works the same with the addition, that if the case is PIECEWISE\_CONSTANT, the vectos have to be manipulated to fake a piecewise constant line chart, as QT isn't able to plot a piecewise constant line with a single QLineSeries. Therefore we use the numerical lower limit to add a data point right after the second one (so that no two value correspond to the same time) to create a slope with infinity gradient.

For constraints the function works like the one for the states, with the difference, that here it plots constraints and objectives. Also before shifting the time to fit to the previous stage, it also needs to be scaled from [0,1] to [0,stageEnd], as here again DyOS used a different convention. As in the selection box a constraint or a objective can be chosen, we differ these two types by using INT\_MAX as constrIndex in the \lstinline{itemToConstraint} mapping.

\paragraph{Visualizer::chartMarkStages()}

As shown in Figure \ref{sec:OutputCallOrder} this function is called whenever a new chart is drawn, Each second stage is shaded by coloring the background with a QBrush. 

\lstinline{axisX->setShadesBrush(QBrush(QColor(0, 0, 0, 7)));}.

Creating default axes has to be done last in the entire process of creating/refreshing a chart, otherwise the old labels and other custom functions will be reset! Also a second x axis is added where the spacings correspond to the stage times. This has to be done after createDefaultAxes(), otherwise all changes to the axis will be reset.

\paragraph{Visualizer::chartMarkConstraints()}\label{sec:chartMarkConstraints}

\lstinline{chartMarkConstraints()} is called whenever a constraint has been selected and a chart is updated. It marks the chosen constraints in the chart for three cases PATH, POINT, ENDPOINT, each are drawn with their own visualization.

PATH     A tube with dashed boundary lines\\
POINT    A boxplot placed at a defined time, with median=dyos\_solution, whiskers=boundaries\\
ENDPOINT A boxplot placed at the stage end,  with median=dyos\_solution, whiskers=boundaries

For each constraint type there is a own function for drawing the individual marker (see chapter \ref{sec:drawPath}). A switch case is used:

\begin{verbatim}
switch (type) {
case UserOutput::ConstraintOutput::PATH:
    chart = drawPath(constraint, chart, timePoints);
    break;
case UserOutput::ConstraintOutput::POINT:
    chart = drawPoint(constraint, chart, timePoints, stageIndex);
    break;
case UserOutput::ConstraintOutput::ENDPOINT:
    chart = drawEndpoint(constraint, chart, timePoints);
    break;
default:
    [... QErrorMessage]
    break;
\end{verbatim}

\paragraph{Visualizer::chartMarkObjectives()}

Follows the same style like \lstinline{chartMarkConstraints()} \ref{sec:chartMarkConstraints} with the only difference, that the objectives are saved differently in the output:

\lstinline{UserOutput::ConstraintOutput *objective = &output.stages[stageIndex].optimizer.objective;}\\
\lstinline{UserOutput::ConstraintOutput::ConstraintType type = objective->type;}

\paragraph{Visualizer::drawPath/Point/Endpoint()}\label{sec:drawPath}

There are three different constraint types for DyOS: Path, Point and Endpoint Constraints.

A Path Constraint sets an upper and lower boundary for a certain variable over an entire stage. This boundary is marked by drawing a rectangle into the chart - showing lower and upper bound over the stage. The same color is applied by using the \lstinline{mapVariableToColor[]}.

The dashed boundary lines are implemented by adding two new line series and the area shading is done by adding a QAreaSeries to the chart:

\begin{verbatim}
vTime = {timeStart, timeEnd};
vBoundaries = {lowerBound, lowerBound};
series0 = createLineSeries(vTime, vBoundaries);
vBoundaries = {upperBound, upperBound};
series1 = createLineSeries(vTime, vBoundaries);

area = new QAreaSeries(series0, series1);
\end{verbatim}

The QAreaSeries need to be set to \lstinline{pen.setColor(Qt::transparent);} as otherwise it would draw boundaries on the left and right. A example for the path constraint can be seen here for 5 stages:

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotConstraintPath.PNG}
\caption{Constraint Type Path - Area}
\end{figure}

A Point Constraint also enforces a lower and upper boundary but at a certain time. These bounds are visualized with "whiskers", where the concept originally came from box plots. QT has integrated box plots but these cannot be integrated into a chart if there are already other QLineSeries present (see chapter \ref{sec:QTProblems}). Similar to creating the charts with the plotted variables \ref{sec:createChart} the time points need to be shifted for proper placement of the marker. If the lower and upper boundary are not the same a "I" is displayed, which is made up by a vertical and two horizontal QLineSeries. As QT cannot plot a vertical line we used the numerical limits again to offset the second value by a tiny bit.

\begin{verbatim}
vTime = {shiftedTimePoints[0] - std::numeric_limits<double>::min(), shiftedTimePoints[0]};
vBoundaries = {lowerBound, upperBound};
series0 = createLineSeries(vTime, vBoundaries);
\end{verbatim}

To be honest this is a very hacky solution to add boundary whiskers, as standard qt functions otherwise won't make it possible to add a boxplot. For the future the markers should be scaled to the entire graph range, as they otherwise might appear to large or are nearly invisible.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotConstraintPoint.PNG}
\caption{Constraint Type Point - "Whisker" for two Point Constraints}
\end{figure}

When the upper and lower boundary for a (End-)Point Constraint are the same the plot draws a cross "X" to show that these two values are exactly the same and not just very close to each other.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputPlotConstraintCross.PNG}
\caption{Constraint Type (End-)Point - Cross}
\end{figure}

A Endpoint Constraint is similar to the above Point Constraint, whereas the time for Endpoint constraint is set to the stage end. These bounds are also visualized with "whiskers".

\paragraph{Plot Settings}

\begin{figure}[H]
   \begin{minipage}{0.48\textwidth}
     \centering
     \includegraphics[width=1\linewidth]{Resources/Output/OutputGridOff.PNG}
     \caption{Output Grid Off}\label{sec:PlotAppearance1}
   \end{minipage}\hfill
   \begin{minipage}{0.48\textwidth}
     \centering
     \includegraphics[width=1\linewidth]{Resources/Output/OutputGridAndShadingOff.PNG}
     \caption{Output Grid \& Shading Off}\label{sec:PlotAppearance2}
   \end{minipage}
\end{figure}

As it already has been teased in the chapter (\ref{sec:OutputWidget}) Overall Functionality, until now we have implemented two general functions for the appearance of the plot. Please refer to Figure (\ref{sec:PlotAppearance1}) and (\ref{sec:PlotAppearance2}).

\lstinline{void on_buttonShowGrid_clicked();} \\
\lstinline{void on_buttonShowStageShader_clicked();}

These two functions are just a foundation, after the users wish for different more sophisticated plotting tools, future developers can add a wide variety of tools to the plotting pipeline (\ref{sec:FutureOutlook})

\paragraph{Visualizer::assignColors()}

This function takes in a color map and assigns unique colors to all entries, so that each variable keeps it own color, even though the plot is made up of multiple QLineSeries. For now the standard RWTH corporate design colors have been used. There are 10 different colors after which the color map loops around again and starts reusing the colors.

\begin{verbatim}
const int numOfDifferentColors = 10;

int colorIndex=0;
for (auto it = map.begin(); it!=map.end();it++) {
    colorIndex = colorIndex % numOfDifferentColors;
    QColor color;
    switch (colorIndex++) {
    case 0:
        color = QColor(0,97,101,255);   // RWTH Petrol
        break;
\end{verbatim}

\paragraph{Visualizer::toggleGridLines()}

Is called by clicking a button in the output widget, it toggles the gridlines in the chart on or off. Has to be called after all series have been added in OutputWidget::updateUI() to the chart. As otherwise the gridlines will be reset again.

\subsubsection{Advanced Output}\label{sec:AdvancedOutput}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{Resources/Output/OutputSummary.PNG}
\caption{The "true" Output Object contents.}\label{sec:OutputSummaryScreen1}
\end{figure}

The \lstinline{advancedOutput} class converts the internal DyOS Output Object into a more user friendly JSON object (see \ref{sec:JSONConversion}) which can be cleanly visualized in a QTreeView.

For this there is the typical setter function, whereas we didn't include a getter functions, as it is unnecessary to return a unchanged output.

\lstinline{void setOutput(const UserOutput::Output &output);}

\lstinline{QJsonModel} is used to to convert the internal object into a usable format for QT, please refer to chapter \ref{sec:QJsonModel} for more.