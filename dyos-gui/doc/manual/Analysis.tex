\section{Analysis}\label{sec:Analysis}

\subsection{Necessity}

DyOS was previously used by specifying all input options in a {\tt .cpp} file, including the main DyOS header file and compiling it as a standalone program. This gave the users a lot of flexibility, but it also caused a lot of usability problems. The user had to be capable of writing C++ code, he had to know all the different possible settings the program has to offer, and, in some cases, their exact spelling. Default values for the different options were also not visible to the user. Furthermore, the user had no way to check whether the set options are correct and consistent, and the {\tt .cpp} file had to be compiled and linked every time an input setting was changed. Overall, DyOS used to be not very intuitive. 



\subsection{User Requirements}

The GUI is supposed to simplify the use of DyOS for inexperienced users. The main goal of the project is to cover the basic functionality of DyOS, which is essentially the selection of a number of input settings, the selection and inspection of output values and data formats, as well as the launch of the main DyOS process.

As an example and benchmark case, the optimization of a basic, multi-stage car model, as well as a model of a "Williams-Otto" process, should be used for tests. The dynamic models are provided by the AVT.SVT in the form of so-called "eso" model files. Our group has to implement and test both main model interfaces (JADE and FMI).

The GUI is required to run on Windows, and, if our group has enough time, also on Linux and MacOS. As programming language and DyOS interface, C++ and the build platform CMake have to be used. The connection of the GUI to the existing input and output structure of DyOS has to be flexible and well documented, such that future development of the GUI will become easier. An exact specification of the features which have to be implemented in the GUI was given to us in the form of a table.



\subsection{Use Case Analysis}

Before developing the GUI, we analysed the different use cases a user may have for it. These use cases give us a rough idea about what functionality the GUI will have to provide, and we can already categorize these use cases into the three main tabs which the GUI will have: Input, Run and Output.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{Resources/UseCaseEnglish}
    \caption{Use Case Diagram}
    \label{fig:Use Case Diagram}
\end{figure}

\newcommand\addrow[2]{#1 &#2\\ }

\newcommand\addheading[2]{#1 &#2\\ \hline}
\newcommand\tabularhead{\begin{tabular}{lp{8cm}}
\hline
}

\newcommand\addmulrow[2]{ \begin{minipage}[t][][t]{2.5cm}#1\end{minipage}% 
   &\begin{minipage}[t][][t]{8cm}
    \begin{enumerate} #2   \end{enumerate}
    \end{minipage}\\ }

\newenvironment{usecaseI}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseI}
  \addheading{Action}{Generate Inputs}
  \addrow{Goal}{GUI shows all set inputs by the user} 
  \addrow{Precondition}{No existing Input, no model is selected}
  \addrow{Postcondition}{Every specification is set, the GUI is ready to compute}
  \addrow{Postcondition in case of error}{The input options remain unchanged}

  \addmulrow{Main path}{
    \item User sets some settings in "Global" Tab
    \item User chooses model for all stages
    \item User sets Integrator options for all stages
    \item User sets Optimizer options for all stages
    }
\end{usecaseI}

\newenvironment{usecaseIa}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseIa}
  \addheading{Action}{Add Stage}
  \addrow{Goal}{The input adds a stage} 
  \addrow{Precondition}{The input has a specific number \# of stages, at least one. The Input tab is selected.}
  \addrow{Postcondition}{The input now consists of \# + 1 stages}
  \addrow{Postcondition in case of error}{No new stage is added, there is still at least one stage} 
  
  \addmulrow{Main path}{
    \item User pushes "Add Stage" Button
    \item GUI duplicates current stage with settings etc\\
    }
\end{usecaseIa}\\

\newenvironment{usecaseIb}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseIb}
  \addheading{Action}{Set Model}
  \addrow{Goal}{Associate an eso model to an stage} 
  \addrow{Precondition}{The User is in the "Input" Tab, and in the "Stage" sub tab}
  \addrow{Postcondition}{The stage now has an associated eso model}
  \addrow{Postcondition in case of error}{No model is selected for the stage}
  \addmulrow{Main path}{
    \item User pushes the button next to the text box
    \item User selects path where model is located
    \item User presses the "Load Model" button
    \item GUI shows model path\\
    }
\end{usecaseIb}\\

\newenvironment{usecaseIc}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseIc}
  \addheading{Action}{Set Integratoroptions (Optimizeroptions analogous)}
  \addrow{Goal}{Customize options for the Integrator} 
  \addrow{Precondition}{An Integrator was chosen in "Global" Tab, the "Integrator" sub tab is shown}
  \addrow{Postcondition}{All Integratoroptions are set for a specific stage}
  \addrow{Postcondition in case of error}{No settings were saved, the standard settings are shown in the GUI}
  \addmulrow{Main path}{
    \item User adds all parameters he wants to customize
    \item User adds all variables he wants to choose initial values for
    \item User changes settings for all parameters and variables
    }
\end{usecaseIc}\\

\newenvironment{usecaseII}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseII}
  \addheading{Action}{Run DyOS}
  \addrow{Goal}{Run a computation with DyOS} 
  \addrow{Precondition}{The user has already put in all the settings, the model, etc.}
  \addrow{Postcondition}{DyOS has successfully computed the results, the output is shown in the Output Tab}
  \addrow{Postcondition in case of error}{DyOS is interrupted and the debugging output is shown in the command line output.}

  \addmulrow{Main path}{
    \item User changes to the "Run" Tab
    \item User pushes "Run DyOS" Button
    \item DyOS runs and computes the wanted output\\
    }
\end{usecaseII}\\

\newenvironment{usecaseIIIa}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseIIIa}
  \addheading{Action}{Show Results}
  \addrow{Goal}{Show plots for specified variables computed by DyOS} 
  \addrow{Precondition}{An output of DyOS is loaded in the GUI.}
  \addrow{Postcondition}{The GUI is showing plots for the selected variables from the output.}
  \addrow{Postcondition in case of error}{The GUI is not showing the correct output plots.}

  \addmulrow{Main path}{
    \item User changes to the "Output" Tab
    \item User selects states he wants to plot
    \item User selects parameters he wants to plot
    \item User selects constraints he wants to plot
    \item The GUI shows plots for the selected values\\
    }
\end{usecaseIIIa}\\

\newenvironment{usecaseIIIb}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseIIIb}
  \addheading{Action}{Save results}
  \addrow{Goal}{Save results as file} 
  \addrow{Precondition}{A computation was successfully completed.}
  \addrow{Postcondition}{The results are saved in a text file.}
  \addrow{Postcondition in case of error}{No results were saved.}
  \addmulrow{Main path}{
    \item User is located in the "Output" Tab
    \item User navigates to the File menu or uses {\tt Ctrl+S} to save the results
    \item User chooses file path for his result file
    \item User saves his result\\
    }
\end{usecaseIIIb}\\

\newenvironment{usecaseIIIc}{\tabularhead}
{\hline\end{tabular}}
\begin{usecaseIIIc}
  \addheading{Action}{Load results}
  \addrow{Goal}{The GUI shows the loaded results} 
  \addrow{Precondition}{The user has a file that can be visualized by the GUI}
  \addrow{Postcondition}{The GUI is showing the results in the "Output" Tab}
  \addrow{Postcondition in case of error}{The file is not compatible with DyOS GUI. Nothing is displayed in the GUI}

  \addmulrow{Main path}{
    \item User is located in the "Output" Tab
    \item User navigates to the File menu or uses {\tt Ctrl+O}
    \item User selects his wanted file
    \item The information is shown in the GUI\\
    }
\end{usecaseIIIc}\\



\subsection{Requirements}

\subsubsection{System Requirements}

The GUI obviously relies on DyOS, so the same system requirements apply to the GUI as the ones specified for DyOS in its manual. Those e.g. include a version of CMake (version 3.11 or newer) for building the GUI and DyOS itself, the redistributable libraries for Intel Fortran Compilers and a C++ compiler with the C++14 standard. Also, version 1.66 or newer of the boost library (consisting of at least libraries filesystem, system and date\_time) is needed. For using the JADE interface, the system should also have the gcc compiler and python 2.7.

In addition to all these requirements, the user must have a version of the Qt library installed, which is necessary for compiling the GUI. An open source version can be downloaded here: \url{https://www.qt.io}



\subsubsection{Functional Requirements}

The contract for the GUI came with a list of very detailed expectations for the final product, especially regarding the input options which should be supported. If applicable, the options to chose from are mentioned in the document (e.g. the three different integrators that can be chosen), if not, the type of input is specified (e.g. "positive integer"), and the GUI is supposed to let the user enter information in suitable UI elements. These inputs should then be checked for validity according to the specifications.

We also have to design a way to present the user with explanations for the different input options.

Additionally, the GUI obviously has to be capable of actually executing DyOS, but also show live runtime information, which is printed on the command line by DyOS.

The specification for representing the output results in the GUI are less precise, so we have a bit more freedom for designing a solution. We were told by our supervisor that the graphing abilities of the GUI do not have to be very advanced, since it will mostly serve as a broad overview and "sanity check", whether the solution makes sense.

For a more advanced and beautiful plot, the GUI user should be able to export the data and open it in another plotting program.



\subsubsection{Non-functional Requirements}

The GUI has to be created in English. It should use the Qt framework and be programmed in C++.

It is up to our group to decide whether we want to use the text file interface, the direct C++ interface or both of them for our implementation.

Furthermore, the installation of the GUI should be implemented in the usual DyOS build process, so the existing CMake build system should be extended for the GUI. 

As Windows is the mainly used operating system for the use of DyOS, making the GUI run on Windows is the main focus. Lastly the implementation should be designed in such a way that it will be easy to expand upon, which also requires a good and detailed documentation. 



\subsubsection{Optional Requirements}

The previously mentioned list with requirements leaves a few optional input options which are either not yet working in the actual DyOS code or are not important for the main use at the current state. These options can be implemented as well, but should be greyed out.

The saving and loading for input text files is not urgent, but would be a nice expansion for the GUI. 

The main focus may lie on Windows, but if there is enough time to implement Linux and Mac compatibility, that should be looked at as well. 
