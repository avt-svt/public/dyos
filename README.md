# DyOS Public Version


The DyOS repository contains submodules for all dependencies. Hence, you need to use the following commands (on Windows, you might need to get Git Bash first).

    $ git clone https://git.rwth-aachen.de/avt.svt/public/dyos.git <directory> && cd <directory> <directory> && cd <directory>
    $ git submodule init
    $ git submodule update
	

Doublecheck CMAKE_INSTALL_PREFIX before running install target.


=====================================================================  \n

(c) Lehrstuhl fuer Systemverfahrenstechnik/Prozesstechnik, RWTH Aachen \n
=====================================================================  \n


DyOS has been developed at AVT.PT (RWTH Aachen) and is still under 
continuous development at AVT.SVT (RWTH Aachen). We are grateful for 
funding from DFG, EU, BMBF, BMWi, Helmholtz and various private 
entities that have contributed directly or indirectly to DyOS.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.

