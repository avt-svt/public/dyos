/**
* @file SolverConfig.hpp.cmake
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Configuration Header for Dyos                                        \n
* =====================================================================\n
* configure header                                                     \n
* =====================================================================\n
* @author Johannes M.M. Faust
* @date 20.11.2018
*/

#pragma once

//(non)linear equation solvers
#cmakedefine BUILD_LINSOL_MA28 "@BUILD_LINSOL_MA28@"
#cmakedefine BUILD_NLEQ1S "@BUILD_NLEQ1S@"

//optimizers
#cmakedefine BUILD_IPOPT "@BUILD_IPOPT@"
#cmakedefine BUILD_FILTERSQP "@BUILD_FILTERSQP@"
#cmakedefine BUILD_NPSOL "@BUILD_NPSOL@"
#cmakedefine BUILD_SNOPT "@BUILD_SNOPT@"

//integrators
#cmakedefine BUILD_LIMEX "@BUILD_LIMEX@"
#cmakedefine BUILD_IDAS "@BUILD_IDAS@"


